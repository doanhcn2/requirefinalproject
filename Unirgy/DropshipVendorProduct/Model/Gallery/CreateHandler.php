<?php

namespace Unirgy\DropshipVendorProduct\Model\Gallery;

class CreateHandler extends \Magento\Catalog\Model\Product\Gallery\CreateHandler
{
    protected $_renamedImages = [];
    protected $_allowUseRenamedImage=false;
    public function execute($product, $arguments = [])
    {
        $this->_allowUseRenamedImage = $product->getData('_allow_use_renamed_image');
        $result = parent::execute($product, $arguments);
        $this->_allowUseRenamedImage = false;
        return $result;
    }
    protected function moveImageFromTmp($file)
    {
        if ($this->_allowUseRenamedImage
            && isset($this->_renamedImages[$file])
        ) {
            return $this->_renamedImages[$file];
        } else {
            $renamedImage = parent::moveImageFromTmp($file);
            $this->_renamedImages[$file] = $renamedImage;
            return $renamedImage;
        }
    }
}