<?php

namespace Unirgy\DropshipVendorProduct\Block\Vendor\Product;

use Magento\Backend\Block\Media\Uploader;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;

class GalleryContentCfg extends GalleryContent
{
    /*
    public function getId()
    {
        if ($this->getData('id')===null) {
            $this->setData('id', $this->_hlp->getObj('\Magento\Framework\Math\Random')->getUniqueHash('id_'));
        }
        return $this->getData('id');
    }

    public function getHtmlId()
    {
        return $this->getId();
    }
    */
    public function idKey($separator='_')
    {
        return $this->_idKey($separator);
    }
    protected function _idKey($separator='_')
    {
        $cfgAttrs = $this->getCfgAttributes();
        $cfgAttrIds = array();
        foreach ($cfgAttrs as $__ca) {
            $cfgAttrIds[] = $__ca->getAttributeId();
        }
        $cfgAttrVals = $this->getCfgAttributeValueTuple();
        $cfgIdKey = '';
        foreach ($cfgAttrs as $__i=>$__ca) {
            $cfgIdKey .= $cfgAttrIds[$__i].$separator.$cfgAttrVals[$__i].$separator;
        }
        $cfgIdKey = substr($cfgIdKey, 0, -1*strlen($separator));
        return $cfgIdKey;
    }
    public function doSuffix($prefix='')
    {
        return $prefix.$this->_idKey('_');
    }
    protected $_product;
    public function setProduct($product)
    {
        $cfgAttrs = $this->getCfgAttributes();
        $filter = array();
        $tuple = $this->getCfgAttributeValueTuple();
        foreach ($cfgAttrs as $__i => $__ca) {
            $filter[$__ca->getAttributeCode()] = $tuple[$__i];
        }
        $simples = $this->_prodHlp->getFilteredSimpleProductData($product, $filter);
        if (empty($simples)) {
            $this->_product = $this->_prodHlp->initProductEdit(array(
                'id' => false,
                'type_id' => 'simple',
                'template_id' => $product->getId(),
                'data' => array()
            ));
        } else {
            foreach ($simples as $simple) {
                $this->_product = $simple['product'];
                break;
            }
        }
        $this->_product->getResource()->getAttribute('media_gallery')->getBackend()->afterLoad($this->_product);
        return $this;
    }
    public function getProduct()
    {
        return $this->_product;
    }
    public function getImages()
    {
        return $this->getProduct()->getData('media_gallery') ?: null;
    }
    public function getImageTypes()
    {
        $imageTypes = array();
        foreach ($this->getMediaAttributes() as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $imageTypes[$attribute->getAttributeCode()] = array(
                'label' => $attribute->getFrontend()->getLabel(),
                'field' => $this->getAttributeFieldName($attribute),
                'name' => $this->getAttributeFieldName($attribute),
                'code' => $attribute->getAttributeCode(),
                'value' => $this->getProduct()->getData($attribute->getAttributeCode()),
            );
        }
        return $imageTypes;
    }

    public function getAttributeFieldName($attribute)
    {
        $name = $attribute->getAttributeCode();
        $name = sprintf("media_gallery[cfg_attributes][%s][%s]",
            $this->_idKey('-'),
            $name
        );
        if ($suffix = $this->getForm()->getFieldNameSuffix()) {
            $name = $this->getForm()->addSuffixToName($name, $suffix);
        }
        return $name;
    }
    public function getName()
    {
        $name = sprintf("media_gallery[cfg_images][%s]",
            $this->_idKey('-')
        );
        if ($suffix = $this->getForm()->getFieldNameSuffix()) {
            $name = $this->getForm()->addSuffixToName($name, $suffix);
        }
        return $name;
    }
    public function getImagesJson()
    {
        $value = $this->getImages();
        if (is_array($value) &&
            array_key_exists('images', $value) &&
            is_array($value['images']) &&
            count($value['images'])
        ) {
            $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
            $images = $this->sortImagesByPosition($value['images']);
            foreach ($images as &$image) {
                $image['url'] = $this->_mediaConfig->getMediaUrl($image['file']);
                try {
                    $fileHandler = $mediaDir->stat($this->_mediaConfig->getMediaPath($image['file']));
                    $image['size'] = $fileHandler['size'];
                } catch (FileSystemException $e) {
                    $image['url'] = $this->getImageHelper()->getDefaultPlaceholderUrl('small_image');
                    $image['size'] = 0;
                    $this->_logger->warning($e);
                }
            }
            return $this->_jsonEncoder->encode($images);
        }
        return '[]';
    }
    private function sortImagesByPosition($images)
    {
        if (is_array($images)) {
            usort($images, function ($imageA, $imageB) {
                return ($imageA['position'] < $imageB['position']) ? -1 : 1;
            });
        }
        return $images;
    }
    public function getMediaAttributes()
    {
        return $this->getProduct()->getMediaAttributes();
    }
}