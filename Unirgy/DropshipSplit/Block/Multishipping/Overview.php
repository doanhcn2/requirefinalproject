<?php

namespace Unirgy\DropshipSplit\Block\Multishipping;

class Overview
    extends \Magento\Multishipping\Block\Checkout\Overview
{
    static $_blockIter = 0;
    protected $_hlp;
    protected $_hlpPr;
    protected $_udsHlp;
    protected $_productFactory;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\Dropship\Helper\ProtectedCode $udropshipHelperProtected,
        \Unirgy\DropshipSplit\Helper\Data $udsplitHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\DropshipSplit\Model\Cart\VendorFactory $cartVendorFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Multishipping\Model\Checkout\Type\Multishipping $multishipping,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector,
        \Magento\Quote\Model\Quote\TotalsReader $totalsReader,
        array $data = []
    ) {
        $this->_hlp = $udropshipHelper;
        $this->_hlpPr = $udropshipHelperProtected;
        $this->_udsHlp = $udsplitHelper;
        $this->_productFactory = $productFactory;
        $this->_cartVendorFactory = $cartVendorFactory;
        return parent::__construct($context, $multishipping, $taxHelper, $priceCurrency, $totalsCollector, $totalsReader, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setData('module_name', 'Magento_Multishipping');
        $this->setTemplate('Magento_Multishipping::checkout/overview.phtml');
    }

    public function getShippingAddressItems($a)
    {
        if (!$this->_udsHlp->isActive()) {
            return parent::getShippingAddressItems($a);
        }

        $q = $this->getCheckout()->getQuote();
        $methods = array();
        $details = $a->getUdropshipShippingDetails();
        if ($details) {
            $details = $this->_hlp->jsonDecode($details);
            $methods = isset($details['methods']) ? $details['methods'] : array();
        }

        $aItems = $a->getAllVisibleItems();
        $vendorItems = array();
        foreach ($aItems as $item) {
            $item->setQuoteItem($q->getItemById($item->getQuoteItemId()));
            $vendorItems[$item->getUdropshipVendor()][] = $item;
        }

        $items = array();

        foreach ($vendorItems as $vId=>$vItems) {
            $obj = $this->_cartVendorFactory->create()
                ->setQuote($q)
                ->setProduct($this->_productFactory->create());
            if (!$this->_hlp->getScopeFlag('carriers/udsplit/hide_vendor_name')) {
                $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                    ->setPart('header')
                    ->setVendor($this->_hlp->getVendor($vId)));
            }

            foreach ($vItems as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                $items[] = $item;
            }

#echo "<pre>"; print_r($rates[$vId]); echo "</pre>";
            $obj = $this->_cartVendorFactory->create()
                ->setQuote($q)
                ->setProduct($this->_productFactory->create());
            $items[] = $obj->setQuoteItem($this->_cartVendorFactory->create()
                ->setPart('footer')
                ->setVendor($this->_hlp->getVendor($vId))
                ->setShowDropdowns(false)
                ->setEstimateRates(array())
                ->setErrorsOnly(false)
                ->setShippingMethod(isset($methods[$vId]) ? $methods[$vId] : null)
                ->setItems($vItems)
                ->setAddress($a)
                ->setQuote1($q));

        }
        return $items;
    }

    public function getItemHtml(\Magento\Framework\DataObject $item)
    {
        if ($item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
            || $item->getQuoteItem() instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
        ) {
            $qItem = !$item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
                || $item->getProduct() && !$item->getProduct()->getId()
                    ? $item->getQuoteItem()
                    : $item;
            $blockName = "vendor_{$qItem->getVendor()->getId()}_{$qItem->getPart()}_".(self::$_blockIter++);
            return $this->getLayout()->createBlock('\Unirgy\DropshipSplit\Block\Multishipping\Vendor', $blockName)
                ->addData($qItem->getData())
                ->setQuote($qItem->getQuote1())
                ->toHtml();
        }
        return parent::getItemHtml($item);
    }

    public function getRowItemHtml(\Magento\Framework\DataObject $item)
    {
        if ($item instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
            || $item->getQuoteItem() instanceof \Unirgy\DropshipSplit\Model\Cart\Vendor
        ) {
            return $this->getItemHtml($item);
        }
        return parent::getRowItemHtml($item);
    }

}