<?php

namespace Unirgy\DropshipProductBulkUpload\Model;

use Magento\Framework\Model\AbstractModel;

class ColumnsConfig extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig');
    }
}
