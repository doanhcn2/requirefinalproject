<?php

/**
 * Class ModelImportConfig
 *
 * @method string getRunStatus
 * @method int getRowsFound
 * @method int getRowsProcessed
 * @method int getProfileStatus
 * @method string getSnapshotAt
 * @method string getStartedAt
 * @method int getStoreId
 * @method $this setRunStatus(string $status)
 * @method $this setPausedAt(string $date)
 * @method $this setStoppedAt(string $date)
 * @method $this setFinishedAt(string $date)
 * @method $this setStartedAt(string $date)
 * @method $this setStoreId(int $id)
 * @method $this unsPausedAt()
 */
namespace Unirgy\DropshipProductBulkUpload\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Unirgy\Dropship\Helper\Data as DropshipHelper;

/**
 * Class Unirgy\DropshipProductBulkUpload\Model\ImportConfig
 *
 * @method string getRunStatus
 * @method int getRowsFound
 * @method int getRowsProcessed
 * @method int getProfileStatus
 * @method string getSnapshotAt
 * @method string getStartedAt
 * @method int getStoreId
 * @method $this setRunStatus(string $status)
 * @method $this setPausedAt(string $date)
 * @method $this setStoppedAt(string $date)
 * @method $this setFinishedAt(string $date)
 * @method $this setStartedAt(string $date)
 * @method $this setStoreId(int $id)
 * @method $this unsPausedAt()
 */
class ImportConfig
    extends AbstractModel
{
    /**
     * @var DropshipHelper
     */
    protected $_hlp;

    public function __construct(
        DropshipHelper $udropshipHelper,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [])
    {
        $this->_hlp = $udropshipHelper;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected $_jsonFields = [
        'columns'       => 'columns_json',
        'options'       => 'options_json',
        'conditions'    => 'conditions_json',
        'profile_state' => 'profile_state_json',
    ];

    protected $_defaults   = [
        'options' => [
            'csv'      => [
                'delimiter'            => ',',
                'enclosure'            => '"',
                'escape'               => '\\',
                'multivalue_separator' => ';',
            ],
            'encoding' => [
                'from' => 'UTF-8',
                'to'   => 'UTF-8',
            ],
        ],
    ];

    public function _construct()
    {
        $this->_init('Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig');
    }

    public function getAttributeSetId()
    {
        return implode('-', array_slice(explode('-', $this->getData('title')), -2));
    }

    public function beforeSave()
    {
        $this->_serializeData();
        parent::beforeSave();
        $this->_dataSaveAllowed = $this->_getData('title') && $this->_getData('profile_type');
    }

    protected function _afterLoad()
    {
        try {
            $this->_hlp->getScopeConfig('urapidflow/dirs/log_dir', $this->getStoreId());
        } catch(\Exception $e) {
            $this->setStoreId(0);
        }
        $this->_unserializeData();
        $this->_applyDefaults();
        parent::_afterLoad();
    }

    protected function _applyDefaults()
    {
        foreach ($this->_defaults as $k => $d) {
            $this->setData($k, $this->_arrayMergeRecursive($d, (array) $this->getData($k)));
        }
    }

    protected function _serializeData()
    {
        foreach ($this->_jsonFields as $k => $f) {
            if (null !== $this->getData($k)) {
                $this->setData($f, $this->_hlp->jsonEncode($this->getData($k)));
            }
        }
    }

    protected function _unserializeData()
    {
        foreach ($this->_jsonFields as $k => $f) {
            if (null !== $this->getData($f)) {
                $this->setData($k, $this->_hlp->jsonDecode($this->getData($f)));
            }
        }
    }

    protected function _arrayMergeRecursive()
    {
        $params = func_get_args();
        $return = array_shift($params);
        foreach ($params as $array) {
            foreach ($array as $key => $value) {
                if (is_numeric($key) && (!in_array($value, $return))) {
                    if (is_array($value) && isset($return[$key])) {
                        $return[] = $this->_arrayMergeRecursive($return[$key], $value);
                    } else {
                        $return[] = $value;
                    }
                } else {
                    if (isset($return[$key]) && is_array($value) && is_array($return[$key])) {
                        $return[$key] = $this->_arrayMergeRecursive($return[$key], $value);
                    } else {
                        $return[$key] = $value;
                    }
                }
            }
        }

        return $return;
    }

}
