<?php

namespace Unirgy\DropshipProductBulkUpload\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Unirgy\DropshipProductBulkUpload\Model\ImportConfigFactory;

class ImportConfig extends AbstractDb
{
    /**
     * @var ImportConfigFactory
     */
    protected $_importConfigFactory;

    public function __construct(
        ImportConfigFactory $importConfigFactory,
        Context $context
    )
    {
        $this->_importConfigFactory = $importConfigFactory;

        parent::__construct($context);
    }

    public function _construct()
    {
        $this->_init('udprodupload_import_config', 'import_id');
    }

    public function sync(AbstractModel $object, $saveFields = null, $loadFields = null)
    {
        $conn  = $this->getConnection();
        $table = $this->getMainTable();

        $condition = $conn->quoteInto($this->getIdFieldName() . '=?', $object->getId());

        if ($saveFields) {
            $saveData = [];
            foreach ($saveFields as $k) {
                $saveData[$k] = $object->getData($k);
            }
            $conn->update($table, $saveData, $condition);
        }

        if ($loadFields) {
            $loadData = $conn->fetchRow($conn->select()->from($table, $loadFields)->where($condition));
            foreach ($loadData as $k => $v) {
                $object->setData($k, $v);
            }
        }

        return $this;
    }

    /**
     * @param $title
     * @return false|\Unirgy\DropshipProductBulkUpload\Model\ImportConfig
     */
    public function findModelByTitle($title)
    {
        $model = $this->getModel();
        $this->load($model, $title, 'title');
        $model->afterLoad();
        return $model;
    }

    /**
     * @return false|\Unirgy\DropshipProductBulkUpload\Model\ImportConfig
     */
    public function getModel()
    {
        return $this->_importConfigFactory->create();
    }

    public function findLastImportByVendorId($vendorId)
    {
        $read   = $this->getConnection();
        $select = $read->select()->from($this->getMainTable())
            ->where('vendor_id=?', $vendorId)
            ->order('started_at DESC');

        $data = $read->fetchRow($select);
        $model = $this->getModel();

        if($data){
            $model->setData($data);
        }
        return $model;
    }

}
