<?php

namespace Unirgy\DropshipProductBulkUpload\Model\ResourceModel\CatalogRule;

use Magento\CatalogRule\Model\ResourceModel\Rule\Collection as RuleCollection;
use \Unirgy\DropshipProductBulkUpload\Helper\Data as BulkUploadHelper;

class Collection extends RuleCollection
{
    protected function _construct()
    {
        $this->_init(
            'Unirgy\DropshipProductBulkUpload\Model\CatalogRule',
            'Unirgy\DropshipProductBulkUpload\Model\ResourceModel\CatalogRule'
        );
    }

    public function addIsActiveFilter($filterNow = false)
    {
        if ($filterNow) {
            $this->getSelect()->where('from_date<=?', BulkUploadHelper::now(true));
        }
        $this->getSelect()->where('to_date>=? or to_date is null', BulkUploadHelper::now(true));
        $this->getSelect()->where('is_active=1');
        return $this;
    }
}
