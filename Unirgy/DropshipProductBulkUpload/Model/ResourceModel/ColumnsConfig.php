<?php

namespace Unirgy\DropshipProductBulkUpload\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ColumnsConfig extends AbstractDb
{
    protected $_serializableFields   = [
        'columns' => [null, []],
        'cfg_columns' => [null, []]
    ];
    public function _construct()
    {
        $this->_init('udprodupload_columns_config', 'columns_config_id');
    }
}