<?php

namespace Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection
    extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Unirgy\DropshipProductBulkUpload\Model\ImportConfig',
            'Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig'
        );
        parent::_construct();
    }
}
