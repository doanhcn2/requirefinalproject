<?php

namespace Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Unirgy\DropshipProductBulkUpload\Model\ColumnsConfig',
            'Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig'
        );
        parent::_construct();
    }
    protected function _toOptionArray($valueField='columns_config_id', $labelField='name', $additional=[])
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
    protected function _toOptionHash($valueField='columns_config_id', $labelField='name')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }
}