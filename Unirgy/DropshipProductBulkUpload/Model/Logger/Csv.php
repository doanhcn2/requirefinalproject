<?php

/**
 * Class LoggerCsv
 *
 * @method $this setLevelSuccess(bool $level)
 * @method $this setLevelWarning(bool $level)
 * @method $this setLevelError(bool $level)
 * @method HelperData getProfile()
 * @method bool getLevelSuccess()
 * @method bool getLevelWarning()
 * @method bool getLevelError()
 */
namespace Unirgy\DropshipProductBulkUpload\Model\Logger;
use Unirgy\DropshipProductBulkUpload\Exception;
use Unirgy\DropshipProductBulkUpload\Helper\Data;

/**
 * Class Csv
 * @method Data getProfile()
 * @method bool getLevelSuccess()
 * @method bool getLevelWarning()
 * @method bool getLevelError()
 * @method $this setLevelSuccess(bool $bool)
 * @method $this setLevelWarning(bool $bool)
 * @method $this setLevelError(bool $bool)
 * @package Unirgy\DropshipProductBulkUpload\Model\Logger
 */
class Csv
    extends AbstractLogger
{
    protected $_defaultIoModel = \Unirgy\DropshipProductBulkUpload\Model\Io\Csv::class;

    /**
     * @param string $mode
     * @return $this
     * @throws Exception
     */
    public function start($mode)
    {
        $this->getIo()->open($this->getProfile()->getLogFilename(), $mode);
        $level = $this->getProfile()->getData('options/log/min_level');
        $this->setLevelSuccess($level === 'SUCCESS');
        $this->setLevelWarning($level === 'SUCCESS' || $level === 'WARNING');
        $this->setLevelError($level === 'SUCCESS' || $level === 'WARNING' || $level === 'ERROR');
        return $this;
    }

    public function stop()
    {
      $this->getIo()->close();
    }

    public function log($rowType, $data)
    {
        $data = (array)$data;
        if (null !== $rowType) {
            array_unshift($data, $rowType);
        }
        $this->getIo()->write($data);
        return $this;
    }

    public function success($message = null)
    {
        if ($this->getLevelSuccess()) {
            $this->log('SUCCESS', [$this->getLine(), $this->getColumn(), $message]);
        }
        return $this;
    }

    public function warning($message = null)
    {
        if ($this->getLevelWarning()) {
            $this->log('WARNING', [$this->getLine(), $this->getColumn(), $message]);
        }
        return $this;
    }

    public function notice($message = null)
    {
        $this->log('NOTICE', [$this->getLine(), $this->getColumn(), $message]);
        return $this;
    }

    public function error($message = null)
    {
        if ($this->getLevelError()) {
            $this->log('ERROR', [$this->getLine(), $this->getColumn(), $message]);
        }
        return $this;
    }

    public function setIo($io)
    {
        parent::setIo($io);
        $this->getIo()->setBaseDir($this->getProfile()->getLogBaseDir());
        return $this;
    }
}
