<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipProductBulkUpload
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */


namespace Unirgy\DropshipProductBulkUpload\Model\Io;

use Magento\Framework\DataObject;
use Unirgy\DropshipProductBulkUpload\Exception;
use Unirgy\DropshipProductBulkUpload\Helper\Data as HelperData;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File as FileDriver;

/**
 * Class File
 *
 * @method string getBaseDir()
 * @method $this setBaseDir(string $dir)
 */
class File extends DataObject implements IoInterface
{
    /**
     * @var HelperData
     */
    protected $udpupHlp;

    /**
     * @var DirectoryList
     */
    protected $_directoryList;

    /**
     * @var FileDriver
     */
    protected $_fileDriver;
    protected $_openMode;
    protected $_filename;
    protected $_fp;

    public function __construct(
        HelperData $helperData,
        DirectoryList $directoryList,
        FileDriver $fileDriver,
        array $data = []
    )
    {
        $this->udpupHlp = $helperData;
        $this->_directoryList = $directoryList;
        $this->_fileDriver = $fileDriver;

        parent::__construct($data);
    }

    public function isOpen()
    {
        return (bool)$this->_fp;
    }

    public function seek($offset, $whence = SEEK_SET)
    {
        @fseek($this->_fp, $offset, $whence);

        return $this;
    }

    public function tell()
    {
        return ftell($this->_fp);
    }

    public function read()
    {
        $length = $this->getData('read_length') ?: 1024;

        return fread($this->_fp, $length);
    }

    public function write($data)
    {
        $length = $this->getData('write_length');
        fwrite($this->_fp, $data, $length);

        return $this;
    }

    public function reset()
    {
        $filename = $this->_filename;
        $openMode = $this->_openMode;
        $this->close();
        @unlink($filename);
        $this->open($filename, $openMode);

        return $this;
    }

    /**
     * Close file and reset file pointer
     */
    public function close()
    {
        if (!$this->_fp) {
            return;
        }
        @fclose($this->_fp);

        $this->_fp = null;
        $this->_filename = null;
    }

    public function open($filename, $mode)
    {
        $filename = $this->getFilepath($filename);
        if ($this->_fp) {
            if ($this->_filename === $filename && $this->_openMode === $mode) {
                return $this;
            }
            $this->close();
        }

        $this->_fp = @fopen($filename, $mode);
        if ($this->_fp === false) {
            $e = error_get_last();
            throw new Exception(
                __("Unable to open the file %1 with mode '%2' (%3)",
                    $filename, $mode, $e['message']));
        }

        $this->_openMode = $mode;
        $this->_filename = $filename;

        return $this;
    }

    public function getFilepath($filename)
    {
        $dir = $this->dir();

        return rtrim($dir, '/') . DIRECTORY_SEPARATOR . ltrim($filename, '/');
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function dir()
    {
        if (!$this->getBaseDir()) {
            $this->setBaseDir($this->_directoryList->getPath('var') . DIRECTORY_SEPARATOR .'udprodupload');
        }
        $dir = $this->getBaseDir();
        if ($dir && !$this->_fileDriver->isExists($dir)) {
                $this->_fileDriver->createDirectory($dir, 0775);
        }

        return $dir;
    }

    /**
     * Close file on object destruct
     */
    public function __destruct()
    {
        $this->close();
    }
}
