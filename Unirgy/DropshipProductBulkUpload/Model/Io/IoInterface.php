<?php
/**
 * Created by pp
 *
 * @project magento210ee
 */

namespace Unirgy\DropshipProductBulkUpload\Model\Io;

interface IoInterface
{
    public function open($filename, $mode);

    public function isOpen();

    public function close();

    public function seek($offset, $whence = SEEK_SET);

    public function tell();

    public function read();

    public function write($data);

    public function reset();

}
