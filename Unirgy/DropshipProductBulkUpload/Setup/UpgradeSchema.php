<?php

namespace Unirgy\DropshipProductBulkUpload\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    const TEXT_SIZE = 65536;
    protected $_hlp;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper
    ) {
        $this->_hlp = $udropshipHelper;
    }
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();
        if (version_compare($context->getVersion(), '3.1.0', '<')) {
            $columnsConfigTable = $setup->getTable('udprodupload_columns_config');
            $connection->addColumn($columnsConfigTable, 'options', ['TYPE'=>Table::TYPE_TEXT,'nullable' => true,'LENGTH'=>self::TEXT_SIZE,'COMMENT'=>'options']);
        }
        if (version_compare($context->getVersion(), '3.2.2', '<')) {
            $columnsConfigTable = $setup->getTable('udprodupload_import_config');
            $connection->addColumn($columnsConfigTable, 'reindexing', ['TYPE'=>Table::TYPE_SMALLINT,'nullable' => false, 'default'=>0,'COMMENT'=>'reindexing']);
            $connection->addColumn($columnsConfigTable, 'reindex_total', ['TYPE'=>Table::TYPE_INTEGER,'nullable' => true,'COMMENT'=>'reindex_total']);
            $connection->addColumn($columnsConfigTable, 'reindex_processed', ['TYPE'=>Table::TYPE_INTEGER,'nullable' => true,'COMMENT'=>'reindex_processed']);
        }

        $setup->endSetup();
    }
}
