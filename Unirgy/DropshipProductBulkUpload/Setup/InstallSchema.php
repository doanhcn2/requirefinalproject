<?php

namespace Unirgy\DropshipProductBulkUpload\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    const MEDIUMTEXT_SIZE=16777216;
    const TEXT_SIZE=65536;
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        $columnsConfigTable = $installer->getTable('udprodupload_columns_config');
        $table = $connection->newTable($columnsConfigTable)
            ->addColumn('columns_config_id', Table::TYPE_INTEGER, 10, [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true
            ])
            ->addColumn('vendor_id', Table::TYPE_INTEGER, null, ['nullable' => true])
            ->addColumn('set_id', Table::TYPE_TEXT, 64, ['nullable' => true])
            ->addColumn('name', Table::TYPE_TEXT, 64, ['nullable' => true])
            ->addColumn('columns', Table::TYPE_TEXT, self::MEDIUMTEXT_SIZE, ['nullable' => true])
            ->addColumn('cfg_columns', Table::TYPE_TEXT, self::MEDIUMTEXT_SIZE, ['nullable' => true])
            ->setComment('Bulk Upload Columns Config Table')
            ->setOption('type', 'InnoDB')
            ->setOption('charset', 'utf8');
        $connection->createTable($table);

        $importConfigTable = $installer->getTable('udprodupload_import_config');
        $table = $connection->newTable($importConfigTable)
            ->addColumn('import_id', Table::TYPE_INTEGER, 10, [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary'  => true
            ])
            ->addColumn('vendor_id', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('title', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('profile_type', Table::TYPE_TEXT, 20, ['nullable' => true])
            ->addColumn('profile_status', Table::TYPE_TEXT, 20, ['nullable' => true])
            ->addColumn('media_type', Table::TYPE_TEXT, 20, ['nullable' => true])
            ->addColumn('run_status', Table::TYPE_TEXT, 20, ['nullable' => true])
            ->addColumn('invoke_status', Table::TYPE_TEXT, 20, ['nullable' => true])
            ->addColumn('data_type', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('base_dir', Table::TYPE_TEXT, self::TEXT_SIZE, ['nullable' => true])
            ->addColumn('filename', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('store_id', Table::TYPE_SMALLINT, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_found', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_processed', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_success', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_nochange', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_empty', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_depends', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('rows_errors', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('num_errors', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('num_warnings', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('scheduled_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('started_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('snapshot_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('paused_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('stopped_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('finished_at', Table::TYPE_DATETIME, null, ['nullable' => true])
            ->addColumn('time_elapsed', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('last_user_id', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('columns_json', Table::TYPE_TEXT, self::TEXT_SIZE, ['nullable' => true])
            ->addColumn('options_json', Table::TYPE_TEXT, self::TEXT_SIZE, ['nullable' => true])
            ->addColumn('conditions_json', Table::TYPE_TEXT, self::TEXT_SIZE, ['nullable' => true])
            ->addColumn('current_activity', Table::TYPE_TEXT, 100, ['nullable' => true])
            ->addColumn('profile_state_json', Table::TYPE_TEXT, self::TEXT_SIZE, ['nullable' => true])
            ->addColumn('memory_usage', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->addColumn('memory_peak_usage', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => true])
            ->setComment('Bulk Upload Import Config Table')
            ->setOption('type', 'InnoDB')
            ->setOption('charset', 'utf8');
        $connection->createTable($table);

        $installer->endSetup();
    }
}
