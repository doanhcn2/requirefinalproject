<?php

namespace Unirgy\DropshipProductBulkUpload\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\Dropship\Helper\Data as HelperData;

class CfgVendorProductStatus
    extends VendorProductStatus
{
    protected $actions = array(
        'cfg_simple_added',
        'cfg_simple_removed',
    );

    public function execute(Observer $observer)
    {
        $vars             = $observer->getData('vars');
        $unpublishActions = $this->getUnpublishActions();

        $all            = in_array('all', $unpublishActions);
        $none           = !in_array('none', $unpublishActions);
        $emptyActions   = empty($unpublishActions);
        $updateProducts = array();

        $changes = $vars['cfg_prod_changes'];

        if($changes){
            foreach ($this->actions as $action) {
                $k = $action === 'cfg_simple_added'? 'added': 'removed';
                if ($none && isset($changes[$k]) &&
                    ($emptyActions || in_array($action, $unpublishActions, true) || $all)
                ) {
                    foreach ($changes[$k] as $sku => $pId) {
                        $updateProducts[] = $pId;
                    }
                }
            } // end foreach actions
        } // end if changes

        if ($updateProducts) {
            $this->updateProductsStatus($updateProducts);
        }
    }
}
