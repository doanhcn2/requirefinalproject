<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipProductBulkUpload
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipProductBulkUpload\Observer;
use Unirgy\Dropship\Helper\Data as HelperData;

class AbstractObserver
{
    /**
     * @var HelperData
     */
    protected $_hlp;

    /**
     * ObserverSystemAttributes constructor.
     * @param HelperData $udropshipHelper
     */
    public function __construct(
        HelperData $udropshipHelper
    )
    {
        $this->_hlp = $udropshipHelper;
    }
}