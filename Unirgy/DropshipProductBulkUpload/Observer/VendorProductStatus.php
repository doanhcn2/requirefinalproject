<?php

namespace Unirgy\DropshipProductBulkUpload\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\Dropship\Helper\Data as HelperData;

class VendorProductStatus extends AbstractObserver implements ObserverInterface
{
    protected $actions = array(
        'none',
        'all',
        'new_product',
        'image_added',
        'image_removed',
        'cfg_simple_added',
        'cfg_simple_removed',
        'attribute_changed',
        'stock_changed',
    );

    public function execute(Observer $observer)
    {
        $vars = $observer->getData('vars');
        $newData = $vars['new_data'];
        $unpublishActions = $this->getUnpublishActions();

        $all = in_array('all', $unpublishActions);
        $none = !in_array('none', $unpublishActions);
        $emptyActions = empty($unpublishActions);
        $updateProducts = array();
        foreach ($newData as $sku => $product) {
            if($vars['valid'][$sku] === false) {
                // product not valid and not saved
                continue;
            }

            $status = isset($product['status'])? $product['status']: 0;
            foreach ($this->actions as $action) {
                if($action === 'new_product'){
                    // check if product new
                    if(!array_key_exists($sku, $vars['insert_entity'])){
                        continue;
                    }
                }

                if($action === 'image_added'){
                    // check if new images added
                    $imageAdded = false;
                    foreach (array('image', 'small_image', 'thumbnail') as $f) {
                        if(isset($vars['change_attr'][$sku], $vars['change_attr'][$sku][$f])){
                            $imageAdded = true;
                        }
                    }
                    if(!$imageAdded){
                        continue;
                    }
                }

                if($action === 'image_removed'){
                    if(!isset($vars['media_changes']['deleted'], $vars['media_changes']['deleted'][$sku])){
                        continue;
                    }
                }

                if ($action === 'attribute_changed') {
                    if(!isset($vars['change_attr'][$sku])){
                        continue;
                    }
                }

                if ($action === 'stock_changed') {
                    if (!isset($vars['change_stock'][$sku])) {
                        continue;
                    }
                }

                if (($emptyActions || in_array($action, $unpublishActions, true) || $all)
                    && ($none || ($status === Unirgy_DropshipVendorProduct_Model_ProductStatus::STATUS_FIX && in_array($action,
                                array('attribute_changed', 'image_added'), true)))
                ) {
                    $updateProducts[] = $vars['skus'][$sku];
                }
            }
        }

        if($updateProducts){
            $this->updateProductsStatus($updateProducts);
        }
    }

    /**
     * @return \Unirgy\Dropship\Model\Vendor
     */
    protected function getCurrentVendor()
    {
        return $this->_hlp->session()->getVendor();
    }

    /**
     * @return array|mixed
     */
    protected function getUnpublishActions()
    {
        $v                = $this->getCurrentVendor();
        $unpublishActions = $this->_hlp->getScopeConfig('udprod/general/unpublish_actions');
        if ($v->getData('is_custom_udprod_unpublish_actions')) {
            $unpublishActions = $v->getData('udprod_unpublish_actions');
        }
        if (!is_array($unpublishActions)) {
            $unpublishActions = array_filter(explode(',', $unpublishActions));
        }

        return $unpublishActions;
    }

    protected function updateProductsStatus(array $updateProducts)
    {
        $unpublishAttrs = array(
            'status'                         => \Unirgy\DropshipVendorProduct\Model\ProductStatus::STATUS_PENDING,
            'udprod_fix_notify'              => 0,
            'udprod_approved_notify'         => 0,
            'udprod_fix_notified'            => 1,
            'udprod_pending_notified'        => 0,
            'udprod_approved_notified'       => 1,
            'udprod_fix_admin_notified'      => 1,
            'udprod_pending_admin_notified'  => 0,
            'udprod_approved_admin_notified' => 1,
            'udprod_pending_notify'          => 1,
        );
        /** @var \Magento\Catalog\Model\Product\Action $resource */
        $resource = $this->_hlp->getObj('\Magento\Catalog\Model\Product\Action');
        $resource->updateAttributes(
            $updateProducts,
            $unpublishAttrs,
            0
        );
    }
}
