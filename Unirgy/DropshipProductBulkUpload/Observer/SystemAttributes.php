<?php

namespace Unirgy\DropshipProductBulkUpload\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\Dropship\Helper\Data as HelperData;

class SystemAttributes extends AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        if ($this->_hlp->isUdmultiActive()) {
            $attributes = $observer->getData('attributes');

            //$observer->setData('attributes', array_merge($attributes, $this->_prepareUdmultiFields()));
            $attributes->addData($this->_prepareUdmultiFields());
        }
    }

    protected function _prepareOptions(&$options)
    {
        foreach ($options as $_k => &$_v) {
            $_v = strtolower(trim((string)$_v));
        }
        return $this;
    }

    protected function _prepareUdmultiFields()
    {

        /** @var HelperData $dropshipHelper */
        $dropshipHelper = $this->_hlp;
        $isUdmultiPriceAvailable = $dropshipHelper->isUdmultiPriceAvailable();
        $statusOptions = $this->_hlp->getObj('\Unirgy\DropshipMulti\Model\Source')
            ->setPath('vendor_product_status')
            ->toOptionHash();
        $this->_prepareOptions($statusOptions);
        $attributesByCode['udmulti.status'] = [
            'backend_type' => 'int',
            'frontend_input' => 'select',
            'frontend_label' => __('Vendor Status'),
            'options' => $statusOptions,
            'options_bytext' => array_flip($statusOptions),
            'is_required' => false,
        ];
        if ($isUdmultiPriceAvailable) {
            $stateOptions = $this->_hlp->getObj('\Unirgy\DropshipMultiPrice\Model\Source')
                ->setPath('vendor_product_state')
                ->toOptionHash();
            $this->_prepareOptions($stateOptions);
            $attributesByCode['udmulti.state'] = [
                'backend_type' => 'int',
                'frontend_input' => 'select',
                'frontend_label' => __('Vendor State (Condition)'),
                'options' => $stateOptions,
                'options_bytext' => array_flip($stateOptions),
                'is_required' => false,
            ];
        }
        $attributesByCode['udmulti.stock_qty'] = [
            'backend_type' => 'decimal',
            'frontend_label' => __('Vendor Stock Qty'),
            'is_required' => false,
        ];
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.state_descr'] = [
                'backend_type' => 'varchar',
                'frontend_label' => __('Vendor State description'),
                'is_required' => false,
            ];
        }
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.vendor_title'] = [
                'backend_type' => 'varchar',
                'frontend_label' => __('Vendor Title'),
                'is_required' => false,
            ];
        }
        $attributesByCode['udmulti.vendor_cost'] = [
            'backend_type' => 'decimal',
            'frontend_label' => __('Vendor Cost'),
            'is_required' => false,
        ];
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.vendor_price'] = [
                'backend_type' => 'decimal',
                'frontend_label' => __('Vendor Price'),
                'is_required' => false,
            ];
        }
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.special_price'] = [
                'backend_type' => 'decimal',
                'frontend_label' => __('Vendor Special Price'),
                'is_required' => false,
            ];
        }
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.special_from_date'] = [
                'backend_type' => 'varchar',
                'frontend_label' => __('Vendor Special From Date'),
                'is_required' => false,
            ];
        }
        if ($isUdmultiPriceAvailable) {
            $attributesByCode['udmulti.special_to_date'] = [
                'backend_type' => 'varchar',
                'frontend_label' => __('Vendor Special To Date'),
                'is_required' => false,
            ];
        }
        $attributesByCode['udmulti.vendor_sku'] = [
            'backend_type' => 'varchar',
            'frontend_label' => __('Vendor Sku'),
            'is_required' => false,
        ];
        $freeshippingOptions = $this->_hlp->src()->setPath('yesno')->toOptionHash();
        $this->_prepareOptions($freeshippingOptions);
        $attributesByCode['udmulti.freeshipping'] = [
            'backend_type' => 'int',
            'frontend_input' => 'select',
            'frontend_label' => __('Vendor Is Free Shipping'),
            'options' => $freeshippingOptions,
            'options_bytext' => array_flip($freeshippingOptions),
            'is_required' => false,
        ];
        $attributesByCode['udmulti.shipping_price'] = [
            'backend_type' => 'decimal',
            'frontend_label' => __('Vendor Shipping Price'),
            'is_required' => false,
        ];
        $backordersOptions = $this->_hlp->getObj('\Unirgy\DropshipMulti\Model\Source')
            ->setPath('backorders')->toOptionHash();
        $this->_prepareOptions($backordersOptions);
        $attributesByCode['udmulti.backorders'] = [
            'backend_type' => 'int',
            'frontend_input' => 'select',
            'frontend_label' => __('Vendor Backorders'),
            'options' => $backordersOptions,
            'options_bytext' => array_flip($backordersOptions),
            'is_required' => false,
        ];

        return $attributesByCode;
    }
}
