<?php

namespace Unirgy\DropshipProductBulkUpload\Helper;

use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\View\LayoutInterface;

class Udprod extends AbstractHelper
{
    /**
     * @var LayoutInterface
     */
    protected $_layout;

    /**
     * @var Collection
     */
    protected $_setCollection;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var DirectoryList
     */
    protected $_directoryList;

    protected $_hlp;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        Context $context,
        LayoutInterface $layout,
        Collection $setCollection,
        ProductFactory $productFactory,
        DirectoryList $directoryList
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->_layout = $layout;
        $this->_setCollection = $setCollection;
        $this->_productFactory = $productFactory;
        $this->_directoryList = $directoryList;

        parent::__construct($context);
    }

    protected $_columnsBlockBySetId = [];

    protected function _initColumnsBlock($setId)
    {
        if (!isset($this->_columnsBlockBySetId[$setId])) {
            $this->_columnsBlockBySetId[$setId] = $this->_layout->createBlock(
                'Unirgy\DropshipProductBulkUpload\Block\Vendor\Columns',
                'udprodupload.columns.' . $setId,
                ['skip_add_head_js' => 1]);
            $this->_columnsBlockBySetId[$setId]->initSetId($setId);
            $this->_columnsBlockBySetId[$setId]->getColumns();
        }
        return $this;
    }

    protected function _getSession()
    {
        return $this->_hlp->session();
    }

    /**
     * @param $setId
     * @return \Unirgy\DropshipProductBulkUpload\Block\Vendor\Columns
     */
    public function getColumnsBlock($setId)
    {
        $this->_initColumnsBlock($setId);
        return $this->_columnsBlockBySetId[$setId];
    }

    public function getColumnsIds($setId)
    {
        $columns = array_keys($this->getColumns($setId));
        $_columns = $this->_getSession()->getData('udprodupload_columns_'.$setId);
        if ($_columns) {
            $columns = $_columns;
        }
        return $columns;
    }

    /**
     * @return \Unirgy\DropshipProductBulkUpload\Helper\Upload
     */
    protected function _getUploadHelper()
    {
        return $this->_hlp->getObj('Unirgy\DropshipProductBulkUpload\Helper\Upload');
    }

    public function getImportOptions($setId)
    {
        $uploadHelper      = $this->_getUploadHelper();
        $defaultProductOptions = $uploadHelper->getDefaultProductOptions();
        $importOptions = $defaultProductOptions['options']['import'];
        if (!is_array($importOptions)) {
            $importOptions = [];
        }
        $_importOptions = $this->_getSession()->getData('udprodupload_options_'.$setId);
        if ($_importOptions) {
            if (!is_array($_importOptions)) {
                $_importOptions = [];
            }
            foreach ($_importOptions as $optKey=>$optVal) {
                if (array_key_exists($optKey, $importOptions)) {
                    $importOptions[$optKey] = $optVal;
                }
            }
        }
        return $importOptions;
    }

    public function getColumns($setId)
    {
        return $this->getColumnsBlock($setId)->getColumns();
    }

    public function getCfgColumnsIds($setId)
    {
        $columns = array_keys($this->getCfgColumns($setId));
        $_columns = $this->_getSession()->getData('udprodupload_cfg_columns_' . $setId);
        if ($_columns) {
            $columns = $_columns;
        }
        return $columns;
    }


    public function getCfgColumns($setId)
    {
        return $this->getColumnsBlock($setId)->getCfgColumns();
    }

    public function getDefaultColumnsOrder($setId)
    {
        return $this->getColumnsBlock($setId)->getDefaultColumnsOrder();
    }

    public function getDefaultCfgColumnsOrder($setId)
    {
        return $this->getColumnsBlock($setId)->getDefaultCfgColumnsOrder();
    }

    protected $_dateColumns = ['udmulti.special_from_date', 'udmulti.special_to_date', ''];
    protected $_numericColumns = ['stock.qty', 'stock.min_qty', 'stock.min_sale_qty', 'stock.max_sale_qty', 'udmulti.stock_qty', 'udmulti.vendor_cost', 'udmulti.vendor_price', 'udmulti.special_price', 'udmulti.shipping_price'];

    public function isNumericColumn($column)
    {
        $isNumeric = false;
        if (isset($column['entity_attribute'])) {
            $isNumeric = in_array($column['entity_attribute']->getBackendType(), ['int', 'decimal']);
        } elseif (in_array($column['id'], $this->_numericColumns)) {
            $isNumeric = true;
        }
        return $isNumeric;
    }

    public function isDateColumn($column)
    {
        $isNumeric = false;
        if (isset($column['entity_attribute'])) {
            $isNumeric = in_array($column['entity_attribute']->getBackendType(), ['date', 'datetime']);
        } elseif (in_array($column['id'], $this->_dateColumns)) {
            $isNumeric = true;
        }
        return $isNumeric;
    }

    protected $_attrSets;

    public function getAttributeSetName($setId)
    {
        if (null === $this->_attrSets) {
            $this->_attrSets = $this->_setCollection
                ->setEntityTypeFilter($this->_productFactory->create()->getResource()->getEntityType()->getId())
                ->load()
                ->toOptionHash();
        }
        return $this->_attrSets[$setId];
    }

    /**
     * @return \Unirgy\Dropship\Model\Vendor
     */
    public function getVendor()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getVendorId()
    {
        return $this->getVendor()->getId();
    }

    public function getBasePath()
    {
        return $this->_directoryList->getPath('var')
            . DIRECTORY_SEPARATOR . 'udprodupload'
            . DIRECTORY_SEPARATOR . 'import'
            . DIRECTORY_SEPARATOR . $this->getVendorId();
    }

    public function getBaseTmpPath()
    {
        return $this->_directoryList->getPath('var')
            . DIRECTORY_SEPARATOR . 'udprodupload'
            . DIRECTORY_SEPARATOR . 'upload'
            . DIRECTORY_SEPARATOR . $this->getVendorId();
    }

    public function moveFileFromTmp($baseTmpPath, $basePath, $file)
    {
        $ioObject = new File();
        $ioObject->setAllowCreateFolders(true);
        $destDirectory = dirname($this->getFilePath($basePath, $file));
        try {
            $ioObject->open(['path' => $destDirectory]);
        } catch (\Exception $e) {
            $ioObject->mkdir($destDirectory, 0777, true);
            $ioObject->open(['path' => $destDirectory]);
        }

        if (strrpos($file, '.tmp') == strlen($file) - 4) {
            $file = substr($file, 0, strlen($file) - 4);
        }

        $destFile = dirname($file) . $ioObject->dirsep()
            . Uploader::getNewFileName($this->getFilePath($basePath, $file));

        $ioObject->mv(
            $this->getFilePath($baseTmpPath, $file),
            $this->getFilePath($basePath, $destFile)
        );
        return str_replace($ioObject->dirsep(), '/', $destFile);
    }

    public function getFilePath($path, $file)
    {
        $file = $this->_prepareFileForPath($file);

        if (substr($file, 0, 1) == DIRECTORY_SEPARATOR) {
            return $path . DIRECTORY_SEPARATOR . substr($file, 1);
        }

        return $path . DIRECTORY_SEPARATOR . $file;
    }

    protected function _prepareFileForPath($file)
    {
        return str_replace('/', DIRECTORY_SEPARATOR, $file);
    }
}
