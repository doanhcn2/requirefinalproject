<?php

namespace Unirgy\DropshipProductBulkUpload\Helper;

use Magento\Backend\Model\Auth;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Model\Product\Url as ProductUrl;
use Magento\CatalogInventory\Model\Configuration;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem\Directory\WriteFactory;
use Magento\Framework\Filesystem\DriverPool;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Model\Session;
use Unirgy\DropshipProductBulkUpload\Exception as UdpuException;
use Unirgy\DropshipProductBulkUpload\Model\ImportConfig as ImportConfigModel;
use Unirgy\DropshipProductBulkUpload\Model\Io\Csv;
use Unirgy\DropshipProductBulkUpload\Model\Io\IoInterface;
use Unirgy\DropshipProductBulkUpload\Model\Logger\Csv as CsvLogger;
use Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig;
use Unirgy\DropshipProductBulkUpload\Exception\Row;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;

/**
 * Created by pp
 *
 * @method $this setRowsFound(int $rows)
 * @method $this setStartedAt(string $date)
 * @method $this setSnapshotAt(string $date)
 * @method $this setMemoryUsage(int $usage)
 * @method $this setMemoryPeakUsage(int $usage)
 */
class Data extends AbstractHelper
{

    const NUM_WARNINGS = 'num_warnings';

    const NUM_ERRORS = 'num_errors';

    const ROWS_EMPTY = 'rows_empty';

    const ROWS_SUCCESS = 'rows_success';

    const ROWS_NOCHANGE = 'rows_nochange';

    const ROWS_DEPENDS = 'rows_depends';

    const ROWS_PROCESSED = 'rows_processed';

    const ROWS_ERRORS = 'rows_errors';
    const TABLE_CATALOG_CATEGORY_ENTITY                           = 'catalog_category_entity';
    const TABLE_CATALOG_CATEGORY_PRODUCT                          = 'catalog_category_product';
    const TABLE_CATALOG_EAV_ATTRIBUTE                             = 'catalog_eav_attribute';
    const TABLE_CATALOG_PRODUCT_BUNDLE_OPTION                     = 'catalog_product_bundle_option';
    const TABLE_CATALOG_PRODUCT_BUNDLE_OPTION_VALUE               = 'catalog_product_bundle_option_value';
    const TABLE_CATALOG_PRODUCT_BUNDLE_SELECTION                  = 'catalog_product_bundle_selection';
    const TABLE_CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE            = 'catalog_product_bundle_selection_price';
    const TABLE_CATALOG_PRODUCT_ENTITY                            = 'catalog_product_entity';
    const TABLE_CATALOG_PRODUCT_ENTITY_GROUP_PRICE                = 'catalog_product_entity_group_price';
    const TABLE_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY              = 'catalog_product_entity_media_gallery';
    const TABLE_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE        = 'catalog_product_entity_media_gallery_value';
    const TABLE_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_ENTITY = 'catalog_product_entity_media_gallery_value_to_entity';
    const TABLE_CATALOG_PRODUCT_ENTITY_MEDIA_GALLERY_VALUE_VIDEO  = 'catalog_product_entity_media_gallery_value_video';
    const TABLE_CATALOG_PRODUCT_ENTITY_TIER_PRICE                 = 'catalog_product_entity_tier_price';
    const TABLE_CATALOG_PRODUCT_ENTITY_VARCHAR                    = 'catalog_product_entity_varchar';
    const TABLE_CATALOG_PRODUCT_LINK                              = 'catalog_product_link';
    const TABLE_CATALOG_PRODUCT_LINK_ATTRIBUTE                    = 'catalog_product_link_attribute';
    const TABLE_CATALOG_PRODUCT_LINK_TYPE                         = 'catalog_product_link_type';
    const TABLE_CATALOG_PRODUCT_OPTION                            = 'catalog_product_option';
    const TABLE_CATALOG_PRODUCT_OPTION_PRICE                      = 'catalog_product_option_price';
    const TABLE_CATALOG_PRODUCT_OPTION_TITLE                      = 'catalog_product_option_title';
    const TABLE_CATALOG_PRODUCT_OPTION_TYPE_PRICE                 = 'catalog_product_option_type_price';
    const TABLE_CATALOG_PRODUCT_OPTION_TYPE_TITLE                 = 'catalog_product_option_type_title';
    const TABLE_CATALOG_PRODUCT_OPTION_TYPE_VALUE                 = 'catalog_product_option_type_value';
    const TABLE_CATALOG_PRODUCT_RELATION                          = 'catalog_product_relation';
    const TABLE_CATALOG_PRODUCT_SUPER_ATTRIBUTE                   = 'catalog_product_super_attribute';
    const TABLE_CATALOG_PRODUCT_SUPER_ATTRIBUTE_LABEL             = 'catalog_product_super_attribute_label';
    const TABLE_CATALOG_PRODUCT_SUPER_ATTRIBUTE_PRICING           = 'catalog_product_super_attribute_pricing';
    const TABLE_CATALOG_PRODUCT_SUPER_LINK                        = 'catalog_product_super_link';
    const TABLE_CATALOG_PRODUCT_WEBSITE                           = 'catalog_product_website';
    const TABLE_CATALOGINVENTORY_STOCK_ITEM                       = 'cataloginventory_stock_item';
    const TABLE_CATEGORY_SEQUENCE                                 = 'sequence_catalog_category';
    const TABLE_CUSTOMER_GROUP                                    = 'customer_group';
    const TABLE_DOWNLOADABLE_LINK                                 = 'downloadable_link';
    const TABLE_DOWNLOADABLE_LINK_PRICE                           = 'downloadable_link_price';
    const TABLE_DOWNLOADABLE_LINK_TITLE                           = 'downloadable_link_title';
    const TABLE_DOWNLOADABLE_SAMPLE                               = 'downloadable_sample';
    const TABLE_DOWNLOADABLE_SAMPLE_TITLE                         = 'downloadable_sample_title';
    const TABLE_EAV_ATTRIBUTE                                     = 'eav_attribute';
    const TABLE_EAV_ATTRIBUTE_GROUP                               = 'eav_attribute_group';
    const TABLE_EAV_ATTRIBUTE_LABEL                               = 'eav_attribute_label';
    const TABLE_EAV_ATTRIBUTE_OPTION                              = 'eav_attribute_option';
    const TABLE_EAV_ATTRIBUTE_OPTION_SWATCH                       = 'eav_attribute_option_swatch';
    const TABLE_EAV_ATTRIBUTE_OPTION_VALUE                        = 'eav_attribute_option_value';
    const TABLE_EAV_ATTRIBUTE_SET                                 = 'eav_attribute_set';
    const TABLE_EAV_ENTITY_ATTRIBUTE                              = 'eav_entity_attribute';
    const TABLE_EAV_ENTITY_TYPE                                   = 'eav_entity_type';
    const TABLE_PRODUCT_SEQUENCE                                  = 'sequence_product';
    const TABLE_STORE                                             = 'store';
    const TABLE_STORE_GROUP                                       = 'store_group';
    const TABLE_STORE_WEBSITE                                     = 'store_website';

    protected static $metadata;
    /**
     * @var ImportConfigModel
     */
    protected $_config;
    protected $_lastSync;
    protected $_saveFields = [
        'snapshot_at',
        'rows_found',
        'rows_processed',
        'rows_success',
        'rows_nochange',
        'rows_empty',
        'rows_depends',
        'rows_errors',
        'num_errors',
        'num_warnings',
        'memory_usage',
        'memory_peak_usage',
        'reindexing',
        'reindex_processed',
        'reindex_total'
    ];
    protected $_loadFields = ['run_status'];
    /**
     * @var \Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig
     */
    protected $_resource;
    protected $_resourceName       = ImportConfig::class;// todo change this to actual resource name
    protected $_defaultIoModel     = Csv::class;
    protected $_defaultLoggerModel = CsvLogger::class;
    protected $_hasMageFeature;
    protected $_storeIds;
    protected $_ee_gws_filter;
    protected $_rowTypes           = [
        'CPSA'  => [
            'columns' => [
                'sku'            => ['col' => 1, 'key' => 1],
                'attribute_code' => ['col' => 2, 'key' => 1],
                'position'       => ['col' => 3],
                'label'          => ['col' => 4],
            ]
        ],
        'CPSAL' => [
            'columns' => [
                'sku'            => ['col' => 1, 'key' => 1],
                'attribute_code' => ['col' => 2, 'key' => 1],
                'store'          => ['col' => 3],
                'label'          => ['col' => 4],
            ]
        ],
        'CPI' => [
            'columns' => [
                'sku' => ['col' => 1, 'key' => 1],
                'image_url' => ['col' => 2, 'key' => 1],
                'label' => ['col' => 3],
                'position' => ['col' => 4],
                'disabled' => ['col' => 5],
            ]
        ],
        'CPSI'  => [
            'columns' => [
                'sku'        => ['col' => 1, 'key' => 1],
                'linked_sku' => ['col' => 2, 'key' => 1],
            ]
        ],
    ];
    protected $_moduleActive       = [];

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var DirectoryList
     */
    protected $_directoryList;

    /**
     * @var WriteFactory
     */
    protected $_directoryWrite;

    /**
     * @var ProductUrl
     */
    protected $_productUrl;

    /**
     * @var Auth
     */
    protected $_backendAuth;

    /**
     * @var ResolverInterface
     */
    protected $_localeResolver;

    /**
     * @var TimezoneInterface
     */
    protected $_localeDate;
    protected $_defaultDatetimeFormat;
    protected $_isoToPhpFormatConvertRegex;
    protected $_isoToPhpFormatConvert;
    protected $_phpToIsoFormatConvert = array(
        'd' => 'dd',
        'D' => 'EE',
        'j' => 'd',
        'l' => 'EEEE',
        'N' => 'e',
        'S' => 'SS',
        'w' => 'eee',
        'z' => 'D',
        'W' => 'ww',
        'F' => 'MMMM',
        'm' => 'MM',
        'M' => 'MMM',
        'n' => 'M',
        't' => 'ddd',
        'L' => 'l',
        'o' => 'YYYY',
        'Y' => 'yyyy',
        'y' => 'yy',
        'a' => 'a',
        'A' => 'a',
        'B' => 'B',
        'g' => 'h',
        'G' => 'H',
        'h' => 'hh',
        'H' => 'HH',
        'i' => 'mm',
        's' => 'ss',
        'e' => 'zzzz',
        'I' => 'I',
        'O' => 'Z',
        'P' => 'ZZZZ',
        'T' => 'z',
        'Z' => 'X',
        'c' => 'yyyy-MM-ddTHH:mm:ssZZZZ',
        'r' => 'r',
        'U' => 'U'
    );

    /**
     * @var ResourceConnection
     */
    protected $_resources;
    /**
     * @var Config
     */
    protected $_entityTypeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var CatalogHelper
     */
    protected $_catalogHelper;
    /**
     * @var \Magento\Catalog\Model\Product\Type
     */
    protected $_productType;

    protected $_productFlatIndexState;

    protected $_productFlatIndexHelper;
    protected $_productsToUpdate = [];
    protected $_downloadRemoteImagesBatch;
    protected $_remoteImagesBatch = [];
    protected $_remoteImagesCache;
    protected $_mediaProductsProcessed;
    protected $_currentMediaSku;
    protected $writeInstances;
    protected $_mediaConfig;
    protected $_fullTextIndexer;
    /**
     * @var Url
     */
    protected $_catalogUrlHelper;
    protected $_indexerRegistry;
    protected $_categoryIdsToUpdate;
    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    protected $formatInterface;
    protected $_profileLocale;

    /**
     * @var Session
     */
    private $_session;

    /**
     * @var \Magento\CatalogInventory\Api\StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\Status
     */
    private $modelStockStatus;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    private $catalogProduct;

    /**
     * @var \Magento\Catalog\Model\Product\Action
     */
    private $modelProductAction;

    /**
     * @var \Magento\Catalog\Model\Product\Image
     */
    private $productImage;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;
    private $_reindexPids;

    protected $vitalForGenerationFields = [
        'sku',
        'url_key',
        'url_path',
        'name',
        'store_id',
        'visibility'
    ];
    protected $_urlPersist;
    protected $_generator;
    protected $_catalogProductFactory;

    public function __construct(
        \Magento\Backend\Model\Auth $backendAuth,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\CatalogInventory\Model\Stock\Status $modelStockStatus,
        \Magento\CatalogSearch\Model\Indexer\Fulltext\Action\Full $fullIndexer,
        \Magento\Catalog\Helper\Data $catalogHelper,
        \Magento\Catalog\Helper\Product\Flat\Indexer $flatIndexer,
        \Magento\Catalog\Model\Indexer\Product\Flat\State $flatIndexerState,
        \Magento\Catalog\Model\Product $catalogProduct,
        \Magento\Catalog\Model\Product\Action $modelProductAction,
        \Magento\Catalog\Model\Product\Image $productImage,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Catalog\Model\Product\Type $productType,
        \Magento\Catalog\Model\Product\Url $productUrl,
        \Magento\Eav\Model\Config $entityTypeConfig,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Directory\WriteFactory $writeFactory,
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Framework\Locale\FormatInterface $formatInterface,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\DropshipProductBulkUpload\Helper\Url $urlHelper,
        \Unirgy\Dropship\Model\Session $session,
        \Magento\Framework\App\Helper\Context $context,
        ProductFactory $catalogFactory,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist
    )
    {
        parent::__construct($context);
        $this->objectManager           = $objectManager;
        $this->_productUrl             = $productUrl;
        $this->_backendAuth            = $backendAuth;
        $this->_localeResolver         = $localeResolver;
        $this->_localeDate             = $localeDate;
        $this->_resources              = $resource;
        $this->_entityTypeConfig       = $entityTypeConfig;
        $this->_storeManager           = $storeManager;
        $this->_catalogHelper          = $catalogHelper;
        $this->_productType            = $productType;
        $this->_productFlatIndexState  = $flatIndexerState;
        $this->_productFlatIndexHelper = $flatIndexer;
        $this->_mediaConfig            = $mediaConfig;
        $this->_fullTextIndexer        = $fullIndexer;
        $this->_catalogUrlHelper       = $urlHelper;
        $this->_indexerRegistry = $indexerRegistry;
        $this->formatInterface = $formatInterface;
        $this->_session = $session;
        $this->stockConfiguration = $stockConfiguration;
        $this->modelStockStatus = $modelStockStatus;
        $this->catalogProduct = $catalogProduct;
        $this->modelProductAction = $modelProductAction;
        $this->productImage = $productImage;
        $this->_directoryList = $directoryList;
        $this->filesystem = $filesystem;
        $this->_directoryWrite = $writeFactory;
        $this->_catalogProductFactory = $catalogFactory;
        $this->_urlPersist = $urlPersist;
        $this->_generator = $productUrlRewriteGenerator;
    }

    public function setConfig($config)
    {
        $this->_config = $config;
        return $this;
    }

    /**
     * @param ImportConfigModel $productConfig
     * @param ImportConfigModel $productCfgConfig
     * @throws \Exception
     */
    public function import($productConfig, $productCfgConfig)
    {
        /** @var \Unirgy\DropshipProductBulkUpload\Helper\ProtectedCode $productImportHelper */
        $productImportHelper = $this->objectManager->get(ProtectedCode::class);
        $this->_config = $productConfig;
        $productImportHelper->setHelper($this);

        session_write_close();
        ignore_user_abort(true);
        set_time_limit(0);
        ob_implicit_flush();

        $this->getLogger()->start('w');

        $this->lock();
        $this->start($this->_config)->save();
        try {
            $productImportHelper->import();
        } catch (\Exception $e) {
            $this->stop($this->_config)->save();
            $this->unlock();
            throw $e;
        }
        $this->_config->setData('reindex_total', count($this->getAllProductIds()));
        $this->finish($this->_config)->save();
        $this->getLogger()->stop();
        $this->unlock();

        $this->_config = $productCfgConfig;
        $this->getLogger()->start('a');
        $this->lock();
        $this->start($this->_config)->save();
        try {
            $productImportHelper->importOptions();
        } catch (\Exception $e) {
            $this->stop($this->_config)->save();
            $this->unlock();
            throw $e;
        }
        $productConfig->setData('reindexing', 1);
        $productConfig->setData('reindex_total', count($this->getAllProductIds()));
        $productConfig->save();
        $this->finish($this->_config)->save();
        $this->_config = $productConfig;
        $this->_config->setData('reindexing', 1);
        $this->_config->setData('reindex_total', count($this->getAllProductIds()));
        $productConfig->save();
        $this->activity('Reindexing products');
        $reindexPidsChunks = array_chunk($this->getAllProductIds(), 100);
        foreach ($reindexPidsChunks as $_chunk) {
            $this->runIndexers($_chunk);
            $this->addValue('reindex_processed', count($_chunk));
        }
        $this->_config->setData('reindexing', 0);
        try {
            $this->updateProductsUrlRewrites();
        } catch (\Magento\Framework\Exception\AlreadyExistsException $e) {
            $this->getLogger()->error($e->getLogMessage());
        }
        try {
            $this->_imageCacheHelper()->flushProductsImageCache($this);
        } catch (\Magento\Framework\Exception\AlreadyExistsException $e) {
            $this->getLogger()->error($e->getLogMessage());
        }
        $this->activity('End reindexing products');
        $this->unlock();
        $this->finish($this->_config)->save();
        $this->getLogger()->stop();
    }

    public function loggerStartProfile()
    {
        $this->getLogger()->startProfile();

        return $this;
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function start($import)
    {
        if ($import->getProfileStatus() !== 'enabled') {
            return $import;
        }

        if (in_array($import->getRunStatus(), ['running', 'paused'])) {
            return $import;
        }

        if ($import->getRunStatus() !== 'pending') {
            $this->reset($import);
        }

        $import->setRunStatus('running');
        $import->setData('reindexing', 0);

        return $import;
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function pause($import)
    {
        if ($import->getRunStatus() !== 'running') {
            return $import;
        }

        $this->unlock();

        $import->setRunStatus('paused');
        $this->getLogger()->pauseProfile();

        $import->setPausedAt(self::now());

        return $import;
    }

    static public function now($dayOnly = false)
    {
        return date($dayOnly ? 'Y-m-d' : 'Y-m-d H:i:s');
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function resume($import)
    {
        if ($import->getRunStatus() !== 'paused') {
            return $import;
        }

        $import->setRunStatus('pending');
        $this->getLogger()->resumeProfile();

        $import->unsPausedAt();

        return $import;
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function stop($import)
    {
        if (!in_array($import->getRunStatus(), array('pending', 'running', 'paused'))) {
            return $import;
        }

        $this->unlock();

        $import->setRunStatus('stopped');
        $this->getLogger()->stopProfile();

        $import->setStoppedAt(self::now());

        return $import;
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function finish($import)
    {
        if ($import->getRunStatus() !== 'running') {
            return $import;
        }

        $this->unlock();

        $import->setRunStatus('finished');
        $this->getLogger()->finishProfile();

        $import->setFinishedAt(self::now());

        return $import;
    }

    /**
     * @param ImportConfigModel $import
     * @return ImportConfigModel
     */
    public function reset($import)
    {
        foreach ($this->_saveFields as $f) {
            $import->setData($f, 0);
        }

        $import->setStartedAt(null)->setPausedAt(null)->setStoppedAt(null)->setFinishedAt(null);

        return $import;
    }

    public function setData($key, $value)
    {
        $this->_config->setData($key, $value);

        return $this;
    }

    public function activity($activity)
    {
        if ($this->getLogger()->getIo()->isOpen()) {
            $this->getLogger()->log('ACTIVITY', array('', '', $activity));
        }
        $this->setData('current_activity', $activity)->sync(true, array('current_activity'), false);

        return $this;
    }

    /**
     * @return CsvLogger
     */
    public function getLogger()
    {
        if (!$this->_config->hasData('logger')) {
            $this->setLogger($this->_defaultLoggerModel);
        }

        return $this->getData('logger');
    }

    public function setLogger($logger)
    {
        if (is_string($logger)) {
            $logger = $this->objectManager->get($logger);
        }
        $logger->setData('profile', $this);
        $this->setData('logger', $logger);

        return $this;
    }

    public function sync($force = false, $saveFields = null, $loadFields = null)
    {
        if (!$force && $this->_lastSync && $this->_lastSync >= time() - 2) {
            return false;
        }
        if (!($this->getData('title') && $this->getData('profile_type'))) {
            return false;
        }
        $saveFields = null !== $saveFields? $saveFields: $this->_saveFields;
        $loadFields = null !== $loadFields? $loadFields: $this->_loadFields;
        //  todo, 5.Jan17 - may need to implement sync method in resource
        $this->_getResource()->sync($this->_config, $saveFields, $loadFields);

        $this->_lastSync = time();

        return true;
    }

    public function getLogBaseDir()
    {
        return $this->_processDir('{var}/udprodupload/log');
    }

    public function getLogFilename()
    {
        $base = str_replace('-cfg', '', $this->getData('title'));

        return $base . '.log';
    }

    public function getData($path)
    {
        return $this->_config->getData($path);
    }

    /**
     * @return \Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig
     */
    protected function _getResource()
    {
        if (empty($this->_resource)) {
            $this->_resource = $this->_config->getResource();
        }
        return $this->_resource;
    }

    public function ioOpenRead()
    {
        $this->getIo()->open($this->getData('filename'), 'r');;

        return $this;
    }

    public function ioOpenReadOptions()
    {
        $fileName = $this->getData('filename_options') ?: $this->getData('filename');
        $this->getIo()->open($fileName, 'r');

        return $this;
    }

    /**
     * @return Csv
     * @throws UdpuException
     */
    public function getIo()
    {
        if (!$this->_config->hasData('io')) {
            $this->setIo($this->_defaultIoModel);
        }

        return $this->getData('io');
    }

    public function setIo($io)
    {
        if (is_string($io)) {
            $io = $this->objectManager->create($io);
        }

        if (!$io instanceof IoInterface) {
            throw new UdpuException(__('Wrong IO model'));
        }

        $io->setBaseDir($this->getFileBaseDir());
        $io->addData((array) $this->getData('options/csv'));
        $this->setData('io', $io);

        return $this;
    }

    /**
     * @return array|false
     * @throws UdpuException
     */
    public function ioRead()
    {
        return $this->getIo()->read();
    }

    public function ioSeekReset($line = 0)
    {
        $this->getIo()->seek($line);

        return $this;
    }

    public function ioClose()
    {
        $this->getIo()->close();
        //$this->doFileActions('after');
    }

    public function isLocked()
    {
        return file_exists($this->getLockFilename());
    }

    public function getLockFilename()
    {
        if (!$this->_config->hasData('lock_filename')) {
            $dir = $this->_directoryList->getPath('var') . '/udprodupload/lock';
            $this->_directoryWrite->create($dir)->create();
            $filename = $dir . '/profile-' . $this->getId() . '.lck';
            $this->setData('lock_filename', $filename);
        }

        return $this->getData('lock_filename');
    }

    public function getFileBaseDir()
    {
        $dir = $this->getData('base_dir');
        if (!$dir) {
            $dir = $this->getConfig(
                'udprodupload/dirs/import_dir',
                ScopeInterface::SCOPE_STORE,
                $this->getImportStoreId()
            );
        }

        return $this->_processDir($dir);
    }

    public function getFilename()
    {
        return $this->getData('filename');
    }

    public function getProfileType()
    {
        return $this->getData('profile_type');
    }

    public function getImportStoreId()
    {
        return $this->getData('store_id');
    }

    public function _($id, $allowDefault = false)
    {
        $store = $this->_storeManager->getStore($id);
        if (!$store || (!$allowDefault && $store->getId() == 0)) {
            throw new LocalizedException(__('Invalid store'));
        }

        return $store->getId();
    }

    protected function _processDir($dir)
    {
        $dir = str_replace(
            ['{magento}', '{var}', '{media}'],
            [
                $this->_directoryList->getPath(DirectoryList::ROOT),
                $this->_directoryList->getPath(DirectoryList::VAR_DIR),
                $this->_directoryList->getPath(DirectoryList::MEDIA)
            ],
            $dir
        );

        return $dir;
    }

    public function getId()
    {
        $this->_config->getData('id');
    }

    public function addValue($key, $v = 1)
    {
        $this->setData($key, $this->getData($key) + $v);

        return $this;
    }

    public function hasMageFeature($feature)
    {
        if (!isset($this->_hasMageFeature[$feature])) {
            $flag = false;
            switch ($feature) {
                case 'row_id':
                    $flag = $this->isEnterpriseEdition() && $this->compareMageVer('2.1.0');
                    break;
            }
            $this->_hasMageFeature[$feature] = $flag;
        }

        return $this->_hasMageFeature[$feature];
    }

    private function isEnterpriseEdition()
    {
        return strtolower(self::_getMetaData()->getEdition()) === 'enterprise';
    }

    public function compareMageVer($ceVer, $eeVer = null, $op = '>=')
    {
        return $this->isEnterpriseEdition()
            ? version_compare(self::getVersion(), null !== $eeVer? $eeVer: $ceVer, $op)
            : version_compare(self::getVersion(), $ceVer, $op);
    }

    /**
     * @return ProductMetadataInterface|null
     * @throws \RuntimeException
     */
    protected static function _getMetaData()
    {
        if (null === self::$metadata) {
            self::$metadata = self::om()->get('Magento\Framework\App\ProductMetadataInterface');
        }

        return self::$metadata;
    }

    /**
     * Get Magento version
     *
     * @return string
     * @throws \RuntimeException
     */
    public static function getVersion()
    {
        /** @var ProductMetadataInterface $metaData */
        $metaData = self::_getMetaData();

        return $metaData->getVersion();
    }

    /**
     * ObjectManager instance
     *
     * @return ObjectManager
     * @throws \RuntimeException
     */
    public static function om()
    {
        return ObjectManager::getInstance();
    }

    public function formatUrlKey($str)
    {
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $this->_productUrl->formatUrlKey($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }

    public function getImagesBaseDir($autoCreate = false)
    {
        $dir = $this->getData('options/dir/images');
        if (!$dir) {
            $dir = $this->getConfig('urapidflow/dirs/images_dir', ScopeInterface::SCOPE_STORE,
                $this->getImportStoreId());
        }
        $dir = $this->_processDir($dir);
        if (!$dir) {
            $dir = $this->_directoryList->getPath(DirectoryList::MEDIA) . DIRECTORY_SEPARATOR . 'import';
        } elseif ($dir[0] !== '/' && $dir[1] !== ':') {
            $dir = rtrim($this->getFileBaseDir(), '/') . '/' . $dir;
        }
        if ($autoCreate) {
            $this->_directoryWrite->create($dir)->create();
        }

        return $dir;
    }

    public function prepareRowTypeData()
    {
        $rowTypeFields = array();
        foreach ($this->_rowTypes as $rowType => $rowNode) {
            foreach ($rowNode['columns'] as $fieldName => $fieldNode) {
                $rowTypeFields[$rowType][$fieldName] = $fieldNode;
            }
            $rowTypeFields[$rowType]['columns'] = $rowNode['columns'];
        }

        return $rowTypeFields;
    }

    public function getStoreIds()
    {
        if (null === $this->_storeIds) {
            $ids = $this->getData('options/store_ids');
            if (empty($ids)) {
                $this->_storeIds = array();

                return $this->_storeIds;
            }
            if (is_string($ids)) {
                $ids = explode(',', $ids);
            }
            $this->_storeIds = $ids;
            if ($this->hasEeGwsFilter()) {
                $this->_storeIds = $this->filterEeGwsStoreIds($this->_storeIds);
            }
        }

        return $this->_storeIds;
    }

    public function hasEeGwsFilter()
    {
        if (null === $this->_ee_gws_filter) {
            $this->_ee_gws_filter = $this->isModuleActive('Magento_AdminGws')
                && $this->_backendAuth->isLoggedIn()
                && !$this->objectManager->get('Magento\AdminGws\Model\Role')->getIsAll();
//            $this->_ee_gws_filter = false;
        }

        return $this->_ee_gws_filter;
    }

    public function filterEeGwsStoreIds($sIds)
    {
        if ($this->hasEeGwsFilter()) {
            return array_intersect($sIds,
                $this->objectManager->get('Magento\AdminGws\Model\Role')->getStoreIds());
        }

        return $sIds;
    }

    public function isModuleActive($code)
    {
        if (!isset($this->_moduleActive[$code])) {
            $this->_moduleActive[$code] = $this->_moduleManager->isEnabled($code);
        }

        return $this->_moduleActive[$code];
    }

    public function filterEeGwsWebsiteIds($wIds)
    {
        if ($this->hasEeGwsFilter()) {
            return array_intersect($wIds,
                $this->objectManager->get('Magento\AdminGws\Model\Role')->getWebsiteIds());
        }

        return $wIds;
    }

    public function getRowTypes()
    {
        return array_keys($this->_rowTypes);
    }

    public function getDefaultDatetimeFormat()
    {
        if (null === $this->_defaultDatetimeFormat) {
            $this->_localeResolver->emulate($this->getImportStoreId());
            $this->_defaultDatetimeFormat = $this->_localeDate
                ->getDateTimeFormat(\IntlDateFormatter::SHORT);
            $this->_localeResolver->revert();
            $this->_defaultDatetimeFormat = $this->convertIsoToPhpDateFormat($this->_defaultDatetimeFormat);
        }

        return $this->_defaultDatetimeFormat;
    }

    public function convertIsoToPhpDateFormat($isoFormat)
    {
        if (null === $this->_isoToPhpFormatConvertRegex) {
            uasort($this->_phpToIsoFormatConvert, array($this, 'sortByLengthDescCallback'));
            $this->_isoToPhpFormatConvertRegex = sprintf('/%s/', implode('|',
                array_map('preg_quote', $this->_phpToIsoFormatConvert)
            ));
        }

        return preg_replace_callback(
            $this->_isoToPhpFormatConvertRegex,
            [$this, 'regexIsoToPhpDateFormatCallback'],
            $isoFormat
        );
    }

    public function regexIsoToPhpDateFormatCallback($matches)
    {
        if (null === $this->_isoToPhpFormatConvert) {
            $this->_isoToPhpFormatConvert = array_flip($this->_phpToIsoFormatConvert);
        }
        return isset($this->_isoToPhpFormatConvert[$matches[0]]) ? $this->_isoToPhpFormatConvert[$matches[0]] : $matches[0];
    }

    public function sortByLengthDescCallback($a, $b)
    {
        $a = strlen($a);
        $b = strlen($b);
        if ($a == $b) {
            return 0;
        }

        return ($a < $b)? 1: -1;
    }

    public function lock()
    {
        // touch() didn't work for some reason...
        @file_put_contents($this->getLockFilename(), time());

        return $this;
    }

    public function unlock()
    {
        @unlink($this->getLockFilename());

        return $this;
    }

    public function getConfig($path, $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($path, $scopeType, $scopeCode);
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     * @throws \DomainException
     */
    public function getConnection()
    {
        return $this->_resources->getConnection(ResourceConnection::DEFAULT_CONNECTION);
    }

    /**
     * @return Config
     */
    public function eavModelConfig()
    {
        return $this->_entityTypeConfig;
    }

    /**
     * @return bool
     */
    public function isSingleStoreMode()
    {
        return $this->_storeManager->isSingleStoreMode();
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface|null
     */
    public function getDefaultStoreView()
    {
        return $this->_storeManager->getDefaultStoreView();
    }

    /**
     * @return bool
     */
    public function isPriceGlobal()
    {
        return $this->_catalogHelper->isPriceGlobal();
    }

    /**
     * @param bool $withDefault
     * @param bool $codeKey
     * @return \Magento\Store\Api\Data\WebsiteInterface[]|\Magento\Store\Model\Website[]
     */
    public function getWebsites($withDefault = false, $codeKey = false)
    {
        return $this->_storeManager->getWebsites($withDefault, $codeKey);
    }

    /**
     * @return \Magento\Catalog\Model\Product\Type
     */
    public function productType()
    {
        return $this->_productType;
    }

    /**
     * @param bool $withDefault
     * @param bool $codeKey
     * @return \Magento\Store\Api\Data\StoreInterface[]|\Magento\Store\Model\Store[]
     */
    public function getStores($withDefault = false, $codeKey = false)
    {
        return $this->_storeManager->getStores($withDefault, $codeKey);
    }

    /**
     * @param $storeId
     * @return \Magento\Store\Api\Data\StoreInterface|\Magento\Store\Model\Store
     */
    public function getStore($storeId)
    {
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @param $wId
     * @return \Magento\Store\Api\Data\WebsiteInterface|\Magento\Store\Model\Website
     * @throws LocalizedException
     */
    public function getWebsite($wId)
    {
        return $this->_storeManager->getWebsite($wId);
    }

    /**
     * @return bool
     */
    public function isFlatEnabled()
    {
        return $this->_productFlatIndexState->isFlatEnabled();
    }

    /**
     * @return array
     */
    public function getAttributeCodes()
    {
        return $this->_productFlatIndexHelper->getAttributeCodes();
    }

    /**
     * @param       $productId
     * @param array $productData
     */
    public function addProductIdForRewriteUpdate($productId, array $productData)
    {
        $this->_productsToUpdate[$productId] = $productData;
    }

    /**
     * @return \Unirgy\DropshipProductBulkUpload\Helper\ImageCache
     */
    protected function _imageCacheHelper()
    {
        return Data::om()->get('Unirgy\DropshipProductBulkUpload\Helper\ImageCache');
    }

    public function updateProductsUrlRewrites($storeId = null)
    {
        foreach ($this->_productsToUpdate as $productId => $productData) {
            $this->refreshProductRewrite($productId, $productData);
            if ($productData['store_id'] !== 0) {
                $productData['store_id'] = 0;
                $this->refreshProductRewrite($productId, $productData);
            }
        }
    }

    /**
     * @param $productId
     * @param array $productData
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function refreshProductRewrite($productId, array $productData = [])
    {
        /** @var Product $product */
        $product = $this->_catalogProductFactory->create();
        $product->setId($productId);
        foreach ($this->vitalForGenerationFields as $field) {
            if (isset($productData[$field])) {
                $product->setData($field, $productData[$field]);
            }
        }

        $this->_urlPersist->deleteByData(
            [
                UrlRewrite::ENTITY_ID => $product->getId(),
                UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                UrlRewrite::REDIRECT_TYPE => 0,
                UrlRewrite::STORE_ID => $product->getStoreId()
            ]
        );

        $this->_urlPersist->replace($this->_generator->generate($product));
    }

    /**
     * @return string
     */
    public function getMediaDirPath()
    {
        return $this->_directoryList->getPath(DirectoryList::MEDIA);

    }

    /**
     * @param      $fromDir
     * @param      $toDir
     * @param      $filename
     * @param bool $import
     * @param null $oldValue
     * @param bool $noCopyFlag
     * @return bool
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws UdpuException
     */
    public function copyImageFile(
        $fromDir,
        $toDir,
        &$filename,
        $import = false,
        $oldValue = null,
        $noCopyFlag = false
    )
    {
        $ds = '/';
        $ch = null;
        $missingImageAction = (string)$this->getData('options/import/image_missing_file');
        $existingImageAction = (string)$this->getData('options/import/image_existing_file');
        $imageFilesRemoteBatch = (string)$this->getData('options/import/image_files_remote_batch');
        $remote = preg_match('#^https?:#', $filename);
        $isRemoteBatch = $remote && $imageFilesRemoteBatch;
        if ($remote && !$this->getData('options/import/image_files_remote')) {
            // when image is remote, and remote images are not allowed, do nothing and reset imported value
            $this->getLogger()->warning(__('Skipping: %1, remote images download is disabled.',
                $filename));
            $this->addValue(self::NUM_WARNINGS);
            $filename = '';

            return false;
        }
        $basename = basename($filename);
        if ($remote) {
            $basename = basename(parse_url($filename, PHP_URL_PATH));
        }

        $fromDir                  = rtrim($fromDir, '/\\');
        $toDir                    = rtrim($toDir, '/\\');

        $exportImageRetainFolders = $this->getData('options/export/image_retain_folders');
        if (!$import && $exportImageRetainFolders) {
            $prefix = substr($filename, 0, -strlen($basename));
            $toDir  = $toDir . $ds . trim($prefix, '/\\');
        }

        if ($import && $remote) {
            $slashPos     = false;
            $fromFilename = $filename;
            $fromExists   = true;
            $fromRemote   = true;
            // if remote image and it has been already downloaded, use the existing file instead of downloading
            if (isset($this->_remoteImagesCache[$fromFilename])) {
                $filename     = $this->_remoteImagesCache[$fromFilename]['name'];
                $this->getLogger()->warning(__('%1 is downloaded already, using local file: %2.',
                    $fromFilename, $filename));
                $fromFilename = $this->_remoteImagesCache[$fromFilename]['path'];
                $fromRemote   = false;
                $fromExists   = $isRemoteBatch || is_readable($fromFilename);
                $slashPos     = strpos($filename, $ds);
            } else {  // remote file is not yet downloaded
                if (!$imageFilesRemoteBatch) {
                    if (!function_exists('curl_init')) {
                        $error = __('Unable to locate curl module');
                    } else {
                        $ch = curl_init($fromFilename);
                    }

                    if (!$ch) {
                        $error = __('Unable to open remote file: %1', $fromFilename);
                    }

                    // test if remote image exists
                    curl_setopt($ch, CURLOPT_NOBODY, 1);
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.5.4');
                    $headResult = curl_exec($ch);
                    if ($headResult === false) {
                        $error = __('Testing for remote file "%1" fails', $fromFilename);
                    } else if (false !== strpos($headResult, '404 Not Found')) {
                        $error = __('"404 Not Found" response for remote file: %1', $fromFilename);
                    }
                    if (!empty($error)) {
                        $fromExists = false;
                        $this->addValue(self::NUM_WARNINGS);
                        $this->getLogger()->warning($error);
                    }
                }

                $level = $this->getData('options/import/image_remote_subfolder_level');
                if ($level) {
                    $filenameArr = explode('/', $filename);
                    array_pop($filenameArr);
                    $filename = $basename;
                    for ($i = 0; $i < $level; $i++) {
                        $filename = array_pop($filenameArr) . $ds . $filename;
                    }
                    $slashPos = strpos($filename, $ds);
                } else {
                    $filename = $basename;
                }
                $filename = str_replace(
                    [' ', '%'],
                    '-',
                    urldecode($filename));
            }
        } else {
            $slashPos     = strpos($filename, $ds);
            $fromFilename = $fromDir . $ds . ltrim($filename, $ds);
            /*
            if ($import && $slashPos===0) {
                // if importing and filename starts with slash, use only basename for source file
                $fromFilename = $fromDir.$ds.basename($filename);
            }
            */
            $fromExists = is_readable($fromFilename);
            $fromRemote = false;
        }

        if (is_dir($fromFilename)) {
            // swatch images are media type attribute but do not have actual image most of the time
            $this->getLogger()->warning(__('%1 is not valid file, skipping copy', $fromFilename));

            return true;
        }

        $warning = '';
        //error_log('4', 3, '/var/www/html/var/log/unirgy.log');
        $origBasename = basename($filename);
        $cleanBasename = \Magento\Framework\File\Uploader::getCorrectFileName($origBasename);
        if ($origBasename!=$cleanBasename) {
            $filename = str_replace($origBasename, $cleanBasename, $filename);
            $warning .= __(' Corrected image name: %1.', $filename);
        }
        //$cleanFilename = $filename;
        $toFilename = $toDir . $ds . ltrim($filename, $ds);
        if ($import) {
            if ($slashPos === false) {
                $prefix     = str_replace('\\', $ds, Uploader::getDispretionPath($filename));
                $toDir      .= $ds . ltrim($prefix, $ds);
                $toFilename = rtrim($toDir, $ds) . $ds . ltrim($filename, $ds);
                $filename   = $prefix . $ds . $filename;
            } elseif ($dirname = dirname($filename)) {
                $toDir .= $ds . ltrim($dirname, $ds);
            }
        } elseif (!$import && $slashPos === 0) {
            $toFilename = $toDir . $ds . basename($filename);
        }
        $toExists = is_readable($toFilename);

        $filename = $ds . ltrim($filename, $ds);

        if ($noCopyFlag) {
            return true;
        }

        //error_log('5', 3, '/var/www/html/var/log/unirgy.log');
        if ($import && $toExists && $existingImageAction) {
            $this->addValue(self::NUM_WARNINGS);
            $warning .= __('Imported image file already exists.');
            if ($filename === $oldValue) {
                // new file name is same as current value
                $warning .= __(' %1 is same as current value, %1.', $filename, $oldValue);
            } else {
                switch ($existingImageAction) {
                    case 'skip':
                        $warning .= __(' Skipping field update');
                        $this->getLogger()->warning($warning);

                        return false;
                        break;
                    case 'replace' :
                        // basically just notify user that there is
                        $warning .= __(' Replacing existing image');
                        break;
                    case 'save_new':
                        $warning .= __(' Updating image name and saving as new image.');
                        $toFilename = $this->_getUniqueImageName($toFilename);
//                        $warning .= __(' $toFilename: %1', $toFilename);
                        $newBasename = basename($toFilename);
                        $oldBasename = basename($filename);
                        if ($newBasename !== $oldBasename) {
                            $filename = str_replace($oldBasename, $newBasename, $filename);
                            $warning .= __(' New image name: %1', $filename);
                        }
                        break;
                }
            }
            $this->getLogger()->warning($warning);
        } else if ($import && !$toExists) {
            // if have to import, but image is new
            $this->_getUniqueImageName($toFilename);
        }

        //error_log('6', 3, '/var/www/html/var/log/unirgy.log');
        if (!$fromExists) {
            $warning .= __(' Source image file not found: %1', $fromFilename);
//            $warning = __('Original file image does not exist');
            if ($missingImageAction === 'error') {
                throw new UdpuException($warning);
            }

            $result = false;
            switch ($missingImageAction) {
                case '':
                case 'warning_save':
                    $result = true;
                    $warning .= '. ' . PHP_EOL . __('Image field set to: %1', $filename);
                    break;

                case 'warning_skip':
                    $filename = '';
                    $warning .= '. ' . __('Image field was not updated');
                    break;

                case 'warning_empty':
                    $warning .= '. ' . __('Image field was reset');
                    $filename = null;
                    $result = true;
                    break;
            }
            $this->addValue(self::NUM_WARNINGS);
            $this->getLogger()->warning($warning);

            return $result;
        } else if ($toExists && $fromExists && !$fromRemote
            && $filename === $oldValue
            && filesize($fromFilename) === filesize($toFilename)
        ) {
            // no need to copy
            return false;
        }

        //error_log('7', 3, '/var/www/html/var/log/unirgy.log');
        $this->_directoryWrite->create($toDir)->create();

        if ($fromRemote) {
            //error_log('8', 3, '/var/www/html/var/log/unirgy.log');
            $error = null;
            try {
                if (!function_exists('curl_init')) {
                    throw new \Exception(__('Unable to locate curl module'));
                }

                if (!$imageFilesRemoteBatch) {
                    if (!($ch = curl_init($fromFilename))) {
                        throw new \Exception(__('Unable to open remote file: %1', $fromFilename));
                    }
                    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_NOBODY, 1);
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.5.4');
                    $headResult = curl_exec($ch);
                    if ($headResult === false) {
                        throw new \Exception(__('Testing for remote file "%1" fails', $fromFilename));
                    }
                    if (false !== strpos($headResult, '404 Not Found')) {
                        throw new \Exception(__('"404 Not Found" response for remote file: %1', $fromFilename));
                    }
                    if (!($fp = fopen($toFilename, 'w'))) {
                        throw new \Exception(__('Unable to open local file for writing: %1', $toFilename));
                    }
                    curl_setopt($ch, CURLOPT_NOBODY, 0);
                    curl_setopt($ch, CURLOPT_HTTPGET, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    /*curl_setopt($ch, CURLOPT_HEADERFUNCTION, function (&$hadle, $header) use (&$filename, &$toFilename) {
                        if (strpos($header, 'filename')) {
                            $tmpFileName = null;
                            if(preg_match('/.*filename=[\'\"]([^\'\"]+)/', $header, $matches)) {
                                $tmpFileName = $matches[1];
                            } else if (preg_match('/.*filename=([^ ]+)/', $header, $matches)) {
                                $tmpFileName = $matches[1];
                            }

                            if ($tmpFileName) {
                                $toFilename = str_replace($filename, $tmpFileName, $toFilename);
                                $filename = $tmpFileName;
                            }
                        }
                    return strlen($header);
                    });*/
                    if (!curl_exec($ch)) {
                        throw new \Exception(__('Unable to fetch remote file: %1', $fromFilename));
                    }

                } else {
                    //error_log("ADDED: {$fromFilename} => {$toFilename}\n", 3, '/var/www/html/var/log/unirgy.log');
                    $this->_remoteImagesBatch[$fromFilename] = $toFilename;
                }
                $this->_remoteImagesCache[$fromFilename]['name'] = $filename;
                $this->_remoteImagesCache[$fromFilename]['path'] = $toFilename;
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }
            if (!empty($ch)) {
                curl_close($ch);
            }
            if (!empty($fp)) {
                fclose($fp);
            }
            if (!$imageFilesRemoteBatch && !$this->_isValidImageFile($toFilename)) {
                @unlink($toFilename);
                unset($this->_remoteImagesCache[$fromFilename]);
                $error = __('Remote file is not an image: %1', $fromFilename);
            }
            if (!empty($error)) {
                $this->addValue(self::NUM_WARNINGS);
                $this->getLogger()->warning($error);

                return false;
            }
        } else {
            if ($fromFilename === $toFilename && ($isRemoteBatch || filesize($fromFilename) === filesize($toFilename))) {
                return true; // do not try to copy same image over itself
            }
            if ($toExists) {
                @unlink($toFilename);
            }
            if (!@copy($fromFilename, $toFilename)) {
                $errors = error_get_last();
                $error  = 'COPY ERROR: ';
                if ($errors && array_key_exists('type', $errors)) {
                    $error .= $errors['type'];
                }
                if ($errors && array_key_exists('message', $errors)) {
                    $error .= PHP_EOL . $errors['message'];
                }
                $this->addValue(self::NUM_WARNINGS);
                $this->getLogger()->warning(__('Was not able to copy image file: %1', $error));

                return false;
            }
        }

        return true;
    }

    protected function _isValidImageFile($filename)
    {
        $validator = new \Zend\Validator\File\MimeType('image/png,image/jpeg,image/pjpeg,image/gif');
        return $validator->isValid($filename);
    }

    public function importProcessRemoteImageBatch()
    {
        if (!$this->_remoteImagesBatch) {
            return;
        }
        $t = microtime(1);
        $mh = curl_multi_init();
        $files = [];
        $handles = [];
        foreach ($this->_remoteImagesBatch as $fromFilename => $toFilename) {
            try {
                if (!($ch = curl_init($fromFilename))) {
                    throw new \Exception(__('Unable to open remote file: %1', $fromFilename));
                }
                if (!($fp = fopen($toFilename, 'w'))) {
                    throw new \Exception(__('Unable to open local file for writing: %1', $toFilename));
                }
                //error_log("STARTED: {$fromFilename} => {$toFilename}\n", 3, '/var/www/html/var/log/unirgy.log');
                curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, 'curl/7.5.4');
                curl_setopt($ch, CURLOPT_NOBODY, 0);
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $files[] = $fp;
                $handles[] = $ch;
                curl_multi_add_handle($mh, $ch);
            } catch (\Exception $e) {
                $this->getLogger()->warning($e->getMessage());
            }
        }

        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
            //error_log('1', 3, '/var/www/html/var/log/unirgy.log');
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(100);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
                //error_log('2', 3, '/var/www/html/var/log/unirgy.log');
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        foreach ($handles as $ch) {
            curl_multi_remove_handle($mh, $ch);
            //error_log('0', 3, '/var/www/html/var/log/unirgy.log');
        }
        curl_multi_close($mh);
        foreach ($files as $fp) {
            fclose($fp);
        }
        //error_log("\nTOTAL TIME: " . (microtime(1) - $t) . "\n", 3, '/var/www/html/var/log/unirgy.log');
        $this->_remoteImagesBatch = [];
    }

    protected function _getUniqueImageName($toFilename)
    {
        // initialize product media cache
        if (empty($this->_mediaProductsProcessed[$this->_currentMediaSku])) {
            $this->_mediaProductsProcessed[$this->_currentMediaSku] = [];
        }

        // if there is no entry for this original file name, get new unique name and store it
        if (!isset($this->_mediaProductsProcessed[$this->_currentMediaSku][$toFilename])) {

            $fileInfo      = pathinfo($toFilename);
            if (!$fileInfo['filename']) {
                throw new Row(__('Cannot use %1 as image file name', $toFilename));
            }
            $newName       = Uploader::getNewFileName($toFilename);
//            $this->getLogger()->warning(__METHOD__ . __LINE__ . $toFilename . ' - ' . $newName);
            $oldName = $fileInfo['basename'];
//            if (isset($fileInfo['extension'])) {
//                $oldName .= $fileInfo['extension'];
//            }

            $toFilenameNew = str_replace($oldName, $newName, $toFilename);

            $this->_mediaProductsProcessed[$this->_currentMediaSku][$toFilename] = $toFilenameNew;
        }
//        $this->getLogger()->warning(__METHOD__ . __LINE__ . print_r($this->_mediaProductsProcessed, 1));

        // return unique file name
        return $this->_mediaProductsProcessed[$this->_currentMediaSku][$toFilename];
    }

    public function currentMediaSku($sku = null)
    {
        if (null !== $sku) {
            $this->_currentMediaSku = $sku;
        }

        return $this->_currentMediaSku;
    }

    public function skipStore($sId, $column = 0, $noEmpty = true)
    {
        if ($noEmpty && !$sId) {
            $this->getLogger()->setColumn($column);
            throw new Row(__('Invalid store'));
        }

        return $this->_storeIds && !in_array($sId, $this->_storeIds);
    }

    public function getLogTail($length = 1000)
    {
        try {
            $io = $this->getLogger()->start('r')->seek(-$length, SEEK_END)->getIo();
        } catch (\Exception $e) {
            return [];
        }
        $tail = [];
        while ($t = $io->read()) {
            if (count($t) !== 4 || !in_array($t[0], ['ERROR', 'WARNING', 'SUCCESS'])) {
                continue;
            }
            $tail[] = ['type' => $t[0], 'line' => $t[1], 'col' => $t[2], 'msg' => $t[3]];
        }

        return $tail;
    }

    /**
     * @return \Unirgy\Dropship\Model\Vendor
     * @throws \RuntimeException
     */
    public function getVendor()
    {
        return $this->_session->getVendor();
    }

    /**
     * Create an instance of directory with write permissions
     *
     * @param string $directoryCode
     * @param string $driverCode
     * @return \Magento\Framework\Filesystem\Directory\WriteInterface
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getDirectoryWrite($directoryCode, $driverCode = DriverPool::FILE)
    {
        return $this->filesystem->getDirectoryWrite($directoryCode, $driverCode);
    }

    /**
     * @param string $directoryCode
     * @param string $driverCode
     * @return \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    public function getDirectoryRead($directoryCode, $driverCode = DriverPool::FILE)
    {
        return $this->filesystem->getDirectoryRead($directoryCode, $driverCode);
    }



    /**
     * @param $_imageToDelete
     * @return string
     */
    public function getMediaPath($_imageToDelete)
    {
        return $this->_mediaConfig->getMediaPath($_imageToDelete);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isShowOutOfStock($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            Configuration::XML_PATH_SHOW_OUT_OF_STOCK,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @return \Magento\CatalogSearch\Model\Indexer\Fulltext\Action\Full
     */
    public function getFullTextIndexer()
    {
        return $this->_fullTextIndexer;
    }

    /**
     * @return Url
     */
    public function getUrlHelper()
    {
        return $this->_catalogUrlHelper;
    }

    /**
     * @return \Magento\Framework\Indexer\IndexerRegistry
     */
    public function getIndexerRegistry()
    {
        return $this->_indexerRegistry;
    }

    /**
     * @var array
     */
    protected $_tables;

    /**
     * @param $table
     * @return string|false
     */
    public function getTableName($table)
    {
        if (empty($this->_tables[$table])) {
            try {
                $this->_tables[$table] = $this->_getResource()->getTable($table);
            } catch(\Exception $e) {
                $this->_tables[$table] = false;
            }
        }

        return $this->_tables[$table];
    }

    public function addCategoryIdForRewriteUpdate($categoryId)
    {
        $this->_categoryIdsToUpdate[] = $categoryId;
    }

    /**
     * @return \Magento\Framework\Locale\FormatInterface
     */
    public function getLocaleFormat()
    {
        return $this->formatInterface;
    }

    public function getProfileLocale()
    {
        if (null === $this->_profileLocale) {
            $this->_localeResolver->emulate($this->getImportStoreId());
            $this->_profileLocale = $this->_localeResolver->getLocale();
            $this->_localeResolver->revert();
        }

        return $this->_profileLocale;
    }

    public function getStoreId($id, $allowDefault = false)
    {
        $store = $this->_storeManager->getStore($id);
        if (!$store || (!$allowDefault && $store->getId() == 0)) {
            throw new LocalizedException(__('Invalid store'));
        }

        return $store->getId();
    }

    public function getColumns()
    {
        return $this->getData('columns');
    }

    public function __call($method, $arguments)
    {
        switch (substr($method, 0, 3)) {
            case 'get':
            case 'has':
                return $this->_config->{$method}(...$arguments);
//                return call_user_func_array([$this->_config, $method], $arguments);
            case 'set':
            case 'uns':
                $this->_config->{$method}(...$arguments);
//                call_user_func_array([$this->_config, $method], $arguments);
                return $this;
        }
        throw new LocalizedException(
            __('Invalid method %1::%2', [get_class($this), $method])
        );
    }

    public function dispatch($eventName, $data)
    {
        $this->_eventManager->dispatch($eventName, $data);
    }

    public function getSystemLogger()
    {
        return $this->_logger;
    }

    /**
     * @param array $pIds
     */
    protected function runIndexers(array $pIds)
    {
        if (!empty($pIds)) {
            $this->activity('Reindexing: ' . implode(',', $pIds));
            /* @var \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry */
            $indexerRegistry = self::om()->get('\Magento\Framework\Indexer\IndexerRegistry');
            /* @var \Magento\Indexer\Model\Config $indexerConfig */
            $indexerConfig = self::om()->get('\Magento\Indexer\Model\Config');

            foreach ([
                         \Unirgy\Dropship\Model\Indexer\ProductVendorAssoc\Processor::INDEXER_ID,
                         \Magento\CatalogInventory\Model\Indexer\Stock\Processor::INDEXER_ID,
                         \Magento\Catalog\Model\Indexer\Product\Price\Processor::INDEXER_ID,
                         \Magento\Catalog\Model\Indexer\Product\Flat\Processor::INDEXER_ID,
                         \Magento\Catalog\Model\Indexer\Product\Eav\Processor::INDEXER_ID,
                         \Magento\CatalogSearch\Model\Indexer\Fulltext::INDEXER_ID,
                         \Magento\Catalog\Model\Indexer\Product\Category\Processor::INDEXER_ID,
                         'udropship_product_vendor_limit'
                     ] as $indexerId) {
                if (!$indexerConfig->getIndexer($indexerId)) {
                    $this->getLogger()->warning($indexerId . ' not loaded');
                    continue;
                }
                $indexer = $indexerRegistry->get($indexerId);
                if ($indexer && !$indexer->isScheduled()) {
                    $this->activity($indexerId . ' running');
                    $indexer->reindexList($pIds);
                } else if (!$indexer) {
                    $this->activity($indexerId . ' not found');
                } else if ($indexer->isScheduled()) {
                    $this->activity($indexerId . ' is scheduled');
                }
            }

            if ($this->isEnterpriseEdition()) {
                //$this->_appCacheInterface->invalidateType('full_page');
            }
            if ($this->isModuleActive('Nexcessnet_Turpentine')) {
                $result = self::om()->create('\Nexcessnet\Turpentine\Model\Varnish\Admin')->flushAll();
                $this->activity('Nexcessnet_Turpentine flushed');
                $this->_eventManager->dispatch( 'turpentine_ban_all_cache', $result );
            }
        } else {
            $this->getLogger()->warning('No products to reindex');
        }
    }

    protected function getAllProductIds()
    {
        return $this->_reindexPids;
    }

    public function setReindexPids($pIds)
    {
        $this->_reindexPids = $pIds;
    }

    public function addReindexPids($pIds)
    {
        if (!is_array($this->_reindexPids)) {
            $this->_reindexPids = [];
        }
        $this->_reindexPids = array_unique(array_merge($this->_reindexPids, $pIds));
    }

    /**
     * @return \Unirgy\Dropship\Helper\Data
     */
    public function getUdropshipHelper()
    {
        return $this->objectManager->get('Unirgy\Dropship\Helper\Data');
    }

    public function isUdmultiActive()
    {
        return $this->getUdropshipHelper()->isUdmultiActive();
    }
}
