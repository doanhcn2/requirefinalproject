<?php

/**
 * Created by pp
 *
 * @project pp-dev-2-unirgy-ext
 */

namespace Unirgy\DropshipProductBulkUpload\Helper;

use InvalidArgumentException;
use Magento\Eav\Model\Config;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipProductBulkUpload\Exception;
use Unirgy\DropshipProductBulkUpload\Helper\Data as DropshipProductBulkUploadHelperData;
use Unirgy\DropshipProductBulkUpload\Model\ImportConfig;
use Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ImportConfig as ImportConfigRes;
use Unirgy\DropshipVendorProduct\Helper\Data as HelperData;

class Upload extends AbstractHelper
{
    const IMPORT_PRODUCT_OPTIONS_PREFIX = 'import_product_options';
    const IMPORT_PRODUCT_PREFIX = 'import';
    const UDPRODUPLOAD_OPTIONS_PREFIX = 'udprodupload_options_';
    const PARENT_SKU_FIELD = '__parent_sku__';
    const SKU_FIELD = 'sku';
    const PROD_SIMPLE = 'simple';
    const PROD_CONFIGURABLE = 'configurable';
    const PRODUCT_TYPE_FIELD = 'product.type';
    const PRODUCT_ATTRIBUTE_SET_FIELD = 'product.attribute_set';
    const IMAGE_FIELD = 'image';
    const BASE_IMAGE_FIELD = 'base_image';
    const SMALL_IMAGE_FIELD = 'small_image';
    const THUMBNAIL_FIELD = 'thumbnail';
    const MEDIA_GALLERY_FIELD = 'media_gallery';
    const IGNORE_COLUMN_FIELD = '__ignore_column__';
    const IMPORT_FILE_INPUT = 'import_file';
    const CFG_IMPORT_FILE_INPUT = 'cfg_import_file';
    const IMPORT_TITLE_TEMPLATE = 'import-%s-%s';
    const IMPORT_CFG_TITLE_TEMPLATE = 'import-cfg-%s-%s';
    const LOG_NAME = 'udupload.log';
    const UDROPSHIP_VENDOR_FIELD = 'udropship_vendor';
    /**
     * @var Udprod
     */
    protected $_myUdprodHlp;
    /**
     * @var DirectoryList
     */
    protected $_directoryList;
    /**
     * @var HelperData
     */
    protected $_udprodHlp;
    /**
     * @var DropshipHelperData
     */
    protected $_hlp;
    /**
     * @var DropshipProductBulkUploadHelperData
     */
    protected $_udpupHlp;
    /**
     * @var ImportConfigRes
     */
    protected $_importConfigRes;
    /**
     * @var Config
     */
    protected $_eavConfig;
    /**
     * @var ProtectedCode
     */
    protected $_udpupHlpPr;
    /**
     * @var Vendor
     */
    protected $vendor;
    /**
     * @var array a map to hold reference of import file columns to final file columns
     */
    protected $headingMap = array();
    /**
     * Product uploader data
     *
     * @var array|false
     */
    protected $productData;
    /**
     * Product Cfg uploader data
     *
     * @var array|false
     */
    protected $productOptionsData;
    /**
     * file handle for uploaded product file
     *
     * @var resource
     */
    protected $fhProduct;
    /**
     * file handle for uploaded product cfg file
     *
     * @var resource
     */
    protected $fhCfgProduct;
    /**
     * file handle for generated product file
     *
     * @var resource
     */
    protected $fhWriteProduct;
    /**
     * file handle for generated product cfg file
     *
     * @var resource
     */
    protected $fhWriteCfgProduct;
    /**
     * product upload file headings
     *
     * @var array
     */
    protected $headings;
    /**
     * product cfg upload file headings
     *
     * @var array
     */
    protected $headingsCfg;
    /**
     * Parent product skus
     *
     * @var array
     */
    protected $parentSkus = [];
    /**
     * Generated product import file headings
     *
     * @var array
     */
    protected $prodHeadings = [];
    /**
     * @var DropshipHelperData
     */
    protected $udHelper;
    /**
     * @var DropshipProductBulkUploadHelperData
     */
    protected $dataHelper;
    /**
     * Default product import options
     *
     * @var array
     */
    protected $_defaultProductOptions = [
        'title' => '',
        'profile_type' => 'import',
        'profile_status' => 'enabled',
        'media_type' => 'csv',
        'run_status' => 'idle',
        'invoke_status' => 'none',
        'data_type' => 'product',
        'store_id' => '0',
        'last_user_id' => null,
        'columns' => [
            'category.ids'                             => [
                'field'               => 'category.ids',
                'alias'               => 'category.ids',
                'default_multiselect' => '1',
                'separator'           => ';'
            ],
            'category.name'                            => [
                'field'               => 'category.name',
                'alias'               => 'product_categories',
                'default_multiselect' => '1',
                'separator'           => ';'
            ],
            'price'                                    => [
                'field'   => 'price',
                'alias'   => 'price',
                'default' => '0.0',
            ],
            'stock.qty'                                => [
                'field'   => 'stock.qty',
                'alias'   => 'stock.qty',
            ],
            'stock.is_in_stock'                        => [
                'field'   => 'stock.is_in_stock',
                'alias'   => 'quantity_and_stock_status',
            ],
            'stock.backorders'                         => [
                'field' => 'stock.backorders',
                'alias' => 'stock.backorders',
            ],
            'stock.enable_qty_increments'              => [
                'field' => 'stock.enable_qty_increments',
                'alias' => 'stock.enable_qty_increments',
            ],
            'stock.addqty'                             => [
                'field' => 'stock.addqty',
                'alias' => 'stock.addqty',
            ],
            'stock.is_qty_decimal'                     => [
                'field' => 'stock.is_qty_decimal',
                'alias' => 'stock.is_qty_decimal',
            ],
            'stock.manage_stock'                       => [
                'field' => 'stock.manage_stock',
                'alias' => 'stock.manage_stock',
            ],
            'stock.max_sale_qty'                       => [
                'field' => 'stock.max_sale_qty',
                'alias' => 'stock.max_sale_qty',
            ],
            'stock.min_qty'                            => [
                'field' => 'stock.min_qty',
                'alias' => 'stock.min_qty',
            ],
            'stock.min_sale_qty'                       => [
                'field' => 'stock.min_sale_qty',
                'alias' => 'stock.min_sale_qty',
            ],
            'stock.notify_stock_qty'                   => [
                'field' => 'stock.notify_stock_qty',
                'alias' => 'stock.notify_stock_qty',
            ],
            'stock.qty_increments'                     => [
                'field' => 'stock.qty_increments',
                'alias' => 'stock.qty_increments',
            ],
            'stock.stock_status_changed_automatically' => [
                'field' => 'stock.stock_status_changed_automatically',
                'alias' => 'stock.stock_status_changed_automatically',
            ],
            'stock.use_config_backorders'              => [
                'field' => 'stock.use_config_backorders',
                'alias' => 'stock.use_config_backorders',
            ],
            'stock.use_config_enable_qty_increments'   => [
                'field' => 'stock.use_config_enable_qty_increments',
                'alias' => 'stock.use_config_enable_qty_increments',
            ],
            'stock.use_config_manage_stock'            => [
                'field' => 'stock.use_config_manage_stock',
                'alias' => 'stock.use_config_manage_stock',
            ],
            'stock.use_config_max_sale_qty'            => [
                'field' => 'stock.use_config_max_sale_qty',
                'alias' => 'stock.use_config_max_sale_qty',
            ],
            'stock.use_config_min_sale_qty'            => [
                'field' => 'stock.use_config_min_sale_qty',
                'alias' => 'stock.use_config_min_sale_qty',
            ],
            'stock.use_config_min_qty'                 => [
                'field' => 'stock.use_config_min_qty',
                'alias' => 'stock.use_config_min_qty',
            ],
            'stock.use_config_qty_increments'          => [
                'field' => 'stock.use_config_qty_increments',
                'alias' => 'stock.use_config_qty_increments',
            ],
            'stock.use_config_notify_stock_qty'        => [
                'field' => 'stock.use_config_notify_stock_qty',
                'alias' => 'stock.use_config_notify_stock_qty',
            ],
            'product.type'                             => [
                'field'   => 'product.type',
                'alias'   => 'product.type',
                'default' => 'simple',
            ],
            'product.attribute_set'                    => [
                'field' => 'product.attribute_set',
                'alias' => 'product.attribute_set',
            ],
            'product.websites'                         => [
                'field'               => 'product.websites',
                'alias'               => 'product_websites',
                'default_multiselect' => '1',
                'separator'           => ';',
            ]
        ],
        'options' => [
            'dir' => [
                'images' => '{var}/udprodupload/import/images'
            ],
            'csv' => [
                'delimiter' => ',',
                'enclosure' => '"',
                'escape' => '\\',
                'multivalue_separator' => ';',
            ],
            'encoding' => [
                'from' => '',
                'to' => 'UTF-8',
                'illegal_char' => '',
            ],
            'finetune' => [
                'import_page_size' => 100,
                'export_page_size' => 100,
                'page_sleep_delay' => 0,
                'status_refresh_delay' => 3,
            ],
            'log' => ['min_level' => 'WARNING',],
            'import' => [
                'actions' => 'any',
                'dryrun' => '0',
                'change_typeset' => '1',
                'select_ids' => '0',
                'not_applicable' => '0',
                'store_value_same_as_default'   => 'default',
                'stock_zero_out' => '0',
                'increment_url_key' => '0',
                'reindex_type' => 'manual',
                'image_files' => '1',
                'image_files_remote' => '1',
                'image_remote_subfolder_level' => '',
                'image_delete_old' => '0',
                'image_delete_skip_usage_check' => '0',
                'image_missing_file' => 'warning_skip',
                'image_existing_file' => 'replace',
                'image_files_remote_batch' => 1,
                'create_options' => '0',
                'create_categories' => '0',
                'create_categories_active' => '1',
                'create_categories_anchor' => '1',
                'create_categories_display' => 'PRODUCTS_AND_PAGE',
                'create_categories_menu' => '1',
                'delete_old_category_products' => '1',
                'create_attributesets' => '0',
                'create_attributeset_template' => '4',
                'insert_attr_chunk_size' => '',
            ],
            'reindex' => [
                'catalog_product_attribute' => 0,
                'catalog_url' => 1,
                'catalog_category_product' => 2,
            ],
        ],
    ];
    /**
     * Default product extra options
     *
     * @var array
     */
    protected $_defaultCfgOptions = [
        'title' => '',
        'profile_type' => 'import',
        'profile_status' => 'enabled',
        'media_type' => 'csv',
        'run_status' => 'idle',
        'invoke_status' => 'none',
        'data_type' => 'product_extra',
        'filename' => '',
        'store_id' => '0',
        'base_dir' => 'import/',
        'options' =>
            [
                'dir' => [
                    'images' => '{var}/udprodupload/import/images'
                ],
                'csv' => [
                    'delimiter' => ',',
                    'enclosure' => '"',
                    'escape' => '\\',
                    'multivalue_separator' => ';',
                ],
                'encoding' => [
                    'from' => '',
                    'to' => 'UTF-8',
                    'illegal_char' => 'IGNORE',
                ],
                'finetune' => [
                    'import_page_size' => 100,
                    'export_page_size' => 100,
                    'page_sleep_delay' => 0,
                    'status_refresh_delay' => 3,
                ],
                'log' =>
                    [
                        'min_level' => 'WARNING',
                    ],
                'import' => [
                    'urlpath_prepend_root' => '0',
                    'reindex_type' => 'manual',
                    'image_files' => '1',
                    'image_files_remote' => '1',
                    'image_remote_subfolder_level' => '',
                    'image_delete_skip_usage_check' => '0',
                    'image_missing_file' => 'warning_skip',
                    'image_existing_file' => 'replace',
                    'image_files_remote_batch' => 1
                ],
            ],
    ];

    public function __construct(
        Context $context,
        Udprod $helperUdprod,
        DirectoryList $directoryList,
        HelperData $helperData,
        DropshipHelperData $dropshipHelperData,
        DropshipProductBulkUploadHelperData $udprodUploadHelper,
        ImportConfigRes $importConfigRes,
        ProtectedCode $helperProtectedCode,
        Config $eavConfig
    )
    {
        $this->_myUdprodHlp = $helperUdprod;
        $this->_directoryList = $directoryList;
        $this->_udprodHlp = $helperData;
        $this->_hlp = $dropshipHelperData;
        $this->_udpupHlp = $udprodUploadHelper;
        $this->_importConfigRes = $importConfigRes;
        $this->_eavConfig = $eavConfig;
        $this->_udpupHlpPr = $helperProtectedCode;

        parent::__construct($context);
    }

    /**
     * @param \Magento\Framework\App\Request\Http $req
     * @return array
     * @throws InvalidArgumentException
     */
    public function uploadAndImport($req)
    {
        $importOptions = $this->uploadAndProcess($req);

        $this->doImportFromRequest($req);

        return $importOptions;
    }

    /**
     * @param Http $req
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function uploadAndProcess($req)
    {
        $setId = $req->getParam('set_id');
        $this->_uploadFiles($req);

        // retrieve config
        $upHlp     = $this->getHelperUdprod();
        $setIdArr = explode('-', $setId, 2);
        $timestamp = date('Y-m-d-H-i');
        // actual files to be imported
        $filename = $this->_filename(self::IMPORT_PRODUCT_PREFIX, $setIdArr[1],
            $upHlp->getAttributeSetName($setIdArr[0]),
            $timestamp);
        $filenameOptions = $this->_filename(self::IMPORT_PRODUCT_OPTIONS_PREFIX, $setIdArr[1],
            $upHlp->getAttributeSetName($setIdArr[0]), $timestamp);

        $this->_openFileHandles($filename, $filenameOptions);

        //$typeOfProduct = $req->getParam('type_of_product');
        $cfgColumns = $upHlp->getCfgColumnsIds($setId);
        $columns = $upHlp->getColumnsIds($setId);
        $this->_setupHeadings($columns, $cfgColumns);

        $this->_processCfgExtras($upHlp->getCfgColumns($setId));

        $this->_buildProdHeadings();

        // set default values
        /** @var Vendor $vendor */
        $vendor = $this->currentVendor();
        $tplProd = $this->_templateProduct($vendor, $setId);

        $defaultData = array_filter($tplProd->getData());
        $defaultData[self::PRODUCT_ATTRIBUTE_SET_FIELD] = $upHlp->getAttributeSetName($setIdArr[0]);
        $vendorId    = $vendor->getId();
        $defaultData[self::UDROPSHIP_VENDOR_FIELD] = $vendorId;
        $this->outputProcessedData($defaultData);
        $this->closeHandles();

        $importOptions = $this->getDefaultProductOptions();
        $importOptions['filename'] = $filename;
        $importOptions['filename_options'] = $filenameOptions;
        $userImportOptions = $upHlp->getImportOptions($setId);

        $importOptions['options']['import'] = $userImportOptions;

        $attributes = $this->prepareAttributeFields();

        foreach ($defaultData as $field => $value) {
            if (!isset($attributes[$field]) || $field === 'sku') {
                continue;
            }
            $fieldConfig = $this->getProductFieldConfig($field);
            $fieldConfig['default'] = $value;
            $importOptions['columns'][$field] = $fieldConfig;
        }

        $websiteIds = $tplProd->getWebsiteIds();
        if (!$this->_udprodHlp->getUseTplProdWebsiteBySetId($setId)) {
            $websiteIds = $this->_udprodHlp->getDefaultWebsiteBySetId($setId);
        }
        if ($websiteIds) {
            // in import file we don't need ids as separate data, we need them as string
            if (is_array($websiteIds)) {
                $websiteIds = implode(',', $websiteIds);
            }
            $fieldConfig = $this->getProductFieldConfig('product.websites');
            $fieldConfig['default'] = $websiteIds;
            $importOptions['columns'][$fieldConfig['field']] = $fieldConfig;
        }

        $categoryIds = $tplProd->getCategoryIds();
        if (!$this->_udprodHlp->getUseTplProdCategoryBySetId($setId)) {
            $categoryIds = $this->_udprodHlp->getDefaultCategoryBySetId($setId);
            // if result is string, explode it
            if (is_string($categoryIds)) {
                $categoryIds = explode(',', $categoryIds);
            }
        }
        if ($categoryIds) {
            $fieldConfig = $this->getProductFieldConfig('category.ids');
            // implode results with configured separator
            $fieldConfig['default'] = implode($fieldConfig['separator'], $categoryIds);
            $importOptions['columns'][$fieldConfig['field']] = $fieldConfig;
        }


        // 1 make profile ID $vendor->getId() + $setId
        // populate \Unirgy_DropshipProductBulkUpload_Model_ResourceModel_ImportConfig and save it
        $productImportName    = sprintf(self::IMPORT_TITLE_TEMPLATE, $vendorId, $setId);
        $productCfgImportName = sprintf(self::IMPORT_CFG_TITLE_TEMPLATE, $vendorId, $setId);

        $productImportModel = $this->importConfigByTitle($productImportName);
        //$productImportModel->addData($importOptions);
        $baseVendorDir = '{var}/udprodupload/import/' . $this->currentVendor()->getId() . '/';
        $importOptions['base_dir'] = $baseVendorDir;
        $importOptions['title'] = $productImportName;
        $importOptions['options']['headings_map'] = $this->headingMap;
        $importOptions['vendor_id'] = $vendorId;
        $productImportModel
            ->addData($importOptions)
            ->save();

        $productCfgImportModel = $this->importConfigByTitle($productCfgImportName);
        $data = $this->_defaultCfgOptions;
        $data['base_dir'] = $baseVendorDir;
        $data['title']= $productCfgImportName;
        $data['filename']= $importOptions['filename_options'];
        $data['options']['headings_map'] = $this->headingMap;
        $data['vendor_id'] = $vendorId;
        $data['options']['import'] = $userImportOptions;
        $productCfgImportModel->addData($data)->save();

        return [
            'import' => $productImportName,
            'import_cfg' => $productCfgImportName,
        ];
    }

    /**
     *
     * @param Http $req
     */
    protected function _uploadFiles($req)
    {
        //$setId     = $req->getParam('set_id');
        //$isCfgAllowed = Mage::helper('udprod')->hasTplConfigurableAttributes(null, $setId);
        $this->productData = $this->_uploadFile($req->getParam(self::IMPORT_FILE_INPUT));
        //if ($isCfgAllowed) {
        //    $this->productOptionsData = $this->_uploadFile($req->getParam(self::CFG_IMPORT_FILE_INPUT));
        //}
    }

    /**
     * @param string $fileName
     * @param string $target
     * @return array|bool
     * @throws LocalizedException
     */
    protected function _uploadFile($fileName, $target = 'udprodupload/import')
    {
        if(empty($fileName)){
            return false;
        }
        /** @var Udprod $udprodHelper */
        $udprodHelper = $this->_myUdprodHlp;
        $tmpPath = $udprodHelper->getFilePath(
            $udprodHelper->getBaseTmpPath(),
            $fileName
        );
        if (!is_file($tmpPath) || !is_readable($tmpPath)) {
            throw new LocalizedException(__('Uploaded file was not found'));
        }
//        $target = $this->_directoryList->getPath(DirectoryList::VAR_DIR) . $target;
//if this is breaking, we may need to manually create target path
        $newFileName = $udprodHelper->moveFileFromTmp(
            $udprodHelper->getBaseTmpPath(),
            $udprodHelper->getBasePath(),
            $fileName
        );
        $result['path'] = $udprodHelper->getBasePath();
        $result['file'] = basename($newFileName);

        return $result;
    }

    /**
     * @return Udprod
     */
    protected function getHelperUdprod()
    {
        return $this->_myUdprodHlp;
    }

    /**
     * @param string $baseName
     * @param int $attrSetName
     * @param string $fieldsetName
     * @param int $timestamp
     * @return string
     */
    protected function _filename($baseName = self::IMPORT_PRODUCT_PREFIX, $attrSetName = 1, $fieldsetName = '',
                                 $timestamp = 0)
    {
        return sprintf('%s-%s-%s-%s.csv', $baseName, $attrSetName, $fieldsetName, $timestamp);
    }

    /**
     * @param $filename
     * @param $filenameOptions
     */
    protected function _openFileHandles($filename, $filenameOptions)
    {
        if (!empty($this->productData) && isset($this->productData['path'], $this->productData['file'])) {
            $this->fhProduct = @fopen($this->productData['path'] . DIRECTORY_SEPARATOR . $this->productData['file'], 'rb');
            $this->fhWriteProduct = @fopen($this->productData['path'] . DIRECTORY_SEPARATOR . $filename, 'wb');
            $this->fhWriteCfgProduct = @fopen($this->productData['path'] . DIRECTORY_SEPARATOR . $filenameOptions, 'wb');
        }

        //if (!empty($this->productOptionsData) && isset($this->productOptionsData['path'], $this->productOptionsData['file'])) {
        //    $this->fhCfgProduct      = @fopen($this->productOptionsData['path'] . DS . $this->productOptionsData['file'],
        //        'rb');
        //}
    }

    /**
     * Setup upload files headings
     *
     * @param $columnsToImport
     * @param $columnsCfgToImport
     * @throws LocalizedException
     */
    protected function _setupHeadings($columnsToImport, $columnsCfgToImport)
    {
        unset($columnsToImport['$ROW']);
        $this->headings = fgetcsv($this->fhProduct);
        $this->_validateColumns($this->headings, $columnsToImport);
        $__skuIdx = array_search(self::SKU_FIELD, $this->headings);
        if ($__skuIdx === false) {
            throw new LocalizedException(__('Products sku column missing'));
        }

        if ($this->fhCfgProduct) {
            $this->headingsCfg = fgetcsv($this->fhCfgProduct);
            unset($columnsCfgToImport['$ROW']);
            $this->_validateColumns($this->headingsCfg, $columnsCfgToImport);
        }
    }

    /**
     * Validate that uploaded columns match configured columns
     *
     * Currently validating columns match in order and names
     * $expected array is 1 based, $actual array is 0 based, so expected index must be decremented prior checking
     * for match in actual
     *
     * @param array $actual
     * @param array $expected
     * @throws InvalidArgumentException
     */
    protected function _validateColumns($actual, $expected)
    {
        if (count($actual) !== count($expected)) {
            throw new InvalidArgumentException('Imported columns count does not match configured columns count');
        }

        foreach ($expected as $idx => $ex) {
            if ($actual[$idx] != $ex) {
                throw new InvalidArgumentException(sprintf('Column %s does not match configured column %s.',
                    $actual[$idx], $ex));
            }
        }
        // alternative check just for presence not order
        //foreach ($expected as $ex) {
        //    if(!in_array($ex, $actual)){
        //        throw new InvalidArgumentException(sprintf('Column %s not found in imported columns.', $ex));
        //    }
        //}
    }

    /**
     * Process and generate extras data
     *
     * @param $cfgColumns
     */
    protected function _processCfgExtras($cfgColumns)
    {
        $this->_checkHandles();
        // populate super attributes
        $superAttributes = [];
        foreach ($cfgColumns as $column) {
            if (isset($column['is_super_attribute']) && $column['is_super_attribute']) {
                $superAttributes[] = $column;
            }
        }
        // create a map of parent-child skus
        $__parentSkuIdx = array_search(self::PARENT_SKU_FIELD, $this->headings);
        $__skuCfgIdx    = array_search(self::SKU_FIELD, $this->headings);
        $__mediaCfgIdx  = array_search(self::MEDIA_GALLERY_FIELD, $this->headings);
        if ($__skuCfgIdx === false) {
            throw new LocalizedException(__('Options sku column missing'));
        }
        // create extras file
        while ($row = fgetcsv($this->fhProduct)) {
            if ($__parentSkuIdx !== false && $row[$__parentSkuIdx]) {
                // fetch and store all parent skus
                $sku = $row[$__parentSkuIdx];
                if (!isset($this->parentSkus[$sku])) {
                    // set parent sku as key for faster lookup
                    $this->parentSkus[$sku] = 1;
                    // generate CPSA entry, once per parent sku
                    $p = 1;
                    foreach ($superAttributes as $attribute) {
                        $cpsaEntry = [
                            'CPSA',
                            $sku,
                            $attribute['id'],
                            $p++,
                            $attribute['label']
                        ];
                        fputcsv($this->fhWriteCfgProduct, $cpsaEntry);
                    }
                    // end generate CPSA entry
                }

                // generate CPSI entry
                $cpsiEntry = ['CPSI', $sku, $row[$__skuCfgIdx]];
                fputcsv($this->fhWriteCfgProduct, $cpsiEntry);
                // end generate CPSI entry
            }

            // generate CPI entries
            if ($__mediaCfgIdx !== false && $row[$__mediaCfgIdx]) {
                $delimiter = ';';
                foreach (explode($delimiter, $row[$__mediaCfgIdx]) as $item) {
                    $cpiEntry = ['CPI', $row[$__skuCfgIdx], $item];
                    fputcsv($this->fhWriteCfgProduct, $cpiEntry);
                }
            }
            // end generate CPI entries
        }
    }

    /**
     *
     */
    protected function _checkHandles()
    {
        if (!$this->fhProduct || !$this->fhWriteProduct || !$this->fhWriteCfgProduct) {
            throw new LocalizedException(__('File handles not open'));
        }
    }

    /**
     * @return void
     */
    protected function _buildProdHeadings()
    {
        // build final prod file headings
        $excluded = [
            self::MEDIA_GALLERY_FIELD,
            self::BASE_IMAGE_FIELD,
            self::PARENT_SKU_FIELD,
            self::IGNORE_COLUMN_FIELD
        ];
        $assignImages = false;

        foreach ($this->headings as $idx => $heading) {
            if (!in_array($heading, $excluded, true)) {
                $this->prodHeadings[] = $heading;
            }

            if ($heading === self::BASE_IMAGE_FIELD || $heading === self::IMAGE_FIELD) { // assign image fields if base image is present
                $assignImages = true;
            }

            $this->headingMap[$heading] = $idx;
        }

        //foreach ($this->headingsCfg as $heading) {
        //    if (!in_array($heading, $excluded) && !in_array($heading, $this->prodHeadings)) {
        //        $this->prodHeadings[] = $heading;
        //    }
        //}

        if (!in_array(self::PRODUCT_TYPE_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::PRODUCT_TYPE_FIELD;
        }

        if (!in_array(self::PRODUCT_ATTRIBUTE_SET_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::PRODUCT_ATTRIBUTE_SET_FIELD;
        }

        if ($assignImages && !in_array(self::IMAGE_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::IMAGE_FIELD;
        }
        if ($assignImages && !in_array(self::SMALL_IMAGE_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::SMALL_IMAGE_FIELD;
        }
        if ($assignImages && !in_array(self::THUMBNAIL_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::THUMBNAIL_FIELD;
        }
        if (!in_array(self::UDROPSHIP_VENDOR_FIELD, $this->prodHeadings, true)) {
            $this->prodHeadings[] = self::UDROPSHIP_VENDOR_FIELD;
        }
        // end build final prod file headings
    }

    /**
     * @return Vendor
     */
    protected function currentVendor()
    {
        if ($this->vendor === null) {
            $this->vendor = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')
                ->getVendor();
        }

        return $this->vendor;
    }

    /**
     * @param $vendor
     * @param $setId
     * @return \Unirgy\DropshipVendorProduct\Model\Product
     */
    protected function _templateProduct($vendor, $setId)
    {
        return $this->_udprodHlp->getTplProdBySetId($vendor, $setId);
    }

    /**
     * @param $defaultData
     */
    protected function outputProcessedData($defaultData)
    {
        fputcsv($this->fhWriteProduct, $this->prodHeadings);// output headings in final file
        fseek($this->fhProduct, 0);
        $r = 0;
        // loop main file and pour data in final file
        while ($row = fgetcsv($this->fhProduct)) {
            if($r === 0) {
                //headings, skip
                $r++;
                continue;
            }
            $row = array_combine($this->headings, $row);
            $sku = $row[self::SKU_FIELD];
            $row[self::PRODUCT_TYPE_FIELD] = self::PROD_SIMPLE;
            if (isset($this->parentSkus[$sku])) {
                $row[self::PRODUCT_TYPE_FIELD] = self::PROD_CONFIGURABLE;
                $this->parentSkus[$sku] = $row; // store parent data for reuse from children, be careful for memory raise
            }

            if (isset($row[self::PARENT_SKU_FIELD]) && $row[self::PARENT_SKU_FIELD]) {
                $parentSku = $row[self::PARENT_SKU_FIELD];
                if (isset($this->parentSkus[$parentSku])) {
                    foreach ($this->parentSkus[$parentSku] as $field => $parentVal) {
                        if (empty($row[$field]) && $parentVal) {
                            $row[$field] = $parentVal;
                        }
                    }
                }
            }

            if (isset($row[self::IMAGE_FIELD])) {
                $row[self::SMALL_IMAGE_FIELD] = $row[self::IMAGE_FIELD];
                $row[self::THUMBNAIL_FIELD] = $row[self::IMAGE_FIELD];
            }

            //if (isset($row[self::MEDIA_GALLERY_FIELD])) {
            //    $delimiter = ';';
            //    foreach (explode($delimiter, $row[self::MEDIA_GALLERY_FIELD]) as $item) {
            //        fputcsv($this->fhWriteCfgProduct, array('CPI', $sku, $item));
            //    }
            //    unset($row[self::MEDIA_GALLERY_FIELD]);
            //}

            if(!isset($row[self::UDROPSHIP_VENDOR_FIELD])){
                $row[self::UDROPSHIP_VENDOR_FIELD] = $defaultData[self::UDROPSHIP_VENDOR_FIELD];
            }


            $entityData = [];
            foreach ($this->prodHeadings as $heading) {
                if (isset($row[$heading])) {
                    $entityData[] = $row[$heading];
                } else if (isset($defaultData[$heading])) {
                    $entityData[] = $defaultData[$heading];
                } else {
                    $entityData[] = '';
                }
            }

            fputcsv($this->fhWriteProduct, $entityData);
        }
        // end loop main file and pour data in final file

        // add simple products from options file to main file
        // loop options file and pour data in final file
        //fseek($this->fhCfgProduct, 0); // reset handle
        //fgetcsv($this->fhCfgProduct); // readout headings row
        //while ($row = fgetcsv($this->fhCfgProduct)) {
        //    $row                           = array_combine($this->headingsCfg, $row);
        //    $parentSku                     = $row[self::PARENT_SKU_FIELD];
        //    $row[self::PRODUCT_TYPE_FIELD] = self::PROD_SIMPLE;
        //

        //    if (isset($row[self::IMAGE_FIELD])) {
        //        $row[self::SMALL_IMAGE_FIELD] = $row[self::IMAGE_FIELD];
        //        $row[self::THUMBNAIL_FIELD]   = $row[self::IMAGE_FIELD];
        //    }
        //

        //    if (isset($this->parentSkus[$parentSku]) && is_array($this->parentSkus[$parentSku])) {
        //        // merge parent data
        //        foreach ($this->parentSkus[$parentSku] as $field => $value) {
        //            if (!isset($row[$field])) {
        //                $row[$field] = $value;
        //            }
        //        }
        //    }
        //

        //    $entityData = array();
        //    foreach ($this->prodHeadings as $heading) {
        //        if (isset($row[$heading])) {
        //            $entityData[] = $row[$heading];
        //        } else if (isset($defaultData[$heading])) {
        //            $entityData[] = $defaultData[$heading];
        //        } else {
        //            $entityData[] = '';
        //        }
        //    }
        //

        //    fputcsv($this->fhWriteProduct, $entityData);
        //}
    }

    /**
     *
     */
    protected function closeHandles()
    {
        // close all file handles
        if ($this->fhProduct) {
            fclose($this->fhProduct);
        }
        if ($this->fhCfgProduct) {
            fclose($this->fhCfgProduct);
        }
        if ($this->fhWriteProduct) {
            fclose($this->fhWriteProduct);
        }
        if ($this->fhWriteCfgProduct) {
            fclose($this->fhWriteCfgProduct);
        }
    }

    protected function prepareAttributeFields()
    {
        $entityType = $this->_eavConfig->getEntityType('catalog_product');
        $attrs = $entityType->getAttributeCollection();
        $fields = [];
        $hidden = [];
        $removeFields = ['has_options', 'required_options', 'category_ids', 'minimal_price',
            'created_at', 'updated_at', 'product.entity_id', 'url_key',
            'entity_id', 'entity_type_id', 'attribute_set_id', 'price.final',
            'price.minimal', 'price.maximum'];
        foreach ($attrs as $k => $attribute) {
            $attr = $attribute->toArray();
            if ($attr['frontend_input'] === 'gallery' || in_array($attr['attribute_code'], $removeFields)) {
                continue;
            }
            if (empty($attr['frontend_label'])) {
                $attr['frontend_label'] = $attr['attribute_code'];
            }
            if (in_array($attr['frontend_input'], ['select', 'multiselect'])) {
                try {
                    if (!$attribute->getSource()) {
                        continue;
                    }
                    $opts = $attribute->getSource()->getAllOptions();
                    foreach ($opts as $o) {
                        if (is_array($o['value'])) {
                            foreach ($o['value'] as $o1) {
                                $attr['options'][$o['label']][$o1['value']] = $o1['label'];
                            }
                        } elseif (is_scalar($o['value'])) {
                            $attr['options'][$o['value']] = $o['label'];
                        }
                    }
                } catch (\Exception $e) {
                    // can be all kinds of custom source models, just ignore
                }
            }
            if (!empty($attr['is_visible'])) {
                $fields[$attr['attribute_code']] = $attr;
            } else {
                unset($attr['is_required']);
                $hidden[$attr['attribute_code']] = $attr;
            }
        }
        $groups['attributes'] = ['label' => __('Product Attributes'), 'fields' => $fields];
        $groups['hidden'] = ['label' => __('Hidden Attributes'), 'fields' => $hidden];

        $attrs = $this->fetchSystemAttributes();
        $gr = [
            'product' => __('System Attributes'),
            'stock' => __('Inventory Stock'),
            'category' => __('Category'),
        ];
        if ($this->_hlp->isUdmultiActive()) {
            $gr['udmulti'] = __('Multi Vendor Data');
        }

        foreach ($attrs as $field => $attribute) {
            if (in_array($field, $removeFields)) {
                continue;
            }
            $fa = explode('.', $field, 2);
            if (empty($fa[1])) {
                if (strpos($field, '_type') !== false) {
                    if (empty($attribute['frontend_label'])) {
                        $attribute['frontend_label'] = $field;
                    }
                    $groups['hidden']['fields'][$field] = $attribute;
                }
                continue;
            }
            if (empty($groups[$fa[0]])) {
                $groups[$fa[0]] = ['label' => $gr[$fa[0]], 'fields' => []];
            }
            $attribute['attribute_code'] = $field;
            $groups[$fa[0]]['fields'][$field] = $attribute;
        }

        $fields = [
            'attribute_code' => 1,
            'backend_type' => 1,
            'frontend_label' => 1,
            'frontend_input' => 1,
            'options' => 1,
            'is_required' => 1
        ];
        $result = [];
        foreach ($groups as &$group) {
            foreach ($group['fields'] as $name => &$attribute) {
                foreach ($attribute as $field => $value) {
                    if (empty($fields[$field])) {
                        unset($attribute[$field]);
                    }
                }
                if (!empty($attribute['options'])) {
                    $options = $attribute['frontend_input'] === 'multiselect' ? [] : ['' => ''];
                    foreach ($attribute['options'] as $k => $value) {
                        if ($k === '') {
                            continue;
                        }
                        if (is_array($value)) {
                            foreach ($value as $k1 => $v1) {
                                $options[$k][$k1 . ' '] = $v1;
                            }
                        } else {
                            $options[$k . ' '] = $value;
                        }
                    }
                    $attribute['options'] = $options;
                }
                $result[$name] = $attribute;
            }
            unset($attribute);
        }
        unset($group);

        return $result;
    }

    /**
     * @return array
     */
    protected function fetchSystemAttributes()
    {
        /** @var ProtectedCode $productImportHelper */
        $productImportHelper = $this->_udpupHlpPr;
        $helper              = $this->dataHelper();
        $helper->setConfig($this->_importConfigRes->getModel());
        $productImportHelper->setHelper($helper);

        return $productImportHelper->prepareSystemAttributes();
    }

    /**
     * @return DropshipProductBulkUploadHelperData
     */
    protected function dataHelper()
    {
        if (!$this->dataHelper) {
            $this->dataHelper = $this->_udpupHlp;
        }

        return $this->dataHelper;
    }

    protected function getProductFieldConfig($field)
    {
        $result = [
            'field' => $field,
            'alias' => $field
        ];

        foreach ($this->getDefaultProductOptions()['columns'] as $column) {
            if ($column['field'] === $field) {
                $result = $column;
                break;
            }
        }

        return $result;
    }

    /**
     * Profile title, should be unique per vendor and set-id
     *
     * @param string $title
     * @return false|ImportConfig
     */
    public function importConfigByTitle($title)
    {
        /** @var ImportConfigRes $resource */
        $resource = $this->_importConfigRes;

        return $resource->findModelByTitle($title);
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $req
     * @return array
     * @throws \Exception
     * @throws Exception
     */
    public function doImportFromRequest($req)
    {
        $setId = $req->getParam('set_id');
        return $this->importBySetId($setId);
    }

    /**
     * @param $setId
     * @return ImportConfig
     * @throws Exception
     */
    protected function getProductImport($setId)
    {
        /** @var Vendor $vendor */
        $vendor = $this->currentVendor();

        $productImportName = sprintf(self::IMPORT_TITLE_TEMPLATE, $vendor->getId(), $setId);

        $productImportModel = $this->importConfigByTitle($productImportName);
        if (!$productImportModel->getId()) {
            // import not found
            throw new Exception(__('Import for %1 set and %2 vendor not found',
                $setId, $vendor->getId()));
        }

        return $productImportModel;
    }

    protected function getCfgProductImport($setId)
    {
        /** @var Vendor $vendor */
        $vendor = $this->currentVendor();

        $productCfgImportName = sprintf(self::IMPORT_CFG_TITLE_TEMPLATE, $vendor->getId(), $setId);
        $productCfgImportModel = $this->importConfigByTitle($productCfgImportName);
        if (!$productCfgImportModel->getId()) {
            // import not found
            throw new Exception(__('Cfg Import for %1 set and %2 vendor not found',
                $setId, $vendor->getId()));
        }

        return $productCfgImportModel;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $req
     * @return array
     * @throws Exception
     */
    public function importStatus($req)
    {
        $helper = $this->dataHelper();
        $setId = $req->getParam('set_id');
        if(!$setId){
            $vendorId           = $this->currentVendor()->getId();
            $productImportModel = $this->getLastImportByVendorId($vendorId);
            $setId = $productImportModel->getAttributeSetId();
        }

        $productImportModel = $this->getProductImport($setId);
        $productCfgImportModel = $this->getCfgProductImport($setId);

        $helper->setConfig($productImportModel);
        $_isRunning = $productImportModel->getRunStatus() === 'running';
        $_percent = $productImportModel->getRowsFound() ? ceil(100 * ($productImportModel->getRowsProcessed() / $productImportModel->getRowsFound())) : 0;
        $_snapshot = strtotime($productImportModel->getSnapshotAt());
        $_started = strtotime($productImportModel->getStartedAt());
        $_runtime = $_snapshot && $_started ? max(1, $_snapshot - $_started) : 0;
        $_rate = $_runtime ? round($productImportModel->getRowsProcessed() / $_runtime, 2) : 0;
        $_estRuntime = $productImportModel->getRowsProcessed() ? floor($_runtime / $productImportModel->getRowsProcessed() * $productImportModel->getRowsFound()) : null;
        $_logTail = (array)$helper->getLogTail();
        $_runtimeString = sprintf('%d:%02d', floor($_runtime / 60), $_runtime % 60);
        $_estRuntimeString = sprintf('%d:%02d', floor($_estRuntime / 60), $_estRuntime % 60);

        $result = [
            'import' => [
                'running' => $_isRunning,
                'dry_run' => $productImportModel->getData('options/import/dryrun') ? true : false,
                'percent' => $_percent,
                'rows_processed' => $productImportModel->getData('rows_processed'),
                'rows_success' => $productImportModel->getData('rows_success'),
                'rows_errors' => $productImportModel->getData('rows_errors'),
                'num_warnings' => $productImportModel->getData('num_warnings'),
                'rate' => $_rate,
                'runtime' => $_runtimeString,
                'estimated_runtime' => $_estRuntimeString,
                'log_tail' => $_logTail
            ],
        ];

        if (isset($productCfgImportModel)) {
        $helper->setConfig($productCfgImportModel);
        $_isRunning = $productCfgImportModel->getRunStatus() === 'running';
        $_percent = $productCfgImportModel->getRowsFound() ? ceil(100 * ($productCfgImportModel->getRowsProcessed() / $productCfgImportModel->getRowsFound())) : 0;
        $_snapshot = strtotime($productCfgImportModel->getSnapshotAt());
        $_started = strtotime($productCfgImportModel->getStartedAt());
        $_runtime = $_snapshot && $_started ? max(1, $_snapshot - $_started) : 0;
        $_rate = $_runtime ? round($productCfgImportModel->getRowsProcessed() / $_runtime, 2) : 0;
        $_estRuntime = $productCfgImportModel->getRowsProcessed() ? floor($_runtime / $productCfgImportModel->getRowsProcessed() * $productCfgImportModel->getRowsFound()) : null;
        $_logTail = (array)$helper->getLogTail();
        $_runtimeString = sprintf('%d:%02d', floor($_runtime / 60), $_runtime % 60);
        $_estRuntimeString = sprintf('%d:%02d', floor($_estRuntime / 60), $_estRuntime % 60);
        $result['import_cfg'] = [
            'running' => $_isRunning,
            'dry_run' => $productCfgImportModel->getData('options/import/dryrun') ? true : false,
            'percent' => $_percent,
            'rows_processed' => $productCfgImportModel->getData('rows_processed'),
            'rows_success' => $productCfgImportModel->getData('rows_success'),
            'rows_errors' => $productCfgImportModel->getData('rows_errors'),
            'num_warnings' => $productCfgImportModel->getData('num_warnings'),
            'rate' => $_rate,
            'runtime' => $_runtimeString,
            'estimated_runtime' => $_estRuntimeString,
            'log_tail' => $_logTail
        ];
        }
        $helper->setConfig($productImportModel);
        $_isRunning = $productImportModel->getRunStatus() === 'running' && $productImportModel->getReindexing();
        $_percent = $productImportModel->getReindexTotal() ? ceil(100 * ($productImportModel->getReindexProcessed() / $productImportModel->getReindexTotal())) : 0;
        $_snapshot = strtotime($productImportModel->getSnapshotAt());
        $_started = strtotime($productImportModel->getStartedAt());
        $_runtime = $_snapshot && $_started ? max(1, $_snapshot - $_started) : 0;
        $_rate = $_runtime ? round($productImportModel->getReindexProcessed() / $_runtime, 2) : 0;
        $_estRuntime = $productImportModel->getReindexProcessed() ? floor($_runtime / $productImportModel->getReindexProcessed() * $productImportModel->getReindexTotal()) : null;
        $_logTail = (array)$helper->getLogTail();
        $_runtimeString = sprintf('%d:%02d', floor($_runtime / 60), $_runtime % 60);
        $_estRuntimeString = sprintf('%d:%02d', floor($_estRuntime / 60), $_estRuntime % 60);

        $result['reindex'] = [
            'running' => $_isRunning,
            'dry_run' => $productImportModel->getData('options/import/dryrun') ? true : false,
            'percent' => $_percent,
            'rows_processed' => $productImportModel->getData('reindex_processed'),
            'rows_success' => $productImportModel->getData('reindex_processed'),
            'rows_errors' => 0,
            'num_warnings' => 0,
            'rate' => $_rate,
            'runtime' => $_runtimeString,
            'estimated_runtime' => $_estRuntimeString,
            'log_tail' => $_logTail
        ];
        return $result;
    }

    public function getLastImportByVendorId($vendorId)
    {
        /** @var ImportConfigRes $resource */
        $resource = $this->_importConfigRes;

        return $resource->findLastImportByVendorId($vendorId);
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $req
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    public function importStop($req)
    {
        $helper = $this->dataHelper();
        $setId = $req->getParam('set_id');

        $productImportModel = $this->getProductImport($setId);
        $helper->stop($productImportModel)->save();
        try {
            $productCfgImportModel = $this->getCfgProductImport($setId);
            $helper->stop($productCfgImportModel)->save();
        } catch (Exception $e) {
            $this->_logger->debug('Cfg import profile not found');
        }

        return 'Import stopped';
    }

    public function getLogFilename($setId)
    {
        $productImportModel = $this->getProductImport($setId);

        return $this->getImportLogFilename($productImportModel);
    }

    /**
     * @param $productImportModel
     * @return string
     */
    public function getImportLogFilename(ImportConfig $productImportModel)
    {
        $helper = $this->dataHelper();
        $helper->setConfig($productImportModel);

        return $helper->getLogFilename();
    }

    public function getLogBaseDir($setId)
    {
        $productImportModel = $this->getProductImport($setId);

        return $this->getImportLogBaseDir($productImportModel);
    }

    /**
     * @param $productImportModel
     * @return mixed
     */
    public function getImportLogBaseDir(ImportConfig $productImportModel)
    {
        $helper = $this->dataHelper();
        $helper->setConfig($productImportModel);

        return $helper->getLogBaseDir();
    }

    public function getCfgLogFilename($setId)
    {
        $productImportModel = $this->getCfgProductImport($setId);

        return $this->getImportLogFilename($productImportModel);
    }

    public function getCfgLogBaseDir($setId)
    {
        $productImportModel = $this->getCfgProductImport($setId);

        return $this->getImportLogBaseDir($productImportModel);
    }

    public function getVendorLogFilename($vendorId)
    {
        $productImportModel = $this->getLastImportByVendorId($vendorId);

        return $this->getImportLogFilename($productImportModel);
    }

    public function getLogBaseDirByVendorId($vendorId)
    {
        $productImportModel = $this->getLastImportByVendorId($vendorId);

        return $this->getImportLogBaseDir($productImportModel);
    }

    /**
     * @param ImportConfig $productImportModel
     * @param ImportConfig $productCfgImportModel
     * @return array
     * @throws \Exception
     */
    public function doImport($productImportModel, $productCfgImportModel)
    {
        $this->dataHelper()->import($productImportModel, $productCfgImportModel);

        return [
            'import' => $productImportModel->getId(),
            'import_cfg' => $productCfgImportModel->getId(),
        ];
    }

    /**
     * @return array
     */
    public function getDefaultProductOptions()
    {
        return $this->_defaultProductOptions;
    }

    /**
     * @param array $defaultProductOptions
     */
    public function setDefaultProductOptions($defaultProductOptions)
    {
        $this->_defaultProductOptions = $defaultProductOptions;

        return $this;
    }

    /**
     * @return DropshipHelperData
     */
    protected function _udHelper()
    {
        if (!$this->udHelper) {
            $this->udHelper = $this->_hlp;
        }

        return $this->udHelper;
    }

    /**
     * @param $setId
     * @return array
     * @throws Exception
     * @throws \Exception
     */
    public function importBySetId($setId)
    {
        $productImportModel = $this->getProductImport($setId);
        if ($productImportModel->getRunStatus() === 'running') {
            return ['error' => __('The import is already running')];
        }
        $productCfgImportModel = $this->getCfgProductImport($setId);

        return $this->doImport($productImportModel, $productCfgImportModel);
    }
}
