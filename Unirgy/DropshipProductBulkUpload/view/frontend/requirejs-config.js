/**
 * Created by pp on 11-19-2015.
 */
var config = {
    map: {
        '*': {
            "dropzone": "Unirgy_DropshipProductBulkUpload/js/dropzone/dropzone",
            "unirgyModal": "Unirgy_DropshipProductBulkUpload/js/modal",
            "unirgySortable": "Unirgy_DropshipProductBulkUpload/js/sortable"
        }
    },
    'shim': {
        "unirgyModal": ["prototype"],
        "unirgySortable": ["prototype"]
    }
};
