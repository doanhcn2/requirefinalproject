<?php

namespace Unirgy\DropshipProductBulkUpload\Block\Vendor;

use Unirgy\DropshipVendorProduct\Block\Vendor\Product;

class Columns extends Product
{
    protected $_uuHiddenFields = ['udtiership_rates','media_gallery'];
    protected $_cachedSetId;
    protected $_cachedIsCfgAllowed;
    public function initSetId($setId)
    {
        $this->_cachedSetId = $setId;
        $this->_cachedIsCfgAllowed = $this->_prodHlp->hasTplConfigurableAttributes(null, $setId);
        return $this;
    }
    protected function _initProduct($productType)
    {
        if (!$this->_cachedSetId) {
            throw new \Exception('Attribute set id not initialized');
        }
        $this->_coreRegistry->unregister('current_product');
        $this->_coreRegistry->unregister('product');
        $this->_form = null;
        $this->_product = $this->_prodHlp->initProductEdit([
            'vendor' => $this->getVendor(),
            'set_id' => $this->_cachedSetId,
            'type_id' => $productType
        ]);
        $this->_coreRegistry->register('current_product', $this->_product);
        $this->_coreRegistry->register('product', $this->_product);
        return $this;
    }
    protected function _isFieldApplicable($prod, $fieldCode, $fsConfig)
    {
        $result = true;
        $ult = @$fsConfig['fields_extra'][$fieldCode]['use_limit_type'];
        $lt = @$fsConfig['fields_extra'][$fieldCode]['limit_type'];
        if (!is_array($lt)) {
            $lt = explode(',', $lt);
        }
        if (strpos($fieldCode, 'udmulti.') === 0
            && !$this->_hlp->isUdmultiActive()
        ) {
            $result = false;
        }
        if (strpos($fieldCode, 'stock_data.') === 0
            && $this->_hlp->isUdmultiActive()
        ) {
            $result = false;
        }
        return $result;
    }

    public function getDefaultColumnsOrder()
    {
        $columnsOrder = [];
        foreach ($this->getColumns() as $column) {
            if ($column['id']!='__ignore_column__') {
                $columnsOrder[] = $column['id'];
            }
        }
        return $columnsOrder;
    }
    public function getDefaultCfgColumnsOrder()
    {
        $columnsOrder = [];
        foreach ($this->getCfgColumns() as $column) {
            if ($column['id']!='__ignore_column__') {
                $columnsOrder[] = $column['id'];
            }
        }
        return $columnsOrder;
    }
    protected $_columns;
    public function getColumns()
    {
        if (null === $this->_columns) {
            $this->_cfgColumns = [];
            $this->_columns = [];
            if ($this->_cachedIsCfgAllowed) {
                $this->_initProduct('configurable');
                $configurableColumns = $this->_getColumns();
                $this->_columns = array_merge($this->_columns, $configurableColumns);
                $cfgColumns = $this->_getCfgColumns();
                $this->_cfgColumns = array_merge($this->_cfgColumns, $cfgColumns);
                $this->_addExtraCfgColumns();
            }
            $this->_initProduct('simple');
            $simpleColumns = $this->_getColumns();
            $this->_columns = array_merge($this->_columns, $simpleColumns);
            $this->_addExtraColumns();
            $this->_initProduct('simple');
            $simpleColumns = $this->_getColumns();
            $this->_columns = array_merge($this->_columns, $simpleColumns);
            $this->_addExtraColumns();
            $maxCount = max(count($this->_columns), count($this->_cfgColumns));
            $__i = $maxCount;
            foreach ($this->_columns as &$c) {
                $c['is_main_column'] = true;
                $c['sort_order'] = $__i--;
            }
            unset($c);
            $__i = $maxCount;
            foreach ($this->_cfgColumns as &$c) {
                $c['is_cfg_column'] = true;
                $c['sort_order'] = $__i--;
            }
            unset($c);
            $this->_columns = array_replace_recursive($this->_columns, $this->_cfgColumns);
            foreach ($this->_columns as &$c) {
                $c['sort_order_rank'] = $this->_getColumnRank($c);
            }
            unset($c);
            uasort($this->_columns, array($this, 'sortColumns'));
        } else {
            $this->_initProduct('simple');
            $simpleColumns = $this->_getColumns();
            $this->_columns = array_merge($this->_columns, $simpleColumns);
            $this->_addExtraColumns();
            $maxCount = count($this->_columns);
            $__i = $maxCount;
            foreach ($this->_columns as &$c) {
                $c['is_main_column'] = true;
                $c['sort_order'] = $__i--;
            }
            unset($c);
            foreach ($this->_columns as &$c) {
                $c['sort_order_rank'] = $this->_getColumnRank($c);
            }
            unset($c);
            uasort($this->_columns, array($this, 'sortColumns'));
        }
        return $this->_columns;
    }

    protected function _getColumnRank($c)
    {
        $rank = 1;
        if ($c['id']=='__parent_sku__') $rank = $rank << 10;
        if ($c['id']=='sku') $rank = $rank << 8;
        if (@$c['is_super_attribute']) $rank = $rank << 6;
        $rank += $c['sort_order'];
        return $rank;
    }
    public function sortColumns($a, $b)
    {
        if ($a['sort_order_rank']>$b['sort_order_rank']) {
            return -1;
        } elseif ($a['sort_order_rank']<$b['sort_order_rank']) {
            return 1;
        }
        return 0;
    }

    protected function _getColumns()
    {
        $form = $this->getForm();
        $columns = [];
        foreach ($form->getElements() as $fieldset) {
            if (0===strpos($fieldset->getId(), 'group_fields')
                || $fieldset->getId()=='group_fields_images'
            ) {
                $elementIds = [];
                $elementContainers = [
                    $fieldset->getTopColumn(),
                    $fieldset->getLeftColumn(),
                    $fieldset->getRightColumn(),
                    $fieldset->getBottomColumn()
                ];
                foreach ($elementContainers as $__elCont) {
                    $elementIds = array_merge($elementIds, $__elCont);
                }
                foreach ($elementIds as $__elId) {
                    if (!in_array($__elId, $this->_uuHiddenFields)) {
                        $__elData = $fieldset->getForm()->getElement($__elId)->getData();
                        if (!array_key_exists('id', $__elData)) {
                            $__elData['id'] = $__elData['name'];
                        }
                        if (strpos($__elData['id'], 'stock_data_') === 0) {
                            $__elData['id'] = str_replace('stock_data_', 'stock.', $__elData['id']);
                        }
                        if (strpos($__elData['id'], 'udmulti_') === 0) {
                            $__elData['id'] = str_replace('udmulti_', 'udmulti.', $__elData['id']);
                        }
                        if ($__elData['id']=='product_categories') {
                            $__elData['type'] = 'select';
                            $__elData['values'] = $this->_helperCatalog->getCategoryValues();
                        }
                        if ($__elData['id']=='product_websites') {
                            $__elData['type'] = 'select';
                            $wsVals = [];
                            foreach ($this->_storeManager->getWebsites() as $website) {
                                $wsVals[] = ['label'=>$website->getCode(), 'value'=>$website->getCode()];
                            }
                            $__elData['values'] = $wsVals;
                        }
                        $this->_prepareColumn($__elData);
                        $columns[$__elData['id']] = $__elData;
                    }
                }
            }
        }
        return $columns;
    }
    protected function _addExtraColumns()
    {
        $columns = $this->_columns;
        $columnsExtra = [];
        if (!array_key_exists('sku', $columns)) {
            $columnsExtra['sku'] = [
                'id' => 'sku',
                'name' => 'sku',
                'label' => 'Sku',
            ];
        }
        $columns = array_merge($columnsExtra, $columns);
        if (!array_key_exists('media_gallery', $columns)) {
            $columns['media_gallery'] = [
                'id' => 'media_gallery',
                'name' => 'media_gallery',
                'label' => 'Media Gallery',
            ];
        }
        $columns['image'] = [
            'id' => 'image',
            'name' => 'image',
            'label' => 'Main Image',
        ];
        /*
        $columns['__ignore_column__'] = [
            'id' => '__ignore_column__',
            'name' => '__ignore_column__',
            'label' => 'Ignore Column',
        ];
        */
        $this->_columns = $columns;
    }
    protected $_cfgColumns;
    public function getCfgColumns()
    {
        if (null === $this->_cfgColumns) {
            $this->_cfgColumns = [];
            if ($this->_cachedIsCfgAllowed) {
                $this->_initProduct('configurable');
                $cfgColumns = $this->_getCfgColumns();
                $this->_cfgColumns = array_merge($this->_cfgColumns, $cfgColumns);
                $this->_addExtraCfgColumns();
            }
        }
        return $this->_cfgColumns;
    }
    protected function _getCfgColumns()
    {
        $cfgEl = $this->getForm()->getElement('_cfg_quick_create');
        /** @var CfgColumns $cfgElRend */
        $cfgElRend = $this->_layout->createBlock('Unirgy\DropshipProductBulkUpload\Block\Vendor\CfgColumns');
        $cfgElRend->setProduct($cfgEl->getProduct());
        $cfgElRend->setCfgAttribute($cfgEl->getCfgAttribute());
        $cfgElRend->setCfgAttributeValue($cfgEl->getCfgAttributeValue());
        $cfgElRend->setCfgAttributeValueTuple($cfgEl->getCfgAttributeValueTuple());
        $cfgElRend->setCfgAttributeLabel($cfgEl->getCfgAttributeLabel());
        $cfgElRend->setElement($cfgEl);
        $cfgColumns = [];
        foreach ($cfgElRend->getConfigurableAttributes() as $cfgAttr) {
            $__elData = $this->_prodHlpForm->getAttributeField($cfgAttr);
            $__elData['is_super_attribute'] = true;
            $__elData['required'] = true;
            $this->_prepareColumn($__elData);
            $cfgColumns[$cfgAttr->getAttributeCode()] = $__elData;
        }
        $columnsForm = $cfgElRend->getColumnsForm();
        foreach ($columnsForm->getElements() as $fieldset) {
            foreach ($fieldset->getElements() as $element) {
                if ($element->getType() != 'fieldset') {
                    $__elData = $element->getData();
                    $__elData['id'] = @$__elData['orig_id'];
                    $__elData['name'] = @$__elData['orig_name'];
                    unset($__elData['orig_id']);
                    unset($__elData['orig_name']);
                    if (strpos($__elData['id'], 'stock_data_') === 0) {
                        $__elData['id'] = str_replace('stock_data_', 'stock.', $__elData['id']);
                    }
                    if (strpos($__elData['id'], 'udmulti_') === 0) {
                        $__elData['id'] = str_replace('udmulti_', 'udmulti.', $__elData['id']);
                    }
                    $this->_prepareColumn($__elData);
                    $cfgColumns[$__elData['id']] = $__elData;
                }
            }
        }
        return $cfgColumns;
    }
    protected function _addExtraCfgColumns()
    {
        $cfgColumns = $this->_cfgColumns;
        $cfgColumnsExtra = [
            '__parent_sku__' => [
                'id' => '__parent_sku__',
                'name' => '__parent_sku__',
                'label' => 'Parent Sku',
                'required' => false,
            ]
        ];
        if (!array_key_exists('sku', $cfgColumns)) {
            $cfgColumnsExtra['sku'] = [
                'id' => 'sku',
                'name' => 'sku',
                'label' => 'Sku',
                'required' => false,
            ];
        }
        $cfgColumns = array_merge($cfgColumnsExtra, $cfgColumns);
        $cfgColumns['media_gallery'] = [
            'id' => 'media_gallery',
            'name' => 'media_gallery',
            'label' => 'Media Gallery',
            'required' => false,
        ];
        $cfgColumns['image'] = [
            'id' => 'image',
            'name' => 'image',
            'label' => 'Main Image',
            'required' => false,
        ];
        /*
        $cfgColumns['__ignore_column__'] = [
            'id' => '__ignore_column__',
            'name' => '__ignore_column__',
            'label' => 'Ignore Column',
            'required' => false,
        ];
        */
        $this->_cfgColumns = $cfgColumns;
        return $this;
    }
    protected function _prepareColumn(&$elData)
    {
        //unset($elData['entity_attribute']);
        unset($elData['product_attribute']);
        unset($elData['container']);
        if (array_key_exists('values', $elData) && is_array($elData)) {
            $newValues = [];
            foreach ($elData['values'] as $__data) {
                if ($__data['value']!=='') {
                    $newValues[] = $__data;
                }
            }
            $elData['values'] = $newValues;
        }
        return $this;
    }
    public function isCfgColumns()
    {
        return 0;
    }
}
