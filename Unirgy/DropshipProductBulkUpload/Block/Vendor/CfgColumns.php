<?php

namespace Unirgy\DropshipProductBulkUpload\Block\Vendor;

use Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer\QuickCreate;

class CfgColumns extends QuickCreate
{
    protected function _uuPrepareOrigField($data)
    {
        if (isset($data['id'])) {
            $data['orig_id'] = $data['id'];
            $data['orig_name'] = $data['name'];
        }
        return $data;
    }
    protected function _getStockItemField($field, $values)
    {
        return $this->_uuPrepareOrigField(parent::_getStockItemField($field, $values));
    }
    protected function _getAttributeField($attribute)
    {
        return $this->_uuPrepareOrigField(parent::_getAttributeField($attribute));
    }
    protected function _getUdmultiField($field, $mvData)
    {
        return $this->_uuPrepareOrigField(parent::_getUdmultiField($field, $mvData));
    }
    public function isCfgColumns()
    {
        return 1;
    }
}