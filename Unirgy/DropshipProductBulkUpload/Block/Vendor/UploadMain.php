<?php

namespace Unirgy\DropshipProductBulkUpload\Block\Vendor;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class UploadMain extends Template
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        Registry $frameworkRegistry, 
        array $data = [])
    {
        $this->_coreRegistry = $frameworkRegistry;
        parent::__construct($context, $data);
    }

    protected $_form;
    protected $_product;
    protected $_oldStoreId;
    protected $_unregUrlStore;

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        if (!$this->_coreRegistry->registry('url_store')) {
            $this->_unregUrlStore = true;
            $this->_coreRegistry->register('url_store', $this->_storeManager->getStore());
        }
        $this->_oldStoreId = $this->_storeManager->getStore()->getId();
        $this->_storeManager->setCurrentStore(0);

        return $this;
    }
    protected function _afterToHtml($html)
    {
        if ($this->_unregUrlStore) {
            $this->_unregUrlStore = false;
            $this->_coreRegistry->unregister('url_store');
        }
        $this->_storeManager->setCurrentStore($this->_oldStoreId);
        return parent::_afterToHtml($html);
    }
    public function getColumnsForm()
    {
        return $this->getChildHtml('udprodupload_columns');
    }
    public function getCfgColumnsForm()
    {
        return $this->getChildHtml('udprodupload_cfg_columns');
    }
}