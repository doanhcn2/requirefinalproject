<?php

namespace Unirgy\DropshipProductBulkUpload\Block\Vendor\UploadForm;

use Magento\Framework\View\Element\Template;

class CfgColumns extends Template
{
    public function isCfgColumns()
    {
        return 1;
    }
}