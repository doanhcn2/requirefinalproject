<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

use Magento\Framework\Exception\LocalizedException;

class PlainLog extends AbstractVendor
{
    public function execute()
    {
        $uploadHelper = $this->_getUploadHelper();

        $vendorId       = $this->_getSession()->getVendorId();
        $import = $uploadHelper->getLastImportByVendorId($vendorId);
        if(!$import->getId()){
            throw new LocalizedException(__('Failed to load last import for vendor id: %1', $vendorId));
        }
        $logFilename = $uploadHelper->getImportLogFilename($import);

        $this->_pipeFile(
            $uploadHelper->getImportLogBaseDir($import) . DIRECTORY_SEPARATOR . $logFilename,
            $logFilename,
            'text/csv'
        );
    }
}
