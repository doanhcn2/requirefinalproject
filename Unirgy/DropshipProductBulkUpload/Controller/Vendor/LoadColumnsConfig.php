<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class LoadColumnsConfig extends AbstractVendor
{
    public function execute()
    {
        $r = $this->getRequest();
        $setId  = $r->getParam('set_id');
        $cfgCollection = $this->_hlp->createObj('Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig\Collection')
            ->addFieldToFilter('vendor_id', $this->_getSession()->getVendorId())
            ->addFieldToFilter('set_id', $setId);
        return $this->returnResult($cfgCollection->toOptionArray());
    }
}
