<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class Index extends AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'udprodupload');
    }
}
