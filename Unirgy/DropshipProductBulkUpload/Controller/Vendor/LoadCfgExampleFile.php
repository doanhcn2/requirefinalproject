<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class LoadCfgExampleFile extends AbstractVendor
{
    public function execute()
    {
        $upHlp = $this->_myProdHlp;
        $setId = $this->_getSession()->getData('udprodupload_current_set_id');
        if (!$setId) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Type of product not selected'));
        }
        $columnsList = $upHlp->getCfgColumns($setId);
        $columnsOrder = $this->_getSession()->getData('udprodupload_cfg_columns_'.$setId);
        if (empty($columnsOrder)) {
            $columnsOrder = $upHlp->getDefaultCfgColumnsOrder($setId);
        }
        $setIdArr = explode('-', $setId, 2);
        $filename = sprintf('import_product_options_example-%s-%s.csv', $setIdArr[1], $upHlp->getAttributeSetName($setIdArr[0]));
        $this->_hlp->sendDownload($filename, $this->_prepareExampleFile($columnsList, $columnsOrder, true), 'text/csv');
    }
}
