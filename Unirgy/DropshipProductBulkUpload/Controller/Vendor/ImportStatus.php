<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;



class ImportStatus extends AbstractVendor
{
    public function execute()
    {
        $result = [];
        try {
            $uploadHelper = $this->_getUploadHelper();
            $result['success'] = true;
            $result['data']      = $uploadHelper->importStatus($this->getRequest());
        } catch(\Exception $e) {
            $result['error']   = true;
            $result['message'] = __('There was a problem with status update');
            $result['details'] = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
        }
        return $this->returnResult($result);
    }
}
