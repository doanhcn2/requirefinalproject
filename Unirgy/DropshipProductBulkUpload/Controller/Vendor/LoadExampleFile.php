<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class LoadExampleFile extends AbstractVendor
{
    public function execute()
    {
        $upHlp = $this->_myProdHlp;
        $setId = $this->_getSession()->getData('udprodupload_current_set_id');
        if (!$setId) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Type of product not selected'));
        }
        $columnsList = $upHlp->getColumns($setId);
        $columnsOrder = $this->_getSession()->getData('udprodupload_columns_'.$setId);
        if (empty($columnsOrder)) {
            $columnsOrder = $upHlp->getDefaultColumnsOrder($setId);
        }
        $setIdArr = explode('-', $setId, 2);
        $filename = sprintf('import_example-%s-%s.csv', $setIdArr[1], $upHlp->getAttributeSetName($setIdArr[0]));
        $exampleData = $this->_prepareExampleFile($columnsList, $columnsOrder, false, false);
        $exampleData .= $this->_prepareExampleFile($columnsList, $columnsOrder, true, false, true);
        $this->_hlp->sendDownload($filename, $exampleData, 'text/csv');
    }
}
