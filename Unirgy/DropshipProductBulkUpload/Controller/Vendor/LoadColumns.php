<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

use Unirgy\DropshipProductBulkUpload\Helper\Upload;
use Unirgy\DropshipProductBulkUpload\Model\ColumnsConfigFactory;

class LoadColumns extends AbstractVendor
{
    public function execute()
    {
        $upHlp = $this->_myProdHlp;
        $r = $this->getRequest();
        $setId  = $r->getParam('set_id');
        $cfgId  = $r->getParam('columns_config_id');
        $r->setParam('id', null);
        $isCfgAllowed = $this->_prodHlp->hasTplConfigurableAttributes(null, $setId);
        if ($isCfgAllowed) {
            $r->setParam('type_id', Upload::PROD_CONFIGURABLE);
        }
        $importOptions = $cfgColumnsOrder = $columnsOrder = [];
        if ($cfgId) {
            $useColumnsConfig = false;
            $columnsConfig = $this->_hlp->createObj('Unirgy\DropshipProductBulkUpload\Model\ColumnsConfig')->load($cfgId);
            if ($columnsConfig->getId()
                && $columnsConfig->getSetId()==$setId
                && $columnsConfig->getVendorId()==$this->_getSession()->getVendorId()
            ) {
                $columnsOrder = $this->_prepareColumnsOrder($columnsConfig->getColumns());
                $cfgColumnsOrder = $this->_prepareColumnsOrder($columnsConfig->getCfgColumns());
                $importOptions = $columnsConfig->getOptions();
                $importOptions = $this->_hlp->unserializeArr($importOptions);
            }
        }
        if (empty($importOptions) || !is_array($importOptions)) {
            $importOptions = $this->_pupHlpUpload->getDefaultProductOptions();
            $importOptions = @$importOptions['options']['import'];
            if (!is_array($importOptions)) {
                $importOptions = [];
            }
        }
        $columnsList = $upHlp->getColumns($setId);
        $cfgColumnsList = [];
        if ($isCfgAllowed) {
            $cfgColumnsList = $upHlp->getCfgColumns($setId);
        }
        if (empty($columnsOrder)) {
            $columnsOrder = $upHlp->getDefaultColumnsOrder($setId);
        }
        if ($isCfgAllowed && empty($cfgColumnsOrder)) {
            $cfgColumnsOrder = $upHlp->getDefaultCfgColumnsOrder($setId);
        }
        return $this->returnResult([
            'columnsOrder' => $columnsOrder,
            'cfgColumnsOrder' => $cfgColumnsOrder,
            'columns' => $columnsList,
            'cfgColumns' => $cfgColumnsList,
            'importOptions' => $importOptions
        ]);
    }
}
