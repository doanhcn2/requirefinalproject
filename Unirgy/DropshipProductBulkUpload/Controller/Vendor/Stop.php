<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;



class Stop extends AbstractVendor
{
    public function execute()
    {
        try {
            $uploadHelper = $this->_getUploadHelper();
            $result['success'] = true;
            $result['data']    = $uploadHelper->importStop($this->getRequest());
        } catch(\Exception $e) {
            $result['error']   = true;
            $result['message'] = __('There was a problem with status update');
            $result['details'] = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
        }
        return $this->returnResult($result);
    }
}
