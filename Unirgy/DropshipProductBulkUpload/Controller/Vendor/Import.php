<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;
use \Magento\Framework\Exception\LocalizedException;

class Import extends AbstractVendor
{
    public function execute()
    {
        $result = [];
        try {
            $setId = $this->getRequest()->getParam('set_id');
            $uploadHelper = $this->_getUploadHelper();
            $defaultProductOptions = $uploadHelper->getDefaultProductOptions();
            $defaultProductOptions['options']['import'] = $this->_myProdHlp->getImportOptions($setId);
            $uploadHelper->setDefaultProductOptions($defaultProductOptions);
            $importResult = $uploadHelper->doImportFromRequest($this->getRequest());
            $result['success'] = true;
            $result['data'] = $importResult;
            if (isset($importResult['error'])) {
                $result['error'] = true;
                $result['message'] = $importResult['error'];
                unset($result['success']);
            }
        } catch(\Exception $e) {
            $result['success'] = false;
            $details = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            if ($e instanceof LocalizedException) {
                $result['message'] = $e->getMessage();
            } else {
                $result['message'] = __('There was a problem saving import data');
            }
            $result['error']   = true;
            $result['details'] = $details;
            $this->_hlp->logError($details);
        }
        return $this->returnResult($result);
    }
}
