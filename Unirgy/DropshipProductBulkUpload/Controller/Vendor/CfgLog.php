<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class CfgLog extends AbstractVendor
{
    public function execute()
    {
        $uploadHelper = $this->_getUploadHelper();

        $setId = $this->getRequest()->getParam('set_id');
        $logFilename = $uploadHelper->getCfgLogFilename($setId);
        $this->_pipeFile(
            $uploadHelper->getCfgLogBaseDir($setId) . DIRECTORY_SEPARATOR . $logFilename,
            $logFilename,
            'text/csv'
        );
    }
}
