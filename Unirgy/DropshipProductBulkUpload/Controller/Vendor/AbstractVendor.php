<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_Dropship
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor as VendorAbstractVendor;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipProductBulkUpload\Helper\Udprod;
use Unirgy\DropshipProductBulkUpload\Helper\Upload;
use Unirgy\DropshipVendorProduct\Helper\Data as DropshipVendorProductHelperData;

abstract class AbstractVendor extends VendorAbstractVendor
{
    /**
     * @var Udprod
     */
    protected $_myProdHlp;

    /**
     * @var Upload
     */
    protected $_pupHlpUpload;

    /**
     * @var DropshipVendorProductHelperData
     */
    protected $_prodHlp;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        StoreManagerInterface $storeManager,
        LayoutFactory $viewLayoutFactory,
        Registry $registry,
        ForwardFactory $resultForwardFactory,
        HelperData $helper,
        PageFactory $resultPageFactory,
        RawFactory $resultRawFactory,
        Header $httpHeader,
        Udprod $helperUdprod,
        Upload $helperUpload,
        DropshipVendorProductHelperData $udprodHlp
    )
    {
        $this->_prodHlp = $udprodHlp;
        $this->_myProdHlp = $helperUdprod;
        $this->_pupHlpUpload = $helperUpload;

        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function returnResult($result)
    {
        return $this->_resultRawFactory->create()->setContents($this->_hlp->jsonEncode($result));
    }

    protected function _prepareColumnsOrder($columns)
    {
        return array_values(array_filter((array)$columns));
    }

    protected function _prepareExampleFile($columnsList, $columnsOrder, $isCfg, $justHeader = false, $skipHeader = false)
    {
        $upHlp = $this->_myProdHlp;
        $now = \Unirgy\DropshipProductBulkUpload\Helper\Data::now();
        $maxOptions = 0;
        foreach ($columnsOrder as $__col) {
            $__colDef = $columnsList[$__col];
            if ($isCfg && !@$__colDef['is_cfg_column']) {
                continue;
            }
            if (!$isCfg && !@$__colDef['is_main_column']) {
                continue;
            }
            if (@$columnsList[$__col]['values']) {
                $maxOptions = max($maxOptions, count($columnsList[$__col]['values']));
            }
        }
        if ($maxOptions == 0) {
            $maxOptions = 10;
        }
        $fp = fopen('php://memory', 'w');
        if (!$skipHeader) {
            $line = array();
            foreach ($columnsOrder as $__col) {
                $line[] = $__col;
            }
            fputcsv($fp, $line);
        }
        $forcedCfgSkip = [];
        $forcedMainSkip = ['__parent_sku__'];
        if (!$justHeader) {
            for ($i = 0; $i < $maxOptions; $i++) {
                $i1 = $i + 1;
                $line = [];
                foreach ($columnsOrder as $__col) {
                    $__colDef = $columnsList[$__col];
                    if ($isCfg && (!@$__colDef['is_cfg_column'] || in_array($__col, $forcedCfgSkip))) {
                        $line[] = '';
                        continue;
                    }
                    if (!$isCfg && (!@$__colDef['is_main_column'] || in_array($__col, $forcedMainSkip))) {
                        $line[] = '';
                        continue;
                    }
                    $colDef = $columnsList[$__col];
                    $isOptions = @$colDef['options'] || @$colDef['values'];
                    if (@$colDef['values']) {
                        $line[] = $colDef['values'][$i % count($colDef['values'])]['label'];
                    } else {
                        if ($__col === Upload::IMAGE_FIELD) {
                            $line[] = sprintf('http://example.com/example_%s_1.jpg', $i1);
                        } elseif ($__col === Upload::MEDIA_GALLERY_FIELD) {
                            $line[] = sprintf('http://example.com/example_%s_1.jpg;http://example.com/example_%s_2.jpg', $i1, $i1);
                        } elseif ($__col === Upload::IGNORE_COLUMN_FIELD) {
                            $line[] = '';
                        } elseif ($__col === Upload::PARENT_SKU_FIELD) {
                            $line[] = strtolower($columnsList[Upload::SKU_FIELD]['label']) . ' 1';
                        } elseif ($__col === Upload::SKU_FIELD && $isCfg) {
                            $line[] = __('child ') . strtolower($colDef['label']) . ' ' . $i1;
                        } elseif ($__col === Upload::SKU_FIELD) {
                            $line[] = strtolower($colDef['label']) . ' ' . $i1;
                        } else {
                            if ($upHlp->isNumericColumn($colDef)) {
                                $line[] = $i1;
                            } elseif ($upHlp->isDateColumn($colDef)) {
                                $line[] = $now;
                            } else {
                                $line[] = $colDef['label'] . ' ' . $i1;
                            }
                        }
                    }
                }
                fputcsv($fp, $line);
            }
        }
        rewind($fp);
        return stream_get_contents($fp);
    }

    /**
     * @return Upload
     */
    protected function _getUploadHelper()
    {
        return $this->_pupHlpUpload;
    }

    protected function _pipeFile($filepath, $filename, $contentType = 'application/octet-stream')
    {
        if (!is_readable($filepath)) {
            header('HTTP/1.1 404 Not Found');
        } else {

            header('HTTP/1.1 200 OK');
            header('Pragma: public');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0', true);
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Last-Modified: ' . date('r'));
            header('Accept-Ranges: bytes');
            header('Content-Length: ' . filesize($filepath));
            header('Content-Type: ', $contentType);

            $from = fopen($filepath, 'rb');
            $to = fopen('php://output', 'wb');

            stream_copy_to_stream($from, $to);
        }
    }
}
