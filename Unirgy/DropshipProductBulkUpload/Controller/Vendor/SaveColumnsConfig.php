<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class SaveColumnsConfig extends AbstractVendor
{
    public function execute()
    {
        $r = $this->getRequest();
        $setId = $r->getParam('set_id');
        $name  = $r->getParam('columns_config_name');
        $cfgId = $r->getParam('columns_config_id');
        $options = $r->getParam('options');
        $columns = $r->getParam('columns');
        unset($columns['$ROW']);
        $cfgColumns = $r->getParam('cfgColumns');
        unset($cfgColumns['$ROW']);
        $columns = $this->_prepareColumnsOrder($columns);
        $cfgColumns = $this->_prepareColumnsOrder($cfgColumns);
        if ($cfgId>0
            && ($columnsConfig = $this->getColumnsConfig()->load($cfgId))
            && $columnsConfig->getId()
            && $columnsConfig->getSetId()==$setId
            && $columnsConfig->getVendorId()==$this->_getSession()->getVendorId()
            && $columnsConfig->getName()==$name
        ) {
            $columnsConfig->setColumns($columns);
            $columnsConfig->setCfgColumns($cfgColumns);
            $columnsConfig->setOptions($this->_hlp->serialize($options));
            $columnsConfig->save();
            return $this->returnResult([
                'label'=>$name,
                'value'=>$cfgId
            ]);
        } else {
            $cfgCollection = $this->getColumnsConfigCollection()
                ->addFieldToFilter('vendor_id', $this->_getSession()->getVendorId())
                ->addFieldToFilter('set_id', $setId)
                ->addFieldToFilter('name', $name);
            if (count($cfgCollection)>0) {
                return $this->returnResult([
                    'error'=>true,
                    'message'=>__('Name already used'),
                ]);
            } else {
                $columnsConfig = $this->getColumnsConfig();
                $columnsConfig->setVendorId($this->_getSession()->getVendorId());
                $columnsConfig->setSetId($setId);
                $columnsConfig->setName($name);
                $columnsConfig->setColumns($columns);
                $columnsConfig->setCfgColumns($cfgColumns);
                $columnsConfig->setOptions($this->_hlp->serialize($options));
                $columnsConfig->save();
                return $this->returnResult([
                    'label'=>$name,
                    'value'=>$columnsConfig->getId(),
                    'is_new'=>true
                ]);
            }
        }
    }

    /**
     * @return \Unirgy\DropshipProductBulkUpload\Model\ColumnsConfig
     */
    protected function getColumnsConfig()
    {
        return $this->_hlp->createObj('\Unirgy\DropshipProductBulkUpload\Model\ColumnsConfig');
    }

    /**
     * @return \Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig\Collection
     */
    protected function getColumnsConfigCollection()
    {
        return $this->_hlp->createObj('Unirgy\DropshipProductBulkUpload\Model\ResourceModel\ColumnsConfig\Collection');
    }

}
