<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

use Magento\Framework\File\Uploader;

class FileUpload extends AbstractVendor
{
    public function execute()
    {
        $field = $this->getRequest()->getParam('file_field');
        $tmpPath = $this->_myProdHlp->getBaseTmpPath();
        $result = [];
        try {
            $uploader = new Uploader($field);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $result = $uploader->save($tmpPath);

            $result['tmp_name'] = str_replace(DIRECTORY_SEPARATOR, "/", $result['tmp_name']);
            $result['path'] = str_replace(DIRECTORY_SEPARATOR, "/", $result['path']);

            $result['cookie'] = [
                'name'     => session_name(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain()
            ];
        } catch (\Exception $e) {
            $result = ['error'=>$e->getMessage(), 'errorcode'=>$e->getCode()];
        }

        return $this->_resultRawFactory->create()->setContents($this->_hlp->jsonEncode($result));
    }
}
