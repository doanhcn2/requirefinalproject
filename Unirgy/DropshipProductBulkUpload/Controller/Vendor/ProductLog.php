<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class ProductLog extends AbstractVendor
{
    public function execute()
    {
        $uploadHelper = $this->_getUploadHelper();
        $setId       = $this->getRequest()->getParam('set_id');
        $logFilename = $uploadHelper->getLogFilename($setId);
        $this->_pipeFile(
            $uploadHelper->getLogBaseDir($setId) . DIRECTORY_SEPARATOR . $logFilename,
            $logFilename,
            'text/csv'
        );
    }
}
