<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class SaveSetId extends AbstractVendor
{
    public function execute()
    {
        $r = $this->getRequest();
        $setId = $r->getParam('set_id');
        $this->_getSession()->setData('udprodupload_current_set_id', $setId);
        $this->returnResult(array(
            'success'=>true,
            'message'=>__('Success'),
        ));
    }
}
