<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;
use \Magento\Framework\Exception\LocalizedException;

class UploadPost extends AbstractVendor
{
    public function execute()
    {
        $result = [];
        try {
            ini_set('auto_detect_line_endings', 1);
            $setId = $this->getRequest()->getParam('set_id');
            $uploadHelper      = $this->_getUploadHelper();
            $result['success'] = true;
            $defaultProductOptions = $uploadHelper->getDefaultProductOptions();
            $defaultProductOptions['options']['import'] = $this->_myProdHlp->getImportOptions($setId);
            $uploadHelper->setDefaultProductOptions($defaultProductOptions);
            $result['data']    = $uploadHelper->uploadAndProcess($this->getRequest());
        } catch(\Exception $e) {
            $result['success'] = false;
            $details = $e->getMessage() . PHP_EOL . $e->getTraceAsString();
            if ($e instanceof LocalizedException) {
                $result['message'] = $e->getMessage();
            } else {
                $result['message'] = __('There was a problem saving import data');
            }
            $result['error']   = true;
            $result['details'] = $details;
            $this->_hlp->logError($details);
            $this->getResponse()->setHttpResponseCode(400);
        }

        return $this->returnResult($result);
    }
}
