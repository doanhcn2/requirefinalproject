<?php

namespace Unirgy\DropshipProductBulkUpload\Controller\Vendor;

class SaveColumns extends AbstractVendor
{
    public function execute()
    {
        $r = $this->getRequest();
        $setId = $r->getParam('set_id');
        $columns = $r->getParam('columns');
        $options = $r->getParam('options');
        unset($columns['$ROW']);
        $cfgColumns = $r->getParam('cfgColumns');
        unset($cfgColumns['$ROW']);
        $columns = $this->_prepareColumnsOrder($columns);
        $cfgColumns = $this->_prepareColumnsOrder($cfgColumns);
        $this->_getSession()->setData('udprodupload_current_set_id', $setId);
        $this->_getSession()->setData('udprodupload_columns_'.$setId, $columns);
        $this->_getSession()->setData('udprodupload_options_'.$setId, $options);
        $this->_getSession()->setData('udprodupload_cfg_columns_'.$setId, $cfgColumns);
        $this->getResponse()->setHeader('content-type', 'application/json');
        return $this->returnResult([
            'success'=>true,
            'message'=>__('Success'),
        ]);
    }
}
