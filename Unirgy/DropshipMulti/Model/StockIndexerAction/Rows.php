<?php

namespace Unirgy\DropshipMulti\Model\StockIndexerAction;

use Magento\Framework\App\ResourceConnection;
use \Magento\Framework\App\ObjectManager;

class Rows extends \Magento\CatalogInventory\Model\Indexer\Stock\Action\Rows
{
    protected function _getTypeIndexers()
    {
        if (empty($this->_indexers)) {
            foreach ($this->_catalogProductType->getTypesByPriority() as $typeId => $typeInfo) {
                $indexerClassName = isset($typeInfo['stock_indexer']) ? $typeInfo['stock_indexer'] : '';

                $indexer = $this->_indexerFactory->create($this->multiHlp()->mapStockIndexer($indexerClassName))
                    ->setTypeId($typeId)
                    ->setIsComposite(!empty($typeInfo['composite']));

                $this->_indexers[$typeId] = $indexer;
            }
        }
        return $this->_indexers;
    }
    /**
     * @return \Unirgy\DropshipMulti\Helper\Data
     */
    protected function multiHlp()
    {
        return ObjectManager::getInstance()->get('Unirgy\DropshipMulti\Helper\Data');
    }
}