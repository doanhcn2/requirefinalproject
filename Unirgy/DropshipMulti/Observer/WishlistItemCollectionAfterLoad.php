<?php

namespace Unirgy\DropshipMulti\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class WishlistItemCollectionAfterLoad extends AbstractObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $pCollection = $observer->getEvent()->getProductCollection();
        $this->attachMultivendorData($pCollection, true);
        foreach ($pCollection as $p) {
            if ($p->getUdmultiBestPrice()) {
                $p->setPrice($p->getUdmultiBestPrice());
                $p->setFinalPrice($p->getUdmultiBestPrice());
            }
        }
    }
}
