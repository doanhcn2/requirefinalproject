<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipSplit
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Unirgy\DropshipMulti\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class ProductFields extends AbstractHelper
{
    protected $_fields = ['vendor_sku'=>0, 'vendor_cost'=>1, 'stock_qty'=>1, 'priority'=>1, 'shipping_price'=>1, 'vendor_price'=>1, 'state'=>0, 'status'=>1, 'vendor_title'=>0, 'avail_state'=>0, 'avail_date'=>0, 'special_price'=>1, 'special_from_date'=>0, 'special_to_date'=>0,'state_descr'=>0, 'backorders'=>1,'freeshipping'=>1];
    public function getFields()
    {
        return array_keys($this->_fields);
    }
    public function getFieldsByKeys()
    {
        return $this->_fields;
    }
    public function isAllowedField($field)
    {
        return array_key_exists($field, $this->_fields);
    }
    public function isNumericField($field)
    {
        return array_key_exists($field, $this->__numericFields);
    }
    public function getNumericFields()
    {
        $this->_initNumericFields();
        return $this->_numericFields;
    }
    protected $__numericFields;
    protected $_numericFields;
    protected function _initNumericFields()
    {
        if (null === $this->__numericFields) {
            foreach ($this->_fields as $field=>$isNum) {
                if ($isNum) $this->__numericFields[$field] = $isNum;
            }
            $this->_numericFields = array_keys($this->__numericFields);
        }
        return $this;
    }
}