<?php

namespace Unirgy\DropshipVendorPromotions\Plugin;

class SalesRuleValueProvider
{
    /** @var \Unirgy\Dropship\Helper\Data */
    protected $_hlp;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper
    )
    {
        $this->_hlp = $udropshipHelper;
    }
    public function afterGetMetadataValues(\Magento\SalesRule\Model\Rule\Metadata\ValueProvider $subject, $result)
    {
        $vendors = $this->_hlp->src()->setPath('allvendors')->toOptionArray();
        array_unshift($vendors, ['label'=>(string)__('* All Vendors'),'value'=>'0']);
        $result['rule_information']['children']['udropship_vendor']['arguments']['data']['config']['options'] = $vendors;
        return $result;
    }
}