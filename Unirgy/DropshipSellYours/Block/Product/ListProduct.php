<?php

namespace Unirgy\DropshipSellYours\Block\Product;

use Magento\Catalog\Block\Product\ListProduct as ProductListProduct;

class ListProduct extends ProductListProduct
{
    protected function _prepareLayout()
    {
        if ($this->_coreRegistry->registry('current_category')) {
            $this->setCategoryId($this->_coreRegistry->registry('current_category')->getId());
        }
        return parent::_prepareLayout();
    }
    protected function _getProductCollection()
    {
        $collection = parent::_getProductCollection();
        $collection->setFlag('has_stock_status_filter', 1);
        return $collection;
    }
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);

        if (!method_exists($this, 'initializeProductCollection')) {
            $this->_eventManager->dispatch(
                'catalog_block_product_list_collection',
                ['collection' => $this->_getProductCollection()]
            );
        }

        $this->_getProductCollection()->load();

        return \Magento\Framework\View\Element\AbstractBlock::_beforeToHtml();
    }
}