<?php

namespace  Unirgy\DropshipVendorImport\Model\Data\Form\Element;

class ImportFile extends \Magento\Framework\Data\Form\Element\File
{
    public function getHtmlId()
    {
        return parent::getHtmlId().'__';
    }
    public function getName()
    {
        return str_replace([']','['], '_', parent::getName());
    }
    public function getElementHtml()
    {
        $html = parent::getElementHtml();
        $html .= '<input id="' . parent::getHtmlId() . '" name="' . parent::getName() . '" type="hidden" value="1" />';
        return $html;
    }
}