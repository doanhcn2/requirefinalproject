/**
 * Created by pp on 11-19-2015.
 */
var config = {
    map: {
        '*': {
            "unirgyModal": "Unirgy_Dropship/js/modal",
            "calendar": "mage/calendar"
        }
    },
    'shim': {
        "unirgyModal": ["prototype"],
        "varien/form": ["prototype"]
    }
};
