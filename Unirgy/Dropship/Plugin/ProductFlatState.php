<?php
namespace Unirgy\Dropship\Plugin;

class ProductFlatState
{
    public function afterIsFlatEnabled(\Magento\Catalog\Model\Indexer\Product\Flat\State $productFlatState, $result)
    {
        if ($this->isInstalled() && $this->_hlpCat()->getProductFlatDisabled()) {
            $result = false;
        }
        return $result;
    }

    /**
     * @return \Unirgy\Dropship\Helper\Catalog
     */
    protected function _hlpCat()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('\Unirgy\Dropship\Helper\Catalog');
    }

    private function isInstalled()
    {
        /** @var \Magento\Framework\App\DeploymentConfig $deploymentConfig */
        $deploymentConfig = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\App\DeploymentConfig::class);
        return $deploymentConfig->isAvailable();
    }

}
