<?php

namespace Unirgy\DropshipMultiPrice\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\App\ObjectManager;

class UpgradeSchema implements UpgradeSchemaInterface
{
    protected $_hlp;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper
    ) {
        $this->_hlp = $udropshipHelper;
    }
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();
        if (version_compare($context->getVersion(), '3.1.19', '<')) {

            $canStates = ObjectManager::getInstance()->get('\Unirgy\DropshipMultiPrice\Model\Source')
                ->setPath('vendor_product_state_canonic')
                ->toOptionHash();
            $tablesList = [];
            if ($this->_hlp->hasMageFeature('index_price_replica')) {
                $tablesList[] = $setup->getTable('catalog_product_index_price_replica');
            }
            foreach ($canStates as $csKey=>$csLbl) {
                foreach ($tablesList as $tbl) {
                    $connection->addColumn($tbl, 'udmp_'.$csKey.'_min_price', 'decimal(12,4)');
                    $connection->addColumn($tbl, 'udmp_'.$csKey.'_max_price', 'decimal(12,4)');
                    $connection->addColumn($tbl, 'udmp_'.$csKey.'_cnt', 'int(10)');
                }
            }
        }

        $setup->endSetup();
    }
}
