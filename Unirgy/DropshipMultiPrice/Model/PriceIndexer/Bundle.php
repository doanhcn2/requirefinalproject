<?php

namespace {
    /** @var \Unirgy\Dropship\Helper\Data $hlp */
    $hlp = \Magento\Framework\App\ObjectManager::getInstance()->get('\Unirgy\Dropship\Helper\Data');
    if ($hlp->compareMageVer('2.1.0',null,'<')) {
        include_once __DIR__ . "/Bundle20.php";
    } elseif ($hlp->compareMageVer('2.2.0',null,'<')) {
        include_once __DIR__ . "/Bundle21.php";
    } else {
        include_once __DIR__ . "/Bundle22.php";
    }
}