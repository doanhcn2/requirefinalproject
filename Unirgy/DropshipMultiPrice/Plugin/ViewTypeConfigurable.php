<?php

namespace Unirgy\DropshipMultiPrice\Plugin;

class ViewTypeConfigurable
{
    /** @var \Unirgy\Dropship\Helper\Data */
    protected $_hlp;
    /** @var \Unirgy\Dropship\Helper\Item */
    protected $_iHlp;
    protected $_multiHlp;
    protected $_mpHlp;
    protected $localeFormat;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\Dropship\Helper\Item $udropshipItemHelper,
        \Unirgy\DropshipMulti\Helper\Data $multiHlp,
        \Unirgy\DropshipMultiPrice\Helper\Data $mpHlp,
        \Magento\Framework\Locale\Format $localeFormat
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->_iHlp = $udropshipItemHelper;
        $this->_multiHlp = $multiHlp;
        $this->_mpHlp = $mpHlp;
        $this->localeFormat = $localeFormat;
    }
    public function afterGetJsonConfig(\Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject, $result)
    {
        $result = $this->_hlp->unserialize($result);
        $simpleProducts = $subject->getAllowProducts();
        $this->_hlp->udmultiHlp()->attachMultivendorData($simpleProducts, true);
        $minSimpleProductPrice = PHP_INT_MAX;
        $minSimpleInfo = [];
        foreach ($simpleProducts as $product) {
            $mvData = $product->getMultiVendorData();
            $bestVendor = $product->getUdmultiBestVendor();
            if (is_array($mvData)) {
                foreach ($mvData as $vp) {
                    if ($vp['vendor_id'] == $bestVendor) {
                        $product->setFinalPrice(null);
                        $product->reloadPriceInfo();
                        $this->_mpHlp->useVendorPrice($product, $vp);
                        $product->getFinalPrice();
                    }
                }
            }
            $tierPrices = [];
            $priceInfo = $product->getPriceInfo();
            $tierPriceModel =  $priceInfo->getPrice('tier_price');
            $tierPricesList = $tierPriceModel->getTierPriceList();
            foreach ($tierPricesList as $tierPrice) {
                $tierPrices[] = [
                    'qty' => $this->localeFormat->getNumber($tierPrice['price_qty']),
                    'price' => $this->localeFormat->getNumber($tierPrice['price']->getValue()),
                    'percentage' => $this->localeFormat->getNumber(
                        $tierPriceModel->getSavePercent($tierPrice['price'])
                    ),
                ];
            }
            $regularPrice = $product->getPriceInfo()->getPrice('regular_price');
            $finalPrice = $product->getPriceInfo()->getPrice('final_price');
            if ($product->getUdmultiBestPrice()<$minSimpleProductPrice) {
                $minSimpleProductPrice = $product->getUdmultiBestPrice();
                $minSimpleInfo = [
                    'product' => $product,
                    'oldPrice' => [
                        'amount' => $this->localeFormat->getNumber($regularPrice->getAmount()->getValue()),
                    ],
                    'basePrice' => [
                        'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getBaseAmount()),
                    ],
                    'finalPrice' => [
                        'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getValue()),
                    ],
                ];
            }

            $result['optionPrices'][$product->getId()] =
                [
                    'oldPrice' => [
                        'amount' => $this->localeFormat->getNumber(
                            $priceInfo->getPrice('regular_price')->getAmount()->getValue()
                        ),
                    ],
                    'basePrice' => [
                        'amount' => $this->localeFormat->getNumber(
                            $priceInfo->getPrice('final_price')->getAmount()->getBaseAmount()
                        ),
                    ],
                    'finalPrice' => [
                        'amount' => $this->localeFormat->getNumber(
                            $priceInfo->getPrice('final_price')->getAmount()->getValue()
                        ),
                    ],
                    'tierPrices' => $tierPrices,
                 ];
        }
        if ($minSimpleInfo) {
            $result['prices']['oldPrice'] = $minSimpleInfo['oldPrice'];
            $result['prices']['basePrice'] = $minSimpleInfo['basePrice'];
            $result['prices']['finalPrice'] = $minSimpleInfo['finalPrice'];
        }
        $result = $this->_hlp->serialize($result);
        return $result;
    }
}