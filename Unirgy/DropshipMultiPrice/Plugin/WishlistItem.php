<?php

namespace Unirgy\DropshipMultiPrice\Plugin;

class WishlistItem
{
    /** @var \Unirgy\Dropship\Helper\Data */
    protected $_hlp;
    /** @var \Unirgy\Dropship\Helper\Item */
    protected $_iHlp;
    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\Dropship\Helper\Item $udropshipItemHelper
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->_iHlp = $udropshipItemHelper;
    }
    public function afterGetProduct(\Magento\Wishlist\Model\Item $subject, $result)
    {
        if ($result->getUdmultiBestPrice()) {
            $result->setPrice($result->getUdmultiBestPrice());
        }
        return $result;
    }
}