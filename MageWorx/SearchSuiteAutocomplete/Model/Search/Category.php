<?php

namespace MageWorx\SearchSuiteAutocomplete\Model\Search;

use \MageWorx\SearchSuiteAutocomplete\Helper\Data as HelperData;
use \Magento\Search\Helper\Data as SearchHelper;
use \Magento\Search\Model\AutocompleteInterface;
use \MageWorx\SearchSuiteAutocomplete\Model\Source\AutocompleteFields;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Search\Model\QueryFactory;

/**
 * Suggested model. Return suggested data used in search autocomplete
 */
class Category implements \MageWorx\SearchSuiteAutocomplete\Model\SearchInterface
{
    /**
     * @var \MageWorx\SearchSuiteAutocomplete\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\Search\Helper\Data
     */
    protected $searchHelper;

    /**
     * @var \Magento\Search\Model\AutocompleteInterface;
     */
    protected $autocomplete;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory;
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Search\Model\QueryFactory
     */
    private $queryFactory;

    /**
     * Suggested constructor.
     *
     * @param HelperData $helperData
     * @param SearchHelper $searchHelper
     * @param AutocompleteInterface $autocomplete
     * @param CategoryFactory $categoryFactory
     * @param QueryFactory $queryFactory
     */
    public function __construct(
        HelperData $helperData,
        SearchHelper $searchHelper,
        AutocompleteInterface $autocomplete,
        CategoryFactory $categoryFactory,
        QueryFactory $queryFactory
    ) {

        $this->helperData = $helperData;
        $this->searchHelper = $searchHelper;
        $this->autocomplete = $autocomplete;
        $this->categoryFactory = $categoryFactory;
        $this->queryFactory = $queryFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseData()
    {
        $responseData['code'] = AutocompleteFields::CATEGORY;
        $responseData['data'] = [];

        if (!$this->canAddToResult()) {
            return $responseData;
        }

        $categoryCollection = $this->categoryFactory->create()->getCollection();
        $queryText = $this->queryFactory->get()->getQueryText();
        $categoryCollection->addFieldToFilter('name', ['like' => '%'.$queryText.'%']);

        $suggestResultNumber = $this->helperData->getSuggestedResultNumber();

        $autocompleteData = $categoryCollection->getItems();
        $autocompleteData = array_slice($autocompleteData, 0, $suggestResultNumber);
        /** @var \Magento\Catalog\Model\Category $item */
        foreach ($autocompleteData as $item) {
            $item->getUrl();
            $data = $item->toArray();
            $productCollection = $item->getProductCollection();
            $data['child_number'] = $productCollection->count();
            $responseData['data'][] = $data;
        }

        return $responseData;
    }

    /**
     * {@inheritdoc}
     */
    public function canAddToResult()
    {
        return in_array(AutocompleteFields::CATEGORY, $this->helperData->getAutocompleteFieldsAsArray());
    }
}
