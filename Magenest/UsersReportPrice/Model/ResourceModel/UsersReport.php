<?php

namespace Magenest\UsersReportPrice\Model\ResourceModel;

class UsersReport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_users_report_price', 'id');
    }
}
