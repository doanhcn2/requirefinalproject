<?php

namespace Magenest\UsersReportPrice\Model\ResourceModel\UsersReport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            'Magenest\UsersReportPrice\Model\UsersReport',
            'Magenest\UsersReportPrice\Model\ResourceModel\UsersReport'
        );
    }
}
