<?php

namespace Magenest\UsersReportPrice\Model;

use Magento\Framework\Model\AbstractModel;

class UsersReport extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\UsersReportPrice\Model\ResourceModel\UsersReport');
    }
}
