<?php

namespace Magenest\UsersReportPrice\Block\Product\Price;

use \Magento\Customer\Model\SessionFactory;
use \Magento\Catalog\Block\Product\View\AbstractView;
use \Magento\Framework\Stdlib\ArrayUtils;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Catalog\Model\Product;

class Report extends AbstractView
{

    private $sessionFactory;

    public function __construct(
        Context $context,
        SessionFactory $sessionFactory,
        ArrayUtils $arrayUtils,
        array $data = []
    )
    {
        $this->sessionFactory = $sessionFactory;
        parent::__construct($context, $arrayUtils, $data);
    }

    public function getProductId()
    {
        $product = $this->getProduct();
        if ($product instanceof Product) {
            return [
                'name' => $product->getName(),
                'id' => $product->getEntityId()
            ];
        }
        return [];
    }
}