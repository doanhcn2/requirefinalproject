<?php

namespace Magenest\UsersReportPrice\Plugin\CustomerData;

use    Magento\Customer\CustomerData\Customer;
use Magento\Customer\Helper\Session\CurrentCustomer;

class addCustomerInfo
{
    private $currentCustomer;

    public function __construct(
        CurrentCustomer $currentCustomer
    )
    {
        $this->currentCustomer = $currentCustomer;
    }

    public function afterGetSectionData(Customer $subject, $result)
    {
        if (!$this->currentCustomer->getCustomerId()) {
            return [];
        }
        $result['email'] = $this->currentCustomer->getCustomer()->getEmail();
        return $result;
    }
}