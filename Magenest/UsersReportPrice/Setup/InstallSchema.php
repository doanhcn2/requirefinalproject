<?php

namespace Magenest\UsersReportPrice\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package Magenest\UsersReportPrice\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * Table prefix
     */
    const MODULE_TABLE_PREFIX = 'magenest_';


    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createUsersReportPriceTable($installer);

        $installer->endSetup();
    }

    /**
     * Create the table magenest_users_report_price
     *
     * @param SetupInterface $installer
     * @throws
     */
    private function createUsersReportPriceTable(SetupInterface $installer)
    {
        $tableName = self::MODULE_TABLE_PREFIX . 'users_report_price';
        if ($installer->tableExists($tableName)) {
            $installer->getConnection()->dropTable($tableName);
        }

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
                'unsigned' => true
            ],
            'Product Id'
        )->addColumn(
            'product_name',
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Product Name'
        )->addColumn(
            'customer_name',
            Table::TYPE_TEXT,
            50,
            [
                'nullable' => true
            ],
            'Customer Name'
        )->addColumn(
            'customer_email',
            Table::TYPE_TEXT,
            80,
            [
                'nullable' => true
            ],
            'Customer Email'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT
            ],
            'Created at'
        )->addColumn(
            'type',
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => false
            ],
            'Type of store'
        )->addColumn(
            'price',
            Table::TYPE_FLOAT,
            10,
            [
                'nullable' => true,
                'unsigned' => true
            ],
            'Price of Product'
        )->addColumn(
            'comment',
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => false
            ],
            'Comment of Customer'
        )->addColumn(
            'url',
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => false
            ],
            'URL of onsite'
        )->addColumn(
            'address',
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => false
            ]
        )->setComment(
            'Sync Log Table'
        );

        $installer->getConnection()->createTable($table);
    }
}
