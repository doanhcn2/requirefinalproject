/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Customer/js/customer-data',
        'Magento_Ui/js/modal/alert',
        'Magento_Ui/js/modal/modal',
        'mage/mage'
    ], function ($, ko, Component, customerData, alert, modal) {
        'use strict';
        return Component.extend({
            defaults: {},

            popup: null,
            price: ko.observable(0),
            onSite: ko.observable(false),
            localStore: ko.observable(false),
            url: ko.observable(''),
            address: ko.observable(''),
            comment: ko.observable(''),
            customer: customerData.get('customer'),
            fullName: ko.observable(''),
            email: ko.observable(''),

            initialize: function () {
                this._super();
                this.submitPriceReport();
                return this;
            },

            initObservable: function () {
                var self = this;
                this._super();
                if (this.customer()) {
                    this.fullName(this.customer()['fullname']);
                    this.email(this.customer()['email']);
                }

                this.customer.subscribe(function (value) {
                    self.fullName(value['fullname']);
                    self.email(value['email']);
                });

                return this;
            },

            initPopupModal: function () {
                var self = this;
                var dataForm = $('#popup-modal-price-report');
                dataForm.mage('validation', {});
                var options = {
                    type: 'popup',
                    responsive: true,
                    modalClass: 'report-price-modal',
                    innerScroll: true,
                    buttons: [{
                        text: 'Cancel',
                        class: 'cancel-button',
                        click: function () {
                            this.closeModal();
                        }
                    },{
                        text: 'Submit',
                        class: 'submit-button',
                        click: function () {
                            if (dataForm.validation('isValid')) {
                                self.submitReport(this.closeModal());
                            }
                        }
                    }]
                };
                this.popup = modal(options, $('#popup-modal-price-report'));
            },

            submitReport: function (action) {
                var self = this;
                var type = 0;
                var result = false;
                if (self.onSite() && self.localStore()) {
                    type = 2;
                } else if (self.onSite()) {
                    type = 1;
                } else {
                    type = 0;
                }
                $.ajax({
                    url: self.base_url + 'usersreportprice/product/pricereport',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'product_id': self.product['id'],
                        'product_name': self.product['name'],
                        'customer_name': self.fullName(),
                        'customer_email': self.email(),
                        'type': type,
                        'url': self.url(),
                        'address': self.address(),
                        'price': self.price(),
                        'comment': self.comment()
                    },
                    showLoader: true,
                    success: function (r) {
                        if (!r.error) {
                            // action();
                            $('.modal-backdrop').hide();
                            self.resetVariables();
                        }
                        alert({
                            'title': '',
                            'content': r.msg
                        });
                    },
                    error: function (r) {
                        alert({
                            'title': 'Error',
                            'content': 'Something wrong happens! Please try again later! Thank you.'
                        });
                        result = false;
                    }
                });
            },

            popupReportModal: function () {
                this.popup.openModal();
            },

            submitPriceReport: function () {
                var self = this;
                $('#price-report-button-let-us-know').click(function (e) {
                    $('#price-report-button').click();
                });
            },

            resetVariables: function () {
                this.price(0);
                this.onSite(false);
                this.localStore(false);
                this.comment('');
                this.url('');
                this.address('');
            }
        });
    }
);