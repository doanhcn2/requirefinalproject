<?php
namespace Magenest\UsersReportPrice\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\UsersReportPrice\Model\UsersReportFactory;
use Magenest\UsersReportPrice\Model\ResourceModel\UsersReport\CollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

/**
 * Class MassDelete
 * @package Magenest\UsersReportPrice\Controller\Adminhtml\Index
 */
class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var UsersReportFactory
     */
    protected $usersReportFactory;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param UsersReportFactory $usersReportFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        UsersReportFactory $usersReportFactory
    ) {
        $this->usersReportFactory = $usersReportFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $reportModel = $this->usersReportFactory->create();
            $ids = [];
            $count = 0;
            $affectedRows = 0;
            $lastItemId = $collection->getLastItem()->getId();
            foreach ($collection as $item) {
                $ids[] = $item->getId();
                $count++;
                if ($count >= 5000 || $item->getId() == $lastItemId) {
                    $idsString = implode(',', $ids);
                    $affectedRows += $reportModel->getResource()->getConnection()->delete($reportModel->getResource()->getMainTable(), 'id IN ('.$idsString.')');
                    $count = 0;
                    $ids = [];
                }
            }

            $this->messageManager->addSuccess(__('Total of %1 record(s) were deleted.'), $affectedRows);
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_UsersReportPrice::usersreportprice');
    }
}
