<?php

namespace Magenest\UsersReportPrice\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\UsersReportPrice\Model\UsersReportFactory;

class PriceReport extends Action
{
    private $userReportFactory;

    public function __construct(
        Context $context,
        UsersReportFactory $usersReportFactory
    )
    {
        $this->userReportFactory = $usersReportFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return object $response
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getPostValue();

            $userReport = $this->userReportFactory->create();
            $userReport->setData($params);
            $userReport->save();
            $result = [
                'error' => false,
                'msg' => "Thank you for reporting this to us"
            ];
        } catch (\Exception $e) {
            $result = ['msg' => $e->getMessage(), 'error' => true, 'errorcode' => $e->getCode()];
        }

        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }
}