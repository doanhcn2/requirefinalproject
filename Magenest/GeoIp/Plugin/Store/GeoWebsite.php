<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Plugin\Store;

use Magento\Store\Model\Website;
use Magento\Store\Model\Store;

class GeoWebsite
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher
     */
    protected $storeSwitcher;

    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     * @param \Magenest\GeoIp\Model\GeoStore\Switcher $storeSwitcher
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        \Magenest\GeoIp\Model\Config\General $generalConfig,
        \Magenest\GeoIp\Model\GeoStore\Switcher $storeSwitcher,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository
    ) {
        $this->generalConfig = $generalConfig;
        $this->storeSwitcher = $storeSwitcher;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @param \Magento\Store\Model\Website $subject
     * @param \Magento\Store\Model\Store $store
     * @return \Magento\Store\Model\Store
     */
    public function afterGetDefaultStore(Website $subject, Store $store)
    {
        if ($this->generalConfig->isAvailable() && $this->storeSwitcher->isInitialized()) {
            $storeId = $this->storeSwitcher->getStoreId();
            if ($store->getId() != $storeId && in_array($storeId, $subject->getStoreIds())) {
                $store = $this->storeRepository->getById($storeId);
            }
        }

        return $store;
    }
}
