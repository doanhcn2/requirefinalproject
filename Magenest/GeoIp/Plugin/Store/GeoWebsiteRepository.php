<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Plugin\Store;

use Magento\Store\Model\WebsiteRepository;
use Magento\Store\Model\Website;
use Magenest\GeoIp\Model\Config\General;
use Magenest\GeoIp\Model\GeoWebsite;
use Magenest\GeoIp\Model\GeoStore\Switcher;

/**
 * Class GeoWebsiteRepository
 * @package Magenest\GeoIp\Plugin\Store
 */
class GeoWebsiteRepository
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @var \Magenest\GeoIp\Model\GeoWebsite
     */
    protected $geoWebsite;

    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher
     */
    protected $storeSwitcher;

    /**
     * GeoWebsiteRepository constructor.
     * @param General $generalConfig
     * @param GeoWebsite $geoWebsite
     * @param Switcher $storeSwitcher
     */
    public function __construct(
        General $generalConfig,
        GeoWebsite $geoWebsite,
        Switcher $storeSwitcher
    ) {
        $this->generalConfig = $generalConfig;
        $this->geoWebsite = $geoWebsite;
        $this->storeSwitcher = $storeSwitcher;
    }

    /**
     * @return bool
     */
    protected function isNeededProcess()
    {
        return $this->storeSwitcher->isInitialized()
            && $this->generalConfig->isAvailable()
            && $this->geoWebsite->getId() !== false;
    }

    /**
     * @param \Magento\Store\Model\WebsiteRepository $subject
     * @param \Magento\Store\Model\Website $website
     * @return \Magento\Store\Model\Website
     */
    public function afterGet(WebsiteRepository $subject, Website $website)
    {
        if ($this->isNeededProcess()) {
            $website->setIsDefault($this->geoWebsite->getId() == $website->getId());
        }

        return $website;
    }

    /**
     * @param WebsiteRepository $subject
     * @param \Magento\Store\Model\Website[] $websiteList
     * @return \Magento\Store\Model\Website[]
     */
    public function afterGetList(WebsiteRepository $subject, array $websiteList)
    {
        if ($this->isNeededProcess()) {
            foreach ($websiteList as $website) {
                $website->setIsDefault($this->geoWebsite->getId() == $website->getId());
            }
        }

        return $websiteList;
    }

    /**
     * @param \Magento\Store\Model\WebsiteRepository $subject
     * @param \Magento\Store\Model\Website $website
     * @return \Magento\Store\Model\Website
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetDefault(WebsiteRepository $subject, Website $website)
    {
        if (!$this->isNeededProcess()) {
            return $website;
        }

        if ($this->geoWebsite->getId() != $website->getId()) {
            $website = $subject->getById($this->geoWebsite->getId());
        }

        return $website;
    }
}
