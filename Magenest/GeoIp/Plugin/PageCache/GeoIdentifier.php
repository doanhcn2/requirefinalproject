<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Plugin\PageCache;

class GeoIdentifier
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher
     */
    protected $storeSwitcher;

    /**
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     * @param \Magenest\GeoIp\Model\GeoStore\Switcher $storeSwitcher
     */
    public function __construct(
        \Magenest\GeoIp\Model\Config\General $generalConfig,
        \Magenest\GeoIp\Model\GeoStore\Switcher $storeSwitcher
    ) {
        $this->generalConfig = $generalConfig;
        $this->storeSwitcher = $storeSwitcher;
    }

    /**
     * Adds a theme key to identifier for a built-in cache if user-agent theme rule is actual
     *
     * @param \Magento\Framework\App\PageCache\Identifier $identifier
     * @param string $result
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetValue(\Magento\Framework\App\PageCache\Identifier $identifier, $result)
    {
        if ($this->generalConfig->isAvailable() && $this->storeSwitcher->getStoreId()) {
            $result .= $this->storeSwitcher->getStoreId();
        }
        return $result;
    }
}
