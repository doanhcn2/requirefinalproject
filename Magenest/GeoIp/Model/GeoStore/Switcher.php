<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore;

use Magenest\GeoIp\Model;
use Psr\Log\LoggerInterface as Logger;
use Magenest\GeoIp\Helper\IPHandle\DetectCountry;
use Magenest\GeoIp\Helper\IPHandle;

class Switcher
{
    /**
     * @var DetectCountry
     */
    protected $_detectCountry;

    /**
     * @var $ipHelper IPHandle\IpHelper
     */
    protected $ipHelper;
    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher\RuleInterface
     */
    protected $rule;

    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher\PermanentRuleInterface
     */
    protected $permanentRule;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var int|bool
     */
    protected $storeId = false;

    /**
     * @var bool
     */
    protected $isInitialized = false;

    /**
     * Switcher constructor.
     * @param Switcher\RuleInterface $rule
     * @param Switcher\PermanentRuleInterface $permanentRule
     * @param Logger $logger
     * @param IPHandle\IpHelper $ipHelper
     * @param DetectCountry $detectCountry
     */
    public function __construct(
        Model\GeoStore\Switcher\RuleInterface $rule,
        Model\GeoStore\Switcher\PermanentRuleInterface $permanentRule,
        Logger $logger,
        IPHandle\IpHelper $ipHelper,
        DetectCountry $detectCountry
    ) {
        $this->_detectCountry = $detectCountry;
        $this->ipHelper = $ipHelper;
        $this->rule = $rule;
        $this->permanentRule = $permanentRule;
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function isInitialized()
    {
        return $this->isInitialized;
    }

    /**
     * @return bool|int
     */
    public function getStoreId()
    {
        if (!$this->isInitialized) {
            $hasStoreDefaultId = false;
            $countryInformation = $this->_detectCountry->detect();
            $countryCode = $countryInformation['country_id'];
            $hasMandatory = $this->ipHelper->isHasMandatoryStoreViewConfig($countryCode);
            if ($hasMandatory !== false) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $stores = $objectManager->create('Magento\Store\Model\StoreManagerInterface')->getStores();
                foreach ($stores as $store) {
                    if ($store->getCode() === $hasMandatory) {
                        $this->storeId = $store->getStoreId();
                        $this->storeId = $this->permanentRule->updateStoreId($this->storeId, $countryCode);
                        $hasStoreDefaultId = true;
                        break;
                    }
                }
            }
            if (!$hasStoreDefaultId) {
                try {
                    $this->storeId = $this->rule->getStoreId($countryCode);
                    $this->storeId = $this->permanentRule->updateStoreId($this->storeId, $countryCode);
                } catch (\Exception $e) {
                    $this->logger->critical($e);
                }
            }
            $this->isInitialized = true;
        }

        return $this->storeId;
    }
}
