<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher;

interface PermanentRuleInterface
{
    /**
     * @param int|bool $storeId
     * @param string $countryCode
     * @return int|bool
     */
    public function updateStoreId($storeId, $countryCode);
}
