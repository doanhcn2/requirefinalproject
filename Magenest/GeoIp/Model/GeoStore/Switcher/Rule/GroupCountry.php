<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher\Rule;

use Magenest\GeoIp\Model;

/**
 * Class GroupCountry
 * @package Magenest\GeoIp\Model\GeoStore\Switcher\Rule
 */
class GroupCountry implements Model\GeoStore\Switcher\RuleInterface
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     */
    public function __construct(
        Model\Config\General $generalConfig
    ) {
        $this->generalConfig = $generalConfig;
    }

    /**
     * @param string $countryCode
     * @return int|bool
     */
    public function getStoreId($countryCode)
    {
        $group = $this->getGroup($countryCode);
        return $group ? $this->generalConfig->getGroupStore($group) : false;
    }

    /**
     * @param string $countryCode
     * @return bool|int
     */
    protected function getGroup($countryCode)
    {
        $groupCount = $this->generalConfig->getGroupCount();
        for ($group = 1; $group <= $groupCount; $group++) {
            if (in_array($countryCode, $this->generalConfig->getGroupCountryList($group))) {
                return $group;
            }
        }

        return false;
    }
}
