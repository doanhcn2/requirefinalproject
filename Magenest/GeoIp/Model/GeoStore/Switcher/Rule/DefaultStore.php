<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher\Rule;

use Magenest\GeoIp\Model;

/**
 * Class DefaultStore
 * @package Magenest\GeoIp\Model\GeoStore\Switcher\Rule
 */
class DefaultStore implements Model\GeoStore\Switcher\RuleInterface
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     */
    public function __construct(
        Model\Config\General $generalConfig
    ) {
        $this->generalConfig = $generalConfig;
    }

    /**
     * @param string $countryCode
     * @return int|bool
     */
    public function getStoreId($countryCode)
    {
        return $this->generalConfig->getDefaultStore();
    }
}
