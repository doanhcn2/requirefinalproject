<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher;

/**
 * Interface RuleInterface
 * @package Magenest\GeoIp\Model\GeoStore\Switcher
 */
interface RuleInterface
{
    /**
     * @param string $countryCode
     * @return int|bool
     */
    public function getStoreId($countryCode);
}
