<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher;

use Magento\Framework\ObjectManagerInterface;
use Magenest\GeoIp\Model;

/**
 * Class RuleFactory
 * @package Magenest\GeoIp\Model\GeoStore\Switcher
 */
class RuleFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager) {
        $this->objectManager = $objectManager;
    }

    /**
     *
     * @param string $className
     * @param array $data
     * @return \Magenest\GeoIp\Model\GeoStore\Switcher\RuleInterface
     * @throws \InvalidArgumentException
     */
    public function create($className, array $data = [])
    {
        $rule = $this->objectManager->create($className, $data);
        if (!$rule instanceof Model\GeoStore\Switcher\RuleInterface) {
            throw new \InvalidArgumentException(
                $className . ' doesn\'t implement \Magenest\GeoIp\Model\Store\Switcher\RuleInterface'
            );
        }

        return $rule;
    }
}
