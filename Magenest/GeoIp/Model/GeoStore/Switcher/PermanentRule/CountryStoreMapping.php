<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher\PermanentRule;

use Magenest\GeoIp\Model;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;

/**
 * Class CountryStoreMapping
 * @package Magenest\GeoIp\Model\GeoStore\Switcher\PermanentRule
 */
class CountryStoreMapping implements Model\GeoStore\Switcher\PermanentRuleInterface
{
    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @var \Magento\Store\Api\WebsiteRepositoryInterface
     */
    protected $websiteRepository;

    /**
     * CountryStoreMapping constructor.
     * @param Model\Config\General $generalConfig
     * @param StoreRepositoryInterface $storeRepository
     * @param WebsiteRepositoryInterface $websiteRepository
     */
    public function __construct(
        Model\Config\General $generalConfig,
        StoreRepositoryInterface $storeRepository,
        WebsiteRepositoryInterface $websiteRepository
    ) {
        $this->generalConfig = $generalConfig;
        $this->storeRepository = $storeRepository;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @param int|bool $storeId
     * @param string $countryCode
     * @return int
     */
    public function updateStoreId($storeId, $countryCode)
    {
        if (!$this->generalConfig->isMappingSore()) {
            return $storeId;
        }

        $countryStoreCode = strtolower($countryCode);
        if (in_array($countryStoreCode, $this->getWebsite($storeId)->getStoreCodes())) {
            $storeId = $this->storeRepository->get($countryStoreCode)->getId();
        }

        return $storeId;
    }

    /**
     * @param int|bool $storeId
     * @return \Magento\Store\Model\Website
     */
    protected function getWebsite($storeId)
    {
        if ($storeId) {
            $websiteId = $this->storeRepository->getById($storeId)->getWebsiteId();
            $website = $this->websiteRepository->getById($websiteId);
        } else {
            $website = $this->websiteRepository->getDefault();
        }

        return $website;
    }
}
