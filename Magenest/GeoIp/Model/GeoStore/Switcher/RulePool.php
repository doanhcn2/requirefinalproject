<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\GeoStore\Switcher;

use Magenest\GeoIp\Model;

/**
 * Class RulePool
 * @package Magenest\GeoIp\Model\GeoStore\Switcher
 */
class RulePool implements RuleInterface
{
    /**
     * @var \Magenest\GeoIp\Model\GeoStore\Switcher\RuleFactory
     */
    protected $ruleFactory;

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @param \Magenest\GeoIp\Model\GeoStore\Switcher\RuleFactory $ruleFactory
     * @param array $rules
     */
    public function __construct(Model\GeoStore\Switcher\RuleFactory $ruleFactory,
                                array $rules)
    {
        $this->ruleFactory = $ruleFactory;
        $this->rules = $rules;
    }

    /**
     * @param string $countryCode
     * @return int|bool
     */
    public function getStoreId($countryCode)
    {
        foreach ($this->rules as $ruleClass) {
            $rule = $this->ruleFactory->create($ruleClass);
            $storeId = $rule->getStoreId($countryCode);
            if ($storeId) {
                return $storeId;
            }
        }

        return false;
    }
}
