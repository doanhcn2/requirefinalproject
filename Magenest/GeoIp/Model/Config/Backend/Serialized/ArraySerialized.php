<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Backend for serialized array data
 *
 */
namespace Magenest\GeoIp\Model\Config\Backend\Serialized;

use Magento\Config\Model\Config\Backend\Serialized;
/**
 * @api
 * @since 100.0.2
 */
class ArraySerialized extends Serialized
{
    /**
     * Handle data before save to database
     *
     * @return $this
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        $lastResults = '';
        if (is_array($value)) {
            $keyID = '';
            $countryCollection = [];
            $currencyOrStoreID = '';
            $endByOneData = 0;
            $identityID = '';
            foreach ($value as $keyOne=>$itemOne) {
                foreach ($itemOne as $keyTwo => $itemTwo)
                {
                    if ($keyTwo === '__empty') continue;
                    if ($keyID === '') {
                        $keyID = $keyTwo;
                    }
                    foreach ($itemTwo as $keyLast=>$valueLast) {
                        if ($keyLast === 'country' && $valueLast !== "") {
                            array_push($countryCollection, $valueLast);
                        } elseif ($keyLast === 'store_view' || $keyLast === 'currency') {
                            $currencyOrStoreID = $valueLast;
                            $identityID = $keyLast;
                            $endByOneData = 1;
                        }
                    }
                    if ($endByOneData == 1) {
                        if (count($countryCollection) !== 0) {
                            if (!isset($lastResults)) {
                                $lastResults = [$keyID => ['country' => $countryCollection, $identityID => $currencyOrStoreID]];
                            } else {
                                $lastResults = [];
                                $lastResults[$keyID] = ['country' => $countryCollection, $identityID => $currencyOrStoreID];
                            }
                        }
                        unset($keyID);
                        $keyID = '';
                        $endByOneData = 0;
                        $currencyOrStoreID = '';
                        $countryCollection = [];
                        $identityID = '';
                    }
                }
            }
        }

        if (@$lastResults) {
            $this->setValue($lastResults);
            return parent::beforeSave();
        }

        if (is_array($value)) {
            foreach ($value as $keyOne=>$itemOne) {
                foreach ($itemOne as $keyTwo => $itemTwo) {
                    if ($keyTwo === '__empty') $value = '';
                }
            }
        }
        $this->setValue($value);

        return parent::beforeSave();
    }
}
