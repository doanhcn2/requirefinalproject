<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\System;

/**
 * Class GroupGeneratorPool
 * @package Magenest\GeoIp\Model\Config\System
 */
class GroupGeneratorPool implements GroupGeneratorInterface
{
    /**
     * @var \Magenest\GeoIp\Model\Config\System\GroupGeneratorFactory
     */
    protected $generatorFactory;

    /**
     * @var array
     */
    protected $generators = [];

    /**
     * @param \Magenest\GeoIp\Model\Config\System\GroupGeneratorFactory $generatorFactory
     * @param array $generators
     */
    public function __construct(GroupGeneratorFactory $generatorFactory, array $generators)
    {
        $this->generatorFactory = $generatorFactory;
        $this->generators = $generators;
    }

    /**
     * @param int $starSortOrder
     * @return array
     */
    public function generate(&$starSortOrder = 1)
    {
        $configurationGroups = [];
        foreach ($this->generators as $generatorClass) {
            $generator = $this->generatorFactory->create($generatorClass);
            $configurationGroups += $generator->generate($starSortOrder);
        }

        return $configurationGroups;
    }
}
