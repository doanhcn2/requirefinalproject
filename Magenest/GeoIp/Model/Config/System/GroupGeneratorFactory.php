<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\System;

use Magento\Framework\ObjectManagerInterface;
use Magenest\GeoIp\Model;

/**
 * Class GroupGeneratorFactory
 * @package Magenest\GeoIp\Model\Config\System
 */
class GroupGeneratorFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     *
     * @param string $className
     * @param array $data
     * @return \Magenest\GeoIp\Model\Config\System\GroupGeneratorInterface
     * @throws \InvalidArgumentException
     */
    public function create($className, array $data = [])
    {
        $generator = $this->objectManager->create($className, $data);
        if (!$generator instanceof Model\Config\System\GroupGeneratorInterface) {
            throw new \InvalidArgumentException(
                $className . ' doesn\'t implement \Magenest\GeoIp\Model\Config\System\GroupGeneratorInterface'
            );
        }

        return $generator;
    }
}
