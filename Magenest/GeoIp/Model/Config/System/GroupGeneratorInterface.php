<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\System;

interface GroupGeneratorInterface
{
    /**
     * @param int $sortOrder
     * @return array
     */
    public function generate(&$sortOrder = 1);
}
