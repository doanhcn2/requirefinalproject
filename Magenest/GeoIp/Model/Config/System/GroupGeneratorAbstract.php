<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\System;

use Magenest\GeoIp\Model;
use Magento\Config\Model\Config\Structure\Element\FlyweightFactory;

/**
 * Class GroupGeneratorAbstract
 * @package Magenest\GeoIp\Model\Config\System
 */
class GroupGeneratorAbstract
{
    /**
     * @var \Magento\Config\Model\Config\Structure\Element\FlyweightFactory
     */
    protected $flyweightFactory;

    /**
     * @var \Magenest\GeoIp\Model\Config\General
     */
    protected $generalConfig;

    /**
     * @param \Magento\Config\Model\Config\Structure\Element\FlyweightFactory $flyweightFactory
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     */
    public function __construct(
        FlyweightFactory $flyweightFactory,
        Model\Config\General $generalConfig
    ) {
        $this->flyweightFactory = $flyweightFactory;
        $this->generalConfig = $generalConfig;
    }

    /**
     * @param array $groupData
     * @param string $scope
     * @return \Magento\Config\Model\Config\Structure\Element\Group
     */
    protected function createGroup(array $groupData, $scope = 'default')
    {
        $group = $this->flyweightFactory->create('group');
        $group->setData($groupData, $scope);

        return $group;
    }
}
