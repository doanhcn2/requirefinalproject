<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\System\GroupGenerator;

use Magento\Config\Model\Config\Structure\Element\FlyweightFactory;
use Magento\Directory\Model\CountryFactory;
use Magenest\GeoIp\Model;
use Magenest\GeoIp\Model\Config\System;

/**
 * Class Country
 * @package Magenest\GeoIp\Model\Config\System\GroupGenerator
 */
class Country extends System\GroupGeneratorAbstract implements System\GroupGeneratorInterface
{
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     * @param \Magento\Config\Model\Config\Structure\Element\FlyweightFactory $flyweightFactory
     * @param \Magenest\GeoIp\Model\Config\General $generalConfig
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     */
    public function __construct(
        FlyweightFactory $flyweightFactory,
        Model\Config\General $generalConfig,
        CountryFactory $countryFactory
    ) {
        $this->countryFactory = $countryFactory;
        parent::__construct($flyweightFactory, $generalConfig);
    }


    /**
     * @param int $sortOrder
     * @return array
     */
    public function generate(&$sortOrder = 1)
    {
        $countries = $this->generalConfig->getCountryList();
        $countriesGroups = [];
        foreach ($countries as $countryCode) {
            $groupData = [
                '_elementType' => 'group',
                'id' => $countryCode,
                'label' => $this->getCountry($countryCode)->getName(),
                'path' => 'magenest_geo_store_switcher',
                'showInDefault' => '1',
                'showInWebsite' => '1',
                'showInStore' => '0',
                'sortOrder' => $sortOrder++,
                'type' => 'text',
                'children' => [
                    'store' => [
                        '_elementType' => 'field',
                        'id' => 'store',
                        'label' => (string)__('Set Store View'),
                        'path' => 'magenest_geo_store_switcher/' . $countryCode,
                        'showInDefault' => '1',
                        'showInWebsite' => '1',
                        'showInStore' => '0',
                        'sortOrder' => '1',
                        'source_model' => 'Magenest\GeoIp\Model\Config\Source\Store',
                        'type' => 'select',
                        'depends' => [
                            'fields' => [
                                'active' => [
                                    '_elementType' => 'field',
                                    'id' => 'magenest_geo_store_switcher/general/active',
                                    'value' => '1',
                                    'dependPath' => [
                                        'magenest_geo_store_switcher',
                                        'general',
                                        'active'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            $countriesGroups[$countryCode] = $this->createGroup($groupData);
        }

        return $countriesGroups;
    }

    /**
     * @param string $countryCode
     * @return \Magento\Directory\Model\Country
     */
    protected function getCountry($countryCode)
    {
        return $this->countryFactory->create()->loadByCode($countryCode);
    }
}
