<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\Source;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface as StoreScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\GeoIp\Model\Config\ScopeDefiner;
/**
 * Class Store
 * @package Magenest\GeoIp\Model\Config\Source
 */
class Store extends \Magento\Store\Model\System\Store
{
    /**
     * @var \Magenest\GeoIp\Model\Config\ScopeDefiner
     */
    protected $scopeDefiner;

    /**
     * Store constructor.
     * @param StoreManagerInterface $storeManager
     * @param ScopeDefiner $scopeDefiner
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeDefiner $scopeDefiner
    ) {
        $this->scopeDefiner = $scopeDefiner;
        parent::__construct($storeManager);
    }

    /**
     * Load/Reload Website collection
     *
     * @return $this
     */
    protected function _loadWebsiteCollection()
    {
        switch ($this->scopeDefiner->getScope()) {
            case ScopeConfigInterface::SCOPE_TYPE_DEFAULT:
                $this->_websiteCollection = $this->_storeManager->getWebsites();
                break;
            case StoreScopeInterface::SCOPE_WEBSITE:
                $websites = $this->_storeManager->getWebsites();
                $websiteId = $this->scopeDefiner->getScopeValue();
                $this->_websiteCollection = isset($websites[$websiteId]) ? [$websites[$websiteId]] : [];
                break;
            case StoreScopeInterface::SCOPE_STORE:
            default:
                $this->_websiteCollection = [];
                break;
        }

        return $this;
    }

    public function getAllStoreViewOptions() {
        $allStores = $this->_storeManager->getStores();
        $result = [];
        foreach ($allStores as $store) {
            array_push($result, ['value' => $store->getCode(), 'label' => $store->getName()]);
        }

        return $result;
    }
}
