<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Website implements ArrayInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
     * @param \Magento\Store\Model\System\Store $store
     */
    public function __construct(
        \Magento\Store\Model\System\Store $store
    ) {
        $this->store = $store;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->store->getWebsiteValuesForForm(true, false);
    }
}
