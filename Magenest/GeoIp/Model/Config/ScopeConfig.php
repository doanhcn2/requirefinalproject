<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\GeoIp\Model\Config;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\GeoIp\Model\Config\ScopeDefiner;
use Magenest\GeoIp\Helper\Config\AppState;

/**
 * Class ScopeConfig
 * @package Magenest\GeoIp\Model\Config
 */
class ScopeConfig
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $appScopeConfig;

    /**
     * @var \Magenest\GeoIp\Model\Config\ScopeDefiner
     */
    protected $scopeDefiner;

    /**
     * @var \Magenest\GeoIp\Helper\Config\AppState
     */
    protected $appStateHelper;

    /**
     * @var int
     */
    protected $originStoreId;

    /**
     * @var int
     */
    protected $originWebsiteId;

    /**
     * ScopeConfig constructor.
     * @param ScopeConfigInterface $appScopeConfig
     * @param \Magenest\GeoIp\Model\Config\ScopeDefiner $scopeDefiner
     * @param AppState $appStateHelper
     */
    public function __construct(
        ScopeConfigInterface $appScopeConfig,
        ScopeDefiner $scopeDefiner,
        AppState $appStateHelper
    )
    {
        $this->appScopeConfig = $appScopeConfig;
        $this->scopeDefiner = $scopeDefiner;
        $this->appStateHelper = $appStateHelper;
    }

    /**
     * @param \Magento\Store\Api\Data\StoreInterface $store
     * @return $this
     */
    public function setOriginStore(StoreInterface $store)
    {
        if (null === $this->originStoreId) {
            $this->originStoreId = $store->getId();
            $this->originWebsiteId = $store->getWebsiteId();
        }
        return $this;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getDefaultValue($path)
    {
        return $this->appScopeConfig->getValue($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function getStoreValue($path)
    {
        return $this->appScopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->originStoreId);
    }

    /**
     * @param string $path
     * @return string
     */
    public function getFrontendStoreOrBackendValue($path)
    {
//        return $this->appStateHelper->isFrontendArea() ? $this->getStoreValue($path) : $this->appScopeConfig->getValue($path, $this->scopeDefiner->getScope(), $this->scopeDefiner->getScopeValue());
        return $this->appStateHelper->isFrontendArea() ? $this->getStoreValue($path) : false;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getWebsiteValue($path)
    {
        return $this->appScopeConfig->getValue($path, ScopeInterface::SCOPE_WEBSITE, $this->originWebsiteId);
    }

    /**
     * @param string $path
     * @return string
     */
    public function getFrontendWebsiteOrBackendValue($path)
    {
        $value = '';
        if ($this->appStateHelper->isFrontendArea()) {
            $value = $this->getWebsiteValue($path);
        } else if ($this->scopeDefiner->getScope() != ScopeInterface::SCOPE_STORE) {
            $value = $this->appScopeConfig->getValue($path, $this->scopeDefiner->getScope(), $this->scopeDefiner->getScopeValue());
        }

        return $value;
    }
}
