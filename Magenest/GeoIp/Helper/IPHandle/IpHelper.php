<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Helper\IPHandle;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magenest\GeoIp\Model\Config\ScopeConfig;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Information as Inf;

/**
 * Class IpHelper
 * @package Magenest\GeoIp\Helper\IPHandle
 */
class IpHelper extends AbstractHelper
{
    const COUNTRY_TO_STORE_VIEW_CONFIG = 'magenest_geo_store_switcher/ctrytsview/ctrytsviewmapping';
    const COUNTRY_TO_CURRENCY_CONFIG = 'magenest_geo_store_switcher/ctrytcurrency/ctrytcurrencymapping';

    /**
     * @var \Magenest\GeoIp\Model\Config\ScopeConfig
     */
    protected $myScopeConfig;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * IpHelper constructor.
     * @param Context $context
     * @param ScopeConfig $scopeConfig
     * @param RemoteAddress $remoteAddress
     */
    public function __construct(Context $context,
                                ScopeConfig $scopeConfig,
                                RemoteAddress $remoteAddress) {
        $this->remoteAddress = $remoteAddress;
        $this->myScopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function isActiveCountryToStoreView() {
        return $this->myScopeConfig->getStoreValue('magenest_geo_store_switcher/ctrytsview/active');
    }

    public function isActiveCountryToCurrency() {
        return $this->myScopeConfig->getStoreValue('magenest_geo_store_switcher/ctrytcurrency/active');
    }

    public function getListCountryToStoreView() {
        $enable = $this->myScopeConfig->getStoreValue('magenest_geo_store_switcher/ctrytsview/active');
        if ($enable === "1") {
            $value = $this->myScopeConfig->getStoreValue(self::COUNTRY_TO_STORE_VIEW_CONFIG);
            return json_decode($value);
        } else {
            return null;
        }
    }

    public function getListCountryToCurrency() {
        if ($this->myScopeConfig->getStoreValue('magenest_geo_store_switcher/ctrytcurrency/active') === "1") {
            $value = $this->myScopeConfig->getStoreValue(self::COUNTRY_TO_CURRENCY_CONFIG);
            return json_decode($value);
        } else {
            return null;
        }
    }

    public function isHasMandatoryStoreViewConfig($countryCode) {
        $configValue = $this->getListCountryToStoreView();
        if ($configValue == null) {
            return false;
        }
        $isHasStoreViewConfig = false;
        /*
         * @todo: For test redirect purpose, change country code here
         */
//        $countryCode = "AF";
        foreach ($configValue as $value) {
            foreach ($value as $key=>$valueAConfig) {
                if ($key == 'country') {
                    foreach ($valueAConfig as $country) {
                        if ($country == $countryCode) {
                            $isHasStoreViewConfig = true;
                        }
                    }
                }
                if ($key == 'store_view') {
                    if ($isHasStoreViewConfig) {
                        return $valueAConfig;
                    }
                }
            }
        }

        return false;
    }

    public function isHasMandatoryCurrencyConfig($countryCode) {
        $configValue = $this->getListCountryToCurrency();
        if ($configValue == null) {
            return false;
        }
        /*
         * @todo: For test redirect purpose, change country code here
         */
//        $countryCode = "AF";
        $isHasStoreViewConfig = false;
        foreach ($configValue as $value) {
            foreach ($value as $key=>$valueAConfig) {
                if ($key == 'country') {
                    foreach ($valueAConfig as $country) {
                        if ($country == $countryCode) {
                            $isHasStoreViewConfig = true;
                        }
                    }
                }
                if ($key == 'currency') {
                    if ($isHasStoreViewConfig) {
                        return $valueAConfig;
                    }
                }
            }
        }

        return false;
    }
}
