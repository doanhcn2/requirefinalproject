<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Helper\IPHandle;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem;
/**
 * Class DetectCountry
 * @package Magenest\GeoIp\Helper\IPHandle
 */
class DetectCountry {

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $_remoteAddress;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * DetectCountry constructor.
     * @param RemoteAddress $remoteAddress
     * @param UrlInterface $urlBuilder
     * @param Filesystem $filesystem
     */
    public function __construct(
        RemoteAddress $remoteAddress,
        UrlInterface $urlBuilder,
        Filesystem $filesystem
    )
    {
        $this->_remoteAddress = $remoteAddress;
        $this->urlBuilder = $urlBuilder;
        $this->fileSystem = $filesystem;
    }

    public function getRemoteIp() {
        return $this->_remoteAddress->getRemoteAddress();
    }

    /**
     * Get ip address of customer and convert to country information
     *
     * @return array
     */
    public function detect()
    {
        $remoteAddress = $this->_remoteAddress->getRemoteAddress();
        /**
         * @todo: For test get and convert ip address purpose, change input ip address here
         */
//        $remoteAddress = '185.100.218.67';
        try {
            if (class_exists('\GeoIp2\Database\Reader')) {
                $directory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
                if ($directory->isFile('magenest/GeoLite2-City.mmdb')) {
                    $reader = new \GeoIp2\Database\Reader($this->getGeoIpDataFile());
                    $record = $reader->city($remoteAddress);
                    $information = array();
                    if ($record) {
                        $information['country_id'] = $record->country->isoCode;
                        $information['city'] = $record->city->name;
                        $information['region'] = $record->mostSpecificSubdivision->name;
                        $information['postcode'] = $record->postal->code;
                        return $information;
                    } else {
                        return null;
                    }
                }
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getGeoIpDataFile() {
        $mediaDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        $url = $mediaDirectory->getAbsolutePath('magenest/GeoLite2-City.mmdb');
        return $url;
    }
}
