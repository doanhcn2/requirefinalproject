<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Helper\Config;

use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\HTTP\Header;

/**
 * Class Request
 * @package Magenest\GeoIp\Helper\Config
 */
class Request
{
    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var \Magento\Framework\HTTP\Header
     */
    protected $httpHeader;

    /**
     * Request constructor.
     * @param RemoteAddress $remoteAddress
     * @param Header $httpHeader
     */
    public function __construct(
        RemoteAddress $remoteAddress,
        Header $httpHeader
    ) {
        $this->remoteAddress = $remoteAddress;
        $this->httpHeader = $httpHeader;
    }

    /**
     * @param array $whiteIps
     * @return bool
     */
    public function isCurrentIp($whiteIps)
    {
        $remoteIp = $this->remoteAddress->getRemoteAddress();
        return !empty($whiteIps) && !empty($remoteIp) && array_search($remoteIp, $whiteIps) !== false;
    }

    /**
     * @param string $uaRegex
     * @return bool
     */
    public function isCurrentUserAgent($uaRegex)
    {
        return $uaRegex && @preg_match($uaRegex, $this->httpHeader->getHttpUserAgent());
    }
}
