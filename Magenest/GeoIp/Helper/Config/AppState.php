<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Helper\Config;

use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\State;

/**
 * Class AppState
 * @package Magenest\GeoIp\Helper\Config
 */
class AppState
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * AppState constructor.
     * @param State $state
     */
    public function __construct(State $state) {
        $this->state = $state;
    }

    /**
     * @return bool
     */
    public function isFrontendArea()
    {
        try {
            if ($this->state->getAreaCode() == Area::AREA_ADMINHTML) {
                return false;
            }
        } catch (LocalizedException $e) {
            /* Area is not initialized. Do nothing. */
            return true;
        }
        return true;
    }
}
