<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\Factory;

/**
 * Class CountryToCurrency
 * @package Magenest\GeoIp\Block\Adminhtml\System\Config
 */
class CountryToCurrency extends AbstractFieldArray
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * CountryToCurrency constructor.
     * @param Context $context
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        array $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initial object
     */
    protected function _construct()
    {
        $this->addColumn('country', ['label' => __('Country'), 'style' => 'width:230px; height: 200px']);
        $this->addColumn('currency', ['label' => __('Currency'), 'style' => 'width:180px; height: 200px']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
        $this->setTemplate('system/config/form/ctrytcurrency.phtml');
    }

    /**
     * Create select box for country field
     *
     * @param $columnName
     * @param null $rowData
     * @return string
     * @throws \Exception
     */
    public function renderMultiSelectCountry($columnName, $rowData = null)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $listCountry = $objectManager->create('Magento\Directory\Model\Config\Source\Country')->toOptionArray();

        if ($rowData !== null) {
            $start = '<select multiple="multiple" id="' . $this->_getCellInputElementId(
                    '<%- _id %>',
                    $columnName
                ) .
                '"' .
                ' name="' .
                $inputName .
                '" ' .
                ($column['size'] ? 'size="' .
                    $column['size'] .
                    '"' : '') .
                ' class="' .
                (isset(
                    $column['class']
                ) ? $column['class'] : 'input-text') . '"' . (isset(
                    $column['style']
                ) ? ' style="' . $column['style'] . '"' : '') . '>';
            $end = '</select>';

            $options = '';
            $rowValue = null;
            $data = $rowData->getData();
            foreach ($data as $key => $value) {
                $rowValue = $value;
                break;
            }
            $count = 0;
            foreach ($listCountry as $country) {
                if ($country['value'] === "") continue;
                if (in_array($country['value'], $rowValue)) {
                    $row = '<option selected="selected" value="'. $country['value'] .'">' . $country['label'] . '</option>';
                } else {
                    $row = '<option value="'. $country['value'] .'">' . $country['label'] . '</option>';
                }
                $count++;
                $options .= $row;
                if ($count == 3) break;
            }

            return $start . $options . $end ;
        }

        $start = '<select multiple="multiple" id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '>';
        $end = '</select>';

        $options = '';
        foreach ($listCountry as $country) {
            $row = '<option value="'. $country['value'] .'">' . $country['label'] . '</option>';
            $options .= $row;
        }

        return $start . $options . $end ;
    }

    /**
     * Create select box for currency field
     *
     * @param $columnName
     * @param null $rowData
     * @return string
     * @throws \Exception
     */
    public function renderMultiSelectCurrency($columnName, $rowData = null)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $listCurrency = $objectManager->create('Magento\Config\Model\Config\Source\Locale\Currency')->toOptionArray();

        if ($rowData) {
            $start = '<select style="height: auto !important; width: 180px" id="' . $this->_getCellInputElementId(
                    '<%- _id %>',
                    $columnName
                ) .
                '"' .
                ' name="' .
                $inputName .
                '" ' .
                ($column['size'] ? 'size="' .
                    $column['size'] .
                    '"' : '') .
                ' class="' .
                (isset(
                    $column['class']
                ) ? $column['class'] : 'input-text') . '"' . (isset(
                    $column['style']
                ) ? ' style="' . $column['style'] . '"' : '') . '>';
            $end = '</select>';

            $options = '';
            $rowCurrency = null;
            $data = $rowData->getData();
            foreach ($data as $key => $value) {
                $rowCurrency = $key;
                break;
            }
            $count = 1;

            foreach ($listCurrency as $currency) {
                if ($currency['value'] === $rowCurrency) {
                    $row = '<option selected="selected" value="'. $currency['value'] .'">' . $currency['label'] . '</option>';
                } else {
                    $row = '<option value="'. $currency['value'] .'">' . $currency['label'] . '</option>';
                }
                $count++;
                if ($count == 3) break;
                $options .= $row;
            }

            return $start . $options . $end;
        }

        $start = '<select style="height: auto !important; width: 180px" id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '>';
        $end = '</select>';
        $options = '';

        foreach ($listCurrency as $currency) {
            $row = '<option value="'. $currency['value'] .'">' . $currency['label'] . '</option>';
            $options .= $row;
        }

        return $start . $options . $end ;
    }
}
