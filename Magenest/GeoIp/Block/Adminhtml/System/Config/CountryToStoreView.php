<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\Factory;

/**
 * Class CountryToStoreView
 * @package Magenest\GeoIp\Block\Adminhtml\System\Config
 */
class CountryToStoreView extends AbstractFieldArray
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * CountryToStoreView constructor.
     * @param Context $context
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        array $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initial object
     */
    protected function _construct()
    {
        $this->addColumn('country', ['label' => __('Country'), 'style' => 'width:230px; height: 200px']);
        $this->addColumn('store_view', ['label' => __('Store View'), 'style' => 'width:180px; height: 200px']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
        $this->setTemplate('system/config/form/ctrytsview.phtml');
    }

    /**
     * Create select box for country field
     *
     * @param $columnName
     * @return string
     * @throws \Exception
     */
    public function renderMultiSelectCountry($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $listCountry = $objectManager->create('Magento\Directory\Model\Config\Source\Country')->toOptionArray();

        $start = '<select multiple="" id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '>';
        $end = '</select>';

        $options = '';
        foreach ($listCountry as $country) {
            $row = '<option value="'. $country['value'] .'">' . $country['label'] . '</option>';
            $options .= $row;
        }

        return $start . $options . $end ;
    }

    /**
     * Create select box for store view field
     *
     * @param $columnName
     * @return string
     * @throws \Exception
     */
    public function renderAllStoreView($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $listStore = $objectManager->create('Magenest\GeoIp\Model\Config\Source\Store')->getAllStoreViewOptions();

        $start = '<select style="height: auto !important; width: 180px" id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '>';
        $end = '</select>';

        $options = '';
        foreach ($listStore as $store) {
            $row = '<option value="'. $store['value'] .'">' . $store['label'] . '</option>';
            $options .= $row;
        }

        return $start . $options . $end ;
    }
}
