<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Config\Model\Config\Structure;
use Magenest\GeoIp\Model\Config\System\GroupGeneratorInterface;

/**
 * Class Form
 * @package Magenest\GeoIp\Block\Adminhtml\System\Config
 */
class Form extends \Magento\Config\Block\System\Config\Form
{
    /**
     * @var \Magenest\GeoIp\Model\Config\System\GroupGeneratorInterface
     */
    protected $groupGenerator;

    /**
     * Form constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param \Magento\Config\Model\Config\Factory $configFactory
     * @param Structure $configStructure
     * @param \Magento\Config\Block\System\Config\Form\Fieldset\Factory $fieldsetFactory
     * @param \Magento\Config\Block\System\Config\Form\Field\Factory $fieldFactory
     * @param GroupGeneratorInterface $groupGenerator
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \Magento\Config\Model\Config\Factory $configFactory,
        Structure $configStructure,
        \Magento\Config\Block\System\Config\Form\Fieldset\Factory $fieldsetFactory,
        \Magento\Config\Block\System\Config\Form\Field\Factory $fieldFactory,
        GroupGeneratorInterface $groupGenerator,
        array $data = []
    ) {
        $this->groupGenerator = $groupGenerator;
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $configFactory,
            $configStructure,
            $fieldsetFactory,
            $fieldFactory,
            $data
        );
    }

    /**
     * Initialize form
     *
     * @return $this
     */
    public function initForm()
    {
        $this->_initObjects();
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        /** @var $section \Magento\Config\Model\Config\Structure\Element\Section */
        $section = $this->_configStructure->getElement($this->getSectionCode());

        if ($section && $section->isVisible($this->getWebsiteCode(), $this->getStoreCode())) {
            foreach ($section->getChildren() as $group) {
                $this->_initGroup($group, $section, $form);
            }
            $sortOrder = isset($group) && $group->getAttribute('sortOrder') ? $group->getAttribute('sortOrder') : 1;
            $sortOrder++;
            foreach ($this->groupGenerator->generate($sortOrder) as $group) {
                $this->_initGroup($group, $section, $form);
            }
        }
        $this->setForm($form);

        return $this;
    }
}
