<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\GeoIp\Observer;

use Magenest\GeoIp\Helper\IPHandle\DetectCountry;
use Magenest\GeoIp\Model\Config\General;
use Magento\Framework\Event\ObserverInterface;
use Magenest\GeoIp\Helper\IPHandle\IpHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Customer\Model\Session;
use Magento\Store\Model\Store;
use Magento\Directory\Model\Data\CountryInformation;

/**
 * Class ChangeByGeoIp
 * @package Magenest\GeoIp\Observer
 */
class ChangeByGeoIp implements ObserverInterface
{
    /**
     * @var$_ipHelper IpHelper
     */
    protected $_ipHelper;

    /**
     * @var $_detection \Magenest\GeoIp\Helper\IPHandle\DetectCountry
     */
    protected $_detection;

    /**
     * @var $_general \Magenest\GeoIp\Model\Config\General
     */
    protected $_general;

    /**
     * @var $_storeManager \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var $_store \Magento\Store\Model\Store
     */
    protected $_store;
    /**
     * @var $_response \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var $_url \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var $_redirect \Magento\Framework\App\Response\RedirectInterface
     */
    protected $_redirect;

    /**
     * @var $_countryInfo \Magento\Directory\Model\Data\CurrencyInformation
     */
    protected $_countryInfo;

    /**
     * @var $_customerSession \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        StoreManagerInterface $storeManager,
        RedirectInterface $redirect,
        UrlInterface $url,
        ResponseInterface $response,
        IpHelper $ipHelper,
        DetectCountry $detectCountry,
        General $general,
        Session $session,
        Store $store,
        CountryInformation $countryInformation
    ) {
        $this->_customerSession = $session;
        $this->_countryInfo = $countryInformation;
        $this->_store = $store;
        $this->_url = $url;
        $this->_response = $response;
        $this->_redirect = $redirect;
        $this->_storeManager = $storeManager;
        $this->_general = $general;
        $this->_detection = $detectCountry;
        $this->_ipHelper = $ipHelper;
    }

    /**
     * Redirect store view and currency base on setting
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $isRedirect = $this->_customerSession->getGeoRedirectFirstTime();
        $isRedirect = @$isRedirect;

        if ($this->_general->isActive() && $this->_ipHelper->isActiveCountryToStoreView() && $isRedirect !== 'true') {
            if ($this->_general->isAvailable()) {
                $remoteInfo = $this->_detection->detect();
                $countryCode = $remoteInfo['country_id'];
                $ctry2stv = $this->_ipHelper->isHasMandatoryStoreViewConfig($countryCode);
                $allStores = $this->_storeManager->getStores();

                if ($ctry2stv !== false) {
                    if ($this->_storeManager->getStore()->getCode() !== $ctry2stv) {
                    foreach ($allStores as $store) {
                        if ($store->getCode() === $ctry2stv) {
                            $storeId = $store->getId();
                            $storeUrl = $this->_storeManager->getStore($storeId)->getBaseUrl();
//                            $this->_storeManager->reinitStores();
                            if ($isRedirect !== 'true') {
                                $this->_customerSession->setGeoRedirectFirstTime('true');
                                $this->_storeManager->setCurrentStore($storeId);
                                $this->_response->setRedirect($storeUrl)->sendResponse();
                            }
                            }
                        }
                    }
                } else {
                    $defaultStoreConfig = $this->_general->getDefaultStore();
                    if ($defaultStoreConfig !== false) {
                        foreach ($allStores as $store) {
                            if ($store->getCode() === $defaultStoreConfig) {
                                $storeId = $store->getId();
                                $storeUrl = $this->_storeManager->getStore($storeId)->getBaseUrl();
//                                $this->_storeManager->reinitStores();
                                $this->_storeManager->setCurrentStore($storeId);
                                $this->_response->setRedirect($storeUrl)->sendResponse();
                            }
                        }
                    } else {
                        $default = $this->_storeManager->getDefaultStoreView()->getId();
                        $this->_storeManager->setCurrentStore($default);
                    }
                }
            }
        }
        $isRedirectCurrency = $this->_customerSession->getGeoRedirectCurrency();
        $isRedirectCurrency = @$isRedirectCurrency;

        if ($this->_general->isActive() && $this->_ipHelper->isActiveCountryToCurrency() && $isRedirectCurrency !== 'true') {
            if ($this->_general->isAvailable()) {
                $remoteInfo = $this->_detection->detect();
                $countryCode = $remoteInfo['country_id'];
                $ctry2cry = $this->_ipHelper->isHasMandatoryCurrencyConfig($countryCode);
                if ($ctry2cry !== false) {
                    if (!in_array($ctry2cry, $this->_store->getAvailableCurrencyCodes())) {
                        $codeAllows = $this->_store->getAvailableCurrencyCodes();
                        array_push($codeAllows, $ctry2cry);
                        $this->_countryInfo->setAvailableCurrencyCodes($codeAllows);
                    }
                    $this->_store->setCurrentCurrencyCode($ctry2cry);
                    $this->_customerSession->setGeoRedirectCurrency('true');
                }
            }
        }
    }
}
