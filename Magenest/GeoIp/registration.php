<?php
/**
 * Copyright © 2016 Magenest. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magenest_GeoIp',
    __DIR__
);
