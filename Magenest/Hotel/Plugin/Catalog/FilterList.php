<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/27/18
 * Time: 4:12 PM
 */

namespace Magenest\Hotel\Plugin\Catalog;

class FilterList
{
    protected $_registry;
    protected $objectManager;
    protected $helper;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magenest\Hotel\Helper\Filter $helper
    ) {
        $this->_registry = $registry;
        $this->objectManager = $objectManager;
        $this->helper = $helper;
    }

    public function aroundGetFilters(
        \Magento\Catalog\Model\Layer\FilterList $subject,
        callable $proceed,
        \Magento\Catalog\Model\Layer $layer
    ) {
        $result = $proceed($layer);
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->_registry->registry('current_category');
        if (!$this->helper->isHotelCategory($category)) {
            return $result;
        }
        $result[] = $this->objectManager->create(
            \Magenest\Hotel\Model\Product\Filter\Location::class,
            ['layer' => $layer]
        );
        return $result;
    }
}