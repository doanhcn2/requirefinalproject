<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Plugin\Unirgy;

use Magento\Framework\App\ObjectManager;

/**
 * Class AfterLoadCollection
 * @package Magenest\Hotel\Plugin\Unirgy
 */
class AfterLoadCollection
{
    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_helper;

    /**
     * @var bool
     */
    protected $_isLoaded;

    /**
     * AfterLoadCollection constructor.
     * @param \Unirgy\Dropship\Helper\Data $helper
     */
    public function __construct(
        \Unirgy\Dropship\Helper\Data $helper
    ) {
        $this->_helper = $helper;
        $this->_isLoaded = false;
    }

    /**
     * @param \Unirgy\DropshipVendorProduct\Block\Vendor\Products $subject
     * @param \Unirgy\Dropship\Model\ResourceModel\ProductCollection $result
     * @return \Unirgy\Dropship\Model\ResourceModel\ProductCollection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetProductCollection(\Unirgy\DropshipVendorProduct\Block\Vendor\Products $subject, $result)
    {
        if ($this->_isLoaded) {
            return $result;
        }
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        if (!$v || !$v->getId()) {
            return $result;
        }

        $collection = $this->_helper->createObj('\Unirgy\Dropship\Model\ResourceModel\ProductCollection')
            ->setFlag('udskip_price_index', 1)
            ->setFlag('has_group_entity', 1)
            ->setFlag('has_stock_status_filter', 1)
            ->addAttributeToFilter('type_id', ['in' => ['hotel']])
            ->addAttributeToSelect(['sku', 'name', 'status', 'price'])
            ->addAttributeToFilter('entity_id', ['in' => $v->getAssociatedProductIds()]);

        foreach ($collection as $item) {
            $result->addItem($item);
        }
        $this->_isLoaded = true;

        return $result;
    }
}
