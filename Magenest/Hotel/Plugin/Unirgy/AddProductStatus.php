<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/26/18
 * Time: 11:06 AM
 */

namespace Magenest\Hotel\Plugin\Unirgy;

use Magenest\Hotel\Model\ProductStatus;

class AddProductStatus
{
    /**
     * @param \Unirgy\DropshipVendorProduct\Model\Source $subject
     * @param \Magento\Framework\Phrase[] $result
     * @return mixed
     */
    public function afterToOptionHash(\Unirgy\DropshipVendorProduct\Model\Source $subject, $result)
    {
        if ($subject->getPath() === 'system_status') {
            $result[ProductStatus::STATUS_WAITING_TO_BE_PUBLISHED] = __('Approved');
            $result[ProductStatus::STATUS_OFFER_EXPIRED] = __('Offer expired');
        }
        return $result;
    }

}