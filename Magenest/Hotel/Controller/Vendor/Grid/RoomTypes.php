<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/4/18
 * Time: 6:56 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Grid;

use Magenest\Hotel\Controller\AbstractGrid;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Service
 * @package Magenest\Hotel\Controller\Vendor\Grid
 */
class RoomTypes extends AbstractGrid
{
    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $_roomTypeAdminFactory;

    /**
     * @var \Magenest\Hotel\Model\BedTypeFactory
     */
    protected $_bedTypeFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\Service\Collection
     */
    protected $_collection;

    /**
     * RoomTypes constructor.
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     * @param \Magenest\Hotel\Helper\Helper $helper
     * @param Context $context
     * @param \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory $collectionFactory
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
     * @param \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory
     */
    public function __construct(
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Helper\Helper $helper,
        Context $context,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory $collectionFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory
    ) {
        $this->_bedTypeFactory = $bedTypeFactory;
        $this->_roomTypeAdminFactory = $roomTypeAdminFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_vendorSession = $vendorSession;
        parent::__construct($context, $helper);
    }


    public function getCollectionData()
    {
        $collectionData = parent::getCollectionData();
        for ($i = 0; $i < count($collectionData); $i++) {
            $type = $this->_roomTypeAdminFactory->create()->load($collectionData[$i]['room_type_admin_id']);
            $collectionData[$i]['type'] = $type->getRoomType();
            $bedSetup = json_decode($collectionData[$i]['beds_type'], true);
            foreach ($bedSetup as $bed) {
                $line = $bed['numberBeds'];
                $bedType = $this->_bedTypeFactory->create()->load($bed['bedId']);
                $line .= " x " . $bedType->getBedType();
                $collectionData[$i]["bedSetup"][] = $line;
            }
            $collectionData[$i]["bedSetup"] = implode("\n", $collectionData[$i]["bedSetup"]);
            if ($collectionData[$i]['has_extra_beds'] == 0) {
                $collectionData[$i]['extraBeds'] = 0;
            } else {
                $collectionData[$i]['extraBeds'] = $collectionData[$i]['number_extra_beds'] . "\n" . $this->getCurrencySymbol() . $collectionData[$i]['price_per_bed'] . "/bed";
            }
            if ($collectionData[$i]['non_smoking'] == 0)
                $collectionData[$i]['smokingPolicy'] = "Non Smoking";
            else
                $collectionData[$i]['smokingPolicy'] = "Smoking";
            $collectionData[$i]['isChecked'] = false;
        }
        return $collectionData;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->helper->getCurrencySymbol();
    }

    public function getCollection()
    {
        if (!$this->_collection) {
            if (!$this->_vendorSession->isLoggedIn()) {
                throw new LocalizedException(__("Vendor not logged in"));
            }
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection _collection */
            $this->_collection = $this->_collectionFactory->create();
            $this->_collection->addVendorToFilter($this->_vendorSession->getVendorId());
        }
        return $this->_collection;
    }

    /**
     * @param $packageId
     * @return string
     */
    public function getServices($packageId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $serviceCollection */
        $serviceCollection = $this->_collectionFactory->create();

        $arr = $serviceCollection->getServicesWithPackageId($this->_vendorSession->getVendorId(), $packageId);
        $titleArr = array_map(function ($item) {
            return @$item['title'];
        }, $arr);
        return implode("\n", $titleArr);
    }
}
