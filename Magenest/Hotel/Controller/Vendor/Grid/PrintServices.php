<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 14/04/2018
 * Time: 16:09
 */

namespace Magenest\Hotel\Controller\Vendor\Grid;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PrintServices
 * @package Magenest\Hotel\Controller\Rate\Categories
 */
class PrintServices extends \Magento\Framework\App\Action\Action
{
    protected $directory;

    protected $fileFactory;

    protected $_serviceFactory;

    public function __construct(
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->_serviceFactory = $serviceFactory;
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->getRequest()->getParams();
            $resultRedirect = $this->resultRedirectFactory->create();
            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($data) {
                $heading = [
                    __('Service Name'),
                    __('Description'),
                    __('Price')
                ];

                $collection = $this->_serviceFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
                if ($data['chosen'] != 'all') {
                    $collection = $collection->addFieldToFilter("id", array('in' => json_decode($data['chosen'], true)));
                } else {
                    if ($data['except'] != 0)
                        $collection = $collection->addFieldToFilter("id", array('nin' => json_decode($data['except'], true)));
                }

                $items = [];
                /** @var  \Magenest\Hotel\Model\Service $model */
                foreach ($collection as $model) {
                    $items[] = [
                        $model->getTitle(),
                        $model->getDescription(),
                        $model->getPrice()
                    ];
                }
                $file = 'export/ListServices' . date('Ymd_His') . '.csv';
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach ($items as $item) {
                    $stream->writeCsv($item);

                }

                return $this->fileFactory->create('ListServices' . date('Ymd_His') . '.csv', [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('hotel/vendor_property/manage/');
    }

}
