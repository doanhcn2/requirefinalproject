<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 14/04/2018
 * Time: 16:09
 */

namespace Magenest\Hotel\Controller\Vendor\Grid;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PrintServices
 * @package Magenest\Hotel\Controller\Rate\Categories
 */
class PrintRoomTypes extends \Magento\Framework\App\Action\Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    protected $fileFactory;

    protected $_bedTypeFactory;
    protected $_roomTypeVendorFactory;
    protected $_roomTypeAdminFactory;

    public function __construct(
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    )
    {
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_roomTypeAdminFactory = $roomTypeAdminFactory;
        $this->fileFactory = $fileFactory;
        $this->_bedTypeFactory = $bedTypeFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->getRequest()->getParams();
            $resultRedirect = $this->resultRedirectFactory->create();
            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($data) {
                $heading = [
                    __('Room Title'),
                    __('Room Type'),
                    __('Number of Rooms'),
                    __('Guests'),
                    __('Bed Setup'),
                    __('Extra Beds'),
                    __('Smoking Policy'),
                    __('Base Price per Night')
                ];

                $collection = $this->_roomTypeVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
                if ($data['chosen'] != 'all') {
                    $collection = $collection->addFieldToFilter("id", array('in' => json_decode($data['chosen'], true)));
                } else {
                    if ($data['except'] != 0)
                        $collection = $collection->addFieldToFilter("id", array('nin' => json_decode($data['except'], true)));
                }

                $items = [];
                /** @var  \Magenest\Hotel\Model\RoomTypeVendor $model */
                foreach ($collection as $model) {
                    $beds = json_decode($model->getBedsType(), true);
                    foreach ($beds as $bed) {
                        $line = $bed['numberBeds'];
                        $bedType = $this->_bedTypeFactory->create()->load($bed['bedId']);
                        $line .= " x " . $bedType->getBedType();
                        $bedSetup[] = $line;
                    }
                    $bedSetup = implode("\n", $bedSetup);
                    if ($model->getHasExtraBeds() == 0) {
                        $extraBeds = 0;
                    } else {
                        $extraBeds = $model->getNumberExtraBeds() . "\n" . $this->getCurrencySymbol() . $model->getPricePerBed() . "/bed";
                    }
                    if ($model->getNonSmoking() == 0)
                        $smokingPolicy = "Non Smoking";
                    else
                        $smokingPolicy = "Smoking";
                    $items[] = [
                        $model->getRoomTitle(),
                        $this->_roomTypeAdminFactory->create()->load($model->getRoomTypeAdminId())->getRoomType(),
                        $model->getNumberRoomsAvailable(),
                        $model->getNumberGuests(),
                        $bedSetup,
                        $extraBeds,
                        $smokingPolicy,
                        $model->getPricePerNight(),
                    ];
                }
                $file = 'export/ListRoomTypes' . date('Ymd_His') . '.csv';
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach ($items as $item) {
                    $stream->writeCsv($item);

                }

                return $this->fileFactory->create('ListRoomTypes' . date('Ymd_His') . '.csv', [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('hotel/vendor_property/manage/');
    }

}
