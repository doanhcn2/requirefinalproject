<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/4/18
 * Time: 6:56 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Grid;

use Magenest\Hotel\Controller\AbstractGrid;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Service
 * @package Magenest\Hotel\Controller\Vendor\Grid
 */
class Service extends AbstractGrid
{
    /**
     * @var \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory
     */
    protected $_collectionFactory;


    /**
     * @var \Magenest\Hotel\Model\ResourceModel\Service\Collection
     */
    protected $_collection;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    public function __construct(
        Context $context,
        \Magenest\Hotel\Helper\Helper $helper,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $collectionFactory
    ) {
        parent::__construct($context, $helper);
        $this->_vendorSession = $vendorSession;
        $this->_collectionFactory = $collectionFactory;
    }

    public function getCollectionData()
    {
        $currencySymbol = $this->helper->getCurrencySymbol();
        $data = parent::getCollectionData();
        foreach ($data as $key => $datum) {
            $data[$key]['isChecked'] = false;
            $data[$key]['price'] = $currencySymbol . $data[$key]['price'];
        }
        return $data;
    }

    public function getCollection()
    {
        if (!$this->_collection) {
            if (!$this->_vendorSession->isLoggedIn()) {
                throw new LocalizedException(__("Vendor not logged in"));
            }
            /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection _collection */
            $this->_collection = $this->_collectionFactory->create();
            $this->_collection->addVendorToFilter($this->_vendorSession->getVendorId());
        }
        return $this->_collection;
    }

}
