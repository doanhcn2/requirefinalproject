<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/3/18
 * Time: 5:18 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Property;

/**
 * Class Manage
 * @package Magenest\Hotel\Controller\Vendor\Property
 */
class Manage extends \Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_renderPage(null, 'magenest_hotel_manage_property');
    }
}
