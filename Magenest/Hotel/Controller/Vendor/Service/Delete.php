<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/5/18
 * Time: 10:40 AM
 */

namespace Magenest\Hotel\Controller\Vendor\Service;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Delete
 * @package Magenest\Hotel\Controller\Vendor\Service
 */
class Delete extends \Magento\Framework\App\Action\Action
{
    protected $vendorSession;

    protected $resource;

    protected $collectionFactory;

    public function __construct(
        Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Model\ResourceModel\Service $resource,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->resource = $resource;
        $this->vendorSession = $vendorSession;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        if (!$this->vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }
        $type = $this->getRequest()->getParam('type');
        $exception = $this->getRequest()->getParam('exception');
        /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addVendorToFilter($this->vendorSession->getVendorId());
        if (is_array($exception)) {
            if ($type === 'checkedAll') {
                $collection->addFieldToFilter('id', ['nin' => $exception]);
            } else {
                $collection->addFieldToFilter('id', ['in' => $exception]);
            }
        }
        $collection->load();
        $collection->walk([$this->resource, 'delete']);
    }
}
