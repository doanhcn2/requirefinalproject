<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Vendor\Service;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_serviceFactory;
    protected $_serviceCollection;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory
    ) {
        parent::__construct($context);
        $this->_serviceCollection = $serviceCollectionFactory;
        $this->_serviceFactory = $serviceFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }

            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\Service $serviceModel */
            $serviceModel = $this->_serviceFactory->create();

            $items = $this->_request->getParam('items');
            if (is_array($items)) {
                foreach ($items as $item) {
                    if ($item['index'] === '0') {
                        $serviceModel = $this->_serviceFactory->create();
                    } else {
                        $serviceModel = $this->_serviceFactory->create()->load($item['index']);
                    }
                    $serviceModel->addData($item)->setData('vendor_id', $vendorId)->save();
                }
            }

            $toRmItems = $this->_request->getParam('remove');

            if (is_array($toRmItems)) {
                foreach ($toRmItems as $item) {
                    if ($item !== '0') {
                        $serviceModel->load($item)->delete();
                    }
                }
            }
            /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $servicesCollection */
            $servicesCollection = $this->_serviceCollection->create();
            $services = $servicesCollection->addVendorToFilter($vendorId)->getData();

            $result->setData([
                'success' => true,
                'items' => $services
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}
