<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Vendor\Gallery;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Upload
 * @package Magenest\Hotel\Controller\Vendor\Gallery
 */
class Upload extends \Magento\Framework\App\Action\Action
{
    const IMG_PATH = 'hotel/gallery/images';

    protected $_vendorSession;

    protected $_storeManager;

    /**
     * Upload constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     * @param Context $context
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\Dropship\Model\Session $vendorSession,
        Context $context
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_vendorSession = $vendorSession;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }
        try {
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'image']
            );
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

            /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
            $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
            $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);

            /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);

            /** @var \Magento\Catalog\Model\Product\Media\Config $config */
            $result = $uploader->save($mediaDirectory->getAbsolutePath(self::IMG_PATH));

            unset($result['tmp_name']);
            unset($result['path']);

            $result['url'] = $result['file'];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }
}
