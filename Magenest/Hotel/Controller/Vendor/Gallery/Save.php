<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/12/18
 * Time: 10:21 AM
 */

namespace Magenest\Hotel\Controller\Vendor\Gallery;


use Magento\Framework\App\Action\Context;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_galleryResource;

    protected $_vendorSession;

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\Gallery $galleryResource,
        \Unirgy\Dropship\Model\Session $vendorSession,
        Context $context
    ) {
        parent::__construct($context);
        $this->_vendorSession = $vendorSession;
        $this->_galleryResource = $galleryResource;
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        if (!$this->_vendorSession->isLoggedIn()) {
            return $result->setContents(__("Vendor not logged in"));
        }
        $data = $this->getRequest()->getParam('data');
        if (!is_array($data)) {
            $data = [];
        }
        $model = $this->_galleryResource->loadByVendorId($this->_vendorSession->getVendorId());
        $model->setGallery(json_encode($data));
        try {
            $this->_galleryResource->save($model);
        } catch (\Exception $e) {
            return $result->setContents(__("Can't save gallery"));
        }
        return $result->setContents(__("Ok"));
    }
}
