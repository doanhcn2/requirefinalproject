<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/21/18
 * Time: 4:02 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Offer;

use Magenest\Hotel\Model\PackagePrice;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Bulk
 * @package Magenest\Hotel\Controller\Vendor\Offer
 */
class Bulk extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packageFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails
     */
    protected $_packageResource;

    /**
     * @var \Magenest\Hotel\Model\PackagePriceFactory
     */
    protected $_packagePriceFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackagePrice
     */
    protected $_priceResource;

    /**
     * Disable constructor.
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource
     * @param \Magenest\Hotel\Model\ResourceModel\PackagePrice $priceResource
     * @param \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice $priceResource,
        \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory,
        \Magento\Framework\App\Action\Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession
    ) {
        parent::__construct($context);
        $this->_vendorSession = $vendorSession;
        $this->_packageFactory = $packageFactory;
        $this->_packageResource = $packageResource;
        $this->_priceResource = $priceResource;
        $this->_packagePriceFactory = $packagePriceFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (!isset($params['qty'],
                $params['sale_price'],
                $params['orig_price'],
                $params['first_date'],
                $params['last_date'],
                $params['package_id'])
            || !is_numeric($params['qty'])
            || !is_numeric($params['sale_price'])
            || !is_numeric($params['orig_price'])
            || !strtotime($params['first_date'])
            || !strtotime($params['last_date'])
            || $params['sale_price'] > $params['orig_price']
        ) {
            throw new LocalizedException(__("Invalid request"));
        }

        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }

        $packageId = $params['package_id'];
        /** @var \Magenest\Hotel\Model\PackageDetails $package */
        $package = $this->_packageFactory->create();
        $this->_packageResource->load($package, $packageId);
        if ((int)$package->getVendorId() !== (int)$this->_vendorSession->getVendorId()) {
            throw new LocalizedException(__("Invalid package id"));
        }

        $firstDate = new \DateTime($params['first_date']);
        $lastDate = new \DateTime($params['last_date']);
        $interval = new \DateInterval("P1D");

        for ($d = $firstDate; $d <= $lastDate; $d->add($interval)) {
            $date = $d->format('Y-m-d');
            /** @var \Magenest\Hotel\Model\PackagePrice $packagePrice */
            $packagePrice = $this->_packagePriceFactory->create();
            $packagePrice = $packagePrice->loadByPackageIdAndDate($packageId, $date);
            $packagePrice->addData([
                'sale_price' => $params['sale_price'],
                'orig_price' => $params['orig_price'],
                'qty' => $params['qty'],
            ]);
            $this->_priceResource->save($packagePrice);
//            $return[] = $d->getTimestamp();
        }

        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $result->setContents("ok");
        return $result;
    }
}
