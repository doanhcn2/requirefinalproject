<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/21/18
 * Time: 10:26 AM
 */

namespace Magenest\Hotel\Controller\Vendor\Package;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Disable
 * @package Magenest\Hotel\Controller\Vendor\Package
 */
class Disable extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory
     */
    protected $_packagePriceCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packageFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails
     */
    protected $_packageResource;

    /**
     * Disable constructor.
     * @param \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory,
        \Magento\Framework\App\Action\Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession
    ) {
        parent::__construct($context);
        $this->_vendorSession = $vendorSession;
        $this->_packageFactory = $packageFactory;
        $this->_packageResource = $packageResource;
        $this->_packagePriceCollectionFactory = $packagePriceCollectionFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }

        $packageId = $this->getRequest()->getParam('package_id');
        $date = $this->getRequest()->getParam('date');
        if (!$packageId || !strtotime($date)) {
            throw new LocalizedException(__("Invalid request"));
        }

        /** @var \Magenest\Hotel\Model\PackageDetails $package */
        $package = $this->_packageFactory->create();
        $this->_packageResource->load($package, $packageId);
        if ((int)$package->getVendorId() !== (int)$this->_vendorSession->getVendorId()) {
            throw new LocalizedException(__("Invalid package id"));
        }

        /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
        $packagePriceCollection = $this->_packagePriceCollectionFactory->create();
        $packagePriceCollection->addDateFilter(date('Y-m-d', strtotime($date)))
            ->addPackageFilter($packageId)
            ->walk('delete');

        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $result->setContents("ok");
        return $result;
    }
}
