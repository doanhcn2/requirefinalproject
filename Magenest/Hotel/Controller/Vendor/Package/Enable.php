<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/21/18
 * Time: 1:34 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Package;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Enable
 * @package Magenest\Hotel\Controller\Vendor\Package
 */
class Enable extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackagePrice
     */
    protected $_packagePriceResource;

    /**
     * @var \Magenest\Hotel\Model\PackagePriceFactory
     */
    protected $_packagePriceFactory;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packageFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails
     */
    protected $_packageResource;

    /**
     * Enable constructor.
     * @param \Magenest\Hotel\Model\ResourceModel\PackagePrice $packagePriceResource
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource
     * @param \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackagePrice $packagePriceResource,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageResource,
        \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageFactory,
        \Magento\Framework\App\Action\Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession
    ) {
        parent::__construct($context);
        $this->_vendorSession = $vendorSession;
        $this->_packageFactory = $packageFactory;
        $this->_packageResource = $packageResource;
        $this->_packagePriceFactory = $packagePriceFactory;
        $this->_packagePriceResource = $packagePriceResource;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);

        $params = $this->getRequest()->getParams();
        if (!isset($params['qty'],
                $params['sale_price'],
                $params['orig_price'])
            || !is_numeric($params['qty'])
            || !is_numeric($params['sale_price'])
            || !is_numeric($params['orig_price'])
        ) {
            throw new LocalizedException(__("Invalid request"));
        }

        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }

        $packageId = $this->getRequest()->getParam('package_id');
        $date = $this->getRequest()->getParam('date');
        if (!$packageId || !strtotime($date)) {
            throw new LocalizedException(__("Invalid request"));
        }

        /** @var \Magenest\Hotel\Model\PackageDetails $package */
        $package = $this->_packageFactory->create();
        $this->_packageResource->load($package, $packageId);
        if ((int)$package->getVendorId() !== (int)$this->_vendorSession->getVendorId()) {
            throw new LocalizedException(__("Invalid package id"));
        }

        /** @var \Magenest\Hotel\Model\PackagePrice $priceUnit */
        $priceUnit = $this->_packagePriceFactory->create();
        $priceUnit->setData([
            'package_id' => $packageId,
            'sale_price' => $params['sale_price'],
            'orig_price' => $params['orig_price'],
            'qty' => $params['qty'],
            'date' => $date
        ]);

        try {
            $this->_packagePriceResource->save($priceUnit);
        } catch (\Exception $e) {
            $result->setContents("save error");
            return $result;
        }
        $result->setContents("ok");
        return $result;
    }
}
