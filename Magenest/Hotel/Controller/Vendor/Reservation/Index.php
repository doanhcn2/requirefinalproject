<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/12/18
 * Time: 7:09 PM
 */

namespace Magenest\Hotel\Controller\Vendor\Reservation;


class Index extends \Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'magenest_hotel_reservation');
    }
}
