<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 14/04/2018
 * Time: 16:09
 */

namespace Magenest\Hotel\Controller\Vendor\Reservation;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class ExportCsv
 * @package Magenest\Hotel\Controller\Vendor\Reservation
 */
class ExportCsv extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $directory;

    /**
     * @var \Magenest\Hotel\Block\Order\Vendor\Listing
     */
    protected $orderBlock;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * ExportCsv constructor.
     * @param \Magenest\Hotel\Block\Order\Vendor\Listing $orderBlock
     * @param Filesystem $filesystem
     * @param FileFactory $fileFactory
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        \Magenest\Hotel\Block\Order\Vendor\Listing $orderBlock,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->orderBlock = $orderBlock;
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->getRequest()->getParams();

            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($data) {
                $heading = [
                    __('Reservation No.'),
                    __('Guest Name'),
                    __('Room Types'),
                    __('Arrival'),
                    __('Departure'),
                    __('Nights'),
                    __('Rate Category'),
                    __('Total Paid'),
                    __('Date Booked')
                ];

                $collection = $this->orderBlock->getHotelOrders();
//                $collection->load();
                if ($data['chosen'] !== 'all') {
                    $chosen = json_decode($data['chosen'], true);
//                    $collection = $collection->addFieldToFilter("id", array('in' => json_decode($data['chosen'], true)));
                    $collection->getSelect()->where("`main_table`.`id` IN(?)", $chosen);
                }
                $items = [];
                /** @var  \Magenest\Hotel\Model\HotelOrder $order */
                foreach ($collection as $order) {
                    $arrivalDate = strtotime($order->getCheckInDate());
                    $departureDate = strtotime($order->getCheckOutDate());
                    $night = ($departureDate - $arrivalDate) / 86400;
                    $createdAt = strtotime($order->getCreatedAt());
                    $createdDate = date('D, d M Y', $createdAt);
                    $createdTime = date('g:i A', $createdAt);
                    $items[] = [
                        $order->getId(),
                        $order->getCustomerName(),
                        "type1,typ2,typ3",
                        date('D, d M Y', $arrivalDate),
                        date('D, d M ', $departureDate),
                        $night,
                        $this->orderBlock->getProductName($order->getProductId()),
                        $this->orderBlock->getCurrencySymbol($order->getCurrency()) . $order->getTotalPaid(),
                        $createdDate . "\n at" . $createdTime
                    ];
                }
                $file = 'export/ListOrders' . date('Ymd_His') . '.csv';
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach ($items as $item) {
                    $stream->writeCsv($item);

                }

                return $this->fileFactory->create('ListOrders' . date('Ymd_His') . '.csv', [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('hotel/vendor_reservation/');
    }

//    protected function getPackage($productId)
//    {
//        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $offer */
//        $offer = $this->_packageDetailsFactory->create()->getCollection();
//        $offer = $offer->getItemByProductId($productId);
//
//        return $offer;
//    }

//    protected function getServices($productId)
//    {
//
//    }
}
