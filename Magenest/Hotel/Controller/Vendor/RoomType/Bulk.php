<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/21/18
 * Time: 4:02 PM
 */

namespace Magenest\Hotel\Controller\Vendor\RoomType;

use Magenest\Hotel\Model\ResourceModel\PackageDetails;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Bulk
 * @package Magenest\Hotel\Controller\Vendor\RoomType
 */
class Bulk extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory
     */
    protected $_packageCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor
     */
    protected $_roomTypeResource;

    /**
     * @var \Magenest\Hotel\Model\PackagePriceFactory
     */
    protected $_packagePriceFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackagePrice
     */
    protected $_priceResource;

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor $roomTypeResource,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice $priceResource,
        \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeFactory,
        \Magento\Framework\App\Action\Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession
    ) {
        parent::__construct($context);
        $this->_vendorSession = $vendorSession;
        $this->_priceResource = $priceResource;
        $this->_roomTypeResource = $roomTypeResource;
        $this->_roomTypeVendorFactory = $roomTypeFactory;
        $this->_packagePriceFactory = $packagePriceFactory;
        $this->_packageCollectionFactory = $packageCollectionFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (!isset($params['qty'],
                $params['sale_price'],
                $params['orig_price'],
                $params['first_date'],
                $params['last_date'],
                $params['room_type'])
            || !is_numeric($params['qty'])
            || !is_numeric($params['sale_price'])
            || !is_numeric($params['orig_price'])
            || !strtotime($params['first_date'])
            || !strtotime($params['last_date'])
            || $params['sale_price'] > $params['orig_price']
        ) {
            throw new LocalizedException(__("Invalid request"));
        }

        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }

        $roomTypeId = $params['room_type'];
        /** @var \Magenest\Hotel\Model\RoomTypeVendor $roomType */
        $roomType = $this->_roomTypeVendorFactory->create();
        $this->_roomTypeResource->load($roomType, $roomTypeId);
        if ((int)$roomType->getVendorId() !== (int)$this->_vendorSession->getVendorId()) {
            throw new LocalizedException(__("Invalid room type"));
        }

        $firstDate = new \DateTime($params['first_date']);
        $lastDate = new \DateTime($params['last_date']);
        $interval = new \DateInterval("P1D");

        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packages */
        $packages = $this->_packageCollectionFactory->create();
        $packages->addVendorRoomFilter($roomTypeId);

        /** @var \Magenest\Hotel\Model\PackageDetails $package */
        foreach ($packages as $package) {
//            $d = $firstDate;
            $packageId = $package->getId();
            for ($d = clone $firstDate; $d <= $lastDate; $d->add($interval)) {
            $date = $d->format('Y-m-d');
            /** @var \Magenest\Hotel\Model\PackagePrice $packagePrice */
            $packagePrice = $this->_packagePriceFactory->create();
            $packagePrice = $packagePrice->loadByPackageIdAndDate($packageId, $date);
            $packagePrice->addData([
                'sale_price' => $params['sale_price'],
                'orig_price' => $params['orig_price'],
                'qty' => $params['qty'],
            ]);
            $this->_priceResource->save($packagePrice);
//            $return[] = $d->getTimestamp();
            }
        }
        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $result->setContents("ok");
        return $result;
    }
}
