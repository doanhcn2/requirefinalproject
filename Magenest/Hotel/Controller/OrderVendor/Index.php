<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/9/18
 * Time: 8:21 AM
 */

namespace Magenest\Hotel\Controller\OrderVendor;


class Index extends \Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'magenest_hotel');
    }
}