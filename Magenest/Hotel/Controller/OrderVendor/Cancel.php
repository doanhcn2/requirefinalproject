<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/5/18
 * Time: 1:26 AM
 */

namespace Magenest\Hotel\Controller\OrderVendor;


use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magenest\Hotel\Helper\BookingStatus as Status;

class Cancel extends \Magento\Framework\App\Action\Action
{
    protected $_hotelOrderCollectionFactory;

    public function __construct(
        Context $context,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory
    ) {
        parent::__construct($context);
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
//        $this->_eventManager->dispatch('magenest_hotel_cancel_order', ['booking_id' => $id]); return;
        if (!$id) {
            $this->messageManager->addErrorMessage(__("No ID found"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        /** @var \Unirgy\Dropship\Model\Session $vendorSession */
        $vendorSession = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        if (!$vendorSession->isLoggedIn()) {
            $this->messageManager->addErrorMessage(__("Vendor not logged in"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        $vendorId = $vendorSession->getVendorId();
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $collection */
        $collection = $this->_hotelOrderCollectionFactory->create();
        $packageTbl = $collection->getTable('magenest_hotel_package_details');
        /** @var \Magenest\Hotel\Model\HotelOrder $model */
        $model = $collection->addFieldToFilter('status', Status::PENDING)
            ->join(
                $packageTbl,
                "`main_table`.`package_id`=`$packageTbl`.`id` AND `$packageTbl`.`vendor_id`=$vendorId AND `main_table`.`id`=$id",
                'vendor_id'
            )
            ->getFirstItem();
        if (!$model->getId()) {
            $this->messageManager->addErrorMessage(__("No record available"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        try {
            $model->setData('status', Status::CANCELLED)->save();
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __("Something went wrong."));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        $this->_eventManager->dispatch('magenest_hotel_cancel_order', ['booking_id' => $id]);
        $this->messageManager->addSuccessMessage(__("Cancelled"));
        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}