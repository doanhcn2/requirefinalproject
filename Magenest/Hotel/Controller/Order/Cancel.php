<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/5/18
 * Time: 1:26 AM
 */

namespace Magenest\Hotel\Controller\Order;


use Magento\Framework\App\Action\Context;
use Magenest\Hotel\Helper\BookingStatus as Status;

class Cancel extends \Magento\Framework\App\Action\Action
{
    protected $_customerSession;
    protected $_hotelOrderCollectionFactory;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory
    ) {
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            $this->messageManager->addErrorMessage(__("No ID found"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $collection */
        $collection = $this->_hotelOrderCollectionFactory->create();
        /** @var \Magenest\Hotel\Model\HotelOrder $model */
        $model = $collection->addFieldToFilter($collection->getIdFieldName(), $id)
            ->addFieldToFilter('status', Status::PENDING)
            ->addFieldToFilter('customer_id', $this->_customerSession->getCustomerId())
            ->getFirstItem();
        if (!$model->getId()) {
            $this->messageManager->addErrorMessage(__("No record available"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        try {
            $model->setData('status', Status::CANCELLED)->save();
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __("Something went wrong."));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        $this->messageManager->addSuccessMessage(__("Cancelled"));
        $this->_eventManager->dispatch('magenest_hotel_cancel_order', ['booking_id' => $id]);
        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}