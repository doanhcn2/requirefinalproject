<?php

namespace Magenest\Hotel\Controller\RoomType;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_roomTypeVendorFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
    )
    {
        parent::__construct($context);
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->_request->getParams();

            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            $roomTypeVendor = $this->_roomTypeVendorFactory->create();
            if ($data) {
                if (array_key_exists("id", $data)) {
                    if ($data['id'] !== '') {
                        $roomTypeVendor->load($data['id']);
                    } else {
                        unset($data['id']);
                    }
                }
                if (@$data['has_extra_beds'] == "true")
                    $data['has_extra_beds'] = 1;
                else
                    $data['has_extra_beds'] = 0;
                if (is_array($data['beds_type']))
                    $data['beds_type'] = json_encode($data['beds_type']);
                else {
                    $data['beds_type'] = null;
                }
                $data['vendor_id'] = $vendorId;
                $roomTypeVendor->addData($data);
                $roomTypeVendor->save();
                $rooms = $this->_roomTypeVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
                $listRooms = [];
                foreach ($rooms as $room) {
                    /** @var \Magenest\Hotel\Model\RoomTypeVendor $room */

                    $listRooms [] = [
                        'id' => $room->getId(),
                        'roomType' => $room->getRoomTypeAdminId(),
                        'roomTitle' => $room->getRoomTitle(),
                        'numberRooms' => $room->getNumberRoomsAvailable(),
                        'smoking' => $room->getNonSmoking(),
                        'description' => $room->getDescription(),
                        'beds' => $room->getBedsType(),
                        'numberGuests' => $room->getNumberGuests(),
                        'roomSize' => $room->getRoomSize(),
                        'hasExtraBeds' => $room->getHasExtraBeds(),
                        'numberExtraBeds' => $room->getNumberExtraBeds(),
                        'priceBed' => $room->getPricePerBed(),
                        'photoRoom'=> $room->getPhotoRoom()
                    ];
                }
                $result->setData([
                    'success' => true,
                    'rooms' => $listRooms
                ]);
            }
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}