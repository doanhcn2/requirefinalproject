<?php

namespace Magenest\Hotel\Controller\RoomType;

class Delete extends \Magento\Framework\App\Action\Action
{
    protected $_roomTypeVendorFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
    ) {
        parent::__construct($context);
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->_request->getParams();

//            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            $roomTypeVendor = $this->_roomTypeVendorFactory->create();
//            $hotelProperty = $hotelProperty->loadByVendorId($vendorId);
            if($data){
                $roomTypeVendor->load($data['id']);
                $roomTypeVendor->delete();
                $rooms = $this->_roomTypeVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorSession->getVendorId());
                $listRooms = [];
                foreach ($rooms as $room) {
                    $listRooms [] = [
                        'id'=>$room->getId(),
                        'roomTitle'=>$room->getRoomTitle(),
                        'numberRooms'=>$room->getNumberRoomsAvailable(),
                        'smoking'=>$room->getNonSmoking(),
                        'description'=>$room->getDescription(),
                        'beds'=>$room->getBedTypes(),
                        'numberGuests'=>$room->getNumberGuests(),
                        'roomSize'=>$room->getRoomSize(),
                        'hasExtraBeds'=>$room->getHasExtraBeds(),
                        'numberExtraBeds'=>$room->getNumberExtraBeds(),
                        'priceBed'=>$room->getPricePerBed()
                    ];
                }
                $result->setData([
                    'success' => true,
                    'rooms'=>json_encode($listRooms)
                ]);
            }
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}