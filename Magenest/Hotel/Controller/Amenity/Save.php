<?php

namespace Magenest\Hotel\Controller\Amenity;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_amenityVendorFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenityVendorFactory
    )
    {
        parent::__construct($context);
        $this->_amenityVendorFactory = $amenityVendorFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->_request->getParams();
            $vendorId = $vendorSession->getVendorId();
            $deleteFirst = $this->_amenityVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
            foreach ($deleteFirst as $item) {
                $item->delete();
            }

            foreach ($data as $amenityId => $rooms) {
                foreach ($data[$amenityId] as $item) {
                    $amenity = $this->_amenityVendorFactory->create();
                    $amenity->addData([
                        "vendor_id" => $vendorId,
                        "room_vendor_id" => $item,
                        "amenity" => $amenityId
                    ])->save();
                }
            }
            $result->setData([
                'success' => true
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}