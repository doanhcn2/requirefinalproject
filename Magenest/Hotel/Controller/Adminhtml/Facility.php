<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Hotel extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Hotel
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Hotel\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin\CollectionFactory as FacilityCollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Hotel
 * @package Magenest\Hotel\Controller\Adminhtml
 */
abstract class Facility extends Action
{
    /**
     * @var RoomAmenitiesAdminFactory
     */
    protected $_facilityFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var TemplateCollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Hotel constructor.
     * @param RoomAmenitiesAdminFactory $amenityFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param AmenityCollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        \Magenest\Hotel\Model\PropertyFacilityAdminFactory $facilityFactory,
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        FacilityCollectionFactory $collectionFactory,
        Filter $filter
    ) {
        $this->_facilityFactory = $facilityFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }
    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Hotel::facility');
        $resultPage->getConfig()->getTitle()->prepend((__('Facility')));
        return $this;
    }

    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Hotel::facility');
    }
}
