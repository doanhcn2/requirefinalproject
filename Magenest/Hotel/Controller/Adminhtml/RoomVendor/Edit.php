<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Adminhtml\RoomVendor;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Magenest\Hotel\Controller\Adminhtml\RoomVendor
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend("Edit Room Type");

        return $resultPage;
    }
}