<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Adminhtml\RoomVendor;

use Magento\Backend\App\Action;
use Magenest\Hotel\Model\Product\ScreenShot\Config;


/**
 * Class Save
 * @package Magenest\Hotel\Controller\Adminhtml\RoomVendor
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Magenest\Hotel\Helper\File
     */
    protected $helperFile;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param Config $config
     * @param \Magenest\Hotel\Helper\File $helperFile
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     */
    public function __construct(
        Action\Context $context,
        Config $config,
        \Magenest\Hotel\Helper\File $helperFile,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory

    ) {
        $this->helperFile = $helperFile;
        $this->config = $config;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        parent::__construct($context);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if (!@$post['room_type_vendor']) {
            return $resultRedirect->setPath('*/*/');
        }
        $roomData = $post['room_type_vendor'];
        try {
            $model = $this->_roomTypeVendorFactory->create()->load($roomData['room_id']);
            if (@$roomData) {
                if ($roomData['room_id'] != "")
                if (is_array(@$roomData['list_bed_type']['add_bed_type']))
                    $roomData['beds_type'] = json_encode($roomData['list_bed_type']['add_bed_type']);
                else
                    $roomData['beds_type'] = null;
                if(@$roomData['room_photos']){
                    if (@$roomData['room_photos']['images'] && is_array($roomData['room_photos']['images'])) {
                        $this->saveGalleryRooms($roomData['room_photos']['images'], $model);
                    }
                }

                $model->addData($roomData)->save();
                $this->messageManager->addSuccessMessage(__('Room Type Vendor has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            }
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit',
                    ['vendor_id' => $roomData['vendor_id'],'room_id' => $roomData['room_id'], '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/',['vendor_id' => $roomData['vendor_id']]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('*/*/edit', ['vendor_id' => $roomData['vendor_id'],'room_id' => $roomData['room_id'], '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');

    }

    /**
     * @param $galleryList
     * @param $vendorId
     */
    protected function saveGalleryRooms($galleryList, $roomTypeVendor)
    {
        if (is_array($galleryList)) {
            $photos = json_decode($roomTypeVendor->getPhotoRoom(), true);
            if ($photos === null)
                $photos = [];
            foreach ($galleryList as $key => $screenshot) {
                try {
                    $isRemoved = (bool)$screenshot['removed'];
                    if ($isRemoved && in_array($screenshot['file'], $photos)) {
                        $photos = array_diff($photos, [$screenshot['file']]);

                        $this->deleteScreenshotFile($screenshot['file']);
                        continue;
                    } elseif (!$isRemoved && in_array($screenshot['file'], $photos))
                        continue;
                    if (strpos($screenshot['file'], ".tmp") >= 0) {
                        $newPhoto = $this->moveScreenshotRoomFromTmp($screenshot['file']);
                        $photos [] = $newPhoto;
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }
            if (is_array($photos)) {
                $photos = json_encode(array_values($photos));
                $roomTypeVendor->setPhotoRoom($photos)->save();
            }
        }
    }

    /**
     * Move images from tmp directory to base directory
     *
     * @param $fileName
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function moveScreenshotRoomFromTmp($fileName)
    {
        $config = $this->config;
        return $this->helperFile->moveFileFromTmp(
            $config->getBaseTmpMediaPath(),
            $config->getBaseMediaPath(),
            $fileName
        );
    }


    /**
     * Delete image
     *
     * @param $path
     */
    protected function deleteScreenshotFile($path)
    {
        $this->helperFile->deleteFile($this->config->getBaseMediaPath() . $path);
    }

}
