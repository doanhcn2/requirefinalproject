<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\RoomVendor;

use Magenest\Hotel\Controller\Adminhtml\RoomVendor as RoomVendorController;

/**
 * Class Index
 * @package Magenest\Hotel\Controller\Adminhtml\RoomVendor
 */
class Index extends RoomVendorController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {

        $this->_setPageData();
        return $this->getResultPage();
    }
}
