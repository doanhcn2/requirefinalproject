<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\Facility;

use Magento\Framework\Controller\ResultFactory;
use Magenest\Hotel\Controller\Adminhtml\Facility as FacilityController;

/**
 * Class MassDelete
 * @package Magenest\Hotel\Controller\Adminhtml\Facility
 */
class MassDelete extends FacilityController
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());

        $delete = 0;
        foreach ($collection as $item) {
            $item->delete();
            $delete++;
        }
        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $delete));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
