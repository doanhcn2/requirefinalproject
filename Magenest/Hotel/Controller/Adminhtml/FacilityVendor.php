<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Hotel extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Hotel
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Hotel\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory as FacilityVendorCollectionFactory;
use Magento\Ui\Component\MassAction\Filter;


/**
 * Class FacilityVendor
 * @package Magenest\Hotel\Controller\Adminhtml
 */
abstract class FacilityVendor extends Action
{

    /**
     * @var \Magenest\Hotel\Model\PropertyFacilityVendorFactory
     */
    protected $_facilityVendorFactory;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var
     */
    protected $_resultPage;

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var FacilityVendorCollectionFactory
     */
    protected $_collectionFactory;

    /**
     * FacilityVendor constructor.
     * @param \Magenest\Hotel\Model\PropertyFacilityVendorFactory $facilityVendorFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param FacilityVendorCollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        \Magenest\Hotel\Model\PropertyFacilityVendorFactory $facilityVendorFactory,
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        FacilityVendorCollectionFactory $collectionFactory,
        Filter $filter
    ) {
        $this->_facilityVendorFactory = $facilityVendorFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    /**
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Hotel::facility_vendor');
        $resultPage->getConfig()->getTitle()->prepend((__('Facility Vendor')));
        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Hotel::facility_vendor');
    }
}
