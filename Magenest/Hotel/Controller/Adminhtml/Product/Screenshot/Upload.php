<?php
namespace Magenest\Hotel\Controller\Adminhtml\Product\Screenshot;

use Magento\Framework\Controller\ResultFactory;

class Upload extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::products';

    /**
     * @return mixed
     */
    public function execute()
    {
        try {
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'image']
            );
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
            $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
            $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');

            /** @var \Magenest\Hotel\Model\Product\Screenshot\Config $config */
            $config = $this->_objectManager->get('Magenest\Hotel\Model\Product\ScreenShot\Config');

            /** @var \Magenest\Hotel\Helper\File $helperFile */
            $helperFile = $this->_objectManager->create('Magenest\Hotel\Helper\File');
            $result = $helperFile->uploadFromTmp($config->getBaseTmpMediaPath(), $uploader);

            $this->_eventManager->dispatch(
                'catalog_product_screenshot_upload_image_after',
                ['result' => $result, 'action' => $this]
            );

            unset($result['tmp_name']);
            unset($result['path']);

            $result['url'] = $config->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
