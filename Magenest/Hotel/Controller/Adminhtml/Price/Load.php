<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/8/18
 * Time: 9:28 AM
 */

namespace Magenest\Hotel\Controller\Adminhtml\Price;

use Magento\Backend\App\Action;

class Load extends Action
{
    protected $_packagePriceCollectionFactory;
    protected $_packageDetailsCollectionFactory;

    public function __construct(
        Action\Context $context,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailCollectionFactory
    ) {
        parent::__construct($context);
        $this->_packagePriceCollectionFactory = $packagePriceCollectionFactory;
        $this->_packageDetailsCollectionFactory = $packageDetailCollectionFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            $productId = $this->getRequest()->getParam('productId');
            if (!$productId) {
                $result->setData([
                    'success' => true,
                    'is_new' => true
                ]);
                return $result;

            }
            /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
            $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
            $packageId = $packageDetailsCollection->getItemByProductId($productId)->getId();

            /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
            $packagePriceCollection = $this->_packagePriceCollectionFactory->create();
            $prices = $packagePriceCollection->getAllPriceByPackage($packageId);
            $data = [];
            foreach ($prices as $price) {
                $data[$price->getDate()] = [
                    'salePrice' => $price->getSalePrice(),
                    'origPrice' => $price->getOrigPrice(),
                    'roomQty' => $price->getQty()
                ];
            }

            $result->setData([
                'success' => true,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}