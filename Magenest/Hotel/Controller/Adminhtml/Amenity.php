<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Hotel extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Hotel
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Hotel\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Hotel\Model\ResourceModel\RoomAmenitiesAdmin\CollectionFactory as AmenityCollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Hotel
 * @package Magenest\Hotel\Controller\Adminhtml
 */
abstract class Amenity extends Action
{
    /**
     * @var RoomAmenitiesAdminFactory
     */
    protected $_amenityFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var TemplateCollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Hotel constructor.
     * @param RoomAmenitiesAdminFactory $amenityFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param AmenityCollectionFactory $collectionFactory
     * @param Filter $filter
     */
    public function __construct(
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenityFactory,
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        AmenityCollectionFactory $collectionFactory,
        Filter $filter
    ) {
        $this->_amenityFactory = $amenityFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_collectionFactory = $collectionFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }
    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Hotel::amenity');
        $resultPage->getConfig()->getTitle()->prepend((__('Amenity')));
        return $this;
    }

    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Hotel::amenity');
    }
}
