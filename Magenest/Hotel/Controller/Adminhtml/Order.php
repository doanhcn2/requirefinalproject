<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Hotel extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Hotel
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Hotel\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Order
 * @package Magenest\Hotel\Controller\Adminhtml
 */
abstract class Order extends Action
{
    protected $_customerRepository;
    protected $_productRepository;
    protected $_orderRepository;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var \Magenest\Hotel\Model\HotelOrderFactory
     */
    protected $_collectionFactory;

    /**
     * Order constructor.
     * @param \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param Filter $filter
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        Filter $filter
    ) {
        $this->_collectionFactory = $hotelOrderFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_customerRepository = $customerRepository;
        $this->_productRepository = $productRepository;
        $this->_orderRepository = $orderRepository;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }
    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Hotel::hotel_order');
        $resultPage->getConfig()->getTitle()->prepend((__('Hotel Orders')));
        return $this;
    }

}
