<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\Amenity;

use Magenest\Hotel\Controller\Adminhtml\Amenity as AmenityController;

/**
 * Class Index
 * @package Magenest\Hotel\Controller\Adminhtml\Amenity
 */
class Index extends AmenityController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }
}
