<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\Amenity;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Magenest\Hotel\Controller\Adminhtml\Amenity
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $id = $this->getRequest()->getParam('id');
        if($id){
            $model = $this->_objectManager->create('\Magenest\Hotel\Model\RoomAmenitiesAdmin')->load($id);
            $title = "Edit Amenity: ".$model->getTitle();
        }else{
            $title = 'New Amenity';
        }
        $resultPage->getConfig()->getTitle()->prepend($title);

        return $resultPage;
    }
}