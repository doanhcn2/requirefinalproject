<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Hotel extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Hotel
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Hotel\Controller\Adminhtml;

use Magento\Framework\App\ObjectManager;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Hotel\Model\Product\ScreenShot\Config;

/**
 * Class RoomVendor
 * @package Magenest\Hotel\Controller\Adminhtml
 */
abstract class RoomVendor extends Action
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;


    /**
     * @var \Magenest\Hotel\Helper\File
     */
    protected $helperFile;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    protected $_collectionFactory;

    /**
     * Property constructor.
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Filter $filter
     */
    public function __construct(
        Registry $coreRegistry,
        Context $context,
        Config $config,
        PageFactory $resultPageFactory,
        \Magenest\Hotel\Helper\File $helperFile,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        Filter $filter
    )
    {
        $this->_collectionFactory = $roomTypeVendorFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->helperFile = $helperFile;
        $this->config = $config;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Hotel::property');
        $vendor = ObjectManager::getInstance()->create('\Unirgy\Dropship\Model\Vendor')->load($this->getRequest()->getParam('vendor_id'));
        $resultPage->getConfig()->getTitle()->prepend((__('Hotel : ' . $vendor->getVendorName())));
        return $this;
    }

}
