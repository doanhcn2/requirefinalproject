<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 21/02/2018
 * Time: 14:36
 */

namespace Magenest\Hotel\Controller\Adminhtml\Order;
use Magenest\Hotel\Controller\Adminhtml\Order as OrderController;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ObjectManager;
use Magenest\Hotel\Helper\BookingStatus as Status;

class Cancel extends OrderController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {

        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            $this->messageManager->addErrorMessage(__("No ID found"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $collection */
        $collection = $this->_collectionFactory->create();
        $packageTbl = $collection->getTable('magenest_hotel_package_details');
        /** @var \Magenest\Hotel\Model\HotelOrder $model */
        $model = $collection->addFieldToFilter('status', Status::PENDING)
            ->join(
                $packageTbl,
                "`main_table`.`package_id`=`$packageTbl`.`id` AND `main_table`.`id`=$id",
                ''
            )
            ->getFirstItem();
        if (!$model->getId()) {
            $this->messageManager->addErrorMessage(__("No record available"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        try {
            $model->setData('status', Status::CANCELLED)->save();
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __("Something went wrong."));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }
        $this->_eventManager->dispatch('magenest_hotel_cancel_order', ['booking_id' => $id]);
        $this->messageManager->addSuccessMessage(__("Cancelled."));
        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}