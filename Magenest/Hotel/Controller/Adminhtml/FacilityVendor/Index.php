<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\FacilityVendor;

use Magenest\Hotel\Controller\Adminhtml\FacilityVendor as FacilityVendorController;

/**
 * Class Index
 * @package Magenest\Hotel\Controller\Adminhtml\FacilityVendor
 */
class Index extends FacilityVendorController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }
}