<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\FacilityVendor;


/**
 * Class Save
 * @package Magenest\Hotel\Controller\Adminhtml\FacilityVendor
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        \Zend_Debug::dump($post);
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$post) {
            return $resultRedirect->setPath('*/*/');
        }
        try {
            $array=[
                'facility_id' => $post['facility_id'],
                'vendor_id' => $post['vendor_id'],
            ];
            $model = $this->_objectManager->create('Magenest\Hotel\Model\PropertyFacilityVendor');
            if ($post['id'] != null) {
                $model->load($post['id']);
            }
            $model->addData($array);
            $model->save();
            $this->messageManager->addSuccessMessage(__('The Facility Vendor has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('*/*/edit' ,['id'=>$this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');

    }

}
