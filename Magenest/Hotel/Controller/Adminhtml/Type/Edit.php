<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\Type;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $model = null;
        $title = 'Template Form';
        // 4. Register model to use later in blocks
        $this->_objectManager->create('Magento\Framework\Registry')->register('ticket_template', $model);

        $resultPage->getConfig()->getTitle()
            ->prepend($title);
        return $resultPage;
    }

}