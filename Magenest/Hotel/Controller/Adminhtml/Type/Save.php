<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Controller\Adminhtml\Type;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
/**
 * Class Save
 * @package Magenest\Hotel\Controller\Adminhtml\Type
 */
class Save extends Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * Save constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if (isset($post['add_room_type']) && count($post['add_room_type']) > 0) {
                $this->saveRoom($post['add_room_type']);
            } else {
                $modelRoom = $this->_objectManager->create('Magenest\Hotel\Model\RoomTypeAdmin')->getCollection();
                foreach ($modelRoom as $room) {
                    $room->delete();
                }
            }

            if (isset($post['add_bed_type']) && count($post['add_bed_type']) > 0) {
                $this->saveBed($post['add_bed_type']);
            } else {
                $modelBed = $this->_objectManager->create('Magenest\Hotel\Model\BedType')->getCollection();
                foreach ($modelBed as $bed) {
                    $bed->delete();
                }
            }

            $this->messageManager->addSuccessMessage(__('The template has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            return $resultRedirect->setPath('*/*/edit');
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the rule.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('*/*/edit' );
        }
        return $resultRedirect->setPath('*/*/edit');
    }

    /**
     * save room type
     * @param $data
     */
    public function saveRoom($data)
    {
        $modelRoom= $this->_objectManager->create('Magenest\Hotel\Model\RoomTypeAdmin')->getCollection();
        $oldList = [];
        foreach ($modelRoom as $list) {
            $id = $list->getId();
            array_push($oldList, $id);
        }
        $availableList = [];

        foreach ($data as $room) {
            $model = $this->_objectManager->create('Magenest\Hotel\Model\RoomTypeAdmin');
            if (isset($room['id']) && $room['id']== null) {
                $result = [
                    'room_type' => $room['room_type']
                ];
                $model->addData($result)->save();
            } else {
                array_push($availableList,$room['id']);
                $model->load($room['id']);
                $result = [
                    'room_type' => $room['room_type']
                ];

                $model->addData($result)->save();
            }
        }

        $delete = array_diff($oldList, $availableList);
        if (!empty($delete)) {
            foreach ($delete as $value) {
                $this->_objectManager->create('Magenest\Hotel\Model\RoomTypeAdmin')->load($value)->delete();
            }
        }
    }

    /**
     * save bed type
     * @param $data
     */
    public function saveBed($data)
    {
        $modelBed= $this->_objectManager->create('Magenest\Hotel\Model\BedType')->getCollection();
        $oldList = [];
        foreach ($modelBed as $list) {
            $id = $list->getId();
            array_push($oldList, $id);
        }

        $availableList = [];
        foreach ($data as $bed) {
            $model = $this->_objectManager->create('Magenest\Hotel\Model\BedType');
            if (isset($bed['id']) && $bed['id']== null) {
                array_push($availableList,$bed['id']);
                $result = [
                    'bed_type' => $bed['bed_type']
                ];
                $model->addData($result)->save();
            } else {
                array_push($availableList,$bed['id']);
                $result = [
                    'bed_type' => $bed['bed_type']
                ];
                $model->load($bed['id']);
                $model->addData($result)->save();
            }
        }

        $delete = array_diff($oldList, $availableList);
        if (!empty($delete)) {
            foreach ($delete as $value) {
                $this->_objectManager->create('Magenest\Hotel\Model\BedType')->load($value)->delete();
            }
        }
    }
}
