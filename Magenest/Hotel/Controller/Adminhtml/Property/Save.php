<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Adminhtml\Property;

use Magento\Backend\App\Action;
use Magento\Framework\App\ObjectManager;
use Magenest\Hotel\Model\Product\ScreenShot\Config;

/**
 * Class Save
 * @package Magenest\Hotel\Controller\Adminhtml\Property
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Magenest\Hotel\Helper\File
     */
    protected $helperFile;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory
     */
    protected $_propertyCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomAmenitiesVendorFactory
     */
    protected $_roomAmenVendorCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    protected $_propertyFacilityVendorFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param Config $config
     * @param \Magenest\Hotel\Helper\File $helperFile
     * @param \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory $propertyFactory
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magenest\Hotel\Model\PropertyFacilityVendorFactory $roomFacilityVendorFactory
     * @param \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenVendorCollectionFactory
     */
    public function __construct(
        Action\Context $context,
        Config $config,
        \Magenest\Hotel\Helper\File $helperFile,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory $propertyFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\PropertyFacilityVendorFactory $roomFacilityVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenVendorCollectionFactory
    ) {
        parent::__construct($context);
        $this->helperFile = $helperFile;
        $this->config = $config;
        $this->_propertyFacilityVendorFactory = $roomFacilityVendorFactory;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_propertyCollectionFactory = $propertyFactory;
        $this->_roomAmenVendorCollectionFactory = $roomAmenVendorCollectionFactory;
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!isset($params['property-detail'], $params['rooms_kind_facility'],
            $params['rooms_kind'], $params['vendor_id'], $params['list_property_services'])) {
            return $resultRedirect->setPath('*/*/');
        }
        $vendorId = $params['vendor_id'];
        try {
            $propertyData = $params['property-detail'];
            $this->saveProperty($vendorId, $propertyData);
            $this->saveFacilities($vendorId, $params['rooms_kind_facility']);
            $this->saveAmenities($vendorId, $params['rooms_kind']);
            $this->saveServices($vendorId, $params['list_property_services']['add_property_services']);
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit',
                    ['vendor_id' => $vendorId, '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/*', ['vendor_id' => $vendorId]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($params);
            return $resultRedirect->setPath('*/*/edit', ['vendor_id' => $vendorId, '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');

    }

    /**
     * @param string|int $vendorId
     * @param array $data
     * @throws \Exception
     */
    private function saveServices($vendorId, $data)
    {
        $serviceIds = [];
        foreach ($data as $service) {
            /** @var \Magenest\Hotel\Model\Service $serviceModel */
            $serviceModel = ObjectManager::getInstance()->create(\Magenest\Hotel\Model\Service::class);
            if (@$service['id']) {
                $serviceModel = $serviceModel->load($service['id']);
            }
            $serviceModel->setData($service)->setData('vendor_id', $vendorId)->save();
            $serviceIds[] = $serviceModel->getId();
        }
        /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $serviceCollection */
        $serviceCollection = ObjectManager::getInstance()->create(\Magenest\Hotel\Model\ResourceModel\Service\Collection::class);
        $serviceCollection
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter($serviceCollection->getIdFieldName(), ['nin' => $serviceIds])
            ->addFieldToSelect($serviceCollection->getIdFieldName());
        $serviceIdsToRm = $serviceCollection->getAllIds();
        $serviceCollection->walk('delete');
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection $packageServiceCollection */
        $packageServiceCollection = ObjectManager::getInstance()->create(\Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection::class);
        $packageServiceCollection->addFieldToFilter('service_id', ['in' => $serviceIdsToRm])->walk('delete');
    }

    private function saveFacilities($vendorId, $data)
    {
        $this->deleteRoomFacilityBeforeSave($vendorId);

        foreach ($data as $room) {
            foreach ($room['room_facilities']['facilities_collection'] as $facility) {
                if ($facility['room_facility'] == 1) {
                    $facilitiesVendor = $this->_propertyFacilityVendorFactory->create();
                    $facilitiesVendor->addData([
                        'vendor_id' => $vendorId,
                        'facility_id' => $facility['facility_id']
                    ])->save();
                }
            }
        }
    }

    /**
     * @param string|int $vendorId
     * @param array $data
     * @throws \Exception
     */
    private function saveProperty($vendorId, $data)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelProperty\Collection $propertyCollection */
        $propertyCollection = $this->_propertyCollectionFactory->create();
        $property = $propertyCollection->getHotelPropertyByVendorId($vendorId);
        $data['languages'] = $this->parseLanguages($data['add_languages']);
        $property->addData($data)->save();
        if (@$data['property_photos']['images'] && is_array($data['property_photos']['images'])) {
            $this->saveGalleryProperty($property, $data['property_photos']['images']);
        }
    }

    /**
     * @param $galleryList
     * @param $vendorId
     */
    protected function saveGalleryProperty($property, $galleryList)
    {
        if (is_array($galleryList)) {
            $photos = json_decode($property->getPhotos(), true);
            if ($photos === null)
                $photos = [];
            foreach ($galleryList as $key => $screenshot) {
                try {
                    $isRemoved = (bool)$screenshot['removed'];
                    if ($isRemoved && in_array($screenshot['file'], $photos)) {
                        $photos = array_diff($photos, [$screenshot['file']]);
                        $this->deleteScreenshotFile($screenshot['file']);
                        continue;
                    } elseif (!$isRemoved && in_array($screenshot['file'], $photos))
                        continue;
                    if (strpos($screenshot['file'], ".tmp") >= 0) {
                        $newPhoto = $this->moveScreenshotFromTmp($screenshot['file']);
                        $photos [] = $newPhoto;
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }
            if (is_array($photos)) {
                $photos = json_encode(array_values($photos));
                $property->setPhotos($photos)->save();
            }
        }
    }

    /**
     * Delete image
     *
     * @param $path
     */
    protected function deleteScreenshotFile($path)
    {
        $this->helperFile->deleteFile($this->config->getBaseMediaPath() . $path);
    }

    /**
     * Move images from tmp directory to base directory
     *
     * @param $fileName
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function moveScreenshotFromTmp($fileName)
    {
        $config = $this->config;
        return $this->helperFile->moveFileFromTmp(
            $config->getBaseTmpMediaPath(),
            "hotel/property/images",
            $fileName
        );
    }

    /**
     * @param string|int $vendorId
     * @param array $data
     */
    private function saveAmenities($vendorId, $data)
    {
        $this->deleteRoomAmenityBeforeSave($vendorId);
        $roomTypeVendor = $this->_roomTypeVendorFactory->create();
        $allRoomTypeIds = $roomTypeVendor->getCollection()->addFieldToFilter("vendor_id", $vendorId)->getAllIds();
        foreach ($data as $roomKind) {
            if (@$roomKind['amenities'] && is_array($roomKind['amenities']['amenities_collection'])) {
                foreach ($roomKind['amenities']['amenities_collection'] as $amenity) {
                    if (@$amenity['all_rooms'] == "1") {
                        foreach ($allRoomTypeIds as $roomTypeId) {
                            $amenityModel = $this->_roomAmenVendorCollectionFactory->create();
                            $amenityModel->addData([
                                'vendor_id' => $vendorId,
                                'room_vendor_id' => $roomTypeId,
                                'amenity' => $amenity['amenity_id']
                            ])->save();
                        }
                    } elseif (array_key_exists("some_rooms", $amenity)) {
                        foreach ($amenity['some_rooms'] as $room) {
                            $amenityModel = $this->_roomAmenVendorCollectionFactory->create();
                            $amenityModel->addData([
                                'vendor_id' => $vendorId,
                                'room_vendor_id' => $room,
                                'amenity' => $amenity['amenity_id']
                            ])->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $vendorId
     */
    private function deleteRoomAmenityBeforeSave($vendorId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection $amenitiesVendor */
        $amenitiesVendor = $this->_roomAmenVendorCollectionFactory->create()->getCollection();
        $amenitiesVendor->addFieldToFilter("vendor_id", $vendorId)->walk('delete');
    }

    /**
     * @param $vendorId
     */
    private function deleteRoomFacilityBeforeSave($vendorId)
    {
        $model = $this->_propertyFacilityVendorFactory->create()->getCollection();
        $model->addFieldToFilter("vendor_id", $vendorId)->walk('delete');
    }

    /**
     * $data: [0 => ['languages' => 'English'], 1 => ['languages' => 'Dutch']]
     * @param $data
     * @return string
     */
    private function parseLanguages($data)
    {
        $arr = array_map(function ($item) {
            return $item['languages'];
        }, $data);
        return json_encode($arr);
    }
}
