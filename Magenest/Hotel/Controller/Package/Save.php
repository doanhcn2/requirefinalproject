<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Controller\Package;

use Magenest\Hotel\Helper\Data as HotelHelperData;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;

class Save extends \Unirgy\DropshipVendorProduct\Controller\Vendor\AbstractVendor
{
    public function __construct(
        Context $context,
        Header $httpHeader,
        Registry $registry,
        HelperData $helper,
        HotelHelperData $helperData,
        RawFactory $resultRawFactory,
        PageFactory $resultPageFactory,
        LayoutFactory $viewLayoutFactory,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        ForwardFactory $resultForwardFactory,
        DesignInterface $viewDesignInterface,
        \Unirgy\Dropship\Helper\Catalog $helperCatalog,
        \Magenest\Hotel\Model\HotelProductFactory $hotelProductFactory,
        \Magento\MediaStorage\Helper\File\Storage\Database $storageDatabase,
        \Unirgy\DropshipVendorProduct\Model\ProductFactory $modelProductFactory
    ) {
        parent::__construct(
            $helperData,
            $context,
            $scopeConfig,
            $viewDesignInterface,
            $storeManager,
            $viewLayoutFactory,
            $registry,
            $resultForwardFactory,
            $helper,
            $resultPageFactory,
            $resultRawFactory,
            $httpHeader,
            $helperCatalog,
            $modelProductFactory,
            $storageDatabase
        );
        $this->_modelProductFactory = $hotelProductFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $v = $vendorSession->getVendor();

            $params = $this->_request->getParams();

            $oldStoreId = $this->_storeManager->getStore()->getId();
            $this->_storeManager->setCurrentStore(0);

            /** @var \Magenest\Hotel\Model\HotelProduct $prod */
            $prod = $this->_initProduct();

            //Some basic product details
            $params['data']['vendorSku'] = 'HB' . $vendorSession->getVendorId() . '-' . time();
            $params['data']['roomQty'] = '999';
            // @important: '4' is Getaways category ID
            $params['data']['categories'] = [0 => '4'];
            $params['data']['websites'] = [0 => $oldStoreId];
            $prod->setData('udmulti', ['vendor_sku' => $params['data']['vendorSku'], 'stock_qty' => $params['data']['roomQty']]);
            $prod->setData('vendor_id', $v->getId());
            $prod->setName($params['data']['offerTitle']);
            $prod->setData('category_ids', $params['data']['categories']);
            $prod->setData('website_ids', $params['data']['websites']);

            $prod->setData('state', 'new');
            $prod->setData('backorders', '1');
////            $prod->setData('vendor_cost', 100);

            $prod->setData('vendor_title', $params['data']['offerTitle']);
            $prod->setData('stock_qty', $params['data']['roomQty']);
////            $prod->setData('stock_item', $params['data']['roomQty']);
////            $prod->setMultiVendorData([$v->getId() => ['stock_qty'=> $params['data']['roomQty']]]);
////            $prod->setAllMultiVendorData([$v->getId() => ['stock_qty'=> $params['data']['roomQty']]]);
////            $prod->setUdmultiStock([$v->getId() => $params['data']['roomQty']]);
////            $prod->setUdmultiAvail([$v->getId() => ['stock_qty' => $params['data']['roomQty']]]);
            $prod->setQty($params['data']['roomQty']);
            $prod->setStockData(['qty' => $params['data']['roomQty'], 'is_in_stock' => true]);
            $prod->setQuantityAndStockStatus(['qty' => $params['data']['roomQty'], 'is_in_stock' => true]);

            if (!$this->_hlp->getScopeFlag('udprod/general/disable_name_check')) {
                $ufName = $prod->formatUrlKey($prod->getName());
                if (!trim($ufName)) {
                    throw new \Exception(__('Product name is invalid'));
                }
            }

            $this->_prodHlp->checkUniqueVendorSku($prod, $v);
            $prod->setTypeId('hotel');
            $prod->setStatus(\Unirgy\DropshipVendorProduct\Model\ProductStatus::STATUS_PENDING);
            $prod->setPrice(0);

            $prod->save();
            $this->_prodHlp->processAfterSave($prod);
            $this->_prodHlp->processUdmultiPost($prod, $v);

            $data = $params['data'];
            /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetails */
            $packageDetails = $this->_objectManager->create(\Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection::class);
            $packageDetails = $packageDetails->getItemByProductId(@$params['id']);
            $packageDetails->setProductId($prod->getId());
//            $packageDetails->setQty($params['data']['roomQty']);
//            $packageDetails->setRoomType($data['roomType']);
//            $packageDetails->setBookFrom($data['bookFrom']);
            $packageDetails->setVendorId($v->getId());
//            $packageDetails->setBookTo($data['bookTo']);
            $packageDetails->setMinNights(@$data['minNights']);
            $packageDetails->setMaxNights(@$data['maxNights']);
            $packageDetails->setCancellationPolicy(@$data['cancellationPolicy']);
            $packageDetails->setPenalty(@$data['penalty']);
            $packageDetails->setMinTimeToBook($data['minTimeToBook']);
            $packageDetails->setIsPublishedAsap(!!@$data['isPublishedAsap']);
//            if (isset($data['publishedDate'])) {
//                $packageDetails->setPublishDate($data['publishedDate']);
//            } else {
//                $packageDetails->setIsPublishedAsap(true);
//            }
//            $packageDetails->setIsUnpublishedAlap(!!@$data['isUnpublishedAlap']);
//            if (isset($data['unpublishedDate'])) {
//                $packageDetails->setUnpublishDate($data['unpublishedDate']);
//            } else {
//                $packageDetails->setIsUnpublishedAlap(true);
//            }
            $packageDetails->setSpecialTerms(@$data['specialTerms']);
            $packageDetails->save();

            /** @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection $packageServiceCollection */
            $packageServiceCollection = $this->_objectManager->create(\Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection::class);
            $packageServiceCollection->addFieldToFilter('package_id', $packageDetails->getId())->walk('delete');
            if (isset($data['services']) && is_array($data['services'])) {
                foreach ($data['services'] as $service) {
                    /** @var \Magenest\Hotel\Model\PackageAndService $packageService */
                    $packageService = $this->_objectManager->create(\Magenest\Hotel\Model\PackageAndService::class);
                    $packageService->setPackageId($packageDetails->getId());
                    $packageService->setServiceId($service);
                    $packageService->save();
                }
            }

//            /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
//            $packagePriceCollection = $this->_objectManager->create(\Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection::class);
//            $packagePriceCollection->addFieldToFilter('package_id', $packageDetails->getId())->walk('delete');
//            if (isset($data['package']) && is_array($data['package'])) {
//                $minPrice = null;
//                foreach ($data['package'] as $date => $prices) {
//                    if (strtotime($date) < strtotime(date('Y-m-d'))) {
//                        continue;
//                    }
//                    /** @var \Magenest\Hotel\Model\PackagePrice $packagePrice */
//                    $packagePrice = $this->_objectManager->create(\Magenest\Hotel\Model\PackagePrice::class);
//                    $packagePrice->loadByPackageIdAndDate($packageDetails->getId(), $date);
//                    $packagePrice->setSalePrice(@$prices['salePrice'] ? $prices['salePrice'] : $prices['origPrice']);
//                    $packagePrice->setOrigPrice($prices['origPrice']);
//                    $packagePrice->setQty($prices['roomQty']);
//                    $packagePrice->save();
//                    if ($minPrice > $packagePrice->getSalePrice() || $minPrice === null)
//                        $minPrice = $packagePrice->getSalePrice();
//                }
//                $prod->setPrice($minPrice)->setData('vendor_price', $minPrice);
//            }
//            $prod->setPrice(3.4);
//            $prod->save();
            $this->_prodHlp->processAfterSave($prod);
            $this->_prodHlp->processUdmultiPost($prod, $v);

            $result->setData([
                'success' => true,
            ]);
            $this->_storeManager->setCurrentStore($oldStoreId);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}
