<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/4/18
 * Time: 6:56 PM
 */

namespace Magenest\Hotel\Controller\Rate\Categories;

use Magenest\Hotel\Controller\AbstractGrid;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Grid
 * @package Magenest\Hotel\Controller\Rate\Categories
 */
class Grid extends AbstractGrid
{
    protected $_collectionFactory;

    protected $_productFactory;
    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    public function __construct(
        \Unirgy\Dropship\Model\ResourceModel\ProductCollectionFactory $collectionFactory,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Helper\Helper $helper,
        \Magento\Framework\UrlInterface $url,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Context $context
    ) {
        $this->_url = $url;
        $this->_collectionFactory = $collectionFactory;
        $this->_vendorSession = $vendorSession;
        parent::__construct($context, $helper);
    }

    public function getCollectionData()
    {
        $collectionData = parent::getCollectionData();
        for ($i = 0; $i < count($collectionData); $i++) {
            try {
                $product = $this->helper->getProduct($collectionData[$i]['entity_id']);
                $collectionData[$i]['status_style'] = $this->getStatusStyle($product->getCampaignStatus());
                $collectionData[$i]['status'] = $this->helper->getOfferStatus($product->getCampaignStatus());
                $collectionData[$i]['name'] = $product->getName();
                $collectionData[$i]['url'] = $this->getProductEditUrl($collectionData[$i]['entity_id']);

            } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                $collectionData[$i]['status_style'] = "black";
                $collectionData[$i]['status'] = '';
                $collectionData[$i]['name'] = 'This product has been deleted.';
                $collectionData[$i]['url'] = "";
            }

            if ($collectionData[$i]['cancellation_policy'] != "Day of Arrival")
                $collectionData[$i]['cancellation_policy'] .= "Prior";
            elseif ($collectionData[$i]['cancellation_policy'] == null)
                $collectionData[$i]['cancellation_policy'] .= "Non Refundable";
            $collectionData[$i]['services'] = $this->helper->getServices($collectionData[$i]['package_id']);
            $collectionData[$i]['isChecked'] = false;
            $collectionData[$i]['id'] = $collectionData[$i]['package_id'];
            $collectionData[$i]['product_id'] = $collectionData[$i]['entity_id'];
        }
        $collectionData = array_values($collectionData);
        return $collectionData;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->helper->getCurrencySymbol();
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getStatusStyle($statusCode)
    {
        switch ($statusCode) {
            case (1):
            case (7):
            case (8):
            case (9):
                return "red";
            case (4):
            case (5):
            case (6):
                return "green";
            case (2):
            case (3):
                return "blue";
        }
        return "black";
    }

    public function getCollection()
    {
        if (!$this->_collection) {
            if (!$this->_vendorSession->isLoggedIn()) {
                throw new LocalizedException(__("Vendor not logged in"));
            }
            $this->_collection = $this->_collectionFactory->create();
            $packageTable = $this->_collection->getTable('magenest_hotel_package_details');
            $this->_collection
                ->addAttributeToSelect(['name', 'campaign_status'])
                ->addAttributeToFilter('type_id', 'hotel')
                ->addAttributeToFilter('entity_id', ['in' => $this->_vendorSession->getVendor()->getAssociatedProductIds()])
                ->joinTable(
                    $packageTable,
                    "product_id=entity_id",
                    ['id AS package_id', 'room_type', 'min_nights', 'max_nights', 'cancellation_policy', 'min_time_to_book', 'penalty']
                );
            $filter = $this->getFilter();
            if ($filter === []) {
                return $this->_collection;
            }
            if (isset($filter['status'])) {
                $this->_collection->addAttributeToFilter('campaign_status', ['eq' => $filter['status']]);
            }
        }

        return $this->_collection;
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        $return = [];
        $status = $this->getRequest()->getParam('status');
        if (is_numeric($status)) {
            $return['status'] = $status;
        }
        return $return;
    }

    /**
     * @param $productId
     * @return string
     */
    public function getProductEditUrl($productId)
    {
        return $this->_url->getUrl('udprod/vendor/productEdit', ['id' => $productId]);
    }
}
