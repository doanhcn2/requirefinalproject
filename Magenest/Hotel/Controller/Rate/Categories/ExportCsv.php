<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 14/04/2018
 * Time: 16:09
 */

namespace Magenest\Hotel\Controller\Rate\Categories;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class ExportCsv
 * @package Magenest\Hotel\Controller\Rate\Categories
 */
class ExportCsv extends \Magento\Framework\App\Action\Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    protected $fileFactory;

    protected $_helper;
    protected $_packageDetailsFactory;

    public function __construct(
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        Filesystem $filesystem,
        \Magenest\Hotel\Helper\Helper $helper,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->fileFactory = $fileFactory;
        $this->_helper = $helper;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_packageDetailsFactory = $packageDetailsFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->getRequest()->getParams();
            $resultRedirect = $this->resultRedirectFactory->create();

            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($data) {
                $heading = [
                    __('Status'),
                    __('ID'),
                    __('Rate Category Name'),
                    __('Room Types'),
                    __('Service'),
                    __('Cancellation'),
                    __('Min Stay'),
                    __('Max Stay'),
                    __('Min Advance Booking')
                ];

                $collection = $this->_packageDetailsFactory->create()->getCollection();
                $collection->addFieldToFilter('vendor_id', $vendorSession->getVendorId());
                if ($data['chosen'] != 'all') {
                    $collection = $collection->addFieldToFilter("id", array('in' => json_decode($data['chosen'], true)));
                } else {
                    if ($data['except'] != 0)
                        $collection = $collection->addFieldToFilter("id", array('nin' => json_decode($data['except'], true)));
                }

                $items = [];
                /** @var  \Magenest\Hotel\Model\PackageDetails $model */
                foreach ($collection as $model) {
                    $item = [];
                    try {
                        $product = $this->_helper->getProduct($model->getProductId());
                        $item[] = $this->_helper->getOfferStatus($product->getCampaignStatus());
                        $item[] = $product->getEntityId();
                        $item[] = $product->getName();
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
                        $item[] = "DELETED";
                        $item[] = "";
                        $item[] = "This product has been deleted.";
                    }

                    $item[] = "type1,typ2,typ3";
                    $item[] = $this->_helper->getServices($model->getId());
                    $item[] = ($model->getCancellationPolicy() == null ? "Non Refundable" :
                            ($model->getCancellationPolicy()
                                . ($model->getCancellationPolicy() === "Day of Arrival" ? '' : " Prior")))
                        . "\n Penalty: " . $model->getPenalty();
                    $item[] = $model->getMinNights();
                    $item[] = $model->getMaxNights();
                    $item[] = $model->getMinTimeToBook();
                    $items[] = $item;
                }
                $file = 'export/ListRateCategories' . date('Ymd_His') . '.csv';
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach ($items as $item) {
                    $stream->writeCsv($item);

                }

                return $this->fileFactory->create('ListRateCategories' . date('Ymd_His') . '.csv', [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('hotel/rate_categories/index');
    }


}
