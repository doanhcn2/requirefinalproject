<?php

namespace Magenest\Hotel\Controller\Property;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_file;

    protected $_mediaDirectory;

    protected $_hotelPropertyFactory;

    protected $_facilVendorFactory;

    protected $_facilVendorCollectionFactory;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magenest\Hotel\Model\PropertyFacilityVendorFactory $roomFacilityVendorFactory,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $facilVendorCollectionF
    ) {
        parent::__construct($context);
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_facilVendorCollectionFactory = $facilVendorCollectionF;
        $this->_facilVendorFactory = $roomFacilityVendorFactory;
        $this->_hotelPropertyFactory = $hotelPropertyFactory;
        $this->_file = $file;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }

            $vendorId = $vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            $hotelProperty = $this->_hotelPropertyFactory->create();
            $hotelProperty = $hotelProperty->loadByVendorId($vendorId);

            $time = $this->_request->getParam('time');
            if ($time) {
                $hotelProperty->setData('check_in_from', @$time['checkinFrom']);
                $hotelProperty->setData('check_in_to', @$time['checkinTo']);
                $hotelProperty->setData('check_out_from', @$time['checkoutFrom']);
                $hotelProperty->setData('check_out_to', @$time['checkoutTo']);
            }

//            $extra = $this->_request->getParam('extra');
//            if ($extra) {
//                $hotelProperty->setData('is_internet', @$extra['isInternetAvailable']);
//                $hotelProperty->setData('is_parking', @$extra['isParkingAvailable']);
//                $hotelProperty->setData('is_breakfast', @$extra['isBreakfastAvailable']);
//                $hotelProperty->setData('is_children_allowed', @$extra['canAccommodate']);
//                $hotelProperty->setData('is_pet_allowed', @$extra['isPetAllowed']);
//            }

            $languages = $this->_request->getParam('languages');
            if ($languages) {
                $hotelProperty->setData('languages', json_encode(array_diff($languages, ['undefined'])));
            } else {
                $hotelProperty->setData('languages', '[]');
            }

            $facilities = $this->_request->getParam('facilities');
            if ($facilities) {
                /** @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection $facilVendorCollection */
                $facilVendorCollection = $this->_facilVendorCollectionFactory->create();
                $facilVendorCollection->addFieldToFilter('vendor_id', $vendorId)->walk('delete');
                foreach ($facilities as $facility) {
                    /** @var \Magenest\Hotel\Model\PropertyFacilityVendor $facilModel */
                    $facilModel = $this->_facilVendorFactory->create();
                    $facilModel->setData(['vendor_id' => $vendorId, 'facility_id' => $facility])->save();
                }
            }

            $toRmPhotos = $this->_request->getParam('toRmPhotos');
            if (is_array($toRmPhotos)) {
                foreach ($toRmPhotos as $photo) {
                    $this->deleteFile(@$photo[0]);
                }
            }

            $photos = $this->_request->getParam('photos');
            if ($photos) {
                $hotelProperty->setData('photos', json_encode($photos));
            } else {
                $hotelProperty->setData('photos', '[]');
            }

            $terms = $this->_request->getParam('terms');
            if ($terms) {
                $hotelProperty->setData('terms', $terms);
            }

            $tax = $this->_request->getParam('tax');
            if ($tax && isset($tax['isEnabled']) && $tax['isEnabled'] !== 'false' && $tax['isEnabled']) {
                $hotelProperty->setData('is_tax', true);
                $hotelProperty->setData('tax_amount', @$tax['amount']);
                $hotelProperty->setData('tax_type', @$tax['type']);
            } else {
                $hotelProperty->setData('is_tax', false);
//                $hotelProperty->setData('tax_amount', '');
//                $hotelProperty->setData('tax_type', '');
            }

            $location = $this->_request->getParam('location');
            if ($location) {
                $hotelProperty->setData('location', $location);
            }

            $coordinate = $this->_request->getParam('coordinate');
            if (isset($coordinate['longitude'], $coordinate['latitude'])) {
                $hotelProperty->addData([
                    'longitude' => $coordinate['longitude'],
                    'latitude' => $coordinate['latitude']
                ]);
            }

            $hotelProperty->save();
            $result->setData([
                'success' => true,
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }

    /**
     * @param $fileName
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function deleteFile($fileName)
    {
        $mediaRootDir = $this->_mediaDirectory->getAbsolutePath(\Magenest\Hotel\Controller\Property\Upload::HOTEL_IMG_PATH);

        if ($this->_file->isExists($mediaRootDir . $fileName)) {
            $this->_file->deleteFile($mediaRootDir . $fileName);
        }
    }
}
