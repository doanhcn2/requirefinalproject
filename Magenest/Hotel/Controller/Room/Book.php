<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 1/30/18
 * Time: 3:42 PM
 */

namespace Magenest\Hotel\Controller\Room;

use Magento\Framework\App\Action\Context;

class Book extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;
    protected $_storeManager;
    protected $productRepository;
    protected $_serviceFactory;

    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory
    ) {
        parent::__construct($context);
        $this->productRepository = $productRepository;
        $this->_serviceFactory = $serviceFactory;
        $this->_storeManager = $storeManager;
        $this->_cart = $cart;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            $params = $this->getRequest()->getParams();
            $productId = $params['productId'];
            $startDate = $params['startDate'];
            $endDate = $params['endDate'];
            $qty = $params['qty'];
            $price = $params['totalPrice'];

            $storeId = $this->_storeManager->getStore()->getId();
            $product = $this->productRepository->getById($productId, false, $storeId);

            $services = [];
            if (@$params['services']) {
                foreach ($params['services'] as $service) {
                    $services[] = $service;
                }
            }

            $additionalOptions = [];
            $additionalOptions[] = [
                'label' => 'Check-in date',
                'value' => $startDate
            ];
            $additionalOptions[] = [
                'label' => 'Check-out date',
                'value' => $endDate
            ];
            $additionalOptions[] = [
                'label' => 'Qty',
                'value' => $qty
            ];

            $qtyPro = [
                'qty' => $qty,
            ];
            $request = new \Magento\Framework\DataObject($qtyPro);
            $quote = $this->_cart->getQuote();
            if (@$params['itemCartId']) {
                $item = $quote->getItemById($params['itemCartId']);
                $item->setProduct($product, $request);
            } else {
                $item = $quote->addProduct($product, $request);
            }

            if (!empty($services)) {
                $additionalOptions[] = [
                    'label' => 'Services',
                    'value' => implode(', ', $services)
                ];
                $item->addOption([
                    'product_id' => $productId,
                    'code' => 'services',
                    'value' => json_encode($services)
                ]);
            }
            $item->addOption([
                'product_id' => $productId,
                'code' => 'additional_options',
                'value' => json_encode($additionalOptions)
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => 'check_in_date',
                'value' => $startDate
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => 'check_out_date',
                'value' => $endDate
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => 'room_qty',
                'value' => $qty
            ]);

            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->getProduct()->setIsSuperMode(true);
            $this->_cart->save();

            $result->setData([
                'success' => true
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}