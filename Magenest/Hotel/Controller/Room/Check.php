<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 1/30/18
 * Time: 1:37 PM
 */

namespace Magenest\Hotel\Controller\Room;


use Magenest\Hotel\Helper\BookingStatus as Status;
use Magento\Framework\App\Action\Context;

class Check extends \Magento\Framework\App\Action\Action
{
    protected $_packageDetailsCollectionFactory;
    protected $_packagePriceCollectionFactory;
    protected $_hotelOrderCollectionFactory;

    public function __construct(
        Context $context,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory
    )
    {
        parent::__construct($context);
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
        $this->_packagePriceCollectionFactory = $packagePriceCollectionFactory;
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            $params = $this->getRequest()->getParams();
            $productId = $params['productId'];
            $startDate = $params['startDate'];
            $endDate = $params['endDate'];
            $qty = $params['qty'];
            // @todo: check is param valid here
            if (!is_int($qty) && $qty <= 0) {
                throw new \Exception(__("Qty is not valid"));
            }

            /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
            $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
            $package = $packageDetailsCollection->getItemByProductId($productId);

            $this->validateNightsNumber($startDate, $endDate, $package->getMinNights(), $package->getMaxNights());

            $packageId = $package->getId();
            $bookFrom = strtotime($package->getBookFrom());
            $bookTo = strtotime($package->getBookTo());
            $minTimeToBook = strtotime($package->getMinTimeToBook());

            /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
            $packagePriceCollection = $this->_packagePriceCollectionFactory->create();
            $prices = $packagePriceCollection->getPricePack($packageId, $startDate, $endDate);

            /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
            $data = [];
            $data['maxQty'] = null;
            $total = 0;
            foreach ($prices as $price) {
                $date = strtotime($price->getDate());
                if ($date < $minTimeToBook || $date < $bookFrom || $date > $bookTo) {
                    $data[$price->getDate()] = [
                        'status' => __("Booking is not available"),
                    ];
                    continue;
                }
                $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();
                $orderedQty = $hotelOrderCollection->getBookedRoomsADay($packageId,$price->getDate());
//                    ->addExpressionFieldToSelect('total_qty', 'SUM({{qty}})', 'qty')
//                    ->addFieldToFilter('package_id', $packageId)
//                    ->addFieldToFilter('check_in_date', ['lteq' => $price->getDate()])
//                    ->addFieldToFilter('check_out_date', ['gteq' => $price->getDate()])
//                    ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED]])
//                    ->getFirstItem()->getTotalQty();
                if ($data['maxQty'] == null || $data['maxQty'] > $price->getQty())
                    $data['maxQty'] = $price->getQty();
                if ($price->getQty() - $orderedQty < $qty) {
                    $data[$price->getDate()] = [
                        'status' => __('Ran out of room'),
                    ];
                } else {
                    $total += $price->getSalePrice();
                    $data[$price->getDate()] = [
                        'salePrice' => $price->getSalePrice(),
                        'origPrice' => $price->getOrigPrice()
                    ];
                }
            }

            $result->setData([
                'success' => true,
                'data' => $data,
                'total' => $total
            ]);
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }

    /**
     * @param $start
     * @param $end
     * @param $min
     * @param $max
     * @throws \Exception
     */
    protected function validateNightsNumber($start, $end, $min, $max)
    {
        $num = ((strtotime($end) - strtotime($start)) / 86400) + 1;
        if ($num < 0) {
            throw new \Exception(__("End date is earlier than start date"));
        }
        if (!!$min && $num < $min) {
            throw new \Exception(__("Number of days must be greater or equal to %1", $min));
        }
        if (!!$max && $num > $max) {
            throw new \Exception(__("Number of days must be less or equal to %1", $max));
        }
    }
}
