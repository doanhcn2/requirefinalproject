<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/4/18
 * Time: 6:20 PM
 */

namespace Magenest\Hotel\Controller;

/**
 * Class AbstractGrid
 * @package Magenest\Hotel\Controller
 */
abstract class AbstractGrid extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Data\Collection
     */
    protected $_collection;

    /**
     * @var \Magenest\Hotel\Helper\Helper
     */
    protected $helper;

    /**
     * @var int
     */
    protected $_curPage = 1;

    /**
     * @var string
     */
    protected $_curPageVar = 'page';

    /**
     * @var int
     */
    protected $_pageSize = 5;

    /**
     * @var string
     */
    protected $_pageSizeVar = 'limit';

    /**
     * AbstractGrid constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magenest\Hotel\Helper\Helper $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Hotel\Helper\Helper $helper
    ) {
        parent::__construct($context);
        $this->helper = $helper;
    }

    /**
     * Prepare Collection
     */
    public function _prepareCollection()
    {
        $this->_pageSize = (int)$this->getRequest()->getParam($this->_pageSizeVar) ?: $this->_pageSize;
        $this->_curPage = (int)$this->getRequest()->getParam($this->_curPageVar) ?: $this->_curPage;
        $this->getCollection()->setPageSize($this->_pageSize)
            ->setCurPage($this->_curPage);
    }

    /**
     * @return \Magento\Framework\Data\Collection
     */
    public function getCollection()
    {
        return $this->_collection;
    }

    /**
     * @return array
     */
    public function getCollectionData()
    {
        $return = [];
        $this->_prepareCollection();
        $items = $this->getCollection()->getItems();
        foreach ($items as $item) {
            $return[] = $item->getData();
        }
        return $return;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);

        $data = $this->getCollectionData();
        $result->setData([
            'items' => $data,
            'total' => $this->getCollection()->getSize()
        ]);

        return $result;
    }
}
