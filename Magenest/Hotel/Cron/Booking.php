<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Cron;

use Magenest\Hotel\Helper\BookingStatus;
use Magenest\Hotel\Logger\Logger;
use Magenest\Hotel\Model\ProductStatus;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;

class Booking
{
    /**
     * @var Logger
     */
    protected $_logger;

    protected $_hotelOrderCollectionFactory;

    protected $_packageDetailsCollectionFactory;

    protected $_productCollectionFactory;

    public function __construct(
        Logger $logger,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory
    ) {
        $this->_logger = $logger;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
    }

    public function daily()
    {
        $this->updateStatus();
//        $this->checkPublish();
//        $this->checkCampaignStatus();
    }

    public function updateStatus()
    {
        $today = date('Y-m-d');
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
        $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();
        $pendingItems = $hotelOrderCollection->addFieldToFilter('check_in_date', $today)
            ->addFieldToFilter('status', BookingStatus::PENDING)
            ->getItems();
        foreach ($pendingItems as $item) {
            $item->setData('status', BookingStatus::ACTIVE)->save();
        }

        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
        $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();
        $activeItems = $hotelOrderCollection->addFieldToFilter('check_out_date', $today)
            ->addFieldToFilter('status', BookingStatus::ACTIVE)
            ->getItems();
        foreach ($activeItems as $item) {
            $item->setData('status', BookingStatus::EXPIRED)->save();
        }
    }

    public function checkPublish()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection
            ->addAttributeToSelect(['id', 'status', 'type_id'])
            ->addAttributeToFilter('type_id', ['hotel']);
        /** @var \Magento\Catalog\Model\Product[] $products */
        $products = $productCollection->getItems();
        foreach ($products as $product) {
            if (!in_array($product->getStatus(), [ProductStatus::STATUS_WAITING_TO_BE_PUBLISHED, ProductStatus::STATUS_ENABLED])) {
                continue;
            }
            /** @var \Magenest\Hotel\Model\PackageDetails $package */
            $package = $this->_packageDetailsCollectionFactory->create()->getItemByProductId($product->getId());
            if (!$package->getIsPublishedAsap()             // don't want to publish asap
                && strtotime($package->getPublishDate()) !== false // and have a valid publish date
                && $product->getStatus() == ProductStatus::STATUS_WAITING_TO_BE_PUBLISHED // and admin approved it
                && strtotime($package->getPublishDate()) == strtotime(date('Y-m-d'))) { // and published date later or equal to today
                $product->setStatus(ProductStatus::STATUS_ENABLED)->save(); // then enable it
            }
            if (strtotime($package->getBookTo()) >= strtotime(date('Y-m-d')) // if offer end
                || (!$package->getIsUnpublishedAlap() // or ...
                    && strtotime($package->getUnpublishDate()) !== false
                    && $product->getStatus() == ProductStatus::STATUS_ENABLED
                    && strtotime($package->getUnpublishDate()) >= strtotime(date('Y-m-d')))) {
                $product->setStatus(ProductStatus::STATUS_DISABLED)->save(); // then disable it
            }
        }
    }

    /**
     * Check campaign status
     *
     * timeline & campaign status
     * created date - approved date - publish date - 1st checkinable date - last checkinable date - later
     *       pending review   -   scheduled   -  live   -              active          -       expired
     *
     * if admin approved, product status turn from "under review" to "enabled" (may be :v)
     */
    public function checkCampaignStatus()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection
            ->addAttributeToSelect(['id', 'status', 'type_id'])
            ->addAttributeToFilter('type_id', ['hotel']);
        /** @var \Magento\Catalog\Model\Product[] $products */
        $products = $productCollection->getItems();
        foreach ($products as $product) {
            // if product is not approved
            if ($product->getStatus() != ProductStatus::STATUS_ENABLED) {
                continue;
            }
            /** @var \Magenest\Hotel\Model\PackageDetails $package */
            $package = $this->_packageDetailsCollectionFactory->create()->getItemByProductId($product->getId());
            //set Scheduled
//            if(){
//                $product->setCampaignStatus(3);
//                continue;
//            }
            // set Live
//            if ($product->getCampaignStatus() == CampaignStatus::STATUS_SCHEDULED
//                && strtotime($package->getBookFrom()) > strtotime(date('Y-m-d'))
//            ) {
//                $product->setCampaignStatus(CampaignStatus::STATUS_LIVE)->save();
//                continue;
//            }
//            // set Active
//            if ($product->getCampaignStatus() == CampaignStatus::STATUS_LIVE
//                && strtotime($package->getBookFrom()) <= strtotime(date('Y-m-d'))
//                && strtotime($package->getBookTo()) > strtotime(date('Y-m-d'))) {
//                $product->setCampaignStatus(CampaignStatus::STATUS_ACTIVE)->save();
//                continue;
//            }
//            // set Expired
//            if (strtotime($package->getBookTo()) <= strtotime(date('Y-m-d'))) {
//                $product->setCampaignStatus(CampaignStatus::STATUS_EXPIRED)->save();
//                continue;
//            }
            // must fix 1 after add approve campaign in admin
//                $now = strtotime(date('Y-m-d'));
//                $bookTo = strtotime($params['hotel']['package-offer']['book_to']);
//                if($now > $bookTo){
//                    $product->setCampaignStatus(CampaignStatus::STATUS_EXPIRED);
//                }else{
//                    $bookFrom = strtotime($params['hotel']['package-offer']['book_from']);
//                    if($now >= $bookFrom){
//                        $product->setCampaignStatus(CampaignStatus::STATUS_ACTIVE);
//                    }else{
//                        $asap = $params['hotel']['package-offer']['is_published_asap'];
//                        if($asap=="1")
//                            $product->setCampaignStatus(CampaignStatus::STATUS_LIVE);
//                        else{
//                            $publishday = strtotime($params['hotel']['package-offer']['publish_date']);
//                            if($now >= $publishday)
//                                $product->setCampaignStatus(CampaignStatus::STATUS_LIVE);
//                            else
//                                $product->setCampaignStatus(CampaignStatus::STATUS_SCHEDULED);
//                        }
//                    }
        }
    }
}
