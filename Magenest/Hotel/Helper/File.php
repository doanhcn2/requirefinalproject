<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Helper;

use Magento\Downloadable\Helper\File as HelperFile;

/**
 * Class File
 * @package Magenest\Hotel\Helper
 */
class File extends HelperFile
{

    /**
     * Checking file for moving and move it
     * @param string $baseTmpPath
     * @param string $basePath
     * @param string $fileName
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function moveFileFromTmp($baseTmpPath, $basePath, $fileName)
    {
        if ($fileName) {
            try {
                $fileName = $this->_moveFileFromTmp($baseTmpPath, $basePath, $fileName);

                return $fileName;
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while saving the file(s).')
                );
            }
        }
        return '';
    }

    public function deleteFile($path)
    {
        $this->_mediaDirectory->delete($path);
    }
}
