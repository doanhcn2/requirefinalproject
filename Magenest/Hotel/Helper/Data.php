<?php

namespace Magenest\Hotel\Helper;

use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\ActionFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Helper\Data as HelperData;
use Magento\CatalogInventory\Model\Stock\ItemFactory;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\DesignInterface;
use Magento\ProductAlert\Helper\Data as ProductAlertHelperData;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Catalog;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\DropshipVendorProduct\Model\ProductFactory as ModelProductFactory;

class Data extends \Unirgy\DropshipVendorProduct\Helper\Data
{

}
