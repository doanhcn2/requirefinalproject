<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/9/18
 * Time: 9:22 AM
 */

namespace Magenest\Hotel\Helper;


use Magento\Framework\App\Helper\Context;

class Helper extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_hotelOrderCollectionFactory;
    protected $_packageDetailsFactory;
    protected $_customerRepository;
    protected $_productRepository;
    protected $_orderRepository;
    protected $_currencyFactory;
    protected $_storeManager;
    protected $_serviceFactory;
    /**
     * @var \Magenest\Groupon\Model\Product\Attribute\CampaignStatus
     */
    protected $_campaignStatus;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    public function __construct(
        Context $context,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        \Magenest\Groupon\Model\Product\Attribute\CampaignStatus $campaignStatus,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context);
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
        $this->_packageDetailsFactory = $packageDetailsFactory;
        $this->_campaignStatus = $campaignStatus;
        $this->_customerRepository = $customerRepository;
        $this->_productRepository = $productRepository;
        $this->_vendorSession = $vendorSession;
        $this->_serviceFactory = $serviceFactory;
        $this->_orderRepository = $orderRepository;
        $this->_currencyFactory = $currencyFactory;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param $id
     * @return array
     */
    public function getOrderInfo($id)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $collection */
        $collection = $this->_hotelOrderCollectionFactory->create();
        $data = $collection->getItemById($id)->getData();
        if (@$data['services']) {
            $data['services'] = json_decode($data['services'], true);
            $data['services'] = implode(', ', $data['services']);
        }

        /** @var \Magenest\Hotel\Model\PackageDetails $packageDetails */
        $packageDetails = $this->_packageDetailsFactory->create();
        $packageDetails = $packageDetails->load($data['package_id']);
        try {
            $product = $this->_productRepository->getById($packageDetails->getProductId());
            $customer = $this->_customerRepository->getById($data['customer_id']);
            $order = $this->_orderRepository->get($data['order_id']);
        } catch (\Exception $e) {
            return [];
        }
        $data['package_id'] = $product->getName();
        $data['customer_id'] = $customer->getEmail();
        $data['order_id'] = $order->getIncrementId();

        return $data;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        $currencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
        /** @var \Magento\Directory\Model\Currency $currency */
        $currency = $this->_currencyFactory->create();
        $currency = $currency->load($currencyCode);
        return $currency->getCurrencySymbol();
    }

    /**
     * @param $packageId
     * @return string
     */
    public function getServices($packageId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $serviceCollection */
        $serviceCollection = $this->_serviceFactory->create()->getCollection();

        $arr = $serviceCollection->getServicesWithPackageId($this->_vendorSession->getVendorId(), $packageId);
        if ($arr == null)
            return '-';
        $titleArr = array_map(function ($item) {
            return @$item['title'];
        }, $arr);
        return implode("\n", $titleArr);
    }

    /**
     * @param $campaignStatus
     * @return string
     */
    public function getOfferStatus($campaignStatus)
    {
        return $this->_campaignStatus->getStatusLabelByCode($campaignStatus);
    }

    public function getProduct($productId)
    {
        return $this->_productRepository->getById($productId);
    }

}
