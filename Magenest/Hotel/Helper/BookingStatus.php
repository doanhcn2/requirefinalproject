<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/10/18
 * Time: 8:33 AM
 */

namespace Magenest\Hotel\Helper;

/**
 * Class BookingStatus
 * @package Magenest\Hotel\Helper
 */
class BookingStatus
{
    const PENDING = 'Pending';
    const REFUNDED = 'Refunded';
    const ACTIVE = 'Active';
    const CANCELLED = 'Cancelled';
    const EXPIRED = 'Expired';
}
