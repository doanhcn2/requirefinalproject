<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 20/04/2018
 * Time: 18:33
 */

namespace Magenest\Hotel\Helper;

use Magenest\Hotel\Helper\BookingStatus as Status;
use Magento\Framework\App\Helper\Context;

class Dashboard extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_hotelOrderFactory;
    protected $_packageDetailsFactory;

    protected $_orderFactory;

    protected $_packagePriceFactory;

    protected $_productCollection;

    public function __construct(
        \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Context $context
    ) {
        $this->_productCollection = $productFactory;
        $this->_orderFactory = $orderFactory;
        $this->_packageDetailsFactory = $packageDetailsFactory;
        $this->_packagePriceFactory = $packagePriceFactory;
        $this->_hotelOrderFactory = $hotelOrderFactory;
        parent::__construct($context);
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getReservationsCollectionSaleToday()
    {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $reservationsCollection */
        $reservationsCollection = $this->_hotelOrderFactory->create()->getCollection();
        $reservationsCollection = $reservationsCollection->loadByVendorId($this->getVendor()->getVendorId())
            ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::REFUNDED]]);
        $reservationFilter = [];
        foreach ($reservationsCollection as $reservation) {
            $orderId = $reservation->getOrderId();
            $order = $this->_orderFactory->create()->load($orderId);
            if (date('Y-m-d', strtotime($order->getCreatedAt())) === $today)
                $reservationFilter[] = $reservation->getId();
        }
        return $this->_hotelOrderFactory->create()->getCollection()->addFieldToFilter("id", ['in' => $reservationFilter]);
    }

    public function getTodayRedeemed()
    {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        $reservationsCollection = $this->getReservationsCollectionSaleToday()
            ->addFieldToFilter('check_in_date', array('like' => "%$today%"));

        return count($reservationsCollection);
    }

    public function getTodaySale()
    {
        $reservations = $this->getReservationsCollectionSaleToday();
        $total = 0;
        foreach ($reservations as $reservation) {
            $total += $reservation->getTotalPaid();
        }
        return $total;
    }

    public function getSoldOutOption()
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $collection */
        $collection = $this->_packageDetailsFactory->create()->getCollection();
        $collection->addFieldToFilter('vendor_id', $this->getVendor()->getVendorId());
        $soldOutInventory = 0;
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        foreach ($collection as $item) {
            $packagePrice = $this->_packagePriceFactory->create()->getCollection()
                ->addFieldToFilter("package_id", $item->getId())
                ->addFieldToFilter('date', array('like' => "%$today%"))->getFirstItem();
            $qtyRooms = $packagePrice->getQty();
            if ($qtyRooms === null)
                continue;
            $hotelOrder = $this->_hotelOrderFactory->create()->getCollection();
            $soldRoomsToday = $hotelOrder->getBookedRoomsADay($item->getId(), $today);
            if ($qtyRooms <= $soldRoomsToday)
                $soldOutInventory++;
        }
        return $soldOutInventory;
    }

    public function getHotelProductCollection()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productCollection->create()->getCollection()
            ->addFieldToFilter('udropship_vendor', $this->getVendor()->getVendorId())
            ->addFieldToFilter('type_id', 'hotel');

        return $collection;
    }

    public function getActiveCampaignCollection()
    {
        return $this->getHotelProductCollection()->addFieldToFilter("campaign_status", 5);
    }

    public function getPreviousXDay(int $daynum)
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval('P' . $daynum . 'D'));
        return $date->format('Y-m-d');
    }

    public function getNextXDay(int $daynum)
    {
        $date = new \DateTime();
        $date->add(new \DateInterval('P' . $daynum . 'D'));
        return $date->format('Y-m-d');
    }

    public function getPackagesBookedNextXDay($packagesId, $dayNum)
    {
        $return = [];
        $dates = [];
        for ($day = 0; $day < $dayNum; $day++) {
            $date = $this->getNextXDay($day);
            $dates [] = $date;
            $order = $this->_hotelOrderFactory->create()->getCollection()
                ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED, Status::REFUNDED]])
                ->addFieldToFilter('package_id', ['in' => $packagesId])
                ->addFieldToFilter('check_in_date', ['lteq' => $date])
                ->addFieldToFilter('check_out_date', ['gteq' => $date]);
            $return[] = $order->getColumnValues('package_id');
        }
        return array_combine($dates, $return);
    }

//    public function getReservationSoldFromDay($dayNum)
//    {
//        $return = [];
//        $dates = [];
//        for ($day = 0; $day < $dayNum; $day++) {
//            $date = $this->getPreviousXDay($day);
//            $dates [] = $date;
//            $order = $this->_hotelOrderFactory->create()->getCollection()
//                ->addFieldToFilter('package_id', ['in' => $packagesId])
//                ->addFieldToFilter('check_in_date', ['lteq' => $date])
//                ->addFieldToFilter('check_out_date', ['gteq' => $date]);
//            $return[] = $order->getColumnValues('package_id');
//        }
//        return array_combine($dates, $return);
//    }

    public function getSoldInNextXDay($packagesId, $dayNum) // availability chart
    {
        $arr = [];
        foreach ($this->getPackagesBookedNextXDay($packagesId, $dayNum) as $day => $packages) {
            foreach ($packages as $item) {
                $roomsBooked = $this->_hotelOrderFactory->create()->getCollection()->getBookedRoomsADay($item, $day);
                if (!isset($arr[$day]))
                    $arr[$day] = $roomsBooked;
                else
                    $arr[$day] += $roomsBooked;
            }
        }
        return $arr;
    }

    public function getInventoryAfterDay($packagesId, $key)
    {
        $day = $this->getNextXDay($key);
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $scheduleHotel */
        $scheduleHotel = $this->_packagePriceFactory->create()->getCollection()
            ->addFieldToFilter("package_id", ["in" => $packagesId])
            ->addFieldToFilter('date', ['lteq' => $day])
            ->addFieldToFilter('date', ['gteq' => $today]);
        return $scheduleHotel;
    }

    public function getInventoryInDay($collection)
    {
        $arr = [];
        foreach ($collection as $value) {
            $key = explode(' ', $value->getDate())[0];
            if (!isset($arr[$key]))
                $arr[$key] = $value->getQty();
            else
                $arr[$key] += $value->getQty();
        }
        return $arr;
    }

    public function getPackagesOfVendor()
    {
        return $this->_packageDetailsFactory->create()->getCollection()
            ->addFieldToFilter("vendor_id", $this->getVendor()->getVendorId());
    }

    public function getOptionAvailability($daynum)
    {
        $arr = [];
        $date = new \DateTime();
        $startDateText = $date->format('Y-m-d');
//        $dateText = $date->format('Y-m-d');
        $allPackagesId = $this->getPackagesOfVendor()->getAllIds();
        $inventoryIndays = $this->getInventoryInDay($this->getInventoryAfterDay($allPackagesId, $daynum));
        $reservedIndays = $this->getSoldInNextXDay($allPackagesId, $daynum);
        for ($i = 0; $i < $daynum; $i++) {
//            $date->add(new \DateInterval('P1D'));
            $dateText = $this->getNextXDay($i);
            $inventory = isset($inventoryIndays[$dateText]) ? $inventoryIndays[$dateText] : 0;
            $reserved = isset($reservedIndays[$dateText]) ? $reservedIndays[$dateText] : 0;
            $arr[] = [$dateText, $inventory, $reserved, 'color: #017cb9; stroke-color: #017cb9'];
        }
        return [$startDateText, $dateText, $arr];
    }

    public function getReservationsNext($dayNum)
    {
        $allPackagesId = $this->getPackagesOfVendor()->getAllIds();
        $reservedIndays = $this->getSoldInNextXDay($allPackagesId, $dayNum);
        $total = 0;
        foreach ($reservedIndays as $reservedInday) {
            $total += $reservedInday;
        }
        return $total;
    }

    public function getAvailableNext($dayNum)
    {
        $allPackagesId = $this->getPackagesOfVendor()->getAllIds();
        $reservedIndays = $this->getSoldInNextXDay($allPackagesId, $dayNum);
        $inventoryIndays = $this->getInventoryInDay($this->getInventoryAfterDay($allPackagesId, $dayNum));
        $total = 0;
        foreach ($inventoryIndays as $date => $inventoryInday) {
            $total = $inventoryInday - (@$reservedIndays[$date] ?: 0);
        }
        return $total;
    }

    public function getReservationsCollectionSaleFromDay($dayNum)
    {
        $date = $this->getPreviousXDay($dayNum);
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $reservationsCollection */
        $reservationsCollection = $this->_hotelOrderFactory->create()->getCollection();
        $reservationsCollection = $reservationsCollection->loadByVendorId($this->getVendor()->getVendorId())
            ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED, Status::REFUNDED]]);
        $reservationFilter = [];
        foreach ($reservationsCollection as $reservation) {
            $orderId = $reservation->getOrderId();
            $order = $this->_orderFactory->create()->load($orderId);
            if (date('Y-m-d', strtotime($order->getCreatedAt())) >= $date)
                $reservationFilter[] = $reservation->getId();
        }
        return $this->_hotelOrderFactory->create()->getCollection()->addFieldToFilter("id", ['in' => $reservationFilter]);
    }

    public function getSaleValuePreviousXDay($key)
    {
        $allPackages = $this->getReservationsCollectionSaleFromDay($key);
        $total = 0;
        foreach ($allPackages as $item) {
            $total += $item->getTotalPaid();
        }
        return $total;
    }

    public function getRefundedFromDay($daynum)
    {
        $day = $this->getPreviousXDay($daynum);
        $allPackagesId = $this->getPackagesOfVendor()->getAllIds();
        $collection = $this->_hotelOrderFactory->create()->getCollection()
            ->addFieldToFilter('package_id', ['in' => $allPackagesId])
            ->addFieldToFilter('refunded_at', array('gt' => $day));

        return $collection;
    }

    public function getRefundedValueFromDay($daynum)
    {
        $ticketsRefunded = $this->getRefundedFromDay($daynum);
        $total = 0;
        foreach ($ticketsRefunded as $item) {
            $total += $item->getTotalPaid();
        }

        return $total;
    }

    public function getSoldBeforeXDay($packagesId, $dayNum)
    {
        $dates = [];
        for ($day = 0; $day < $dayNum; $day++) {
            $date = $this->getPreviousXDay($day);
            $dates [$date] = 0;
            $hotelOrders = $this->_hotelOrderFactory->create()->getCollection()
                ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED, Status::REFUNDED]])
                ->addFieldToFilter('package_id', ['in' => $packagesId]);
            foreach ($hotelOrders as $hotelOrder) {
                $orderId = $hotelOrder->getOrderId();
                $order = $this->_orderFactory->create()->load($orderId);
                if (date('Y-m-d', strtotime($order->getCreatedAt())) == $date)
                    $dates[$date]++;
            }
        }
        return $dates;
    }

    public function getSaleBeforeXDay($packagesId, $dayNum)
    {
        $dates = [];
        for ($day = 0; $day < $dayNum; $day++) {
            $date = $this->getPreviousXDay($day);
            $dates [$date] = 0;
            $hotelOrders = $this->_hotelOrderFactory->create()->getCollection()
                ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED, Status::REFUNDED]])
                ->addFieldToFilter('package_id', ['in' => $packagesId]);
            foreach ($hotelOrders as $hotelOrder) {
                $orderId = $hotelOrder->getOrderId();
                $order = $this->_orderFactory->create()->load($orderId);
                if (date('Y-m-d', strtotime($order->getCreatedAt())) == $date)
                    $dates[$date] += $hotelOrder->getTotalPaid();
            }
        }
        return $dates;
    }

    public function getRefundedBeforeXDay($packagesId, $dayNum)
    {
        $dates = [];
        for ($day = 0; $day < $dayNum; $day++) {
            $date = $this->getPreviousXDay($day);
            $dates [$date] = 0;
            $hotelOrders = $this->_hotelOrderFactory->create()->getCollection()
                ->addFieldToFilter('status', ['in' => [Status::REFUNDED]])
                ->addFieldToFilter('package_id', ['in' => $packagesId]);
            foreach ($hotelOrders as $hotelOrder) {
                $orderId = $hotelOrder->getOrderId();
                $order = $this->_orderFactory->create()->load($orderId);
                if (date('Y-m-d', strtotime($order->getCreatedAt())) == $date)
                    $dates[$date]++;
            }
        }
        return $dates;
    }

    public function getRefundedValueBeforeXDay($packagesId, $dayNum)
    {
        $dates = [];
        for ($day = 0; $day < $dayNum; $day++) {
            $date = $this->getPreviousXDay($day);
            $dates [$date] = 0;
            $hotelOrders = $this->_hotelOrderFactory->create()->getCollection()
                ->addFieldToFilter('status', ['in' => [Status::REFUNDED]])
                ->addFieldToFilter('package_id', ['in' => $packagesId]);
            foreach ($hotelOrders as $hotelOrder) {
                $orderId = $hotelOrder->getOrderId();
                $order = $this->_orderFactory->create()->load($orderId);
                if (date('Y-m-d', strtotime($order->getCreatedAt())) == $date)
                    $dates[$date] += $hotelOrder->getTotalPaid();
            }
        }
        return $dates;
    }

    public function getOption($daynum)
    {
        $arr = [];
        $daystart = new \DateTime();
        $date = $daystart->sub(new \DateInterval('P' . $daynum . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $allPackagesId = $this->getPackagesOfVendor()->getAllIds();
        $soldInDays = $this->getSoldBeforeXDay($allPackagesId, $daynum);
        $soldInDaysValue = $this->getSaleBeforeXDay($allPackagesId, $daynum);
        $refundInDays = $this->getRefundedBeforeXDay($allPackagesId, $daynum);
        $refundInDaysValue = $this->getRefundedValueBeforeXDay($allPackagesId, $daynum);
        for ($i = 0; $i < $daynum; $i++) {
            $date->add(new \DateInterval('P1D'));
            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [$dateText, $sales, $qtySale, $refunds, $qtyRefund, 'color: #017cb9; stroke-color: #017cb9'];
        }
        return [$startDateText, $dateText, $arr];
    }
}
