<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/29/18
 * Time: 9:26 AM
 */

namespace Magenest\Hotel\Helper;


class Filter
{
    /**
     * all lowercase
     * @var array
     */
    protected $hotelCatNames = [
        'getaways',
        'getaway',
        'hotels',
        'hotel',
        'new product type'
    ];

    public function isHotelCategory(\Magento\Catalog\Model\Category $category = null)
    {
        if ($category === null) {
            return false;
        }
        return in_array(strtolower($category->getName()), $this->hotelCatNames);
    }
}
