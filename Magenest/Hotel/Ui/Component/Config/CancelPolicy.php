<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class CancelPolicy implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label'=>__('Cancellation Policy'), 'value'=>''],
            ['label'=>__('Day of Arrival'), 'value'=>'Day of Arrival'],
            ['label'=>__('1 Day'), 'value'=>"1 Day"],
            ['label'=>__('2 Days'), 'value'=>'2 Days'],
            ['label'=>__('3 Days'), 'value'=>'3 Days'],
            ['label'=>__('7 Days'), 'value'=>'7 Days'],
            ['label'=>__('14 Days'), 'value'=>'14 Days'],
        ];
    }
}
