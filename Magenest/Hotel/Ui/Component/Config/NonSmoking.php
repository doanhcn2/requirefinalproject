<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class NonSmoking implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label'=>__('Non Smoking'), 'value'=>0],
            ['label'=>__('Smoking'), 'value'=>1],
        ];
    }
}
