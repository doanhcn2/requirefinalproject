<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class TimeUnPublish implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label' => __('I would like my offer unpublished at the end of the offer availability period'), 'value' => 1],
            ['label' => __('I would like my offer unpublished on the following date '), 'value' => 0],
        ];
    }
}
