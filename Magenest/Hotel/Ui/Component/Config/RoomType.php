<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class RoomType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $roomType;

    /***
     * RoomType constructor.
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
     */
    public function __construct(
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
    ) {
        $this->roomType = $roomTypeAdminFactory;
    }

    public function toOptionArray()
    {
        $return = [];
        $model = $this->roomType->create()->getCollection();
        foreach ($model as $type) {
            $value = [
                'label' => __($type->getRoomType()),
                'value' => $type->getId(),
            ];
            array_push($return, $value);
        }
        return $return;
    }
}
