<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Locale\Bundle\LanguageBundle;
use Magento\Framework\Locale\Resolver;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class Languages implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $return = [];
        $data=[];
        $languages = (new LanguageBundle())->get(Resolver::DEFAULT_LOCALE)->get('Languages');
        $locales = \ResourceBundle::getLocales('') ?: [];
        foreach ($locales as $locale) {
            $language = \Locale::getPrimaryLanguage($locale);
            if (!$languages[$language]) {
                continue;
            }

            array_push($data, $languages[$language]);
        }
        $data = array_values(array_unique($data));
        sort($data);
        foreach ($data as $item) {
            $language = [
                'label' => __($item),
                'value' => $item,
            ];
            array_push($return, $language);
        }
        return $return;
    }
}
