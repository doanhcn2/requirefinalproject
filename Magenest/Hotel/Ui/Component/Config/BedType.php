<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class BedType
 * @package Magenest\Hotel\Ui\Component\Config
 */
class BedType implements ArrayInterface
{
    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $bedType;

    /**
     * BedType constructor.
     * @param \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory
     */
    public function __construct(
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory
    ) {
        $this->bedType = $bedTypeFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $return = [];
        $model = $this->bedType->create()->getCollection();
        foreach ($model as $type) {
            $value = [
                'label' => __($type->getBedType()),
                'value' => $type->getId(),
            ];
            array_push($return, $value);
        }
        return $return;
    }
}
