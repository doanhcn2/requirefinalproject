<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class Penalty implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label'=>__('Penalty'), 'value'=>''],
            ['label'=>__('100% of first Night'), 'value'=>'100% of first Night'],
            ['label'=>__('100% of Whole Stay'), 'value'=>'100% of Whole Stay']
        ];
    }
}
