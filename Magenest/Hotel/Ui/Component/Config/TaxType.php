<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Ui\Component\Config;

/**
 * Class TaxType
 * @package Magenest\Hotel\Ui\Component\Config
 */
class TaxType implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {

        return
            [
                [
                    'label' => __("$ / Stay"),
                    'value' => 1,
                ],
                [
                    'label' => __("$ / Night"),
                    'value' => 2,
                ],
                [
                    'label' => __("%"),
                    'value' => 3,
                ],
                [
                    'label' => __("$ / Person / Stay"),
                    'value' => 4,
                ],
            ];
    }
}
