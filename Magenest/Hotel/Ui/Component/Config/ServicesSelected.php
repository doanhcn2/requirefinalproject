<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 05/02/2018
 * Time: 09:48
 */

namespace Magenest\Hotel\Ui\Component\Config;


class ServicesSelected implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $_serviceFactory;

    /**
     * Request
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    protected $_servicesSelected;

    protected $_packageDetailsFactory;

    public function __construct(
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\Hotel\Model\PackageAndServiceFactory $servicesSelected
    ) {

        $this->_serviceFactory = $serviceFactory;
        $this->_request = $request;
        $this->_packageDetailsFactory = $packageDetailsFactory;
        $this->_servicesSelected = $servicesSelected;

    }

    public function toOptionArray()
    {
        $return = [];
        $params = $this->_request->getParams();
        if (!isset($params['id'])) {
            return $return;
        }
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $offer */
        $offer = $this->_packageDetailsFactory->create()->getCollection();
        $offer = $offer->getItemByProductId($params['id']);
        $services = $this->_serviceFactory->create()->getCollection()->addFieldToFilter("vendor_id", $offer->getVendorId());
        foreach ($services as $service) {
            $value = [
                'label' => __($service->getTitle()) . " - " . ($service->getPrice() + 0),
                'value' => $service->getId(),
            ];
            array_push($return, $value);
        }
        return $return;
    }
}
