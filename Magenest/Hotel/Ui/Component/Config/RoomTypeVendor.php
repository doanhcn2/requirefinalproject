<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Ui\Component\Config;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class RoomTypeVendor implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $roomType;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packageDetailsFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails
     */
    protected $_packageDetailsResource;

    /**
     * RoomTypeVendor constructor.
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageDetailsResource
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails $packageDetailsResource,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_request = $request;
        $this->roomType = $roomTypeVendorFactory;
        $this->_packageDetailsFactory = $packageDetailsFactory;
        $this->_packageDetailsResource = $packageDetailsResource;
    }

    public function toOptionArray()
    {
        $return = [];
        $productId = $this->_request->getParam('id');
        /** @var \Magenest\Hotel\Model\PackageDetails $model */
        $model = $this->_packageDetailsFactory->create();
        $this->_packageDetailsResource->load($model, $productId, 'product_id');
        $vendorId = $model->getVendorId();
        $models = $this->roomType->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId);
        foreach ($models as $type) {
            $value = [
                'label' => __($type->getRoomTitle()),
                'value' => $type->getId(),
            ];
            array_push($return, $value);
        }

        return $return;
    }
}
