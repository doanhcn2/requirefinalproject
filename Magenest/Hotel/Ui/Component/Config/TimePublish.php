<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Ui\Component\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Languages
 * @package Magenest\Hotel\Ui\Component\Config
 */
class TimePublish implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['label' => __('I would like my offer published as soon as possible'), 'value' => 1],
            ['label' => __('Publish my offer from the following date'), 'value' => 0],
        ];
    }
}
