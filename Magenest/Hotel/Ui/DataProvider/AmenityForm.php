<?php
namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class AmenityForm
 * @package Magenest\Hotel\Ui\DataProvider
 */
class AmenityForm extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    public function __construct(
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenityFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $amenityFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $request = $objectManager->get('Magento\Framework\App\RequestInterface');
//        $id = $request->getParam('amenity_id');
//        if($id){
//            $model = $objectManager->create('Magenest\Hotel\Model\AmenityGroup')->load($id);
//            $data[$id]= $model->getData();
//            $data[$id]['pdf_coordinates'] = unserialize($data[$id]['pdf_coordinates']);
//            $data[$id]['pdf_background'] =unserialize($data[$id]['pdf_background']);
//        }
        return $data;
    }
}