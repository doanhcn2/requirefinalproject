<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\RequestInterface;

/**
 * Class HotelDataProvider
 * @package Magenest\Hotel\Ui\DataProvider\Product\Form\Modifier
 */
class HotelDataProvider extends AbstractModifier
{
    const PRODUCT_TYPE = 'hotel';
    const CONTROLLER_ACTION_EDIT_PRODUCT = 'catalog_product_edit';
    const CONTROLLER_ACTION_NEW_PRODUCT = 'catalog_product_new';

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var RequestInterface
     */
    protected $request;

    protected $_hotelPropertyFactory;
    protected $_roomTypeFactory;
    protected $_roomAmenitiesVendorFactory;
    protected $_roomAmenitiesAdminFactory;
    protected $_serviceFactory;
    protected $_packageDetailsFactory;
    protected $_packageAndServiceFactory;

    protected $_storeManager;

    /**
     * HotelDataProvider constructor.
     * @param RequestInterface $request
     * @param LocatorInterface $locator
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenitiesVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $roomAmenitiesAdminFactory,
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magenest\Hotel\Model\PackageAndServiceFactory $packageAndServiceFactory,
        RequestInterface $request,
        LocatorInterface $locator
    )
    {
        $this->_storeManager = $storeManager;
        $this->_packageAndServiceFactory = $packageAndServiceFactory;
        $this->_hotelPropertyFactory = $hotelPropertyFactory;
        $this->_roomAmenitiesAdminFactory = $roomAmenitiesAdminFactory;
        $this->_roomAmenitiesVendorFactory = $roomAmenitiesVendorFactory;
        $this->_roomTypeFactory = $roomTypeFactory;
        $this->_serviceFactory = $serviceFactory;
        $this->_packageDetailsFactory = $packageDetailsFactory;
        $this->request = $request;
        $this->locator = $locator;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        if ($this->isHotel()) {
            /** @var \Magenest\Ticket\Model\Event $eventModel */
            $offerModel = $this->_packageDetailsFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->getFirstItem();
            if ($offerModel->getId()) {
                $property = $this->_hotelPropertyFactory->create()->getCollection()
                    ->addFieldToFilter('vendor_id', $offerModel->getVendorId())->getFirstItem();
                $data[strval($productId)]['hotel']['property-detail'] = [
                    'check_in_from' => $property->getCheckInFrom(),
                    'check_in_to' => $property->getCheckInTo(),
                    'check_out_from' => $property->getCheckOutFrom(),
                    'check_out_to' => $property->getCheckOutTo(),
                    'is_internet' => $property->getIsInternet(),
                    'is_parking' => $property->getIsParking(),
                    'is_breakfast' => $property->getIsBreakfast(),
                    'is_children_allowed' => $property->getIsChildrenAllowed(),
                    'is_pet_allowed' => $property->getIsPetAllowed()
                ];

                $languages = $property->getLanguages();
                if ($languages != null) {
                    $languages = json_decode($languages, true);
                    for ($i = 0; $i < count($languages); $i++) {
                        $data[strval($productId)]['hotel']['property-detail']['add_languages'][$i]["languages"] = $languages[$i];
                    }

                }

                $data[strval($productId)]['hotel']['property-facilities'] = [
                    'service_desk_24h' => $property->getServiceDesk24h(),
                    'gym' => $property->getGym(),
                    'laundry' => $property->getLaundry(),
                    'smoking_rooms' => $property->getSmokingRooms(),
                    'pool' => $property->getPool(),
                    'shuttle_service' => $property->getShuttleService(),
                    'restaurant' => $property->getRestaurant(),
                    'bar' => $property->getBar(),
                    'non_smoking_rooms' => $property->getNonSmokingRooms(),
                    'bbq_facility' => $property->getBbqFacility(),
                    'front_desk' => $property->getFrontDesk(),
                    'concierge' => $property->getConcierge(),
                    'terms' => $property->getTerms(),
                    'is_tax' => $property->getIsTax(),
                    'tax_amount' => $property->getTaxAmount(),
                    'tax_type' => $property->getTaxType()
                ];
                $data[strval($productId)]['hotel']['package-offer'] = [
                    'room_type' => $offerModel->getRoomType(),
                    'book_to' => $offerModel->getBookTo(),
                    'book_from' => $offerModel->getBookFrom(),
                    'min_nights' => $offerModel->getMinNights(),
                    'max_nights' => $offerModel->getMaxNights(),
                    'cancellation_policy' => $offerModel->getCancellationPolicy(),
                    'penalty' => $offerModel->getPenalty(),
                    'min_time_to_book' => $offerModel->getMinTimeToBook(),
                    'is_published_asap' => $offerModel->getIsPublishedAsap(),
                    'publish_date' => $offerModel->getPublishDate(),
                    'is_unpublished_alap' => $offerModel->getIsUnpublishedAlap(),
                    'unpublish_date' => $offerModel->getUnpublishDate(),
                    'special_terms' => $offerModel->getSpecialTerms(),
                ];
                $roomType = $this->_roomTypeFactory->create()->load($offerModel->getRoomType());
                if ($roomType->getId()) {
                    $data[strval($productId)]['hotel']['room_type_vendor'] = [
                        'room_id' => $roomType->getId(),
                        'number_rooms_available' => $roomType->getNumberRoomsAvailable(),
                        'room_title' => $roomType->getRoomTitle(),
                        'room_description' => $roomType->getDescription(),
                        'non_smoking' => $roomType->getNonSmoking(),
                        'number_guests' => $roomType->getNumberGuests(),
                        'room_size' => $roomType->getRoomSize(),
                        'has_extra_beds' => $roomType->getHasExtraBeds(),
                        'number_extra_beds' => $roomType->getNumberExtraBeds(),
                        'price_per_bed' => $roomType->getPricePerBed(),
                        'photo_room'=>json_decode($roomType->getPhotoRoom(),true)
                    ];
                    $beds = json_decode($roomType->getBedsType(), true);
                    for ($j = 0; $j < count($beds); $j++) {
                        $data[strval($productId)]['hotel']['room_type_vendor']["list_bed_type"]['add_bed_type'][$j] = [
                            'bedId' => $beds[$j]['bedId'],
                            'numberBeds' => $beds[$j]['numberBeds']
                        ];
                    }
                }
                $servicesVendor = $this->_serviceFactory->create()->getCollection()
                    ->addFieldToFilter("vendor_id", $offerModel->getVendorId());
                $servicesSelected = $this->_packageAndServiceFactory->create()->getCollection()
                    ->addFieldToFilter("package_id", $offerModel->getId());
                foreach ($servicesSelected as $service) {
                    $data[strval($productId)]['hotel']['package-offer']['services_selected'][] = $service->getServiceId();
                }
                $index = 0;
                foreach ($servicesVendor as $service) {
                    $data[strval($productId)]['hotel']['list_property_services']['add_property_services'][$index] = [
                        'title' => $service->getTitle(),
                        'description' => $service->getDescription(),
                        'price' => $service->getPrice(),
                        'id' => $service->getId()
                    ];
                    $index++;
                }
                $collectionRoomKind = $this->_roomAmenitiesAdminFactory->create()->getCollection()->addFieldToSelect("room_kind")->getSelect()->distinct(true);
                $amenityVendor = $this->_roomAmenitiesVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $offerModel->getVendorId());
                $allRoomTypeIds = $this->_roomTypeFactory->create()->getCollection()->addFieldToFilter("vendor_id", $offerModel->getVendorId())->getAllIds();
                $index1 = 0;
                $dataAmenitiesVendor = [];
                foreach ($amenityVendor as $item) {
                    $dataAmenitiesVendor [$item->getAmenity()][] = $item->getRoomVendorId();
                }
                $roomsKind = $this->_roomAmenitiesAdminFactory->create()->getCollection()->getConnection()->fetchAll($collectionRoomKind);
                foreach ($roomsKind as $kind) {
                    $data[strval($productId)]['hotel']['rooms_kind'][$index1]['room_kind_title'] = $kind['room_kind'];
                    $amenities = $this->_roomAmenitiesAdminFactory->create()->getCollection()->addFieldToFilter('room_kind', $kind["room_kind"]);
                    $index2 = 0;
                    foreach ($amenities as $amenity) {
                        $data[strval($productId)]['hotel']['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['amenity_id'] = $amenity->getId();
                        $data[strval($productId)]['hotel']['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['amenity_title'] = $amenity->getName();
                        if (array_key_exists($amenity->getId(), $dataAmenitiesVendor)) {
                            if (count($dataAmenitiesVendor[$amenity->getId()]) == count($allRoomTypeIds))
                                $data[strval($productId)]['hotel']['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['all_rooms'] = "1";
                            else {
                                foreach ($dataAmenitiesVendor[$amenity->getId()] as $room) {
                                    $data[strval($productId)]['hotel']['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['some_rooms'] = $room;
                                }
                            }

                        }
                        $index2++;
                    }
                    $index1++;
                }
            }
        }
        return $data;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        if (!$this->isHotel()) {
            $meta['hotel']['arguments']['data']['config'] = [
                'disabled' => true,
                'visible' => false
            ];
        } else {
            unset($meta['product-details']['children']['container_price']);
            unset($meta['product-details']['children']['quantity_and_stock_status_qty']);
        }
        return $meta;
    }

    /**
     * @return bool
     */
    protected function isHotel()
    {
        $actionName = $this->request->getFullActionName();
        $isHotel = false;
        if ($actionName == self::CONTROLLER_ACTION_EDIT_PRODUCT) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->locator->getProduct();
            if ($product->getTypeId() == self::PRODUCT_TYPE) {
                $isHotel = true;
            }
        } elseif ($actionName == self::CONTROLLER_ACTION_NEW_PRODUCT) {
            if (self::PRODUCT_TYPE == $this->request->getParam('type')) {
                $isHotel = true;
            }
        }

        return $isHotel;
    }
}
