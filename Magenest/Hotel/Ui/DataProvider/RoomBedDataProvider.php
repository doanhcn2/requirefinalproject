<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class RoomBedDataProvider
 * @package Magenest\Hotel\Ui\DataProvider
 */
class RoomBedDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * RoomBedDataProvider constructor.
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $templateFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param array $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magenest\Hotel\Model\RoomTypeAdminFactory $templateFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $templateFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelRoom = $objectManager->create('Magenest\Hotel\Model\RoomTypeAdmin')->getCollection();
        $modelBed = $objectManager->create('Magenest\Hotel\Model\BedType')->getCollection();
        $arrayRoom = [];
        foreach ($modelRoom as $value) {
            $roomArray = [
                'id' => $value->getId(),
                'room_type' => $value->getRoomType()
            ];
            $arrayRoom [] = $roomArray;
        }
        $data['']['add_room_type'] = $arrayRoom;

        $arrayBed = [];
        foreach ($modelBed as $value) {
            $bedArray = [
                'id' => $value->getId(),
                'bed_type' => $value->getBedType()
            ];
            $arrayBed [] = $bedArray;
        }
        $data['']['add_bed_type'] = $arrayBed;

        return $data;
    }
}
