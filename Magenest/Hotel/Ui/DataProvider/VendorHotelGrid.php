<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 29/01/2018
 * Time: 14:27
 */
namespace Magenest\Hotel\Ui\DataProvider;


/**
 * Class VendorHotelGrid
 * @package Magenest\Hotel\Ui\DataProvider
 */
class VendorHotelGrid extends \Magento\Ui\DataProvider\AbstractDataProvider

{

    protected $_productFactory;

    public function __construct(
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $vendorFactory->create()->getCollection()->addFieldToFilter("promote_product",2);
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }
}