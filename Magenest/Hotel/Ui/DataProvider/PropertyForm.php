<?php

namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class PropertyForm
 * @package Magenest\Hotel\Ui\DataProvider
 */
class PropertyForm extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * @var \Magenest\Hotel\Model\HotelPropertyFactory
     */
    protected $_hotelPropertyFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomAmenitiesVendorFactory
     */
    protected $_roomAmenitiesVendorFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomAmenitiesAdminFactory
     */
    protected $_roomAmenitiesAdminFactory;

    /**
     * @var \Magenest\Hotel\Model\ServiceFactory
     */
    protected $_serviceFactory;

    /**
     * @var \Magenest\Hotel\Model\PackageAndServiceFactory
     */
    protected $_packageAndServiceFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    protected $_roomFacilityAdminFactory;

    protected $_roomFacilityVendorFactory;

    /**
     * PropertyForm constructor.
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory
     * @param \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory
     * @param \Magenest\Hotel\Model\ServiceFactory $serviceFactory
     * @param \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenitiesVendorFactory
     * @param \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $roomAmenitiesAdminFactory
     * @param \Magenest\Hotel\Model\PackageAndServiceFactory $packageAndServiceFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magenest\Hotel\Model\ServiceFactory $serviceFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\PropertyFacilityAdminFactory $roomFacilityAdminFactory,
        \Magenest\Hotel\Model\PropertyFacilityVendorFactory $roomFacilityVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenitiesVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $roomAmenitiesAdminFactory,
        \Magenest\Hotel\Model\PackageAndServiceFactory $packageAndServiceFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    )
    {
        $this->_request = $request;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->collection = $packageDetailsFactory->create()->getCollection();
        $this->_hotelPropertyFactory = $hotelPropertyFactory;
        $this->_roomAmenitiesAdminFactory = $roomAmenitiesAdminFactory;
        $this->_roomAmenitiesVendorFactory = $roomAmenitiesVendorFactory;
        $this->_roomFacilityVendorFactory = $roomFacilityVendorFactory;
        $this->_roomFacilityAdminFactory = $roomFacilityAdminFactory;
        $this->_serviceFactory = $serviceFactory;
        $this->_packageAndServiceFactory = $packageAndServiceFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        $vendorId = $this->_request->getParam('vendor_id');

        if ($vendorId) {
            // tab Property Detail
            $property = $this->_hotelPropertyFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $vendorId)->getFirstItem();
            $data[$vendorId]['vendor_id'] = $vendorId;
            $data[$vendorId]['property-detail'] = [
                'check_in_from' => $property->getCheckInFrom(),
                'check_in_to' => $property->getCheckInTo(),
                'check_out_from' => $property->getCheckOutFrom(),
                'check_out_to' => $property->getCheckOutTo(),
                'is_internet' => $property->getIsInternet(),
                'is_parking' => $property->getIsParking(),
                'is_breakfast' => $property->getIsBreakfast(),
                'is_children_allowed' => $property->getIsChildrenAllowed(),
                'is_pet_allowed' => $property->getIsPetAllowed()
            ];
            $languages = $property->getLanguages();
            if ($languages != null) {
                $languages = json_decode($languages, true);
                for ($i = 0; $i < count($languages); $i++) {
                    $data[$vendorId]['property-detail']['add_languages'][$i]["languages"] = $languages[$i];
                }
            }

            //tab Facilities
//            $data[$vendorId]['property-facilities'] = [
//                'service_desk_24h' => $property->getServiceDesk24h(),
//                'gym' => $property->getGym(),
//                'laundry' => $property->getLaundry(),
//                'smoking_rooms' => $property->getSmokingRooms(),
//                'pool' => $property->getPool(),
//                'shuttle_service' => $property->getShuttleService(),
//                'restaurant' => $property->getRestaurant(),
//                'bar' => $property->getBar(),
//                'non_smoking_rooms' => $property->getNonSmokingRooms(),
//                'bbq_facility' => $property->getBbqFacility(),
//                'front_desk' => $property->getFrontDesk(),
//                'concierge' => $property->getConcierge(),
//                'terms' => $property->getTerms(),
//                'is_tax' => $property->getIsTax(),
//                'tax_amount' => $property->getTaxAmount(),
//                'tax_type' => $property->getTaxType()
//            ];
            $collectionRoomKind = $this->_roomFacilityAdminFactory->create()->getCollection()->addFieldToSelect("facility_kind")->getSelect()->distinct(true);
            $facilityVendor = $this->_roomFacilityVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
            $indexFac1 = 0;
            $dataFacilityVendor = [];
            foreach ($facilityVendor as $item) {
                $dataFacilityVendor [] = $item->getFacilityId();
            }
            $roomsKind = $this->_roomFacilityAdminFactory->create()->getCollection()->getConnection()->fetchAll($collectionRoomKind);
            foreach ($roomsKind as $kind) {
                $data[$vendorId]['rooms_kind_facility'][$indexFac1]['room_kind_title'] = $kind['facility_kind'];
                $facilities = $this->_roomFacilityAdminFactory->create()->getCollection()->addFieldToFilter('facility_kind', $kind["facility_kind"]);
                $indexFac2 = 0;
                foreach ($facilities as $facility) {
                    $data[$vendorId]['rooms_kind_facility'][$indexFac1]["room_facilities"]['facilities_collection'][$indexFac2]['facility_id'] = $facility->getId();
                    $data[$vendorId]['rooms_kind_facility'][$indexFac1]["room_facilities"]['facilities_collection'][$indexFac2]['facility_title'] = $facility->getName();
                    if (in_array($facility->getId(), $dataFacilityVendor)) {
                        $data[$vendorId]['rooms_kind_facility'][$indexFac1]["room_facilities"]['facilities_collection'][$indexFac2]['room_facility'] = "1";
                    }
                    $indexFac2++;
                }
                $indexFac1++;
            }
        }
        //tab amenity
        $collectionRoomKind = $this->_roomAmenitiesAdminFactory->create()->getCollection()->addFieldToSelect("room_kind")->getSelect()->distinct(true);
        $amenityVendor = $this->_roomAmenitiesVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId);
        $allRoomTypeIds = $this->_roomTypeVendorFactory->create()->getCollection()->addFieldToFilter("vendor_id", $vendorId)->getAllIds();
        $index1 = 0;
        $dataAmenitiesVendor = [];
        foreach ($amenityVendor as $item) {
            $dataAmenitiesVendor [$item->getAmenity()][] = $item->getRoomVendorId();
        }
        $roomsKind = $this->_roomAmenitiesAdminFactory->create()->getCollection()->getConnection()->fetchAll($collectionRoomKind);
        foreach ($roomsKind as $kind) {
            $data[$vendorId]['rooms_kind'][$index1]['room_kind_title'] = $kind['room_kind'];
            $amenities = $this->_roomAmenitiesAdminFactory->create()->getCollection()->addFieldToFilter('room_kind', $kind["room_kind"]);
            $index2 = 0;
            foreach ($amenities as $amenity) {
                $data[$vendorId]['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['amenity_id'] = $amenity->getId();
                $data[$vendorId]['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['amenity_title'] = $amenity->getName();
                if (array_key_exists($amenity->getId(), $dataAmenitiesVendor)) {
                    if (count($dataAmenitiesVendor[$amenity->getId()]) == count($allRoomTypeIds))
                        $data[$vendorId]['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['all_rooms'] = "1";
                    else {
                        foreach ($dataAmenitiesVendor[$amenity->getId()] as $room) {
                            $data[$vendorId]['rooms_kind'][$index1]["amenities"]['amenities_collection'][$index2]['some_rooms'] = $room;
                        }
                    }

                }
                $index2++;
            }
            $index1++;

            //tab Service
            $servicesVendor = $this->_serviceFactory->create()->getCollection()
                ->addFieldToFilter("vendor_id", $vendorId);
            $index = 0;
            foreach ($servicesVendor as $service) {
                $data[$vendorId]['list_property_services']['add_property_services'][$index] = [
                    'title' => $service->getTitle(),
                    'description' => $service->getDescription(),
                    'price' => $service->getPrice(),
                    'id' => $service->getId()
                ];
                $index++;
            }
        }
        return $data;
    }
}
