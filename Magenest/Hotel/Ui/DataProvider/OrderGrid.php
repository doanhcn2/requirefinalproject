<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 29/01/2018
 * Time: 14:27
 */
namespace Magenest\Hotel\Ui\DataProvider;


/**
 * Class OrderGrid
 * @package Magenest\Hotel\Ui\DataProvider
 */

class OrderGrid extends \Magento\Ui\DataProvider\AbstractDataProvider

{

    protected $_productFactory;

    public function __construct(
        \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $hotelOrderFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }

        $data =  $this->getCollection();
        $data = $this->joinCustomerCollection($data);
        $data = $this->joinOrderCollection($data);
        $data = $this->joinPackageDetailCollection($data);

        $data = $data->getData();
        $data = $this->getProductName($data);

        return [
            "totalRecords"=>count($data),
            "items"=>$data
        ];
    }

    public function joinCustomerCollection($collection){
        $collection->getSelect()->joinLeft(
            $collection->getTable('customer_entity'),
            "main_table.customer_id = customer_entity.entity_id" ,
            "customer_entity.email"
        );
        return $collection;
    }

    public function joinOrderCollection($collection){
        $collection->getSelect()->joinLeft(
            $collection->getTable('sales_order'),
            "main_table.order_id = sales_order.entity_id" ,
            "sales_order.increment_id"
        );
        return $collection;
    }


    public function joinPackageDetailCollection($collection){
        $collection->getSelect()->joinLeft(
            $collection->getTable('magenest_hotel_package_details'),
            "main_table.package_id = magenest_hotel_package_details.id" ,
            "magenest_hotel_package_details.product_id"
        );
        return $collection;
    }
    public function getProductName($collection){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach ($collection as $key=>$value) {
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($value['product_id']);
            $collection[$key]['offer_title'] = $product->getName();

        }
        return $collection;
    }
}