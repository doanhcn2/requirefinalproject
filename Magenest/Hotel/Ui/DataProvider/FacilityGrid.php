<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 29/01/2018
 * Time: 14:27
 */
namespace Magenest\Hotel\Ui\DataProvider;


/**

 * Class PetsDataProvider

 */

class FacilityGrid extends \Magento\Ui\DataProvider\AbstractDataProvider

{

    protected $_productFactory;

    public function __construct(
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenitiesAdminFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $amenitiesAdminFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $data =  $this->getCollection()->toArray();

        foreach ($data['items'] as $key=>$value) {
            $data['items'][$key]['is_enable'] = ($value['is_enable']==1)?'Enable':'Disable';
        }

        return $data;
    }

}