<?php

namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class PropertyForm
 * @package Magenest\Hotel\Ui\DataProvider
 */
class RoomVendorForm extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;


    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * RoomVendorForm constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    )
    {
        $this->_request = $request;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->collection = $roomTypeVendorFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        $roomId = $this->_request->getParam('room_id');
        $roomType = $this->_roomTypeVendorFactory->create()->load($roomId);
        //tab Facilities
        if ($roomId) {
            $data[$roomId]['room_type_vendor'] = [
                'vendor_id'=>$this->_request->getParam('vendor_id'),
                'room_id' => $roomType->getId(),
                'number_rooms_available' => $roomType->getNumberRoomsAvailable(),
                'room_title' => $roomType->getRoomTitle(),
                'room_description' => $roomType->getDescription(),
                'non_smoking' => $roomType->getNonSmoking(),
                'number_guests' => $roomType->getNumberGuests(),
                'room_size' => $roomType->getRoomSize(),
                'has_extra_beds' => $roomType->getHasExtraBeds(),
                'number_extra_beds' => $roomType->getNumberExtraBeds(),
                'price_per_bed' => $roomType->getPricePerBed(),
//                'photo_room' => json_decode($roomType->getPhotoRoom(), true)
            ];
            $beds = json_decode($roomType->getBedsType(), true);
            for ($j = 0; $j < count($beds); $j++) {
                $data[$roomId]['room_type_vendor']["list_bed_type"]['add_bed_type'][$j] = [
                    'bedId' => $beds[$j]['bedId'],
                    'numberBeds' => $beds[$j]['numberBeds']
                ];
            }
        }

        return $data;
    }
}