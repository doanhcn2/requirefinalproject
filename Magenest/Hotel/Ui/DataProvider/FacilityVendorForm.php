<?php
namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class FacilityVendorForm
 * @package Magenest\Hotel\Ui\DataProvider
 */
class FacilityVendorForm extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    public function __construct(
        \Magenest\Hotel\Model\PropertyFacilityVendorFactory $facilityVendorFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $facilityVendorFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        return $data;
    }
}
