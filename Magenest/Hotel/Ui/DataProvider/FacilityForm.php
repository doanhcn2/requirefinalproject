<?php
namespace Magenest\Hotel\Ui\DataProvider;

/**
 * Class FacilityForm
 * @package Magenest\Hotel\Ui\DataProvider
 */
class FacilityForm extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    public function __construct(
        \Magenest\Hotel\Model\PropertyFacilityAdminFactory $facilityFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $facilityFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;

        return $data;
    }
}
