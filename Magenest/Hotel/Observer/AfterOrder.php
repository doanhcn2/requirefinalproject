<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 1/30/18
 * Time: 11:28 PM
 */

namespace Magenest\Hotel\Observer;

use Magenest\Hotel\Helper\BookingStatus as Status;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item;

class AfterOrder implements ObserverInterface
{
    protected $_quoteFactory;
    protected $_hotelOrderFactory;
    protected $_packageDetailsFactory;
    protected $_request;

    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory
    ) {
        $this->_request = $request;
        $this->_quoteFactory = $quoteFactory;
        $this->_hotelOrderFactory = $hotelOrderFactory;
        $this->_packageDetailsFactory = $packageDetailsCollectionFactory;
    }

    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $salesOrder */
        $salesOrder = $observer->getData('order');

        /** @var \Magento\Sales\Model\Order\Item[] $items */
        $items = $salesOrder->getAllVisibleItems();
        $request = $this->_request->getParams();

        if(array_key_exists("creditmemo",$request))
            return;
        $quoteId = $salesOrder->getQuoteId();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_quoteFactory->create()->load($quoteId);
//        $items = $quote->getAllVisibleItems();
        foreach ($items as $salesItem) {
            if ($salesItem->getProductType() !== 'hotel' || $salesItem->getStatusId() === Item::STATUS_INVOICED || isset($request['udpo'])) {
                continue;
            }
            $quoteItemId = $salesItem->getQuoteItemId();
//            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $quote->getItemById($quoteItemId);
            $total = $salesItem->getRowTotalInclTax() ?: $salesItem->getRowTotal();
            $options = $item->getOptions();
            $checkInDate = $item->getOptionByCode('check_in_date')->getValue();
            $checkOutDate = $item->getOptionByCode('check_out_date')->getValue();
            $services = $item->getOptionByCode('services');
            if ($services) {
                $services = $services->getValue();
            }
            $qty = $item->getOptionByCode('room_qty')->getValue();
            $productId = $item->getProduct()->getId();
            $packageId = $this->_packageDetailsFactory->create()->getItemByProductId($productId)->getId();
            /** @var \Magenest\Hotel\Model\HotelOrder $hotelOrder */
            $hotelOrder = $this->_hotelOrderFactory->create();
            $hotelOrder->setPackageId($packageId);
            $hotelOrder->setOrderId($salesOrder->getId());
            $hotelOrder->setServices($services);
            $hotelOrder->setQty($item->getQty());
            $hotelOrder->setStatus(Status::PENDING);
            $hotelOrder->setCheckInDate($checkInDate);
            $hotelOrder->setCheckOutDate($checkOutDate);
            $hotelOrder->setCustomerId($salesOrder->getCustomerId());
            $hotelOrder->setTotalPaid($total);
            $hotelOrder->save();
        }
    }
}
