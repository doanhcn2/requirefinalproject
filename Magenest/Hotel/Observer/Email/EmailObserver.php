<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/11/17
 * Time: 3:33 PM
 */

namespace Magenest\Hotel\Observer\Email;

abstract class EmailObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magenest\Hotel\Helper\Email
     */
    protected $_email;

    /**
     * @var string
     */
    protected $_toAddress;

    /**
     * @var string
     */
    protected $_toName;

    /**
     * @var array
     */
    protected $_var;

    /**
     * @var string
     */
    protected $_templateId;

    /**
     * @var \Magenest\Hotel\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * EmailObserver constructor.
     * @param \Magenest\Hotel\Logger\Logger $logger
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magenest\Hotel\Helper\Email $emailHelper
     */
    public function __construct(
        \Magenest\Hotel\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $url,
        \Magenest\Hotel\Helper\Email $emailHelper
    ) {

        $this->_urlBuilder = $url;
        $this->_logger = $logger;
        $this->_email = $emailHelper;
        $this->_var = [];
    }

    /**
     * @param $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->_toAddress = $address;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setToName($name)
    {
        $this->_toName = $name;
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setVar($data)
    {
        $this->_var = $data;
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function send()
    {
        try {
            $this->_email->send(
                $this->_toAddress,
                $this->_toName,
                $this->_templateId,
                $this->_var
            );
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
            throw $e;
        }
    }
}
