<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/11/17
 * Time: 3:44 PM
 */

namespace Magenest\Hotel\Observer\Email;

use Magento\Customer\Api\CustomerRepositoryInterface;

class Cancel extends EmailObserver
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magenest\Hotel\Helper\Helper
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuiler;

    /**
     * Cancel constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magenest\Hotel\Logger\Logger $logger
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magenest\Hotel\Helper\Email $emailHelper
     * @param \Magenest\Hotel\Helper\Helper $helper
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magenest\Hotel\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $url,
        \Magenest\Hotel\Helper\Email $emailHelper,
        \Magenest\Hotel\Helper\Helper $helper
    ) {

        $this->_urlBuiler = $url;
        $this->_helper = $helper;
        $this->_customerRepository = $customerRepository;
        $this->_templateId = $emailHelper::CANCEL_ORDER_TEMPLATE_ID;
        parent::__construct($logger, $url, $emailHelper);
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $bookingId = $observer->getEvent()->getData('booking_id');
        if ($bookingId) {
            $orderInfo = $this->_helper->getOrderInfo($bookingId);
            $customer = $this->_customerRepository->get(@$orderInfo['customer_id']);
            $this->setAddress($customer->getEmail());
            $name = $customer->getFirstname() . ' ' . $customer->getLastname();
            $this->setToName($name);
            $this->setVar([
                'customer_name' => $name,
                'order_id' => $orderInfo['order_id']
            ]);

            $this->send();
        }
    }
}
