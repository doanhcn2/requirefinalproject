<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Observer\Backend\Product;

use Magenest\Hotel\Model\Product\ScreenShot\Config;
use Magenest\Hotel\Model\ProductStatus;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Save
 * @package Magenest\Hotel\Observer\Backend\Product
 */
class Save implements ObserverInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Hotel\Model\HotelProperty
     */
    protected $_hotelProperty;

    /**
     * @var ObjectManager
     */
    protected $_objManager;

    protected $_amenityVendorFactory;

    protected $_roomTypeVendorFactory;

    protected $_logger;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * Save constructor.
     * @param RequestInterface $request
     * @param StoreManagerInterface $storeManagerInterface
     * @param Config $config
     * @param \Magenest\Hotel\Helper\File $helperFile
     * @param \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenityVendorFactory
     * @param ManagerInterface $messageManager
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magenest\Hotel\Logger\Logger $logger
     */
    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManagerInterface,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenityVendorFactory,
        ManagerInterface $messageManager,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Logger\Logger $logger
    )
    {
        $this->storeManage = $storeManagerInterface;
        $this->_amenityVendorFactory = $amenityVendorFactory;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_request = $request;
        $this->_logger = $logger;
        $this->messageManager = $messageManager;
        $this->_objManager = ObjectManager::getInstance();
    }

    /**
     * Set new customer group to all his quotes
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $observer->getEvent()->getProduct();
            $productId = $product->getId();
            $productTypeId = $product->getTypeId();
            $params = $this->_request->getParams();
            if (empty($params['hotel']) || $productTypeId !== 'hotel') {
                return;
            }
            $vendorId = $product->getData('udropship_vendor');
            if (!$vendorId) {
                return;
            }
            $dataHotel = $params['hotel'];


            if (@$dataHotel['package-offer']) {
                if ($params['product']['status'] == ProductStatus::STATUS_ENABLED
                    && @$dataHotel['package-offer']['is_published_asap'] !== '1'
                    && strtotime(@$dataHotel['package-offer']['publish_date']) !== false) {
                    if (strtotime($dataHotel['package-offer']['publish_date']) > strtotime(date('d/m/Y'))) {
                        $product->setStatus(ProductStatus::STATUS_WAITING_TO_BE_PUBLISHED)->save();
                    }
                }

                /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
                $packageDetailsCollection = $this->_objManager->create(\Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection::class);
                $packageDetails = $packageDetailsCollection->getItemByProductId($productId);
                $packageDetails->addData($dataHotel['package-offer'])->save();

                $selectedServices = @$dataHotel['package-offer']['services_selected'];
                /** @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection $packageServiceCollection */
                $packageServiceCollection = $this->_objManager->create(\Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection::class);
                $packageServiceCollection->addFieldToFilter('package_id', $packageDetails->getId())->walk('delete');
                if (is_array($selectedServices)) {
                    foreach ($selectedServices as $service) {
                        /** @var \Magenest\Hotel\Model\PackageAndService $packageService */
                        $packageService = $this->_objManager->create(\Magenest\Hotel\Model\PackageAndService::class);
                        $packageService->setPackageId($packageDetails->getId());
                        $packageService->setServiceId($service);
                        $packageService->save();
                    }
                }

                if (@$dataHotel['package-offer']['hotel_prices']) {
                    $package = json_decode($dataHotel['package-offer']['hotel_prices'], true);
                    /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
                    $packagePriceCollection = $this->_objManager->create(\Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection::class);
                    $packagePriceCollection->addFieldToFilter('package_id', $packageDetails->getId())->walk('delete');
                    if (is_array($package)) {
                        foreach ($package as $date => $prices) {
                            /** @var \Magenest\Hotel\Model\PackagePrice $packagePrice */
                            $packagePrice = $this->_objManager->create(\Magenest\Hotel\Model\PackagePrice::class);
                            $packagePrice->loadByPackageIdAndDate($packageDetails->getId(), $date);
                            $packagePrice->setSalePrice($prices['salePrice']);
                            $packagePrice->setOrigPrice($prices['origPrice']);
                            $packagePrice->setQty($prices['roomQty']);
                            $packagePrice->save();
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Save Hotel Error"));
            $this->_logger->debug($e->getMessage());
        }
        return;
    }

    public function loadHotelProperty($vendorId)
    {
        if (!$this->_hotelProperty) {
            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            $hotelProperty = $this->_objManager->create(\Magenest\Hotel\Model\HotelProperty::class);
            $this->_hotelProperty = $hotelProperty->loadByVendorId($vendorId);
        }
        return $this->_hotelProperty;
    }

}
