<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 04/04/2018
 * Time: 14:08
 */

namespace Magenest\Hotel\Observer\Backend\Product;

use Magenest\Hotel\Model\Product\ScreenShot\Config;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;

class BeforeSave implements ObserverInterface
{

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Hotel\Model\HotelProperty
     */
    protected $_hotelProperty;

    /**
     * @var ObjectManager
     */
    protected $_objManager;

    protected $config;
    protected $_amenityVendorFactory;
    protected $_roomTypeVendorFactory;
    protected $_logger;
    protected $messageManager;
    protected $campaignStatus;

    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManagerInterface,
        Config $config,
        ManagerInterface $messageManager,
        \Magenest\Hotel\Logger\Logger $logger,
        \Magenest\FixUnirgy\Cron\CheckCampaignStatus $checkCampaignStatus
    ) {
        $this->storeManage = $storeManagerInterface;
        $this->messageManager = $messageManager;
        $this->config = $config;
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_objManager = ObjectManager::getInstance();
        $this->campaignStatus = $checkCampaignStatus;
    }


    public function execute(Observer $observer)
    {
        try {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $observer->getEvent()->getProduct();
            $params = $this->_request->getParams();
            if ($product->getTypeId() != "hotel")
                return;
            // must fix 1 after add approve campaign in admin
            if ($params['product']['campaign_status'] == -1) {
                $bookFrom = strtotime($params['hotel']['package-offer']['book_from']);
                $bookTo = strtotime($params['hotel']['package-offer']['book_to']);
                if ($params['hotel']['package-offer']['is_published_asap'] == '1') {
                    $pubDate = 1;
                } else {
                    $pubDate = strtotime($params['hotel']['package-offer']['publish_date']);
                }
                if ($params['hotel']['package-offer']['is_unpublished_alap'] == '1') {
                    $unPubDate = $bookTo;
                } else {
                    $unPubDate = strtotime($params['hotel']['package-offer']['unpublish_date']);
                }

                $campaignStatus = $this->campaignStatus->getHotelCampaignStatus($product, $pubDate, $unPubDate, $bookFrom, $bookTo);
                $product->setCampaignStatus($campaignStatus);
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Save Hotel Error"));
            $this->_logger->debug($e->getMessage());
        }
        return;
    }

}
