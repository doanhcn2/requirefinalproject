<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Observer\Backend\Product;

use Magenest\Hotel\Model\HotelPropertyFactory;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AfterLoadProduct
 * @package Magenest\DynamicImages\Observer
 */
class AfterLoadProduct implements ObserverInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    protected $packageDetailsFactory;

    /**
     * AfterLoadProduct constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        HotelPropertyFactory $collectionFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->packageDetailsFactory = $packageDetailsFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product->getId()) {
            $offer = $this->packageDetailsFactory->create()->getCollection()
                ->addFieldToFilter("product_id", $product->getId())->getFirstItem();

            $collections = $this->collectionFactory->create()
                ->getCollection()->addFieldToFilter('vendor_id', $offer->getVendorId())
                ->getFirstItem();
            $images = json_decode($collections->getPhotos());
            $dataImages = [];
            if (is_array($images)) {
                foreach ($images as $image) {
                    $dataImages[] = [
                        "file" => $image
                    ];
                }
                $product->setImages(
                    [
                        'images' => $dataImages
                    ]
                );
            }
        }
        return $this;
    }
}
