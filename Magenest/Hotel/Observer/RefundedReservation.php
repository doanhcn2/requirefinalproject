<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Hotel\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magenest\Hotel\Helper\BookingStatus as Status;

/**
 * Class RefundedTicket
 * @package Magenest\Ticket\Observer
 */
class RefundedReservation implements ObserverInterface
{
    protected $_hotelOrderFactory;

    public function __construct(
        \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory
    )
    {
        $this->_hotelOrderFactory = $hotelOrderFactory;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var OrderItem $orderItem */
        $orderId = $observer->getCreditmemo()->getOrderId();
        $model = $this->_hotelOrderFactory->create()->getCollection()->addFieldToFilter("order_id",$orderId);
        if($model->count()>0){
            $model->getFirstItem()->setRefundedAt(date("Y-m-d"))->setStatus(Status::REFUNDED)->save();

        }

        return;
    }
}
