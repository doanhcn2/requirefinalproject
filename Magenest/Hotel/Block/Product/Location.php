<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/27/18
 * Time: 1:30 PM
 */

namespace Magenest\Hotel\Block\Product;

use Magento\Framework\View\Element\Template;

class Location extends Template
{
    protected $_hotelCollectionFactory;
    protected $registry;
    protected $helper;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Helper\Filter $helper,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory $hotelCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_hotelCollectionFactory = $hotelCollectionFactory;
        $this->registry = $registry;
        $this->helper = $helper;
    }

    public function isShowFilterInput()
    {
        $category = $this->registry->registry('current_category');
        return $this->helper->isHotelCategory($category);
    }

    public function getLocations()
    {
        return $this->_hotelCollectionFactory->create()->getAllLocations();
    }
}