<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 1/29/18
 * Time: 5:34 PM
 */

namespace Magenest\Hotel\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\UrlInterface;

/**
 * Class Room
 * @package Magenest\Hotel\Block\Product
 */
class Room extends Template
{
    /**
     * @var \Magenest\Hotel\Helper\Helper
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magenest\Register\Helper\DataHelper
     */
    protected $_registerHelper;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory
     */
    protected $serviceCollectionFactory;

    /**
     * @var array
     */
    protected $servies;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\CollectionFactory
     */
    protected $packageServiceCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory
     */
    protected $packageCollectionFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Item
     */
    protected $_quoteItem;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \Magenest\Hotel\Model\PackageDetails
     */
    protected $_currentPackage;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendor
     */
    protected $_roomTypeVendor;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Hotel\Model\HotelProperty
     */
    protected $_propertyDetails;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory
     */
    protected $_hotelFacilCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection
     */
    protected $hotelFacils;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\CollectionFactory
     */
    protected $_roomAmenCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection
     */
    protected $roomAmens;

    /**
     * Room constructor.
     * @param Template\Context $context
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Hotel\Helper\Helper $helper
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @param \Magenest\Register\Helper\DataHelper $registerHelper
     * @param \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory
     * @param \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\CollectionFactory $roomAmenFactory
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageCollectionFactory
     * @param \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $hotelFacilCollectionFactory
     * @param \Magenest\Hotel\Model\ResourceModel\PackageAndService\CollectionFactory $packageServiceCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Helper\Helper $helper,
        \Magento\Quote\Model\Quote\Item $quoteItem,
        \Magenest\Register\Helper\DataHelper $registerHelper,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\CollectionFactory $roomAmenFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $hotelFacilCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageAndService\CollectionFactory $packageServiceCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_cart = $cart;
        $this->helper = $helper;
        $this->_quoteItem = $quoteItem;
        $this->coreRegistry = $registry;
        $this->_registerHelper = $registerHelper;
        $this->_request = $context->getRequest();
        $this->_roomAmenCollectionFactory = $roomAmenFactory;
        $this->packageCollectionFactory = $packageCollectionFactory;
        $this->serviceCollectionFactory = $serviceCollectionFactory;
        $this->_hotelFacilCollectionFactory = $hotelFacilCollectionFactory;
        $this->packageServiceCollectionFactory = $packageServiceCollectionFactory;
    }

    /**
     * @return string
     */
    public function getGoogleMapApiKey()
    {
        return $this->_registerHelper->getGoogleMapApiKey();
    }

    /**
     * @return bool
     */
    public function isRoomTypeProduct()
    {
        return $this->getCurrentProduct()->getTypeId() === 'hotel';
    }

    /**
     * @return int
     */
    public function getCurrentProductId()
    {
        return $this->getCurrentProduct()->getId();
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getCurrentProduct()
    {
        return $this->coreRegistry->registry('current_product');
    }

    /**
     * @return \Magenest\Hotel\Model\HotelProperty
     */
    public function getPropertyDetails()
    {
        if (!$this->_propertyDetails) {
            $package = $this->getCurrentPackage();
            $this->_propertyDetails = $package->getPropertyDetails();
        }
        return $this->_propertyDetails;
    }

    /**
     * @return \Magenest\Hotel\Model\PackageDetails
     */
    public function getCurrentPackage()
    {
        if (!$this->_currentPackage) {
            $product = $this->getCurrentProduct();
            /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageColelction */
            $packageColelction = $this->packageCollectionFactory->create();
            $this->_currentPackage = $packageColelction->getItemByProductId($product->getId());
        }
        return $this->_currentPackage;
    }

    /**
     * @return \Magenest\Hotel\Model\RoomTypeVendor
     */
    public function getRoomTypeVendor()
    {
        if (!$this->_roomTypeVendor) {
            $package = $this->getCurrentPackage();
            $this->_roomTypeVendor = $package->getRoomTypeVendor();
        }
        return $this->_roomTypeVendor;
    }

    /**
     * @todo: should return collection instead of array
     * @return array
     */
    public function getServices()
    {
        if ($this->servies === null) {
            $product = $this->coreRegistry->registry('current_product');
            $productId = $product->getId();

            $packageColelction = $this->packageCollectionFactory->create();
            $packageServiceCollection = $this->packageServiceCollectionFactory->create();
            $serviceCollectionTable = $this->serviceCollectionFactory->create()->getMainTable();
            $packageId = $packageColelction->getItemByProductId($productId)->getId();

            $this->servies = $packageServiceCollection
                ->addFieldToFilter('package_id', $packageId)
                ->join(
                    $serviceCollectionTable,
                    "main_table.service_id = $serviceCollectionTable.id",
                    ['title', 'description', 'price']
                )
                ->getData();
        }
        return $this->servies;
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection
     */
    public function getHotelFacil()
    {
        if (!$this->hotelFacils) {
            /** @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection $hotelFacilCollection */
            $hotelFacilCollection = $this->_hotelFacilCollectionFactory->create();
            $vendorId = $this->getCurrentProduct()->getUdropshipVendor();
            $this->hotelFacils = $hotelFacilCollection->addVendorToFilter($vendorId)->addFacilDetails();
        }
        return $this->hotelFacils;
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection
     */
    public function getRoomAmenities()
    {
        if (!$this->roomAmens) {
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection $roomAmenCollection */
            $roomAmenCollection = $this->_roomAmenCollectionFactory->create();
            $roomTypeId = $this->getCurrentPackage()->getRoomType();
            $this->roomAmens = $roomAmenCollection->addRoomTypeToFilter($roomTypeId)->addAmenityDetails();
        }
        return $this->roomAmens;
    }

    /**
     * @return string
     */
    public function getPrefixHotelImgUrl()
    {
        $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . \Magenest\Hotel\Controller\Property\Upload::HOTEL_IMG_PATH;
        return $url;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->helper->getCurrencySymbol();
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->getCurrentPackage()->getBookFrom();
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->getCurrentPackage()->getBookTo();
    }

    /**
     * @return int|string
     */
    public function getMinTimeToBook()
    {
        return $this->getCurrentPackage()->getMinTimeToBook();
    }

    /**
     * @return array
     */
    public function checkEditCart()
    {
        $moduleName = $this->_request->getModuleName();
        $actionName = $this->_request->getActionName();
        $productId = $this->_request->getParam("product_id");

        $data = [];
        if ($moduleName == "checkout" && $actionName == "configure") {
            $items = $this->_cart->getQuote()->getItems();
            foreach ($items as $item) {
                /** @var \Magento\Quote\Model\Quote\Item $item */

                if ($item->getProductId() == $productId) {
                    $services = $item->getOptionByCode("services");
                    if ($services)
                        $services = $services->getData()['value'];
                    else
                        $services = [];
                    $checkInDate = $item->getOptionByCode("check_in_date")->getData();
                    $checkOutDate = $item->getOptionByCode("check_out_date")->getData();
                    $qty = $item->getOptionByCode("room_qty")->getData();
                    $data = [
                        "itemCartId" => $item->getId(),
                        'service' => $services ? json_decode($services, true) : [],
                        "check_in_date" => $checkInDate['value'],
                        "check_out_date" => $checkOutDate['value'],
                        'qty' => $qty['value']
                    ];
                }
            }
        }
        return $data;
    }
}
