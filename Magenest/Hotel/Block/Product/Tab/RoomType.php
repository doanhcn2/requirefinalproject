<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/3/18
 * Time: 8:58 AM
 */

namespace Magenest\Hotel\Block\Product\Tab;


class RoomType extends AbstractTab
{
    protected $_roomTypeVendor;
    protected $_roomTypeAdmin;
    protected $_roomTypeAdminResourceFactory;
    protected $_roomTypeAdminFactory;
    protected $_roomTypeVendorResourceFactory;
    protected $_roomTypeVendorFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendorFactory $roomTypeVendorResourceFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeAdminFactory $roomTypeAdminResourceFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $packageDetailsCollectionFactory, $data);
        $this->_roomTypeVendorResourceFactory = $roomTypeVendorResourceFactory;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_roomTypeAdminResourceFactory = $roomTypeAdminResourceFactory;
        $this->_roomTypeAdminFactory = $roomTypeAdminFactory;
    }

    /**
     * @return string
     */
    public function getRoomTypeVendorId()
    {
        return $this->getPackageDetails()->getRoomType();
    }

    /**
     * @return \Magenest\Hotel\Model\RoomTypeVendor
     */
    public function getRoomTypeVendorModel()
    {
        if (!$this->_roomTypeVendor) {
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor $roomTypeVendorResource */
            $roomTypeVendorResource = $this->_roomTypeVendorResourceFactory->create();
            $this->_roomTypeVendor = $this->_roomTypeVendorFactory->create();
            $roomTypeVendorResource->load($this->_roomTypeVendor, $this->getRoomTypeVendorId());
        }
        return $this->_roomTypeVendor;
    }

    /**
     * @return string
     */
    public function getRoomTypeAdminId()
    {
        return $this->getRoomTypeVendorModel()->getRoomTypeAdminId();
    }

    /**
     * @return \Magenest\Hotel\Model\RoomTypeAdmin
     */
    public function getRoomTypeAdminModel()
    {
        if (!$this->_roomTypeAdmin) {
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeAdmin $roomTypeAdminResource */
            $roomTypeAdminResource = $this->_roomTypeAdminResourceFactory->create();
            $this->_roomTypeAdmin = $this->_roomTypeAdminFactory->create();
            $roomTypeAdminResource->load($this->_roomTypeAdmin, $this->getRoomTypeAdminId());
        }
        return $this->_roomTypeAdmin;
    }

    public function getRoomTypeLabel()
    {
        return $this->getRoomTypeAdminModel()->getData('room_type');
    }
}
