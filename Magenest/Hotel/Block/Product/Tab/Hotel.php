<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/3/18
 * Time: 8:58 AM
 */

namespace Magenest\Hotel\Block\Product\Tab;


use Magento\Framework\UrlInterface;

class Hotel extends AbstractTab
{
    protected $_hotelPropertyFactory;

    /**
     * @var \Magenest\Hotel\Model\HotelProperty
     */
    protected $_hotelProperty;

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $packageDetailsCollectionFactory, $data);
        $this->_hotelPropertyFactory = $hotelPropertyFactory;
    }

    /**
     * @return \Magenest\Hotel\Model\HotelProperty
     */
    public function getHotelProperty()
    {
        if (!$this->_hotelProperty) {
            $this->_hotelProperty = $this->_hotelPropertyFactory->create()->loadByVendorId($this->getVendorId());
        }
        return $this->_hotelProperty;
    }

    public function getPrefixHotelImgUrl()
    {
        $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . \Magenest\Hotel\Controller\Property\Upload::HOTEL_IMG_PATH;
        return $url;
    }
}
