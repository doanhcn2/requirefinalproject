<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/3/18
 * Time: 8:58 AM
 */

namespace Magenest\Hotel\Block\Product\Tab;


class RoomAmenities extends AbstractTab
{
    protected $_roomTypeVendor;
    protected $_roomTypeVendorResourceFactory;
    protected $_roomTypeVendorFactory;
    protected $_amenitiesAdminFactory;
    protected $_amenitiesVendorFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendorFactory $roomTypeVendorResourceFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenitiesAdminFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenitiesVendorFactory,

        array $data = []
    ) {
        parent::__construct($context, $registry, $packageDetailsCollectionFactory, $data);
        $this->_roomTypeVendorResourceFactory = $roomTypeVendorResourceFactory;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_amenitiesAdminFactory=$amenitiesAdminFactory;
        $this->_amenitiesVendorFactory=$amenitiesVendorFactory;

    }


    /**
     * @return \Magenest\Hotel\Model\RoomTypeVendor
     */
    public function getRoomTypeVendorModel()
    {
        if (!$this->_roomTypeVendor) {
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor $roomTypeVendorResource */
            $roomTypeVendorResource = $this->_roomTypeVendorResourceFactory->create();
            $this->_roomTypeVendor = $this->_roomTypeVendorFactory->create();
            $roomTypeVendorResource->load($this->_roomTypeVendor, $this->getRoomTypeVendorId());
        }
        return $this->_roomTypeVendor;
    }

    /**
     * @return string
     */
    public function getRoomTypeVendorId()
    {
        return $this->getPackageDetails()->getRoomType();
    }

    public function getInnerJoinCollection(){
        $roomTypeId = $this->getRoomTypeVendorModel()->getId();
        $collection = $this->_amenitiesAdminFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            'magenest_hotel_room_amenities_vendor',
            "main_table.id = magenest_hotel_room_amenities_vendor.amenity
             AND magenest_hotel_room_amenities_vendor.room_vendor_id = {$roomTypeId}" ,
            ""
        );
        return $collection->addFieldToSelect(['room_kind','name','is_enable']);
    }

    public function getAmenitiesData(){
        $data = [];
        $collection = $this->getInnerJoinCollection()->getData();
        foreach ($collection as $item) {
            if($item['is_enable']=="1")
                $data[$item['room_kind']][] = $item['name'];
        }
        return $data;
    }
}
