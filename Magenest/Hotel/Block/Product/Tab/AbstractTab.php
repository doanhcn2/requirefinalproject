<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/3/18
 * Time: 10:05 AM
 */

namespace Magenest\Hotel\Block\Product\Tab;

/**
 * Class AbstractTab
 * @package Magenest\Hotel\Block\Product\Tab
 */
abstract class AbstractTab extends \Magento\Catalog\Block\Product\View\Description
{
    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory
     */
    protected $_packageDetailsCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\PackageDetails
     */
    protected $_package;

    /**
     * AbstractTab constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
    }

    /**
     * @return \Magenest\Hotel\Model\PackageDetails
     */
    public function getPackageDetails()
    {
        if (!$this->_package) {
            $this->_package = $this->_packageDetailsCollectionFactory->create()->getItemByProductId($this->getProduct()->getId());
        }
        return $this->_package;
    }

    /**
     * @return string
     */
    public function getVendorId()
    {
        return $this->getPackageDetails()->getVendorId();
    }

}
