<?php

namespace Magenest\Hotel\Block\Adminhtml\Amenity;
/**
 * Class Edit
 * @package Magenest\Hotel\Block\Adminhtml\Amenity
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'amenity_id';
        $this->_blockGroup = 'Magenest_Hotel';
        $this->_controller = 'adminhtml_amenity';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save'));
        $this->buttonList->remove('delete');

        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
                ]
            ],
            -100
        );

    }

    /**
     * Get edit form container header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('amenity_template')->getAmenityId()) {
            return __("Amenity ". $this->_coreRegistry->registry('amenity_template')->getTitle());
        } else {
            return __('New Amenity');
        }
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {

        return $this->getUrl('*/*/');
    }

}
