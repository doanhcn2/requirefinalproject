<?php

namespace Magenest\Hotel\Block\Adminhtml\Amenity\Edit;
/**
 * Class Form
 * @package Magenest\Hotel\Block\Adminhtml\Amenity\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('amenity_form');
        $this->setTitle(__('Amenity Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
//        $model = $this->_coreRegistry->registry('ticket_template');

//        /** @var \Magento\Framework\Data\Form $form */
//        $form = $this->_formFactory->create(
//            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
//        );
//
//        $fieldset = $form->addFieldset('template_fieldset',array(
//            'legend' => __('Amenity')
//        ));
//        $fieldset->addField('title','text',array(
//            'label' 	=> __('Title'),
//            'class' 	=> 'required-entry',
//            'required' 	=> true,
//            'style' 	=> 'width:700px;',
//            'name' 		=> 'title'
//        ));
//
//
//        $form->setValues($model->getData());
//
//        $form->addField('template_id', 'hidden', array
//        (
//            'name' => 'template_id',
//            'value' => $model->getAmenityId(),
//        ));
//
//        $form->setUseContainer(true);
//
//        $this->setForm($form);

        return parent::_prepareForm();
    }
}
