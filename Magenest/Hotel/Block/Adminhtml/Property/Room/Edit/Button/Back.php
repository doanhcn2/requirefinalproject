<?php
namespace Magenest\Hotel\Block\Adminhtml\Property\Room\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class Back
 * @package Magenest\Hotel\Block\Adminhtml\Room\Edit\Button
 */
class Back extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {

        return $this->getUrl('*/*/',['vendor_id'=>$this->_request->getParam("vendor_id")]);
    }
}
