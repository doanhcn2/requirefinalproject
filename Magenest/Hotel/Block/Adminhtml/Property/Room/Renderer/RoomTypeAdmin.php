<?php

namespace Magenest\Hotel\Block\Adminhtml\Property\Room\Renderer;

/**
 * Class RoomTypeAdmin
 * @package Magenest\Hotel\Block\Adminhtml\Property\Room\Renderer
 */
class RoomTypeAdmin extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $_roomTypeAdminFactory;

    /**
     * RoomTypeAdmin constructor.
     * @param \Magento\Backend\Block\Context $context
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
     * @param string $name
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        array $data = []
    ) {

        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_roomTypeAdminFactory = $roomTypeAdminFactory;
        parent::__construct($context, $data);
    }

    /**
     * Render the grid cell value
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $roomTypeAdmin = $this->_roomTypeAdminFactory->create()->load($row->getRoomTypeAdminId());
        return $roomTypeAdmin->getRoomType();
    }


}
