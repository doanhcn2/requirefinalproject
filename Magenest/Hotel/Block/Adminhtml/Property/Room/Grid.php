<?php

namespace Magenest\Hotel\Block\Adminhtml\Property\Room;

use Magento\Backend\Block\Widget\Grid as WidgetGrid;

/**
 * Class Grid
 * @package Magenest\Hotel\Block\Adminhtml\Property\Room
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendorFactory;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        array $data = []
    )
    {
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        parent::__construct($context, $backendHelper, $data);
        $this->setEmptyText(__('No Results Found'));
    }


    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', [
            'header' => __('ID'),
            'align' => 'right',
            'width' => '20px',
            'index' => 'id',
        ]);
        $this->addColumn('room_type_admin_id', [
            'header' => __('Room Type Admin'),
            'align' => 'right',
            'renderer' => 'Magenest\Hotel\Block\Adminhtml\Property\Room\Renderer\RoomTypeAdmin',
            'width' => '50px',
            'index' => 'room_type_admin_id',
        ]);


        $this->addColumn('room_title', [
            'header' => __('Room Title'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'room_title',
        ]);

        return parent::_prepareColumns();
    }

    /**
     * Initialize the Result collection
     *
     * @return WidgetGrid
     */
    protected function _prepareCollection()
    {

        $this->setCollection(
            $this->_roomTypeVendorFactory->create()
                ->getCollection()->addFieldToFilter("vendor_id", $this->getRequest()->getParam('vendor_id')
                )
        );

        return parent::_prepareCollection();
    }

//    /**
//     * @return $this
//     */
//    protected function _prepareMassaction()
//    {
//        $this->setMassactionIdField('id');
//        $this->getMassactionBlock()->setFormFieldName('rooms_id');
//        $this->getMassactionBlock()->addItem(
//            'delete',
//            [
//                'label' => __('Delete'),
//                'url' => $this->getUrl('hotel/roomVendor/massDelete',
//                    ['_current' => true,'vendor_id'=>$this->getRequest()->getParam('vendor_id')]),
//                'confirm' => __('Are you sure?')
//            ]
//        );
//
//        return $this;
//    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {

        $vendorId = $this->getRequest()->getParam('vendor_id');
        return $this->getUrl(
            '*/*/edit',
            ['store' => $this->getRequest()->getParam('store'), 'vendor_id' => $vendorId, 'room_id' => $row->getId()]
        );
    }
}
