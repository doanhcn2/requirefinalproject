<?php
namespace Magenest\Hotel\Block\Adminhtml\Property;
/**
 * Class Room
 * @package Magenest\Hotel\Block\Adminhtml\Property
 */
class Room extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Hotel';
        $this->_controller = 'adminhtml_roomVendor';

        $this->addButton(
            'back',
            [
                'label' => __('Back'),
                'onclick' => 'setLocation(\'' . $this->getBackUrl() . '\')',
                'class' => 'back'
            ],
            -1
        );

        parent::_construct();
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {

        return $this->getUrl('hotel/property/index/');
    }

}
