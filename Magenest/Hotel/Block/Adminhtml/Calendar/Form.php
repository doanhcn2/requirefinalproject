<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Block\Adminhtml\Calendar;
use Magento\Framework\App\ObjectManager;

/**
 * Class Form
 * @package Magenest\Hotel\Block\Adminhtml\Calendar
 */
class Form extends \Magento\Catalog\Block\Adminhtml\Product\Edit\Js
{
    protected $_template = 'calendar/tool.phtml';

    public function getPricesUrl()
    {
        return $this->getUrl('hotel/price/load', []);
    }

    public function getProductId()
    {
        $productId = $this->getProduct()->getId();
        if ($productId) {
            return $productId;
        }
        return '0';
    }

    public function getCurrency()
    {
        $helper = ObjectManager::getInstance()->get('Magenest\Hotel\Helper\Helper');
        return $helper->getCurrencySymbol();
    }

}
