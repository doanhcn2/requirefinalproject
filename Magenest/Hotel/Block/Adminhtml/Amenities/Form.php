<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Block\Adminhtml\Amenities;

use Magento\Framework\View\Element\Template;

/**
 * Class Form
 * @package Magenest\Hotel\Block\Adminhtml\Amenities
 */
class Form extends Template
{
    protected $_template = 'amenities/form.phtml';
}
