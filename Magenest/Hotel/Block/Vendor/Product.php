<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Block\Vendor;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\Bundle\LanguageBundle;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class Product
 * @package Magenest\Ticket\Block\Vendor
 */
class Product extends Template
{
    protected $_template = 'vendor/product.phtml';

    protected $_vendorFactory;

    protected $_hotelPropertyCollectionFactory;

    protected $_roomTypeAdminFactory;

    protected $_roomTypeVendorFactory;

    protected $_bedTypeFactory;

    protected $_serviceCollectionFactory;

    protected $_packageDetailsCollectionFactory;

    protected $_packagePriceCollectionFactory;

    protected $_amenitiesAdminFactory;

    protected $_amenitiesVendorFactory;

    protected $_form;

    protected $_registry;

    protected $helper;

    protected $_facilAdminCollectionFactory;

    protected $_facilVendorCollectionFactory;

    protected $_dataHelper;

    protected $vendorSession;

    protected $hotelProperty;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Helper\Helper $helper,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Register\Helper\DataHelper $dataHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenitiesAdminFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenitiesVendorFactory,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin\CollectionFactory $facilAdminCollectionF,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $facilVendorCollectionF,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory $hotelPropertyCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->_registry = $registry;
        $this->_dataHelper = $dataHelper;
        $this->vendorSession = $vendorSession;
        $this->_vendorFactory = $vendorFactory;
        $this->_bedTypeFactory = $bedTypeFactory;
        $this->_roomTypeAdminFactory = $roomTypeAdminFactory;
        $this->_roomTypeVendorFactory = $roomTypeVendorFactory;
        $this->_amenitiesAdminFactory = $amenitiesAdminFactory;
        $this->_amenitiesVendorFactory = $amenitiesVendorFactory;
        $this->_serviceCollectionFactory = $serviceCollectionFactory;
        $this->_facilAdminCollectionFactory = $facilAdminCollectionF;
        $this->_facilVendorCollectionFactory = $facilVendorCollectionF;
        $this->_packagePriceCollectionFactory = $packagePriceCollectionFactory;
        $this->_hotelPropertyCollectionFactory = $hotelPropertyCollectionFactory;
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
    }

    public function getGoogleMapApiKey()
    {
        return $this->_dataHelper->getGoogleMapApiKey();
    }

    public function getWebsites()
    {
        $return = [];
        $websites = $this->_storeManager->getWebsites();
        foreach ($websites as $website) {
            $return[] = ['id' => $website->getId(), 'name' => $website->getName()];
        }
        return $return;
    }

    public function getCurrentProduct()
    {
        /** @var \Unirgy\DropshipVendorProduct\Model\Product $product */
        $product = $this->_registry->registry('current_product');
        return $product;
    }

    public function getProductId()
    {
        $product = $this->getCurrentProduct();
        if ($product->getId())
            return $product->getId();
        return "new";
    }

    public function getPackageData()
    {
        $product = $this->getCurrentProduct();
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
        $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
        $packageDetails = $packageDetailsCollection->getItemByProductId($product->getId());
        $data = $packageDetails->getData();
        if ($data && is_array($data)) {
            $data['offerTitle'] = $product->getName();
            if ($this->getVendorId()) {
                $data['vendorSku'] = $product->getMultiVendorData()[$this->getVendorId()]['vendor_sku'];
            }
            $data['currentPackage'] = $this->getPricesArray($packageDetails->getId());
        }
        $roomTypeVendor = $this->getRoomTypeVendor();
        foreach ($roomTypeVendor as $room) {
            $data["optionsRoomType"][] = [
                "id" => $room['id'],
                "title" => $room['roomTitle']
            ];
        }
        $data['currency'] = $this->helper->getCurrencySymbol();
        return $data;
    }

    protected function getPricesArray($packageId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $packagePriceCollection */
        $packagePriceCollection = $this->_packagePriceCollectionFactory->create();
        $prices = $packagePriceCollection->getAllPriceByPackage($packageId);
        $data = [];
        foreach ($prices as $price) {
            $data[$price->getDate()] = [
                'salePrice' => $price->getSalePrice(),
                'origPrice' => $price->getOrigPrice(),
                'roomQty' => $price->getQty()
            ];
        }
        return $data;
    }

    public function getIsVendorFinishOnboarding()
    {
        $vendorId = $this->getVendorId();
        if ($vendorId) {
            /** @var \Unirgy\Dropship\Model\Vendor $vendor */
            $vendor = $this->_vendorFactory->create()->load($vendorId);
            return !!$vendor->getData('finish_onboarding');
        }
        return false;
    }

    /**
     * @return bool|string|int
     */
    protected function getVendorId()
    {
        if (!$this->vendorSession->isLoggedIn()) {
            return false;
        }
        return $this->vendorSession->getVendorId();
    }

    /**
     * @return \Magento\Framework\Data\Form
     */
    public function getForm()
    {
        if ($this->_form === null) {
            $formFactory = ObjectManager::getInstance()->get('\Magento\Framework\Data\FormFactory');
            /** @var \Magento\Framework\Data\Form $form */
            $form = $formFactory->create();
            $form->addElement(ObjectManager::getInstance()->create('Unirgy\Dropship\Block\CategoriesField'));
            $this->_form = $form;
        }
        return $this->_form;
    }

    public function getConfigData()
    {
        return [];
    }

    public function getHotelProperties()
    {
        if (!$this->hotelProperty) {
            if (!$this->vendorSession->isLoggedIn()) {
                return [];
            }
            $vendorId = $this->vendorSession->getVendorId();

            /** @var \Magenest\Hotel\Model\ResourceModel\HotelProperty\Collection $hotelPropertyCollection */
            $hotelPropertyCollection = $this->_hotelPropertyCollectionFactory->create();
            $properties = $hotelPropertyCollection->getHotelPropertyByVendorId($vendorId)->getData();
//        $this->getLanguages();
//        ObjectManager::getInstance()->get('Magento\Framework\Setup\Lists')->getLocaleList();
            $this->hotelProperty = $properties;
        }
        return $this->hotelProperty;
    }

    public function getRoomTypeAdmin()
    {
        $roomTypes = $this->_roomTypeAdminFactory->create()->getCollection();
        $types = [];
        foreach ($roomTypes as $roomType) {
            $types [] = [
                "text" => $roomType->getRoomType(),
                "value" => $roomType->getId()
            ];
        }
        if ($types === []) {
            throw new LocalizedException(__("No room type created"));
        }
        return $types;
    }

    public function getRoomTypeVendor()
    {
        $roomTypes = $this->_roomTypeVendorFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
        $types = [];
        foreach ($roomTypes as $roomType) {
            $types [] = [
                "id" => $roomType->getId(),
                "roomType" => $roomType->getRoomTypeAdminId(),
                "roomTitle" => $roomType->getRoomTitle(),
                "smoking" => $roomType->getNonSmoking(),
                "numberRooms" => $roomType->getNumberRoomsAvailable(),
                "description" => $roomType->getDescription(),
                "beds" => $roomType->getBedsType(),
                "numberGuests" => $roomType->getNumberGuests(),
                "roomSize" => $roomType->getRoomSize(),
                "hasExtraBeds" => $roomType->getHasExtraBeds(),
                "numberExtraBeds" => $roomType->getNumberExtraBeds(),
                "priceBed" => $roomType->getPricePerBed(),
                "photos" => $roomType->getPhotoRoom()
            ];
        }

        return $types;
    }

    public function getBedTypeAdmin()
    {
        $bedTypes = $this->_bedTypeFactory->create()->getCollection();
        $types = [];
        foreach ($bedTypes as $bedType) {
            $types [] = [
                "text" => $bedType->getBedType(),
                "value" => $bedType->getId()
            ];
        }
        if ($types === []) {
            throw new LocalizedException(__("No bed type created"));
        }
        return $types;
    }

    public function getAmenitiesAdmin()
    {
        $amenities = $this->_amenitiesAdminFactory->create()->getCollection();
        $types = [];
        $checkedAmenities = $this->getAmenitiesVendor();
        foreach ($amenities as $amenity) {
            if ($amenity->getIsEnable() == 1) {
                if (array_key_exists($amenity->getId(), $checkedAmenities)) {
                    $types[$amenity->getRoomKind()][$amenity->getId()]["checked"] = $checkedAmenities[$amenity->getId()];
                }
                $types[$amenity->getRoomKind()][$amenity->getId()]["name"] = $amenity->getName();

            }
        }
        if ($types === []) {
            throw new LocalizedException(__("No amenities was enabled. Please contact admin to enable."));
        }
        return $types;
    }

    public function getAmenitiesVendor()
    {
        $amenities = $this->_amenitiesVendorFactory->create()->getCollection();
        $types = [];
        $vendorId = $this->getVendorId();
        if ($vendorId) {
            $amenities = $amenities->addFieldToFilter('vendor_id', $vendorId);
            foreach ($amenities as $amenity) {
                $types[$amenity->getAmenity()][] = $amenity->getRoomVendorId();
            }
            return $types;
        }

        return null;
    }

    public function checkStepOne()
    {
        $controllerName = $this->_request->getControllerName();
        if ($controllerName == "detail")
            return __("Save");
        return __("Continue");
    }

    public function getHotelFacilities()
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin\Collection $facilAdmin */
        $facilAdmin = $this->_facilAdminCollectionFactory->create();
        $facilAdmin->addVendorId($this->getVendorId());
        $facilAdmin->load();
        $return = [];
        foreach ($facilAdmin as $item) {
            $facilKind = $item->getFacilityKind();
            if (!$facilKind) {
                continue;
            }
            if (!isset($return[$facilKind])) {
                $return[$facilKind] = [];
            }
//            $item->unsetData('facility_kind');
            $return[$facilKind][] = $item->getData();
        }
        return $return;
    }

    public function getPrefixHotelImgUrl()
    {
        $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . \Magenest\Hotel\Controller\Property\Upload::HOTEL_IMG_PATH;
        return $url;
    }

    public function getPrefixRoomImgUrl()
    {
        $url = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . \Magenest\Hotel\Controller\RoomType\UploadImage::ROOM_IMG_PATH;
        return $url;
    }

    public function getLanguages()
    {
        $return = [];
        $languages = (new LanguageBundle())->get(Resolver::DEFAULT_LOCALE)->get('Languages');
        $locales = \ResourceBundle::getLocales('') ?: [];
        foreach ($locales as $locale) {
            $language = \Locale::getPrimaryLanguage($locale);
            if (!$languages[$language]) {
                continue;
            }
            array_push($return, $languages[$language]);
        }
        $return = array_values(array_unique($return));
        sort($return);
        return $return;
    }

    public function getServices()
    {
        if (!$this->vendorSession->isLoggedIn()) {
            return [];
        }
        $vendorId = $this->vendorSession->getVendorId();

        /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $servicesCollection */
        $servicesCollection = $this->_serviceCollectionFactory->create();
        $product = $this->getCurrentProduct();

        if (!$product->getId()) {
            $services = $servicesCollection->addVendorToFilter($vendorId)->getData();
        } else {
            /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
            $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
            $packageId = $packageDetailsCollection->getItemByProductId($product->getId())->getId();
            $services = $servicesCollection->getServicesWithPackageId($vendorId, $packageId);
        }

        return $services;
    }
}
