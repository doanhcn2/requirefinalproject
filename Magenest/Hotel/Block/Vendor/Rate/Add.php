<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/11/18
 * Time: 9:53 AM
 */

namespace Magenest\Hotel\Block\Vendor\Rate;

use Magento\Framework\View\Element\Template;

class Add extends \Magento\Framework\View\Element\Template
{
    protected $_packageDetailsCollectionFactory;

    protected $_roomTypeVendorCollectionFactory;

    protected $_serviceCollectionFactory;

    protected $vendorSession;

    protected $services;

    protected $roomTypes;

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory $roomTypeVendorCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Unirgy\Dropship\Model\Session $vendorSession,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->vendorSession = $vendorSession;
        $this->_serviceCollectionFactory = $serviceCollectionFactory;
        $this->_roomTypeVendorCollectionFactory = $roomTypeVendorCollectionFactory;
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
    }

    protected function getCurrentProduct()
    {
        return null;
    }

    protected function getVendorId()
    {
        if (!$this->vendorSession->isLoggedIn()) {
            return false;
        }
        return $this->vendorSession->getVendorId();
    }

    public function getProductId()
    {
        $product = $this->getCurrentProduct();
        if ($product !== null && $product->getId())
            return $product->getId();
        return "new";
    }

    public function getRoomTypeVendor()
    {
        if ($this->roomTypes === null) {
            if ($this->getVendorId() === false) {
                return [];
            }
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection $collection */
            $collection = $this->_roomTypeVendorCollectionFactory->create();
            $collection->addFieldToSelect(['id', 'room_title'])
                ->addVendorToFilter($this->getVendorId());
            $data = $collection->getData();
            foreach ($data as $i => $d) {
                $data[$i]['is_enabled'] = false;
            }
            $this->roomTypes = $data;
        }
        return $this->roomTypes;
    }

    public function getServices()
    {
        if (!$this->services) {
            if (!$this->vendorSession->isLoggedIn()) {
                return [];
            }
            $vendorId = $this->vendorSession->getVendorId();

            /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $servicesCollection */
            $servicesCollection = $this->_serviceCollectionFactory->create();
            $product = $this->getCurrentProduct();

            if ($product === null || !$product->getId()) {
                $services = $servicesCollection
                    ->addFieldToSelect(['id', 'title', 'price'])
                    ->addVendorToFilter($vendorId)->getData();
                foreach ($services as $i => $s) {
                    $services[$i]['is_enabled'] = false;
                }
            } else {
                /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
                $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
                $packageId = $packageDetailsCollection->getItemByProductId($product->getId())->getId();
                $services = $servicesCollection->getServicesWithPackageId($vendorId, $packageId);

            }
            $this->services = $services;
        }
        return $this->services;
    }
}
