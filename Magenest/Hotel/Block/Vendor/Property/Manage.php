<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/3/18
 * Time: 6:02 PM
 */

namespace Magenest\Hotel\Block\Vendor\Property;

use Magento\Framework\View\Element\Template;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magenest\Profile\Model\ImageUploadFactory;
use \Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Manage
 * @package Magenest\Hotel\Block\Vendor\Property
 */
class Manage extends \Magenest\Hotel\Block\Vendor\Product
{
    /**
     * @var \Magenest\Hotel\Model\ResourceModel\Gallery
     */
    protected $_galleryResource;

    /**
     * @var array
     */
    protected $basicInformation;

    /**
     * @var array
     */
    protected $services;

    /**
     * @var array
     */
    protected $propertyDetails;

    /**
     * @var string
     */
    protected $gallery;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Helper\Helper $helper,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Register\Helper\DataHelper $dataHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory,
        \Magenest\Hotel\Model\ResourceModel\Gallery $galleryResource,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $amenitiesAdminFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $amenitiesVendorFactory,
        \Magenest\Hotel\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin\CollectionFactory $facilAdminCollectionF,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $facilVendorCollectionF,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $packagePriceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty\CollectionFactory $hotelPropertyCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $helper,
            $vendorSession,
            $dataHelper,
            $vendorFactory,
            $bedTypeFactory,
            $roomTypeAdminFactory,
            $roomTypeVendorFactory,
            $amenitiesAdminFactory,
            $amenitiesVendorFactory,
            $serviceCollectionFactory,
            $facilAdminCollectionF,
            $facilVendorCollectionF,
            $packagePriceCollectionFactory,
            $hotelPropertyCollectionFactory,
            $packageDetailsCollectionFactory,
            $data
        );
        $this->_galleryResource = $galleryResource;
    }

    /**
     * @return array|string
     */
    public function getServices()
    {
        if (!$this->services) {
            /** @var \Magenest\Hotel\Model\ResourceModel\Service\Collection $collection */
            $collection = $this->_serviceCollectionFactory->create();
            $collection->addVendorToFilter($this->getVendorId());
            $this->services = $collection->getData();
        }
        return $this->services;
    }

    /**
     * @return string
     */
    public function getPrefixGalleryImgUrl()
    {
        return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) .
            \Magenest\Hotel\Controller\Vendor\Gallery\Upload::IMG_PATH;
    }

    /**
     * @return string
     */
    public function getPhotoGallery()
    {
        if (!$this->gallery) {
            if ($this->getVendorId() === false) {
                return '[]';
            }
            $gallery = $this->_galleryResource->loadByVendorId($this->getVendorId())->getGallery();
            if (!is_array(json_decode($gallery))) {
                $gallery = '[]';
            }
            $this->gallery = $gallery;
        }
        return $this->gallery;
    }

    /**
     * @return array
     */
    public function getBasicInformation()
    {
        if (!$this->basicInformation) {
            $this->basicInformation = [];
            $vendor = $this->getVendor();
            // Property Information
            $this->basicInformation['property_name'] = $vendor->getVendorName();
            $this->basicInformation['property_type'] = $vendor->getBusinessCategory();
            $this->basicInformation['website'] = $vendor->getWebsite();
            $this->basicInformation['rating'] = 'not done yet';
            // Address
            $address = $vendor->getAddressObj();
            $this->basicInformation['country'] = $address->getCountryModel()->getName();
            $this->basicInformation['governorate'] = $address->getRegion();
            $this->basicInformation['city'] = $address->getCity() ?: $vendor->getCity();
            $this->basicInformation['address'] = $address->getStreetFull() ?: $vendor->getStreetFull();
            $this->basicInformation['phone_number'] = $address->getTelephone() ?: $vendor->getTelephone();
            // Property Description
            $this->basicInformation['property_description'] = $vendor->getBusinessDes();
            // Property Term
            $this->basicInformation['property_terms'] = @$this->getHotelProperties()['terms'];
            // Contact
            $this->basicInformation['contact_name'] = $vendor->getVendorName();
            $this->basicInformation['position'] = $vendor->getBusinessRole();
//            $this->basicInformation['phone_number'] = $vendor->getTelephone() ?: $address->getTelephone();
            $this->basicInformation['email'] = $vendor->getEmail();
        }
        return $this->basicInformation;
    }

    /**
     * @return array
     */
    public function getPropertyDetails()
    {
        if (!$this->propertyDetails) {
            $this->propertyDetails = [];
            $vendorId = $this->getVendorId();
            if ($vendorId === false) {
                return [];
            }
            $this->propertyDetails['check_in_from'] = @$this->getHotelProperties()['check_in_from'];
            $this->propertyDetails['check_in_to'] = @$this->getHotelProperties()['check_in_to'];
            $this->propertyDetails['check_out_from'] = @$this->getHotelProperties()['check_out_from'];
            $this->propertyDetails['check_out_to'] = @$this->getHotelProperties()['check_out_to'];
            $this->propertyDetails['facilities'] = $this->getHotelFacilities();
        }
        return $this->propertyDetails;
    }

    /**
     * @return \Unirgy\Dropship\Model\Vendor
     */
    public function getVendor()
    {
        return $this->vendorSession->getVendor();
    }
}
