<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 05/04/2018
 * Time: 13:31
 */

namespace Magenest\Hotel\Block\Order\Vendor\Rate;

use Magenest\Hotel\Helper\BookingStatus as Status;

/**
 * Class Availability
 * @package Magenest\Hotel\Block\Order\Vendor
 */
class Availability extends \Magento\Framework\View\Element\Template
{
    const CONTROLLER = 'hotel/rate/availability';

    /**
     * @var array
     */
    protected $availFilter = [7, 14, 30, 60, 90];

    /**
     * @var int
     */
    protected $defaultFilter = 30;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory
     */
    protected $_roomTypeVendorCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory
     */
    protected $_packageCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory
     */
    protected $_qtyCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory
     */
    protected $_hotelOrderCollectionFactory;

    /**
     * @var \Magenest\Hotel\Logger\Logger
     */
    protected $_logger;

    /**
     * @var bool
     */
    protected $_isCustomFilter;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection
     */
    protected $roomTypeCollection;

    /**
     * @var array
     */
    protected $dateArr;

    /**
     * @var string
     */
    protected $firstDate;

    /**
     * @var string
     */
    protected $lastDate;

    /**
     * @var array
     */
    protected $totalQtyDataArr = [];

    /**
     * @var array
     */
    protected $bookedQtyDataArr = [];

    /**
     * @var array
     */
    protected $priceArr = [];

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\CollectionFactory $roomTypeVendorCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\CollectionFactory $qtyCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Logger\Logger $logger,
        array $data = []
    ) {
        $this->_roomTypeVendorCollectionFactory = $roomTypeVendorCollectionFactory;
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
        $this->_packageCollectionFactory = $packageCollectionFactory;
        $this->_qtyCollectionFactory = $qtyCollectionFactory;
        $this->_vendorSession = $vendorSession;
        $this->_logger = $logger;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection
     */
    public function getRoomTypeVendor()
    {
        if (!$this->_vendorSession->isLoggedIn()) {
            return;
        }
        if (!$this->roomTypeCollection) {
            /** @var \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection $roomtypeCollection */
            $roomtypeCollection = $this->_roomTypeVendorCollectionFactory->create();
            $this->roomTypeCollection = $roomtypeCollection
                ->addVendorToFilter($this->_vendorSession->getVendorId());
        }
        return $this->roomTypeCollection;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getDateArr()
    {
        if (!$this->dateArr) {
            $return = [];
            $firstDate = new \DateTime($this->getFirstDate('Y-m-d'));
            $lastDate = new \DateTime($this->getLastDate('Y-m-d'));
            $interval = new \DateInterval("P1D");
            for ($d = $firstDate; $d <= $lastDate; $d->add($interval)) {
                $return[] = $d->getTimestamp();
            }
            $this->dateArr = $return;
        }
        return $this->dateArr;
    }

    /**
     * @return array
     */
    public function getAvailableFilter()
    {
        return $this->availFilter;
    }

    /**
     * @return int
     */
    public function getDefaultFilter()
    {
        return $this->defaultFilter;
    }

    /**
     * @return bool
     */
    public function isCustomFilter()
    {
        if (!is_bool($this->_isCustomFilter)) {
            $params = $this->getRequest()->getParams();
            $this->_isCustomFilter = (
                isset($params['from'], $params['to']) &&
                strtotime($params['from']) &&
                strtotime($params['to']) &&
                strtotime($params['from']) <= strtotime($params['to'])
            );
        }
        return $this->_isCustomFilter;
    }

    /**
     * @param null $format
     * @return false|int|string
     */
    public function getFirstDate($format = null)
    {
        if (!$this->firstDate) {
            if ($this->isCustomFilter()) {
                $this->firstDate = strtotime($this->getRequest()->getParam('from'));
            } else {
                $this->firstDate = strtotime('+ 1 day');
            }
        }

        if (!$format) {
            return $this->firstDate;
        } else {
            return date($format, $this->firstDate);
        }
    }

    /**
     * @param null $format
     * @return false|int|string
     */
    public function getLastDate($format = null)
    {
        if (!$this->lastDate) {
            if ($this->isCustomFilter()) {
                $this->lastDate = strtotime($this->getRequest()->getParam('to'));
            } elseif ($next = (int)$this->getRequest()->getParam('next')) {
                $this->lastDate = strtotime("+ $next days");
            } else {
                $this->lastDate = strtotime("+ {$this->getDefaultFilter()} days");
            }
        }

        if (!$format) {
            return $this->lastDate;
        } else {
            return date($format, $this->lastDate);
        }
    }

    /**
     * @param string $fromVar
     * @param string $toVar
     * @return string
     */
    public function getDateFilterUrlTemplate($fromVar, $toVar)
    {
        return $this->getUrl(self::CONTROLLER, ['from' => $fromVar, 'to' => $toVar]);
    }

    /**
     * @param int $dayNum
     * @return string
     */
    public function getNextDateFilterUrl($dayNum)
    {
        return $this->getUrl(self::CONTROLLER, ['next' => $dayNum]);
    }

    /**
     * @return string
     */
    public function getDateDisableUrl()
    {
        return $this->getUrl('hotel/vendor_package/disable');
    }

    /**
     * @return string
     */
    public function getDateEnableUrl()
    {
        return $this->getUrl('hotel/vendor_package/enable');
    }

    /**
     * @return string
     */
    public function getOfferBulkUpdateUrl()
    {
        return $this->getUrl('hotel/vendor_offer/bulk');
    }

    /**
     * @return string
     */
    public function getRoomTypeBulkUpdateUrl()
    {
        return $this->getUrl('hotel/vendor_roomtype/bulk');
    }

    /**
     * @return int
     */
    public function getCurrentOption()
    {
        if ($this->isCustomFilter()) {
            return 0;
        }
        $param = $this->getRequest()->getParam('next');
        return $param === null ? $this->getDefaultFilter() : (int)$param;
    }

    /**
     * @return string
     */
    public function getButtonText()
    {
        $text = '';
        $format = 'F d, Y';
        if ($this->isCustomFilter()) {
            $text .= __("Custom: ");
        } else {
            $text .= __("Next %1 days: ", $this->getCurrentOption());
        }

        $text .= __("%1 - %2", $this->getFirstDate($format), $this->getLastDate($format));
        return $text;
    }

    /**
     * @param $roomTypeId
     */
    protected function loadBookedQty($roomTypeId)
    {
        try {
            $this->bookedQtyDataArr[$roomTypeId] = [];
            /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
            $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();
            $hotelOrderCollection->groupQtyByRoomType($roomTypeId);
//        ->addStatusToFilter([Status::PENDING, Status::ACTIVE]);
            foreach ($hotelOrderCollection as $i) {
                $this->bookedQtyDataArr[$roomTypeId][$i->getDate()] = $i->getTotal();
            }
        } catch (\Exception $e) {
            $this->_logger->debug("Errr loading qty in vendor page" . $e->getMessage());
        }
    }

    /**
     * @param $roomTypeId
     * @return array
     */
    public function getBookedQtyDataArr($roomTypeId)
    {
        if (!isset($this->bookedQtyDataArr[$roomTypeId])) {
            $this->loadBookedQty($roomTypeId);
        }
        return $this->bookedQtyDataArr[$roomTypeId];
    }

    /**
     * @param $packageId
     */
    protected function loadPriceData($packageId)
    {
        try {
            $this->priceArr[$packageId] = [];
            /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $priceCollection */
            $priceCollection = $this->_qtyCollectionFactory->create();
            $priceCollection->addDateRangeFilter($this->getFirstDate('Y-m-d'), $this->getLastDate('Y-m-d'))
                ->addPackageFilter($packageId);
            foreach ($priceCollection as $price) {
                $this->priceArr[$packageId][$price->getDate()] = ['sale' => (float)$price->getSalePrice(), 'orig' => (float)$price->getOrigPrice()];
            }
        } catch (\Exception $e) {
            $this->_logger->debug("Errr loading qty in vendor page" . $e->getMessage());
        }
    }

    /**
     * @param $packageId
     * @return array
     */
    public function getPriceArr($packageId)
    {
        if (!isset($this->priceArr[$packageId])) {
            $this->loadPriceData($packageId);
        }
        return $this->priceArr[$packageId];
    }

    /**
     * @param $roomTypeId
     */
    protected function loadTotalRoomQty($roomTypeId)
    {
        try {
            $this->totalQtyDataArr[$roomTypeId] = [];
            /** @var \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $qtyCollection */
            $qtyCollection = $this->_qtyCollectionFactory->create();
            $qtyCollection->addDateRangeFilter($this->getFirstDate('Y-m-d'), $this->getLastDate('Y-m-d'))
                ->groupQtyByRoomType($roomTypeId);
            foreach ($qtyCollection as $qty) {
                $this->totalQtyDataArr[$roomTypeId][$qty->getDate()] = $qty->getTotal();
            }
        } catch (\Exception $e) {
            $this->_logger->debug("Errr loading qty in vendor page" . $e->getMessage());
        }
    }

    /**
     * @param $roomTypeId
     * @return array
     */
    public function getTotalRoomQtyArr($roomTypeId)
    {
        if (!isset($this->totalQtyDataArr[$roomTypeId])) {
            $this->loadTotalRoomQty($roomTypeId);
        }
        return $this->totalQtyDataArr[$roomTypeId];
    }

    /**
     * @param $roomId
     * @return \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection
     */
    public function getOffers($roomId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageCollection */
        $packageCollection = $this->_packageCollectionFactory->create();
        return $packageCollection->addVendorRoomFilter($roomId);
    }
}
