<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 05/04/2018
 * Time: 13:31
 */

namespace Magenest\Hotel\Block\Order\Vendor\Rate;


/**
 * Class Categories
 * @package Magenest\Hotel\Block\Order\Vendor
 */
class Categories extends \Magento\Framework\View\Element\Template
{
    /**
     * @return int|string
     */
    public function getCurrentStatus()
    {
        if (!isset($this->getFilter()['status'])) {
            return 'all';
        }
        return (int)($this->getFilter()['status']);
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        $return = [];
        $status = $this->getRequest()->getParam('status');
        if (is_numeric($status)) {
            $return['status'] = $status;
        }
        return $return;
    }
}
