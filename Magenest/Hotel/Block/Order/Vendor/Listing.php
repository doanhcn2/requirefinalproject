<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Block\Order\Vendor;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

/**
 * Class Listing
 * @package Magenest\Hotel\Block\Order\Vendor
 */
class Listing extends \Magento\Framework\View\Element\Template
{
    const CONTROLLER = "hotel/vendor_reservation/index";

    /**
     * @var array
     */
    protected $availFilter = [7, 14, 30, 60, 90];

    /**
     * @var int
     */
    protected $defaultFilter = 30;

    /**
     * @var string
     */
    protected $firstDate;

    /**
     * @var string
     */
    protected $lastDate;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory
     */
    protected $_hotelOrderCollectionFactory;

    /**
     * @var \Magenest\Hotel\Helper\Helper
     */
    protected $_helper;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    protected $_hotelOrders;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_currency;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepo;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * Listing constructor.
     * @param \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Locale\CurrencyInterface $currency
     * @param \Unirgy\Dropship\Model\Session $vendorSession
     * @param \Magenest\Hotel\Helper\Helper $helper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Locale\CurrencyInterface $currency,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magenest\Hotel\Helper\Helper $helper,
        Template\Context $context,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
        $this->productRepo = $productRepository;
        $this->_vendorSession = $vendorSession;
        $this->_currency = $currency;
        $this->_helper = $helper;
    }

    /**
     * @return $this
     * @throws LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getHotelOrders()) {
            /** @var \Magento\Theme\Block\Html\Pager $pager */
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'hotel.order.history.pager'
            );
            $pager->setCollection(
                $this->getHotelOrders()
            );
            $this->setChild('pager', $pager);
            $this->getHotelOrders()->load();
        }
        return $this;
    }

    /**
     * @param null $format
     * @return false|int|string
     */
    public function getFirstDate($format = null)
    {
        if (!$this->firstDate) {
            $this->firstDate = strtotime('+ 1 day');
        }

        if (!$format) {
            return $this->firstDate;
        } else {
            return date($format, $this->firstDate);
        }
    }

    /**
     * @param null $format
     * @return false|int|string
     */
    public function getLastDate($format = null)
    {
        if (!$this->lastDate) {
            if ($next = (int)$this->getRequest()->getParam('next')) {
                $this->lastDate = strtotime("+ $next days");
            } else {
                $this->lastDate = strtotime("+ {$this->getDefaultFilter()} days");
            }
        }

        if (!$format) {
            return $this->lastDate;
        } else {
            return date($format, $this->lastDate);
        }
    }

    /**
     * @return string
     */
    public function getButtonText()
    {
        $format = 'F d, Y';
        $text = __("Next %1 days: ", $this->getCurrentOption());

        $text .= __("%1 - %2", $this->getFirstDate($format), $this->getLastDate($format));
        return $text;
    }

    /**
     * @param int $next
     * @return string
     */
    public function getOptionText($next)
    {
        $format = 'F d, Y';
        return __(
            "Next %1 days: %2 - %3",
            $next,
            $this->getFirstDate($format),
            date($format, strtotime("+ {$next} days"))
        );
    }

    /**
     * @return array
     */
    public function getAvailableFilter()
    {
        return $this->availFilter;
    }

    /**
     * @return int
     */
    public function getCurrentOption()
    {
        $param = $this->getRequest()->getParam('next');
        return $param === null ? $this->getDefaultFilter() : (int)$param;
    }

    /**
     * @param string $sortVar
     * @param string $orderVar
     * @param string $nextVar
     * @return string
     */
    public function getSortOrderUrl($sortVar, $orderVar, $nextVar)
    {
        return $this->getUrl(self::CONTROLLER, ['next' => $nextVar, 'sort' => $sortVar, 'order' => $orderVar]);
    }

    /**
     * @param int $dayNum
     * @return string
     */
    public function getNextDateFilterUrl($dayNum)
    {
        return $this->getUrl(self::CONTROLLER, ['next' => $dayNum]);
    }

    /**
     * @return int
     */
    public function getDefaultFilter()
    {
        return $this->defaultFilter;
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     * @throws LocalizedException
     */
    public function getHotelOrders()
    {
        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }
        if (!$this->_hotelOrders) {
            $vendorId = $this->_vendorSession->getVendorId();
            /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
            $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();

            $this->_hotelOrders = $hotelOrderCollection->loadByVendorId($vendorId)->moreDetails();
            $filter = $this->getFilter();
            if ($filter === []) {
                return $this->_hotelOrders;
            }
            if (isset($filter['arrival'], $filter['arrival']['from'], $filter['arrival']['to'])) {
                $this->_hotelOrders->addFieldToFilter('check_in_date',
                    [
                        'from' => $filter['arrival']['from'],
                        'to' => $filter['arrival']['to']
                    ]
                );
            }
            if (isset($filter['sort'], $filter['order'])) {
                $this->_hotelOrders->setOrder($filter['sort'], strtoupper($filter['order']));
            }
        }
        return $this->_hotelOrders;
    }

    /**
     * @param $productId
     * @return null|string
     */
    public function getProductName($productId)
    {
        try {
            return $this->productRepo->getById($productId)->getName();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return '';
        }
    }

    /**
     * @param $code
     * @return string
     */
    public function getCurrencySymbol($code = null)
    {
        if ($code === null) {
            $code = $this->_currency->getDefaultCurrency();
        }
        return $this->_currency->getCurrency($code)->getSymbol();
    }

    /**
     * @param $id
     * @return array
     */
    public function getOrderInfo($id)
    {
        return $this->_helper->getOrderInfo($id);
    }

    /**
     * @return array
     */
    public function getFilter()
    {
        $return = [];
        $params = $this->getRequest()->getParams();
        if (!is_array($params)) {
            return $return;
        }
        foreach ($params as $key => $value) {
            if ($key === 'next' && intval($value) > 0) {
                $return['arrival'] = [
                    'from' => date('Y-m-d'),
                    'to' => date('Y-m-d', strtotime("+ {$value} days"))
                ];
                continue;
            }
            if ($key === 'sort' && isset($params['order'])) {
                if (!in_array($params['order'], ['asc', 'desc'])) {
                    continue;
                }
                if (!in_array(
                    $params['sort'],
                    ['check_in_date', 'check_out_date', 'product_id', 'total_paid', 'created_at'])
                ) {
                    continue;
                }
                $return['sort'] = $params['sort'];
                $return['order'] = $params['order'];
            }
        }
        if (!isset($return['arrival'])) {
            $return['arrival'] = [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d', strtotime("+ {$this->getDefaultFilter()} days"))
            ];
        }
        return $return;
    }

    /**
     * @param $id
     * @return string
     */
    public function getCancelUrl($id)
    {
        return $this->getUrl('hotel/ordervendor/cancel', ['id' => $id]);
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
