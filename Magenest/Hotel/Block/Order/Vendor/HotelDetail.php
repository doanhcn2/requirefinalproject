<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 05/04/2018
 * Time: 13:31
 */

namespace Magenest\Hotel\Block\Order\Vendor;

class HotelDetail extends \Magenest\Hotel\Block\Vendor\Product
{
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
