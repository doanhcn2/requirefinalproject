<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 2/4/18
 * Time: 9:30 PM
 */

namespace Magenest\Hotel\Block\Order;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class History extends Template
{
    protected $_customerSession;

    protected $_hotelOrderCollectionFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    protected $hotelOrders;

    protected $_helper;

    public function __construct(
        Template\Context $context,
        CustomerSession $customerSession,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $collectionFactory,
        \Magenest\Hotel\Helper\Helper $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_customerSession = $customerSession;
        $this->_hotelOrderCollectionFactory = $collectionFactory;
        $this->_helper = $helper;
    }

    public function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Hotel Bookings'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getHotelOrders()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'hotel.order.history.pager'
            )->setCollection(
                $this->getHotelOrders()
            );
            $this->setChild('pager', $pager);
            $this->getHotelOrders()->load();
        }

        return $this;
    }

    public function getOrderInfo($id)
    {
        return $this->_helper->getOrderInfo($id);
    }

    public function getCancelUrl($id)
    {
        return $this->getUrl('hotel/order/cancel', ['id' => $id]);
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getHotelOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            throw new LocalizedException(__("Customer not logged in"));
        }
        if (!$this->hotelOrders) {
            $this->hotelOrders = $this->_hotelOrderCollectionFactory->create()->loadByCustomerId($customerId);
        }
        $items = $this->hotelOrders;
        return $this->hotelOrders;
    }
}
