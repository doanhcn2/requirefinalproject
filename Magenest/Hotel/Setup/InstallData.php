<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 28/01/2018
 * Time: 14:26
 */
namespace Magenest\Hotel\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallDataInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $fixedAmenities = [
            [
                'room_kind'=>"Room",
                'name'=>"Clothes Rack"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Drying Rack"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Fold Up Bed"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Air Conditioning"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Heating"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Fire Place"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Iron"
            ],
            [
                'room_kind'=>"Room",
                'name'=>"Fan"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Bidet"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Bath Tub"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Hair Dryer"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Free Toiletries"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Spa Bath"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Shower"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Slippers"
            ],
            [
                'room_kind'=>"Bathroom",
                'name'=>"Toilet Grab Rails"
            ]
        ];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach ($fixedAmenities as $fixedAmenity) {
            /** @var \Magenest\Hotel\Model\RoomAmenitiesAdmin $amenity */
            $amenity = $objectManager->create('Magenest\Hotel\Model\RoomAmenitiesAdmin');
            $amenity->addData($fixedAmenity)->save();
        }

    }
}