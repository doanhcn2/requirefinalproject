<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 1/31/18
 * Time: 4:18 PM
 */

namespace Magenest\Hotel\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Magenest\Hotel\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->fixPackageDetailsTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->fixAmenityTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->fixHotelOrderTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->addColumnPackageDetailTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->addColumnHotelTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $this->addLocationColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $this->createHotelPropertiesAdminTable($setup);
            $this->createHotelPropertiesVendorTable($setup);
        }

        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            $this->addTotalPaidColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.0.11', '<')) {
            $this->addRefundedColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.0.12', '<')) {
            $this->addLongLatColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.0.13', '<')) {
            $this->dropFacilityCols($setup);
        }

        if (version_compare($context->getVersion(), '1.0.14', '<')) {
            $this->changeFacilTablesName($setup);
        }

        if (version_compare($context->getVersion(), '1.0.15', '<')) {
            $this->addBasePricePerNightRoomType($setup);
        }

        if (version_compare($context->getVersion(), '1.0.16', '<')) {
            $this->addLogoRoomKind($setup);
        }

        if (version_compare($context->getVersion(), '1.0.17', '<')) {
            $this->addGalleryTable($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function addGalleryTable($setup)
    {
        $vendorTable = $setup->getTable('udropship_vendor');
        $galleryTable = $setup->getTable('magenest_hotel_gallery');
        $table = $setup->getConnection()->newTable($galleryTable)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Vendor Id'
            )
            ->addColumn(
                'gallery',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'gallery'
            )->addForeignKey(
                $setup->getFkName($galleryTable, 'vendor_id', $vendorTable, 'vendor_id'),
                'vendor_id',
                $vendorTable,
                'vendor_id',
                Table::ACTION_CASCADE
            );
        $setup->getConnection()->createTable($table);
    }


    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addLogoRoomKind($setup)
    {
        $table = $setup->getTable('magenest_hotel_room_amenities_admin');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'logo',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'logo link'
            ]
        );
    }


    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addBasePricePerNightRoomType($setup)
    {
        $table = $setup->getTable('magenest_hotel_room_type_vendor');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'price_per_night',
            [
                'type' => Table::TYPE_INTEGER,
                'comment' => 'Base Price per Night'
            ]
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function changeFacilTablesName($setup)
    {
        $connection = $setup->getConnection();
        $tables = [
            'magenest_hotel_property_details_admin' => 'magenest_hotel_property_facil_admin',
            'magenest_hotel_property_details_vendor' => 'magenest_hotel_property_facil_vendor'
        ];
        foreach ($tables as $old => $new) {
            if ($connection->isTableExists($old)) {
                $connection->renameTable($old, $new);
            }
        }
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function dropFacilityCols($setup)
    {
        $table = $setup->getTable('magenest_hotel_property_details');
        $connection = $setup->getConnection();
        $cols = [
            'gym', 'laundry', 'smoking_rooms', 'pool', 'shuttle_service',
            'service_desk_24h', 'restaurant', 'bar', 'non_smoking_rooms',
            'bbq_facility', 'front_desk', 'concierge'
        ];
        foreach ($cols as $col) {
            if ($connection->tableColumnExists($table, $col)) {
                $connection->dropColumn($table, $col);
            }
        }
    }


    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addLongLatColumn($setup)
    {
        $table = $setup->getTable('magenest_hotel_property_details');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'longitude',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Longitude'
            ]
        );
        $connection->addColumn(
            $table,
            'latitude',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Latitude'
            ]
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addRefundedColumn($setup)
    {
        $table = $setup->getTable('magenest_hotel_order');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'refunded_at',
            [
                'type' => Table::TYPE_DATE,
                'comment' => 'Refunded At'
            ]
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addLocationColumn($setup)
    {
        $table = $setup->getTable('magenest_hotel_property_details');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'location',
            [
                'type' => Table::TYPE_TEXT,
                'comment' => 'Hotel Location'
            ]
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function fixPackageDetailsTable($setup)
    {
        $table = $setup->getTable('magenest_hotel_package_details');
        $connection = $setup->getConnection();
        if ($connection->tableColumnExists($table, 'unpublish_Date')) {
            $connection->changeColumn(
                $table,
                'unpublish_Date',
                'unpublish_date',
                ['type' => Table::TYPE_DATE, 'comment' => 'Unpublish Date']
            );
        }
        $connection->modifyColumn(
            $table,
            'min_nights',
            ['type' => Table::TYPE_INTEGER, 'nullable' => true, 'unsigned' => true, 'comment' => 'Minimum Nights']
        );
        $connection->modifyColumn(
            $table,
            'max_nights',
            ['type' => Table::TYPE_INTEGER, 'nullable' => true, 'unsigned' => true, 'comment' => 'Maximum Nights']
        );
        $connection->addColumn(
            $table,
            'penalty',
            ['type' => Table::TYPE_TEXT, 'nullable' => true, 'comment' => 'Penalty']
        );

    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function fixAmenityTable($setup)
    {
        $table = $setup->getTable('magenest_hotel_room_amenities_vendor');
        $connection = $setup->getConnection();
        if ($connection->tableColumnExists($table, 'amenities')) {
            $connection->changeColumn(
                $table,
                'amenities',
                'amenity',
                ['type' => Table::TYPE_INTEGER, 'comment' => 'Amentity Id']
            );
        }
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function fixHotelOrderTable($setup)
    {
        $table = $setup->getTable('magenest_hotel_order');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'order_id',
            [
                'type' => Table::TYPE_INTEGER,
                'unsigned' => true,
                'comment' => 'Order Id'
            ]);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addColumnPackageDetailTable($setup)
    {
        $table = $setup->getTable('magenest_hotel_package_details');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'vendor_id',
            [
                'type' => Table::TYPE_INTEGER,
                'unsigned' => true,
                'comment' => 'Vendor Id'
            ]);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addColumnHotelTable($setup)
    {
        $table = $setup->getTable('magenest_hotel_order');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'customer_id',
            [
                'type' => Table::TYPE_INTEGER,
                'unsigned' => true,
                'comment' => 'Customer Id'
            ]);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     */
    public function addTotalPaidColumn($setup)
    {
        $table = $setup->getTable('magenest_hotel_order');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'total_paid',
            [
                'type' => Table::TYPE_DECIMAL,
                'unsigned' => true,
                'nullable' => true,
                'length' => '12,4',
                'comment' => 'Customer Id'
            ]);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function createHotelPropertiesAdminTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_property_facil_admin'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'facility_kind',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Facility Kind'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Name of Facility'
            )
            ->addColumn(
                'is_enable',
                Table::TYPE_SMALLINT,
                1,
                ['nullable' => false],
                'Status of Facility'
            );

        $setup->getConnection()->createTable($table);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function createHotelPropertiesVendorTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_property_facil_vendor'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                'facility_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Facility ID'
            );
        $setup->getConnection()->createTable($table);
    }
}
