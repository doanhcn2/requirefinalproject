<?php

namespace Magenest\Hotel\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Store\Model\Store;

/**
 * Class InstallSchema
 * @package Magenest\Hotel\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    protected $_entityTypeModel;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    protected $_catalogAttribute;

    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    protected $_eavSetup;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * @var \Magento\Store\Model\Store
     */
    protected $store;

    /**
     * InstallSchema constructor.
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetup
     * @param \Magento\Eav\Model\Entity\Type $entityType
     * @param \Magento\Eav\Model\Entity\Attribute $catalogAttribute
     * @param \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewrite
     * @param Store $store
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetup,
        \Magento\Eav\Model\Entity\Type $entityType,
        \Magento\Eav\Model\Entity\Attribute $catalogAttribute,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewrite,
        Store $store
    ) {
        $this->_eavSetup = $eavSetup;
        $this->_entityTypeModel = $entityType;
        $this->_catalogAttribute = $catalogAttribute;
        $this->_urlRewrite = $urlRewrite;
        $this->store = $store;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_room_type_admin'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'room_type',
                Table::TYPE_TEXT,
                null,
                [],
                'Room Type'
            );
        $setup->getConnection()->createTable($table);

        $this->createHotelPropertyTable($setup);
        $this->createPropertyServiceTable($setup);
        $this->createPackageAndServiceTable($setup);
        $this->createPackageDetailsTable($setup);
        $this->createPackagePriceTable($setup);
        $this->createHotelOrderTable($setup);

        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_room_type_vendor'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'room_type_admin_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Room Type'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                'room_title',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Room Title'
            )
            ->addColumn(
                'non_smoking',
                Table::TYPE_INTEGER,
                1,
                ['nullable' => true, 'default' => 0],
                'Is non smoking'
            )
            ->addColumn(
                'number_rooms_available',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Number of Rooms Available'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Description this room type'
            )
            ->addColumn(
                'beds_type',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Bed Type'
            )
            ->addColumn(
                'number_guests',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Max Guests of Room'
            )
            ->addColumn(
                'room_size',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Room Size'
            )
            ->addColumn(
                'has_extra_beds',
                Table::TYPE_INTEGER,
                1,
                ['nullable' => false, 'default' => 0],
                'Has Extra Beds'
            )
            ->addColumn(
                'number_extra_beds',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Number Extra Beds'
            )
            ->addColumn(
                'price_per_bed',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Price of Extra Bed'
            )
            ->addColumn(
                'photo_room',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Photo of Room'
            );
        $setup->getConnection()->createTable($table);


        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_bed_type_admin'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'bed_type',
                Table::TYPE_TEXT,
                null,
                [],
                'Bed Type'
            );
        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_room_amenities_vendor'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                'room_vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Room of Vendor'
            )
            ->addColumn(
                'amenities',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'amenities of room'
            );
        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_room_amenities_admin'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'room_kind',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Room Kind'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Name of Amenity'
            )
            ->addColumn(
                'is_enable',
                Table::TYPE_SMALLINT,
                1,
                ['nullable' => false],
                'Status of Amenity'
            );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    protected function createHotelPropertyTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_property_details'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                'check_in_from',
                Table::TYPE_TEXT,
                null,
                [],
                'Check in from time'
            )
            ->addColumn(
                'check_in_to',
                Table::TYPE_TEXT,
                null,
                [],
                'Check in to time'
            )
            ->addColumn(
                'check_out_from',
                Table::TYPE_TEXT,
                null,
                [],
                'Check out from time'
            )
            ->addColumn(
                'check_out_to',
                Table::TYPE_TEXT,
                null,
                [],
                'Check out to time'
            )
            ->addColumn(
                'is_internet',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Is Internet Available to Guests?'
            )
            ->addColumn(
                'is_parking',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Is Parking Available to Guests?'
            )
            ->addColumn(
                'is_breakfast',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Is Breakfast Available to Guests?'
            )
            ->addColumn(
                'is_children_allowed',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Can you Accommodate Children?'
            )
            ->addColumn(
                'is_pet_allowed',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Do you allow Pets?'
            )
            ->addColumn(
                'languages',
                Table::TYPE_TEXT,
                null,
                [],
                'Languages spoken by Staff'
            )
            ->addColumn(
                'service_desk_24h',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                '24 Hour Service Desk'
            )
            ->addColumn(
                'gym',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Gym'
            )
            ->addColumn(
                'laundry',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Laundry'
            )
            ->addColumn(
                'smoking_rooms',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Smoking Rooms'
            )
            ->addColumn(
                'pool',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Pool'
            )
            ->addColumn(
                'service_desk_24h',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                '24 Hour Service Desk'
            )
            ->addColumn(
                'shuttle_service',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Shuttle Service'
            )
            ->addColumn(
                'service_desk_24h',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                '24 Hour Service Desk'
            )
            ->addColumn(
                'restaurant',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Restaurant'
            )
            ->addColumn(
                'bar',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Bar'
            )
            ->addColumn(
                'non_smoking_rooms',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Non Smoking Rooms'
            )
            ->addColumn(
                'bbq_facility',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'bbq Facility'
            )
            ->addColumn(
                'front_desk',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Front Desk'
            )
            ->addColumn(
                'concierge',
                Table::TYPE_BOOLEAN,
                null,
                ['default' => false],
                'Concierge'
            )
            ->addColumn(
                'photos',
                Table::TYPE_TEXT,
                null,
                [],
                'Photos URL'
            )
            ->addColumn(
                'terms',
                Table::TYPE_TEXT,
                null,
                [],
                'Mention any special terms applicable to this property'
            )
            ->addColumn(
                'is_tax',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Is there additional taxes in your area not included in the rate?'
            )
            ->addColumn(
                'tax_amount',
                Table::TYPE_DECIMAL,
                '10,2',
                [],
                'Tax Amount'
            )
            ->addColumn(
                'tax_type',
                Table::TYPE_TEXT,
                null,
                [],
                'Tax Type'
            );

        $setup->getConnection()->createTable($table);
    }

    public function createPropertyServiceTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_service'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'vendor_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Vendor Id'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                [],
                'Title'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                null,
                [],
                'Description'
            )
            ->addColumn(
                'price',
                Table::TYPE_DECIMAL,
                '10,2',
                [],
                'Price'
            );

        $setup->getConnection()->createTable($table);
    }

    public function createPackageDetailsTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_package_details'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Product Id'
            )
            ->addColumn(
                'qty',
                Table::TYPE_INTEGER,
                null,
                [],
                'Numbers of room'
            )
            ->addColumn(
                'room_type',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Room Type ID'
            )
            ->addColumn(
                'book_from',
                Table::TYPE_DATE,
                null,
                [],
                'Start date guests able to book'
            )
            ->addColumn(
                'book_to',
                Table::TYPE_DATE,
                null,
                [],
                'End date guests able to book'
            )
            ->addColumn(
                'min_nights',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Minimum nights'
            )
            ->addColumn(
                'max_nights',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Maximum nights'
            )
            ->addColumn(
                'cancellation_policy',
                Table::TYPE_TEXT,
                null,
                [],
                'Cancellation Policy'
            )
            ->addColumn(
                'min_time_to_book',
                Table::TYPE_TEXT,
                null,
                [],
                'What is the minimum time in advance customers can be make a booking?'
            )
            ->addColumn(
                'is_published_asap',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Is published ASAP'
            )
            ->addColumn(
                'publish_date',
                Table::TYPE_DATE,
                null,
                [],
                'Publish date'
            )
            ->addColumn(
                'is_unpublished_alap',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Is unpublished alap'
            )
            ->addColumn(
                'unpublish_Date',
                Table::TYPE_DATE,
                null,
                [],
                'Unpublish date'
            )
            ->addColumn(
                'special_terms',
                Table::TYPE_TEXT,
                null,
                [],
                'Special Terms'
            );

        $setup->getConnection()->createTable($table);
    }

    public function createPackageAndServiceTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_package_and_service'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'package_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Package Id'
            )
            ->addColumn(
                'service_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Service ID'
            );

        $setup->getConnection()->createTable($table);
    }

    public function createPackagePriceTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_package_price'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'package_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Package Id'
            )
            ->addColumn(
                'sale_price',
                Table::TYPE_DECIMAL,
                '10,2',
                [],
                'Sale Price'
            )
            ->addColumn(
                'orig_price',
                Table::TYPE_DECIMAL,
                '10,2',
                [],
                'Original Price'
            )
            ->addColumn(
                'qty',
                Table::TYPE_INTEGER,
                null,
                [],
                'Quantity'
            )
            ->addColumn(
                'date',
                Table::TYPE_DATE,
                null,
                [],
                'Date'
            );

        $setup->getConnection()->createTable($table);
    }

    public function createHotelOrderTable($setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_hotel_order'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'package_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Package Id'
            )
            ->addColumn(
                'qty',
                Table::TYPE_INTEGER,
                null,
                [],
                'Room Qty'
            )
            ->addColumn(
                'services',
                Table::TYPE_TEXT,
                null,
                [],
                'Selected Services'
            )
            ->addColumn(
                'check_in_date',
                Table::TYPE_DATE,
                null,
                [],
                'Check-in date'
            )
            ->addColumn(
                'check_out_date',
                Table::TYPE_DATE,
                null,
                [],
                'Check-out-date'
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                null,
                [],
                'Order status'
            );

        $setup->getConnection()->createTable($table);
    }
}
