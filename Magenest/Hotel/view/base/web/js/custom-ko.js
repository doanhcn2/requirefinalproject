require([
        'jquery',
        'ko',
        'jquery/ui'
    ], function ($, ko) {
        'use strict';

        ko.bindingHandlers.foreachprop = {
            transformObject: function (obj) {
                var properties = [];
                ko.utils.objectForEach(obj, function (key, value) {
                    if (value instanceof Array) {
                        value = ko.observableArray(value);
                    }
                    properties.push({key: key, value: value});
                });
                return properties;
            },
            init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
                var properties = ko.pureComputed(function () {
                    var obj = ko.utils.unwrapObservable(valueAccessor());
                    return ko.bindingHandlers.foreachprop.transformObject(obj);
                });
                ko.applyBindingsToNode(element, {foreach: properties}, bindingContext);
                return {controlsDescendantBindings: true};
            }
        };

        ko.bindingHandlers.sortable = {
            init: function (e, v) {
                $(e).sortable(v());
                $(e).disableSelection();
            }
        };
    }
);
