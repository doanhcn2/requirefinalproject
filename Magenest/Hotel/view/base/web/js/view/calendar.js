/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'underscore',
        'mage/translate',
        'ko',
        'Magento_Ui/js/form/element/abstract',
        'uiRegistry',
        'mage/calendar',
        'prototype',
        'Magenest_Hotel/js/model/calendar/calendarview',
        'varien/form'
    ], function ($, _, $t, ko, Component, registry) {
        'use strict';

        ko.bindingHandlers.mmCalendar = {
            init: function (e, v) {
                $(e).calendar({
                    minDate: 0,
                    dateFormat: "yyyy-mm-dd"
                });
            }
        };

        var selectHandler = function (calendar, value) {
            var selectedDate = registry.get('custom_calendar_date');
            if (selectedDate !== undefined) {
                selectedDate(value);
            }
        };

        ko.bindingHandlers.customCalendarBinding = {
            init: function (e, v) {
                function setupCalendars() {
                    // Embedded Calendar
                    Calendar.setup(
                        {
                            // dateField: 'customDateField',
                            parentElement: 'customCalendar',
                            selectHandler: selectHandler
                        }
                    );
                }

                setupCalendars();
            }
        };

        return Component.extend({
            defaults: {
                elementTmpl: 'Magenest_Hotel/vendor/calendar',
                /* a package = { date: {prices: amt}, otherdate: {otherprices: otheramt},... } */
                currentPackage: ko.observable({}),
                selectedDate: ko.observable(),
                currentSalePrice: ko.observable(),
                currentOrigPrice: ko.observable(),
                errorPrice: ko.observable(false),
                currentRoomQty: ko.observable(),
                qtyRequired: ko.observable(false),
                originalRequired: ko.observable(false),
                bulkStartDate: ko.observable(),
                currency: ko.observable(""),
                bulkEndDate: ko.observable()
            },
            initObservable: function () {
                this._super().observe('selectedDate')
                    .observe('currentPackage');
                return this;
            },
            initialize: function () {
                var self = this;
                this._super();
                // this.initToday();
                // this.updateFields();

                this.selectedDate.subscribe(function (newValue) {
                    self.bulkStartDate(newValue);
                    self.bulkEndDate(newValue);
                    self.updateFields();
                });
                // setInterval(function () {
                //     console.log(self);
                // }, 2000);
            },
            ajaxLoadData: function () {
                var self = this;
                $.ajax({
                    url: window.HOTEL_PRICE_URL,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        formKey: FORM_KEY,
                        productId: PRODUCT_ID
                    },
                    showLoader: true,
                    success: function (r) {
                        if (r.success === true) {
                            if (r.is_new) {
                                return;
                            }
                            // var data = r.data
                            var currentPackage = {};
                            _.each(r.data, function (value, key) {
                                currentPackage[key] = value;
                            });
                            self.currentPackage(currentPackage);
                            self.currency(window.currency);
                            $("[name='hotel[package-offer][hotel_prices]']").val(JSON.stringify(self.currentPackage()));
                            $("[name='hotel[package-offer][hotel_prices]']").change();
                            self.initToday();
                            self.updateFields();
                        } else {
                            console.log(r);
                        }
                    },
                    error: function (r) {
                        console.log(r);
                    }
                });
            },
            getTemplate: function () {
                this.ajaxLoadData();
                return this._super();
            },
            updateFields: function () {
                if (this.currentPackage() && this.currentPackage()[this.selectedDate()]) {
                    this.currentSalePrice(this.currentPackage()[this.selectedDate()].salePrice);
                    this.currentOrigPrice(this.currentPackage()[this.selectedDate()].origPrice);
                    this.currentRoomQty(this.currentPackage()[this.selectedDate()].roomQty);
                } else {
                    this.currentSalePrice('');
                    this.currentOrigPrice('');
                    this.currentRoomQty('');
                }
            },
            initToday: function () {
                var today = new Date();
                today = this.dateToStr(today);
                this.selectedDate(today);
                this.bulkStartDate(today);
                this.bulkEndDate(today);
                registry.set('custom_calendar_date', this.selectedDate);
            },
            dateToStr: function (date) {
                return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
            },
            setPrice: function (dateStr, salePrice, origPrice, roomQty) {
                this.currentPackage()[dateStr] = {
                    salePrice: salePrice,
                    origPrice: origPrice,
                    roomQty: roomQty
                }
            },
            saveCurrent: function () {
                var self = this;
                var form = new VarienForm('current-date-form', true);
                if (form.validator && form.validator.validate()) {
                    if (self.currentSalePrice() > 0 && self.currentOrigPrice() <= self.currentSalePrice()) {
                        self.errorPrice(true);
                        return;
                    } else {
                        self.errorPrice(false);
                    }

                    var start = new Date(self.bulkStartDate());
                    var end = new Date(self.bulkEndDate());
                    var dateRange = end - start;
                    if (dateRange < 0) {
                        return;
                    }
                    var errorRequired = false;
                    if (self.currentOrigPrice() == "") {
                        self.originalRequired(true);
                        errorRequired = true;
                    } else {
                        errorRequired = false;
                        self.originalRequired(false);
                    }

                    if (self.currentRoomQty() == "") {
                        self.qtyRequired(true);
                        errorRequired = true;
                    } else {
                        errorRequired = false;
                        self.qtyRequired(false);
                    }
                    if (errorRequired) return;
                    if (parseFloat(self.currentSalePrice()) > 0 && parseFloat(self.currentOrigPrice()) <= parseFloat(self.currentSalePrice())) {
                        self.errorPrice(true);
                        return;
                    } else {
                        self.errorPrice(false);
                    }
                    while (dateRange >= 0) {
                        var currDate = start;
                        self.setPrice(self.dateToStr(currDate), self.currentSalePrice(), self.currentOrigPrice(), self.currentRoomQty());
                        currDate.setDate(currDate.getDate() + 1);
                        dateRange = end - currDate;
                    }
                    $("[name='hotel[package-offer][hotel_prices]']").val(JSON.stringify(self.currentPackage()));
                    // $('body').trigger('contentUpdated');
                    $("[name='hotel[package-offer][hotel_prices]']").change();
                    var savedMessageElement = $('div#saved-message');
                    savedMessageElement.hide().fadeIn();
                    setTimeout(function () {
                        savedMessageElement.fadeOut();
                    }, 4000);
                }
            }
        });
    }
);