/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'abstractGrid': 'Magenest_Hotel/js/abstract-grid'
        }
    }
};
