/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'mage/translate',
        'jquery',
        'ko',
        'uiComponent'
    ], function ($t, $, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: '',
                title: $t('Title')
            },
            items: ko.observableArray([]),
            allItems: {},
            total: ko.observable(),
            startNumber: ko.observable(),
            endNumber: ko.observable(),
            pageText: '',
            isCheckedAll: ko.observable(false),
            defaultChecked: false,
            checkedArr: [],
            unCheckArr: [],
            initialize: function () {
                // var self = this;
                this._super();
                this.loadData();
                this.curPage.subscribe(function (val) {
                    if (val > 0) {
                        this.loadData();
                    }
                }.bind(this));
                this.pageText = ko.pureComputed(function () {
                    return this.startNumber() + ' - ' + this.endNumber() + ' of ' + this.total();
                }.bind(this));
                this.isCheckedAll.subscribe(function (val) {
                    this.checkedArr = [];
                    this.unCheckArr = [];
                }.bind(this));
            },
            initObservable: function () {
                this._super().observe([
                    'items',
                    'curPage',
                    'pageSize',
                    'lastPage'
                ]);
                return this;
            },
            massCheck: function (val) {
                for (var page in this.allItems[this.pageSize()]) {
                    for (var i = 0; i < this.allItems[this.pageSize()][page].length; i++) {
                        this.allItems[this.pageSize()][page][i].isChecked = val;
                    }
                }
                this.items([]);
                this.items(this.allItems[this.pageSize()][this.curPage()]);
                // this.items.valueHasMutated();
            },
            clickCheck: function (id, val) {
                if (val === true) {
                    this.isCheckedAll(false);
                }
                var index;
                if (this.defaultChecked && val) {
                    this.unCheckArr.push(id);
                } else if (this.defaultChecked && !val) {
                    index = this.unCheckArr.indexOf(id);
                    if (index > -1) {
                        this.unCheckArr.splice(index, 1);
                    }
                } else if (!this.defaultChecked && !val) {
                    this.checkedArr.push(id);
                } else if (!this.defaultChecked && val) {
                    index = this.checkedArr.indexOf(id);
                    if (index > -1) {
                        this.checkedArr.splice(index, 1);
                    }
                }
                return true;
            },
            uncheckAll: function (val) {
                this.massCheck(!val());
                this.defaultChecked = !val();
                return true;
            },
            deleteItem: function () {
                var count = this.getCheckedCount();
                if (count === 0) {
                    alert($t("Please select at least 1 row"));
                } else if (confirm($t("Do you want to delete %1 row(s)?").replace('%1', count))) {
                    // var self = this;
                    $.ajax({
                        url: this.deleteUrl,
                        method: 'POST',
                        data: this.getChecked()
                    }).done(function () {
                        this.allItems = {};
                        this.curPage(0);
                        this.curPage(1);
                    }.bind(this));
                }
            },
            getCheckedCount: function () {
                var checked = this.getChecked();
                if (checked.type === 'uncheckAll') {
                    return checked.exception.length;
                } else {
                    return this.total() - checked.exception.length;
                }
            },
            getChecked: function () {
                if (this.isCheckedAll() === true) {
                    return {type: 'checkedAll', exception: []};
                } else {
                    if (this.defaultChecked) {
                        return {type: 'checkedAll', exception: this.unCheckArr};
                    } else {
                        return {type: 'uncheckAll', exception: this.checkedArr};
                    }
                }
            },
            getAjaxUrl: function () {
                return this.ajaxUrl.replace(this.curPageVar, this.curPage())
                    .replace(this.pageSizeVar, this.pageSize());
            },
            loadData: function () {
                if (this.allItems[this.pageSize()] !== undefined
                    && this.allItems[this.pageSize()][this.curPage()] !== undefined) {
                    this.items(this.allItems[this.pageSize()][this.curPage()]);
                } else {
                    this.loadDataFromServer();
                    return;
                }
                this.startNumber(this.total() > 0 ?
                    Math.max((this.curPage() - 1) * this.pageSize() + 1, 1)
                    : 0
                );
                this.endNumber(Math.min(this.curPage() * this.pageSize(), this.total()));
                this.lastPage(Math.ceil(this.total() / this.pageSize()));
                if (this.defaultChecked) {
                    this.massCheck(true);
                }
            },
            loadDataFromServer: function () {
                // var self = this;
                $.ajax({
                    url: this.getAjaxUrl(),
                    showLoader: true
                }).done(function (res) {
                    if (this.allItems[this.pageSize()] === undefined) {
                        this.allItems[this.pageSize()] = {};
                    }
                    this.allItems[this.pageSize()][this.curPage()] = res.items;
                    this.total(res.total);
                    this.loadData();
                }.bind(this));
            },
            toPrevPage: function () {
                if (this.curPage() > 1) {
                    this.curPage(this.curPage() - 1);
                }
            },
            toNextPage: function () {
                if (this.curPage() < this.lastPage()) {
                    this.curPage(this.curPage() + 1);
                }
            },
            toFirstPage: function () {
                this.curPage(1);
            },
            toLastPage: function () {
                this.curPage(this.lastPage());
            }
        });
    }
);
