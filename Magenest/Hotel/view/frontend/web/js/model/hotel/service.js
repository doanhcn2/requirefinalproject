/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'ko',
        'uiComponent'
    ], function ($, ko, Component) {
        'use strict';
        return function (t, d, p) {
            this.title = ko.observable(t);
            this.description = ko.observable(d);
            this.price = ko.observable(p);
        }
    }
);