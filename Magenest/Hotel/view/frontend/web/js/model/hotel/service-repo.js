/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'ko',
        'uiComponent',
        'uiRegistry'
    ], function (ko, Component, registry) {
        'use strict';

        var Service = function (id, t, d, p, e) {
            this.index = ko.observable((id !== undefined && id > 0) ? id : 0);
            this.title = ko.observable(t);
            this.description = ko.observable(d);
            this.price = ko.observable(p);
            this.isEnabled = ko.observable(!!e);
        };

        return Component.extend({
            defaults: {
                all: ko.observableArray(),
                toRemove: [],
                getNew: function (id, title, description, price, enabled) {
                    return new Service(id, title, description, price, enabled);
                },
                save: function (s) {
                    this.all.push(s);
                },
                remove: function (e) {
                    this.toRemove.push(e.index());
                    this.all.remove(e);
                },
                gatherData: function () {
                    var data = [];
                    for (var i = 0; i < this.all().length; i++) {
                        var s = this.all()[i];
                        data.push({
                            index: s.index(),
                            title: s.title(),
                            description: s.description(),
                            price: s.price(),
                            isEnabled: s.isEnabled()
                        });
                    }
                    return data;
                },
                init: function () {
                    var self = this,
                        oldServices = registry.get('old_hotel_services') || [];
                    self.toRemove = [];
                    self.all([]);
                    // get data hotel_services and save
                    if (registry.get('hotel_services') === undefined) {
                        registry.set('hotel_services', this.services);
                    } else {
                        this.services = registry.get('hotel_services');
                    }
                    for (var i = 0; i < self.services.length; i++) {
                        var s = self.services[i];
                        if (oldServices[i] !== undefined && oldServices[i].is_enabled) {
                            s.is_enabled = true;
                        }
                        self.save(self.getNew(s.id, s.title, s.description, s.price, s.is_enabled));
                    }
                }
            },
            initialize: function () {
                this._super();
                this.init();
            }
        });
    }
);