/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        "prototype",
        "varien/form",
        'custom-ko'
    ], function ($, $t, ko, Component) {
        'use strict';

        var route = '', position = '';
        var place = '';

        function initAutocomplete() {
            // Create the address object, restricting the search to geographical
            // location types.
            route = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
                {types: ['geocode']});
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            route.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the address object.
            place = route.getPlace();
            position = place.geometry.location;
            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            initMap();
        }

        //Set up some of our variables.
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker?

        //Function called to initialize / create the map.
        //This is called when the page has loaded.
        function initMap() {
            if (!position) {
                position = new google.maps.LatLng(33.89340415905593, 35.50159554000834);
            }
            //Map options.
            var options = {
                center: position, //Set center.
                zoom: 12 //The zoom value.
            };

            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);

            //Init marker
            marker = new google.maps.Marker({
                position: position,
                map: map,
                draggable: true //make it draggable
            });

            markerLocation();

            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function (event) {
                var clickedLocation = event.latLng;
                marker.setPosition(clickedLocation);
                // circle.setCenter(clickedLocation);
                markerLocation();
            });

            marker.addListener('drag', function (event) {
                markerLocation();
            });
            marker.addListener('dragend', function (event) {
                markerLocation();
            });
        }

        //This function will get the marker's current location and then add the lat/long
        //values to our textfields so that we can save the location.
        function markerLocation() {
            //Get location.
            var currentLocation = marker.getPosition();
            //Add lat and lng values to a field that we can save.
            if (currentLocation !== undefined) {
                document.getElementById('lat').value = currentLocation.lat(); //latitude
                document.getElementById('lng').value = currentLocation.lng(); //longitude
            }
        }

        // google.maps.event.addDomListener(window, 'load', initMap);
        ko.bindingHandlers.ggmap = {
            init: initMap
        };

        ko.bindingHandlers.ggmapAddress = {
            init: initAutocomplete
        };

        ko.bindingHandlers.timePicker = {
            init: function (e) {
                $(e).timepicker({timeFormat: 'hh:mm TT', showButtonPanel: false});
            }
        };

        // ko.bindingHandlers.foreachprop = {
        //     transformObject: function (obj) {
        //         var properties = [];
        //         ko.utils.objectForEach(obj, function (key, value) {
        //             properties.push({key: key, value: value});
        //         });
        //         return properties;
        //     },
        //     init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        //         var properties = ko.pureComputed(function () {
        //             var obj = ko.utils.unwrapObservable(valueAccessor());
        //             return ko.bindingHandlers.foreachprop.transformObject(obj);
        //         });
        //         ko.applyBindingsToNode(element, {foreach: properties}, bindingContext);
        //         return {controlsDescendantBindings: true};
        //     }
        // };

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/steps/one',
                title: $t('Hotel Property'),
                time: {
                    checkinFrom: ko.observable(),
                    checkinTo: ko.observable(),
                    checkoutFrom: ko.observable(),
                    checkoutTo: ko.observable()
                },
                extra: {
                    isInternetAvailable: ko.observable(),
                    isParkingAvailable: ko.observable(),
                    isBreakfastAvailable: ko.observable(),
                    canAccommodate: ko.observable(),
                    isPetAllowed: ko.observable()
                },
                languageOptions: {
                    list: ko.observableArray(),
                    selected: ko.observableArray()
                },
                checkedFacil: ko.observableArray(),
                photos: ko.observableArray(),
                toRmPhotos: ko.observableArray(),
                terms: ko.observable(),
                tax: {
                    isEnabled: ko.observable(0),
                    amount: ko.observable(),
                    type: ko.observable()
                },
                location: {
                    addr: ko.observable(),
                    longitude: ko.observable(),
                    latitude: ko.observable()
                }
            },
            errorMessage: ko.observable(''),
            yesnoOptions: ko.observableArray([
                {text: 'Yes', value: 1},
                {text: 'No', value: 0}
            ]),
            yesnoPaidOptions: ko.observableArray([
                {text: 'No', value: 0},
                {text: 'Yes, free', value: 1},
                {text: 'Yes, paid', value: 2}
            ]),
            yesnoUponOptions: ko.observableArray([
                {text: 'Yes, free', value: 1},
                {text: 'No', value: 0},
                {text: 'Upon Request', value: 2}
            ]),
            taxOptions: ko.observableArray([
                {text: '$ / Stay', value: 1},
                {text: '$ / Night', value: 2},
                {text: '%', value: 3},
                {text: '$ / Person / Stay', value: 4}
            ]),
            initialize: function () {
                this._super();
                this.initLanguage();
                this.initData(this.hotelProperties);
                this.initFacilities(this.hotelFacilities);
                this.initMapComponent(this.location);
            },
            initMapComponent: function (coor) {
                if (coor.longitude() && coor.latitude()) {
                    position = new google.maps.LatLng(coor.latitude(), coor.longitude());
                    // initMap();
                }
            },
            initObservable: function () {
                this._super();
                return this;
            },
            initData: function (d) {
                if (d.vendor_id) {
                    this.time.checkinFrom(d.check_in_from);
                    this.time.checkinTo(d.check_in_to);
                    this.time.checkoutFrom(d.check_out_from);
                    this.time.checkoutTo(d.check_out_to);
                    this.extra.isInternetAvailable(d.is_internet);
                    this.extra.isParkingAvailable(d.is_parking);
                    this.extra.isBreakfastAvailable(d.is_breakfast);
                    this.extra.canAccommodate(d.is_children_allowed);
                    this.extra.isPetAllowed(d.is_pet_allowed);
                    if (d.languages === "") {
                        d.languages = '[]';
                    }
                    this.languageOptions.selected(JSON.parse(d.languages));
                    if (d.photos === "") {
                        d.photos = '[]';
                    }
                    this.photos(JSON.parse(d.photos));
                    this.terms(d.terms);
                    this.tax.isEnabled(d.is_tax === '1');
                    this.tax.amount(d.tax_amount);
                    this.tax.type(d.tax_type);
                    this.location.addr(d.location);
                    this.location.longitude(d.longitude);
                    this.location.latitude(d.latitude);
                    // var interval = setInterval(function () {
                    //     if ($('input#autocomplete', $('iframe#location-picker-iframe').contents()).length === 1) {
                    //         $('input#autocomplete', $('iframe#location-picker-iframe').contents()).val(d.location);
                    //         clearInterval(interval);
                    //     }
                    // }, 500);
                }
            },
            initLanguage: function () {
                this.languageOptions.list(this.languages);
            },
            initFacilities: function (data) {
                for (var d in data) {
                    for (var i = 0; i < data[d].length; i++) {
                        var facil = data[d][i];
                        if (facil === undefined || !facil.hasOwnProperty('is_enable') || !facil.is_enable) {
                            continue;
                        }
                        this.checkedFacil.push(facil.id);
                    }
                }
                this.facilities = data;
            },
            isFacilChecked: function (id) {
                return this.checkedFacil.indexOf(id) !== -1;
            },
            addLanguage: function () {
                this.languageOptions.selected.push(ko.observable(''));
            },
            rmLanguage: function (i) {
                this.languageOptions.selected.splice(i, 1);
            },
            facilClick: function (id) {
                var isCheck = $('div#facilities input#' + id).is(':checked');
                if (isCheck) {
                    this.checkedFacil.push(id);
                } else {
                    this.checkedFacil.remove(id);
                }
                return true;
            },
            nextStep: function () {
                var self = this;
                var parent = this.containers[0];
                var form = new VarienForm('step-one-form', false);
                // var location = $('input#autocomplete', $('iframe#location-picker-iframe').contents()).val();
                // check img property
                if (!(this.photos() instanceof Array) || (this.photos().length < 1)) {
                    $('div#advice-required-entry-img-selector').hide();
                    $('<div/>', {
                        id: 'advice-required-entry-img-selector',
                        class: 'validation-advice',
                        text: $t('This is a required field.')
                    }).appendTo('form#property-img-form');
                    form.validator.validate();
                    return;
                }
                if (form.validator && form.validator.validate()) {
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/property/save',
                        method: 'POST',
                        dataType: 'json',
                        data: self.gatherData(),
                        showLoader: true,
                        success: function (r) {
                            if (parent !== undefined) {
                                if (r.success === true) {
                                    parent.nextStep();
                                } else {
                                    parent.addError(r.message);
                                }
                            } else {
                                self.addMessage(r.success ? $t('Saved') : r.message, !r.success);
                            }
                        },
                        error: function (r) {
                            if (parent !== undefined) {
                                parent.addError(r);
                            } else {
                                self.addMessage(r, false);
                            }
                        }
                    });
                }
            },
            addMessage: function (message, isError) {
                var self = this;
                if (isError !== true) {
                    var savedMessageElement = $('div#saved-message');
                    savedMessageElement.css("display", "inline-block");
                    setTimeout(function () {
                        savedMessageElement.fadeOut();
                    }, 4000);
                } else {
                    self.errorMessage(message);
                    setTimeout(function () {
                        self.errorMessage('');
                    }, 4000);
                }
            },
            gatherData: function () {
                var self = this;
                return {
                    time: {
                        checkinFrom: self.time.checkinFrom(),
                        checkinTo: self.time.checkinTo(),
                        checkoutFrom: self.time.checkoutFrom(),
                        checkoutTo: self.time.checkoutTo()
                    },
                    extra: {
                        isInternetAvailable: self.extra.isInternetAvailable(),
                        isParkingAvailable: self.extra.isParkingAvailable(),
                        isBreakfastAvailable: self.extra.isBreakfastAvailable(),
                        canAccommodate: self.extra.canAccommodate(),
                        isPetAllowed: self.extra.isPetAllowed()
                    },
                    languages: self.languageOptions.selected(),
                    facilities: self.checkedFacil(),
                    photos: self.photos(),
                    toRmPhotos: self.toRmPhotos(),
                    terms: self.terms(),
                    location: $('input#address').val(),
                    coordinate: {
                        longitude: $('input#lng').val(),
                        latitude: $('input#lat').val()
                    },
                    tax: {
                        isEnabled: self.tax.isEnabled(),
                        amount: self.tax.amount(),
                        type: self.tax.type()
                    }
                };
            },
            // getFacilitiesChecked: function () {
            //     var self = this;
            //     var data = [];
            //     console.log(self.hotelRooms());
            //     for (var i = 0; i < self.hotelRooms().length; i++) {
            //         console.log(self.hotelRooms()[i]);
            //         for (var j = 0; j < self.hotelRooms()[i].roomFacilities().length; j++) {
            //             console.log(self.hotelRooms()[i].roomFacilities()[j]);
            //             if (self.hotelRooms()[i].roomFacilities()[j].checked() == true)
            //                 data.push(self.hotelRooms()[i].roomFacilities()[j].id());
            //         }
            //     }
            //     console.log(data);
            // },
            uploadImage: function (u, e) {
                var parent = this.containers[0];
                var input = e.target;
                var formData = new FormData(input.form);
                formData.append('file', input.files[0]);
                $.ajax({
                    url: ORIG_BASE_URL + 'hotel/property/upload',
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    showLoader: true,
                    success: function (r) {
                        if (r.error === 0) {
                            u.photos.push(r.url);
                            $('div#advice-required-entry-img-selector').hide();
                        } else {
                            parent.addError(r.error);
                        }
                    },
                    error: function (r) {
                        parent.addError(r);
                    }
                });
            },
            rmImg: function (i) {
                var toRm = this.photos.splice(i, 1);
                this.toRmPhotos.push(toRm);
            }
        });
    }
);
