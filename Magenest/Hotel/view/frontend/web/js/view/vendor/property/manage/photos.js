/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'mage/translate',
        'jquery',
        'ko',
        'uiComponent',
        'custom-ko'
    ], function ($t, $, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/property/manage/photos',
                title: $t('Photos')
            },
            currentData: [],
            initialize: function () {
                this._super();
            },
            initObservable: function () {
                this._super().observe([
                    'photos'
                ]);
                return this;
            },
            onChange: function () {
                this.saveGallery();
            },
            gatherData: function () {
                return $('#hotel-images-list').sortable('toArray', {'attribute': 'data-value'});
            },
            saveGallery: function () {
                $.ajax({
                    url: this.gallerySaveUrl,
                    method: 'POST',
                    data: {'data': this.gatherData()},
                    showLoader: true
                }).done(function () {
                }).fail(function (r) {
                });
            },
            uploadImage: function (u, e) {
                var parent = this.containers[0],
                    input = e.target,
                    formData = new FormData(input.form);
                formData.append('file', input.files[0]);
                $.ajax({
                    url: this.galleryUploadUrl,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    showLoader: true,
                    success: function (r) {
                        if (r.error === 0) {
                            u.photos.push(r.url);
                            this.saveGallery();
                        } else {
                            parent.addError(r.error);
                        }
                    }.bind(this),
                    error: function (r) {
                        parent.addError(r);
                    }
                });
            },
            rmImg: function (i) {
                this.photos.splice(i, 1);
                this.saveGallery();
            }
        });
    }
);
