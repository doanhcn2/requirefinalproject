/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry',
        'Magenest_Hotel/js/model/hotel/service-repo',
        'mage/calendar',
        'prototype',
        'Magenest_Hotel/js/model/calendar/calendarview'
    ], function ($, $t, ko, Component, registry, serviceRepo) {
        'use strict';

        ko.bindingHandlers.mmCalendar = {
            init: function (e, v) {
                $(e).calendar({
                    minDate: 0,
                    dateFormat: "yyyy-mm-dd"
                });
            }
        };

        var selectHandler = function (calendar, value) {
            var selectedDate = registry.get('custom_calendar_date');
            if (selectedDate !== undefined) {
                selectedDate(value);
            }
        };

        ko.bindingHandlers.customCalendarBinding = {
            init: function (e, v) {
                function setupCalendars() {
                    // Embedded Calendar
                    Calendar.setup(
                        {
                            // dateField: 'customDateField',
                            parentElement: 'customCalendar',
                            selectHandler: selectHandler
                        }
                    );
                }

                setupCalendars();
            }
        };

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/steps/five',
                title: $t('Packages & Offers'),
                serviceRepo: serviceRepo(),
                websitesArray: ko.observableArray(),
                selectedWebsites: ko.observableArray(),
                /* a package = { date: {prices: amt}, otherdate: {otherprices: otheramt},... } */
                currentPackage: ko.observable({}),
                selectedDate: ko.observable(),
                currentSalePrice: ko.observable(),
                currentOrigPrice: ko.observable(),
                currentRoomQty: ko.observable(),
                errorPrice: ko.observable(false),
                originalRequired: ko.observable(false),
                qtyRequired: ko.observable(false),
                savedPrice: ko.observable(false),
                qty: ko.observable(),
                vendorSku: ko.observable(),
                currency: ko.observable(""),
                bulkStartDate: ko.observable(),
                bulkEndDate: ko.observable()
            },
            cancellationPolicyOptions: ko.observableArray([
                {text: $t('Day of Arrival'), value: 'Day of Arrival'},
                {text: $t('1 Day'), value: '1 Day'},
                {text: $t('2 Days'), value: '2 Days'},
                {text: $t('3 Days'), value: '3 Days'},
                {text: $t('7 Days'), value: '7 Days'},
                {text: $t('14 Days'), value: '14 Days'}
            ]),
            minTimeToBookOptions: ko.observableArray([
                {text: $t('Same Day'), value: 'Same Day'},
                {text: $t('1 Day'), value: '1 Day'},
                {text: $t('2 Days'), value: '2 Days'},
                {text: $t('3 Days'), value: '3 Days'},
                {text: $t('7 Days'), value: '7 Days'},
                {text: $t('14 Days'), value: '14 Days'}
            ]),
            penaltyOptions: ko.observableArray([
                {text: $t('100% of first Night'), value: '100% of first Night'},
                {text: $t('100% of Whole Stay'), value: '100% of Whole Stay'}
            ]),
            initObservable: function () {
                this._super().observe([
                    'selectedDate',
                    'optionsRoomType',
                    'currency',
                    'offerTitle',
                    'duplicateName',
                    'vendorSku',
                    'currentPackage',
                    'qty',
                    'room_type',
                    'book_from',
                    'book_to',
                    'min_nights',
                    'max_nights',
                    'cancellation_policy',
                    'penalty',
                    'min_time_to_book',
                    'is_published_asap',
                    'publish_date',
                    'is_unpublished_alap',
                    'unpublish_date',
                    'special_terms'
                ]);
                return this;
            },
            initialize: function () {
                var self = this;
                this._super();
                this.websitesArray(this.websites);
                this.initToday();
                this.updateFields();
                this.validateObserver();
                this.selectedDate.subscribe(function (newValue) {
                    self.bulkStartDate(newValue);
                    self.bulkEndDate(newValue);
                    self.updateFields();
                });
                self.min_nights.subscribe(function (value) {
                    if (parseInt(value) > parseInt(self.max_nights())) {
                        self.max_nights("");
                    }
                });
                self.max_nights.subscribe(function (value) {
                    if (parseInt(value) < parseInt(self.min_nights())) {
                        self.min_nights("");
                    }
                });
                self.book_from.subscribe(function (value) {
                    if (new Date(value) > new Date(self.book_to())) {
                        self.book_to("");
                    }
                    self.bulkStartDate(value);
                });
                self.book_to.subscribe(function (value) {
                    if (new Date(value) < new Date(self.book_from())) {
                        self.book_from("");
                    }
                    self.bulkEndDate(value);
                });
                self.bulkStartDate.subscribe(function (value) {
                    if (new Date(value) > new Date(self.bulkEndDate())) {
                        self.bulkEndDate("");
                    }
                });
                self.bulkEndDate.subscribe(function (value) {
                    if (new Date(value) < new Date(self.bulkStartDate())) {
                        self.bulkStartDate("");
                    }
                });
                self.offerTitle.subscribe(function (value) {
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/vendor/checkName',
                        method: 'POST',
                        data: {"name": value, "product_id": PRODUCT_ID_HOTEL},
                        success: function (r) {
                            if (r.success === true) {
                                self.duplicateName(false);
                            } else {
                                self.duplicateName(true);
                            }
                        },
                        error: function (r) {
                            parent.addError(r);
                        }
                    });
                });
                // setInterval(function () {
                //     console.log(self.room_type());
                // }, 2000);
            },
            validateObserver: function () {

            },
            updateFields: function () {
                if (this.currentPackage() && this.currentPackage()[this.selectedDate()]) {
                    this.currentSalePrice(this.currentPackage()[this.selectedDate()].salePrice);
                    this.currentOrigPrice(this.currentPackage()[this.selectedDate()].origPrice);
                    this.currentRoomQty(this.currentPackage()[this.selectedDate()].roomQty);
                } else {
                    this.currentSalePrice('');
                    this.currentOrigPrice('');
                    this.currentRoomQty('');
                }
            },
            initToday: function () {
                var today = new Date();
                today = this.dateToStr(today);
                this.selectedDate(today);
                this.bulkStartDate(today);
                this.bulkEndDate(today);
                registry.set('custom_calendar_date', this.selectedDate);
            },
            getCalendarTemplate: function () {
                return 'Magenest_Hotel/vendor/calendar';
            },
            addOffer: function () {
                var parent = this.containers[0];
                var self = this;
                var origform = new VarienForm('product-edit-form', true);
                if (origform.validator && origform.validator.validate()) {
                    var form = new VarienForm('package-details', true);
                    if (form.validator && form.validator.validate()) {
                        if (self.duplicateName())
                            $("#hotel-offer-title").focusin();
                        else {
                            var data = {};
                            $('div[data-role="image"] input').each(function (e, a) {
                                // console.log($(a).attr('name'));
                                data[$(a).attr('name')] = $(a).val();
                            });
                            $.ajax({
                                url: ORIG_BASE_URL + 'hotel/package/save',
                                method: 'POST',
                                dataType: 'json',
                                data: Object.assign({
                                    data: self.gatherData(),
                                    set_id: '4-HotelBooking', //to _initProduct()
                                    id: $('input[name="id"]').val(),
                                    'product[udmulti][stock_qty]': self.qty()
                                }, data),
                                showLoader: true,
                                success: function (r) {
                                    if (r.success === true) {
                                        console.log(self.sortOrder);
                                        if (self.sortOrder === 4) {
                                            console.log(ORIG_BASE_URL);
                                            window.location = ORIG_BASE_URL + 'register/vendor/registerfinal/'
                                        } else {
                                            window.location = BASE_URL + 'vendor/products/';
                                        }
                                    } else {
                                        parent.addError(r.message);
                                    }
                                },
                                error: function (r) {
                                    parent.addError(r);
                                }
                            });
                        }
                    }
                }
            },
            dateToStr: function (date) {
                return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
            },
            setPrice: function (dateStr, salePrice, origPrice, roomQty) {
                this.currentPackage()[dateStr] = {
                    salePrice: salePrice,
                    origPrice: origPrice,
                    roomQty: roomQty
                }
            },
            reloadRoomType: function () {
                var self = this;
                var room_types = registry.get('hotel_room_type');
                self.optionsRoomType(room_types);
            },
            getTemplate: function () {
                // to re-init service repo multiple times, after come from step 4
                this.serviceRepo.init();
                this.reloadRoomType();

                return this._super();
            },
            gatherData: function () {
                var self = this;
                var services = [];
                var allServices = self.serviceRepo.gatherData();
                for (var i = 0; i < allServices.length; i++) {
                    if (allServices[i].isEnabled) {
                        services.push(allServices[i].index);
                    }
                }
                return {
                    offerTitle: self.offerTitle(), //product name
                    vendorSku: self.vendorSku(), //product sku
                    roomQty: 9999, //self.qty(), // product qty
                    roomType: self.room_type(),
                    services: services, //services
                    bookFrom: self.book_from(),
                    bookTo: self.book_to(),
                    minNights: self.min_nights(),
                    maxNights: self.max_nights(),
                    cancellationPolicy: self.cancellation_policy(),
                    penalty: self.penalty(),
                    package: self.currentPackage(), //package
                    minTimeToBook: self.min_time_to_book(),
                    isPublishedAsap: self.is_published_asap(),
                    publishedDate: self.publish_date(),
                    isUnpublishedAlap: self.is_unpublished_alap(),
                    unpublishedDate: self.unpublish_date(),
                    specialTerms: self.special_terms(),
                    categories: $('#product_categories').val(),
                    websites: $('#product_websites').val()
                };
            },
            saveCurrent: function () {
                var self = this;
                var form = new VarienForm('current-date-form', true);
                if (form.validator && form.validator.validate()) {
                    var start = new Date(self.bulkStartDate());
                    var end = new Date(self.bulkEndDate());
                    var dateRange = end - start;
                    if (dateRange < 0) {
                        return;
                    }
                    var errorRequired = false;
                    if (self.currentOrigPrice() == "") {
                        self.originalRequired(true);
                        errorRequired = true;
                    } else {
                        errorRequired = false;
                        self.originalRequired(false);
                    }

                    if (self.currentRoomQty() == "") {
                        self.qtyRequired(true);
                        errorRequired = true;
                    } else {
                        errorRequired = false;
                        self.qtyRequired(false);
                    }
                    if (errorRequired) return;
                    if (parseFloat(self.currentSalePrice()) > 0 && parseFloat(self.currentOrigPrice()) <= parseFloat(self.currentSalePrice())) {
                        self.errorPrice(true);
                        return;
                    } else {
                        self.errorPrice(false);
                    }
                    // if (dateRange === 0) {
                    //     self.setPrice(self.selectedDate(), self.currentSalePrice(), self.currentOrigPrice());
                    //     return;
                    // }
                    while (dateRange >= 0) {
                        var currDate = start;
                        self.setPrice(self.dateToStr(currDate), self.currentSalePrice(), self.currentOrigPrice(), self.currentRoomQty());
                        currDate.setDate(currDate.getDate() + 1);
                        dateRange = end - currDate;
                    }
                    // self.savedPrice(true);
                    var savedMessageElement = $('div#saved-message');
                    savedMessageElement.hide().fadeIn();
                    setTimeout(function () {
                        savedMessageElement.fadeOut();
                    }, 4000);
                }
            }
        });
    }
)
;
