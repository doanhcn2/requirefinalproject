define([
        'mage/translate',
        'jquery',
        'ko',
        'abstractGrid'
    ], function ($t, $, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/property/manage/service',
                title: $t('Property Services')
            },
            items: ko.observableArray([]),
            allItems: {},
            total: ko.observable(),
            startNumber: ko.observable(),
            endNumber: ko.observable(),
            pageText: '',
            isCheckedAll: ko.observable(false),
            defaultChecked: false,
            checkedArr: [],
            unCheckArr: [],
            exportCSV: function () {
                var self = this;
                var checked = null;
                var except = null;
                if (self.getChecked().type == "checkedAll") {
                    checked = "all";
                    if (self.getChecked().exception.length == 0)
                        except = 0;
                    else
                        except = JSON.stringify(self.getChecked().exception);
                }
                else {
                    if (self.getChecked().exception.length == 0) {
                        alert("You haven't selected any items!");
                        return;
                    } else {
                        checked = self.getChecked().exception;
                        checked = JSON.stringify(checked);
                    }
                }
                var href = ORIG_BASE_URL + 'hotel/vendor_grid/printServices';
                window.location = href + '/chosen/' + checked + '/except/' + except;
            }
        });
    }
);
