/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($, $t, ko, Component) {
        'use strict';

        function RoomType(id, title) {
            var self = this;
            self.id = id;
            self.roomTypeSelected = ko.observable();
            self.roomTitle = title;
        }
        function amenity(id, name, room_types, checked) {
            var self = this;
            self.id = id;
            self.amenityName = name;
            self.selectedChoice = null;
            self.checkAll = ko.observable();
            self.someRoom = ko.observable();
            self.allRoomIds = ko.observableArray();
            self.roomTypes = ko.observableArray();
            for (var i = 0; i < room_types.length; i++) {
                self.roomTypes.push(
                    new RoomType(room_types[i].id, room_types[i].roomTitle)
                );
                self.allRoomIds.push(
                    room_types[i].id
                );

            }
            self.checkAll.subscribe(function (value) {
                if (value == true) {
                    self.someRoom(false);
                    self.selectedChoice = self.allRoomIds();
                } else
                    self.selectedChoice = null;
            });
            self.someRoom.subscribe(function (value) {
                if (value == true) {
                    self.checkAll(false);
                    self.selectedChoice = null;
                }

            });
            var lookup = {};
            for (var i = 0; i < self.roomTypes().length; i++) {
                lookup[self.roomTypes()[i].id] = self.roomTypes()[i];
            }
            if (checked != undefined) {
                if (checked.length == self.allRoomIds().length)
                    self.checkAll(true);
                else {
                    self.someRoom(true);
                    for (var j = 0; j < checked.length; j++) {
                        if (lookup[checked[j]] === undefined) {
                            continue;
                        }
                        lookup[checked[j]].roomTypeSelected(true);
                    }
                }
            }
        }

        function roomKind(kind, amenities, room_types) {
            var self = this;
            self.kind = kind;
            self.amenities = ko.observableArray();
            for (var i in amenities) {
                self.amenities.push(
                    new amenity(i, amenities[i].name, room_types, amenities[i].checked)
                );
            }
        }

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/property/manage/amenities',
                title: $t('Room Amenities')
            },
            reloadRoomType: function () {
                var self = this;
                var data = self.room_amenities_admin;
                self.roomKind = ko.observableArray();
                var room_types = self.room_type_vendor;
                for (var i in data) {
                    self.roomKind.push(
                        new roomKind(i, data[i], room_types)
                    );
                }
            },
            getTemplate: function () {
                this.reloadRoomType();
                return this._super();
            },
            save: function () {
                var self = this;
                var parent = this.containers[0];
                $.ajax({
                    url: ORIG_BASE_URL + 'hotel/amenity/save',
                    method: 'POST',
                    dataType: 'json',
                    data: self.gatherData(),
                    showLoader: true,
                    success: function (r) {
                        if (r.success && r.success === true) {
                            parent.addSuccess("Done.");
                        } else if (r.message) {
                            parent.addError(r.message);
                        } else {
                            parent.addError($t("Unknown Error"));
                            console.log(r);
                        }
                    },
                    error: function (r) {
                        parent.addError(r);
                    }
                });
            },
            gatherData: function () {
                var self = this;
                var data = {};
                var roomKinds = self.roomKind();
                for (var i = 0; i < roomKinds.length; i++) {
                    for (var j = 0; j < roomKinds[i].amenities().length; j++) {
                        if (roomKinds[i].amenities()[j].selectedChoice != null) {
                            data[roomKinds[i].amenities()[j].id] = roomKinds[i].amenities()[j].allRoomIds();
                        } else {
                            if (roomKinds[i].amenities()[j].someRoom() == true) {
                                data[roomKinds[i].amenities()[j].id] = [];
                                for (var k = 0; k < roomKinds[i].amenities()[j].roomTypes().length; k++) {
                                    if (roomKinds[i].amenities()[j].roomTypes()[k].roomTypeSelected() == true) {
                                        data[roomKinds[i].amenities()[j].id].push(roomKinds[i].amenities()[j].roomTypes()[k].id);
                                    }
                                }
                            }
                        }
                    }
                }
                return data;
            }

        });
    }
);