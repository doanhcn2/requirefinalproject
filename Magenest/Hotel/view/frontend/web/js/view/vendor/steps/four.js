/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'Magenest_Hotel/js/model/hotel/service-repo',
        'uiRegistry'
    ], function ($, $t, ko, Component, serviceRepo, registry) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/steps/four',
                title: $t('Property Services'),
                currentService: serviceRepo().getNew(),
                serviceRepo: serviceRepo()
            },
            initialize: function () {
                this._super();
            },
            addService: function () {
                var self = this;
                var form = new VarienForm('step-four-form', true);
                if (form.validator && form.validator.validate()) {
                    // save current service to repo
                    self.serviceRepo.save(self.serviceRepo.getNew(
                        0, // id: 0 -> create new record in table
                        this.currentService.title(),
                        this.currentService.description(),
                        this.currentService.price(),
                        this.currentService.isEnabled()
                    ));
                    // clear input data
                    this.currentService.title('');
                    this.currentService.description('');
                    this.currentService.price('');
                    this.currentService.isEnabled(true);
                }
            },
            rmService: function (e) {
                this.serviceRepo.remove(e);
            },
            nextStep: function () {
                var parent = this.containers[0];
                var self = this;
                // self.addService();
                var form = new VarienForm('services-all', true);
                if (form.validator && form.validator.validate()) {
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/vendor_service/save',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            items: self.serviceRepo.gatherData(),
                            remove: self.serviceRepo.toRemove
                        },
                        showLoader: true,
                        success: function (r) {
                            if (r.success === true) {
                                registry.set('old_hotel_services', self.serviceRepo.services);
                                registry.set('hotel_services', r.items);
                                self.serviceRepo.init();
                                parent.nextStep();
                            } else {
                                parent.addError(r.message);
                            }
                        },
                        error: function (r) {
                            parent.addError(r);
                        }
                    });
                }
            }
        });
    }
);
