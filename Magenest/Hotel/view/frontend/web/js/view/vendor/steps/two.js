/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry'
    ], function ($, $t, ko, Component, registry) {
        'use strict';

        function Bed(bedType, number) {
            var self = this;
            self.bedType = ko.observable(bedType);
            // self.optionsBedType= ko.observableArray(optionsBedType);
            self.numberBeds = ko.observable(number);
            self.toArrayBeds = function () {
                return {
                    bedId: self.bedType(),
                    numberBeds: self.numberBeds()
                };
            };

        }

        function Room(id,
                      roomTitle,
                      numberRooms,
                      smokingPolicy,
                      description,
                      beds,
                      numberGuests,
                      roomSize,
                      hasExtraBeds,
                      numberExtraBeds,
                      priceBed,
                      roomType,
                      photos) {
            var self = this;
            self.titleRoomTypeForm = ko.observable();
            // self.bedType = ko.observableArray(beds);
            self.roomType = ko.observable(roomType);
            self.numberRooms = ko.observable(numberRooms);
            self.smoking = ko.observable(smokingPolicy);
            self.beds = beds;
            self.roomDescription = ko.observable(description);
            self.smoking = ko.observable(smokingPolicy);
            self.numberGuests = ko.observable(numberGuests);
            self.roomSize = ko.observable(roomSize);
            self.hasExtraBeds = ko.observable(hasExtraBeds);
            self.numberExtraBeds = ko.observable(numberExtraBeds);
            self.priceBed = ko.observable(priceBed);
            self.id = ko.observable(id);
            self.roomTitleList = ko.observable(roomTitle);
            self.roomNumberList = ko.observable(numberRooms);
            self.photos = photos;

        }

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/steps/two',
                title: $t('Room types')
                // roomTypeSelect: ko.observableArray(this.room_type_admin)
            },
            initialize: function () {
                this._super();
                var self = this;
                self.optionsRoomType = ko.observableArray(self.room_type_admin);
                self.optionsBedType = ko.observableArray(self.bed_type_admin);
                self.beds = ko.observableArray(
                    [new Bed(null, null)]
                );
                self.photos = ko.observableArray();
                self.addBed = function () {
                    self.beds.push(
                        new Bed(null, null)
                    );
                };
                self.rmImg = function (i) {
                    self.photos.splice(i, 1);
                };
                self.rooms = ko.observableArray();
                var roomCreated = self.room_type_vendor;
                if (roomCreated.length > 0) {
                    self.reloadRoomsList(roomCreated);
                }
                self.removeBed = function (bed) {
                    self.beds.remove(bed);
                };
            },

            reloadRoomsList: function (array) {
                var self = this;
                self.rooms.removeAll();
                for (var i = 0; i < array.length; i++) {
                    array[i]['beds'] = JSON.parse(array[i]['beds']);
                    self.rooms.push(
                        new Room(
                            array[i]["id"],
                            array[i]["roomTitle"],
                            array[i]["numberRooms"],
                            array[i]["smoking"],
                            array[i]['description'],
                            array[i]['beds'],
                            array[i]['numberGuests'],
                            array[i]['roomSize'],
                            array[i]['hasExtraBeds'],
                            array[i]['numberExtraBeds'],
                            array[i]['priceBed'],
                            array[i]['roomType'],
                            array[i]['photos']
                        )
                    );
                }
                registry.set('hotel_room_type', array);
                registry.set('is_hotel_room_type_changed', true);
            },
            uploadImage: function (u, e) {
                var input = e.target;
                var formData = new FormData(input.form);
                formData.append('file', input.files[0]);
                $.ajax({
                    url: ORIG_BASE_URL + 'hotel/roomType/uploadImage',
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    showLoader: true,
                    success: function (r) {
                        if (r.error === 0) {
                            u.photos.push(r.url);
                            console.log(u.photos);
                            $('div#advice-required-entry-img-selector').hide();
                        } else {
                        }
                    },
                    error: function (r) {
                    }
                });
            },
            editRoom: function (roomEdit) {
                var self = this;
                for (var i = 0; i < self.rooms().length; i++) {
                    if (self.rooms()[i].id() == roomEdit.id()) {
                        self.beds.removeAll();
                        for (var j = 0; j < self.rooms()[i].beds.length; j++) {
                            self.beds.push(
                                new Bed(self.rooms()[i].beds[j].bedId, self.rooms()[i].beds[j].numberBeds)
                            );
                        }
                        self.photos.removeAll();

                        if (self.rooms()[i].photos!=null){
                            var photos = JSON.parse(self.rooms()[i].photos);
                            for (var k = 0; k < photos.length; k++) {
                                self.photos.push(
                                    photos[k]
                                );
                            }
                        }

                        self.roomId(self.rooms()[i].id());
                        // console.log(self.rooms()[i].roomType());
                        self.roomType(self.rooms()[i].roomType());
                        self.numberRooms(self.rooms()[i].numberRooms());
                        self.roomTitle(self.rooms()[i].roomTitleList());
                        self.smoking(self.rooms()[i].smoking());
                        self.roomDescription(self.rooms()[i].roomDescription());
                        self.numberGuests(self.rooms()[i].numberGuests() === '0' ? null : self.rooms()[i].numberGuests());
                        self.roomSize(self.rooms()[i].roomSize() === '0' ? null : self.rooms()[i].roomSize());
                        self.hasExtraBeds(self.rooms()[i].hasExtraBeds() == '0' ? false : true);
                        self.numberExtraBeds(self.rooms()[i].numberExtraBeds());
                        self.priceBed(self.rooms()[i].priceBed());
                        self.titleRoomTypeForm($t("Edit Room Type"));
                        break;
                    }
                }
            },
            addNewRoom: function () {
                var self = this;
                self.saveRoomType();
                self.roomId(null);
                self.titleRoomTypeForm("Add Another Room Type");
                self.roomType(null);
                self.bedType(null);
                self.beds.removeAll();
                self.beds.push(
                    new Bed()
                );
                self.numberBeds(null);
                self.numberGuests(null);
                self.roomSize(null);
                self.numberRooms(null);
                self.hasExtraBeds(false);
                self.numberExtraBeds(null);
                self.priceBed(null);
                self.roomTitle(null);
                self.smoking(false);
                self.roomDescription(null);
                self.photos.removeAll();
            },
            deleteRoom: function (room) {
                var self = this;
                var parent = self.containers[0];
                if (window.confirm($t("Do you really want to delete %1?").replace('%1', room.roomTitleList()))) {
                    var roomToDelete = {
                        id: room.id()
                    };
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/roomType/delete',
                        method: 'POST',
                        dataType: 'json',
                        showLoader: true,
                        data: roomToDelete,
                        success: function (r) {
                            var list = JSON.parse(r.rooms);
                            if (list.length >= 0)
                                self.reloadRoomsList(list);
                        },
                        error: function (r) {
                            parent.addError(r);
                        }
                    });
                }
            },
            gatherData: function () {
                var self = this;
                var bedsVendor = [];
                for (var i = 0; i < self.beds().length; i++) {
                    bedsVendor.push(self.beds()[i].toArrayBeds());
                }
                if (bedsVendor.length === 0) {
                    var parent = self.containers[0];
                    parent.addError($t("Must add Bed for this room type first!"));
                    return false;
                }
                var photosRoom = [];
                for (var j = 0; j < self.photos().length; j++) {
                    photosRoom.push(self.photos()[j]);
                }
                return {
                    id: self.roomId(),
                    room_type_admin_id: self.roomType(),
                    number_rooms_available: self.numberRooms(),
                    room_title: self.roomTitle(),
                    non_smoking: self.smoking(),
                    description: self.roomDescription(),
                    beds_type: bedsVendor,
                    number_guests: self.numberGuests(),
                    room_size: self.roomSize(),
                    has_extra_beds: self.hasExtraBeds(),
                    number_extra_beds: self.numberExtraBeds(),
                    price_per_bed: self.priceBed(),
                    photo_room: JSON.stringify(photosRoom)
                };
            },
            saveRoomType: function () {
                var self = this;
                var parent = this.containers[0];
                var form = new VarienForm('step-two-form', true);
                if (form.validator && form.validator.validate() && self.gatherData() !== false) {
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/roomType/save',
                        method: 'POST',
                        dataType: 'json',
                        showLoader: true,
                        data: self.gatherData(),
                        success: function (r) {
                            var list = r.rooms;
                            if (list.length > 0) {
                                self.reloadRoomsList(list);
                            }
                            self.addNewRoom();
                        },
                        error: function (r) {
                            console.log(r);
                            parent.addError($t("Error occurs when saving."));
                        }
                    });
                }
            },
            nextStep: function () {
                if (this.rooms().length > 0) {
                    this.containers[0].nextStep();
                } else {
                    window.alert($t("Please add at least 1 room type"));
                }
            },
            previousStep: function () {
                this.containers[0].prevStep();

            },
            roomId: ko.observable(),
            titleRoomTypeForm: ko.observable(""),
            roomType: ko.observable(),
            bedType: ko.observable(),
            numberBeds: ko.observable(),
            numberGuests: ko.observable(),
            roomSize: ko.observable(),
            numberRooms: ko.observable(),
            hasExtraBeds: ko.observable(),
            numberExtraBeds: ko.observable(),
            priceBed: ko.observable(),
            roomTitle: ko.observable(),
            smoking: ko.observable(),
            optionsSmoking: ko.observableArray([
                {text: 'Non Smoking', value: 0},
                {text: 'Smoking', value: 1}
            ]),
            optionsBedType: ko.observableArray([
                {text: 'Non Smoking', value: 0},
                {text: 'Smoking', value: 1}
            ]),
            roomDescription: ko.observable()

        });
    }
);