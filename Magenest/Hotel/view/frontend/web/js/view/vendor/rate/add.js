/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'varien/form'
    ], function ($, $t, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/rate/add',
                title: $t('New Rate Category')
            },
            cancellationPolicyOptions: ko.observableArray([
                {text: $t('Day of Arrival'), value: 'Day of Arrival'},
                {text: $t('1 Day'), value: '1 Day'},
                {text: $t('2 Days'), value: '2 Days'},
                {text: $t('3 Days'), value: '3 Days'},
                {text: $t('7 Days'), value: '7 Days'},
                {text: $t('14 Days'), value: '14 Days'}
            ]),
            minTimeToBookOptions: ko.observableArray([
                {text: $t('Same Day'), value: 'Same Day'},
                {text: $t('1 Day'), value: '1 Day'},
                {text: $t('2 Days'), value: '2 Days'},
                {text: $t('3 Days'), value: '3 Days'},
                {text: $t('7 Days'), value: '7 Days'},
                {text: $t('14 Days'), value: '14 Days'}
            ]),
            penaltyOptions: ko.observableArray([
                {text: $t('100% of first Night'), value: '100% of first Night'},
                {text: $t('100% of Whole Stay'), value: '100% of Whole Stay'}
            ]),
            initObservable: function () {
                this._super().observe([
                    'roomTypes',
                    'services',
                    'offerTitle',
                    'duplicateName',
                    'invalidNight',
                    'min_nights',
                    'max_nights',
                    'cancellation_policy',
                    'penalty',
                    'min_time_to_book'
                ]);
                return this;
            },
            initialize: function () {
                var self = this;
                this._super();
                self.min_nights.subscribe(function (value) {
                    self.invalidNight(parseInt(value) > parseInt(self.max_nights()));
                });
                self.max_nights.subscribe(function (value) {
                    self.invalidNight(parseInt(value) < parseInt(self.min_nights()));
                });
                self.offerTitle.subscribe(function (value) {
                    $.ajax({
                        url: ORIG_BASE_URL + 'hotel/vendor/checkName',
                        method: 'POST',
                        showLoader: true,
                        data: {"name": value, "product_id": PRODUCT_ID_HOTEL},
                        success: function (r) {
                            if (r.success === true) {
                                self.duplicateName(false);
                            } else {
                                self.duplicateName(true);
                            }
                        },
                        error: function (r) {
                            parent.addError(r);
                        }
                    });
                });
            },
            addOffer: function () {
                var self = this,
                    parent = this.containers[0],
                    form = new VarienForm('package-details', true);
                if (form.validator && form.validator.validate()) {
                    if (self.duplicateName())
                        $("#hotel-offer-title").focusin();
                    else {
                        console.log(this.gatherData());
                        var data = {};
                        $('div[data-role="image"] input').each(function (e, a) {
                            // console.log($(a).attr('name'));
                            data[$(a).attr('name')] = $(a).val();
                        });
                        $.ajax({
                            url: ORIG_BASE_URL + 'hotel/package/save',
                            method: 'POST',
                            dataType: 'json',
                            data: Object.assign({
                                data: self.gatherData(),
                                set_id: '4-HotelBooking', //for _initProduct()
                                id: $('input[name="id"]').val()
                                // 'product[udmulti][stock_qty]': self.qty()
                            }, data),
                            showLoader: true,
                            success: function (r) {
                                if (r.success === true) {
                                    if (self.sortOrder === 4) {
                                        console.log(ORIG_BASE_URL);
                                        window.location = ORIG_BASE_URL + 'register/vendor/registerfinal/'
                                    } else {
                                        window.location = BASE_URL + 'vendor/products/';
                                    }
                                } else {
                                    parent.addError(r.message);
                                }
                            },
                            error: function (r) {
                                parent.addError(r);
                            }
                        });
                    }
                }
            },
            gatherData: function () {
                var i,
                    services = [],
                    roomTypes = [],
                    allServices = this.services(),
                    allRoomTypes = this.roomTypes();
                for (i = 0; i < allServices.length; i++) {
                    if (allServices[i].is_enabled) {
                        services.push(allServices[i].id);
                    }
                }
                for (i = 0; i < allRoomTypes.length; i++) {
                    if (allRoomTypes[i].is_enabled) {
                        roomTypes.push(allRoomTypes[i].id)
                    }
                }
                return {
                    offerTitle: this.offerTitle(), //product name
                    // vendorSku: this.vendorSku(), //product sku
                    // roomQty: 9999, //this.qty(), // product qty
                    roomType: roomTypes,
                    services: services, //services
                    minNights: this.min_nights(),
                    maxNights: this.max_nights(),
                    cancellationPolicy: this.cancellation_policy(),
                    penalty: this.penalty(),
                    // package: this.currentPackage(), //package
                    minTimeToBook: this.min_time_to_book()/*,*/
                    // isPublishedAsap: this.is_published_asap(),
                    // publishedDate: this.publish_date(),
                    // isUnpublishedAlap: this.is_unpublished_alap(),
                    // unpublishedDate: this.unpublish_date(),
                    // specialTerms: this.special_terms(),
                    // categories: $('#product_categories').val(),
                    // websites: $('#product_websites').val()
                };
            }
        });
    }
);
