/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($t, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/property/manage/basic-information',
                title: $t('Hotel Property')
            },
            initialize: function () {
                this._super();
            },
            initObservable: function () {
                this._super().observe([
                    // Propery Information
                    'property_name',
                    'property_type',
                    'website',
                    'rating',
                    // Address
                    'country',
                    'governorate',
                    'city',
                    'address',
                    'phone_number',
                    // Property Description
                    'property_description',
                    // Property Terms
                    'property_terms',
                    // Contact
                    'contact_name',
                    'position',
                    // 'phone_number',
                    'email'
                ]);
                return this;
            }
        });
    }
);
