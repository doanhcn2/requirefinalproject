/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'mage/translate',
        'jquery',
        'ko',
        'uiComponent',
        'custom-ko'
    ], function ($t, $, ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/property/manage/details',
                title: $t('Property Details')
            },
            checkInTime: '',
            checkOutTime: '',
            initialize: function () {
                this._super();
                this.initCheckInTime();
            },
            initCheckInTime: function () {
                var self = this;
                self.checkInTime = ko.pureComputed(function () {
                    if (self.check_in_to()) {
                        return $t('From %1 until %2')
                            .replace('%1', self.check_in_from())
                            .replace('%2', self.check_in_to());
                    } else {
                        return $t('From %1').replace('%1', self.check_in_from());
                    }
                });
                self.checkOutTime = ko.pureComputed(function () {
                    if (self.check_out_to()) {
                        return $t('From %1 until %2')
                            .replace('%1', self.check_out_from())
                            .replace('%2', self.check_out_to());
                    } else {
                        return $t('From %1').replace('%1', self.check_out_from());
                    }
                });
            },
            initObservable: function () {
                this._super().observe([
                    'check_in_from',
                    'check_in_to',
                    'check_out_from',
                    'check_out_to',
                    'facilities'
                ]);
                return this;
            }
        });
    }
);
