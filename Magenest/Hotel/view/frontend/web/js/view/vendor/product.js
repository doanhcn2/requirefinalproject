define([
        'ko',
        'uiComponent'
    ], function (ko, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/vendor/product',
                currentStep: ko.observable(0),
                errors: ko.observableArray()
            },
            initObservable: function () {
                this._super().observe('currentStep');
                return this;
            },
            addError: function (m) {
                this.errors.push({message: m});
            },
            nextStep: function (i) {
                this.currentStep(this.currentStep() + 1);
            },
            prevStep: function () {
                if (this.currentStep() > 0) {
                    this.currentStep(this.currentStep() - 1);
                }
            },
            moveToStep: function (i) {
                if (i < this.currentStep()) {
                    this.currentStep(i);
                }
            }
        });
    }
);
