/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry',
        'mage/calendar',
        'Magento_Customer/js/customer-data'
    ], function ($, $t, ko, Component, registry, calendar, customerData) {
        'use strict';

        ko.bindingHandlers.mmCalendar = {
            init: function (e, v) {
                $(e).calendar({
                    minDate: v().startDate,
                    maxDate: v().endDate,
                    dateFormat: "yyyy-mm-dd",
                    showWeek: false,
                    changeMonth: false,
                    changeYear: false
                });
            }
        };

        return Component.extend({
            defaults: {
                startDate: ko.observable(),
                endDate: ko.observable(),
                formatedTotal: ko.observable(),
                dateRanges: ko.observableArray(),
                roomQty: ko.observable(1),
                qtyLabel: ko.observable($t("Qty")),
                isValidDate: ko.observable(false),
                services: ko.observableArray(),
                serviceData: {},
                selectedServices: ko.observableArray(),
                totalPrice: 0,
                totalSelectedServicesPrice: 0,
                errorMessage: ko.observable('')
            },
            calendar: undefined,
            calendarData: {
                val: null,
                prevVal: null
            },
            initObservable: function () {
                this._super().observe('services');
                return this;
            },
            initialize: function () {
                this._super();
                this.init();
                this.calendarInit();
                // var self = this;
                // setInterval(function () {
                //     console.log(self.selectedServices());
                // }, 2000);
            },
            getService: function (id) {
                return this.serviceData[id];
            },
            calendarInit: function () {
                var self = this,
                    onSelectFunc = function (date) {
                        if (self.calendarData.prevVal !== null) {
                            self.calendarData.prevVal = null;
                        } else {
                            self.calendarData.prevVal = self.calendarData.val;
                        }
                        self.calendarData.val = date;

                        if (!self.calendarData.prevVal || !self.calendarData.val) {
                            return;
                        }
                        var val = new Date(self.calendarData.val),
                            prevVal = new Date(self.calendarData.prevVal),
                            flag = val > prevVal;
                        self.startDate(flag ? self.calendarData.prevVal : self.calendarData.val);
                        self.endDate(!flag ? self.calendarData.prevVal : self.calendarData.val);
                    },
                    beforeShowDayFunc = function (date) {
                        if (self.startDate() === undefined || self.endDate() === undefined) {
                            return [true, '', ''];
                        }
                        var startDate = new Date(self.startDate()),
                            endDate = new Date(self.endDate()),
                            current = new Date(date);
                        startDate.setHours(0);
                        endDate.setHours(0);
                        current.setHours(0);
                        if (startDate.getTime() <= current.getTime() && current.getTime() <= endDate.getTime()) {
                            return [true, 'selected-date', ''];
                        }
                        // console.log('current: ' + date);
                        // console.log('start: ' + self.startDate());
                        // console.log('current: ' + current.toTimeString() + ' ' + current.getTime());
                        // console.log('start: ' + startDate.toTimeString() + ' ' + startDate.getTime());
                        // console.log('------------------------------');
                        return [true, '', ''];
                    };

                ko.bindingHandlers.m2Calendar = {
                    init: function (e, v) {
                        $(e).calendar({
                            minDate: v().startDate,
                            maxDate: v().endDate,
                            dateFormat: "yyyy-mm-dd",
                            showWeek: false,
                            changeMonth: false,
                            changeYear: false,
                            onSelect: onSelectFunc,
                            beforeShowDay: beforeShowDayFunc.bind(e)
                        });
                        self.calendar = $(e);
                    }
                };
            },
            init: function () {
                var self = this;
                for (var i = 0; i < self.services().length; i++) {
                    self.serviceData[self.services()[i].title] = self.services()[i];
                }
                var dateOnChangeFunc = function () {
                    self.isValidDate(false);
                    var startDateStr = self.startDate();
                    var endDateStr = self.endDate();
                    var start = Date.parse(startDateStr);
                    var end = Date.parse(endDateStr);
                    if (self.roomQty() < 0) {
                        return;
                    }
                    if (isNaN(end) || isNaN(start)) {
                        return;
                    }
                    start = new Date(start);
                    end = new Date(end);
                    // var today = new Date();
                    if (end - start < 0) {
                        console.log('end < start');
                        this.errorMessage($t('End date is earlier than start date'));
                        return;
                    }
                    setTimeout(function () {
                        // self.calendar.datepicker('setDate', null);
                        self.calendar.datepicker('refresh');
                    }, 0); // don't know why it doesn't work without setTimout
                    $.ajax({
                        url: BASE_URL + 'hotel/room/check',
                        method: 'POST',
                        dataType: 'json',
                        showLoader: true,
                        async: true,
                        data: {
                            productId: self.productId,
                            startDate: startDateStr,
                            endDate: endDateStr,
                            qty: self.roomQty()
                        },
                        success: function (r) {
                            if (r.success === true) {
                                var prices = r.data;
                                if (prices.maxQty) {
                                    self.qtyLabel($t("Qty ( Max : %1 room(s) )").replace('%1', prices.maxQty));
                                }
                                self.dateRanges([]);
                                self.totalPrice = 0;
                                self.isValidDate(true);
                                var status,
                                    isValid = true;
                                for (var d = start; d <= end; d.setDate(d.getDate() + 1)) {
                                    var price = prices[self.dateToStr(d)];
                                    self.isValidDate(false);
                                    if (price === undefined || price === null) {
                                        isValid = false;
                                        status = $t("Undefined price");
                                    } else if (price.salePrice === undefined) {
                                        isValid = false;
                                        status = price.status;
                                    } else {
                                        self.totalPrice += Number(price.salePrice);
                                        status = self.currencySymbol + price.salePrice
                                            + " - Regular Price: " + self.currencySymbol + price.origPrice;
                                    }
                                    self.dateRanges.push({
                                        date: self.dateToStr(d),
                                        status: status
                                    });
                                }
                                self.isValidDate(isValid);
                                self.totalPrice = r.total;
                                self.formatPrice();
                            } else {
                                console.log(r);
                                if (r.message === undefined) {
                                    r.message = $t('Unknown error');
                                }
                                self.errorMessage(r.message);
                            }
                        },
                        error: function (r) {
                            console.log(r);
                        },
                        complete: function () {

                        }
                    });
                };
                self.startDate.subscribe(dateOnChangeFunc.bind(self));
                self.endDate.subscribe(dateOnChangeFunc.bind(self));
                self.roomQty.subscribe(dateOnChangeFunc.bind(self));
                self.selectedServices.subscribe(function () {
                    self.totalSelectedServicesPrice = 0;
                    for (var i = 0; i < self.selectedServices().length; i++) {
                        var service = self.getService(self.selectedServices()[i]);
                        self.totalSelectedServicesPrice += parseFloat(service.price);
                    }
                    self.formatPrice();
                });
                var cartData = self.productInCartData;

                if (cartData.itemCartId !== undefined) {
                    self.startDate(cartData.check_in_date);
                    self.endDate(cartData.check_out_date);
                    self.roomQty(cartData.qty);
                    self.selectedServices(cartData.service);
                }
            },
            formatPrice: function () {
                this.errorMessage('');
                this.formatedTotal('$' + parseFloat(this.totalPrice + this.totalSelectedServicesPrice).toFixed(2));
            },
            dateToStr: function (date) {
                return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
            },
            doBookAction: function () {
                var self = this;
                var startDateStr = self.startDate();
                var endDateStr = self.endDate();
                var cartData = self.productInCartData;
                var itemCartId = null;
                if (cartData.itemCartId !== undefined) {
                    itemCartId = cartData.itemCartId;
                }
                //@todo: validate date
                $.ajax({
                    url: BASE_URL + 'hotel/room/book',
                    method: 'POST',
                    dataType: 'json',
                    showLoader: true,
                    data: {
                        itemCartId: itemCartId,
                        productId: self.productId,
                        startDate: startDateStr,
                        endDate: endDateStr,
                        qty: self.roomQty(),
                        services: self.selectedServices(),
                        totalPrice: self.totalPrice + self.totalSelectedServicesPrice
                    },
                    success: function (r) {
                        if (r.success === true) {
                            var sections = ['cart'];
                            customerData.invalidate(sections);
                            customerData.reload(sections, true);
                        } else {
                            console.log(r);
                        }
                    },
                    error: function (r) {
                        console.log(r);
                    },
                    complete: function () {

                    }
                });
            }
        });
    }
);
