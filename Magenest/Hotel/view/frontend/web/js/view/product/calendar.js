/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'underscore',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry',
        'Magenest_Hotel/js/model/calendar/calendarview'
    ], function ($, _, $t, ko, Component, registry) {
        'use strict';

        var mainComponent = registry.get('hotel_date_selector'),
            dateFormat = '%Y-%m-%d',
            selectHandler = function (calendar, value) {
                var calendarComponent = registry.get('hotel_date_selector.calendar'),
                    startDate = calendarComponent.startDate,
                    endDate = calendarComponent.endDate,
                    isValid = true;

                var parsedValue = Date.parse(value);
                if (parsedValue > Date.parse(endDate) ||
                    parsedValue < Date.parse(startDate)) {
                    isValid = false;
                }

                if (isValid) {
                    var prevDate = registry.get('prevDate'),
                        currentDate = registry.get('currentDate');
                    if (currentDate !== undefined && prevDate !== undefined) {
                        prevDate(currentDate());
                        currentDate(value);
                    }
                    var start, end;
                    if (prevDate() === currentDate() || prevDate() === undefined) {
                        start = currentDate();
                        end = currentDate();
                    } else if (Date.parse(currentDate()) - Date.parse(prevDate()) > 0) {
                        start = prevDate();
                        end = currentDate();
                    } else {
                        start = currentDate();
                        end = prevDate();
                    }
                    mainComponent.startDate(start);
                    mainComponent.endDate(end);
                }

                var date = new Date(mainComponent.startDate()),
                    d, m, y;
                $('div#customCalendar tbody td.selected').removeClass('selected');
                while (date.getTime() <= Date.parse(mainComponent.endDate())) {
                    d = date.getDate();
                    m = date.getMonth();
                    y = date.getFullYear();
                    $("div#customCalendar tbody td[data-day='" + d + "'][data-month='" + m + "'][data-year='" + y + "']").addClass('selected');
                    date.setDate(date.getDate() + 1);
                }
            };

        ko.bindingHandlers.customCalendarBinding = {
            init: function (e, v) {
                function setupCalendars() {
                    // Embedded Calendar
                    Calendar.setup(
                        {
                            // dateField: 'customDateField',
                            parentElement: 'customCalendar',
                            selectHandler: selectHandler,
                            dateFormat: dateFormat
                        }
                    );
                }

                setupCalendars();
            }
        };

        return Component.extend({
            defaults: {
                template: 'Magenest_Hotel/product/calendar',
                currentDate: ko.observable(),
                prevDate: ko.observable()
            },
            initialize: function () {
                this._super();
                this.init();
            },
            init: function () {
                registry.set('prevDate', this.prevDate);
                registry.set('currentDate', this.currentDate);
            }
        });
    }
);
