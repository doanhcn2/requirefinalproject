<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Logger;

class Logger extends \Monolog\Logger
{
    public function __construct(
        \Magenest\Hotel\Logger\Handler $handler,
        $name = "HotelLogger",
        $handlers = array(),
        $processors = array()
    )
    {
        parent::__construct($name, $handlers, $processors);
//        $this->handlers = ['system' => $handler];
        $this->pushHandler($handler);
    }
}