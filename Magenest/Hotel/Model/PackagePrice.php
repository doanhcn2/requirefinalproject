<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class Hotel
 * @package Magenest\Hotel\Model
 * @method setPackageId($id)
 * @method setSalePrice($decimal)
 * @method setOrigPrice($decimal)
 * @method setDate($date)
 * @method setQty($qty)
 * @method getPackageId()
 * @method getSalePrice()
 * @method getOrigPrice()
 * @method getDate()
 * @method getQty()
 */
class PackagePrice extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'hotel_package_price_';

    /**
     * Hotel constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\PackagePrice $resource
     * @param ResourceModel\PackagePrice\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice $resource,
        \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param $packageId
     * @param $date
     * @return \Magenest\Hotel\Model\PackagePrice
     */
    public function loadByPackageIdAndDate($packageId, $date)
    {
        $items = $this->getCollection()
            ->addFieldToFilter('package_id', $packageId)
            ->addFieldToFilter('date', $date)
            ->getItems();
        if (!empty($items)) {
            return reset($items);
        }
        $this->setPackageId($packageId);
        $this->setDate($date);
        return $this;
    }
}
