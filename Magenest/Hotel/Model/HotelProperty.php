<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class HotelProperty
 * @package Magenest\Hotel\Model
 * @method string getVendorId()
 * @method string getCheckInFrom()
 * @method string getCheckInTo()
 * @method string getCheckOutFrom()
 * @method string getCheckOutTo()
 * @method string getIsInternet()
 * @method string getIsParking()
 * @method string getIsBreakfast()
 * @method string getIsChildrenAllowed()
 * @method string getIsPetAllowed()
 * @method string getLanguages()
 * @method string getPhotos()
 * @method string getTerms()
 * @method string getIsTax()
 * @method string getTaxAmount()
 * @method string getTaxType()
 * @method string getLocation()
 * @method string getLongitude()
 * @method string getLatitude()
 */
class HotelProperty extends \Magento\Framework\Model\AbstractModel
{
    const NO = 0;
    const YES_FREE = 1;
    const YES_PAID = 2;

    /**
     * @var string
     */
    protected $_eventPrefix = 'hotel_property_';

    /**
     * Hotel constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\HotelProperty $resource
     * @param ResourceModel\HotelProperty\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty $resource,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param $id
     * @return \Magento\Framework\DataObject
     */
    public function loadByVendorId($id)
    {
        $model = $this->_resourceCollection->addFieldToFilter('vendor_id', $id)->getFirstItem();
        if ($model->isEmpty()) {
            $model->setData('vendor_id', $id);
        }
        return $model;
    }
}
