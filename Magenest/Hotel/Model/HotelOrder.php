<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class HotelOrder
 * @package Magenest\Hotel\Model
 * @method setPackageId($id)
 * @method setQty($qty)
 * @method setServices($arrJson)
 * @method setCheckInDate($date)
 * @method setCheckOutDate($date)
 * @method setStatus($text)
 * @method setOrderId($id)
 * @method setCustomerId($id)
 * @method setTotalPaid($total)
 */
class HotelOrder extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'hotel_order_';

    /**
     * HotelOrder constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\HotelOrder $resource
     * @param ResourceModel\HotelOrder\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder $resource,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
