<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class RoomTypeVendor
 * @package Magenest\Hotel\Model
 * @method string getRoomTypeAdminId()
 * @method string getVendorId()
 * @method string getRoomTitle()
 * @method string getNonSmoking()
 * @method string|null getNumberRoomsAvailable()
 * @method string|null getDescription()
 * @method string getBedsType()
 * @method string getNumberGuests()
 * @method string|null getRoomSize()
 * @method string getHasExtraBeds()
 * @method string|null getPricePerBed()
 * @method string|null getPhotoRoom()
 */
class RoomTypeVendor extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'RoomTypeVendor';

    /**
     * RoomTypeVendor constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init('Magenest\Hotel\Model\ResourceModel\RoomTypeVendor');
    }
}
