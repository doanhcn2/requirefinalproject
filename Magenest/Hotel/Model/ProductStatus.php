<?php

namespace Magenest\Hotel\Model;

use Magento\Framework\App\ObjectManager;

class ProductStatus extends \Unirgy\DropshipVendorProduct\Model\ProductStatus
{
    const STATUS_PENDING = 3;
    const STATUS_FIX = 4;
    const STATUS_DISCARD = 5;
    const STATUS_VACATION = 6;
    const STATUS_SUSPENDED = 7;
    const STATUS_WAITING_TO_BE_PUBLISHED = 8;
    const STATUS_OFFER_EXPIRED = 9;

    static public function getOptionArray()
    {
        $res = [
            self::STATUS_ENABLED => __('Enabled'),
            self::STATUS_DISABLED => __('Disabled'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_FIX => __('Fix'),
            self::STATUS_DISCARD => __('Discard'),
            self::STATUS_VACATION => __('Vacation'),
            self::STATUS_WAITING_TO_BE_PUBLISHED => __('Waiting to be published'),
            self::STATUS_OFFER_EXPIRED => __('Offer Expired')
        ];
        /** @var \Unirgy\Dropship\Helper\Data $hlp */
        $hlp = ObjectManager::getInstance()->get('\Unirgy\Dropship\Helper\Data');
        if ($hlp->isModuleActive('Unirgy_DropshipVendorMembership')) {
            $res[self::STATUS_SUSPENDED] = __('Suspended');
        }
        return $res;
    }

    public function getAllOptions()
    {
        $res = [
            [
                'value' => '',
                'label' => __('-- Please Select --')
            ]
        ];
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = [
                'value' => $index,
                'label' => $value
            ];
        }
        return $res;
    }
}