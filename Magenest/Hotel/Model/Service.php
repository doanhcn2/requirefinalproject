<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class Hotel
 * @package Magenest\Hotel\Model
 */
class Service extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'hotel_property_';

    /**
     * Hotel constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Service $resource
     * @param ResourceModel\Service\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\Service $resource,
        \Magenest\Hotel\Model\ResourceModel\Service\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function loadByVendorId($id) {
        $model = $this->_resourceCollection->addFieldToFilter('vendor_id', $id)->getItems();
//        if ($model->isEmpty()) {
//            $model->setData('vendor_id', $id);
//        }
        return $model;
    }
}
