<?php
namespace Magenest\Hotel\Model\Form\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\TestFramework\Event\Magento;

class OptionsMultiselect implements ArrayInterface
{
    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $facilityCollection = $objectManager->create('Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin\Collection');
        $facilityCollection->addFieldToSelect('*');
        $options = [];
        foreach ($facilityCollection as $facility) {
            $options[] = [
                'label' => $facility->getData('name'),
                'value' => $facility->getData('id')
            ];
        }
        return $options;

    }
}
