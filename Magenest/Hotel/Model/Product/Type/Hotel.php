<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Hotel\Model\Product\Type;

/**
 * Class Hotel
 * @package Magenest\Hotel\Model\Product\Type
 */
class Hotel extends \Magento\Catalog\Model\Product\Type\Virtual
{
    /**
     * Check if product has options
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    public function hasOptions($product)
    {
        return true;
    }
}
