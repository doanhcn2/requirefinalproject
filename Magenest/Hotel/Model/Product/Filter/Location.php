<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/28/18
 * Time: 10:21 PM
 */

namespace Magenest\Hotel\Model\Product\Filter;


class Location extends \Magento\Catalog\Model\Layer\Filter\AbstractFilter
{
    protected $_requestVar = 'location';
    protected $productType = 'hotel';

    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        $filter = $request->getParam($this->getRequestVar());

        if (!$filter) {
            return $this;
        }

        $tableName = $this->getLayer()->getProductCollection()->getTable('magenest_hotel_property_details');
        $this->getLayer()->getProductCollection()
            ->addAttributeToFilter('type_id', 'hotel')
            ->joinTable(
                $tableName,
                "`vendor_id`=udropship_vendor",
                ['location'],
                "`$tableName`.`vendor_id`=`at_udropship_vendor`.`value` AND `location` LIKE '%$filter%'"
            );
        $this->getLayer()->getState()->addFilter(
            $this->_createItem($filter, $filter)
        );

        return $this;
    }

    public function getName()
    {
        return 'Location';
    }
}