<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class PackageDetails
 * @package Magenest\Hotel\Model
 * @method setProductId($id)
 * @method setQty($qty)
 * @method setRoomType($id)
 * @method setBookFrom($date)
 * @method setBookTo($date)
 * @method setMinNights($int)
 * @method setMaxNights($int)
 * @method setCancellationPolicy($text)
 * @method setPenalty($text)
 * @method setMinTimeToBook($text)
 * @method setIsPublishedAsap($bool)
 * @method setPublishDate($date)
 * @method setIsUnpublishedAlap($bool)
 * @method setUnpublishDate($date)
 * @method setSpecialTerms($text)
 * @method setVendorId($id)
 * @method string|int getProductId()
 * @method string|int getQty()
 * @method string|int getRoomType()
 * @method string getBookFrom()
 * @method string getBookTo()
 * @method string|int getMinNights()
 * @method string|int getMaxNights()
 * @method string getCancellationPolicy()
 * @method string getPenalty()
 * @method string|int getMinTimeToBook()
 * @method string|int|boolean getIsPublishedAsap()
 * @method string|int|boolean getIsUnpublishedAlap()
 * @method string getSpecialTerms()
 * @method string|int getVendorId()
 */
class PackageDetails extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'hotel_package_details_';

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendor
     */
    protected $roomtypeVendor;

    /**
     * @var \Magenest\Hotel\Model\HotelProperty
     */
    protected $property;

    /**
     * @var ResourceModel\RoomTypeVendor
     */
    protected $roomtypeResource;

    /**
     * @var RoomTypeVendorFactory
     */
    protected $roomtypeFactory;

    /**
     * @var ResourceModel\HotelProperty
     */
    protected $propertyResource;

    /**
     * @var HotelPropertyFactory
     */
    protected $propertyFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepo;

    /**
     * PackageDetails constructor.
     * @param ResourceModel\PackageDetails\Collection $resourceCollection
     * @param ResourceModel\RoomTypeVendor $roomtypeResource
     * @param ResourceModel\HotelProperty $propertyResource
     * @param ProductRepositoryInterface $productRepository
     * @param RoomTypeVendorFactory $roomTypeVendorFactory
     * @param HotelPropertyFactory $hotelPropertyFactory
     * @param ResourceModel\PackageDetails $resource
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $resourceCollection,
        \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor $roomtypeResource,
        \Magenest\Hotel\Model\ResourceModel\HotelProperty $propertyResource,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails $resource,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->roomtypeFactory = $roomTypeVendorFactory;
        $this->propertyFactory = $hotelPropertyFactory;
        $this->roomtypeResource = $roomtypeResource;
        $this->propertyResource = $propertyResource;
        $this->productRepo = $productRepository;
    }

    /**
     * @return string
     */
    public function getPublishDate()
    {
        if ($this->getIsPublishedAsap()) {
            return '2001-01-13';
        } else {
            return $this->getData('publish_date');
        }
    }

    /**
     * @return string
     */
    public function getUnpublishDate()
    {
        if ($this->getIsUnpublishedAlap()) {
            return $this->getBookTo();
        } else {
            return $this->getData('unpublish_date');
        }
    }

    /**
     * @return \Magenest\Hotel\Model\RoomTypeVendor
     */
    public function getRoomTypeVendor()
    {
        if (!$this->roomtypeVendor) {
            $id = $this->getRoomType();
            $this->roomtypeVendor = $this->roomtypeFactory->create();
            $this->roomtypeResource->load($this->roomtypeVendor, $id);
        }
        return $this->roomtypeVendor;
    }

    /**
     * @return \Magenest\Hotel\Model\HotelProperty
     */
    public function getPropertyDetails()
    {
        if (!$this->property) {
            $vendorId = $this->getVendorId();
            $this->property = $this->propertyFactory->create();
            $this->propertyResource->load($this->property, $vendorId, 'vendor_id');
        }
        return $this->property;
    }

    /**
     * @return null|string
     */
    public function getProductName()
    {
        try {
            return $this->productRepo->getById($this->getProductId())->getName();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            return '';
        }
    }
}
