<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Hotel\Model\Config\Source;

use Magento\Config\Model\Config\Source\Email\Template as ParentTemplate;

class EmailTemplate extends ParentTemplate
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        array_unshift($options, ['value' => -1, 'label' => "Don't send email"]);
        return $options;
    }
}
