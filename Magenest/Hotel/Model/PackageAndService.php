<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

/**
 * Class Hotel
 * @package Magenest\Hotel\Model
 * @method setPackageId($id)
 * @method setServiceId($id)
 */
class PackageAndService extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'hotel_package_and_service_';

    /**
     * Hotel constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\PackageAndService $resource
     * @param ResourceModel\PackageAndService\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Hotel\Model\ResourceModel\PackageAndService $resource,
        \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
