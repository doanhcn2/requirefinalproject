<?php
namespace Magenest\Hotel\Model\ResourceModel;

class PropertyFacilityVendor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_property_facil_vendor', 'id');
    }
}
