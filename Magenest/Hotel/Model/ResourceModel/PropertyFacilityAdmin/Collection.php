<?php

namespace Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\PropertyFacilityAdmin', 'Magenest\Hotel\Model\ResourceModel\PropertyFacilityAdmin');
    }

    public function addVendorId($vendorId)
    {
        $vendorTable = $this->getTable('magenest_hotel_property_facil_vendor');
        $this->addFieldToSelect(['id', 'facility_kind', 'name'])
            ->addFieldToFilter('is_enable', ['eq' => '1'])
            ->getSelect()
            ->joinLeft(
                $vendorTable,
                "`$vendorTable`.`vendor_id`=$vendorId AND `main_table`.`id`=`$vendorTable`.`facility_id`",
                ['vendor_id AS is_enable']
            );
        return $this;
    }
}
