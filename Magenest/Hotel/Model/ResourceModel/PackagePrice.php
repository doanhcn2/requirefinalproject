<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class PackagePrice
 * @package Magenest\Hotel\Model\ResourceModel
 */
class PackagePrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_package_price', 'id');
    }
}
