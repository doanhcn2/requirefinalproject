<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\PackagePrice;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\PackagePrice
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Magenest\Hotel\Model\PackagePrice::class, \Magenest\Hotel\Model\ResourceModel\PackagePrice::class);
    }

    /**
     * @param $packageId
     * @param $startDate
     * @param $endDate
     * @return \Magenest\Hotel\Model\PackagePrice[]
     */
    public function getPricePack($packageId, $startDate, $endDate)
    {
        $prices = $this->addFieldToFilter('package_id', $packageId)
            ->addFieldToFilter('date', ['from' => $startDate, 'to' => $endDate])->getItems();
        return $prices;
    }

    /**
     * @param $packageId
     * @return \Magenest\Hotel\Model\PackagePrice[]
     */
    public function getAllPriceByPackage($packageId)
    {
        $prices = $this->addFieldToFilter('package_id', $packageId)->getItems();
        return $prices;
    }

    /**
     * @param string $firstDate
     * @param string $lastDate
     * @return \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection
     */
    public function addDateRangeFilter($firstDate, $lastDate)
    {
        return $this->addFieldToFilter('date', ['from' => $firstDate, 'to' => $lastDate]);
    }

    /**
     * @param $date
     * @return \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection
     */
    public function addDateFilter($date)
    {
        return $this->addFieldToFilter('date', $date);
    }

    /**
     * @param string|int $packageId
     * @return \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection
     */
    public function addPackageFilter($packageId)
    {
        return $this->addFieldToFilter('package_id', $packageId);
    }

    /**
     * @param string $roomTypeId
     * @return \Magenest\Hotel\Model\ResourceModel\PackagePrice\Collection
     */
    public function groupQtyByRoomType($roomTypeId)
    {
        $packageTable = $this->getTable('magenest_hotel_package_details');
        $this->removeAllFieldsFromSelect()
            ->addFieldToSelect('date')
            ->join(
                $packageTable,
                "`$packageTable`.`room_type`='$roomTypeId' AND `$packageTable`.`id`=`main_table`.`package_id`",
                ["$packageTable.room_type AS room_type_id"]
            )->getSelect()->columns("SUM(`main_table`.`qty`) AS total")->group('date');
        return $this;
    }
}
