<?php
namespace Magenest\Hotel\Model\ResourceModel\RoomAmenitiesAdmin;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\RoomAmanities
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\RoomAmenitiesAdmin', 'Magenest\Hotel\Model\ResourceModel\RoomAmenitiesAdmin');
    }
}
