<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\PackageAndService;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\PackageAndService
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\PackageAndService', 'Magenest\Hotel\Model\ResourceModel\PackageAndService');
    }

    /**
     * @param $packageId
     * @return \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection
     */
    public function addPackageIdToFilter($packageId) {
        return $this->addFieldToFilter('package_id', $packageId);
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection
     */
    public function moreDetails() {
        $serviceTable = $this->getTable('magenest_hotel_service');
        return $this->join(
            $serviceTable,
            "`main_table`.`service_id`=`$serviceTable`.`id`",
            ['title', 'description', 'price']
        );
    }
}
