<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\Service;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\Service
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Magenest\Hotel\Model\Service::class, \Magenest\Hotel\Model\ResourceModel\Service::class);
    }

    /**
     * @param $id
     * @return \Magenest\Hotel\Model\ResourceModel\Service\Collection
     */
    public function addVendorToFilter($id)
    {
        return $this->addFieldToFilter('vendor_id', $id);
    }

    /**
     * @param $vendorId
     * @param $packageId
     * @return array|string
     */
    public function getServicesWithPackageId($vendorId, $packageId)
    {
        $services = $this
            ->addFieldToFilter('vendor_id', $vendorId)
            ->getSelect()
            ->joinLeft(
                $this->getTable('magenest_hotel_package_and_service'),
                'main_table.id=service_id AND package_id=' . $packageId,
                'service_id AS is_enabled'
            )
            ->distinct(true)
            ->order('id asc');
//            ->__toString();
        $services = $this->getConnection()->fetchAll($services);
        return $services;
    }
}
