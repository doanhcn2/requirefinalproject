<?php
namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class RoomAmenitiesVendor
 * @package Magenest\Hotel\Model\ResourceModel
 */
class RoomAmenitiesVendor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_room_amenities_vendor', 'id');
    }
}
