<?php
namespace Magenest\Hotel\Model\ResourceModel\RoomTypeAdmin;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\RoomTypeAdmin
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\RoomTypeAdmin', 'Magenest\Hotel\Model\ResourceModel\RoomTypeAdmin');
    }
}
