<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\HotelOrder;

use Magenest\Hotel\Helper\BookingStatus as Status;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\Hotel
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_packageDetailsCollectionFactory;

    protected $_idFieldName = 'id';

    public function __construct(
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magenest\Hotel\Logger\Logger $logger,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    )
    {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
    }

    protected function _construct()
    {
        $this->_init(\Magenest\Hotel\Model\HotelOrder::class, \Magenest\Hotel\Model\ResourceModel\HotelOrder::class);
    }

    /**
     * @param $customerId
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    public function loadByCustomerId($customerId)
    {
        return $this->addFieldToFilter('customer_id', $customerId);
    }

    /**
     * @param $vendorId
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    public function loadByVendorId($vendorId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageDetails\Collection $packageDetailsCollection */
        $packageDetailsCollection = $this->_packageDetailsCollectionFactory->create();
        $packageIds = $packageDetailsCollection
            ->addFieldToSelect($packageDetailsCollection->getIdFieldName())
            ->addFieldToFilter('vendor_id', $vendorId)
            ->getData();
        $ids = [];
        foreach ($packageIds as $packageId) {
            $ids[] = $packageId['id'];
        }
        return $this->addFieldToFilter('package_id', ['in' => $ids]);
    }

    /**
     * @param array|string $status
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    public function addStatusToFilter($status)
    {
        return $this->addFieldToFilter('status', $status);
    }

    /**
     * @todo: can't use along with status filter at addStatusToFilter($status)
     * @param $roomTypeId
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    public function groupQtyByRoomType($roomTypeId)
    {
        $packageTable = $this->getTable('magenest_hotel_package_details');
        $priceTable = $this->getTable('magenest_hotel_package_price');
        $dateFilter = "`date`>=`main_table`.`check_in_date` AND `date`<=`main_table`.`check_out_date`";
        $statusFilter = "`status` IN ('" . Status::PENDING . "', '" . Status::ACTIVE . "')";
        $this->removeAllFieldsFromSelect()
            ->join(
                $packageTable,
                "`$packageTable`.`room_type`='$roomTypeId' AND `$packageTable`.`id`=`main_table`.`package_id`",
                [])
            ->getSelect()->columns("SUM(`main_table`.`qty`) AS total")
            ->group('date')
            ->joinRight(
                $priceTable,
                "`main_table`.`package_id`=`$priceTable`.`package_id` AND $dateFilter AND $statusFilter",
                "date"
            );
        return $this;
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection
     */
    public function moreDetails()
    {
        $orderTable = $this->getTable('sales_order');
        $packageTable = $this->getTable('magenest_hotel_package_details');
        $roomTypeTable = $this->getTable('magenest_hotel_room_type_vendor');
        $this->join(
            $orderTable,
            "`$orderTable`.`entity_id`=`main_table`.`order_id`",
            [
                'order_currency_code AS currency',
                'created_at', 'CONCAT(`customer_firstname`, " ", `customer_lastname`) AS customer_name'
            ]
        )->join(
            $packageTable,
            "`$packageTable`.`id`=`main_table`.`package_id`",
            ['room_type', 'product_id']
        )->join(
            $roomTypeTable,
            "`$packageTable`.`room_type`=`$roomTypeTable`.`id`",
            'room_title'
        );
        return $this;
    }

    public function getBookedRoomsADay($packageId, $date)
    {
        $a = $this
            ->addExpressionFieldToSelect('total_qty', 'SUM({{qty}})', 'qty')
            ->addFieldToFilter('package_id', $packageId)
            ->addFieldToFilter('check_in_date', ['lteq' => $date])
            ->addFieldToFilter('check_out_date', ['gteq' => $date])
            ->addFieldToFilter('status', ['nin' => [Status::CANCELLED, Status::EXPIRED,Status::REFUNDED]])
            ->getFirstItem();
        return $a->getTotalQty();
    }
}
