<?php
namespace Magenest\Hotel\Model\ResourceModel\BedType;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\BedType
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\BedType', 'Magenest\Hotel\Model\ResourceModel\BedType');
    }
}
