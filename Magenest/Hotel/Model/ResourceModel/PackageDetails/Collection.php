<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\PackageDetails;

use Magento\Framework\App\ObjectManager;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\PackageDetails
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\PackageDetails', 'Magenest\Hotel\Model\ResourceModel\PackageDetails');
    }

    /**
     * @param $productId
     * @return \Magenest\Hotel\Model\PackageDetails
     */
    public function getItemByProductId($productId)
    {
        // @todo: check if package with this product id is exsist before return
        return $this->addFieldToFilter('product_id', $productId)->getFirstItem();
    }

    /**
     * @param $status
     * @return $this
     */
    public function filterByCampaignStatus($status)
    {
        $productsId = [];
        foreach ($this as $item) {
            $productsId[] = $item->getProductId();
        }

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $products */
        $products = ObjectManager::getInstance()->create('Magento\Catalog\Model\Product')->getCollection();
        $products = $products->addFieldToFilter('entity_id', $productsId)->addFieldToFilter('campaign_status', $status);
        $productsId = $products->getAllIds();
        return $this->addFieldToFilter('product_id', ['in' => $productsId]);
    }

    /**
     * @param $roomTypeId
     * @return $this
     */
    public function addVendorRoomFilter($roomTypeId)
    {
        return $this->addFieldToFilter('room_type', $roomTypeId);
    }

//    public function addProductName()
//    {
//        $eav_eav_entity_type = $this->getTable('eav_entity_type');
//        $eav_attribute = $this->getTable('eav_attribute');
//        return $this->join('', '', '');
//    }
}
