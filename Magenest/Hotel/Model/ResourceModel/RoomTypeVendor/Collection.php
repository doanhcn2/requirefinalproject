<?php

namespace Magenest\Hotel\Model\ResourceModel\RoomTypeVendor;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\RoomTypeVendor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Magenest\Hotel\Model\RoomTypeVendor::class, \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor::class);
    }

    /**
     * @param $vendorId
     * @return \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection
     */
    public function addVendorToFilter($vendorId)
    {
        $this->addFieldToFilter('vendor_id', $vendorId);
//        $this->getSelect()->where("`main_table`.`vendor_id` = ?", $vendorId);
        return $this;
    }

//    /**
//     * @return \Magenest\Hotel\Model\ResourceModel\RoomTypeVendor\Collection
//     */
//    public function addTotalRoomToSelect()
//    {
//        $packageTable = $this->getTable('magenest_hotel_package_details');
//        $priceTable = $this->getTable('magenest_hotel_package_price');
//
//        $this->join(
//            $packageTable,
//            "`$packageTable`.`room_type`=`main_table`.`{$this->_idFieldName}`",
//            []
//        );
//
//        $this->getSelect()
//            ->joinLeft(
//                $priceTable,
//                "`$priceTable`.`package_id`=`$packageTable`.`id`",
//                "SUM(`$priceTable`.`qty`) AS total_rooms"
//            )
//            ->group(['id']);
//
//        $a = $this->getSelect()->__toString();
//
//        return $this;
//    }
}
