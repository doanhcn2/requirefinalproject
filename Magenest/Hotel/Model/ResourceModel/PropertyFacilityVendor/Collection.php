<?php

namespace Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Magenest\Hotel\Model\PropertyFacilityVendor::class, \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor::class);
    }

    /**
     * @param $vendorId
     * @return \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection
     */
    public function addVendorToFilter($vendorId)
    {
        return $this->addFieldToFilter('vendor_id', $vendorId);
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection
     */
    public function addFacilDetails()
    {
        $detailsTable = $this->getTable('magenest_hotel_property_facil_admin');
        return $this->join(
            $detailsTable,
            "`main_table`.`facility_id`=`$detailsTable`.`id` AND `$detailsTable`.`is_enable`='1'",
            ["facility_kind AS kind", "name"]
        )->setOrder('kind');
    }
}
