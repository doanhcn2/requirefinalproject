<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel;

class Gallery extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_modelFactory;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magenest\Hotel\Model\GalleryFactory $modelFactory,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_modelFactory = $modelFactory;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_hotel_gallery', 'id');
    }

    public function loadByVendorId($vendorId)
    {
        /** @var \Magenest\Hotel\Model\Gallery $model */
        $model = $this->_modelFactory->create();
        $this->load($model, $vendorId, 'vendor_id');
        if (!$model->getId()) {
            $model->setVendorId($vendorId);
        }
        return $model;
    }
}
