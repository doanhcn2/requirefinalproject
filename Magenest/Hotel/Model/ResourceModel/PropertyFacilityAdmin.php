<?php
namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class PropertyFacilityAdmin
 * @package Magenest\Hotel\Model\ResourceModel
 */
class PropertyFacilityAdmin extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_property_facil_admin', 'id');
    }
}
