<?php
namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class RoomAmenitiesAdmin
 * @package Magenest\Hotel\Model\ResourceModel
 */
class RoomAmenitiesAdmin extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_room_amenities_admin', 'id');
    }
}
