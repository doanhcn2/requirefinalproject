<?php

namespace Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\RoomAmenitiesVendor', 'Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor');
    }

    /**
     * @param $roomVendorId
     * @return array
     */
    public function getAmenitiesData($roomVendorId)
    {
        $adminTable = $this->getTable('magenest_hotel_room_amenities_admin');
        $collection = $this->addFieldToFilter('room_vendor_id', $roomVendorId)
            ->join(
                $adminTable,
                "`main_table`.`amenity`=`$adminTable`.`id` AND `$adminTable`.`is_enable` = 1",
                'name'
            );
        $data = $collection->getData();
        return $data;
    }

    /**
     * @param $vendorId
     * @return \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection
     */
    public function addRoomTypeToFilter($vendorId)
    {
        return $this->addFieldToFilter('room_vendor_id', $vendorId);
    }

    /**
     * @return \Magenest\Hotel\Model\ResourceModel\RoomAmenitiesVendor\Collection
     */
    public function addAmenityDetails()
    {
        $adminTable = $this->getTable('magenest_hotel_room_amenities_admin');
        return $this->join(
            $adminTable,
            "`main_table`.`amenity`=`$adminTable`.`id` AND `$adminTable`.`is_enable` = 1",
            ["room_kind AS kind", "name"]
        )->setOrder("kind");
    }
}
