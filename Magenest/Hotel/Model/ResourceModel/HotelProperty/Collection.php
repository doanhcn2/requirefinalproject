<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model\ResourceModel\HotelProperty;

/**
 * Class Collection
 * @package Magenest\Hotel\Model\ResourceModel\Hotel
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\HotelProperty', 'Magenest\Hotel\Model\ResourceModel\HotelProperty');
    }

    /**
     * @param $id
     * @return \Magenest\Hotel\Model\HotelProperty
     */
    public function getHotelPropertyByVendorId($id)
    {
        return $this->addFieldToFilter('vendor_id', $id)->getFirstItem();
    }

    public function getAllLocations()
    {
        $connection = $this->getConnection();
        $sql = $connection->select()->distinct()->from($this->getMainTable(), 'location');
        $arr = $connection->fetchCol($sql);
        return $arr;
    }
}
