<?php
namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class RoomType
 * @package Magenest\Hotel\Model\ResourceModel
 */
class BedType extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_bed_type_admin', 'id');
    }
}
