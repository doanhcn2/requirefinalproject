<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 12/08/2016
 * Time: 14:19
 */
namespace Magenest\Hotel\Model\ResourceModel;

/**
 * Class RoomType
 * @package Magenest\Hotel\Model\ResourceModel
 */
class RoomTypeAdmin extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_hotel_room_type_admin', 'id');
    }
}
