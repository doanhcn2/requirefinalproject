<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Hotel\Model;

class Gallery extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'magenest_hotel_gallery';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Hotel\Model\ResourceModel\Gallery');
    }
}
