<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Block\Vendor;

/**
 * Class Product
 * @package Magenest\Wrapper\Block\Vendor
 */
class Product extends \Magento\Catalog\Block\Product\View
{
}
