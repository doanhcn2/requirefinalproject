<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Block\Order;

/**
 * Class Info
 * @package Magenest\Wrapper\Block\Order
 */
class Info extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Catalog\Helper\Image $_imageHelper */
    protected $_imageHelper;

    /** @var \Magento\Sales\Model\Order $order */
    protected $order;

    /** @var \Magenest\Wrapper\Model\Wrapper $wrapper */
    protected $wrapper;

    /**
     * @var string
     */
    protected $_productImageId = 'product_page_image_small';

    /**
     * @var
     */
    protected $_orderId;

    /**
     * @var
     */
    protected $_quoteId;

    /**
     * Info constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Sales\Model\Order $order
     * @param \Magenest\Wrapper\Model\Wrapper $wrapper
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Sales\Model\Order $order,
        \Magenest\Wrapper\Model\Wrapper $wrapper,
        array $data
    ) {
    
        $this->wrapper = $wrapper;
        $this->order = $order;
        $this->_imageHelper = $context->getImageHelper();
        parent::__construct($context, $data);
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId)
    {
        $this->_orderId = $orderId;
        $this->_quoteId = $this->order->load($orderId)->getQuoteId();
    }

    /**
     * @param $product
     * @return string
     */
    public function getProductImg($product)
    {
        if ($product instanceof \Magento\Catalog\Model\Product) {
            return $this->_imageHelper->init($product, $this->_productImageId)->getUrl();
        }

        return '';
    }

    /**
     * @return \Magenest\Wrapper\Model\Wrapper[]
     */
    public function getWrappers()
    {
        return $this->wrapper->loadByQuoteId($this->_quoteId);
    }
}
