<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Helper;

/**
 * Class Data
 * @package Magenest\Wrapper\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSesion;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
         \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSesion = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * @param $itemId
     * @return bool
     */
    public function isWrapped($itemId)
    {
        $quote = $this->checkoutSesion->getQuote();
        $quoteId = $quote->getId();
        $wrappers = $this->wrapper->loadByQuoteId($quoteId);
        if (count($wrappers) > 0) {

            foreach ($wrappers as $wrapper) {
                $products = $wrapper->getProducts();
                foreach ($products as $product) {
                    if ($product->item_id == $itemId) {
                        return true;
                    }

                }
            }
        }
    }

    /**
     * @param $itemId
     * @return bool
     */
    public function isWrappableItem($itemId)
    {
        $quote = $this->checkoutSesion->getQuote();
        $quoteId = $quote->getId();
        $wrappers = $this->wrapper->loadByQuoteId($quoteId);
        if (count($wrappers) > 0) {
            foreach ($wrappers as $wrapper) {
                $box_quote_id = $wrapper->getData('wrapper_quote_id');
                $card_quote_id = $wrapper->getData('card_quote_id');
                if ($itemId == $box_quote_id) return true;
                if ($itemId == $card_quote_id) return true;
            }
        }
    }

    /**
     * @param $randTime
     * @return mixed
     */
    public function getQuoteItemByTime($randTime)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->checkoutSesion->getQuote();
        $allItems = $quote->getAllVisibleItems();

        if (count($allItems) > 0) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($allItems as $item) {
                /** @var \Magento\Quote\Model\Quote\Item\Option $option */
                $option = $item->getOptionByCode('info_buyRequest');
                $buyInfo = Util::parse($option->getValue(), true);

                if (isset($buyInfo['added_time'])) {
                    if ($randTime == $buyInfo['added_time']) {
                        return $item->getId();
                    }
                }
            }
        }
    }
}
