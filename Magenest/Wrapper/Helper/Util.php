<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Helper;

/**
 * Class Util
 * @package Magenest\Wrapper\Helper
 */
class Util
{
    /**
     * @return float|string
     */
    public static function checkMagentoVersion()
    {
        $productMetadata = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        $subVersion = substr($version, 0, 3);
        $subVersion = floatval($subVersion);

        return $subVersion;
    }

    /**
     * @param mixed $a
     * @return string
     */
    public static function stringify($a)
    {
        $version = self::checkMagentoVersion();

        if ($version <= 2.1) {
            return serialize($a);
        } else {
            return json_encode($a);
        }
    }

    /**
     * @param string $s
     * @param bool $arr
     * @return mixed
     */
    public static function parse($s, $arr = false)
    {
        $version = self::checkMagentoVersion();

        if ($version <= 2.1) {
            return unserialize($s);
        } else {
            return json_decode($s, $arr);
        }
    }
}
