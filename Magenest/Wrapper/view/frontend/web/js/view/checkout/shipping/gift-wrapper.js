/**
 * Created by Pham Quang Hau on 05/01/2016.
 */
define([ // jshint ignore:line
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data'
], function ($,
             _,
             Component,
             ko,
             modal,
             $t,
             totals,
             quote,
             fullScreenLoader,
             getPaymentInformationAction,
             getTotals,
             customerData) {
    'use strict';

    var popUp = null;
    var options = {
        type: 'popup',
        responsive: true,
        innerScroll: true,
        title: $t('Your Cart')
    };

    var wrapperCounter = 0;
    var baseUrl;
    var wrappable;

    // noinspection JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols,JSUnusedLocalSymbols
    return Component.extend({
        defaults: {
            template: 'Magenest_Wrapper/checkout/shipping/gift-wrapper'
        },

        wrapperArray: ko.observableArray(),
        giftCardArray: ko.observableArray(),
        selectedWrapperArray: ko.observableArray(),
        productArray: ko.observableArray(),

        isModalOpened: ko.observable(false),
        isSelectedWrapperVisible: ko.observable(false),

        getItems: totals.getItems(),
        imageData: window.checkoutConfig.imageData,
        quoteId: window.checkoutConfig.quoteData.entity_id,

        isChecked: ko.observable(false),
        isInboxChecked: ko.observable(false),
        isOutboxChecked: ko.observable(false),
        isMessageVisible: ko.observable(true),
        enableButton: true,
        textButton: $t('Add Wrapper'),

        /**
         *
         */
        initialize: function () {
            var self = this;
            this._super();
            baseUrl = BASE_URL; // jshint ignore:line

            // todo: combine 3 ajax into 1.
            self.loadWrappersAndCards();
            self.loadSelectedWrappers();
            self.loadWrappableItems();
        },

        /**
         * Get wrappable items from cart
         */
        loadWrappableItems: function () {
            var self = this;
            var itemUrl = baseUrl.concat('wrapper/checkout/items');

            $.ajax({
                type: "POST",
                url: itemUrl,
                async: false,
                data: {
                    quote_id: self.quoteId
                },
                success: function (result) {
                    wrappable = result;
                    if (wrappable.length === 0) {
                        self.enableButton = false;
                        self.textButton = $t('No Wrapple Product');
                    }
                }
            });

            var i = 0;
            while (i < wrappable.length) {
                for (var j = 0; j < self.getItems().length; j++) {
                    if (wrappable[i].item_id !== self.getItems()[j].item_id) {
                        continue;
                    }
                    var productObj = {};
                    productObj = {
                        item_id: self.getItems()[j].item_id,
                        item_name: self.getItems()[j].name,
                        imageSrc: self.imageData[self.getItems()[j].item_id]['src'], // jshint ignore:line
                        quantity: self.getItems()[j].qty,
                        options: []
                    };

                    var productOptions = JSON.parse(self.getItems()[j].options);
                    if (productOptions.length > 0) {
                        productObj.hasOptions = 1;
                        productObj.options = productOptions;
                    } else {
                        productObj.hasOptions = 0;
                    }

                    self.productArray.push(productObj);
                }
                i++;
            }

        },

        /**
         *
         * @param itemId
         * @returns ko.observableArray([{value: '', text: ''},...])
         */
        getSelectOptionsArray: function (itemId) {
            var self = this;
            var qtyArray = [];
            var quantity = 0;

            for (var i = 0; i < self.productArray().length; i++) {
                if (self.productArray()[i].item_id === itemId) {
                    quantity = self.productArray()[i].quantity;
                }
            }

            for (i = 0; i < self.selectedWrapperArray().length; i++) {
                for (var j = 0; j < self.selectedWrapperArray()[i].qtyOptions.length; j++) {
                    if (self.selectedWrapperArray()[i].qtyOptions[j].item_id === itemId) {
                        quantity -= self.selectedWrapperArray()[i].qtyOptions[j].qty;
                    }
                }
            }
            for (var k = 0; k <= quantity; k++) {
                var qtyobj = {
                    value: k,
                    text: k === 0 ?
                        "Don't wrap this" :
                        ((quantity === 1 && k === 1) ?
                            "Wrap this" :
                            (k === 1 ? "Just one" : k + " products"))
                };
                qtyArray.push(qtyobj);
            }

            return ko.observableArray(qtyArray);
        },

        /**
         * Get already wrapped wrappers
         */
        loadSelectedWrappers: function () {
            var self = this;
            var wrappedUrl = baseUrl.concat('wrapper/checkout/wrapped');
            $.ajax({
                type: "POST",
                url: wrappedUrl,
                data: {
                    quote_id: self.quoteId
                },
                success: function (result) {
                    // var wrapped = result;
                    self.selectedWrapperArray(result);
                },
                async: false
            });

            // for (var i = 0; i < wrapped.length; i++) {
            //     wrapped[i].onclick = self.deleteWrapper.bind(self);
            //     wrapperCounter++;
            //     self.selectedWrapperArray.push(wrapped[i]);
            //     self.isSelectedWrapperVisible(true);
            // }
        },

        /**
         * Get wrappers & cards information
         */
        loadWrappersAndCards: function () {
            var self = this;
            var requestUrl = baseUrl.concat('wrapper/checkout/wrapperandcard');
            $.ajax({
                type: "POST",
                url: requestUrl,
                success: function (result) {
                    self.wrapperArray(result.wraps);
                    self.giftCardArray(result.cards);
                    console.log(self.wrapperArray());
                    if (self.wrapperArray().length === 0) {
                        self.textButton = $t('No Wrapper Available Now');
                        self.enableButton = false;
                    }
                },
                async: false
            });
        },

        /**
         * Delete a wrapper from cart
         *
         * @param data
         * @param event
         */
        deleteWrapper: function (data, event) {
            var self = this;
            var id = data.wrapper_id;
            for (var i = 0; i < self.selectedWrapperArray().length; i++) {
                /** loop through the array of selected wrappers */
                if (id === self.selectedWrapperArray()[i].wrapper_id) {
                    for (var j = 0; j < self.selectedWrapperArray()[i].qtyOptions.length; j++) {
                        /** loop through the array of products that has been chosen along with the wrapper */
                        for (var k = 0; k < self.productArray().length; k++) {
                            /** loop through the array of products in cart */
                            if (self.selectedWrapperArray()[i].qtyOptions[j].item_id === self.productArray()[k].item_id) { // if item id of the 2 product matches
                                var a = parseInt(self.productArray()[k].quantity);
                                var b = parseInt(self.selectedWrapperArray()[i].qtyOptions[j].qty);

                                self.productArray()[k].quantity = a + b;
                                // self.productArray()[k].qtyArray([]);

                                // for (var m = 0; m <= self.productArray()[k].quantity; m++) {
                                //     self.productArray()[k].qtyArray.push(m);
                                // }
                            }
                        }
                    }

                    /** Send request to delete wrapper */
                    var deleteUrl = baseUrl.concat('wrapper/checkout/delete');
                    $.ajax({
                        type: "POST",
                        url: deleteUrl,
                        data: {
                            id: id
                        },
                        async: false
                    }).done(function (response) {
                        if (response && response.result == true) {
                            var deferred = $.Deferred();
                            totals.isLoading(true);
                            getPaymentInformationAction(deferred);

                            //update summary
                            getTotals(function () {
                                //totals.totals().items_qty(99);

                            });
                            customerData.reload();

                            $.when(deferred).done(function () {
                                totals.isLoading(false);

                            });
                            fullScreenLoader.stopLoader();

                            var summaryCount = $('div.items-in-cart').find('span').first();
                            var itemInCart = $('.counter-number').html();
                            var thenum = itemInCart.replace(/^\D+/g, ''); // replace all leading non-digits with nothing

                            summaryCount.html(thenum);
                        }
                    });

                    self.selectedWrapperArray.splice(i, 1);
                }
            }
        },

        /**
         * Show popup dialog
         */
        showModal: function () {
            this.getPopUp().openModal();
        },

        /**
         * @returns {boolean}
         */
        showInbox: function () {
            return true;
        },

        /**
         * @returns {boolean}
         */
        showOutbox: function () {
            return true;
        },

        clearInput: function () {
            var radios = $.find(".wrapper-radio");
            $(radios).each(function () {
                $(this).prop('checked', false);
            });
        },

        /**
         * @returns {*}
         */
        getPopUp: function () {
            var self = this;
            if (!popUp) {
                options.buttons = [
                    {
                        text: $t('Cancel'),
                        class: 'action secondary action-hide-popup',
                        click: function () {
                            this.closeModal();
                        }
                    },
                    {
                        text: $t('Save Selection'),
                        class: 'action primary action-save-address',
                        click: self.saveSelectedWrapper.bind(self)
                    }
                ];

                options.opened = function () {
                    self.isModalOpened(true);
                };

                options.closed = function () {
                    self.clearInput();
                    self.isModalOpened(false);
                };

                popUp = modal(options, $('#wrapper-popup-modal'));
            }
            return popUp;
        },

        /**
         * Save wrapper on clicking Save
         */
        saveSelectedWrapper: function () {
            var self = this;
            var selected = 0;
            var giftCardSelected = 0;
            var product_selected = 0;
            var message_selected = 0;
            fullScreenLoader.startLoader(true);

            var obj = {};

            var i = 0;
            var trElements = $('#wrapper-popup-table').find('tbody').find('tr');
            $(trElements).each(function (index, element) {
                var radio = $(element).find('input');
                if (radio.is(':checked')) {
                    obj = {
                        id: self.wrapperArray()[i].id,
                        name: self.wrapperArray()[i].name,
                        price: self.wrapperArray()[i].price,
                        image: self.wrapperArray()[i].image,
                        description: self.wrapperArray()[i].description,
                        wrapper_id: self.quoteId.concat(wrapperCounter),
                        onclick: self.deleteWrapper.bind(self)
                    };

                    i++;
                    selected = 1;
                } else {
                    i++;
                }
            });

            i = 0;
            var giftCardElements = $('#gift_card_wrapper_selector').find('tbody').find('tr');
            $(giftCardElements).each(function (index, element) {
                var radio = $(element).find('input');
                if (radio.is(':checked')) {
                    obj.giftCard_id = self.giftCardArray()[i].id;
                    giftCardSelected = 1;
                }
                ++i;
            });

            var messageTrElements = $('#wrapper-popup-message-table').find('tbody').find('tr');
            $(messageTrElements).each(function (index, element) {
                var checkbox = $(element).find('input');
                if (checkbox.is(':checked')) {
                    var textArea = $(element).find('textarea');
                    if (textArea.attr('name') === 'inboxTextArea') {
                        obj.inbox = textArea.val();
                    } else if (textArea.attr('name') === 'outboxTextArea') {
                        obj.outbox = textArea.val();
                    }
                    textArea.val('');
                }
                checkbox.prop('checked', false);
            });

            if (obj.outbox === undefined || obj.outbox.length === 0) {
                obj.outbox = '';
            } else {
                message_selected++;
            }
            if (obj.inbox === undefined || obj.inbox.length === 0) {
                obj.inbox = '';
            } else {
                message_selected++;
            }

            obj.isMessageVisible = message_selected !== 0;

            if (selected === 0 && product_selected === 0) {
                window.alert('Please select a wrapper');
                fullScreenLoader.stopLoader();
            } else if (selected === 1 && product_selected === 0) {
                product_selected++;
                var itemTrElements = $('#wrapper-popup-product-selection-table').find('tbody').find('tr');

                obj.qtyOptions = [];

                $(itemTrElements).each(function (index, element) {
                    var select = $(element).find('select');
                    var selectedItemId = select.attr('data-qty');
                    i = 0;
                    while (i < self.productArray().length) {
                        if (selectedItemId === self.productArray()[i].item_id) {
                            var fullName = self.productArray()[i].item_name;
                            for (var x = 0; x < self.productArray()[i].options.length; x++) {
                                fullName += '-';
                                fullName += self.productArray()[i].options[x].value;
                            }
                            obj.qtyOptions.push(
                                {
                                    item_id: self.productArray()[i].item_id,
                                    item_name: self.productArray()[i].item_name,
                                    qty: select.val(),
                                    hasOptions: self.productArray()[i].hasOptions,
                                    full_name: fullName
                                }
                            );
                            if (select.val() > 0) {
                                product_selected++;
                            }

                            // Generate a new array for cart item dropdown
                            self.productArray()[i].quantity -= select.val();
                            if (self.productArray()[i].quantity === 0) {
                                self.productArray()[i].isVisible = false;
                            }
                            // self.productArray()[i].qtyArray([]);

                            // for (var j = 0; j <= self.productArray()[i].quantity; j++) {
                            //     self.productArray()[i].qtyArray.push(j);
                            // }
                        }
                        i++;
                    }

                });

                if (selected === 1 && product_selected === 1) {
                    window.alert('Please choose at least 1 item in cart to wrap');
                    fullScreenLoader.stopLoader();
                }
                else if (selected === 1 && product_selected > 1) {
                    i = 0;
                    $(trElements).each(function (index, element) {
                        var radio = $(element).find('input');
                        if (radio.is(':checked')) {
                            radio.prop('checked', false);
                        }
                        i++;
                    });
                    self.selectedWrapperArray.push(obj);
                    wrapperCounter++;
                    this.getPopUp().closeModal();
                    self.isSelectedWrapperVisible(true);
                    self.isChecked(true);

                    /////start refactoring

                    /** add the box first**/
                        //todo
                    var addToCartUrl = baseUrl.concat('checkout/cart/add');

                    var box = {
                        product: obj.id
                    };

                    var item_in_box = {};
                    //AdditionalOptions

                    ko.utils.arrayForEach(obj.qtyOptions, function (item) {
                        var itemName = item.item_name;
                        var itemQty = item.qty;
                        var itemId = item.item_id;

                        item_in_box[itemName] = itemQty;
                    });

                    if (obj.inbox.length > 0) item_in_box['Inbox'] = obj.inbox;
                    if (obj.outbox.length > 0) item_in_box['Outbox'] = obj.outbox;

                    box.additional_options = item_in_box;
                    box['wrapper_custom_id'] = obj.wrapper_id;

                    var seconds_box = new Date().getTime() / 1000;
                    box['added_time'] = seconds_box;

                    box['form_key'] = window.checkoutConfig.formKey;
                    $.ajax({
                        type: "POST",
                        url: addToCartUrl,
                        data: box,
                        async: false
                    });

                    //send the ajax request to add the card
                    var card = {
                        product: obj.giftCard_id,
                        qty: 1
                    };

                    var cardInfo = {};

                    ko.utils.arrayForEach(obj.qtyOptions, function (item) {
                        var itemName = item.item_name;
                        var itemQty = item.qty;
                        var itemId = item.item_id;

                        cardInfo[itemName] = itemQty;
                    });


                    card.additional_options = cardInfo;
                    card['wrapper_custom_id'] = obj.wrapper_id;
                    card['gift_card_wrapper_id'] = obj.giftCard_id;

                    var seconds_card = new Date().getTime() / 1000;

                    card['added_time'] = seconds_card;

                    card['form_key'] = window.checkoutConfig.formKey;

                    $.ajax({
                        type: "POST",
                        url: addToCartUrl,
                        data: card,
                        async: false
                    });

                    // var card['qty']
                    obj['box_added_time'] = seconds_box;
                    obj['card_added_time'] = seconds_card;

                    var requestUrl = baseUrl.concat('wrapper/checkout/createwrapper');
                    $.ajax({
                        type: "POST",
                        url: requestUrl,
                        data: {
                            new_item: JSON.stringify(obj)
                        },
                        async: false
                    }).done(function (response) {
                        if (response && response.result == true) {
                            var deferred = $.Deferred();

                            totals.isLoading(true);
                            getPaymentInformationAction(deferred);
                            //update summary
                            getTotals(function () {
                                // console.log(totals.items_qty);
                                // totals.totals().items_qty(99);
                            });

                            //calculate the qty of


                            customerData.reload();

                            $.when(deferred).done(function () {
                                //alert(customerData.keys('summary_count'));

                                totals.isLoading(false);
                            });


                            fullScreenLoader.stopLoader();


                            var summaryCount = $('div.items-in-cart').find('span').first();
                            var itemInCart = $('.counter-number').html();
                            var thenum = itemInCart.replace(/^\D+/g, ''); // replace all leading non-digits with nothing

                            summaryCount.html(itemInCart);
                            // messageContainer.addSuccessMessage({'message': message});
                        }
                    }); //end of processing add item to cart

                    //////////////end refactoring
                }
            }
        }
    });
});
