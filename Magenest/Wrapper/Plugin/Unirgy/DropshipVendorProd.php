<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Plugin\Unirgy;

/**
 * Class DropshipVendorProd
 * @package Magenest\Wrapper\Plugin\Unirgy
 */
class DropshipVendorProd
{
    protected $_giftWrapTypeName = [
        'giftwrap',
        'giftwrapper',
        'wrapper'
    ];

    protected $_giftCardTypeName = [
        'postcard',
        'greetingcard'
    ];

    /**
     * @param \Unirgy\DropshipVendorProduct\Model\Product $subject
     */
    public function beforeBeforeSave(\Unirgy\DropshipVendorProduct\Model\Product $subject)
    {
        $setId = $subject->getData('udprod_attribute_set_key');
        if (!$setId) {
            return;
        }
        list(, $typeId) = explode('-', $setId);
        // remove whitespace & lower string
        $typeId = strtolower(preg_replace('/\s+/', '', $typeId));
        if (in_array($typeId, $this->_giftWrapTypeName)) {
            $subject->setTypeId('wrapper');
        } elseif (in_array($typeId, $this->_giftCardTypeName)) {
            $subject->setTypeId('wrapper_gift_card');
        }
    }
}
