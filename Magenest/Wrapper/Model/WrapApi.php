<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/27/18
 * Time: 1:32 PM
 */

namespace Magenest\Wrapper\Model;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class WrapApi
 * @package Magenest\Wrapper\Model
 */
class WrapApi implements \Magenest\Wrapper\Api\WrapApiInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cart
    ) {
        $this->cart = $cart;
        $this->storeManager = $storeManager;
        $this->cartRepository = $cartRepository;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function wrap($cartId, $wrapperProductId, $productOptions, $messages = null)
    {
        $wrapper = $this->_initProduct($wrapperProductId);
        if ($wrapper === false) {
            return __("Can't get product")->getText();
        }
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->get($cartId);
        $this->_initSession($quote);
//        $quote = $this->cart->getQuote();
        $requestInfo = $this->_initRequestInfo($productOptions, $messages);
        $requestInfo['product'] = $wrapperProductId;
        $requestInfo['qty'] = 1;
        $request = new \Magento\Framework\DataObject($requestInfo);
        $this->cart->addProduct($wrapper, $request);
//        $this->cart->save();
        $item = $quote->getItemByProduct($wrapper);
        $item->addOption([
            'product_id' => $wrapperProductId,
            'code' => 'additional_options',
            'value' => json_encode($requestInfo['additional_options'])
        ]);
        $this->cart->save();
        return "Ok";
    }

    /**
     * @param array $prodOptions
     * @param array|null $messages
     * @return array
     */
    protected function _initRequestInfo($prodOptions, $messages)
    {
        $return = [];
        $return['additional_options'] = [];
        foreach ($prodOptions as $option) {
            if (isset($option['item_id'], $option['qty'])) {
                $return['additional_options'][] = [
                    'label' => $this->getProductName($option['item_id']),
                    'value' => $option['qty']
                ];
            }
        }
        if (is_array($messages)) {
            if (isset($messages['inbox'])) {
                $return['additional_options'][] = [
                    'label' => 'Inbox',
                    'value' => $messages['inbox']
                ];
            }
            if (isset($messages['outbox'])) {
                $return['additional_options'][] = [
                    'label' => 'Outbox',
                    'value' => $messages['outbox']
                ];
            }
        }
        return $return;
    }

    /**
     * @param string $itemId
     * @return string
     */
    protected function getProductName($itemId)
    {
        return $this->cart->getQuote()->getItemById($itemId)->getName();
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface $cart
     */
    protected function _initSession($cart)
    {
        if ($cart->getCustomer()->getId()) {
            $this->checkoutSession->setCustomerData($cart->getCustomer());
        } else {
            $this->customerSession->setCustomerId(null);
        }
        $this->checkoutSession->setQuoteId($cart->getId());
    }

    /**
     * @param $productId
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface
     */
    protected function _initProduct($productId)
    {
        if ($productId) {
            $storeId = $this->storeManager->getStore()->getId();
            try {
                return $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }
}
