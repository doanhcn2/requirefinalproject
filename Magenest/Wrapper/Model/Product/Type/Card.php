<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Model\Product\Type;

/**
 * Class Card
 * @package Magenest\Wrapper\Model\Product\Type
 */
class Card extends \Magento\Catalog\Model\Product\Type\AbstractType
{
    /**
     * @param \Magento\Catalog\Model\Product $product
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
    }

    /**
     * @return bool
     */
    public function hasWeight()
    {
        return false;
    }
}
