<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Model\Product\Type;

/**
 * Class Price
 *
 * @package Magenest\Wrapper\Model\Product\Type
 */
class Price extends \Magento\Catalog\Model\Product\Type\Price
{

}
