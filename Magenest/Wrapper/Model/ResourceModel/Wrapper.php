<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Model\ResourceModel;

/**
 * Class Wrapper
 * @package Magenest\Wrapper\Model\ResourceModel
 */
class Wrapper extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_wrapper_quote', 'id');
    }
}
