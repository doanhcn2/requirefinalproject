<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Model\ResourceModel\Wrapper;

/**
 * Class Collection
 * @package Magenest\Wrapper\Model\ResourceModel\Wrapper
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Magenest\Wrapper\Model\Wrapper', 'Magenest\Wrapper\Model\ResourceModel\Wrapper');
    }
}
