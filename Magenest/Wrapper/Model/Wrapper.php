<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Model;

use Magenest\Wrapper\Model\ResourceModel\Wrapper as ResourceWrapper;
use Magenest\Wrapper\Model\ResourceModel\Wrapper\Collection as Collection;
use Magenest\Wrapper\Helper\Util;

/**
 * Class Wrapper
 * @package Magenest\Wrapper\Model
 */
class Wrapper extends \Magento\Framework\Model\AbstractModel
{
    /** @var string $_eventPrefix */
    protected $_eventPrefix = 'wrapper';
    protected $_inboxFieldName = 'wrapper_inbox';
    protected $_outBoxFieldName = 'wrapper_outbox';
    protected $_wrapperIdFieldName = 'wrapper_id';
    protected $_cardIdFieldName = 'gift_card_wrapper_id';
    protected $_wrapperOptionsFieldName = 'wrapper_options';
    protected $_wrapperQuoteIdFieldName = 'wrapper_quote_id';
    protected $_cardQuoteIdFieldName = 'card_quote_id';

    /** @var \Magento\Catalog\Model\Product $_productModel */
    protected $_productModel;

    /** @var \Magento\Checkout\Model\Cart $cart */
    protected $cart;

    /**
     * Wrapper constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceWrapper $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ResourceWrapper $resource,
        Collection $resourceCollection,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Cart $cart,
        array $data = []
    ) {
        $this->cart = $cart;
        $this->_productModel = $product;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param int $quoteId
     * @return \Magenest\Wrapper\Model\Wrapper[]
     */
    public function loadByQuoteId($quoteId)
    {
        $objM = \Magento\Framework\App\ObjectManager::getInstance();
        $return = [];
        $wrapperIds = $this->getCollection()
            ->addFieldToFilter('quote_id', $quoteId)
            ->getAllIds();
        foreach ($wrapperIds as $id) {
            $return[] = $objM->create('Magenest\Wrapper\Model\Wrapper')->load($id);
        }

        return $return;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return Util::parse($this->getData($this->_wrapperOptionsFieldName));
    }

    /**
     * @return mixed
     */
    public function getInbox()
    {
        return $this->getData($this->_inboxFieldName);
    }

    /**
     * @return mixed
     */
    public function getOutbox()
    {
        return $this->getData($this->_outBoxFieldName);
    }

    /**
     * @return $this
     */
    public function getWrapperProduct()
    {
        return $this->_productModel->load($this->getData($this->_wrapperIdFieldName));
    }

    /**
     * @return $this|null
     */
    public function getCardProduct()
    {
        $cardId = $this->getData($this->_cardIdFieldName);

        return $cardId ? $this->_productModel->load($cardId) : null;
    }

    public function removeFromQuote()
    {
        $this->delete();
        $this->cart->removeItem($this->getData($this->_wrapperQuoteIdFieldName));
        $cardQuoteId = $this->getData($this->_cardQuoteIdFieldName);
        if (!empty($cardQuoteId)) {
            $this->cart->removeItem($cardQuoteId);
        }
        $this->cart->save();
        $this->cart->getCheckoutSession()->setCartWasUpdated(true);
    }

    protected function _construct()
    {
        $this->_init('Magenest\Wrapper\Model\ResourceModel\Wrapper');
    }
}
