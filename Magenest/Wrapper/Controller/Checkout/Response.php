<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Wrapper\Helper\Util;

/**
 * Class Response
 * @package Magenest\Wrapper\Controller\Checkout
 */
class Response extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\Wrapper\Model\WrapperFactory
     */
    protected $_wrapperFactory;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory
     */
    public function __construct(
        Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory
    ) {
    
        $this->_cart = $cart;
        $this->_logger = $logger;
        $this->_productFactory = $productFactory;
        $this->_wrapperFactory = $wrapperFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('new_item');
        $model = $this->_productFactory->create();
        /** @var \Magenest\Wrapper\Model\Wrapper $wrapperModel */
        $wrapperModel = $this->_wrapperFactory->create();
        $checkoutSession = $this->_cart->getCheckoutSession();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $obj = Util::parse($data);
            $quoteID = $checkoutSession->getQuoteId();
            foreach ($obj->qtyOptions as $key => $qtyOption) {
                if ($qtyOption->qty == 0) {
                    unset($obj->qtyOptions[$key]);
                }
            }
            $product = $model->load($obj->id);
            $option_array = [];
            if ($obj->inbox != '') {
                $option_array['Inbox'] = $obj->inbox;
            }
            if ($obj->outbox != '') {
                $option_array['Outbox'] = $obj->outbox;
            }
            foreach ($obj->qtyOptions as $qtyOption) {
                $option_array[$qtyOption->full_name] = $qtyOption->qty;
            }

            $params = [
                'wrapper_custom_id' => $obj->wrapper_id,
                'qty' => 1,
                'additional_options' => $option_array
            ];
            if ($product) {
                try {
                    $this->_cart->addProduct($product, $params);
                    $this->_cart->save();
                    /** @noinspection PhpUndefinedMethodInspection */
                    $wrapperQuoteId = $this->_cart->getItems()->getLastItem()->getId();
                    $cardQuoteId = 0;
                    if (!empty($obj->giftCard_id)) {
                        $giftCardProduct = $this->_productFactory->create()->load($obj->giftCard_id);
                        $giftCardParams = [
                            'qty' => 1,
                            'additional_options' => $option_array
                        ];
                        $params['gift_card_wrapper_id'] = $obj->giftCard_id;
                        $this->_cart->addProduct($giftCardProduct, $giftCardParams);
                        $this->_cart->save();
                        /** @noinspection PhpUndefinedMethodInspection */
                        $cardQuoteId = $this->_cart->getItems()->getLastItem()->getId();
                    } else {
                        $obj->giftCard_id = 0;
                    }

                    $quoteData = [
                        'wrapper_id' => $obj->id,
                        'quote_id' => $quoteID,
                        'wrapper_quote_id' => $wrapperQuoteId,
                        'wrapper_custom_id' => $obj->wrapper_id,
                        'wrapper_inbox' => $obj->inbox,
                        'wrapper_outbox' => $obj->outbox,
                        'wrapper_options' => Util::stringify($obj->qtyOptions),
                        'gift_card_wrapper_id' => $obj->giftCard_id,
                        'card_quote_id' => $cardQuoteId
                    ];

                    $wrapperModel->setData($quoteData)->save();
                    /** @noinspection PhpUndefinedMethodInspection */
                    $checkoutSession->setCartWasUpdated(true);
                    $resultJson->setData(['result' => true]);

                    return $resultJson;
                } catch (\Exception $e) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $this->_logger->addException($e, __('error.'));
                }
            }
        }
        $resultJson->setData(['result' => false]);

        return $resultJson;
    }
}
