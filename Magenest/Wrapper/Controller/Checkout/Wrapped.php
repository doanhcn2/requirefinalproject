<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface as Logger;
use Magenest\Wrapper\Helper\Util;

/**
 * Class Wrapped
 * @package Magenest\Wrapper\Controller\Checkout
 */
class Wrapped extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magenest\Wrapper\Model\WrapperFactory
     */
    protected $_wrapperFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * Wrapped constructor.
     *
     * @param Logger $logger
     * @param \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        Logger $logger,
        \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->_logger = $logger;
        $this->_productFactory = $productFactory;
        $this->_wrapperFactory = $wrapperFactory;
        $this->_imageHelper = $imageHelper;
        parent::__construct($context);
    }

    /**
     * Sending wrappable cart item ids action
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('quote_id');
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        /** @var $model \Magenest\Wrapper\Model\Wrapper */
        $model = $this->_wrapperFactory->create();
        /** @var \Magento\Catalog\Model\Product $productModel */
        $productModel = $this->_productFactory->create();
        $sendData = [];

        if ($data) {
            $quoteId = intval($data);
            $wrapperCollection = $model->getCollection()->addFieldToFilter('quote_id', $quoteId);
            $wrapperData = $wrapperCollection->getData();

            foreach ($wrapperData as $wrapper) {
                $product = $productModel->load($wrapper['wrapper_id']);
                $description = strip_tags($product->getDescription());
                $name = $product->getName();
                $imageUrl = $this->_imageHelper->init($product, 'product_page_image_small')->getUrl();
                $price = number_format($product->getPrice(), 2);

                if ($wrapper['wrapper_inbox'] == null) {
                    $wrapper['wrapper_inbox'] = '';
                }
                if ($wrapper['wrapper_outbox'] == null) {
                    $wrapper['wrapper_outbox'] = '';
                }
                if ($wrapper['wrapper_inbox'] == '' && $wrapper['wrapper_outbox'] == '') {
                    $hasMessage = 0;
                } else {
                    $hasMessage = 1;
                }

                $newArray = [
                    'quote_id' => $wrapper['quote_id'],
                    'id' => $wrapper['wrapper_id'],
                    'name' => $name,
                    'price' => $price,
                    'image' => $imageUrl,
                    'description' => $description,
                    'wrapper_id' => $wrapper['wrapper_custom_id'],
                    'inbox' => $wrapper['wrapper_inbox'],
                    'outbox' => $wrapper['wrapper_outbox'],
                    'qtyOptions' => Util::parse($wrapper['wrapper_options']),
                    'isMessageVisible' => $hasMessage
                ];
                array_push($sendData, $newArray);
            }
        }

        $resultJson->setData($sendData);

        return $resultJson;
    }
}
