<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Delete
 * @package Magenest\Wrapper\Controller\Checkout
 */
class Delete extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magenest\Wrapper\Model\WrapperFactory
     */
    protected $_wrapperFactory;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var \Magento\Quote\Model\Quote\Item\OptionFactory
     */
    protected $_optionFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Quote\Model\Quote\Item\OptionFactory $optionFactory
     * @param \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory
     */
    public function __construct(
        Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Model\Quote\Item\OptionFactory $optionFactory,
        \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_logger = $logger;
        $this->cart = $cart;
        $this->_optionFactory = $optionFactory;
        $this->_wrapperFactory = $wrapperFactory;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * Delete a selected wrapper action
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('id');
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        /** @var \Magenest\Wrapper\Model\Wrapper $wrapperModel */
        $wrapperModel = $this->_wrapperFactory->create();

        if ($data) {
            $wrapperId = intval($data);

            $wrapperData = $wrapperModel->getCollection()->addFieldToFilter('wrapper_custom_id', $wrapperId)->getFirstItem();
            $id = $wrapperData[$wrapperModel->getIdFieldName()];
            $wrapper = $wrapperModel->load($id);
            $wrapper->removeFromQuote();
            $this->checkoutSession->getQuote()->collectTotals();

            $resultJson->setData(['result' => true]);
            return $resultJson;
        }
        $resultJson->setData(['result' => false]);

        return $resultJson;
    }
}
