<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface as Logger;
use Magenest\Wrapper\Helper\Util;

/**
 * Class CreateWrapper
 * @package Magenest\Wrapper\Controller\Checkout
 */
class CreateWrapper extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\Wrapper\Model\WrapperFactory
     */
    protected $_wrapperFactory;


    /**
     * @var \Magenest\Wrapper\Helper\Data
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory
     */
    public function __construct(
        Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magenest\Wrapper\Model\WrapperFactory $wrapperFactory,
        \Magenest\Wrapper\Helper\Data $helper
    ) {

        $this->_cart = $cart;
        $this->_logger = $logger;
        $this->_productFactory = $productFactory;
        $this->_wrapperFactory = $wrapperFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Receive selected wrappers data action
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('new_item');

        /** @var \Magenest\Wrapper\Model\Wrapper $wrapperModel */
        $wrapperModel = $this->_wrapperFactory->create();
        $checkoutSession = $this->_cart->getCheckoutSession();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $obj = json_decode($data);
            $box_added_time = $obj->box_added_time;
            $card_added_time = $obj->card_added_time;
            $quoteID = $checkoutSession->getQuoteId();

            foreach ($obj->qtyOptions as $key => $qtyOption) {
                if ($qtyOption->qty == 0) {
                    unset($obj->qtyOptions[$key]);
                }
            }

            try {

                /** @noinspection PhpUndefinedMethodInspection */
                $wrapperQuoteId = $this->helper->getQuoteItemByTime($box_added_time);
                $cardQuoteId = 0;
                if (!empty($obj->giftCard_id)) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $cardQuoteId = $this->helper->getQuoteItemByTime($card_added_time);
                } else {
                    $obj->giftCard_id = 0;
                }

                $quoteData = [
                    'wrapper_id' => $obj->id,
                    'quote_id' => $quoteID,
                    'wrapper_quote_id' => $wrapperQuoteId,
                    'wrapper_custom_id' => $obj->wrapper_id,
                    'wrapper_inbox' => $obj->inbox,
                    'wrapper_outbox' => $obj->outbox,
                    'wrapper_options' => Util::stringify($obj->qtyOptions),
                    'gift_card_wrapper_id' => $obj->giftCard_id,
                    'card_quote_id' => $cardQuoteId
                ];

                $wrapperModel->setData($quoteData)->save();
                $resultJson->setData(['result' => true]);
                return $resultJson;
            } catch (\Exception $e) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->_logger->addException($e, __('error.'));
            }
        }
        $resultJson->setData(['result' => false]);

        return $resultJson;
    }
}
