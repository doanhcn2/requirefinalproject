<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Magento\Framework\Controller\ResultFactory;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class Items
 * @package Magenest\Wrapper\Controller\Checkout
 */
class Items extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Quote\Model\Quote\ItemFactory
     */
    protected $_itemFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Wrappable Product Types
     * @var array
     */
    protected $_wrappableType = ['simple', 'configurable', 'bundle', 'grouped'];

    /**
     * Items constructor.
     * @param Logger $logger
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Quote\Model\Quote\ItemFactory $itemFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        Logger $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\Quote\ItemFactory $itemFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_productFactory = $productFactory;
        $this->_logger = $logger;
        $this->_itemFactory = $itemFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * Sending wrappable cart item ids
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('quote_id');
        $wrappable = [];
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($data) {
            $quote = $this->_checkoutSession->getQuote();
            $itemData = $quote->getItemsCollection();

            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($itemData as $item) {
                /** @var $product \Magento\Catalog\Model\Product */
                $product = $item->getProduct();
                $product = $product->load($product->getId());
                $canWrapped = $product->getData('can_be_wrapped');
                $typeId = $product->getTypeId();

                if (!in_array($typeId, $this->_wrappableType) || !$canWrapped) {
                    continue;
                }
                array_push($wrappable, ['item_id' => $item->getId()]);
            }
        }
        $resultJson->setData($wrappable);

        return $resultJson;
    }
}
