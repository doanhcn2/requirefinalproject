<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Controller\Checkout;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class WrapperAndCard
 * @package Magenest\Wrapper\Controller\Checkout
 */
class WrapperAndCard extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $_currencyFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * WrapperAndCard constructor.
     * @param Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_currencyFactory = $currencyFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response = [
            'wraps' => [],
            'cards' => []
        ];
//        $productIds = [];
//        $quote = $this->_checkoutSession->getQuote();
//        $items = $quote->getItemsCollection();
//
//        /** @var \Magento\Quote\Model\Quote\Item $item */
//        foreach ($items as $item) {
//            $vendorData = $item->getOptionByCode('udmp_vendor_data')->getValue();
//            $dataObj = json_decode($vendorData);
//            $vendorId = $dataObj->vendor_id;
//            /** @var  $hlp \Unirgy\Dropship\Helper\Data */
//            $hlp = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data');
//            $v = $hlp->getVendor($vendorId);
//            $productIds = array_merge($productIds, $v->getAssociatedProductIds());
//        }

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection
            ->addAttributeToSelect(['price', 'description', 'name', 'image'])
            ->addAttributeToFilter('type_id', ['wrapper', 'wrapper_gift_card']);
//            ->addAttributeToFilter('entity_id', ['in' => $productIds]);
        /** @var \Magento\Catalog\Model\Product[] $data */
        $data = $productCollection->getItems();

        foreach ($data as $product) {
            if (!$product->isSalable() || $product->getStatus() !== \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED) {
                continue;
            }

            $description = strip_tags($product->getDescription());
            $name = $product->getName();
            $price = $product->getPrice();
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();

            /** @var \Magento\Directory\Model\Currency $currencyModel */
            $currencyModel = $this->_currencyFactory->create();
            $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
            $newPrice = $currencyModel->load($currencyCode)->formatTxt($price * $productCollection->getCurrencyRate());

            $responseData = [
                'id' => $product->getId(),
                'name' => $name,
                'price' => $newPrice,
                'image' => $imageUrl,
                'description' => $description,
            ];
            if ($product->getTypeId() == 'wrapper') {
                array_push($response['wraps'], $responseData);
            } else {
                array_push($response['cards'], $responseData);
            }
        }
        $resultJson->setData($response);

        return $resultJson;
    }
}
