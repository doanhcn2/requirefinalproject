<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\CustomerData;

/**
 * Class DefaultItem
 * @package Magenest\Wrapper\CustomerData
 */
class DefaultItem
{
    /**
     * @param \Magento\Checkout\CustomerData\DefaultItem $item
     * @param $result
     * @return mixed
     */
    public function afterGetItemData(\Magento\Checkout\CustomerData\DefaultItem $item, $result)
    {
        $result['is_wrapped'] = 'yes';
        $result['is_wrapper'] = 'yes';

        return $result;
    }

    /**
     * @param $result
     * @return mixed
     */
    protected function afterDoGetItemData( $result)
    {
        $result['is_wrapped'] = 'yes';
        $result['is_wrapper'] = 'yes';

        return $result;
    }


}