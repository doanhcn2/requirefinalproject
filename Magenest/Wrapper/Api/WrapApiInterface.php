<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/27/18
 * Time: 1:29 PM
 */

namespace Magenest\Wrapper\Api;

/**
 * Interface WrapInterface
 * @package Magenest\Wrapper\Api
 */
interface WrapApiInterface
{
    /**
     * @param string $cartId
     * @param string $wrapperProductId
     * @param mixed $productOptions
     * @param mixed|null $messages
     * @return string
     */
    public function wrap($cartId, $wrapperProductId, $productOptions, $messages = null);
}
