<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Delete
 * @package Magenest\Wrapper\Observer\Quote
 */
class Delete implements ObserverInterface
{
    /**
     * @var \Magenest\Wrapper\Model\Wrapper
     */
    protected $wrapper;

    /**
     * Delete constructor.
     * @param \Magenest\Wrapper\Model\Wrapper $wrapper
     */
    public function __construct(
        \Magenest\Wrapper\Model\Wrapper $wrapper
    ) {
        $this->wrapper = $wrapper;
    }

    /**
     * @param $id
     * @param $quote
     * @return \Magenest\Wrapper\Model\Wrapper
     */
    private function getAssociatedWrapper($id, $quote)
    {
        $quoteId = $quote->getId();
        $wrappers = $this->wrapper->loadByQuoteId($quoteId);

        if (count($wrappers) > 0) {
            foreach ($wrappers as $wrapper) {
                $box_quote_id = $wrapper->getData('wrapper_quote_id');
                $card_quote_id = $wrapper->getData('card_quote_id');
                if ($id == $box_quote_id) return $wrapper;
                if ($id == $card_quote_id) return $wrapper;
                $products = $wrapper->getProducts();

                foreach ($products as $product) {
                    if ($product->item_id == $id) {
                        return $wrapper;
                    }
                }
            }
        }
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote\Item $quote */
        $quoteItem = $observer->getData('quote_item');
        $deletedItemId = $quoteItem->getId();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $quoteItem->getQuote();
        $wrapper = $this->getAssociatedWrapper($deletedItemId,$quote);

        if (is_object($wrapper) && $wrapper->getId()) {
            $wrapper->removeFromQuote();
        }
    }

    /**
     * This method will remove the gift wrapper if all item is wrapped are deleted
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function executeExtend(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote\Item $quote */
        $quoteItem = $observer->getData('quote_item');
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $quoteItem->getQuote();
        $quoteId = $quote->getId();
        $items = $quote->getAllItems();
        $wrappers = $this->wrapper->loadByQuoteId($quoteId);

        foreach ($wrappers as $wrapper) {
            $products = $wrapper->getProducts();
            foreach ($products as $product) {
                foreach ($items as $item) {
                    if ($item == $quoteItem) {
                        continue;
                    }
                    if ($item->getData('item_id') == $product->item_id) {
                        $wrapper->setData('keep', true);
                        break;
                    }
                }
            }
            if (!$wrapper->getData('keep')) {
                $wrapper->removeFromQuote();
            }
        }
    }
}
