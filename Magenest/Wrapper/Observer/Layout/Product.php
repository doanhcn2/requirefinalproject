<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class Product
 * @package Magenest\Wrapper\Observer\Layout
 */
class Product implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * Product constructor.
     *
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger
    ) {
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if ($block->getNameInLayout() =='product.info.review') {
            $block->setTemplate('Magenest_Wrapper::catalog/product/wrapper.phtml');
        }
    }
}
