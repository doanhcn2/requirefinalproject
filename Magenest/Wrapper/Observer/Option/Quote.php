<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Observer\Option;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class Quote
 * @package Magenest\Wrapper\Observer\Option
 */
class Quote implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var  $order \Magento\Sales\Model\Order */
        $order = $observer->getEvent()->getOrder();
        /** @var  $quote \Magento\Quote\Model\Quote */
        $quote = $observer->getEvent()->getQuote();

        /** @var  $quoteItem \Magento\Quote\Model\Quote\Item */
        foreach ($quote->getAllItems() as $quoteItem) {
            if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
                /** @var  $orderItem \Magento\Sales\Model\Order\Item */
                $orderItem = $order->getItemByQuoteItemId($quoteItem->getId());
                $options = $orderItem->getProductOptions();
                $options ['additional_options'] = json_decode($additionalOptions->getValue());
                $orderItem->setProductOptions($options);
            }
        }
    }
}
