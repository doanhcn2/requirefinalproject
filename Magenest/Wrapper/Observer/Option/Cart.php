<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Observer\Option;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface as Logger;
use Magenest\Wrapper\Helper\Util;

/**
 * Class Cart
 * @package Magenest\Wrapper\Observer\Option
 */
class Cart implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @param Logger $logger
     */
    public function _construct(
        Logger $logger
    )
    {
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $item \Magento\Quote\Model\Quote\Item */
        $item = $observer->getEvent()->getQuoteItem();
        $buyInfo = $item->getBuyRequest();
        if ($item->getProductType() !== 'ticket' && $item->getProductType() !== 'configurable') {
            if ($options = $buyInfo->getAdditionalOptions()) {
                $additionalOptions = [];

                foreach ($options as $key => $value) {
                    $additionalOptions[] = [
                        'label' => $key,
                        'value' => $value
                    ];
                }
                /** @noinspection PhpParamsInspection */
                $item->addOption([
                    'code' => 'additional_options',
                    'value' => Util::stringify($additionalOptions)
                ]);
            }
        }
    }
}
