<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Observer\Product;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class Save
 * @package Magenest\Wrapper\Observer\Product
 */
class Save implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_productVisibility;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * Save constructor.
     *
     * @param Logger $logger
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     */
    public function __construct(
        Logger $logger,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
        $this->_request = $request;
        $this->_logger = $logger;
        $this->_productVisibility = $productVisibility;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getProduct();
        if ($product->getTypeId() === 'configurable') {
            $value = '';
            $attribute = $product->getResource()->getAttribute('can_be_wrapped');
            if ($attribute) {
                $value = $attribute->getFrontend()->getValue($product);
            }

            /** @var $productTypeInstance \Magento\ConfigurableProduct\Model\Product\Type\Configurable */
            $productTypeInstance = $product->getTypeInstance();
            $productTypeInstance->setStoreFilter($product->getStoreId(), $product);
            if ($value && @$value->getText() === 'Yes') {
                foreach ($productTypeInstance->getUsedProducts($product) as $child) {
                    /** @var $child \Magento\Catalog\Model\Product */
                    $child->setData('can_be_wrapped', 1);
                    $child->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
                    $child->save();
                }
            }
            if (!$value || @$value->getText() === 'No') {
                foreach ($productTypeInstance->getUsedProducts($product) as $child) {
                    $child->setData('can_be_wrapped', 0);
                    $child->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
                    $child->save();
                }
            }
        }
    }
}
