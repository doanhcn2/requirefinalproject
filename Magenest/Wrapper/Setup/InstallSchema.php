<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Wrapper\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table as Table;

/**
 * Class InstallSchema
 * @package Magenest\Wrapper\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_wrapper_quote'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'quote_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Quote ID'
            )
            ->addColumn(
                'wrapper_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Wrapper ID'
            )
            ->addColumn(
                'wrapper_custom_id',
                Table::TYPE_NUMERIC,
                null,
                [],
                'Wrapper Custom ID'
            )
            ->addColumn(
                'wrapper_inbox',
                Table::TYPE_TEXT,
                null,
                [],
                'Wrapper Inbox'
            )
            ->addColumn(
                'wrapper_outbox',
                Table::TYPE_TEXT,
                null,
                [],
                'Wrapper Outbox'
            )
            ->addColumn(
                'wrapper_options',
                Table::TYPE_TEXT,
                null,
                [],
                'Wrapper Options'
            )
            ->addColumn(
                'gift_card_wrapper_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Wrapper Gift Card ID'
            )
            ->addColumn(
                'date_created',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false],
                'Date created'
            )
            ->setComment('Wrapper Created Date');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
