/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Reward/js/view/summary/reward'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magenest_RewardPoints/summary/reward'
        },
        rewardPointsRemoveUrl: window.checkoutConfig.review.reward.removeUrl
    });
});
