<?php
namespace Magenest\RewardPoints\Observer;

class OrderCompleted extends \Magento\Reward\Observer\OrderCompleted
{
    protected function _isOrderPaidNow($order)
    {
        $isOrderPaid = (double)$order->getBaseTotalPaid() >= 0 &&
            $order->getBaseGrandTotal() - $order->getBaseSubtotalCanceled() - $order->getBaseTotalPaid() < 0.0001;

        if (!$order->getOrigData('base_grand_total')) {
            //New order with "Sale" payment action
            return $isOrderPaid;
        }

        return $isOrderPaid && $order->getOrigData(
                'base_grand_total'
            ) - $order->getOrigData(
                'base_subtotal_canceled'
            ) - $order->getOrigData(
                'base_total_paid'
            ) >= 0;
    }
}
