<?php
/**
 * Created by Magenest.
 * Author: Pham Quang Hau
 * Date: 26/05/2016
 * Time: 21:55
 */
namespace Magenest\RewardPoints\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use Magenest\RewardPoints\Model\RuleFactory;

abstract class Rule extends Action
{
    protected $_ruleFactory;

    protected $_pageFactory;

    protected $_coreRegistry;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        RuleFactory $ruleFactory,
        Registry $registry
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_ruleFactory = $ruleFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_RewardPoints::system_rewardpoints_earning_rule')
            ->addBreadcrumb(__('Earning Rules Manager'), __('Earning Rules Manager'));

        $resultPage->getConfig()->getTitle()->set(__('Earning Rules Manager'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_RewardPoints::system_rewardpoints_earning_rule');
    }
}
