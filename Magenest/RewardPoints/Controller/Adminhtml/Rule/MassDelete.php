<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 10:08
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magenest\RewardPoints\Model\RuleFactory;

class MassDelete extends Action
{
    protected $_filter;

    protected $_ruleFactory;
    
    public function __construct(
        Action\Context $context,
        Filter $filter,
        RuleFactory $ruleFactory
    ) {
        $this->_filter = $filter;
        $this->_ruleFactory = $ruleFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_ruleFactory->create()->getCollection());
        $ruleDeleted = 0;
        foreach ($collection->getItems() as $rule) {
            $rule->delete();
            $ruleDeleted++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $ruleDeleted)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('rewardpoints/*/index');
    }
}
