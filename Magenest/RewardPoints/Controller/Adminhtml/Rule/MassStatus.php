<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 10:11
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magenest\RewardPoints\Model\RuleFactory;
use Magento\Framework\Exception\LocalizedException;

class MassStatus extends Action
{
    protected $_filter;

    protected $_ruleFactory;


    public function __construct(
        Filter $filter,
        RuleFactory $ruleFactory,
        Action\Context $context
    ) {
        $this->_filter = $filter;
        $this->_ruleFactory = $ruleFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $status = (int) $this->getRequest()->getParam('status');
        $collection = $this->_filter->getCollection($this->_ruleFactory->create()->getCollection());
        $total = 0;

        try {
            foreach ($collection as $item) {
                $item->setData('status', $status)->save();
                $total++;
            }
            $this->messageManager->addSuccess(__('A total of %1 record(s) have been updated.', $total));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('rewardpoints/*/index');
    }
}
