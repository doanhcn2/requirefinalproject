<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 09:54
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;

class NewAction extends Action
{
    protected $_resultForwardFactory;

    public function __construct(
        Action\Context $context,
        ForwardFactory $forwardFactory
    ) {
        $this->_resultForwardFactory = $forwardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
