<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 09:55
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magenest\RewardPoints\Controller\Adminhtml\Rule;
use Magento\Backend\App\Action;
use Magenest\RewardPoints\Model\RuleFactory;

class Edit extends Rule
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magenest\RewardPoints\Model\Rule $model */
        $model = $this->_ruleFactory->create();

        if ($id) {
            $model->load($id);

            if(!$model->getId()) {
                $this->messageManager->addError(__('This rule doesn\'t exist'));

                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('rewardpoints_rule', $model);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? __('Edit Rule \'%1\'', $model->getData('title')) : __('New Reward Points Rule'));

        return $resultPage;
    }
}
