<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 24/07/2016
 * Time: 11:24
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;

class Delete extends Action
{
    public function __construct(
        Action\Context $context
    ) {
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $model = $this->_objectManager->create('Magenest\RewardPoints\Model\Rule');

            $model->load($id);
            if ($id != $model->getId()) {
                throw new LocalizedException(__('Something is wrong while deleting the rule. Please try again.'));
            }

            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());

            try {
                $model->delete();
                $this->messageManager->addSuccess(__('The rule has been successfully deleted.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        return $resultRedirect->setPath('*/*/index');
    }
}
