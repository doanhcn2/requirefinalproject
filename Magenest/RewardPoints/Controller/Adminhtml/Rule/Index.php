<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 12/07/2016
 * Time: 15:46
 */
namespace Magenest\RewardPoints\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;
use Magenest\RewardPoints\Controller\Adminhtml\Rule;

class Index extends Rule
{
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Earning Rules Manager'));

        return $resultPage;
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_RewardPoints::system_rewardpoints_earning_rule');
    }
}
