<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 15/08/2016
 * Time: 15:21
 */

namespace Magenest\RewardPoints\Helper;

use Magenest\RewardPoints\Model\RuleFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogRule\Model\RuleFactory as CoreRuleFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Helper\Session\CurrentCustomer;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CONFIG_PRODUCT_LIST_ENABLE = 'magento_reward/point_config/product_list_enable';

    const XML_PATH_CONFIG_PRODUCT_DETAIL_ENABLE = 'magento_reward/point_config/product_detail_enable';

    protected $_ruleFactory;

    protected $_coreRuleFactory;

    protected $customerFactory;

    protected $_productFactory;

    protected $_storeManager;

    protected $_customersFactory;

    protected $_configurable;

    protected $orderFactory;

    protected $quoteFactory;

    protected $_currentCustomer;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        CoreRuleFactory $coreRuleFactory,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customersFactory,
        CustomerFactory $customerFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        RuleFactory $ruleFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
    )
    {
        $this->_configurable = $configurable;
        $this->_ruleFactory = $ruleFactory;
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->_coreRuleFactory = $coreRuleFactory;
        $this->_customersFactory = $customersFactory;
        $this->customerFactory = $customerFactory;
        $this->_productFactory = $productFactory;
        $this->_storeManager = $storeManagerInterface;
        $this->_currentCustomer = $currentCustomer;
        parent::__construct($context);
    }

    public function getMatchingViewProductIds($ruleId, $websiteId)
    {
        if ($ruleId == 0) {
            return [];
        }
        /** @var \Magento\CatalogRule\Model\Rule $coreRule */
        $coreRule = $this->_coreRuleFactory->create();
        $ruleModel = $this->_ruleFactory->create()->load($ruleId);
        $conditions = $ruleModel->getData('conditions_serialized');
        $coreRule->setConditionsSerialized($conditions);
        $coreRule->setWebsiteIds($websiteId);
        $coreRule->setIsActive(1);
        $ids = [];
        $product_ids = $coreRule->getMatchingProductIds();
        foreach ($product_ids as $product_id => $validationByWebsite) {
            if (!empty($validationByWebsite[$websiteId])) {
                array_push($ids, $product_id);
            }
        }
        return $ids;
    }

    public function getProductRule($productId)
    {
        $ruleModel = $this->_ruleFactory->create();
        $rules = $ruleModel->getCollection();
        $point = 0;
        $result = [];
        foreach ($rules as $rule) {
            $ruleId = $rule->getId();
            $websiteId = $this->_storeManager->getWebsite()->getId();
            \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->unregister('current_product_reward_id');
            \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->register('current_product_reward_id', $productId);
            $ruleProductIds = $this->getMatchingViewProductIds($ruleId, $websiteId);
            if (in_array($productId, $ruleProductIds)) {
                $result[] = array('point' => $rule->getPoints(), 'steps' => $rule->getSteps());
            }
        }
        return $result;
    }
}
