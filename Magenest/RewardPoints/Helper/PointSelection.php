<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 15/08/2016
 * Time: 15:21
 */

namespace Magenest\RewardPoints\Helper;

use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogRule\Model\RuleFactory as CoreRuleFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magenest\RewardPoints\Model\RuleFactory;


class PointSelection extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CONFIG_PRODUCT_LIST_ENABLE = 'magento_reward/general/product_list_enable';

    const XML_PATH_CONFIG_PRODUCT_DETAIL_ENABLE = 'magento_reward/general/product_detail_enable';

    const XML_PATH_CONFIG_CATEGORY_PRIORITY = 'magento_reward/general/category';

    /**
     * @var \Magento\Framework\App\State\
     */
    protected $state;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    protected $_coreRuleFactory;

    protected $customerFactory;

    protected $_productFactory;

    protected $_storeManager;

    protected $_customersFactory;

    protected $_configurable;

    protected $orderFactory;

    protected $quoteFactory;

    protected $_customerSession;

    protected $_helper;

    protected $_ruleFactory;

    protected $categoryFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        CoreRuleFactory $coreRuleFactory,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customersFactory,
        CustomerFactory $customerFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Customer\Model\Session $_customerSession,
        \Magenest\RewardPoints\Helper\Data $data,
        RuleFactory $ruleFactory,
        \Magento\Framework\App\State $state,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->categoryFactory   = $categoryFactory;
        $this->_ruleFactory      = $ruleFactory;
        $this->_helper           = $data;
        $this->state             = $state;
        $this->storeManager      = $storeManager;
        $this->_configurable     = $configurable;
        $this->orderFactory      = $orderFactory;
        $this->quoteFactory      = $quoteFactory;
        $this->_coreRuleFactory  = $coreRuleFactory;
        $this->_customersFactory = $customersFactory;
        $this->customerFactory   = $customerFactory;
        $this->_productFactory   = $productFactory;
        $this->_storeManager     = $storeManagerInterface;
        $this->_customerSession  = $_customerSession;
        parent::__construct($context);
    }

    public function isShowProductDetailEnabled()
    {
        $enable = $this->scopeConfig->getValue(self::XML_PATH_CONFIG_PRODUCT_DETAIL_ENABLE);
        if ($enable == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getCategoryRule()
    {
        $select = $this->scopeConfig->getValue(self::XML_PATH_CONFIG_CATEGORY_PRIORITY);
        if ($select == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isShowProductListEnabled()
    {
        $enable = $this->scopeConfig->getValue(self::XML_PATH_CONFIG_PRODUCT_LIST_ENABLE);
        if ($enable == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public function resolveCurrentWebsiteId()
    {
        if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            // in admin area
            /** @var \Magento\Framework\App\RequestInterface $request */
            $request = $this->_request;
            $storeId = (int)$request->getParam('store', 0);
        } else {
            // frontend area
            $storeId = true; // get current store from the store resolver
        }
        $store     = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

        return $websiteId;
    }

    public function getCoreRates($website_id, $customerGroup)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /**
         * @var $rates \Magento\Reward\Model\ResourceModel\Reward\Rate\Collection
         */
        $rates           = $objectManager->create('Magento\Reward\Model\ResourceModel\Reward\Rate\CollectionFactory')->create();
        $finalRate       = [];
        $rateByDirection = $rates->addFieldToFilter('direction', '2')->getData();
        if (count($rateByDirection) <= 1) {
            return $rateByDirection;
        }
        $rates           = $objectManager->create('Magento\Reward\Model\ResourceModel\Reward\Rate\CollectionFactory')->create();
        $rateByWebsiteId = $rates->addFieldToFilter('direction', '2')->addFieldToFilter('website_id', $website_id)->getData();
        if (count($rateByWebsiteId) == 1) {
            $finalRate = $rateByWebsiteId;
        } else if ($customerGroup != null) {
            $rates                 = $objectManager->create('Magento\Reward\Model\ResourceModel\Reward\Rate\CollectionFactory')->create();
            $rateByCustomerGroupId = $rates->addFieldToFilter('direction', '2')->addFieldToFilter('website_id', $website_id)->addFieldToFilter('customer_group_id', $customerGroup)->getData();
            if (count($rateByCustomerGroupId) == 1) {
                $finalRate = $rateByCustomerGroupId;
            } else if (count($rateByCustomerGroupId) > 1) {
                $finalRate = $rateByCustomerGroupId[0];
            } else {
                $finalRate = $rateByWebsiteId[0];
            }
        } else {
            $finalRate = $rateByWebsiteId[0];
        }

        return $finalRate;
    }

    /**
     * @param \Magento\Catalog\Model\Product || \Magento\Quote\Model\Quote\Item || \Magento\Sales\Model\Order\Item  $product
     *
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function getCoreRewardPoints($product)
    {
        $website_id    = $this->resolveCurrentWebsiteId();
        $customerGroup = $this->_customerSession->getCustomer()->getGroupId();
        $coreRate      = [];
        $coreRate      = $this->getCoreRates($website_id, $customerGroup);
        if (count($coreRate) == 0) {
            return 0;
        }
        $pointsStep     = $coreRate[0]['points'];
        $currencyAmount = $coreRate[0]['currency_amount'];
        $productPrice   = $product->getPrice();
        $amountValue    = (int)((string)$productPrice / (string)$currencyAmount);
        $corePoints     = floor($amountValue * $pointsStep);

        return $corePoints;
    }

    public function getProductPoints($productId)
    {
        $productData = $this->_productFactory->create()->load($productId);

        return $productData->getMagenestRewardPoints();
    }

    public function getVendorRulePoints()
    {
        return 0;
    }

    /**
     * Return point for product. (only)
     *
     * @param $product
     *
     * @return float
     */
    public function getProductPointFinal($product)
    {
        if (!$this->_customerSession->isLoggedIn())
            return 0;

        $productPoint = $this->getProductPoints($product->getId());
        if ($productPoint !== null) {
            return $productPoint;
        }

        $productPointByCategory = $this->getProductPointByCategory($product);
        if ($productPointByCategory !== null) {
            return $productPointByCategory;
        }

        $coreFlatPoints = $this->getCoreRewardPoints($product);
        if ($coreFlatPoints != null && $coreFlatPoints > 0) {
            return $coreFlatPoints;
        }

        return 0;
    }

    /**
     * @param \Magento\Catalog\Model\Product || \Magento\Quote\Model\Quote\Item || \Magento\Sales\Model\Order\Item  $product
     *
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductPointByCategory($product)
    {
        $isItem = false;
        if ($product instanceof \Magento\Quote\Model\Quote\Item || $product instanceof \Magento\Sales\Model\Order\Item) {
            $isItem = true;
        }
        $ruleModel   = $this->_ruleFactory->create();
        $rules       = $ruleModel->getCollection();
        $childrenIds = [];
        if ($product->getTypeId() == 'configurable' && $product->getChildren()) {
            $children = $product->getChildren();
            foreach ($children as $child) {
                $childrenIds[] = $child->getProductId();
            }
        }
        $point = 0;
        /** @var  \Magenest\RewardPoints\Model\Rule $rule */
        $rule       = $this->getMinPointCategory($rules);
        $finalPrice = $product->getPrice();//final price in base currency
        if ($rule) {
            $ruleId    = $rule->getId();
            $websiteId = $this->_storeManager->getWebsite()->getId();
            \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->unregister('current_product_reward_id');
            if ($product instanceof Product)
                \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->register('current_product_reward_id', $product->getId());
            else
                \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->register('current_product_reward_id', $product->getProduct()->getId());
            $ruleProductIds = $this->getMatchingViewProductIds($ruleId, $websiteId);
            if ($isItem) {
                $productId = $product->getProduct()->getId();
            } else {
                $productId = $product->getId();
            }
            if (in_array($productId, $ruleProductIds) or array_intersect($childrenIds, $ruleProductIds)) {
                if ($rule->getActionType() == 1) {
                    $point += $rule->getPoints();
                } elseif ($rule->getActionType() == 2) {
                    $step  = $rule->getSteps();
                    $base  = $rule->getPoints();
                    $point += ($finalPrice / $step) * $base;
                }
            }

        }

        return $point === 0 ? null : $point;
    }

    /**
     * @param \Magenest\RewardPoints\Model\ResourceModel\Rule\Collection $rules
     */
    public function getMinPointCategory($rules)
    {
        if (count($rules) == 0)
            return false;
        $point     = null;
        $ruleFinal = null;
        /** @var  \Magenest\RewardPoints\Model\Rule $rule */
        if ($this->getCategoryRule()) {
            foreach ($rules as $rule) {
                if ($point == null) {
                    $point     = (int)$rule->getPoints();
                    $ruleFinal = $rule;
                    continue;
                }
                if ((int)$rule->getPoints() > $point) {
                    $point     = (int)$rule->getPoints();
                    $ruleFinal = $rule;
                }
            }
        } else {
            foreach ($rules as $rule) {
                if ($point == null) {
                    $point     = (int)$rule->getPoints();
                    $ruleFinal = $rule;
                    continue;
                }
                if ((int)$rule->getPoints() < $point) {
                    $point     = (int)$rule->getPoints();
                    $ruleFinal = $rule;
                }
            }
        }

        return $ruleFinal;
    }

    public function getMatchingViewProductIds($ruleId, $websiteId)
    {
        if ($ruleId == 0) {
            return [];
        }
        /** @var \Magento\CatalogRule\Model\Rule $coreRule */
        $coreRule   = $this->_coreRuleFactory->create();
        $ruleModel  = $this->_ruleFactory->create()->load($ruleId);
        $conditions = $ruleModel->getData('conditions_serialized');
        $coreRule->setConditionsSerialized($conditions);
        $coreRule->setWebsiteIds($websiteId);
        $coreRule->setIsActive(1);
        $ids         = [];
        $product_ids = $coreRule->getMatchingProductIds();
        foreach ($product_ids as $product_id => $validationByWebsite) {
            if (!empty($validationByWebsite[$websiteId])) {
                array_push($ids, $product_id);
            }
        }

        return $ids;
    }
}
