<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 12/07/2016
 * Time: 14:34
 */
namespace Magenest\RewardPoints\Model\ResourceModel;

class Rule extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_rewardpoints_rule', 'id');
    }
}
