<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 18/07/2016
 * Time: 10:30
 */
namespace Magenest\RewardPoints\Model\ResourceModel\Rule;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    { 
        $this->_init('Magenest\RewardPoints\Model\Rule', 'Magenest\RewardPoints\Model\ResourceModel\Rule');
    }
}
