<?php
namespace Magenest\RewardPoints\Model\Action;

class OrderExtra extends \Magento\Reward\Model\Action\OrderExtra
{
    public function getPoints($websiteId)
    {
        if (!$this->_rewardData->isOrderAllowed($this->getReward()->getWebsiteId())) {
            return 0;
        }
        if ($this->_quote)
            return $this->getReward()->getRateToPoints()->calculateToPointsCustom($this->_quote,null);
        return $this->getReward()->getRateToPoints()->calculateToPointsCustom(null,$this->getEntity());
    }
}
