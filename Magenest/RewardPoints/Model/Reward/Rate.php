<?php
namespace Magenest\RewardPoints\Model\Reward;


class Rate extends \Magento\Reward\Model\Reward\Rate
{
    public function calculateToPointsCustom($quote, $entity)
    {
        $points = 0;
        /** @var \Magenest\RewardPoints\Helper\PointSelection $helper */
        $helper = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\RewardPoints\Helper\PointSelection');
        if ($quote){
            /** @var \Magento\Customer\Model\Session $customerSession */
            $customerSession = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Customer\Model\Session');
            if (!$customerSession->isLoggedIn())
                return 0;
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($quote->getItems() as $item){
                $productPoint = $helper->getProductPoints($item->getProduct()->getId());
                if ($productPoint !== null){
                    $points += $productPoint * $item->getQty();
                    continue;
                }

                $productPointByCategory = $helper->getProductPointByCategory($item);
                if ($productPointByCategory !== null) {
                    return $productPointByCategory * $item->getQty();
                }


                $coreFlatPoints = $helper->getCoreRewardPoints($item);
                if ($coreFlatPoints != null && $coreFlatPoints > 0){
                    $points += $coreFlatPoints * $item->getQty();
                    continue;
                }
            }
        }else{
            foreach ($entity->getItems() as $item){
                if ($item->getProduct() !== null) {
                    $productPoint = $helper->getProductPoints($item->getProduct()->getId());
                }
                if (@$productPoint !== null){
                    $points += $productPoint * $item->getQtyInvoiced();
                    continue;
                }

                $productPointByCategory = $helper->getProductPointByCategory($item);
                if ($productPointByCategory !== null) {
                    return $productPointByCategory * $item->getQtyInvoiced();
                }

                $coreFlatPoints = $helper->getCoreRewardPoints($item);
                if ($coreFlatPoints != null && $coreFlatPoints > 0){
                    $points += $coreFlatPoints * $item->getQtyInvoiced();
                    continue;
                }
            }
        }
        return $points;
    }
}
