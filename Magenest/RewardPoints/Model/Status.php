<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 12/07/2016
 * Time: 16:08
 */
namespace Magenest\RewardPoints\Model;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Status extends AbstractSource
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public static function getOptionArray()
    {
        return [
            self::STATUS_ACTIVE => __('Enable'),
            self::STATUS_INACTIVE => __('Disable')
        ];
    }

    public function getAllOptions()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }

    public function getOptionGrid($optionId)
    {
        $options = self::getOptionArray();
        if ($optionId == self::STATUS_ACTIVE) {
            $html = '<span class="grid-severity-notice"><span>' . $options[$optionId] . '</span>'.'</span>'; 
        }
        else {
            $html = '<span class="grid-severity-critical"><span>' . $options[$optionId] . '</span></span>'; 
        }

        return $html;
    }
}
