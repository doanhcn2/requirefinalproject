<?php

namespace Magenest\RewardPoints\Model;

class CatalogRule extends \Magento\CatalogRule\Model\Rule
{
    protected $ids = [];

    public function getMatchingProductIds()
    {
        $productId = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\Registry')->registry('current_product_reward_id');

        if (!empty(intval($productId))) {
            if ($this->getWebsiteIds()) {
                /** @var $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
                $productCollection = $this->_productCollectionFactory->create();
                $productCollection->addWebsiteFilter($this->getWebsiteIds());
                if ($this->_productsFilter) {
                    $productCollection->addIdFilter($this->_productsFilter);
                }
                $productCollection->addFieldToFilter('entity_id', $productId);
                $this->getConditions()->collectValidatedAttributes($productCollection);

                $this->_resourceIterator->walk(
                    $productCollection->getSelect(),
                    [[$this, 'magenestCallbackValidateProduct']],
                    [
                        'attributes' => $this->getCollectedAttributes(),
                        'product'    => $this->_productFactory->create()
                    ]
                );
            }

            return $this->ids;
        } else
            return parent::getMatchingProductIds();
    }

    public function magenestCallbackValidateProduct($args)
    {
        $product = clone $args['product'];
        $product->setData($args['row']);

        $websites = $this->_getWebsitesMap();
        $results  = [];

        foreach ($websites as $websiteId => $defaultStoreId) {
            $product->setStoreId($defaultStoreId);
            $results[$websiteId] = $this->getConditions()->validate($product);
        }
        $this->ids[$product->getId()] = $results;
    }

    private function getRuleConditionConverter()
    {
        if (null === $this->ruleConditionConverter) {
            $this->ruleConditionConverter = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\CatalogRule\Model\Data\Condition\Converter::class);
        }

        return $this->ruleConditionConverter;
    }
}
