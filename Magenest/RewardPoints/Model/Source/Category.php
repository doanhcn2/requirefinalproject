<?php
/**
 * Created by Magenest.
 * Author: Pham Quang Hau
 * Date: 08/05/2016
 * Time: 15:13
 */

namespace Magenest\RewardPoints\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class Category implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'value' => '0',
                'label' => __('Select rule with MIN point value.'),
            ],
            [
                'value' => '1',
                'label' => __('Select rule with MAX point value.')
            ]
        ];
    }
}
