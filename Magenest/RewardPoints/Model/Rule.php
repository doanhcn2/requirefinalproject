<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 12/07/2016
 * Time: 14:32
 */
namespace Magenest\RewardPoints\Model;

use Magento\Catalog\Model\Product;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Rule\Model\AbstractModel;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\CatalogRule\Model\Rule\Condition\CombineFactory;
use Magento\CatalogRule\Model\Rule\Action\CollectionFactory;
use Magento\CatalogRule\Model\Indexer\Rule\RuleProductProcessor;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

class Rule extends AbstractModel
{
    protected $_action_types;

    protected $_eventPrefix = 'magenest_rewardpoints_rule_';

    protected $_eventObject = 'rule';

    protected $_combineFactory;

    protected $_actionCollectionFactory;

    const RULE_ACTION_TYPE_ADD = 1;

    const RULE_ACTION_TYPE_ADDBYSTEP = 2;

    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        TimezoneInterface $localeDate,
        CombineFactory $combineFactory,
        CollectionFactory $actionCollectionFactory,
        RuleProductProcessor $ruleProductProcessor,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $relatedCacheTypes = [],
        array $data = []
    ) {
        $this->_combineFactory = $combineFactory;
        $this->_actionCollectionFactory = $actionCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $localeDate, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        $this->_init('Magenest\RewardPoints\Model\ResourceModel\Rule');
        $this->setIdFieldName('id');
        $this->_action_types = array(
            self::RULE_ACTION_TYPE_ADD => __("Give X points to customer"),
            self::RULE_ACTION_TYPE_ADDBYSTEP => __("For every Y spent, give X points"),
        );
    }

    public function ruleActionTypesToArray() 
    {
        return $this->_toArray($this->_action_types);
    }

    protected function _toArray($array) 
    {
        $res = array();
        foreach ($array as $value => $label) {
            $res[$value] = $label;
        }
        return $res;
    }
    public function getConditionsInstance()
    {
        return $this->_combineFactory->create();
    }

    public function getActionsInstance()
    {
        return $this->_actionCollectionFactory->create();
    }
}
