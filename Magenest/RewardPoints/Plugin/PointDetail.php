<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 17/09/2016
 * Time: 14:34
 */

namespace Magenest\RewardPoints\Plugin;

use Magenest\RewardPoints\Helper\PointSelection;
use Magento\Customer\Helper\Session\CurrentCustomer;

class PointDetail
{

    protected $_currentCustomer;

    protected $_helper;

    public function __construct(
        CurrentCustomer $currentCustomer,
        PointSelection $pointSelection
    )
    {
        $this->_helper = $pointSelection;
        $this->_currentCustomer = $currentCustomer;

    }

    public function aroundGetProductPrice(
        \Magento\Catalog\Block\Product\AbstractProduct $subject,
        callable $proceed,
        \Magento\Catalog\Model\Product $product
    )
    {
        $result = $proceed($product);
        $point = $this->_helper->getProductPointFinal($product);
        $point = floor($point);
        $isEnabled = $this->_helper->isShowProductListEnabled();
        if ($point > 0 && $isEnabled) {
            $result = $result . '<div class="price-box price-final_price"><strong><em style="color: #ff6711; font-size: 12px"><+' . $point . ' P></em></strong></div>';
        }
        return $result;
    }
}
