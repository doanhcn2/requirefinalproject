<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 10:49
 */
namespace Magenest\RewardPoints\Block\Adminhtml\Rule;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container as FormContainer;
use Magento\Framework\Registry;

class Edit extends FormContainer
{
    protected $_coreRegistry;

    public function __construct(
        Registry $registry,
        Context $context,
        array $data
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Page Constructor
     */
    protected function _construct()
    {
        $templates = $this->_coreRegistry->registry('rewardpoints_rule');

        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_RewardPoints';
        $this->_controller = 'adminhtml_rule';
        parent::_construct();
        if (!$templates->getId()) {
            $this->buttonList->remove('save', 'label', __('Save Rule'));
        } else { $this->buttonList->update('save', 'label', __('Save Rule')); 
        }
        $this->buttonList->add(
            'save-and-continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ]
                    ]
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Rule'));
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        $templates = $this->_coreRegistry->registry('rewardpoints_rule');
        if ($templates->getId()) {
            return __("Edit Rule '%1'", $this->escapeHtml($templates->getTitle()));
        }
        return __('New Rule');
    }

    /**
     * @return string
     */
    public function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            'rewardpoints/*/save',
            ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']
        );
    }
}
