<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 11:56
 */
namespace Magenest\RewardPoints\Block\Adminhtml\Rule\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('rewardpoints_rule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Magenest Reward Points Configuration'));
    }
}
