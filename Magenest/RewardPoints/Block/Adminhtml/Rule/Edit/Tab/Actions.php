<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 19/07/2016
 * Time: 11:21
 */
namespace Magenest\RewardPoints\Block\Adminhtml\Rule\Edit\Tab;

use Magento\Backend\Block\Widget\Form;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Actions extends Generic implements TabInterface
{
    protected $_status;
    
    protected $type;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magenest\RewardPoints\Model\Status $status,
        array $data
    ) {
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('rewardpoints_rule');
        $this->type = $model->getRuleType();
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('rule_');
        $fieldset = $form->addFieldset(
            'action_fieldset',
            ['legend' => __('Set appropriate point action')]
        );

        $fieldset->addField(
            'action_type',
            'select',
            [
                'label' => __('Apply'),
                'name' => 'action_type',
                'options' => $model->ruleActionTypesToArray()
            ]
        );

        $fieldset->addField(
            'points',
            'text',
            ['name' => 'points', 'required' => true, 'label' => __('Number of points (X)')]
        );
        $fieldset->addField(
            'steps',
            'text',
            ['name' => 'steps', 'required' => true, 'label' => __('Step (Y)')]
        );
        $htmlIdPrefix = $form->getHtmlIdPrefix();
        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                'Magento\Backend\Block\Widget\Form\Element\Dependence'
            )->addFieldMap(
                "{$htmlIdPrefix}action_type",
                'action_type'
            )->addFieldMap(
                "{$htmlIdPrefix}points",
                'points'
            )->addFieldDependence(
                'points',
                'action_type',
                '1'
            )
        );
        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                'Magento\Backend\Block\Widget\Form\Element\Dependence'
            )->addFieldMap(
                "{$htmlIdPrefix}action_type",
                'action_type'
            )->addFieldMap(
                "{$htmlIdPrefix}steps",
                'steps'
            )->addFieldDependence(
                'steps',
                'action_type',
                '2'
            )
        );
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return __('Actions');
    }

    public function getTabTitle()
    {
        return __('Actions');
    }

    public function canShowTab()
    {
        $model = $this->_coreRegistry->registry('rewardpoints_rule');
        if ($model->getId()) {
            return true;
        } else { return false; 
        }
    }

    public function isHidden()
    {
        $model = $this->_coreRegistry->registry('rewardpoints_rule');
        if ($model->getId()) {
            return false;
        } else { return true; 
        }
    }
}
