<?php
namespace Magenest\RewardPoints\Block;

use Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Catalog\Block\Product\Context;

class PointInfo extends \Magento\Catalog\Block\Product\AbstractProduct
{
    public $_helper;

    protected $_selectionHelper;

    protected $_currentCustomer;

    public function __construct(Context $context,
                                CurrentCustomer $currentCustomer,
                                \Magenest\RewardPoints\Helper\Data $helper,
                                \Magenest\RewardPoints\Helper\PointSelection $pointSelection,
                                array $data = [])
    {
        $this->_helper = $helper;
        $this->_currentCustomer = $currentCustomer;
        $this->_selectionHelper = $pointSelection;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct(){
        return $this->_coreRegistry->registry('current_product');
    }

    public function getIsShowProductDetailEnabled()
    {
        return $this->_selectionHelper->isShowProductDetailEnabled();
    }

    public function getProductPoint($product)
    {
        $productPoint = $this->_selectionHelper->getProductPointFinal($product);
        return floor($productPoint);
    }

    public function getProductRule($productId)
    {
        return $this->_helper->getProductRule($productId);
    }
}
