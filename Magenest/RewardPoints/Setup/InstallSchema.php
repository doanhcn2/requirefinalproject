<?php
/**
 * Created by PhpStorm.
 * User: katsu
 * Date: 12/07/2016
 * Time: 11:31
 */
namespace Magenest\RewardPoints\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table as Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_rewardpoints_rule'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false],
                'Rule Title'
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                10,
                [],
                'Rule Status'
            )
            ->addColumn(
                'customer_group_ids',
                Table::TYPE_TEXT,
                255,
                [],
                'Customer Group Ids'
            )
            ->addColumn(
                'web_ids',
                Table::TYPE_TEXT,
                255,
                [],
                'Website Ids'
            )
            ->addColumn(
                'action_type',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Action Type'
            )
            ->addColumn(
                'conditions_serialized',
                Table::TYPE_TEXT,
                null,
                [],
                'Conditions'
            )->addColumn(
                'condition',
                Table::TYPE_TEXT,
                null,
                [],
                'Condition'
            )
            ->addColumn(
                'points',
                Table::TYPE_FLOAT,
                null,
                ['nullable' => true],
                'Points'
            )
            ->addColumn(
                'steps',
                Table::TYPE_FLOAT,
                null,
                ['nullable' => true],
                'Steps'
            )
            ->setComment('Reward Points Rule table');

        $installer->getConnection()->createTable($table);

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
