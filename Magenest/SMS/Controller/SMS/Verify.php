<?php

namespace Magenest\SMS\Controller\SMS;

use Magenest\SMS\Helper\Data;

/**
 * Class Verify
 * @package Magenest\SMS\Controller\SMS
 */
class Verify extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    protected $jsonFactory;

    protected $dataHelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\SMS\Helper\Data $dataHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->dataHelper = $dataHelper;
        $this->session = $customerSession;
        $this->jsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                $number = $this->getRequest()->getParam('number');
                if (!$number) {
                    return $this->jsonFactory->create()->setData([
                        'success' => false,
                        'error' => true,
                        'errorMessage' => __("Please fill in phone number.")
                    ]);
                }

                $authId = $this->dataHelper->getAuthId();
                $fromNumber = $this->dataHelper->getNumber();
                $message = $this->dataHelper->getMessage();
                $otp = rand(123456, 987654);
                $data = [
                    'src' => $fromNumber,
                    'dst' => $number,
                    'text' => (string)__($message, $otp)
                ];
                $response = $this->dataHelper->sendCurl("Account/" . $authId . "/Message/", $data);
                if (!$response) {
                    return $this->jsonFactory->create()->setData([
                        'success' => false,
                        'error' => true,
                        'errorMessage' => __("Something went wrong. Please try again later.")
                    ]);
                } elseif (isset($response['error'])) {
                    return $this->jsonFactory->create()->setData([
                        'success' => false,
                        'error' => true,
                        'errorMessage' => __($response['error'])
                    ]);
                } else {
                    return $this->jsonFactory->create()->setData([
                        'success' => true,
                        'error' => false,
                        'otp' => $otp
                    ]);
                }
            } catch (\Exception $e) {
                return $this->jsonFactory->create()->setData([
                    'success' => false,
                    'error' => true,
                    'errorMessage' => $e->getMessage()
                ]);
            }
        }
    }
}