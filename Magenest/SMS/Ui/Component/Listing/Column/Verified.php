<?php

namespace Magenest\SMS\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Customer\Model\CustomerFactory;

class Verified extends Column
{
    private $customerFactory;

    /**
     * Constructor
     *
     * @param CustomerFactory $customerFactory
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        CustomerFactory $customerFactory,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customerFactory = $customerFactory;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                /** @var \Magento\Customer\Model\Customer $customerModel */
                $customerModel = $this->customerFactory->create();
                $customer = $customerModel->load($item['entity_id']);
                $item['sms_verified'] = empty($customer->getData('sms_verified')) ? '0' : $customer->getData('sms_verified') ;
            }
        }
        return $dataSource;
    }
}