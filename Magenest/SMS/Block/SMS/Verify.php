<?php

namespace Magenest\SMS\Block\SMS;

use Magento\Framework\View\Element\Template;

/**
 * Class Verify
 * @package Magenest\SMS\Block\SMS
 */
class Verify extends \Magento\Framework\View\Element\Template
{
    protected $dataHelper;

    public $customerSession;
    public function __construct(
        \Magenest\SMS\Helper\Data $dataHelper,
        \Magento\Customer\Model\Session $session,
        Template\Context $context, array $data = [])
    {
        $this->customerSession = $session;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    public function getDataHelper()
    {
        return $this->dataHelper;
    }
}