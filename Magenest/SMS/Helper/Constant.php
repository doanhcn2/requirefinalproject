<?php
namespace Magenest\SMS\Helper;

class Constant{
    const SMS_DATA = 'SMS_data';
    const VERIFIED_NUMBER = 'verified_number';
    const TIME = 'time';
}