<?php

namespace Magenest\SMS\Helper;

use Magento\Framework\Encryption\EncryptorInterface;

class Data
{
    const XML_PATH_AUTH_ID = 'sms/setting/auth_id';
    const XML_PATH_AUTH_TOKEN = 'sms/setting/auth_token';
    const XML_PATH_NUMBER = 'sms/setting/number';
    const XML_PATH_EXPIRY_TIME = 'sms/setting/expiry_time';
    const XML_PATH_ENABLE = 'sms/setting/active';
    const XML_PATH_MESSAGE = 'sms/setting/message';
    const XML_PATH_VERIFY_PAGE_INSTRUCTION = 'sms/setting/verify_page_instruction';
    const XML_PATH_OTP_PAGE_INSTRUCTION = 'sms/setting/otp_page_instruction';
    const URL_ENDPOINT = 'https://api.plivo.com/v1/';
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    protected $_encryptor;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptorInterface
    )
    {
        $this->_encryptor = $encryptorInterface;
        $this->_scopeConfig = $scopeConfig;
    }

    public function getEnable()
    {
        return (boolean)$this->_scopeConfig->getValue(
            self::XML_PATH_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getAuthId()
    {
        return $this->_encryptor->decrypt($this->_scopeConfig->getValue(
            self::XML_PATH_AUTH_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }

    public function getAuthToken()
    {
        return $this->_encryptor->decrypt($this->_scopeConfig->getValue(
            self::XML_PATH_AUTH_TOKEN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }

    public function getNumber()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_NUMBER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getExpiryTime()
    {
        return (int)$this->_scopeConfig->getValue(
            self::XML_PATH_EXPIRY_TIME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getVerifyPageIns()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_VERIFY_PAGE_INSTRUCTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getMessage()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_MESSAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getOtpPageIns()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_OTP_PAGE_INSTRUCTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function sendCurl($url, $field, $type = 'POST')
    {
        try {
            $url = self::URL_ENDPOINT.$url;
            $field = json_encode($field);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            /** @var \Magenest\SMS\Helper\Curl $curl */
            $curl = $objectManager->create('Magenest\SMS\Helper\Curl');
            $a = $this->getAuthToken();
            $a=$this->getAuthId();
            $curl->setHeaders(['Content-Type' => 'application/json']);
            $curl->setCredentials($this->getAuthId(), $this->getAuthToken());
            switch (strtoupper($type)) {
                case "GET":
                    $curl->get($url);
                    break;
                case "POST":
                    $curl->post($url, $field);
                    break;
                case "DELETE":
                    $curl->delete($url);
                    break;
                case "PATCH":
                    $curl->patch($url, $field);
                    break;
                default:
                    return false;
            }
            $result = $curl->getBody();

            return json_decode($result, true);
        } catch (\Exception $e) {
            return false;
        }
    }
}