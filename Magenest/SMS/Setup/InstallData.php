<?php

namespace Magenest\SMS\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    private $eavConfig;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'sms_verified',
            [
                'type' => 'int',
                'label' => 'Is Verified',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'default' => '0',
                'sort_order' => 5,
                'system' => false,
                'frontend_class' => 'hidden-for-admin',
                'position' => 100
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'sms_number',
            [
                'type' => 'varchar',
                'label' => 'Phone Number',
                'input' => 'text',
                'source' => '',
                'required' => false,
                'default' => '',
                'sort_order' => 4,
                'system' => false,
                'frontend_class' => 'hidden-for-admin',
                'position' => 99
            ]
        );
        $smsPhone = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'sms_number');
        $smsVerified = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'sms_verified');
        $used_in_forms[] = "adminhtml_customer";
        $used_in_forms[] = "checkout_register";
        $used_in_forms[] = "customer_account_create";
        $used_in_forms[] = "customer_account_edit";
        $used_in_forms[] = "adminhtml_checkout";

        $smsPhone->setData('used_in_forms', $used_in_forms);
        $smsVerified->setData('used_in_forms', $used_in_forms);
        $smsPhone->save();
        $smsVerified->save();
    }
}