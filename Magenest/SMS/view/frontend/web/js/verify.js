/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
require([
    'jquery',
    "Magento_Ui/js/modal/modal"
], function ($, modal) {
    'use strict';

    function validatePhone(phoneNumber) {
        var phoneNumberPattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
        return phoneNumberPattern.test(phoneNumber);
    }

    function resetErrorField() {
        errorArea.hide();
        errorArea.html("");
    }

    function setErrorField(mess) {
        errorArea.show();
        errorArea.html(mess);
    }

    var phoneInput = $('input#sms-number');
    var submitButton = $('button#sms-number-submit');
    var errorArea = $('div.field.error>p');
    var otpInput = $('input#otp-input');
    var OTP = null;
    var timeLeft = 30;
    var elem = document.getElementById('resend');

    var options = {
        autoOpen: true,
        type: 'popup',
        responsive: true,
        modalClass: 'otp-modal',
        title: "OTP code", //write your popup title
        buttons: []
    };
    var popupdata = $('div#otp-div');

    submitButton.on('click', function (event) {
        resetErrorField();
        event.preventDefault();
        if (!validatePhone(phoneInput.val())) {
            setErrorField("Not a valid phone number. Please try again");
        }
        else {
            OTP = null;
            $.ajax({
                url: window.smsVerifyUrl,
                dataType: 'json',
                method: 'POST',
                showLoader: true,
                data: {number: phoneInput.val()},
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            OTP = response.otp;
                            modal(options, popupdata);
                        } else if (response.error) {
                            setErrorField(response.errorMessage);
                        }
                    }
                    else {
                        setErrorField("Something went wrong. Please try again later.")
                    }
                },
                error: function () {
                    setErrorField("Something went wrong. Please try again later.")
                }
            });
        }
    });

    phoneInput.on('keypress', function (event) {
        resetErrorField();
    });

    $('a#use-other').on('click', function (event) {
        event.preventDefault();
        $('button#sms-number-submit').prop('style', '');
        phoneInput.css('opacity', '1');
        phoneInput.prop('readonly', false);
        $('p.use-another').hide();
    });

    $('div#otp-div button#submit').on('click', function (event) {
        event.preventDefault();
        if (otpInput.val() && otpInput.val() == OTP) {
            otpMatch();
            popupdata.modal('closeModal');
        } else {
            $('div#otp-div div.error p').show().html("The OTP doesnot match! Please try again.");
        }
    });

    $('div#otp-div button#resend').on('click', function (event) {
        event.preventDefault();
        OTP = null;
        $.ajax({
            url: window.smsVerifyUrl,
            dataType: 'json',
            method: 'POST',
            showLoader: true,
            data: {number: phoneInput.val()},
            success: function (response) {
                if (response) {
                    if (response.success) {
                        OTP = response.otp;
                        elem.setAttribute('disabled',true);
                        var timerId = setInterval(function () {
                            if (timeLeft == 0) {
                                clearTimeout(timerId);
                                timeLeft = 30;
                                elem.innerHTML = 'Resend';
                                elem.removeAttribute('disabled');

                            } else {
                                elem.innerHTML = 'Resend (' +timeLeft + ')';
                                timeLeft--;
                            }
                        }, 1000);
                        alert('sent');
                    } else if (response.error) {
                        setErrorField(response.errorMessage);
                    }
                }
                else {
                    setErrorField("Something went wrong. Please try again later.")
                }

            },
            error: function () {
                setErrorField("Something went wrong. Please try again later.")
            }
        });
    });

    function otpMatch() {
        if (phoneInput.val()) {
            window.localStorage.smsNumber = phoneInput.val();
        } else if (window.localStorage.smsNumber && window.localStorage.isSmsVerified) {
            phoneInput.val(window.localStorage.smsNumber);
        } else {
            return;
        }
        $('input#sms-verify').val('1');
        window.localStorage.isSmsVerified = true;
        submitButton.hide();
        phoneInput.css('opacity', '0.5');
        phoneInput.prop('readonly', true);
        $('p.use-another').show();
    }
});
