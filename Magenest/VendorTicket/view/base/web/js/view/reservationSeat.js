define([
    'jquery',
    'ko',
    'uiComponent',
    'https://cdn.rawgit.com/konvajs/konva/1.7.6/konva.js',
    'Magenest_VendorTicket/js/model/Chair',
    'Magenest_VendorTicket/js/model/Table',
    'Magenest_VendorTicket/js/model/Ticket',
    "jquery/colorpicker/js/colorpicker"
], function ($, ko, Component, konva, chairt, tablet, ticket) {
    'use strict';

    function remove(array, element) {
        const index = array.indexOf(element);

        if (index !== -1) {
            array.splice(index, 1);
        }
    }

    function toLetters(num) {
        "use strict";
        var mod = num % 26,
            pow = num / 26 | 0,
            out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
        return pow ? toLetters(pow) + out : out;
    }

    function fromLetters(str) {
        "use strict";
        var out = 0, len = str.length, pos = len;
        while (--pos > -1) {
            out += (str.charCodeAt(pos) - 64) * Math.pow(26, len - 1 - pos);
        }
        return out;
    }

    return Component.extend({
        defaults: {
            dataScope: 'reservationSeat',
            template: 'Magenest_VendorTicket/seat',
            mode: ['map', 'ticket', 'hold'],
            activeMode: 'ticket',
            layoutType: 'roundTable',
            blocks: [],
            seats: [],
            selectedSeats: [],
            tables: [],
            tableGroupArr: [],
            tickets: [],
            activeTicket: {},
            holds: [],
            layer: '',
            tableNumber: '',
            chairNumber: '',
            stageWidth: '',
            stageHeight: '',
            stage: '',
            defaultColor: 'silver',
            currentTicket: '',
            isSessionMap: '',
            isEditData: ko.observable(false),
            /**
             *  new Konva.Group
             */
            activeObject: '',
            allObj: [],
            isSessionMapVenue: ko.observable(false),
            venueMapData: ko.observable(),
            venueTicketData: ko.observable(),
            lastAlphaKey: 'A',
            labels: []
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super().observe('activeMode');
            this._super().observe('activeTicket');
            this._super().observe('holds');
            this._super().observe('tableNumber');
            this._super().observe('chairNumber');
            this._super().observe('seats');
            this._super().observe('tables');
            this._super().observe('labels');
            this._super().observe('tickets');
            this._super().observe('activeObject');
            this._super().observe('stage');
            this._super().observe('stageWidth');
            this._super().observe('stageHeight');
            this._super().observe('isEditData');
            this._super().observe('venueMapData');
            this._super().observe('venueTicketData');
            return this;
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();
            var self = this;
            if (self.isSessionMap === "true") {
                self.isSessionMapVenue(true);
            } else {
                self.isSessionMapVenue(false);
            }
            self.stageWidth = 1200;
            self.stageHeight = 400;
            var stage;
            this.roundTable;
            self.lastAlphaKey = fromLetters(self.lastAlphaKey);
            if (self.lastAlphaKey > 1) {
                self.lastAlphaKey++;
            }
            if (self.stage()) {
                stage = Konva.Node.create(self.stage(), 'canvas');
                self.layer = stage.getChildren()[0];
                $.each(self.layer.getChildren(), function (index, item) {
                    self.mapModeSelectObj(item);
                    $.each(item.getChildren(), function (index, _item) {
                        if (_item.getAttr('objectType') === 'chair')
                            self.seats.push(_item);
                        if (_item.getAttr('objectType') === 'table')
                            self.tables.push(_item);
                        if (_item.className === 'Text')
                            self.labels.push(_item);
                    });
                });
            } else {
                stage = new Konva.Stage({
                    container: 'canvas',
                    width: self.stageWidth,
                    height: self.stageHeight
                });
                self.layer = new Konva.Layer();
                self.initTableAndChair(self.tableNumber(), self.chairNumber());
                stage.add(self.layer);
            }
            if (self.tickets()) {
                $.each(self.tickets(), function (index, ticket) {
                    var label = ticket.label;
                    ticket.label = ko.observable(label);
                    var cnt = ticket.cnt;
                    ticket.cnt = ko.observable(cnt);
                    var colorTag = ticket.colorTag;
                    ticket.colorTag = ko.observable(colorTag);
                    if (ticket.chairs.length === 0) {
                        ticket.chairs = [];
                    } else {
                        var chairsData = [];
                        $.each(ticket.chairs, function (index, chairs) {
                            var data = JSON.parse(chairs);
                            chairsData.push(data);
                        });
                        ticket.chairs = chairsData;
                    }
                });
            } else {
                self.tickets([]);
            }
            self.stage(stage);
            $.each(self.layer.getChildren(), function (index, item) {
                item.on('mouseenter', function () {
                    stage.container().style.cursor = 'move';
                });

                item.on('mouseleave', function () {
                    stage.container().style.cursor = 'default';
                });
            });
            if (self.tickets()) {
                self.bindTicketWithChairGlobal(self.tickets());
            }
            self.layer.draw();
        },

        /**
         * init tables and chair on the stage base on the user setting
         */
        initTableAndChair: function (tableAmt, chairAmt) {
            var self = this;

            var calResult = self.calculateNodes(chairAmt, 70);

            var deltaX = calResult[0] + 100;
            var deltaY = calResult[1] + 150;
            var currentlength = self.layer.getChildren().length;
            for (var i = currentlength; i < parseInt(tableAmt) + currentlength; i++) {
                var result;

                if (i === 0) {
                    result = self.createNodes(chairAmt, 70, 0, 0);
                    self.manipulateTheNode(result[0], result[1]);

                } else if (i < 4 && i > 0) {
                    result = self.createNodes(chairAmt, 70, deltaX * i, 0);
                    self.manipulateTheNode(result[0], result[1]);
                } else if (i >= 4) {
                    var mod = i % 4;
                    result = self.createNodes(chairAmt, 70, deltaX * mod, deltaY);
                    self.manipulateTheNode(result[0], result[1]);
                }
            }
        },
        createNodes: function (numNodes, radius, delx, dely) {
            var nodes = [],
                width = (radius * 2) + 50,
                height = (radius * 2) + 50,
                angle,
                x,
                y,
                i;
            for (i = 0; i < numNodes; i++) {
                angle = (i / (numNodes / 2)) * Math.PI; // Calculate the angle at which the element will be placed.
                                                        // For a semicircle, we would use (i / numNodes) * Math.PI.
                x = (radius * Math.cos(angle)) + (width / 2) + delx; // Calculate the x position of the element.
                y = (radius * Math.sin(angle)) + (height / 2) + dely; // Calculate the y position of the element.
                nodes.push({'id': i, 'x': x, 'y': y});
            }

            //find min x and min y
            var min_x = 100000000000000;
            var min_y = 100000000000000;
            var max_x = 0;
            var max_y = 0;

            $.each(nodes, function (index, node) {

                if (node.x > max_x) {
                    max_x = node.x;
                }

                if (node.y > max_y) {
                    max_y = node.y;
                }

                if (node.x < min_x) {
                    min_x = node.x;
                }

                if (node.y < min_y) {
                    min_y = node.y;
                }
            });

            var roundTable = tablet;

            if (numNodes === "3") {
                roundTable.x = (max_x - min_x) / 2 + min_x - 15.6;
                roundTable.y = (max_y - min_y) / 2 + min_y;
                roundTable.radius = 30;
            } else {
                roundTable.x = (max_x - min_x) / 2 + min_x;
                roundTable.y = (max_y - min_y) / 2 + min_y;
                roundTable.radius = 30;
            }

            return [nodes, roundTable];
        },

        /**
         * calculate the initial params to draw table
         *
         * @param numNodes
         * @param radius
         * @returns {*[]}
         */
        calculateNodes: function (numNodes, radius) {
            var nodes = [],
                width = (radius * 2) + 50,
                height = (radius * 2) + 50,
                angle,
                x,
                y,
                i;
            for (i = 0; i < numNodes; i++) {
                angle = (i / (numNodes / 2)) * Math.PI; // Calculate the angle at which the element will be placed.
                                                        // For a semicircle, we would use (i / numNodes) * Math.PI.
                x = (radius * Math.cos(angle)) + (width / 2); // Calculate the x position of the element.
                y = (radius * Math.sin(angle)) + (width / 2); // Calculate the y position of the element.
                nodes.push({'id': i, 'x': x, 'y': y});
            }

            //find min x and min y
            var min_x = 100000000000000;
            var min_y = 100000000000000;
            var max_x = 0;
            var max_y = 0;

            $.each(nodes, function (index, node) {
                if (node.x > max_x) {
                    max_x = node.x;
                }

                if (node.y > max_y) {
                    max_y = node.y;
                }

                if (node.x < min_x) {
                    min_x = node.x;
                }

                if (node.y < min_y) {
                    min_y = node.y;
                }
            });

            var roundTable = tablet;

            if (numNodes === "3") {
                roundTable.x = (max_x - min_x) / 2 + min_x - 15.6;
                roundTable.y = (max_y - min_y) / 2 + min_y;
                roundTable.radius = 30;
            } else {
                roundTable.x = (max_x - min_x) / 2 + min_x;
                roundTable.y = (max_y - min_y) / 2 + min_y;
                roundTable.radius = 30;
            }

            var detaX = max_x - min_x;
            var detaY = max_y - min_y;

            return [detaX, detaY];
        },

        /**
         * create combo table and seat
         *
         * @param chairsInputData
         * @param rawTableInputData
         */
        manipulateTheNode: function (chairsInputData, rawTableInputData) {
            var self = this;
            var group = new Konva.Group({
                draggable: true
            });

            var currentX = group.getAttr('x');
            var currentY = group.getAttr('y');
            var table = tablet;
            table.label = toLetters(self.lastAlphaKey);
            table.x = rawTableInputData.x;
            table.y = rawTableInputData.y;
            table.radius = 35;
            table.fill = '#89b717';
            table.objectType = 'table';
            var tableCircle = new Konva.Circle(table);
            var tableLabel = new Konva.Text({
                x: rawTableInputData.x - 2,
                y: rawTableInputData.y - 9,
                text: toLetters(self.lastAlphaKey),
                fontFamily: 'Calibri',
                fill: 'black',
                fontStyle: 'bold',
                fontSize: 20
            });
            group.add(tableLabel);
            group.add(tableCircle);
            self.tables.push(tableCircle);
            self.layer.add(group);
            tableLabel.moveToTop();
            var currentX1 = group.clipX();
            var currentY1 = group.getAttr('y');
            var i = 0;
            $.each(chairsInputData, function (index, chairData) {
                var chair = chairt;
                chair.label = toLetters(self.lastAlphaKey) + i;
                chair.x = chairData.x;
                chair.y = chairData.y;
                chair.radius = 20;
                chair.fill = 'silver';
                chair.objectType = 'chair';
                var chairCircle = new Konva.Circle(chair);
                var chairLabel = new Konva.Text({
                    x: chairData.x - 6,
                    y: chairData.y - 8,
                    text: toLetters(self.lastAlphaKey) + i,
                    fontFamily: 'Calibri',
                    fill: 'black',
                    fontStyle: 'bold',
                    fontSize: 16
                });
                self.seats.push(chairCircle);
                self.labels.push(chairLabel);
                group.add(chairLabel);
                group.add(chairCircle);
                chairLabel.moveToTop();
                i++;
            });
            self.lastAlphaKey++;
            self.mapModeSelectObj(group);
            self.venueMapData(JSON.stringify(self.stage()));
            $('#venue_map_data').change();
        },

        mapModeSelectObj: function (item) {
            var self = this;
            item.on('click.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
            });
            item.on('dragend.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
                self.venueMapData(JSON.stringify(self.stage()));
                $('#venue_map_data').change();
            });
            item.on('dragstart.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
            });
        },

        /**
         * duplicate and table combo
         */
        mapModeDuplicate: function () {
            var self = this;
            self.activeMode('map');
            if (self.activeMode() === 'map') {
                var activeObj = self.activeObject();
                if (activeObj !== undefined) {
                    var numChair = (parseFloat(activeObj.getChildren().length) / 2) - 1;
                    self.initTableAndChair(1, numChair);
                    self.layer.draw();
                }
            }
            self.lastAlphaKey++;
            self.venueMapData(JSON.stringify(self.stage()));
            $('#venue_map_data').change();
        }
        ,

        /**
         * delete table combo
         */
        mapModeDelete: function () {
            var self = this;
            self.activeMode('map');
            if (self.activeMode() === 'map') {
                var activeObj = self.activeObject();
                if (activeObj !== undefined) {
                    // $.each(activeObj.children, function (index, item) {
                    //     $.each(self.tickets, function (index, ticket) {
                    //         if (self.chairInTicket(item, ticket)) {
                    //             remove(ticket.chairs, item);
                    //         }
                    //     });
                    // });
                    activeObj.destroy();
                    self.layer.draw();
                }
            }
            self.lastAlphaKey--;
            self.venueMapData(JSON.stringify(self.stage()));
            $('#venue_map_data').change();
        }
        ,

        /**
         * create table combo
         */
        mapModeCreate: function () {
            var self = this;
            self.initTableAndChair(self.tableNumber(), self.chairNumber());
            self.layer.draw();
        }
        ,

        /**
         * save all coordinates of objects on the stage and re-drawing without grouping
         */
        transferToTicketMode: function () {
            var self = this;
            $.each(self.layer.getChildren(), function (index, item) {
                item.setAttrs({draggable: false});
            });
            self.layer.draw();
            self.bindClickActionTicketMode();
        }
        ,

        transferToMapMode: function () {
            var self = this;
            $.each(self.seats(), function (index, chair) {
                //if the chair is selected then de-select it
                chair.off('click.ticket');
            });
            $.each(self.layer.getChildren(), function (index, item) {
                item.setAttrs({draggable: true});
            });
            self.layer.draw();
        }
        ,

        /**
         * binding each chair with a ticket type
         */
        bindClickActionTicketMode: function () {
            var self = this;
            if (self.seats().length > 0) {
                $.each(self.seats(), function (index, chair) {
                    //if the chair is selected then de-select it
                    chair.on('click.ticket', function () {
                        if (!self.currentTicket) {
                            return;
                        }
                        if (self.chairInTicket(chair, self.currentTicket)
                            && chair.getAttr('fill') !== self.defaultColor
                            && chair.getAttr('fill') === self.currentTicket.colorTag()) {
                            chair.setAttrs({fill: self.defaultColor});
                            var tmp_currentTicket = [];
                            if (self.currentTicket.chairs.length > 0) {
                                ko.utils.arrayForEach(self.currentTicket.chairs, function (seat) {
                                    if (seat !== chair) {
                                        tmp_currentTicket.push(seat);
                                    }
                                });
                            }
                            self.currentTicket.chairs = tmp_currentTicket;
                        } else if (chair.getAttr('fill') === self.defaultColor) {
                            chair.setAttrs({fill: self.currentTicket.colorTag()});
                            self.currentTicket.chairs.push(chair);
                        }
                        self.layer.draw();
                        self.currentTicket.cnt(self.currentTicket.chairs.length);
                        self.parseObjectData();
                    });
                });
                $.each(self.labels(), function (index, label) {
                    label.on('click', function () {
                        var label = this;
                        var labelText = label.getAttr('text');
                        $.each(self.seats(), function (index, chair) {
                            if (chair.getAttr('label') === labelText) {
                                chair.fire('click');
                            }
                        });
                    });
                });
            }
        },

        changeStageSize: function (width, height) {
            var self = this;
            self.stage().height(height);
            self.stage().width(width);
            self.layer.draw();
            self.venueMapData(JSON.stringify(self.stage()));
            $('#venue_map_data').change();
        },

        chairInTicket: function (chair, ticket) {
            for (var i = 0; i < ticket.chairs.length; i++) {
                if (ticket.chairs[i] === chair) {
                    return true;
                }
            }
            return false;
        }
        ,
        /**
         * assign seats to ticket type
         */
        addTicket: function () {
            var self = this;
            var newTicket = new ticket;
            newTicket.label = ko.observable('Ticket ' + (self.tickets().length + 1));
            newTicket.id = (self.tickets().length + 1);
            self.tickets.push(newTicket);

            var $el = $(".ticket-color[name='label_" + newTicket.id + "']");
            $el.ColorPicker({
                color: '',
                onChange: function (hsb, hex, rgb) {
                    $el.css("backgroundColor", "#" + hex).val("#" + hex);
                    $el.val("#" + hex);
                    $el.change();
                    $.each(newTicket.chairs, function (index, chair) {
                        chair.setAttrs({fill: "#" + hex});
                    });
                    self.layer.draw();
                }
            });
            $('input.switch[name="switch_' + newTicket.id + '"]').on('change', function (event) {
                $('input.switch[name!="switch_' + newTicket.id + '"]').prop('checked', null);
                if ($('input.switch[name="switch_' + newTicket.id + '"]:checked').length) {
                    self.currentTicket = newTicket;
                }
                else {
                    self.currentTicket = false;
                }
            });
        }
        ,

        /**
         *
         * @param tabName
         */
        chooseTab: function (tabName) {
            var self = this;
            self.activeMode(tabName);
            var selector = 'div[data-role="ticket"]';

            if (tabName == 'ticket') {
                self.transferToTicketMode();
                $(selector).show();
            }
            if (tabName == 'map') {
                self.transferToMapMode();
                $(selector).hide();
            }
        }
        ,

        /**
         * assign seats to ticket type
         */
        assignSeatsToTicket: function (ticketType) {
            var ticket = arguments[0];
            var self = this;
            if (self.selectedSeats.length > 0) {
                $.each(self.selectedSeats, function (index, seat) {
                    seat.ticketType = ticket.label;
                });
                ticket.cnt(self.selectedSeats.length);
            }
        }
        ,

        renderedTicketHandle: function (elements, ticket) {
            var self = this;
            var $el = $(".ticket-color[name='label_" + ticket.id + "']");
            $el.css("backgroundColor", ticket.colorTag()).val(ticket.colorTag());
            $el.val(ticket.colorTag());
            $el.ColorPicker({
                color: ticket.colorTag(),
                onChange: function (hsb, hex, rgb) {
                    $el.css("backgroundColor", "#" + hex).val("#" + hex);
                    $el.val("#" + hex);
                    self.parseObjectData();
                    $el.change();
                    $.each(ticket.chairs, function (index, chair) {
                        chair.setAttrs({fill: "#" + hex});
                    });
                    self.layer.draw();
                }
            });
            $('input.switch[name="switch_' + ticket.id + '"]').on('change', function (event) {
                $('input.switch[name!="switch_' + ticket.id + '"]').prop('checked', null);
                if ($('input.switch[name="switch_' + ticket.id + '"]:checked').length) {
                    self.currentTicket = ticket;
                }
                else {
                    self.currentTicket = false;
                }
            });
        },

        parseObjectData: function () {
            var self = this;
            var ticketData = [];
            if (self.tickets()) {
                $.each(self.tickets(), function (index, ticketRecord) {
                    var newRecord = new ticket();
                    newRecord.label = ticketRecord.label();
                    newRecord.id = ticketRecord.id;
                    newRecord.cnt = ticketRecord.cnt();
                    if (ticketRecord.colorTag() === "") {
                        newRecord.colorTag = '#ffffff';
                    } else {
                        newRecord.colorTag = ticketRecord.colorTag();
                    }
                    newRecord.chairs = ticketRecord.chairs;
                    ticketData.push(newRecord);
                });
            } else {
                self.tickets(null);
            }
            self.venueTicketData(JSON.stringify(ticketData));
            $('#venue_ticket_data').change();
        },

        bindTicketWithChairGlobal: function (tickets) {
            var self = this;
            $.each(tickets, function (index, ticket) {
                if (ticket.chairs.length > 0) {
                    var chairsDataBind = [];
                    $.each(ticket.chairs, function (index, chairs) {
                        var chairX = chairs.attrs.x;
                        var chairY = chairs.attrs.y;
                        $.each(self.seats(), function (index, seat) {
                            var seatX = seat.attrs.x;
                            var seatY = seat.attrs.y;
                            var deltaX = seatX - chairX;
                            var deltaY = seatY - chairY;
                            if (deltaX === 0 && deltaY === 0) {
                                chairsDataBind.push(seat);
                                seat.setAttrs({fill: ticket.colorTag()});
                            }
                        });
                    });
                    ticket.chairs = chairsDataBind;
                }
            });
        }
    });
});
