define([
    'ko'
], function (ko) {
    'use strict';

    return function () {
        return {
            id: 0,
            label: ko.observable('Ticket '),
            cnt: ko.observable(0),
            colorTag: ko.observable(''),
            chairs: []
        }
    };
});