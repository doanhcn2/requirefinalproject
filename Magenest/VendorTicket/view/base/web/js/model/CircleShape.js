define([
    'ko'
], function (ko) {
    'use strict';

    var model = {
        label: 'A',
        x: 10,
        y: 100,
        radius: 10,
        fill: 'red',
        stroke: 'black',
        strokeWidth: 4,
        objectType : 'obj'

    };
     return model;
});
