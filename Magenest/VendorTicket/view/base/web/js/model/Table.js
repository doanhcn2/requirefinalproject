define([
    'ko',
    'Magenest_VendorTicket/js/model/CircleShape'

], function (ko,CircleShape) {
    'use strict';
    CircleShape.label = '';
    CircleShape.fill = 'purple';
    CircleShape.radius = 35;
    CircleShape.objectType = 'table';
    return CircleShape;
});
