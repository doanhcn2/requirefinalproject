define([
    'ko',
    'Magenest_VendorTicket/js/model/CircleShape'
], function (ko, CircleShape) {
    'use strict';

    CircleShape.label = '';
    CircleShape.fill = 'silver';
    CircleShape.ticketType ='';
    CircleShape.x = 122;
    CircleShape.y = 50;
    CircleShape.radius = 20;
    CircleShape.objectType = 'chair';
    return CircleShape;
});
