/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            konva: 'https://cdn.rawgit.com/konvajs/konva/1.7.6/konva.min.js'
        }
    }
};
