<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) //@codingStandardsIgnoreLine
    {
        $setup->startSetup();

        /**
         * Create table 'magenest_vendorticket_venuemap'
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('magenest_vendorticket_venuemap')
        )->addColumn(
            'venue_map_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Venue Map ID'
        )->addColumn(
            'venue_map_name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Venue Map Name'
        )->addColumn(
            'venue_map_data',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => null],
            'Venue Map Data'
        )->addColumn(
            'created',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Venue Map Created Time'
        )->setComment(
            'VenueMap Table'
        );

        // Add more columns here
        $setup->getConnection()->createTable($table);

        /**
         * Create table 'magenest_vendorticket_venuemapdata'
         */
        $table = $setup->getConnection()->newTable(
            $setup->getTable('magenest_vendorticket_venuemapdata')
        )->addColumn(
            'map_data_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'VenueMap Data ID'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Product Id'
        )->addColumn(
            'venue_map_id',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Venue Map Id'
        )->addColumn(
            'session_map_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Venue Map Id For Session'
        )->addColumn(
            'session_map_name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Session Map Name'
        )->addColumn(
            'session_map_data',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Venue Map Data Per Each Session'
        )->addColumn(
            'session_ticket_data',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true, 'default' => null],
            'Venue Map Ticket Data'
        )->setComment(
            'Venue Map Data Table For Session'
        );

        // Add more columns here
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}
