<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorTicket\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package Magenest\Ticket\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /**
             * Add more column to table for new reserved seating booking product type
             */
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_vendorticket_venuemap'),
                'vendor_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => null,
                    'comment' => 'Vendor ID who create this map'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_vendorticket_venuemapdata'),
                'vendor_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 15,
                    'default' => null,
                    'comment' => 'Vendor ID who create this map'
                ]
            );
        }
        $setup->endSetup();
    }
}
