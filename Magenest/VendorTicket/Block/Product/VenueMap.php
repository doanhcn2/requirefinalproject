<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 27/12/2017
 * Time: 10:16
 */

namespace Magenest\VendorTicket\Block\Product;

use Magento\Framework\View\Element\Template;
use Magenest\VendorTicket\Model\VenueMapFactory;

class VenueMap extends Template
{
    /**
     * @var $_venueMapFactory VenueMapFactory
     */
    protected $_venueMapFactory;

    public function __construct(Template\Context $context, VenueMapFactory $venueMapFactory, array $data = [])
    {
        $this->_venueMapFactory = $venueMapFactory;
        parent::__construct($context, $data);
    }

    public function getVenueMapData()
    {
        $mapDataId = $this->getRequest()->getParam('venue_map_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('venue_map_id', $mapDataId)->addFieldToFilter('vendor_id', $this->getVendorId())->getFirstItem();
        return $sessionMapData->getData() === null ? null : $sessionMapData;
    }

    public function getIdentifierKey($venueMapId)
    {
        if ($venueMapId == null) {
            return 'A';
        }
        /**
         * @todo: need to change if save venue map method change
         */
        $venueMapData = $this->_venueMapFactory->create()->load($venueMapId)->getVenueMapData();
        if ($venueMapData) {
            $data = json_decode($venueMapData, true);
            $allmapData = json_decode($data, true);
            $shapeObject = $allmapData['children'][0]['children'];
            $lastObjectNum = count($shapeObject);
            if ($lastObjectNum !== 0) {
                $lastKey = $shapeObject[$lastObjectNum - 1]['children'][0]['attrs']['label'];
                return $lastKey;
            }
        }
        return 'A';
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}