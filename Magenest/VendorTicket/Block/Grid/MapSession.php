<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\VendorTicket\Block\Grid;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magento\Catalog\Model\ProductFactory;

class MapSession extends Template
{
    /**
     * @var VenueMapDataFactory $_venueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var VenueMapFactory $_venueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var $_products ProductFactory
     */
    protected $_products;

    public function __construct(
        Context $context,
        VenueMapFactory $venueMapFactory,
        ProductFactory $productFactory,
        VenueMapDataFactory $venueMapDataFactory,
        array $data = [])
    {
        $this->_products = $productFactory;
        $this->_venueMapFactory = $venueMapFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magenest\VendorTicket\Block\Grid\Pager $toolbar */
        try {
            if ($toolbar = $this->getLayout()->getBlock('venuemap.grid.toolbar')) {
                $toolbar->setCollection($this->getVenueMapDataCollection());
                $this->setChild('toolbar', $toolbar);
            }
        } catch (LocalizedException $e) {
        }
        return $this;
    }

    public function getProductNameById($id) {
        $product = $this->_products->create()->load($id);
        return $product->getName();
    }

    public function getVenueMapNameById($id) {
        $name = $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('venue_map_id', $id)->getFirstItem()->getVenueMapName();
        return $name;
    }

    public function getVenueMapDataCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 10;
        return $this->_venueMapDataFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize($pageSize)->setCurPage($page)->setOrder('product_id', 'DES');
    }

    public function getVendorId(){
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}