<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\VendorTicket\Block\Grid;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\VendorTicket\Model\VenueMapFactory;

class VenueMap extends Template
{
    /**
     * @var VenueMapFactory $_venueMapFactory
     */
    protected $_venueMapFactory;

    public function __construct(
        Context $context,
        VenueMapFactory $venueMapFactory,
        array $data = [])
    {
        $this->_venueMapFactory = $venueMapFactory;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magenest\VendorTicket\Block\Grid\Pager $toolbar */
        if ($toolbar = $this->getLayout()->getBlock('venuemap.grid.toolbar')) {
            $toolbar->setCollection($this->getVenueMapCollection());
            $this->setChild('toolbar', $toolbar);
        }
        return $this;
    }

    public function getVenueMapCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 10;
        return $this->_venueMapFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize($pageSize)->setCurPage($page)->setOrder('created', 'DES');
    }

    public function getVendorId(){
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}