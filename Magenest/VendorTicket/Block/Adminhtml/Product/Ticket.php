<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 27/12/2017
 * Time: 10:16
 */
namespace Magenest\VendorTicket\Block\Adminhtml\Product;

use Magento\Backend\Block\Template;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magenest\Ticket\Model\SessionFactory;

class Ticket extends Template
{
    /**
     * @var $_venueMapDataFactory VenueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var $_ticketFactory TicketsFactory
     */
    protected $_ticketFactory;

    /**
     * @var $_ticketSessionFactory SessionFactory
     */
    protected $_ticketSessionFactory;

    public function __construct(Template\Context $context, VenueMapDataFactory $venueMapDataFactory, TicketsFactory $ticketsFactory, SessionFactory $sessionFactory, array $data = [])
    {
        $this->_ticketFactory = $ticketsFactory;
        $this->_ticketSessionFactory = $sessionFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        parent::__construct($context, $data);
    }

    public function isSessionMap() {
        return true;
    }

    public function getSessionData() {
        $mapDataId = $this->getRequest()->getParam('map_data_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapDataFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('map_data_id', $mapDataId)->getFirstItem();
        return $sessionMapData;
    }

    public function getSessionMapData() {
        $mapDataId = $this->getRequest()->getParam('map_data_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapDataFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('map_data_id', $mapDataId)->getFirstItem();
        return $sessionMapData->getSessionMapData();
    }
    public function getTicketSessionData() {
        $mapDataId = $this->getRequest()->getParam('map_data_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapDataFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('map_data_id', $mapDataId)->getFirstItem();
        $sessionTicketData = $sessionMapData->getSessionTicketData();
        if ($sessionTicketData == null) {
            $sessionMapId = $sessionMapData->getDataByKey('session_map_id');
            $productId = $sessionMapData->getDataByKey('product_id');
            $ticketData = $this->getTicketDataByProductAndSession($sessionMapId, $productId);
            return $this->prepareTicketData($ticketData);
        } else {
            return $sessionTicketData;
        }
    }

    protected function getTicketDataByProductAndSession($sessionMapId, $productId)
    {
        /**
         * @var $ticketCollection \Magenest\Ticket\Model\ResourceModel\Tickets\Collection
         */
        $ticketCollection = $this->_ticketFactory->create()->getCollection();
        $listTicketByProduct = $ticketCollection->addFieldToFilter('product_id', $productId);
        $sessionTicketData = [];
        foreach ($listTicketByProduct as $ticket) {
            $sessionData = $this->_ticketSessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', $ticket->getTicketsId())->addFieldToFilter('session_map_id', $sessionMapId)->getFirstItem();
            if (count($sessionData->getData()) !== 0) {
                array_push($sessionTicketData, $sessionData->getData());
            }
        }
        return $sessionTicketData;
    }

    protected function getLabelTicket($ticketId) {
        return $this->_ticketFactory->create()->getCollection()->addFieldToFilter('tickets_id', $ticketId)->getFirstItem()->getTitle();
    }

    protected function prepareTicketData($ticketData)
    {
        $ticketDataFinal = [];
        if (isset($ticketData) && !empty($ticketData) && count($ticketData) !== 0) {
            foreach ($ticketData as $ticket) {
                $usedTicket = [];
                foreach ($ticket as $key=>$value) {
                    if ($key === "session_id") {
                        $usedTicket['id'] = $value;
                    } elseif ($key === "tickets_id") {
                        $usedTicket['label'] = $this->getLabelTicket($value);
                    } else continue;
                }
                $usedTicket['cnt'] = 0;
                $usedTicket['colorTag'] = '';
                $usedTicket['chairs'] = [];
                array_push($ticketDataFinal, $usedTicket);
            }
            return json_encode($ticketDataFinal);
        }
        return null;
    }
}