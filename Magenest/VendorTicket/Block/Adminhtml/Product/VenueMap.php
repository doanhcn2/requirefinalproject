<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 27/12/2017
 * Time: 10:16
 */
namespace Magenest\VendorTicket\Block\Adminhtml\Product;

use Magento\Backend\Block\Template;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magenest\Ticket\Model\SessionFactory;

class VenueMap extends Template
{
    /**
     * @var $_venueMapDataFactory VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var $_ticketFactory TicketsFactory
     */
    protected $_ticketFactory;

    /**
     * @var $_ticketSessionFactory SessionFactory
     */
    protected $_ticketSessionFactory;

    public function __construct(Template\Context $context, VenueMapFactory $venueMapFactory, TicketsFactory $ticketsFactory, SessionFactory $sessionFactory, array $data = [])
    {
        $this->_ticketFactory = $ticketsFactory;
        $this->_ticketSessionFactory = $sessionFactory;
        $this->_venueMapFactory = $venueMapFactory;
        parent::__construct($context, $data);
    }

    public function isSessionMap()
    {
        return false;
    }

    public function getVenueMapData()
    {
        $mapDataId = $this->getRequest()->getParam('venue_map_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('venue_map_id', $mapDataId)->getFirstItem();
        return $sessionMapData->getVenueMapData();
    }

    public function getVenueMapName() {
        $mapDataId = $this->getRequest()->getParam('venue_map_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('venue_map_id', $mapDataId)->getFirstItem();
        return $sessionMapData->getVenueMapName();
    }

    public function getVenueMapId() {
        $mapDataId = $this->getRequest()->getParam('venue_map_id');
        if ($mapDataId === null) {
            return null;
        }
        /**
         * @var $venueMap \Magenest\VendorTicket\Model\ResourceModel\VenueMapData\Collection
         */
        $venueMap = $this->_venueMapFactory->create()->getCollection();
        $sessionMapData = $venueMap->addFieldToFilter('venue_map_id', $mapDataId)->getFirstItem();
        return $sessionMapData->getVenueMapId();
    }
}