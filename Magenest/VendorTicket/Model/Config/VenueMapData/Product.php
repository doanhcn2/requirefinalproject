<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Model\Config\VenueMapData;

use Magento\Framework\Option\ArrayInterface;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;

class Product implements ArrayInterface
{
    /**
     * @var $_venueMapFactory VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var $_eventFactory EventFactory
     */
    protected $_eventFactory;

    public function __construct(VenueMapFactory $venueMapFactory, EventFactory $eventFactory)
    {
        $this->_eventFactory = $eventFactory;
        $this->_venueMapFactory = $venueMapFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        /**
         * @var $eventCollection \Magenest\Ticket\Model\ResourceModel\Event\Collection
         */
        $eventCollection = $this->_eventFactory->create()->getCollection();
        $allEvent = $eventCollection->addFieldToFilter('is_online', '1')->addFieldToFilter('is_reserved_seating', '1');
        $result = [];
        foreach ($allEvent as $options) {
            $item = ['label' => $options->getEventName(), 'value' => $options->getProductId()];
            array_push($result, $item);
        }
        return $result;
    }
}
