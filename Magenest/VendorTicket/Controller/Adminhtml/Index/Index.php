<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 27/12/2017
 * Time: 09:59
 */

namespace Magenest\VendorTicket\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\View\Result\Page;

class Index extends Action
{
    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    public function __construct(Action\Context $context, Page $page, PageFactory $pageFactory) {
        $this->_resultPage = $page;
        $this->_resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        if ($this->getRequest()->getParam('map_data_id')) {
            $resultPage->getConfig()->getTitle()->set(__('Edit Session Map'));
        } else {
            $resultPage->getConfig()->getTitle()->set(__('Add New Session Map'));
        }

        return $resultPage;
    }
}