<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Controller\Adminhtml\VenueMapData;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magenest\VendorTicket\Model\ResourceModel\VenueMapData\CollectionFactory;

/**
 * Class MassDelete
 */
class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /** @var CollectionFactory $objectCollection */
    protected $objectCollection;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        $this->filter = $filter;
        $this->objectCollection = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $collectionSize = 0;
        $venueCollection = $this->objectCollection->create();
        if (array_key_exists('selected', $params) && isset($params['selected'])) {
            foreach ($params['selected'] as $recordId) {
                $venueCollection->addFieldToSelect('map_data_id', $recordId)->getFirstItem()->delete();
                $collectionSize++;
            }
        } elseif (array_key_exists('excluded', $params) && isset($params['excluded'])) {
            if ($params['excluded'] == false) {
                foreach ($venueCollection->getItems() as $item) {
                    $item->delete();
                    $collectionSize++;
                }
            } else {
                foreach ($venueCollection->getItems() as $item) {
                    if (!in_array($item->getDataByKey('map_data_id'), $params['excluded'])) {
                        $item->delete();
                        $collectionSize++;
                    }
                }
            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
