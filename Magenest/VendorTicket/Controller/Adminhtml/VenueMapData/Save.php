<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Controller\Adminhtml\VenueMapData;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\Ticket\Model\SessionFactory;

class Save extends Action
{
    /** @var VenueMapDataFactory $objectFactory */
    protected $objectFactory;

    /**
     * @var $_sessionFactory SessionFactory
     */
    protected $_sessionFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param VenueMapDataFactory $objectFactory
     * @param SessionFactory $sessionFactory
     */
    public function __construct(
        Context $context,
        SessionFactory $sessionFactory,
        VenueMapDataFactory $objectFactory
    ) {
        $this->_sessionFactory = $sessionFactory;
        $this->objectFactory = $objectFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_VendorTicket::venuemapdata');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $params = [];
            $objectInstance = $this->objectFactory->create();
            $idField = $objectInstance->getIdFieldName();
            if (empty($data[$idField])) {
                $data[$idField] = null;
            } else {
                $objectInstance->load($data[$idField]);
                $params[$idField] = $data[$idField];
            }
            $objectInstance->addData($data);

            $this->_eventManager->dispatch(
                'magenest_vendorticket_venuemapdata_prepare_save',
                ['object' => $this->objectFactory, 'request' => $this->getRequest()]
            );

            $this->updateTicketQty(json_decode($data['session_ticket_data'], true));
            try {
                $objectInstance->save();
                $this->messageManager->addSuccessMessage(__('You saved this record.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $params = [$idField => $objectInstance->getId(), '_current' => true];
                    return $resultRedirect->setPath('*/*/edit', $params);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record.'));
            }

            $this->_getSession()->setFormData($this->getRequest()->getPostValue());
            return $resultRedirect->setPath('*/*/edit', $params);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function updateTicketQty($ticketData) {
        foreach ($ticketData as $ticket) {
            $sessionModel = $this->_sessionFactory->create()->load($ticket['id']);
            $sessionModel->setQty("{$ticket['cnt']}");
            $sessionModel->setQtyAvailable("{$ticket['cnt']}");
            $sessionModel->save();
        }
    }
}
