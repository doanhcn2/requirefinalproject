<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Controller\Adminhtml\VenueMapData;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapDataFactory;

class Delete extends Action
{
    /** @var VenueMapDataFactory $objectFactory */
    protected $objectFactory;

    /**
     * @param Context $context
     * @param VenueMapDataFactory $objectFactory
     */
    public function __construct(
    Context $context,
    VenueMapDataFactory $objectFactory
    ) {
        $this->objectFactory = $objectFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_VendorTicket::venuemapdata');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
//        $id = $this->getRequest()->getParam('map_data_id', null);
//        For disable delete Venue Map For Session function
        $id = null;

        try {
            $objectInstance = $this->objectFactory->create()->load($id);
            if ($objectInstance->getId()) {
                $objectInstance->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        
        return $resultRedirect->setPath('*/*');
    }
}
