<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorTicket\Controller\Grid;

use Magento\Backend\App\Action;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class MapSession extends AbstractVendor
{
    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_renderPage(null);
    }
}
