<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorTicket\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magento\Framework\Controller\ResultFactory;

class Delete extends Action
{
    /** @var VenueMapDataFactory $_venueMapFactory */
    protected $_venueMapDataFactory;

    /**
     * Delete constructor.
     * @param Context $context
     * @param VenueMapDataFactory $venueMapDataFactory
     */
    public function __construct(
        Context $context,
        VenueMapDataFactory $venueMapDataFactory
    )
    {
        $this->_venueMapDataFactory = $venueMapDataFactory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
//        $id = $this->getRequest()->getParam('map_data_id', null);
//        For disable delete Venue Map For Session function
        $id = null;

        try {
            $objectInstance = $this->_venueMapDataFactory->create()->load($id);
            if ($objectInstance->getId()) {
                $objectInstance->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        return $resultRedirect->setPath('vendorticket/grid/mapsession');
    }
}
