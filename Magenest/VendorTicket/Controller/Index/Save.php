<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorTicket\Controller\Index;

use Magenest\VendorTicket\Model\Config\VenueMapData\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Ticket\Model\SessionFactory;

class Save extends Action
{
    /** @var VenueMapDataFactory $_venueMapDataFactory*/
    protected $_venueMapDataFactory;

    /**
     * @var $_sessionFactory SessionFactory
     */
    protected $_sessionFactory;

    /**
     * @var $_ticketOrderFactory \Magenest\Ticket\Model\TicketFactory;
     */
    protected $_ticketOrderFactory;

    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param SessionFactory $sessionFactory
     */
    public function __construct(Context $context,
                                VenueMapDataFactory $venueMapDataFactory,
                                SessionFactory $sessionFactory,
                                \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
                                \Magento\Sales\Model\OrderFactory $orderFactory,
                                \Magenest\Ticket\Model\TicketFactory $ticketFactory,
                                \Magento\Catalog\Model\ProductFactory $productFactory)
    {
        $this->productFactory = $productFactory;
        $this->_unirgyProductFactory = $unirgyProductFactory;
        $this->_orderFactory = $orderFactory;
        $this->_ticketOrderFactory = $ticketFactory;
        $this->_sessionFactory = $sessionFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|ResultFactory|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        /** @var \Magento\Framework\Controller\ResultFactory $resultJson */
        $resultJson =  $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($data) {
            $params = [];
            $objectInstance = $this->_venueMapDataFactory->create();
            $idField = $objectInstance->getIdFieldName();
            if (empty($data[$idField])) {
                $data[$idField] = null;
            } else {
                $objectInstance->load($data[$idField]);
                $params[$idField] = $data[$idField];
            }
            $objectInstanceData = $objectInstance->getData();
            foreach ($objectInstanceData as $key => $value) {
                if (!array_key_exists($key, $data) || $key === 'session_map_data' || $key === 'session_ticket_data') {
                    if ($key === 'session_map_data') {
                        if (isset($data['session_map_data']) && !empty($data['session_map_data'])) {
                            continue;
                        } else {
                            $data['session_map_data'] = $value;
                        }
                    }
                    if ($key === 'session_ticket_data') {
                        if (isset($data['session_ticket_data']) && !empty($data['session_ticket_data'])) {
                            continue;
                        } else {
                            $data['session_ticket_data'] = $value;
                        }
                    } else {
                        $data[$key] = $value;
                    }
                }
            }
            $objectInstance->addData($data);

            $this->updateTicketQty(json_decode($data['session_ticket_data'], true));
            try {
                $objectInstance->save();
                $this->messageManager->addSuccessMessage(__('You saved this record.'));
                $resultJson->setData(['success' => true]);
                $this->updateProductQuantity($data['product_id']);
                return $resultJson;
            } catch (\Exception $e) {
                $resultJson->setData(['success' => false]);
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record.'));
            }
        }

        return $resultJson;
    }

    public function updateTicketQty($ticketData) {
        foreach ($ticketData as $ticket) {
            $sessionModel = $this->_sessionFactory->create()->load($ticket['id']);
            $sessionModel->setQty("{$ticket['cnt']}");
            $sessionModel->setQtyAvailable("{$ticket['cnt']}");
            $sessionModel->save();
        }
    }

    private function updateProductQuantity($productId)
    {
        $qty = $this->getTicketQuantity($productId);
        if (@$qty !== 0) {
            /**
             * @var $product \Magento\Catalog\Model\Product
             */
            $product = $this->productFactory->create();
            try {
                $product->load($productId)->setQty(floatval($qty))->save();
            } catch (\Exception $e) {
            }

            $unirgyProduct = $this->_unirgyProductFactory->create()->load($productId, 'product_id');
            $unirgyProduct->setStockQty(floatval($qty))->save();
        }
    }

    private function getTicketQuantity($productId)
    {
        $count = 0;
        $allSession = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        foreach ($allSession as $session) {
            $ticket_info = json_decode($session->getSessionTicketData(), true);
            foreach ($ticket_info as $ticket) {
                $count += intval($ticket['cnt']);
            }
        }

        $ordereds = $this->_ticketOrderFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $orderedCount = 0;
        foreach ($ordereds as $order) {
            $orderedCount ++;
        }
        if (@$orderedCount) {
            return $count - intval($orderedCount);
        }

        return $count;
    }
}
