<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 27/12/2017
 * Time: 10:00
 */
namespace Magenest\VendorTicket\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Controller\ResultFactory;
use Unirgy\Dropship\Controller\VendorAbstract;

class Index extends VendorAbstract
{

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     */
    public function execute()
    {
        $this->_renderPage(null);
    }
}