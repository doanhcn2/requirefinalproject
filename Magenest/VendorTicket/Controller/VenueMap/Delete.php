<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorTicket\Controller\VenueMap;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magento\Framework\Controller\ResultFactory;
use Unirgy\Dropship\Helper\Data as HelperData;

class Delete extends Action
{
    /**
     * @var HelperData
     */
    protected $_hlp;

    /** @var VenueMapFactory $_venueMapFactory */
    protected $_venueMapFactory;

    /**
     * Delete constructor.
     * @param Context $context
     * @param VenueMapFactory $venueMapFactory
     * @param HelperData $helperData
     */
    public function __construct(
        Context $context,
        VenueMapFactory $venueMapFactory,
        HelperData $helperData
    )
    {
        $this->_hlp = $helperData;
        $this->_venueMapFactory = $venueMapFactory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('venue_map_id', null);

        try {
            $objectInstance = $this->_venueMapFactory->create()->load($id);
            if ($this->isAllowed($objectInstance->getVendorId())) {
                $this->messageManager->addErrorMessage(__('You can\'t delete this record.'));
            } elseif ($objectInstance->getId()) {
                $objectInstance->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        return $resultRedirect->setPath('vendorticket/grid/venuemap');
    }

    public function isAllowed($vendorId)
    {
        $currentVendorId = $this->_hlp->session()->getVendorId();
        if ($currentVendorId === $vendorId) {
            return false;
        }
        return true;
    }
}
