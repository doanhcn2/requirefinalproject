<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorTicket\Controller\VenueMap;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magento\Framework\Controller\ResultFactory;

class Save extends Action
{
    /** @var VenueMapFactory $_venueMapFactory */
    protected $_venueMapFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param VenueMapFactory $venueMapFactory
     */
    public function __construct(Context $context, VenueMapFactory $venueMapFactory)
    {
        $this->_venueMapFactory = $venueMapFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultJson =  $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($data) {
            $params = [];
            $objectInstance = $this->_venueMapFactory->create();
            $idField = $objectInstance->getIdFieldName();
            if (empty($data[$idField])) {
                $data[$idField] = null;
            } else {
                $objectInstance->load($data[$idField]);
                $params[$idField] = $data[$idField];
            }
            $objectInstance->addData($data);

            try {
                $objectInstance->save();
                $this->messageManager->addSuccessMessage(__('You saved this record.'));
                $resultJson->setData(['success' => true]);
                return $resultJson;
            } catch (\Exception $e) {
                $resultJson->setData(['success' => false]);
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record.'));
            }
        }

        return $resultJson;
    }
}
