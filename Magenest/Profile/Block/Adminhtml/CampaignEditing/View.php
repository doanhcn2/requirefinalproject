<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\CampaignEditing;

use Magenest\Profile\Model\CampaignEditingFactory;
use Magento\Backend\Block\Widget\Context;

/**
 * Class View
 * @package Magenest\Profile\Block\Adminhtml\CampaignEditing
 */
class View extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @var $_campaignEditFactory CampaignEditingFactory
     */
    protected $_campaignEditFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_eventLocationFactory \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $_eventLocationFactory;

    /**
     * @var $_deliveryFactory \Magenest\Groupon\Model\DeliveryFactory
     */
    protected $_deliveryFactory;

    /**
     * @var $_ticketsFactory \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketsFactory;

    /**
     * @var $_sessionFactory \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    public function __construct(
        Context $context,
        CampaignEditingFactory $campaignEditing,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        array $data = []
    ) {
        $this->_sessionFactory = $sessionFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_eventLocationFactory = $eventLocationFactory;
        $this->_eventFactory = $eventFactory;
        $this->_deliveryFactory = $deliveryFactory;
        $this->_locationFactory = $locationFactory;
        $this->_dealFactory = $dealFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_productFactory = $productFactory;
        $this->_campaignEditFactory = $campaignEditing;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getNewProductData()
    {
        $campaignNew = $this->getCampaign();
        $NewProductData = $campaignNew->getNewProductData();
        $NewProductDataArray = json_decode($NewProductData, true);
        return $NewProductDataArray;
    }

    /**
     * @return mixed
     */
    public function getCampaign()
    {
        $id = $this->getRequest()->getParam('id');
        return $this->_campaignEditFactory->create()->load($id);
    }

    /**
     * @return mixed
     */
    public function getNewCouponOptionData()
    {
        $campaignNew = $this->getCampaign();
        $NewCouponData = $campaignNew->getNewCouponData();
        $NewCouponDataArray = json_decode($NewCouponData, true);
        return $NewCouponDataArray;
    }

    /**
     * @return mixed
     */
    public function getNewLocationData()
    {
        $campaignNew = $this->getCampaign();
        $NewCouponData = $campaignNew->getNewCouponLocation();
        $NewCouponDataArray = json_decode($NewCouponData, true);
        return $NewCouponDataArray;
    }

    /**
     * @return mixed
     */
    public function getTicketInfoChange()
    {
        $campaignNew = $this->getCampaign();
        $NewCouponData = $campaignNew->getTicketInfoChange();
        $NewCouponDataArray = json_decode($NewCouponData, true);
        return $NewCouponDataArray;
    }

    public function getTicketEventLocation()
    {
        $productId = $this->getProductId();
        $locations = $this->_eventLocationFactory->create()->load($productId, 'product_id');
        if ($locations->getId()) {
            return $locations->getData();
        }
        return [];
    }

    public function getCurrentTicketType()
    {
        $eventData = $this->getCurrentEventData();
        return $eventData['ticket_type'];
    }

    public function getCurrentTicketInformation()
    {
        $productId = $this->getProductId();
        $tickets = $this->_ticketsFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getData();
        foreach($tickets as &$ticket) {
            $ticket['sessions'] = $this->_sessionFactory->create()->getCollection()
                ->addFieldToFilter('tickets_id', $ticket['tickets_id'])->getData();
        }

        return $tickets;
    }

    /**
     * @return mixed
     */
    public function getCurrentCouponOptionData()
    {
        $id = $this->getProductId();
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $couponOptionCollection */
        $couponOptionCollection = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $id);
        $udropshipVendorProduct = $couponOptionCollection->getTable('udropship_vendor_product');
        $couponOptionCollection->getSelect()->joinLeft(
            ['uvp' => $udropshipVendorProduct],
            "main_table.product_id = uvp.product_id",
            ['vendor_price' => 'uvp.vendor_price', 'special_price' => 'uvp.special_price', 'stock_qty' => 'uvp.stock_qty']
        );
        $productTable = $couponOptionCollection->getTable('catalog_product_entity_varchar');
        $couponOptionCollection->getSelect()->joinLeft(
            ['cpev' => $productTable],
            "cpev.attribute_id = 73 && cpev.row_id = main_table.product_id",
            ['name' => "cpev.value"]
        )->group('deal_id');
        $newCouponAdded = $this->getNewCouponAdded();
        $currentCouponOptionDataArray = [];
        foreach($couponOptionCollection as $couponOption) {
            if (in_array($couponOption->getProductId(), $newCouponAdded)) continue;
            if ($couponOption->getVendorPrice() && $couponOption->getSpecialPrice()) {
                $currentCouponOptionDataArray[] = $couponOption->getData();
            } else {
                $vendorPrice = $this->_productFactory->create()->load($couponOption->getProductId())->getPrice();
                $specialPrice = $this->_productFactory->create()->load($couponOption->getProductId())->getSpecialPrice();
                $couponOption->setVendorPrice($vendorPrice)->setSpecialPrice($specialPrice);
                $currentCouponOptionDataArray[] = $couponOption->getData();
            }
        }
        return $currentCouponOptionDataArray;
    }

    public function getProductId()
    {
        return $this->getCampaign()->getProductId();
    }

    private function getNewCouponAdded()
    {
        $campaign = $this->getCampaign()->getCouponDealChanges();
        $coupons = json_decode($campaign, true);
        if (is_array($coupons)) {
            foreach($coupons as $key => $coupon) {
                if ($key === 'addCoupon') {
                    return $coupon;
                }
            }
        }
        return [];
    }

    public function getProductType()
    {
        $productId = $this->getProductId();
        if (\Magenest\Groupon\Helper\Data::isDeal($productId)) {
            return 'groupon';
        }
        $eventModel = $this->_eventFactory->create()->load($productId, 'product_id');
        if ($eventModel->getId()) {
            return 'ticket';
        }

        return 'simple';
    }

    /**
     * @return mixed
     */
    public function getCurrentProductData()
    {
        $productId = $this->getProductId();
        $productModel = $this->_productFactory->create()->load($productId);
        $productData = $productModel->getData();
        return $productData;
    }

    public function getCategoriesName($categoryIds)
    {
        $result = [];
        if (isset($categoryIds) && is_array($categoryIds)) {
            foreach($categoryIds as $categoryId) {
                $name = $this->_categoryFactory->create()->load($categoryId)->getName();
                array_push($result, $name);
            }
        }

        return implode(', ', $result);
    }

    public function getCurrentDealData()
    {
        $id = $this->getProductId();
        $dealModel = $this->_dealFactory->create()->load($id, 'product_id');

        return $dealModel->getData();
    }

    public function getCurrentEventData()
    {
        $id = $this->getProductId();
        $dealModel = $this->_eventFactory->create()->load($id, 'product_id');

        return $dealModel->getData();
    }

    public function prepareTermsData($terms, $isNew)
    {
        $result = "";
        if (!$isNew) {
            $terms = json_decode($terms, true);
        }
        if (@$terms['reservation']['required'] && @$terms['reservation']['required'] === 'on') {
            $result .= "Reservation Required " . (int)@$terms['reservation']['days'] . " day in advance. <br>";
        }
        if (@$terms['combine'] && $terms['combine'] === 'on') {
            $result .= "May combine coupons for greaters value. <br>";
        }
        if (@$terms['age_limit']['needed'] && @$terms['age_limit']['needed'] === 'on') {
            $result .= "No coupon required for kids below " . (int)@$terms['age_limit']['age_number'] . " years of age. <br>";
        }
        if (@$terms["sharing"] && $terms["sharing"] === 'on') {
            $result .= "Coupons cannot be shared. <br>";
        }
        if (@$terms['vendor_specify'] && trim($terms['vendor_specify']) !== "") {
            $result .= "Additional Terms: " . $terms['vendor_specify'];
        }

        return $result;
    }

    public function prepareTicketTermsData($data)
    {
        $result = "";
        if (@$data['show_up'] && (@$data['show_up'] === "on" || @$data['show_up'] === "1")) {
            $result .= "Must Show up before session start ";
            if (@$data['time_show_up']) {
                $result .= (int)@$data['time_show_up'] . " minutes before start of session ";
            } elseif (@$data['show_up_time']) {
                $result .= (int)@$data['show_up_time'] . " minutes before start of session ";
            }
            $result .= ". <br>";
        }
        if (@$data['can_purchase'] && ($data['can_purchase'] === 'on' || @$data['can_purchase'] === "1")) {
            $result .= "Additional Tickets can be purchased at venue (full price and upon availability). <br>";
        }
        if (@$data['must_print'] && ($data['must_print'] === 'on' || @$data['must_print'] === "1")) {
            $result .= "Tickets must be printed. <br>";
        }

        return $result;
    }

    public function getOldLocationData($locationIds)
    {
        $locations = explode(',', $locationIds);
        if (isset($locationIds) && is_array($locations)) {
            $result = [];
            foreach($locations as $locationId) {
                $location = $this->_locationFactory->create()->load($locationId);
                array_push($result, $this->prepareLocationDisplayData($location->getData()));
            }

            return $result;
        }
        return [];
    }

    public function prepareLocationDisplayData($location)
    {
        if (is_array($location)) {
            $result = "";
            if (@$location['location_name']) {
                $result .= $location['location_name'] . " - ";
            }
            if (@$location['street']) {
                $result .= $location['street'] . ", ";
            }
            if (@$location['region']) {
                $result .= $location['region'] . ", ";
            } else {
                if (@$location['city']) {
                    $result .= $location['city'] . ", ";
                }
            }
            if (@$location['country']) {
                $result .= $location['country'];
            }

            return $result;
        }

        return false;
    }

    public function getOldDeliveryData($deliveryIds)
    {
        $locations = explode(',', $deliveryIds);
        if (isset($deliveryIds) && is_array($locations)) {
            $result = [];
            foreach($locations as $locationId) {
                $location = $this->_deliveryFactory->create()->load($locationId);
                $deliveryText = "Address: " . $location->getAddress() . " - " . "Distance: " . $location->getDistance();
                array_push($result, $deliveryText);
            }

            return $result;
        }
        return [];
    }

    public function getImageDisplayPath($path)
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if ($path !== null) {
            $imageUrl = $mediaUrl . 'catalog/product' . $path;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    public function getCurrentCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrencyCode();
    }

    protected function _construct()
    {
        $this->_objectId   = 'row_id';
        $this->_blockGroup = 'Magenest_Profile';
        $this->_controller = 'adminhtml_editing';
        parent::_construct();
        $rejectUrl = $this->getUrl('*/*/reject');
        $this->buttonList->add('approve', [
            'label'   => __('Approve'),
            'onclick' => "document.getElementById(\"editting_form\").submit()",
            'class'   => 'primary',
        ], 0, 3);
        $this->buttonList->add('reject', [
            'label'   => __('Reject'),
            'onclick' => "document.getElementById(\"editting_form\").action='{$rejectUrl}'; document.getElementById(\"editting_form\").submit()",
            'class'   => 'secondary',
        ], 0, 2);

        $this->buttonList->remove('save');
        $this->buttonList->remove('reset');
    }
}
