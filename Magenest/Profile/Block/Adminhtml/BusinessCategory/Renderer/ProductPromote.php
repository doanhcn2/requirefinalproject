<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

/**
 * Class ProductPromote
 * @package Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer
 */
class ProductPromote extends AbstractRenderer
{
    /**
     * @param DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function render(DataObject $row)
    {
        $productPromote = $row->getProductPromote();
        switch ($productPromote) {
            case 1: return 'Groupon'; break;
            case 2: return 'Accommodation'; break;
            case 3: return 'Event'; break;
            case 4: return 'Product'; break;
        }
    }
}
