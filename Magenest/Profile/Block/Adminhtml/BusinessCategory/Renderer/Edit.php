<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

/**
 * Class Edit
 * @package Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer
 */
class Edit extends AbstractRenderer
{
    /**
     * @param DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function render(DataObject $row)
    {
        $id = $row->getId();
        $url = $this->getUrl('profile/businesscategory/edit/',['id'=>$id]);
        return '<a href="'.$url.'">Edit</a>';
    }
}
