<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;

/**
 * Class RootCategory
 * @package Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer
 */
class RootCategory extends AbstractRenderer
{
    /**
     * @param DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function render(DataObject $row)
    {
        $rootCategory = $row->getRootCategory();
        $objectManager = ObjectManager::getInstance();
        $categoryCollection = $objectManager->create('\Magento\Catalog\Model\Category');
        $categoryName = $categoryCollection->load($rootCategory);
        return $categoryName->getName();
    }
}
