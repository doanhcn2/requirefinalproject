<?php

namespace Magenest\Profile\Block\Adminhtml\BusinessCategory;

/**
 * Class BusinessCategory
 * @package Magenest\Profile\Block\Adminhtml\BusinessCategory
 */
class BusinessCategory extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        parent::_construct();
        $this->buttonList->remove('add');
        $this->_blockGroup = 'Magenest_Profile';
        $this->_controller = 'adminhtml_profile';
    }
}
