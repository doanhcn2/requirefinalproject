<?php

namespace Magenest\Profile\Block\Adminhtml\BusinessCategory;

/**
 * Class Grid
 * @package Magenest\Profile\Block\Adminhtml\BusinessCategory
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_bussinessCategoryCollection;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magenest\Profile\Model\ResourceModel\BusinessCategory\Collection $bussinessCategoryCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magenest\Profile\Model\ResourceModel\BusinessCategory\Collection $bussinessCategoryCollection,
        array $data = []
    )
    {
        $this->_bussinessCategoryCollection = $bussinessCategoryCollection;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->_bussinessCategoryCollection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->setFilterVisibility(false);
        $this->addColumn(
            'product_promote',
            [
                'header' => __('Product Promote'),
                'index' => 'product_promote',
                'renderer'=>'Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer\ProductPromote',
                'sortable' => false,
                'filter' => false
            ]
        );
        $this->addColumn(
            'root_category',
            [
                'header' => __('Root Category'),
                'index' => 'root_category',
                'renderer'=>'Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer\RootCategory',
                'sortable' => false,
                'filter' => false
            ]
        );
        $this->addColumn(
            'Action',
            [
                'header' => __('Action'),
                'renderer'=>'Magenest\Profile\Block\Adminhtml\BusinessCategory\Renderer\Edit',
                'sortable' => false,
                'filter' => false
            ]
        );
        return $this;
    }
}