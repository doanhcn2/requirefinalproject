<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\Location\Edit\Button;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Psr\Http\Message\RequestInterface;


class GoToVendor extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back To Vendor'),
            'class' => 'back',
            'on_click' => sprintf("location.href = '%s';", $this->getVendorUrl()),
        ];
    }

    public function getVendorUrl()
    {
        $id = ObjectManager::getInstance()->get(\Magento\Framework\App\RequestInterface::class)->getParam('id');
        $vendor_id = ObjectManager::getInstance()->get(\Magento\Framework\App\RequestInterface::class)->getParam('vendor_id');
        $model = ObjectManager::getInstance()->create('\Magenest\Groupon\Model\Location')->load($id);
        if ($model->getId()) {
            return $this->getUrl('udropship/vendor/edit', ['id' => $model->getData('vendor_id')]);
        } else
            return $this->getUrl('udropship/vendor/edit', ['id' => @$vendor_id]);
    }
}
