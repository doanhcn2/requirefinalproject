<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Block\Adminhtml\Location\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;


class Save extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save Location'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ],
            'sort_order' => 90
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getSaveUrl()
    {
        $vendor_id = ObjectManager::getInstance()->get(\Magento\Framework\App\RequestInterface::class)->getParam('vendor_id');

        return $this->getUrl('*/*/save', ['vendor_id' => $vendor_id]);
    }
}
