<?php
namespace Magenest\Profile\Block\Adminhtml\VendorEditTab;

use \Magento\Backend\Block\Template\Context;
use \Magento\Backend\Helper\Data as HelperData;
use \Magento\Framework\Registry;
use \Unirgy\Dropship\Helper\Data as DropshipHelperData;

class Delivery extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var DropshipHelperData
     */
    protected $_hlp;

    public function __construct(
        Registry $registry,
        DropshipHelperData $helperData,
        Context $context,
        HelperData $backendHelper,
        array $data = []
    )
    {
        $this->_registry = $registry;
        $this->_hlp = $helperData;

        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->setId('vendor_delivery_area');
        $this->setUseAjax(true);
    }

    public function getVendor()
    {
        $vendor = $this->_registry->registry('vendor_data');
        if (!$vendor) {
            $vendor = $this->_hlp->createObj('\Unirgy\Dropship\Model\Vendor')->load($this->getVendorId());
        }
        return $vendor;
    }

    protected function _prepareCollection()
    {
        $collection = $this->_hlp->createObj('\Magenest\Groupon\Model\Delivery')->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendor()->getData('vendor_id'));
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('delivery_id', array(
            'header' => __('Id'),
            'index' => 'delivery_id'
        ));
        $this->addColumn('address', array(
            'header' => __('Address'),
            'index' => 'address'
        ));
        $this->addColumn('distance', array(
            'header' => __('Distance (km)'),
            'index' => 'distance'
        ));
        $this->addColumn('note', array(
            'header' => __('Note'),
            'width' => '300px',
            'index' => 'note'
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('profile/vendoredittab/deliveryGrid', array('_current' => true));
    }

}
