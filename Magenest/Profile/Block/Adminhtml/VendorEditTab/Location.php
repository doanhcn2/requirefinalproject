<?php

namespace Magenest\Profile\Block\Adminhtml\VendorEditTab;

use \Magento\Backend\Block\Template\Context;
use \Magento\Backend\Helper\Data as HelperData;
use \Magento\Framework\Registry;
use \Unirgy\Dropship\Helper\Data as DropshipHelperData;

class Location extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * @var DropshipHelperData
     */
    protected $_hlp;

    public function __construct(
        Registry $registry,
        DropshipHelperData $helperData,
        Context $context,
        HelperData $backendHelper,
        array $data = []
    )
    {
        $this->_registry = $registry;
        $this->_hlp = $helperData;

        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->setId('vendor_location');
        $this->setUseAjax(true);
    }

    public function getVendor()
    {
        $vendor = $this->_registry->registry('vendor_data');
        if (!$vendor) {
            $vendor = $this->_hlp->createObj('\Unirgy\Dropship\Model\Vendor')->load($this->getVendorId());
        }
        return $vendor;
    }

    protected function _prepareCollection()
    {
        $collection = $this->_hlp->createObj('\Magenest\Groupon\Model\Location')->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendor()->getData('vendor_id'));
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('location_location_id', array(
            'header' => __('Id'),
            'index' => 'location_id'
        ));
        $this->addColumn('location_location_name', array(
            'header' => __('Location Name'),
            'index' => 'location_name'
        ));
        $this->addColumn('location_street', array(
            'header' => __('Street'),
            'index' => 'street'
        ));
        $this->addColumn('location_street_two', array(
            'header' => __('Street 2'),
            'index' => 'street_two'
        ));
        $this->addColumn('location_city', array(
            'header' => __('City'),
            'index' => 'city'
        ));
        $this->addColumn('location_country', array(
            'header' => __('Country'),
            'index' => 'country'
        ));
        $this->addColumn('location_region', array(
            'header' => __('Region'),
            'index' => 'region'
        ));
        $this->addColumn('location_postcode', array(
            'header' => __('Postcode'),
            'index' => 'postcode'
        ));
        $this->addColumn('location_phone', array(
            'header' => __('Phone'),
            'index' => 'phone'
        ));
        $this->addColumn('location_description', array(
            'header' => __('Description'),
            'index' => 'description'
        ));
        $this->addColumn('location_status', array(
            'header' => __('Status'),
            'index' => 'status'
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('profile/vendoredittab/locationGrid', array('_current' => true));
    }

    /**
     * Return row url for js event handlers
     *
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('profile/location/edit', array('id' => $item->getData('location_id')));
    }

    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();//get the parent class buttons
        $addButton = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
            ->setData(array(
                'label' => 'Add new Location',
                'on_click' => sprintf("location.href = '%s';", $this->getCreateUrl()),
                'class' => 'action primary'
            ))->toHtml();
        return $addButton . $html;
    }

    public function getCreateUrl()
    {
        return $this->getUrl('profile/location/edit', ['vendor_id' => $this->getVendor()->getData('vendor_id')]);
    }
}
