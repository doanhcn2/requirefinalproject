<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipTierShipping
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\Profile\Block\Adminhtml\VendorEditTab\Payment;

use Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{
    protected function _prepareForm()
    {
        $vendor = $this->_coreRegistry->registry('vendor_data');
        $id = $this->getRequest()->getParam('id');
        $form = $this->_formFactory->create();
        $this->setForm($form);

        $fieldset = $form->addFieldset('payment_details', [
            'legend' => __('Payment Details')
        ]);
        $fieldset->addField('bank_name', 'text', [
            'name' => 'bank_name',
            'label' => __('Bank Name'),
        ]);
        $fieldset->addField('bank_swiftcode', 'text', [
            'name' => 'bank_swiftcode',
            'label' => __('Bank Swift Code'),
        ]);
        $fieldset->addField('bank_iban', 'text', [
            'name' => 'bank_iban',
            'label' => __('Bank IBAN'),
        ]);
        $fieldset->addField('bank_account_name', 'text', [
            'name' => 'bank_account_name',
            'label' => __('Bank Account Name'),
        ]);
        $fieldset->addField('legal_business_name', 'text', [
            'name' => 'legal_business_name',
            'label' => __('Legal Business Name'),
        ]);
        $fieldset->addField('tax_number', 'text', [
            'name' => 'tax_number',
            'label' => __('Tax Number'),
        ]);

        if ($vendor) {
            $form->setValues($vendor->getData());
        }
        return parent::_prepareForm();
    }
}
