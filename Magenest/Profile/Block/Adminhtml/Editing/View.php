<?php

namespace Magenest\Profile\Block\Adminhtml\Editing;


class View extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $vendorFactory;
    protected $editingFactory;
    protected $documentHlp;
    protected $edittingModel;

    public function __construct(
        \Magenest\Profile\Helper\Document $document,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Magento\Backend\Block\Widget\Context $context, array $data = [])
    {
        $this->documentHlp = $document;
        $this->vendorFactory = $vendorFactory;
        $this->editingFactory = $editingFactory;
        parent::__construct($context, $data);
    }
    protected function _construct()
    {
        $this->_objectId = 'row_id';
        $this->_blockGroup = 'Magenest_Profile';
        $this->_controller = 'adminhtml_editing';
        parent::_construct();
        if ($this->_isAllowedAction('Magenest_Profile::editing')) {
            $this->buttonList->add('approve', [
                'label' => __('Approve'),
                'onclick' => "location.href = '{$this->getUrl('*/*/approve',['id'=>$this->getRequest()->getParam('id')])}'",
                'class' => 'primary',
            ], 0,3);
            $this->buttonList->add('reject', [
                'label' => __('Reject'),
                'onclick' => "location.href = '{$this->getUrl('*/*/reject',['id'=>$this->getRequest()->getParam('id')])}'",
                'class' => 'secondary',
            ], 0,2);
        }
        $this->buttonList->remove('save');
        $this->buttonList->remove('reset');
    }

    /**
     * Retrieve text for header element depending on loaded image.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Review Vendor details editing');
    }

    /**
     * Check permission for passed action.
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getVendor(){
        return $this->vendorFactory->create()->load($this->getEditing()['vendor_id'])->getData();
    }

    /**
     * @return \Magenest\Profile\Model\Editing
     */
    private function getEdittingModel() {
        if (!isset($this->edittingModel)) {
            $this->edittingModel = $this->editingFactory->create()->load($this->getRequest()->getParam('id'));
        }
        return $this->edittingModel;
    }

    public function getEditing(){
        return $this->getEdittingModel()->getData();
    }

    public function getLabel($code){
        return str_replace('_',' ',$code);
    }

    public function getDocument($name, $pending = false) {
        if (!$this->documentHlp->getVendorId()) {
            $this->documentHlp->setVendorId($this->getEdittingModel()->getVendorId());
        }
        return $this->documentHlp->getDocumentUrl($name, $pending);
    }
}
