<?php

namespace Magenest\Profile\Block\Location;

use \Magento\Framework\View\Element\Template;

class Edit extends Template
{
    protected $locationFactory;

    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        Template\Context $context, array $data = [])
    {
        $this->locationFactory = $locationFactory;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getLocation($id)
    {
        return $this->locationFactory->create()->load($id);
    }

    public function getWeekDay($number)
    {
        switch ($number) {
            case 1:
                return 'sunday';
            case 2:
                return 'monday';
            case 3:
                return 'tuesday';
            case 4:
                return 'wednesday';
            case 5:
                return 'thursday';
            case 6:
                return 'friday';
            case 7:
                return 'saturday';
            default:
                return false;
        }
    }
}