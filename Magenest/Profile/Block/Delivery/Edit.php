<?php

namespace Magenest\Profile\Block\Delivery;

use \Magento\Framework\View\Element\Template;

class Edit extends Template
{
    protected $deliveryFactory;
    protected $deliveryId;

    public function __construct(
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        Template\Context $context, array $data = [])
    {
        $this->deliveryFactory = $deliveryFactory;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getDelivery()
    {
        return $this->deliveryFactory->create()->load($this->deliveryId);
    }

    public function setDeliveryId($id)
    {
        $this->deliveryId = $id;
    }
}