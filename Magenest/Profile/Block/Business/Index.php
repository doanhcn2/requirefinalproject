<?php

namespace Magenest\Profile\Block\Business;

use Magento\Framework\App\ObjectManager;
use \Magento\Framework\View\Element\Template;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magenest\Profile\Model\ImageUploadFactory;
use \Magento\Framework\App\Filesystem\DirectoryList;

class Index extends Template
{
    protected $locationFactory;
    protected $deliveryFactory;
    protected $productRepository;
    protected $imagesLoader;
    protected $fileSystem;

    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        ProductRepositoryInterface $productRepository,
        ImageUploadFactory $imagesLoader,
        \Magento\Framework\Filesystem $fileSystem,
        Template\Context $context, array $data = []
    ) {
        $this->locationFactory = $locationFactory;
        $this->deliveryFactory = $deliveryFactory;
        $this->productRepository = $productRepository;
        $this->imagesLoader = $imagesLoader;
        $this->fileSystem = $fileSystem;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getVendor()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getVendorPromoteProduct()
    {
        $promote = $this->getVendor()->getPromoteProduct();
        return (int)$promote;
    }

    public function getCampaignImages($dealCollection)
    {
        $imagehelper = ObjectManager::getInstance()->create('Magento\Catalog\Helper\Image');
        $images = [];
        foreach($dealCollection as $deal) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productRepository->getById($deal->getProductId());
//            $children = $product->getTypeInstance()->getUsedProducts($product);
//            foreach ($children as $child) {
//                $images[] = $imagehelper->init($child,'category_page_list')
//                    ->constrainOnly(FALSE)
//                    ->keepAspectRatio(TRUE)
//                    ->keepFrame(FALSE)
//                    ->getUrl();
//            }
            $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $imageItems = $product->getMediaGallery()['images'];
            foreach($imageItems as $item) {
                $fileName = $item['file'];
                $url = $baseMediaUrl . 'catalog/product' . $fileName;
                $images[] = $url;
            }
        }
        return $images;
    }

    public function getUploadedImages()
    {
        $images = $this->imagesLoader->create()->getCollection()
            ->AddFieldToFilter('vendor_id', $this->getVendorId());
        $imageURLs = [];
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        foreach($images as $image) {
            $imageURLs[] = $baseMediaUrl . 'vendor/' . $image->getImagePath();
        }
        return $imageURLs;
//        $result['url'] = $baseMediaUrl . __(self::IMG_PATH, $this->getVendorId()) . $result['file'];

    }

    public function getLocation()
    {
        $collection = $this->locationFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->setOrder('sort_order', 'ASC');
        $firstItem = $collection->getFirstItem();
        $i = 1;
        if ($firstItem->getData('sort_order') === null)
            foreach($collection as $item) {
                $item->setData('sort_order', $i++);
                $item->save();
            }
        return $collection;
    }

    public function getLocationById($id)
    {
        $model = $this->locationFactory->create()->load($id);
        return $model->getId() == null ? $this->locationFactory->create()->getCollection()->getFirstItem() : $model;
    }

    public function getDelivery()
    {
        $collection = $this->deliveryFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->setOrder('sort_order', 'ASC');
        $firstItem = $collection->getFirstItem();
        $i = 1;
        if ($firstItem->getData('sort_order') === null)
            foreach($collection as $item) {
                $item->setData('sort_order', $i++);
                $item->save();
            }
        return $collection;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getUploadedDocuments()
    {
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $documentsUrl = $baseMediaUrl . 'upload/vendor_' . $this->getVendorId() . '/documents/';
        $directory = $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath('upload/vendor_' . $this->getVendorId() . '/documents/');
        $document = [];
        try {
            $files = array_diff(scandir($directory), array('..', '.'));
            foreach($files as $file) {
                $fileName = pathinfo($file, PATHINFO_FILENAME);
                $document[$fileName] = $documentsUrl . $file;
            }
        } catch (\Exception $e) {

        }
        return $document;
    }
}
