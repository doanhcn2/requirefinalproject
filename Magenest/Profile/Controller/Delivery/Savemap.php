<?php

namespace Magenest\Profile\Controller\Delivery;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Savemap extends AbstractVendor
{
    protected $jsonFactory;
    protected $deliveryFactory;

    public function __construct(
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        $this->deliveryFactory = $deliveryFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $mapInfo = $this->getRequest()->getParam('map_info');
        $id = $this->getRequest()->getParam('delivery_id');
        $collection = $this->deliveryFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->setOrder('sort_order', 'DESC');
        $firstItem = $collection->getFirstItem();
        $so = $firstItem->getData('sort_order');
        $delivery = $this->deliveryFactory->create()->load($id);
        if ($this->getVendorId())
            $delivery->setData('vendor_id', $this->getVendorId());
        $delivery->setData('map_info', @$mapInfo['lat'] . ',' . @$mapInfo['lng']);
        $delivery->setData('address', @$this->getRequest()->getParam('address'));
        $delivery->setData('distance', @$this->getRequest()->getParam('distance'));
        $delivery->setData('note', @$this->getRequest()->getParam('note'));
        $delivery->setData('sort_order', (int)$so + 1);
        $delivery->save();
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}