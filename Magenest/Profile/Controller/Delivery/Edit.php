<?php
namespace Magenest\Profile\Controller\Delivery;

use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Edit extends AbstractVendor
{
    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Profile\Block\Delivery\Edit $editBlock */
        $editBlock = $view->getLayout()->getBlock('edit');
        $editBlock->setDeliveryId($this->getRequest()->getParam('delivery_id'));
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($editBlock->toHtml());
    }
}