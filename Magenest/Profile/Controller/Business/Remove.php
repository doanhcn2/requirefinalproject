<?php

namespace Magenest\Profile\Controller\Business;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Profile\Model\ImageUploadFactory;

class Remove extends \Magento\Framework\App\Action\Action
{
    protected $_storeManager;
    protected $_file;
    const IMG_PATH = 'vendor/%1/images';
    protected $imageUpload;

    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem\Driver\File $file,
        ImageUploadFactory $imageUploadFactory
    )
    {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_file = $file;
        $this->imageUpload = $imageUploadFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $url = $this->getRequest()->getPostValue()['url'];
            $fileName = str_replace($baseMediaUrl, "", $url);
            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);

            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $mediaRootDir = $mediaDirectory->getAbsolutePath();


            if ($this->_file->isExists($mediaRootDir . $fileName))  {
                $image = getimagesize($mediaRootDir . $fileName) ? true : false;

                if ($image){
                    $imagesLoader = $this->imageUpload->create()->getCollection()
                        ->addFieldToFilter('absolute_path', $mediaRootDir . $fileName)
                        ->getFirstItem();
                    $imagesLoader->delete();
                    $this->_file->deleteFile($mediaRootDir . $fileName);
                    $result = ['success' => 'Image Deleted', 'error' => 0];
                }else {
                    $result = ['error' => 'File is not an image'];
                }
            } else {
                $result = ['error' => 'File is not existed'];
            }

        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
