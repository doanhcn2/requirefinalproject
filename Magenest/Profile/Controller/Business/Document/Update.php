<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 28/08/2018
 * Time: 11:13
 */

namespace Magenest\Profile\Controller\Business\Document;

use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class Update
 * @package Magenest\Profile\Controller\Business\Document
 */
class Update extends \Magenest\Profile\Controller\Business\SaveDocuments
{

    public function execute()
    {
        $result   = parent::execute();
        $editing = $this->editingFactory->create()->load($this->getVendorId(), 'vendor_id');
        if (!$editing->getId()) {
            $editing->setVendorId($this->getVendorId())->save();
        }
        return $result;
    }

    protected function getDestinationPath()
    {
        return parent::getDestinationPath() . 'pending/';
    }
}