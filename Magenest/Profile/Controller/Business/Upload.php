<?php

namespace Magenest\Profile\Controller\Business;

use Magenest\Profile\Model\ImageUpload;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Profile\Model\ImageUploadFactory;

class Upload extends \Magento\Framework\App\Action\Action
{
    protected $_storeManager;
    const IMG_PATH = 'vendor/%1/images';
    protected $imageUpload;

    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ImageUploadFactory $imageUploadFactory
    )
    {
        parent::__construct($context);
        $this->imageUpload = $imageUploadFactory;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'image']
            );
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
            $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
            $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);
            /** @var \Magento\Catalog\Model\Product\Media\Config $config */
            $result = $uploader->save($mediaDirectory->getAbsolutePath(__(self::IMG_PATH, $this->getVendorId())));

            $this->_eventManager->dispatch(
                'catalog_product_gallery_upload_image_after',
                ['result' => $result, 'action' => $this]
            );

            $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $result['url'] = $baseMediaUrl . __(self::IMG_PATH, $this->getVendorId()) . $result['file'];

            $imageUpload = $this->imageUpload->create();
            $data = [
                'vendor_id' => $this->getVendorId(),
                'image_path' => $this->getVendorId().'/images'.$result['file'],
                'absolute_path' => $result['path'].$result['file']
            ];
            $imageUpload->setData($data);
            $imageUpload->save();

            unset($result['tmp_name']);
            unset($result['path']);

        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
