<?php

namespace Magenest\Profile\Controller\Business;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Save extends AbstractVendor
{
    protected $jsonFactory;
    protected $vendorFactory;
    protected $editingFactory;
    protected $messageManager;
    protected $unSaveableField = [
        'business_category',
        'website',
        'vendor_name',
        'business_des',
        'bank_name',
        'bank_swiftcode',
        'bank_iban',
        'bank_account_name',
        'legal_business_name',
        'tax_number',
        'primary_location_id'
    ];

    /**
     * @var $_vendorSession \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        ManagerInterface $manager,
        Context $context, ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        StoreManagerInterface $storeManager,
        LayoutFactory $viewLayoutFactory,
        Registry $registry,
        ForwardFactory $resultForwardFactory,
        \Unirgy\Dropship\Model\Session $vendorSession,
        HelperData $helper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\HTTP\Header $httpHeader
    ) {
        $this->_vendorSession = $vendorSession;
        $this->messageManager = $manager;
        $this->editingFactory = $editingFactory;
        $this->vendorFactory = $vendorFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (!@$this->getRequest()->getParam('finish_onboarding')) {
            if ($this->getVendorId()) {
                $vendor = $this->editingFactory->create()->load($this->getVendorId(), 'vendor_id');
                $data['vendor_id'] = $this->getVendorId();
                $vendor->addData($data);
                $vendor->save();
                $this->saveOtherData($data);
            }
        } else {
            if ($this->getVendorId()) {
                $vendor = $this->vendorFactory->create()->load($this->getVendorId(), 'vendor_id');
                $vendor->addData($data);
                if (!$vendor->getUrlKey()) {
                    $vendor->setUrlKey($this->formatUrlKey($vendor->getVendorName()));
                }
                $vendor->setAllowUdratings(1);
                $vendor->save();
                if ($vendor->getData('new_reg') === "1") {
                    $this->_redirect('udropship/vendor/logout');
                    $this->_vendorSession->setRegistedVendor(true);
                } else {
                    $this->_redirect('overview');
                }
            }
        }
    }

    public function formatUrlKey($str)
    {
        /* @var \Magento\Framework\Filter\FilterManager $filter */
        $filter = $this->_hlp->getObj('\Magento\Framework\Filter\FilterManager');
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $filter->translitUrl($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function saveOtherData($data)
    {
        foreach($data as $key => $val) {
            if (in_array($key, $this->unSaveableField))
                unset($data[$key]);
        }

        $vendor = $this->vendorFactory->create()->load($this->getVendorId(), 'vendor_id');
        $vendor->addData($data);
        $vendor->save();
    }
}