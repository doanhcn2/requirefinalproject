<?php
namespace Magenest\Profile\Controller\Business;

use Magento\Backend\App\Action;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Index extends AbstractVendor
{
    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_renderPage(null, 'magenest_profile');
    }
}