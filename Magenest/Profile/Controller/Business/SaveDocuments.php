<?php

namespace Magenest\Profile\Controller\Business;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;
use Magento\Framework\Message\ManagerInterface;

class SaveDocuments extends AbstractVendor
{
    protected $jsonFactory;
    protected $vendorFactory;
    protected $editingFactory;
    protected $fileSystem;
    protected $messageManager;
    const DOC_PATH = 'vendor/%1/docs';
    protected $_fileUploaderFactory;

    protected $unSaveableField = [
        'business_category',
        'website',
        'vendor_name',
        'business_des',
        'bank_name',
        'bank_swiftcode',
        'bank_iban',
        'bank_account_name',
        'legal_business_name',
        'tax_number',
        'primary_location_id'
    ];
//    protected $allowedExtensions = [];

    protected $allowedExtensions = ['jpg', 'jpeg', 'png', 'doc', 'txt', 'docx', 'pdf'];

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ManagerInterface $manager,
        Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        $this->messageManager = $manager;
        $this->editingFactory = $editingFactory;
        $this->vendorFactory = $vendorFactory;
        $this->fileSystem = $fileSystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $result = [
            'success' => false,
            'error' => true,
            'msg' => __("Something went wrong. Please try again.")
        ];
        try {
            $data = (array)$this->getRequest()->getFiles();
            $i = 0;
            foreach ($data as $fileId => $file) {
                if (isset($file['name']) && (file_exists($file['tmp_name']))) {
                    $destinationPath = $this->getDestinationPath();
                    $filename = strip_tags($file['name']);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
                    $uploader = $this->_fileUploaderFactory
                        ->create(['fileId' => $fileId])
                        ->setAllowCreateFolders(true)
                        ->setAllowedExtensions($this->allowedExtensions);
                    $newName = empty($extension) ? $fileId : $fileId . "." . $extension;
                    $uploader->save($destinationPath, $newName);
                    $i++;
                }
            }
            if ($i === 0) {
                throw new LocalizedException(__("Error while uploading file."));
            }
            $result = [
                'success' => true,
                'error' => false,
                'msg' => __("%1 file(s) was submitted.", $i)
            ];
        } catch (\Exception $exception) {
            if ($exception->getMessage() === "Disallowed file type.") {
                $msg = "";
                $msg .= $exception->getMessage();
                $msg .= " Allowed ";
                foreach($this->allowedExtensions as $item) {
                    $msg .= $item . " ";
                }
                if ($msg) {
                    $result['msg'] = $msg;
                }
            } else {
                $result['msg'] = $exception->getMessage();
            }
        }
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function saveOtherData($data)
    {
        foreach ($data as $key => $val) {
            if (in_array($key, $this->unSaveableField))
                unset($data[$key]);
        }

        $vendor = $this->vendorFactory->create()->load($this->getVendorId(), 'vendor_id');
        $vendor->addData($data);
        $vendor->save();
    }

    protected function getDestinationPath()
    {
        return $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath('upload/vendor_' . $this->getVendorId() . '/documents/');
    }

    /**
     * @param String $message
     */
    protected function returnError($message)
    {
        $this->messageManager->addSuccessMessage(__('Product has been saved'));
        $this->messageManager->addErrorMessage($message);
        return $this->_redirect('register/vendor/registerfinal');
    }
}