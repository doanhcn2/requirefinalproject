<?php
/**
 * Created by Magenest.
 * Author: Pham Quang Hau
 * Date: 12/08/2016
 * Time: 16:41
 */

namespace Magenest\Profile\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use Psr\Log\LoggerInterface;

abstract class Editing extends Action
{
    protected $_pageFactory;

    protected $_logger;

    protected $_coreRegistry;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        LoggerInterface $loggerInterface,
        Registry $registry
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_logger = $loggerInterface;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Profile::editing')
            ->addBreadcrumb(__('Manage Vendor Details Editing'), __('Manage Vendor Details Editing'));

        $resultPage->getConfig()->getTitle()->set(__('Manage Vendor Details Editing'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Profile::editing');
    }

    public function getResultPage()
    {
        if (empty($this->_resultPage)) {
            $this->_resultPage = $this->_pageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Profile::editing');
        $resultPage->getConfig()->getTitle()->prepend((__('Manage Vendor Details Editing')));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Manage Vendor Details Editing'), __('Manage Vendor Details Editing'));

        return $this;
    }
}
