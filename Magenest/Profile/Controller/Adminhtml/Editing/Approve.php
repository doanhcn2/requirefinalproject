<?php

namespace Magenest\Profile\Controller\Adminhtml\Editing;

use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;

class Approve extends \Magento\Backend\App\Action
{
    protected $editingFactory;
    protected $vendorFactory;

    /**
     * @var $__cacheTypeList TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var Pool $_cacheFrontendPool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magenest\Profile\Helper\Document
     */
    protected $_documentHlp;

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Profile\Helper\Document $document,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool,
        Action\Context $context)
    {
        $this->_documentHlp = $document;
        $this->editingFactory = $editingFactory;
        $this->vendorFactory = $vendorFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $editingModel = $this->editingFactory->create()->load($id);
                $editingData = $editingModel->getData();
                $vendorModel = $this->vendorFactory->create()->load($editingModel->getData('vendor_id'));
                unset($editingData['id']);
                unset($editingData['primary_location_id']);
                unset($editingData['vendor_id']);
                $vendorModel->addData(array_filter($editingData));
                $this->_documentHlp->setVendorId($editingModel->getData('vendor_id'))->approve();
                $vendorModel->save();
                $editingModel->delete();
                $this->messageManager->addSuccess(__('Apply successful'));
                $this->_cacheTypeList->cleanType('full_page');
                return $this->_redirect('*/*');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $this->_redirect('*/*/view', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->messageManager->addError(__('We can\'t find record to apply.'));
        return $this->_redirect('*/*/');
    }
}