<?php

namespace Magenest\Profile\Controller\Adminhtml\Editing;

use Magento\Backend\App\Action;

class Reject extends \Magento\Backend\App\Action
{
    protected $editingFactory;

    protected $documentHlp;

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Magenest\Profile\Helper\Document $document,
        Action\Context $context)
    {
        $this->editingFactory = $editingFactory;
        $this->documentHlp = $document;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->editingFactory->create();
                $model->load($id);
                $this->documentHlp->setVendorId($model->getVendorId())->reject();
                $model->delete();
                $this->messageManager->addSuccess(__('Rejected successful'));
                return $this->_redirect('*/*');
            } catch (\Exception $e) {
                $this->messageManager->addError(
                    __('Something went wrong. Please try again later.')
                );
                return $this->_redirect('*/*/view', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->messageManager->addError(__('We can\'t find record to cancel.'));
        return $this->_redirect('*/*/');
    }
}