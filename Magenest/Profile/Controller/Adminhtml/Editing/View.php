<?php

namespace Magenest\Profile\Controller\Adminhtml\Editing;


use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

class View extends \Magenest\Profile\Controller\Adminhtml\Editing
{
    protected $editingFactory;

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        Action\Context $context, PageFactory $pageFactory, LoggerInterface $loggerInterface, Registry $registry)
    {
        $this->editingFactory = $editingFactory;
        parent::__construct($context, $pageFactory, $loggerInterface, $registry);
    }

    public function execute()
    {
        $rowId = (int)$this->getRequest()->getParam('id');
        if ($rowId) {
            $rowData = $this->editingFactory->create();
            $rowData = $rowData->load($rowId);
            if (!$rowData->getId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                return $this->_redirect('*/*/index');
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->_setPageData();
        $title =  __('Review Vendor details editing');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
}
