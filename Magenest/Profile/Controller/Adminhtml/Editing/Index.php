<?php

namespace Magenest\Profile\Controller\Adminhtml\Editing;

class Index extends \Magenest\Profile\Controller\Adminhtml\Editing
{
    public function execute()
    {
        //Call page factory to render layout and page content
        $this->_setPageData();
        return $this->getResultPage();
    }
}
