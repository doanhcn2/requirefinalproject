<?php
namespace Magenest\Profile\Controller\Adminhtml\Editing;


use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;

class MassReject extends \Magento\Backend\App\Action
{
    protected $editingFactory;

    protected $documentHlp;

    public function __construct(
        Context $context,
        \Magenest\Profile\Helper\Document $document,
        \Magenest\Profile\Model\EditingFactory $editingFactory
    )
    {
        $this->editingFactory = $editingFactory;
        $this->documentHlp    = $document;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('selected');
        if ($ids) {
            $ids = implode(', ', $ids);
            $collection = $this->editingFactory->create()->getCollection()->addFieldToFilter('id', array('in' => $ids));
        } else
            $collection = $this->editingFactory->create()->getCollection();
        $collectionSize = count($collection);
        foreach ($collection as $item) {
            $this->documentHlp->setVendorId($item->getVendorId())->reject();
            $item->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been rejected.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}