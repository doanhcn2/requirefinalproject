<?php

namespace Magenest\Profile\Controller\Adminhtml\Editing;


use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;

class MassApprove extends \Magento\Backend\App\Action
{
    protected $editingFactory;
    protected $vendorFactory;

    /**
     * @var \Magenest\Profile\Helper\Document
     */
    protected $_documentHlp;

    public function __construct(
        \Magenest\Profile\Model\EditingFactory $editingFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Profile\Helper\Document $document,
        Context $context
    )
    {
        $this->_documentHlp = $document;
        $this->vendorFactory = $vendorFactory;
        $this->editingFactory = $editingFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('selected');
        if ($ids) {
            $ids = implode(', ', $ids);
            $collection = $this->editingFactory->create()->getCollection()->addFieldToFilter('id', array('in' => $ids));
        } else
            $collection = $this->editingFactory->create()->getCollection();
        $collectionSize = count($collection);
        foreach ($collection as $item) {
            $editingData = [];
            $editingData = $item->getData();
            unset($editingData['id']);
            unset($editingData['primary_location_id']);
            unset($editingData['vendor_id']);
            $vendorModel = $this->vendorFactory->create()->load($item->getData('vendor_id'));
            $vendorModel->addData(array_filter($editingData));
            $this->_documentHlp->setVendorId($item->getData('vendor_id'))->scanDocuments(true)->approve();
            $vendorModel->save();
            $item->delete();
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been approved.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}