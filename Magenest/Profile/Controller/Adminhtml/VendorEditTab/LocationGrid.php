<?php
namespace Magenest\Profile\Controller\Adminhtml\VendorEditTab;

class LocationGrid extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()
            ->getBlock('magenest.vendor.location')
            ->setVendorId($this->getRequest()->getParam('id'));
        $this->_view->renderLayout();
    }
}
