<?php
/**
 * Hello Admin Hello helloGrid Controller
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Magenest\Profile\Controller\Adminhtml\VendorEditTab;

class DeliveryGrid extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getLayout()
            ->getBlock('magenest.vendor.delivery')
            ->setVendorId($this->getRequest()->getParam('id'));
        $this->_view->renderLayout();
    }
}
