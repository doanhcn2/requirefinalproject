<?php

namespace Magenest\Profile\Controller\Adminhtml\Location;

use Magento\Framework\Controller\ResultFactory;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        $vendorId = $this->getRequest()->getParam('vendor_id');
        if (empty($id) && empty($vendorId))
            return $resultRedirect->setPath('admin');

        $model = $this->_objectManager->create('\Magenest\Groupon\Model\Location')->load($id);

        if ($model->getId()) {
            $title = "Edit Location: " . $model->getData('location_name');
            $title .= ' - Vendor ID: ' . $model->getData('vendor_id');

        } else {
            $title = 'New Location';
            if ($vendorId)
                $title .= ' - Vendor ID: ' . $vendorId;
        }
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
}