<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\Location;


use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;

class Save extends Action
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $post['map_info'] = $post['lat'] . ',' . $post['long'];
            $model = $this->_objectManager->create('Magenest\Groupon\Model\Location')->load(@$post['location_id']);
            unset($post['location_id']);
            $model->addData($post);
            $model->save();
            $this->messageManager->addSuccessMessage(__('The location has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the location.'));
        }
        return $resultRedirect->setPath('*/*/edit',['id'=> $model->getData('location_id')]);
    }
}
