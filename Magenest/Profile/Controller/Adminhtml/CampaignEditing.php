<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

abstract class CampaignEditing extends Action
{
    protected $_pageFactory;

    protected $_logger;

    protected $_coreRegistry;

    protected $_resultPage;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        LoggerInterface $loggerInterface,
        Registry $registry
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_logger = $loggerInterface;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Profile::campaign_editing')
            ->addBreadcrumb(__('Manage Vendor Campaigns Editing'), __('Manage Vendor Campaigns Editing'));

        $resultPage->getConfig()->getTitle()->set(__('Manage Vendor Campaign Editing'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Profile::campaign_editing');
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Profile::campaign_editing');
        $resultPage->getConfig()->getTitle()->prepend((__('Manage Vendor Campaigns Editing')));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Manage Vendor Campaigns Editing'), __('Manage Vendor Campaigns Editing'));

        return $this;
    }

    public function getResultPage()
    {
        if (empty($this->_resultPage)) {
            $this->_resultPage = $this->_pageFactory->create();
        }
        return $this->_resultPage;
    }
}
