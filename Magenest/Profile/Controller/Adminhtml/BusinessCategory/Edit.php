<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 19/04/2018
 * Time: 09:10
 */

namespace Magenest\Profile\Controller\Adminhtml\BusinessCategory;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 * @package Magenest\Profile\Controller\Adminhtml\BusinessCategory
 */
class Edit extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $registry,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->_registry = $registry;
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // TODO: Implement execute() method
        $id = $this->getRequest()->getParam('id');
        if (isset($id)) $this->_registry->register('businessCategory', $id);

        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Profile::bussiness_category');
        $resultPage->addBreadcrumb(__('Business Category Details'), __('Business Category Details'));
        $resultPage->getConfig()->getTitle()->prepend(__('Business Category Details'));

        return $resultPage;
    }
}
