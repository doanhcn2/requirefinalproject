<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\BusinessCategory;

use Magento\Backend\App\Action;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        // TODO: Implement execute() method
        $post_data = $this->getRequest()->getPostValue();
        $objectManager = ObjectManager::getInstance();
        $businessCategory = $objectManager->create('Magenest\Profile\Model\BusinessCategory');

        $businessCategory->setData($post_data);
        $businessCategory->save();
        $this->_redirect('profile/businesscategory/edit', ['id' => $businessCategory->getid()]);
    }
}
