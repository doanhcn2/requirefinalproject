<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 19/04/2018
 * Time: 09:10
 */
namespace Magenest\Profile\Controller\Adminhtml\BusinessCategory;

use Magento\Backend\App\Action;

/**
 * Class NewAction
 * @package Magenest\Profile\Controller\Adminhtml\BusinessCategory
 */
class NewAction extends Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * NewAction constructor.
     * @param Action\Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
    }
    public function execute()
    {
        // TODO: Implement execute() method
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');

    }
}
