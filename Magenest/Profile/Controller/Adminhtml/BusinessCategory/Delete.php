<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 21/04/2018
 * Time: 16:05
 */
namespace Magenest\Profile\Controller\Adminhtml\BusinessCategory;

use Magento\Backend\App\Action;
use Magento\Framework\App\ObjectManager;

/**
 * Class Delete
 * @package Magenest\Profile\Controller\Adminhtml\BusinessCategory
 */
class Delete extends Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->_request->getParam('id');
        $objectManager = ObjectManager::getInstance();
        $businessCategory = $objectManager->create('Magenest\Profile\Model\BusinessCategory');
        $businessCategory->load($id);
        $businessCategory->delete();
        $this->_redirect('profile/businesscategory/grid');

    }
}
