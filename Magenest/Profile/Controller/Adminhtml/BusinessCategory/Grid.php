<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 19/04/2018
 * Time: 09:10
 */
namespace Magenest\Profile\Controller\Adminhtml\BusinessCategory;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Grid
 * @package Magenest\Profile\Controller\Adminhtml\BusinessCategory
 */
class Grid extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Grid constructor.
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Profile::bussiness_category');
        $resultPage->addBreadcrumb(__('Business Category'), __('Business Category'));
        $resultPage->getConfig()->getTitle()->prepend(__('Business Category'));

        return $resultPage;
    }
}
