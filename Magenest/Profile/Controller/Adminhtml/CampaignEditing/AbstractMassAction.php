<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 15/09/2018
 * Time: 12:46
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class AbstractMassAction
 * @package Magenest\Profile\Controller\Adminhtml\CampaignEditing
 */
abstract class AbstractMassAction extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing
{
    protected $campaignEdittingCollectionF;

    /**
     * AbstractMassAction constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Profile\Model\ResourceModel\CampaignEditing\CollectionFactory $CECollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Registry $registry,
        \Magenest\Profile\Model\ResourceModel\CampaignEditing\CollectionFactory $CECollectionFactory
    ) {
        $this->campaignEdittingCollectionF = $CECollectionFactory;
        parent::__construct($context, $pageFactory, $loggerInterface, $registry);
    }

    /**
     * @return \Magenest\Profile\Model\ResourceModel\CampaignEditing\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCollection()
    {
        /** @var \Magenest\Profile\Model\ResourceModel\CampaignEditing\Collection $collection */
        $collection = $this->campaignEdittingCollectionF->create();
        $selected   = $this->getRequest()->getParam('selected');
        $excluded   = $this->getRequest()->getParam('excluded');
        if (isset($selected) && is_array($selected)) {
            return $collection->addFieldToFilter($collection->getIdFieldName(), $selected);
        } elseif (isset($excluded)) {
            if ($excluded === 'false') {
                return $collection;
            } elseif (is_array($excluded)) {
                return $collection->addFieldToFilter($collection->getIdFieldName(), ['nin' => $excluded]);
            }
        }
        throw new LocalizedException(__("Invalid Request."));
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_execute();

        return $this->_redirect('*/*');
    }

    /**
     * @return void
     */
    abstract function _execute();
}