<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Unirgy\Dropship\Helper\Catalog;
use Unirgy\DropshipVendorProduct\Helper\Data as DropshipVendorProductHelperData;

class Approve extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing
{
    /**
     * @var $_editCampaignFactory \Magenest\Profile\Model\CampaignEditingFactory
     */
    protected $_editCampaignFactory;

    protected $categoryLinkManagement;

    /**
     * @var $_udProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_udProductFactory;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_dealModel \Magenest\Groupon\Model\Deal | \Magenest\Ticket\Model\Event
     */
    protected $_dealModel;

    /**
     * @var $_productModel \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_productModel \Magento\Catalog\Model\Product
     */
    protected $_productModel;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_deliveryFactory \Magenest\Groupon\Model\DeliveryFactory
     */
    protected $_deliveryFactory;

    /**
     * @var $_storeManagerInterface \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var $_prdHelper \Magenest\FixUnirgy\Helper\Data
     */
    protected $_prdHelper;

    /**
     * @var Catalog
     */
    protected $_helperCatalog;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var $_ticketsFactory \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketsFactory;

    /**
     * @var $_sessionFactory \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * @var $_eventLocationFactory \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $_eventLocationFactory;

    /**
     * @var DropshipVendorProductHelperData
     */
    protected $_prodHlp;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        LoggerInterface $loggerInterface,
        Registry $registry,
        \Magenest\Profile\Model\CampaignEditingFactory $campaignEditingFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\FixUnirgy\Helper\Data $prdHelperData,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        DropshipVendorProductHelperData $prodHlp,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $udProductFactory,
        Catalog $catalog
    ) {
        $this->_udProductFactory = $udProductFactory;
        $this->_prodHlp = $prodHlp;
        $this->_eventLocationFactory = $eventLocationFactory;
        $this->_eventFactory = $eventFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_sessionFactory = $sessionFactory;
        $this->_helperCatalog = $catalog;
        $this->_prdHelper = $prdHelperData;
        $this->_dealFactory = $dealFactory;
        $this->_deliveryFactory = $deliveryFactory;
        $this->_productFactory = $productFactory;
        $this->_locationFactory = $locationFactory;
        $this->_editCampaignFactory = $campaignEditingFactory;
        parent::__construct($context, $pageFactory, $loggerInterface, $registry);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $editId        = $this->getRequest()->getParam('id');
        $statusComment = $this->getRequest()->getParam('campaign_status_comment');

        try {
            $this->approve($editId, $statusComment);
            $this->messageManager->addSuccessMessage(__('Item ID %1 has been approved.', $editId));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage('Something went wrong, please try again later.');
        }

        return $this->_redirect('profile/campaignediting/index');
    }

    /**
     * @param $editId
     * @param string $statusComment
     *
     * @throws \Exception
     */
    public function approve($editId, $statusComment = '')
    {
        $editModel    = $this->_editCampaignFactory->create()->load($editId);
        $productId    = $editModel->getProductId();
        $productModel = $this->_productFactory->create()->load($productId);
        $this->setProductModel($productModel);
        $productType = $productModel->getTypeId();

        $newProductData = json_decode($editModel->getNewProductData(), true);
        $this->saveProductData($newProductData);
        if ($productType === 'configurable' && \Magenest\Groupon\Helper\Data::isDeal($productId)) {
            $dealModel    = $this->_dealFactory->create()->load($productId, 'product_id');
            $this->setDealModel($dealModel);

            $newLocationData = $editModel->getNewCouponLocation();
            $this->saveCampaignLocationData($newLocationData);

            $newCampaignData = json_decode($editModel->getNewCouponData(), true);
            $this->saveCampaignData($newCampaignData);

            $this->saveChangeQuickCreates($newProductData['product']["_cfg_attribute"]["quick_create"], $newCampaignData);

            $couponChanges = json_decode($editModel->getCouponDealChanges(), true);
            if (@$couponChanges['addCoupon'] && !empty($couponChanges['addCoupon']) && is_array($couponChanges['addCoupon'])) {
                $extensionAttributes = $productModel->getExtensionAttributes();
                $productLink         = $extensionAttributes->getConfigurableProductLinks();

                foreach ($couponChanges['addCoupon'] as $addSimplePid) {
                    $this->_helperCatalog->linkCfgSimple($productModel->getId(), $addSimplePid, true);
                    if (@$productLink && is_array($productLink)) {
                        $productLink[$addSimplePid] = $addSimplePid;
                    }
                    $this->_dealFactory->create()->load($addSimplePid, 'product_id')->setParentId($productId)->save();
                }
                $extensionAttributes->setConfigurableProductLinks($productLink);
                $productModel->setExtensionAttributes($extensionAttributes);

                $this->_prdHelper->_addCfgSimplesDescrDataApproval($productModel, false, $couponChanges['addCoupon'], 'udprod_cfg_simples_added');
            }

            if (@$couponChanges['delCoupon'] && !empty($couponChanges['delCoupon']) && is_array($couponChanges['delCoupon'])) {
                $delProd             = $this->_productFactory->create();
                $extensionAttributes = $productModel->getExtensionAttributes();
                $productLink         = $extensionAttributes->getConfigurableProductLinks();

                foreach ($couponChanges['delCoupon'] as $delSimplePid) {
                    $this->_helperCatalog->unlinkCfgSimple($productModel->getId(), $delSimplePid, true);
                    if (@$productLink && is_array($productLink)) {
                        unset($productLink[$delSimplePid]);
                    }
                    $delProd->load($delSimplePid)->delete();
                    $this->_dealFactory->create()->load($delSimplePid, 'product_id')->delete();
                }
                $extensionAttributes->setConfigurableProductLinks($productLink);
                $productModel->setExtensionAttributes($extensionAttributes);

                $this->_prdHelper->_addCfgSimplesDescrDataApproval($productModel, false, $couponChanges['delCoupon'], 'udprod_cfg_simples_removed');
            }
        } elseif ($productType === 'ticket') {
            $eventModel = $this->_eventFactory->create()->load($productId, 'product_id');
            $this->setDealModel($eventModel);
            $this->_dealModel->setEventName(@$newProductData['product']['name']);

            $newEventData = json_decode($editModel->getNewCouponData(), true);
            $this->saveNewEventData($newEventData);

            $changeTicketInfo = json_decode($editModel->getTicketInfoChange(), true);
            $this->saveTicketInfoChange($changeTicketInfo, $productId);

            $params = $newProductData;
            $params['event'] = $newEventData;
            $params['approval'] = 'approved';
            $params['event']['ticket_info'] = $changeTicketInfo;
            $params['product']['campaign_status'] = -1;
            $this->getRequest()->setParams($params);
        }

        $this->saveStatusComment($statusComment);

        try {
            $this->saveDealModel();
            $this->saveProductModel();
            $prod = $this->_productModel;
            $this->_prodHlp->reindexProduct($prod);
            $this->getCategoryLinkManagement()->assignProductToCategories(
                $prod->getSku(),
                $prod->getCategoryIds()
            );
            $this->resetApprovalModel();
            $editModel->delete();
        } catch (\Exception $e) {
            $this->_logger->critical('Approve Campaign: ' . $e->getMessage());
            throw $e;
        }
    }

    private function getCategoryLinkManagement()
    {
        if (null === $this->categoryLinkManagement) {
            $this->categoryLinkManagement = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Catalog\Api\CategoryLinkManagementInterface');
        }
        return $this->categoryLinkManagement;
    }

    protected function saveTicketInfoChange($tickets, $productId)
    {
        if (isset($tickets, $tickets[0], $tickets[0]['type']) && $tickets[0]['type'] !== $this->_dealModel->getTicketType()) {
            $newTicketType = $tickets[0]['type'];
            $this->_dealModel->setTicketType($newTicketType);
        }
        $oldTicketsId = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getAllIds();
        $oldSessionsId = $this->_sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $oldTicketsId])->getAllIds();
        $newSessionIds = $newTicketIds = [];
        foreach($tickets as $ticket) {
            if ($ticket['id']) {
                array_push($newTicketIds, $ticket['id']);
            }
            foreach($ticket['sessions'] as $session) {
                if ($session['id']) {
                    array_push($newSessionIds, $session['id']);
                }
            }
        }

        $deletedTickets = array_diff($oldTicketsId, $newTicketIds);
        $deletedSessions = array_diff($oldSessionsId, $newSessionIds);
        $this->deleteDeletedTicketIds($deletedTickets, $deletedSessions);

        foreach($tickets as $ticket) {
            $ticketsModel = $this->_ticketsFactory->create()->load($ticket['id']);
            $sessionData = $ticket['sessions'];
            unset($ticket['sessions']);
            $ticket['product_id'] = $productId;
            $ticketsModel->addData($ticket)->save();
            $this->saveSessionTicketEvent($sessionData, $ticketsModel->getId(), $productId);
        }
    }

    protected function saveSessionTicketEvent($sessionData, $ticketsId, $productId)
    {
        foreach($sessionData as $sessionDatum) {
            $sessionModel = $this->_sessionFactory->create()->load($sessionDatum['id']);
            $data['session_from'] = @$sessionDatum['session_start'];
            $data['session_to'] = @$sessionDatum['session_end'];
            $data['sell_from'] = @$sessionDatum['sale_start'];
            $data['sell_to'] = @$sessionDatum['sale_end'];
            $data['sale_price'] = $sessionDatum['salePrice'] === null || $sessionDatum['salePrice'] === "" ? null : @$sessionDatum['salePrice'];
            $data['original_value'] = @$sessionDatum['orig_value'];
            $data['qty'] = @$sessionDatum['qty'];
            $data['max_qty'] = $sessionDatum['max_qty'] === null || $sessionDatum['max_qty'] === "" ? null : (int)$sessionDatum['max_qty'];
            $data['hide_sold_out'] = @$sessionDatum['is_hide'] ? 1 : 0;
            $data['tickets_id'] = $ticketsId;
            $data['product_id'] = $productId;

            $sessionModel->addData($data)->save();
        }
    }

    protected function deleteDeletedTicketIds($deletedTickets, $deletedSessions)
    {
        $this->_sessionFactory->create()->getCollection()->addFieldToFilter('session_id', ['in' => $deletedSessions])->walk('delete');
        $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $deletedTickets])->walk('delete');
    }

    protected function saveNewEventData($eventData)
    {
        $data = [];
        if (isset($eventData['is_online'])) {
            $data['is_online'] = 1;
            $this->saveEventLocationData(@$eventData['location']);
        } else {
            $data['is_online'] = 0;
        }
        if (isset($eventData['use_map'])) {
            $data['use_map'] = 1;
        } else {
            $data['use_map'] = 0;
        }
        $data['age'] = @$eventData['age_suitability'];
        $data['refund_policy'] = @$eventData['refund_policy'];
        $data['event_type'] = @$eventData['event_type'];
        $data['show_up_time'] = @$eventData['time_show_up'];
        if (isset($eventData['show_up'])) {
            $data['show_up'] = 1;
        } else {
            $data['show_up'] = 0;
        }
        if (isset($eventData['can_purchase'])) {
            $data['can_purchase'] = 1;
        } else {
            $data['can_purchase'] = 0;
        }
        if (isset($eventData['must_print'])) {
            $data['must_print'] = 1;
        } else {
            $data['must_print'] = 0;
        }
        if (@$eventData['refund_policy'] === 'non_refund') {
            $data['refund_policy_day'] = null;
        } else {
            $data['refund_policy_day'] = (int)@$eventData['refund_day'];
        }

        if (isset($eventData['organizer_detail'])) {
            $data['public_organizer'] = 1;
        } else {
            $data['public_organizer'] = 0;
        }
        $data['organizer_name'] = @$eventData['organizer_name'];
        $data['organizer_description'] = @$eventData['organizer_description'];

        $this->_dealModel->addData($data);
    }

    protected function saveEventLocationData($locations)
    {
        $productId = $this->_productModel->getId();
        $locations['event_id'] = $this->_dealModel->getId();
        $locations['product_id'] = $productId;
        $this->_eventLocationFactory->create()->load($productId, 'product_id')->addData($locations)->save();
    }

    /**
     * @param $statusComment
     */
    private function saveStatusComment($statusComment)
    {
        $this->_productModel->setStatusComment($statusComment)
            ->setCampaignStatusChanged(true);
    }

    protected function saveProductData($productData)
    {
        $data = $productData['product'];
        $prod = $this->_productModel;
        $prod->setCategoryIds(@$data['category_ids']);
        $prod->setName(@$data['name']);
        $prod->setDescription(@$data['description']);
        $this->saveProductImages($data);
    }

    protected function saveChangeQuickCreates($cfgData, $campaignData)
    {
        if (@$cfgData && is_array($cfgData)) {
            foreach($cfgData as $key => $cfg) {
                if ($key === '$ROW') continue;
                if (!isset($cfg['simple_id']) || $cfg['simple_id'] === "") continue;
                if (@$cfg['udmulti']['stock_qty'] === "" || (float)$cfg['udmulti']['stock_qty'] > 9999.99) {
                    $qty = 9999.99;
                } else {
                    $qty = (float)$cfg['udmulti']['stock_qty'];
                }
                /** @var \Magento\Catalog\Model\Product $couponProduct */
                $couponProduct = $this->_productFactory->create()->load($cfg['simple_id']);

                /** @var \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry */
                $stockRegistry = $this->getStockRegistry();
                $stockItem = $stockRegistry->getStockItem($cfg['simple_id']);
                $stockItem->setQty($qty);
                $stockItem->save();

                $couponProduct->setName($cfg['name']);
                $couponProduct->addAttributeUpdate('name', $cfg['name'], 0);

                $couponProduct->setName($cfg['price']);
                $couponProduct->addAttributeUpdate('price', $cfg['price'], 0);

                $couponProduct->setName($cfg['special_price']);
                $couponProduct->addAttributeUpdate('special_price', $cfg['special_price'], 0);

                $couponDealModel = $this->_dealFactory->create()->load($cfg['simple_id'], 'product_id');
                $couponDealModel->setMinimumBuyersLimit(@$campaignData[$cfg['groupon_option']]['minimum_buyers_limit'])
                    ->setMaximumBuyersLimit(@$campaignData[$cfg['groupon_option']]['maximum_buyers_limit'])->save();

                $udCoupon = $this->_udProductFactory->create()->load($cfg['simple_id'], 'product_id');
                $udQty = @$cfg['udmulti']['stock_qty'] === "" ? null : $cfg['udmulti']['stock_qty'];
                $udCoupon->setStockQty($udQty)
                    ->setVendorPrice((float)@$cfg['price'])
                    ->setSpecialPrice((float)@$cfg['special_price'])->save();
            }
        }
    }

    private function getStockRegistry()
    {
        $stockRegistry = $this->_objectManager->get(\Magento\CatalogInventory\Api\StockRegistryInterface::class);
        if (!$stockRegistry) {
            $stockRegistry = $this->_objectManager->create(\Magento\CatalogInventory\Api\StockRegistryInterface::class);
        }

        return $stockRegistry;
    }

    private function saveProductImages($data)
    {
        if (!isset($data['media_gallery'], $data['media_gallery']['images']) || !is_array($data['media_gallery']['images'])) {
            return;
        }
        /** @var \Magento\Catalog\Model\Product\Gallery\Processor $galleryProcessor */
        $galleryProcessor = $this->_objectManager->get(\Magento\Catalog\Model\Product\Gallery\Processor::class);
        foreach ($data['media_gallery']['images'] as $image) {
            if (!!@$image['removed']) {
                $galleryProcessor->removeImage($this->_productModel, @$image['file']);
            } else if (!@$image['value_id']) {
                $this->_productModel->addImageToMediaGallery('/catalog/product' . $image['file'], ['image', 'thumbnail', 'small_image'], false, !!@$image['disabled']);
            } else {
                $galleryProcessor->updateImage($this->_productModel, @$image['file'], $image);
            }
        }
        foreach (['image', 'thumbnail', 'small_image'] as $attr) {
            $this->setImageData($attr, $data);
        }
    }

    /**
     * @param $attr
     * @param $data
     */
    private function setImageData($attr, $data)
    {
        if (@$data[$attr] && @$data[$attr] !== 'no_selection') {
            $this->_productModel->setData($attr, $data[$attr]);
        }
    }

    protected function saveCampaignLocationData($locationData)
    {
        $locations   = json_decode($locationData, true);
        $locationIds = [];
        if (@$locations && is_array($locations)) {
            foreach ($locations as $location) {
                if (@$location['location_id'] === "") {
                    unset($location['location_id']);
                    $location['vendor_id']  = $this->getVendorId();
                    $location['product_id'] = $this->getProductId();
                    $locationModel          = $this->_locationFactory->create();
                    $locationModel->addData($location);
                    $locationModel->save();
                    $locationId              = $locationModel->getId();
                    $location['location_id'] = $locationId;
                }
                if (@$location['used'] && $location['used'] === 'on') {
                    $locationIds[] = $location['location_id'];
                }
            }
        }

        $grouponLocations = implode(',', $locationIds);
        $this->_dealModel->setLocationIds($grouponLocations);
    }

    protected function saveCampaignData($newCampaignData)
    {
        $savedData                              = [];
        $savedData['term']                      = json_encode($this->beforeSaveTermsData(@$newCampaignData['terms']));
        $savedData['is_business_address']       = @$newCampaignData['is_business_address'];
        $savedData['customer_specify_location'] = @$newCampaignData['customer_specify_location'];
        $savedData['is_delivery_address']       = @$newCampaignData['is_delivery_address'];
        if (@$newCampaignData['is_delivery_address'] && $newCampaignData['is_delivery_address'] === '1') {
            $this->saveDeliveryAddress(@$newCampaignData['delivery_location']);
        }
        $savedData['redeemable_after'] = @$newCampaignData['redeemable_after'];
        $savedData['is_online_coupon'] = @$newCampaignData['is_online_coupon'];
        if (@$newCampaignData['is_online_coupon'] && $newCampaignData['is_online_coupon'] === '1') {
            $savedData['is_business_address'] = 0;
            $savedData['is_delivery_address'] = 0;
        }
        $savedData['groupon_expire'] = @$newCampaignData['groupon_expire'];
        $savedData['start_time']     = @$newCampaignData['start_time'];
        $savedData['end_time']       = @$newCampaignData['end_time'];

        $this->_dealModel->addData($savedData);
    }

    protected function saveDeliveryAddress($deliveries)
    {
        if (isset($deliveries) && is_array($deliveries)) {
            $deliveryData = [];
            foreach ($deliveries as $delivery) {
                if (@$delivery['delivery_id'] === "") {
                    unset($delivery['delivery_id']);
                    $delivery['vendor_id']   = $this->getVendorId();
                    $delivery['product_id']  = $this->getProductId();
                    $deliveryModel           = $this->_deliveryFactory->create()->addData($delivery)->save();
                    $delivery['delivery_id'] = $deliveryModel->getId();
                }
                array_push($deliveryData, @$delivery['delivery_id']);
            }

            $deliveryData = implode(',', $deliveryData);
            $this->_dealModel->setDeliveryIds($deliveryData);
        }
    }

    protected function beforeSaveTermsData($data)
    {
        $result = [];
        if (isset($data['reservation'])
            && count($data['reservation']) > 1
            && isset($data['reservation']['required'])
            && isset($data['reservation']['days'])
        ) {
            $result['reservation']['enable'] = 1;
            $result['reservation']['days']   = $data['reservation']['days'];
        } else {
            $result['reservation']['enable'] = 0;
        }
        if (isset($data['combine'])) {
            $result['combine'] = 1;
        } else {
            $result['combine'] = 0;
        }
        if (isset($data['age_limit']['needed'])
            && count($data['age_limit']) > 1
            && isset($data['age_limit']['age_number'])
        ) {
            $result['age_limit']['enable']     = 1;
            $result['age_limit']['age_number'] = $data['age_limit']['age_number'];
        } else {
            $result['age_limit']['enable'] = 0;
        }
        if (isset($data['sharing'])) {
            $result['sharing'] = 1;
        } else {
            $result['sharing'] = 0;
        }
        if (isset($data['vendor_specify'])) {
            $result['vendor_specify'] = ltrim($data['vendor_specify']);
        }

        return @$result;
    }

    protected function getVendorId()
    {
        return $this->_dealModel->getVendorId();
    }

    protected function getProductId()
    {
        return $this->_dealModel->getProductId();
    }

    private function setDealModel($dealModel)
    {
        $this->_dealModel = $dealModel;
    }

    private function setProductModel($productModel)
    {
        $this->_productModel = $productModel;
    }

    private function saveDealModel()
    {
        $this->_dealModel->save();
    }

    private function saveProductModel()
    {
        $this->_productModel->setStoreId(0);
        $this->_productModel->save();
        $this->_productModel->setStoreId(1);
        $this->_productModel->save();
    }

    private function resetApprovalModel()
    {
        $this->_productModel = null;
        $this->_dealModel    = null;
    }
}
