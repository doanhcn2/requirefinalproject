<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Reject extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing
{
    /**
     * @var $_editCampaignFactory \Magenest\Profile\Model\CampaignEditingFactory
     */
    protected $_editCampaignFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        LoggerInterface $loggerInterface,
        \Magenest\Profile\Model\CampaignEditingFactory $campaignEditingFactory,
        Registry $registry
    ) {
        $this->_editCampaignFactory = $campaignEditingFactory;
        parent::__construct($context, $pageFactory, $loggerInterface, $registry);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $counts = 0;
        try {
            /** @var \Magenest\Profile\Model\CampaignEditing $editItems */
            $editItems = $this->_editCampaignFactory->create()->load($id);
            $editItems->rejectChangeForCampaign();
            $counts++;
            $this->messageManager->addSuccessMessage(__("%1 change of campaigns was rejected.", $counts));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Something went wrong. Please try again later."));
        }

        return $this->_redirect('profile/campaignediting/index');
    }
}
