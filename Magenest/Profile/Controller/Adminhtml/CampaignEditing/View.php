<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

/**
 * Class View
 * @package Magenest\Profile\Controller\Adminhtml\CampaignEditing
 */
class View extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $title = __('Review Campaign Details Editing');
        $resultPage->getConfig()->getTitle()->prepend($title);
        //Call page factory to render layout and page content
        return $this->getResultPage();
    }
}
