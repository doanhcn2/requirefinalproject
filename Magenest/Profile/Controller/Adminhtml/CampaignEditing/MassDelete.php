<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 15/09/2018
 * Time: 12:34
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

/**
 * Class MassDelete
 * @package Magenest\Profile\Controller\Adminhtml\CampaignEditing
 */
class MassDelete extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing\AbstractMassAction
{
    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _execute()
    {
        $collection = $this->getCollection();
        $collection->walk('delete');
        $this->messageManager->addSuccessMessage(__("%1 record(s) has been deleted.", $collection->count()));
    }
}