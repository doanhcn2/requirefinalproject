<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 15/09/2018
 * Time: 13:08
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;


/**
 * Class MassStatus
 * @package Magenest\Profile\Controller\Adminhtml\CampaignEditing
 */
class MassStatus extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing\AbstractMassAction
{
    protected $approve;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Registry $registry,
        \Magenest\Profile\Controller\Adminhtml\CampaignEditing\Approve $approve,
        \Magenest\Profile\Model\ResourceModel\CampaignEditing\CollectionFactory $CECollectionFactory
    ) {
        $this->approve = $approve;
        parent::__construct($context, $pageFactory, $loggerInterface, $registry, $CECollectionFactory);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _execute()
    {
        $collection    = $this->getCollection();
        $action        = $this->getRequest()->getParam('status');
        $commentStatus = $this->getRequest()->getParam('comment_status');
        if (!$action) {
            // Reject
            $collection->walk('rejectChangeForCampaign', [$commentStatus]);
            $this->messageManager->addSuccessMessage(__("Rejected %1 record(s).", $collection->count()));
        } else {
            // Approve
            $err      = $this->approve($collection, $commentStatus);
            $approved = $collection->count() - $err;
            if ($approved > 0) {
                $this->messageManager->addSuccessMessage(__("Approved %1 record(s)", $approved));
            }
        }
    }

    /**
     * @param $collection
     *
     * @return int
     */
    private function approve($collection, $commentStatus)
    {
        $err = 0;
        foreach ($collection as $item) {
            try {
                $this->approve->approve($item->getId(), $commentStatus);
            } catch (\Exception $e) {
                $err++;
                $this->messageManager->addErrorMessage('Item ID: %1: Something went wrong.', $item->getId());
            }
        }

        return $err;
    }
}