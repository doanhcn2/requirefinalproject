<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Adminhtml\CampaignEditing;

class Index extends \Magenest\Profile\Controller\Adminhtml\CampaignEditing
{
    public function execute()
    {
        //Call page factory to render layout and page content
        $this->_setPageData();
        return $this->getResultPage();
    }
}
