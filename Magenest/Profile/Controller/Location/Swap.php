<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Controller\Location;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Swap extends Action
{
    protected $locationFactory;

    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $factory,
        Context $context
    )
    {
        $this->locationFactory = $factory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id1 = $this->getRequest()->getParam('id1');
        $id2 = $this->getRequest()->getParam('id2');
        $model1 = $this->locationFactory->create()->load($id1);
        $so1 = $model1->getData('sort_order');
        $model2 = $this->locationFactory->create()->load($id2);
        $so2 = $model2->getData('sort_order');
        $model1->setData('sort_order', $so2);
        $model1->save();
        $model2->setData('sort_order', $so1);
        $model2->save();
    }
}
