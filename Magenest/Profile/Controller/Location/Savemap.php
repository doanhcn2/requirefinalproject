<?php

namespace Magenest\Profile\Controller\Location;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Savemap extends AbstractVendor
{
    protected $jsonFactory;
    protected $locationFactory;

    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        $this->locationFactory = $locationFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $mapInfo = $this->getRequest()->getParam('map_info');
        $id = $this->getRequest()->getParam('location_id');
        $location = $this->locationFactory->create()->load($id);
        if ($this->getVendorId())
            $location->setData('vendor_id', $this->getVendorId());
        $location->setData('map_info', $mapInfo['lat'] . ',' . $mapInfo['lng']);
        $location->save();

    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}