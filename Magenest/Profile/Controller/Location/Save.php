<?php

namespace Magenest\Profile\Controller\Location;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends AbstractVendor
{
    protected $locationFactory;
    protected $uploaderFactory;
    protected $adapterFactory;
    protected $fileSystem;
    protected $vendorFactory;
    protected $allowedExtensions = ['jpg', 'jpeg', 'png', 'doc', 'txt', 'docx', 'pdf'];
    protected $fileId = 'artifact_';

    public function __construct(
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        $this->vendorFactory = $vendorFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->fileSystem = $fileSystem;
        $this->locationFactory = $locationFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $location_id = $this->getRequest()->getParam('location_id');
        $removeValues = $this->getRequest()->getParam('remove-value');
        if ($removeValues != '') {
            $this->removeAttachedFile($location_id, $removeValues);
        }
        if (!$location_id) {
            $location_id = $this->createLocation();
        }
        $location_id = $this->saveBusinessHour($location_id);
        $location_id = $this->saveFile($location_id);
        $collection = $this->locationFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
        if (count($collection) == 1) {
            $vendor = $this->vendorFactory->create()->load($this->getVendorId());
            $vendor->setData('primary_location_id', $location_id);
            $vendor->save();
        }
        if (@$this->getRequest()->getParam('from-onboard')) {
            $vendor = $this->vendorFactory->create()->load($this->getVendorId());
            $vendor->setData('business_des', $this->getRequest()->getParam('business_des'));
            $vendor->save();
            return $this->_redirect('udprod/vendor/productNew', array('set_id' => '4-Groupon', 'type_of_product' => 'Groupon', 'type_id' => 'configurable'));
        }
        return $this->_redirect('*/*/edit', ['location_id' => $location_id]);
    }

    protected function removeAttachedFile($locationId, $values)
    {
        $location = $this->locationFactory->create()->load($locationId);
        $attachedFile = json_decode($location->getData('attached_file'), true);
        $valueArr = explode("|$|", $values);
        $isChange = false;
        foreach($valueArr as $value) {
            if ($value) {
                $valuePair = explode('|^|', $value);
                if (isset($valuePair[0]) && isset($valuePair[1])) {
                    foreach($attachedFile as $key => $item) {
                        if ($key === $valuePair[0] && $item === $valuePair[1]) {
                            unset($attachedFile[$valuePair[0]]);
                            $isChange = true;
                        }
                    }
                }
            }
        }
        if ($isChange) {
            $location->setData('attached_file', json_encode($attachedFile));
            try {
                $location->save();
            } catch (\Exception $e) {
            }
        }
    }

    protected function createLocation()
    {
        $location = $this->locationFactory->create()->load(null);
        $collection = $this->locationFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->setOrder('sort_order', 'DESC');
        $firstItem = $collection->getFirstItem();
        $so = $firstItem->getData('sort_order');
        $params = $this->getRequest()->getParams();
        $lat = isset($params['lat']) ? $params['lat'] : '';
        $lng = isset($params['lng']) ? $params['lng'] : '';
        $params['map_info'] = (!empty($lat) && !empty($lng)) ? $lat . ',' . $lng : '';
        $params['vendor_id'] = $this->getVendorId();
        $params['status'] = 0;
        $params['sort_order'] = (int)$so + 1;
        $location->setData($params);
        $location->save();
        return $location->getData('location_id');
    }

    protected function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    protected function getDestinationPath($location_id)
    {
        return $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath('upload/vendor_' . $this->getVendorId() . '/location_' . $location_id . '/');
    }

    protected function saveBusinessHour($location_id)
    {
        $location = $this->locationFactory->create()->load($location_id);
        $businessHour = [];
        $isError = false;
        for($i = 1; $i <= 7; $i++) {
            $weekDay = $this->getWeekDay($i);
            if ($this->getRequest()->getParam($weekDay)) {
                if ($this->getRequest()->getParam($weekDay . '-to-1') > $this->getRequest()->getParam($weekDay . '-from-1')) {
                    $businessHour[$weekDay]['row1']['from'] = $this->getRequest()->getParam($weekDay . '-from-1');
                    $businessHour[$weekDay]['row1']['to'] = $this->getRequest()->getParam($weekDay . '-to-1');
                } else {
                    $isError = true;
                }
                if ($this->getRequest()->getParam($weekDay . '-from-2') && $this->getRequest()->getParam($weekDay . '-to-2')
                && $this->getRequest()->getParam($weekDay . '-to-2') > $this->getRequest()->getParam($weekDay . '-from-2')) {
                    if (isset($businessHour[$weekDay]['row1'])) {
                        $businessHour[$weekDay]['row2']['from'] = $this->getRequest()->getParam($weekDay . '-from-2');
                        $businessHour[$weekDay]['row2']['to'] = $this->getRequest()->getParam($weekDay . '-to-2');
                    } else {
                        $businessHour[$weekDay]['row1']['from'] = $this->getRequest()->getParam($weekDay . '-from-2');
                        $businessHour[$weekDay]['row1']['to'] = $this->getRequest()->getParam($weekDay . '-to-2');
                    }
                } else {
                    $isError = true;
                }
            }
        }
        if ($isError) {
            $this->messageManager->addErrorMessage('Some business hour is invalid, it will be ignore.' );
        }
        if ($businessHour) {
            $location->setData('business_hour', json_encode($businessHour));
            $location->save();
        }

        if (@$applyToAll = $this->getRequest()->getParam('apply_hour')) {
            $collection = $this->locationFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
            foreach($collection as $_location) {
                $_location->setData('business_hour', json_encode($businessHour));
                $_location->save();
            }
        }
        return $location->getData('location_id');
    }

    protected function getWeekDay($number)
    {
        switch ($number) {
            case 1:
                return 'sunday';
            case 2:
                return 'monday';
            case 3:
                return 'tuesday';
            case 4:
                return 'wednesday';
            case 5:
                return 'thursday';
            case 6:
                return 'friday';
            case 7:
                return 'saturday';
            default:
                return false;
        }
    }

    protected function saveFile($location_id)
    {
        $location = $this->locationFactory->create()->load($location_id);
        $allFileName = json_decode($location->getData('attached_file'), true);
        foreach($_FILES as $fileId => $file) {
            if (isset($file['name']) && ($this->getRequest()->getParam($fileId . '_name')) !== null && (file_exists($file['tmp_name']))) {
                try {
                    $destinationPath = $this->getDestinationPath($location_id);
                    $filename = strip_tags($file['name']);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
                    $uploader = $this->uploaderFactory
                        ->create(['fileId' => $fileId])
                        ->setAllowCreateFolders(true)
                        ->setAllowedExtensions($this->allowedExtensions);
                    $newName = $fileId . "." . $extension;
                    $uploader->save($destinationPath, $newName);
                    $key = trim($this->getRequest()->getParam($fileId . '_name'));
                    $allFileName[$key] = "location_" . $location_id . "/" . $newName;
                } catch (\Exception $exception) {
                }
            }
        }
        if (!empty($allFileName)) {
            $location->setData('attached_file', json_encode($allFileName));
            $location->save();
        }

        if (@$this->getRequest()->getParam('apply_file')) {
            $collection = $this->locationFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
            foreach($collection as $_location) {
                $_location->setData('attached_file', $location->getData('attached_file'));
                $_location->save();
            }
        }
        return $location->getData('location_id');
    }
}