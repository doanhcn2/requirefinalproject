<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 28/08/2018
 * Time: 14:18
 */

namespace Magenest\Profile\Helper;


/**
 * Class Document
 * @package Magenest\Profile\Helper
 */
class Document
{
    const ALL_FILES = true;

    const FILES = [
        'commercial_circular',
        'commercial_number',
        'mof_register',
        'registration_document'
    ];

    /**
     * @var string
     */
    protected $vendorId;

    /**
     * @var array
     */
    protected $pendingPaths;

    /**
     * @var array
     */
    protected $approvedPaths;

    /**
     * @var array
     */
    protected $pendingUrls;

    /**
     * @var array
     */
    protected $approvedUrls;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * Document constructor.
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->_filesystem   = $filesystem;
        $this->_storeManager = $storeManager;
    }

    /**
     * @return string
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param $vendorId
     *
     * @return \Magenest\Profile\Helper\Document
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;

        return $this;
    }

    /**
     * @param $pending
     *
     * @return string
     * @throws \Exception
     */
    private function getDirectoryPath($pending)
    {
        $vendorId  = $this->getVendorId();
        $directory = $this->_filesystem
            ->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
            ->getAbsolutePath("upload/vendor_{$vendorId}/documents/");
        if ($pending) {
            $directory .= 'pending/';
        }

        return $directory;
    }

    /**
     * @param $pending
     *
     * @return string
     */
    private function getDocumentsFolderUrl($pending)
    {
        $vendorId     = $this->getVendorId();
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $documentsUrl = "{$baseMediaUrl}upload/vendor_{$vendorId}/documents/";
        if ($pending) {
            $documentsUrl .= 'pending/';
        }

        return $documentsUrl;
    }

    /**
     * @param $name
     * @param bool $pending
     *
     * @return string
     */
    public function getDocumentUrl($name, $pending = false)
    {
        $this->scanDocuments();

        return $pending ? @$this->pendingUrls[$name] : @$this->approvedUrls[$name];
    }

    /**
     * @param bool $force
     *
     * @return \Magenest\Profile\Helper\Document
     */
    public function scanDocuments($force = false)
    {
        if (!isset($this->pendingUrls, $this->pendingPaths) || $force) {
            try {
                $directory = $this->getDirectoryPath(true);
                $folderUrl = $this->getDocumentsFolderUrl(true);

                $this->pendingUrls  = [];
                $this->pendingPaths = [];
                $files              = array_diff(scandir($directory), array('..', '.'));
                foreach ($files as $file) {
                    $fileName                      = pathinfo($file, PATHINFO_FILENAME);
                    $this->pendingUrls[$fileName]  = $folderUrl . $file;
                    $this->pendingPaths[$fileName] = $directory . $file;
                }
            } catch (\Exception $e) {
            }
        }

        if (!isset($this->approvedUrls, $this->approvedPaths) || $force) {
            try {
                $directory = $this->getDirectoryPath(false);
                $folderUrl = $this->getDocumentsFolderUrl(false);

                $this->approvedUrls  = [];
                $this->approvedPaths = [];
                $files               = array_diff(scandir($directory), array('..', '.'));
                foreach ($files as $file) {
                    $fileName                       = pathinfo($file, PATHINFO_FILENAME);
                    $this->approvedUrls[$fileName]  = $folderUrl . $file;
                    $this->approvedPaths[$fileName] = $directory . $file;
                }
            } catch (\Exception $e) {
            }
        }

        return $this;
    }

    /**
     * @param $name
     * @param bool $pending
     *
     * @return string
     */
    private function getDocumentPath($name, $pending = false)
    {
        $this->scanDocuments();

        return $pending ? @$this->pendingPaths[$name] : @$this->approvedPaths[$name];
    }

    /**
     * @param $dir
     * @param array $fileNames
     */
    private function cleanDir($dir, $fileNames = [])
    {
        $filesToRm = array_intersect(self::FILES, $fileNames);
        try {
            $files = array_diff(scandir($dir), array('..', '.'));
        } catch (\Exception $e) {
            $files = [];
        }
        foreach ($files as $file) {
            $fileName = pathinfo($file, PATHINFO_FILENAME);
            if (!in_array($fileName, $filesToRm)) {
                continue;
            }
            try {
                unlink($dir . $file);
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * @param bool $file
     *
     * @throws \Exception
     */
    public function approve($file = self::ALL_FILES)
    {
        if ($file !== self::ALL_FILES) {
            return;
        }
        $this->scanDocuments();
        $documentDir = $this->getDirectoryPath(false);
        $this->cleanDir($documentDir, array_keys($this->pendingPaths));
        foreach (self::FILES as $file) {
            $pending  = $this->getDocumentPath($file, true);
            if ($pending === null) {
                continue;
            }
            $fileName = pathinfo($pending, PATHINFO_FILENAME) . '.' . pathinfo($pending, PATHINFO_EXTENSION);
            rename($pending, $documentDir . $fileName);
        }
    }

    /**
     * @throws \Exception
     */
    public function reject()
    {
        $dir = $this->getDirectoryPath(true);
        $this->cleanDir($dir, self::FILES);
    }
}