/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'underscore',
    'mageUtils',
    'Magento_Ui/js/grid/tree-massactions',
    'Magento_Ui/js/modal/prompt',
    'Magento_Ui/js/modal/alert',
    'uiRegistry'
], function (_, utils, Massactions, prompt, alert, registry) {
    'use strict';

    return Massactions.extend({
        applyAction: function (actionIndex) {
            var data = this.getSelections(),
                action = this.getAction(actionIndex),
                callback,
                visibility;

            if (action.visible) {
                visibility = action.visible();

                this.hideSubmenus(action.parent);
                action.visible(!visibility);

                return this;
            }

            if (!data.total) {
                alert({
                    content: this.noItemsMsg
                });

                return this;
            }

            callback = this._getCallback(action, data);

            if (action.prompt) {
                this._prompt(action, callback);
            } else {
                action.confirm ?
                    this._confirm(action, callback) :
                    callback();
            }
        },

        _getCallback: function (action, selections) {
            var callback = action.callback,
                args = [action, selections];

            if (utils.isObject(callback)) {
                args.unshift(callback.target);

                callback = registry.async(callback.provider);
            } else if (typeof callback !== 'function') {
                callback = this.defaultCallback.bind(this);
            }

            return function (promptVal) {
                if (promptVal) {
                    args.push(promptVal);
                }
                callback.apply(null, args);
            };
        },

        defaultCallback: function (action, data, promptVal) {
            var itemsType = data.excludeMode ? 'excluded' : 'selected',
                selections = {};

            selections[itemsType] = data[itemsType];

            if (!selections[itemsType].length) {
                selections[itemsType] = false;
            }

            _.extend(selections, data.params || {}, promptVal || {});

            utils.submit({
                url: action.url,
                data: selections
            });
        },

        _prompt: function (action, callback) {
            var params = action.prompt,
                args = {},
                fieldName;
            prompt({
                title: params.title,
                label: params.label,
                attributesForm: params.attributesForm,
                attributesField: params.attributesField,
                actions: {
                    confirm: function (v) {
                        if (params.attributesField && params.attributesField.name) {
                            fieldName = params.attributesField.name;
                        } else {
                            fieldName = 'promptValue';
                        }
                        args[fieldName] = v;
                        callback(args);
                    }
                }
            });
        }
    });
});
