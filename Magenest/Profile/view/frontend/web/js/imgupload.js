/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($, $t, ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                photos: ko.observableArray()
            },

            initialize: function () {
                this._super();
                this.photos(this.photos().concat(this.url));
                console.log(this.photos());
                return this;
            },
            uploadImage: function (u, e) {
                var input = e.target;
                var formData = new FormData(input.form);
                formData.append('file', input.files[0]);
                $.ajax({
                    url: window.ORIGIN_BASE_URL + 'profile/business/upload',
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    showLoader: true,
                    success: function (r) {
                        if (r.error === 0) {
                            u.photos.push(r.url);
                        } else {
                            alert(r.error);
                        }
                    },
                    error: function (r) {
                        alert(r);
                    }
                });
            },

            rmImg: function (i) {
                var self = this;
                console.log(self.photos()[i]);
                $.ajax({
                    url: window.ORIGIN_BASE_URL + 'profile/business/remove',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'url' : self.photos()[i]
                    },
                    showLoader: true,
                    success: function (r) {
                        console.log(r);
                        if (r.error === 0) {
                            self.photos.splice(i, 1);
                        } else {
                            alert(r.error);
                        }
                    },
                    error: function (r) {
                        alert(r);
                    }
                });
            }
        });
    }
);