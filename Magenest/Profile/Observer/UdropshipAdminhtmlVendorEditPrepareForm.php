<?php

namespace Magenest\Profile\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UdropshipAdminhtmlVendorEditPrepareForm implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $id = $observer->getEvent()->getId();
        $form = $observer->getEvent()->getForm();
        $fieldset = $form->getElement('vendor_form');

        $countries = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Directory\Model\Config\Source\Country')->toOptionArray();
        $fieldset->addField('promote_product', 'select', array(
            'name' => 'promote_product',
            'label' => __('Promote Product'),
            'required' => true,
            'options' => [
                null => __('------- Please Choose -------'),
                1 => __('Groupon'),
                2 => __('Accommodation'),
                3 => __('Event'),
                4 => __('Product')
            ],

        ));
        $fieldset->addField('business_category', 'text', array(
            'name' => 'business_category',
            'label' => __('Business Category')
        ));
        $fieldset->addField('website', 'text', array(
            'name' => 'website',
            'label' => __('Website')
        ));
        $fieldset->addField('firstname', 'text', array(
            'name' => 'firstname',
            'label' => __('First Name')
        ));
        $fieldset->addField('lastname', 'text', array(
            'name' => 'lastname',
            'label' => __('Last Name')
        ));
        $fieldset->addField('business_role', 'text', array(
            'name' => 'business_role',
            'label' => __('Business Role')
        ));
        $fieldset->addField('business_des', 'textarea', array(
            'name' => 'business_des',
            'label' => __('Business Description')
        ));

        return $this;
    }
}
