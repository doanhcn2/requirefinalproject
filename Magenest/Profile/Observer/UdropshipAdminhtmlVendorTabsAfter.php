<?php

namespace Magenest\Profile\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Layout;

class UdropshipAdminhtmlVendorTabsAfter implements ObserverInterface
{
    /**
     * @var Layout
     */
    protected $_viewLayout;

    public function __construct(
        Layout $viewLayout
    )
    {
        $this->_viewLayout = $viewLayout;
    }

    public function execute(Observer $observer)
    {
        $block = $observer->getBlock();
        $block->addTab('payment_details', [
            'label' => __('Payment Details'),
            'after' => 'vendor_form',
            'content' => $this->_viewLayout->createBlock('Magenest\Profile\Block\Adminhtml\VendorEditTab\Payment\Form', 'vendor.payment.form')
                ->toHtml()
        ]);

        $block->addTab('delivery_area', [
            'label' => __('Delivery Area'),
            'after' => 'payment_details',
            'content' => $this->_viewLayout->createBlock('Magenest\Profile\Block\Adminhtml\VendorEditTab\Delivery', 'vendor.delivery.grid')
                ->toHtml()
        ]);

        $block->addTab('location', [
            'label' => __('Location'),
            'after' => 'location',
            'content' => $this->_viewLayout->createBlock('Magenest\Profile\Block\Adminhtml\VendorEditTab\Location', 'vendor.location.grid')
                ->toHtml()
        ]);


    }
}
