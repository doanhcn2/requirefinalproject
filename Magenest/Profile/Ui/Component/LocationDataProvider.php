<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Ui\Component;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;

/**
 * Class RoomBedDataProvider
 * @package Magenest\Hotel\Ui\DataProvider
 */
class LocationDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $templateFactory,
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $templateFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        if ($this->count($items))
            foreach ($items as $location) {
                $this->_loadedData[$location->getData('location_id')] = $location->getData();
                $mapInfo = explode(',', $location->getData('map_info'));
                $this->_loadedData[$location->getData('location_id')]['lat'] = (@$mapInfo[0] == null) ? '' : @$mapInfo[0];
                $this->_loadedData[$location->getData('location_id')]['long'] = (@$mapInfo[1] == null) ? '' : @$mapInfo[1];
            }
        else
            $this->_loadedData[null] = [
                'vendor_id' => $vendor_id = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\App\RequestInterface::class)->getParam('vendor_id')
            ];
        return $this->_loadedData;
    }
}
