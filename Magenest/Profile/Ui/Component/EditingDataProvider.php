<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Ui\Component;

use Magento\Customer\Api\Data\AttributeMetadataInterface;
use Magento\Customer\Ui\Component\Listing\AttributeRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as FrameworkDataProvider;

class EditingDataProvider extends FrameworkDataProvider
{
    protected $collectionFactory;

    public function __construct(
        \Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory $collectionFactory,
        string $name, string $primaryFieldName, string $requestFieldName, ReportingInterface $reporting, SearchCriteriaBuilder $searchCriteriaBuilder, RequestInterface $request, FilterBuilder $filterBuilder, array $meta = [], array $data = [])
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = parent::getData();
        $collection = $this->collectionFactory->create()
            ->addFieldToSelect('vendor_id')
            ->addFieldToSelect('vendor_name')
            ->getData();
        $arr = [];
        foreach ($collection as $name)
            $arr[$name['vendor_id']] = $name['vendor_name'];

        for ($i = 0; $i < $data['totalRecords']; $i++)
            $data['items'][$i]['vendor_name_old'] = $arr[$data['items'][$i]['vendor_id']];
        return $data;
    }
}
