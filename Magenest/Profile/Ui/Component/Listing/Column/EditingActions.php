<?php

namespace Magenest\Profile\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class EditingActions extends Column
{
    const GRID_URL_PATH_VIEW = 'profile/editing/view';
    const GRID_URL_PATH_REJECT = 'profile/editing/reject';
    const GRID_URL_PATH_APPROVE = 'profile/editing/approve';

    protected $urlBuilder;
    private $viewUrl;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $viewUrl = self::GRID_URL_PATH_VIEW
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->viewUrl = $viewUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['id'])) {
                    $item[$name]['view'] = [
                        'href' => $this->urlBuilder->getUrl($this->viewUrl, ['id' => $item['id']]),
                        'label' => __('View')
                    ];
                    $item[$name]['approve'] = [
                        'href' => $this->urlBuilder->getUrl(self::GRID_URL_PATH_APPROVE, ['id' => $item['id']]),
                        'label' => __('Approve'),
                        'confirm' => [
                            'message' => __('Are you sure you wan\'t to approve  vendor "${ $.$data.id }" details editing?')
                        ]
                    ];
                    $item[$name]['reject'] = [
                        'href' => $this->urlBuilder->getUrl(self::GRID_URL_PATH_REJECT, ['id' => $item['id']]),
                        'label' => __('Reject'),
                        'confirm' => [
                            'message' => __('Are you sure you wan\'t to reject  vendor "${ $.$data.id }" details editing?')
                        ]
                    ];
                }
            }
        }
        return $dataSource;
    }
}