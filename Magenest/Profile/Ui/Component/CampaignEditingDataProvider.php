<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Ui\Component;

use Magento\Customer\Api\Data\AttributeMetadataInterface;
use Magento\Customer\Ui\Component\Listing\AttributeRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as FrameworkDataProvider;

/**
 * Class CampaignEditingDataProvider
 * @package Magenest\Profile\Ui\Component
 */
class CampaignEditingDataProvider extends FrameworkDataProvider
{
    protected $collectionFactory;

    public function __construct(
        \Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory $collectionFactory,
        string $name, string $primaryFieldName, string $requestFieldName, ReportingInterface $reporting, SearchCriteriaBuilder $searchCriteriaBuilder, RequestInterface $request, FilterBuilder $filterBuilder, array $meta = [], array $data = [])
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = parent::getData();

        for ($i = 0; $i < count($data['items']); $i++) {
            /**
             * @var \Magento\Catalog\Model\Product $productModel
             */
            $productModel = ObjectManager::getInstance()->create('Magento\Catalog\Model\Product')->load($data['items'][$i]['product_id']);
            $data['items'][$i]['product_id'] = $productModel->getName();

            /**
             * @var \Unirgy\Dropship\Model\Vendor $vendorModel
             */
            $vendorModel = ObjectManager::getInstance()->create('Unirgy\Dropship\Model\Vendor')->load($data['items'][$i]['vendor_id']);
            $data['items'][$i]['vendor_id'] = $vendorModel->getVendorName();

        }
        return $data;
    }
}
