<?php
namespace Magenest\Profile\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class BusinessCategory
 * @package Magenest\Profile\Model
 */
class BusinessCategory extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\Profile\Model\ResourceModel\BusinessCategory');
    }
}
