<?php
namespace Magenest\Profile\Model;

use Magenest\Profile\Model\ResourceModel\BusinessCategory\CollectionFactory;

/**
 * Class DataProvider
 * @package Magenest\Profile\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array|null
     */
    public function getData()
    {
        if (isset($this->_loadedData))
        {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $business)
        {
            $this->_loadedData[$business->getId()] = $business->getData();
        }
        if(isset( $this->_loadedData)) return $this->_loadedData;
        else return null;
    }
}
