<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 21/04/2018
 * Time: 09:09
 */
namespace Magenest\Profile\Model\ProductPromote;

/**
 * Class Options
 * @package Magenest\Profile\Model\ProductPromote
 */
class Options implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => null, 'label' =>  __('------- Please Choose -------')],
            ['value' => '1', 'label' => __('Groupon')],
            ['value' => '2', 'label' => __('Accommodation')],
            ['value' => '3', 'label' => __('Event')],
            ['value' => '4', 'label' => __('Product')],
        ];
    }
}
