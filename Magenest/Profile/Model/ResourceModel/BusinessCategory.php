<?php
namespace Magenest\Profile\Model\ResourceModel;

/**
 * Class BusinessCategory
 * @package Magenest\Profile\Model\ResourceModel
 */
class BusinessCategory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_business_category', 'id');
    }
}
