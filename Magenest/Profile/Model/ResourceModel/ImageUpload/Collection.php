<?php
namespace Magenest\Profile\Model\ResourceModel\ImageUpload;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Profile\Model\ImageUpload',
            'Magenest\Profile\Model\ResourceModel\ImageUpload'
        );
    }
}
