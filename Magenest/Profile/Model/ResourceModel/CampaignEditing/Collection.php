<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Model\ResourceModel\CampaignEditing;

/**
 * Class Collection
 * @package Magenest\Profile\Model\ResourceModel\CampaignEditing
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Profile\Model\CampaignEditing',
            'Magenest\Profile\Model\ResourceModel\CampaignEditing'
        );
    }
}
