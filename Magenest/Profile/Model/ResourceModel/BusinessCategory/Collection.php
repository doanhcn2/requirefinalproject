<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Model\ResourceModel\BusinessCategory;

/**
 * Class Collection
 * @package Magenest\Profile\Model\ResourceModel\BusinessCategory
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Profile\Model\BusinessCategory',
            'Magenest\Profile\Model\ResourceModel\BusinessCategory'
        );
    }
}
