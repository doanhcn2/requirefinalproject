<?php
namespace Magenest\Profile\Model\ResourceModel;

class Editing extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_vendor_editing', 'id');
    }
}
