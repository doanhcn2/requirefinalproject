<?php
namespace Magenest\Profile\Model\ResourceModel\Editing;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Profile\Model\Editing',
            'Magenest\Profile\Model\ResourceModel\Editing'
        );
    }
}
