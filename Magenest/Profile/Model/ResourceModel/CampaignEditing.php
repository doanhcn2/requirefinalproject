<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Model\ResourceModel;

/**
 * Class CampaignEditing
 * @package Magenest\Profile\Model\ResourceModel
 */
class CampaignEditing extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_vendor_campaign_edit', 'id');
    }
}
