<?php
namespace Magenest\Profile\Model\ResourceModel;

class ImageUpload extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_business_uploaded_images', 'id');
    }
}
