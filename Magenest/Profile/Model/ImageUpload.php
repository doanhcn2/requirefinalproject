<?php
namespace Magenest\Profile\Model;

use Magento\Framework\Model\AbstractModel;

class ImageUpload extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\Profile\Model\ResourceModel\ImageUpload');
    }
}
