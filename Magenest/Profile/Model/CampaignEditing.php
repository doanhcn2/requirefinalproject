<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thang
 * Date: 08/09/2018
 * Time: 10:48
 */

namespace Magenest\Profile\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class CampaignEditing
 * @package Magenest\Profile\Model
 */
class CampaignEditing extends AbstractModel
{
    const EDIT_STATUS_APPROVED = 1;
    const EDIT_STATUS_REJECTED = 2;
    const EDIT_STATUS_PENDING  = 0;

    protected $statusFactory;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\StatusFactory $statusFactory,
        \Magenest\Profile\Model\ResourceModel\CampaignEditing $resource,
        \Magenest\Profile\Model\ResourceModel\CampaignEditing\Collection $resourceCollection,
        array $data = []
    ) {
        $this->statusFactory = $statusFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\Profile\Model\ResourceModel\CampaignEditing');
    }

    /**
     * @param string $statusComment
     *
     * @throws \Exception
     */
    public function rejectChangeForCampaign($statusComment = '')
    {
        $statusHistory = $this->statusFactory->create();
        $statusHistory->setData([
            'product_id'  => $this->getProductId(),
            'status_code' => \Magenest\Groupon\Model\Product\Attribute\CampaignStatus::STATUS_REJECTED,
            'comment'     => $statusComment,
        ])->save();

        $this->delete();
    }
}
