<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Profile\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_street');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_city');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_region');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_zip');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_telephone');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'mailling_country_id');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'email');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'firstname');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'lastname');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'business_role');
            $setup->getConnection()->dropColumn($setup->getTable('magenest_vendor_editing'), 'telephone');

            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_street');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_city');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_region');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_zip');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_telephone');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor'), 'mailling_country_id');

            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_street');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_city');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_region');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_zip');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_telephone');
            $setup->getConnection()->dropColumn($setup->getTable('udropship_vendor_registration'), 'mailling_country_id');

            $table = $setup->getTable('magenest_groupon_delivery');
            $connection = $setup->getConnection();
            $connection->addColumn($table, 'vendor_id', ['TYPE' => Table::TYPE_SMALLINT, 'nullable' => true, 'COMMENT' => 'vendor id']);

            $table2 = $setup->getTable('magenest_groupon_location');
            $connection2 = $setup->getConnection();
            $connection2->addColumn($table2, 'sort_order', ['TYPE' => Table::TYPE_SMALLINT, 'nullable' => true, 'COMMENT' => 'sort order']);
        }

        if (version_compare($context->getVersion(), '1.0.4', '<')) {

            $table = $setup->getTable('magenest_groupon_delivery');
            $connection = $setup->getConnection();
            $connection->addColumn($table, 'map_info', ['TYPE' => Table::TYPE_TEXT, 'default' => '', 'nullable' => true, 'COMMENT' => 'Map Info']);
            $connection->addColumn($table, 'sort_order', ['TYPE' => Table::TYPE_SMALLINT, 'nullable' => true, 'COMMENT' => 'sort order']);


            $table2 = $setup->getTable('magenest_groupon_location');
            $connection2 = $setup->getConnection();
            $connection2->addColumn($table2, 'map_info', ['TYPE' => Table::TYPE_TEXT, 'default' => '', 'nullable' => true, 'COMMENT' => 'Map Info']);
            $connection2->addColumn($table2, 'business_hour', ['TYPE' => Table::TYPE_TEXT, 'default' => '', 'nullable' => true, 'COMMENT' => 'Business Hours']);
            $connection2->addColumn($table2, 'attached_file', ['TYPE' => Table::TYPE_TEXT, 'default' => '', 'nullable' => true, 'COMMENT' => 'Attached File']);
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->createUploadImageTable($setup, $context);
        }
//        if (version_compare($context->getVersion(), '1.0.7', '<')) {
//            $this->addBusinessPhrasesColumn($setup);
//        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $this->createVendorCampaignEditing($setup, $context);
        }

        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $installer = $setup;
            $tableName = 'magenest_business_category';
            if ($installer->tableExists($tableName)) {
                return;
            }

            $table = $installer->getConnection()->newTable(
                $installer->getTable($tableName)
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )->addColumn(
                'product_promote',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false,
                ],
                'Product Promote'
            )->addColumn(
                'root_category',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false
                ],
                'Root Category'
            );
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $tableName = $setup->getTable('magenest_vendor_campaign_edit');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'coupon_deal_changes' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Coupon Deal Change'
                    ],
                    'created_at' => [
                        'type' => Table::TYPE_DATETIME,
                        'nullable' => true,
                        'comment' => 'Created At'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        if (version_compare($context->getVersion(), '1.0.10') < 0) {
            $tableName = $setup->getTable('magenest_vendor_campaign_edit');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'ticket_info_change' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Ticket Info Change'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    private function createUploadImageTable(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $tableName = 'magenest_business_uploaded_images';
        if ($installer->tableExists($tableName)) {
            return;
        }

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Id'
        )->addColumn(
            'vendor_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
            ],
            'Vendor Id'
        )->addColumn(
            'image_path',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => false
            ],
            'Image Path'
        )->addColumn(
            'absolute_path',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => false
            ]
        )->setComment(
            'Magenest Uploaded Business Image'
        );
        $installer->getConnection()->createTable($table);
    }

    private function addBusinessPhrasesColumn($setup)
    {
        $table = $setup->getTable('magenest_business_category');
        $connection = $setup->getConnection();
        $connection->addColumn($table, 'phrases', ['TYPE' => Table::TYPE_TEXT, 'default' => '', 'nullable' => true, 'COMMENT' => 'Phrases']);
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    private function createVendorCampaignEditing(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $tableName = 'magenest_vendor_campaign_edit';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
            ],
            'Product Id'
        )->addColumn(
            'vendor_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
            ],
            'Vendor Id'
        )->addColumn(
            'new_product_data',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => false,
            ],
            'Product Data Before Fix'
        )->addColumn(
            'new_coupon_data',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => false,
            ],
            'Product Data Before Fix'
        )->addColumn(
            'new_coupon_location',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => false,
            ],
            'Location Data Before Fix'
        )->addColumn(
            'status',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
            ],
            'Status'
        )->setComment(
            'Magenest Vendor Campaign Editing'
        );
        $installer->getConnection()->createTable($table);
    }
}
