<?php

namespace Magenest\Profile\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->startSetup();
        $installer = $setup;
        $tableName = 'magenest_vendor_editing';
        $installer->getConnection()->dropTable($tableName);
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn('id',                    Table::TYPE_SMALLINT,    null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'id')
            ->addColumn('vendor_id',             Table::TYPE_SMALLINT,    null, ['nullable' => true],   'vendor_id')
            ->addColumn('business_category',     Table::TYPE_TEXT,        null, ['nullable' => true],   'business_category')
            ->addColumn('website',               Table::TYPE_TEXT,        null, ['nullable' => true],   'website')
            ->addColumn('firstname',             Table::TYPE_TEXT,        null, ['nullable' => true],   'firstname')
            ->addColumn('lastname',              Table::TYPE_TEXT,        null, ['nullable' => true],   'lastname')
            ->addColumn('business_role',         Table::TYPE_TEXT,        null, ['nullable' => true],   'business_role')
            ->addColumn('email',                 Table::TYPE_TEXT,        null, ['nullable' => true],   'email')
            ->addColumn('telephone',             Table::TYPE_TEXT,        null, ['nullable' => true],   'telephone')
            ->addColumn('vendor_name',           Table::TYPE_TEXT,        null, ['nullable' => true],   'vendor_name')
            ->addColumn('business_des',          Table::TYPE_TEXT,        null, ['nullable' => true],   'business_des')
            ->addColumn('bank_name',             Table::TYPE_TEXT,        null, ['nullable' => true],   'bank_name')
            ->addColumn('bank_swiftcode',        Table::TYPE_TEXT,        null, ['nullable' => true],   'bank_swiftcode')
            ->addColumn('bank_iban',             Table::TYPE_TEXT,        null, ['nullable' => true],   'bank_iban')
            ->addColumn('bank_account_name',     Table::TYPE_TEXT,        null, ['nullable' => true],   'bank_account_name')
            ->addColumn('legal_business_name',   Table::TYPE_TEXT,        null, ['nullable' => true],   'legal_business_name')
            ->addColumn('tax_number',            Table::TYPE_TEXT,        null, ['nullable' => true],   'tax_number')
            ->addColumn('primary_location_id',   Table::TYPE_INTEGER,     null, ['nullable' => true],   'primary_location_id')
            ->addColumn('mailling_street',       Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_street')
            ->addColumn('mailling_city',         Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_city')
            ->addColumn('mailling_region',       Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_region')
            ->addColumn('mailling_zip',          Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_zip')
            ->addColumn('mailling_telephone',    Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_telephone')
            ->addColumn('mailling_country_id',   Table::TYPE_TEXT,        null, ['nullable' => true],   'mailling_country_id');
        $installer->getConnection()->createTable($table);
        $setup->endSetup();
    }
}