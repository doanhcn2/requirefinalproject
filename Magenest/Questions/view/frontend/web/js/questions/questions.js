require([
    'jquery',
    'Magento_Ui/js/modal/alert',
    "prototype", "domReady!"
], function($, alert) {
    "use strict";

    $('#filter-btn-all').on("click", filterQuestions.bind(this, 'all'));
    $('#filter-btn-answered').on("click", filterQuestions.bind(this, 'answered'));
    $('#filter-btn-unanswered').on("click", function(){ filterQuestions('unanswered'); });
    $('#filter-btn-public').on("click", filterQuestions.bind(this, 'public'));
    $('#filter-btn-private').on("click", filterQuestions.bind(this, 'private'));

    function filterQuestions(name){
        var type = name;
        var comments = $('#comment-area-vendor-dashboard');
        var url = window.reindexQuestionUrl.replace('TYPE', type);
        $.ajax({
            url: url,
            type: 'post',
            data: {type: type},
            dataType: 'json',
            showLoader: true,
            success: function (response) {
                if (response.result) {
                    comments.html(response.result);
                } else {
                    comments.html("<div style='text-align: center; width: 100%; margin-top: 50px;'><span>No question in this stage.</span></div>");
                }
                $('button[name=' + type + ']').addClass('question-active');
                $('button[name!=' + type + ']').removeClass('question-active');
            },
            error: function () {
                alert({
                    content: $.mage.__('Sorry, something went wrong. Please try again later.')
                });
            }
        });
    }
});