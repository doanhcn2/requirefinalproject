<?php

namespace Magenest\Questions\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('udqa_question');

        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                'udqa_question',
                'is_answered',
                ['type' => Table::TYPE_SMALLINT, 'nullable' => false, 'default'=> 0, 'comment' => 'Questions is answered??'] );
        }
        $setup->endSetup();
    }
}