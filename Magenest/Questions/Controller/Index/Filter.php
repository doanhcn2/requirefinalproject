<?php

namespace Magenest\Questions\Controller\Index;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use function Sodium\add;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Magento\Framework\Controller\ResultFactory;

class Filter extends AbstractVendor
{
    /**
     * @var QuestionFactory
     */
    protected $_questionFactory;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        StoreManagerInterface $storeManager,
        LayoutFactory $viewLayoutFactory,
        Registry $registry,
        ForwardFactory $resultForwardFactory,
        HelperData $helper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\HTTP\Header $httpHeader,
        QuestionFactory $questionFactory
    ) {
        $this->_questionFactory = $questionFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Questions\Block\Questions\Index $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('info.question');
        $view->getLayout()->initMessages();
        $htmlData = $infoBlock->toHtml();
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData(['result' => $htmlData]);
    }
}