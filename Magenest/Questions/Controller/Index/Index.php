<?php
namespace Magenest\Questions\Controller\Index;

use Magento\Backend\App\Action;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Index extends AbstractVendor
{

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_renderPage(null, 'magenest_qa');
    }
}