<?php
namespace Magenest\Questions\Controller\Rating;

use Magento\Backend\App\Action;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Vendor extends AbstractVendor
{

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_renderPage(null, 'magenest_rating');
    }
}