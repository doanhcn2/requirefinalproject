<?php

namespace Magenest\Questions\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Unirgy\DropshipVendorAskQuestion\Controller\Adminhtml\Index\AbstractIndex;

class Save extends AbstractIndex
{
    public function execute()
    {
        if (($data = (array)$this->getRequest()->getPost()) && ($questionId = $this->getRequest()->getParam('id'))) {
            $question = $this->_questionFactory->create()->load($questionId);

            if (! $question->getId()) {
                $this->messageManager->addError(__('The question was removed by another user or does not exist.'));
            } else {
                try {
                    $question->setIsAdminChanges(true);
                    $withoutSpace = preg_replace('/\s+/', '', $data['answer_text']);
                    if (strlen($withoutSpace) > 0 && $data['answer_text'] !== null) {
                        $data['is_answered'] = 1;
                    } else {
                        $data['answer_text'] = null;
                    }
                    $question->addData($data)->save();
                    $this->messageManager->addSuccess(__('The question has been saved.'));
                } catch (\Exception $e){
                    $this->messageManager->addError($e->getMessage());
                }
            }

            return $this->_redirect($this->getRequest()->getParam('ret') == 'pending' ? '*/*/pending' : '*/*/');
        }
        $this->_redirect('/');
    }
}
