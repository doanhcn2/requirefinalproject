<?php

namespace Magenest\Questions\Block\Questions;

use Magento\Backend\Model\Url;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\DropshipVendorAskQuestion\Block\Vendor\Question;
use Unirgy\DropshipVendorAskQuestion\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Unirgy\DropshipVendorAskQuestion\Model\Source;
use Unirgy\Dropship\Model\VendorFactory;
use Magento\Catalog\Model\ProductFactory;

class Index extends Question
{
    /*
     * @vars $_vendorFactory VendorFactory
     */
    private $_vendorFactory;

    /*
     * @vars $_productFactory ProductFactory
     */
    private $_productFactory;

    /**
     * @var $_salesOrderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_salesOrderFactory;

    public function __construct(
        \Magento\Framework\Data\FormFactory $formFactory,
        Context $context,
        QuestionFactory $modelQuestionFactory,
        Source $modelSource,
        HelperData $helperData,
        DropshipHelperData $dropshipHelperData,
        VendorFactory $vendorFactory,
        ProductFactory $productFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Url $modelUrl, array $data = []
    ) {
        $this->_salesOrderFactory = $orderFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_productFactory = $productFactory;
        parent::__construct($formFactory, $context, $modelQuestionFactory, $modelSource, $helperData, $dropshipHelperData, $modelUrl, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Questions'));
        if ($this->getQuestions()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'fme.news.pager'
            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
                $this->getQuestions()
            );
            $this->setChild('pager', $pager);
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getNumStatusQuestion()
    {
        $allData = $this->_questionFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->getData();
        $numAnswer = $numUnanswer = 0;
        $numPrivate = $numPublic = 0;
        foreach($allData as $data) {
            if ($data['question_status'] == 1 && $data['is_answered'] == 0) {
                $numUnanswer++;
            }
            if ($data['question_status'] == 1 && $data['is_answered'] == 1) {
                $numAnswer++;
            }
            if ($data['question_status'] == 1 && $data['visibility'] == 0) {
                $numPrivate++;
            }
            if ($data['question_status'] == 1 && $data['visibility'] == 1) {
                $numPublic++;
            }
        }
        return array('answer' => $numAnswer, 'unanswer' => $numUnanswer, 'private' => $numPrivate, 'public' => $numPublic);
    }

    public function getQuestions($typeRequest = false)
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        if ($this->getRequest()->getParam('type') !== null && $this->getRequest()->getParam('type') !== 'all') {
            $type = $this->getRequest()->getParam('type');
            if ($typeRequest !== false) {
                if (!isset($type)) $type = $typeRequest;
            }
            /** @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection $resultCollections */
            $resultCollections = $this->_questionFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
            if ($type === 'answered') {
                $resultCollections->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_status', '1')->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('is_answered', '1');
            } elseif ($type == 'unanswered') {
                $resultCollections->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_status', '1')->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('is_answered', '0');
            } elseif ($type == 'private') {
                $resultCollections->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_status', '1')->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('visibility', '0');
            } elseif ($type == 'public') {
                $resultCollections->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_status', '1')->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('visibility', '1');
            }
            return $resultCollections->setPageSize($pageSize)->setCurPage($page);
        } else {
            return $this->_questionFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())
                ->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_status', '1')->addOrder('question_date', 'DESC')
                ->setPageSize($pageSize)->setCurPage($page);
        }
    }

    public function getRequestType()
    {
        return $this->getRequest()->getParam('type', false);
    }

    public function getProductInfoByID($product_id)
    {
        $product = $this->_productFactory->create()->load($product_id);
        return [$product->getName(), $product->getProductUrl()];
    }

    public function getVendorNameByID($vendor_id)
    {
        $vendor = $this->_vendorFactory->create()->load($vendor_id);
        return $vendor->getVendorName();
    }

    public function getReindexUrl()
    {
        return $this->getUrl('questions/index/filter/type/TYPE');
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getOrderIncrementId($orderId)
    {
        return $this->_salesOrderFactory->create()->load($orderId)->getIncrementId();
    }
}