<?php

namespace Magenest\Questions\Block\Vendor;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\LayoutFactory;
use Unirgy\DropshipMicrosite\Helper\Data as DropshipMicrositeHelperData;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;

class ListVendor extends Template
{
    /**
     * @var HelperData
     */
    protected $_rateHlp;

    /**
     * @var DropshipMicrositeHelperData
     */
    protected $_msHlp;

    /**
     * @var DropshipHelperData
     */
    protected $_hlp;

    /**
     * @var $_ratingHelper \Magenest\FixUnirgy\Helper\Review\Data
     */
    protected $_ratingHelper;

    public function __construct(
        Context $context,
        HelperData $helperData,
        DropshipMicrositeHelperData $dropshipMicrositeHelperData,
        DropshipHelperData $dropshipHelperData,
        \Magenest\FixUnirgy\Helper\Review\Data $helpData,
        array $data = [])
    {
        $this->_ratingHelper = $helpData;
        $this->_rateHlp = $helperData;
        $this->_msHlp = $dropshipMicrositeHelperData;
        $this->_hlp = $dropshipHelperData;

        parent::__construct($context, $data);
    }

    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        if ($toolbar = $this->getLayout()->getBlock('magenest.rating.toolbar')) {
            $toolbar->setCollection($this->getReviewsCollection());
            $this->setChild('toolbar', $toolbar);
        }

        return $this;
    }

    protected $_reviewCollection;

    public function getReviewsCollection()
    {
        if (null === $this->_reviewCollection) {
            $this->_reviewCollection = $this->_ratingHelper->getListAllRatingCollection($this->getVendor()->getId());
        }
        return $this->_reviewCollection;
    }

    public function getVendor()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getSize()
    {
        return $this->getReviewsCollection()->getSize();
    }

    public function getAddressFormatted($review)
    {
        $addrStr = '';
        if (($__sa = $review->getShippingAddress())) {
            $addrStr = $__sa->getCity();
            if ($__sa->getRegionCode()) {
                $addrStr .= ', ' . $__sa->getRegionCode();
            }
            $addrStr .= ', ' . $__sa->getCountry();
        }
        return $addrStr;
    }
}