<?php
namespace Magenest\AttributeImport\Model\Import;

use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

class Attribute extends \Magento\ImportExport\Model\Import\AbstractEntity
{
    const ENTITY_TYPE_CODE = 'product_attributes';
    const ERROR_OPTION_FORMAT_JSON_INVALID = 'optionFormatJsonInvalid';
    const ERROR_OPTION_FORMAT_INVALID = 'optionFormatInvalid';

    protected $categorySetupFactory;
    protected $masterAttributeCode = 'attribute_code';
    protected $needColumnCheck = true;
    protected $loadedAttributeSet = [];

    protected $messageTemplates = [
        self::ERROR_OPTION_FORMAT_JSON_INVALID => 'Option Format is not in JSON',
        self::ERROR_OPTION_FORMAT_INVALID => 'Option Format is invalid'
    ];

    protected $predefinedData = [
        'user_defined' => 1,
        'visible' => 1,
        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL
    ];

    protected $validColumnNames = [
        'attribute_code',
        'attribute_set',
        'type',
        'option',
        'input',
        'label',
        'required',
        'default',
        'unique',
        'global',
        'searchable',
        'filterable',
        'comparable',
        'visible_on_front',
        'visible_in_advanced_search',
        'filterable_in_search',
        'used_in_product_listing',
        'used_for_sort_by',
        'apply_to',
        'position',
        'used_for_promo_rules',
        'is_used_in_grid',
        'is_visible_in_grid',
        'is_filterable_in_grid'
    ];

    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\ImportExport\Model\ImportFactory $importFactory,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        CategorySetupFactory $categorySetupFactory,
        array $data = []
    )
    {
        parent::__construct($string, $scopeConfig, $importFactory, $resourceHelper, $resource, $errorAggregator, $data);
        $this->categorySetupFactory = $categorySetupFactory;
        foreach ($this->messageTemplates as $errorCode => $message) {
            $this->addMessageTemplate($errorCode, $message);
        }
    }

    protected function _importData()
    {
        /** @var CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create();
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNumber => $rowData) {
                $this->addStaticData($rowData)->convertData($rowData);
                if (!$this->validateRow($rowData, $rowNumber)) {
                    continue;
                }
                $attributeSetId = $this->getAttributeSet($categorySetup, @$rowData['attribute_set']);
                $categorySetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $rowData['attribute_code'],
                    $rowData
                );
                $attributeId = $categorySetup->getAttribute(
                    $categorySetup::CATALOG_PRODUCT_ENTITY_TYPE_ID,
                    $rowData['attribute_code'],
                    'attribute_id'
                );
                $categorySetup->addAttributeToSet(
                    $categorySetup::CATALOG_PRODUCT_ENTITY_TYPE_ID,
                    $attributeSetId,
                    'product-details',
                    $attributeId,
                    1000
                );
            }

        }
        return true;
    }

    private function addStaticData(&$rowData)
    {
        foreach ($this->predefinedData as $key => $value) {
            $rowData[$key] = $value;
        }
        return $this;
    }

    private function convertData(&$rowData)
    {
        $this->convertOption($rowData);
        return $this;
    }

    private function convertOption(&$rowData)
    {
        $rowData['option'] = json_decode($rowData['option'], true);
        return $this;
    }

    /**
     * @param CategorySetup $categorySetup
     * @param string $attributeSetName
     * @return int|string
     */
    private function getAttributeSet($categorySetup, $attributeSetName)
    {
        if (empty($attributeSetName)) {
            return 4;
        }
        $attributeSetName = trim($attributeSetName);
        if (!isset($this->loadedAttributeSet[$attributeSetName])) {
            /* @var $attributeSet \Magento\Eav\Model\Entity\Attribute\Set */
            $attributeSet = ObjectManager::getInstance()->create(\Magento\Eav\Model\Entity\Attribute\Set::class);
            $attributeSet->load($attributeSetName, 'attribute_set_name');

            if (!$attributeSet->getId()) {
                $attributeSet->setEntityTypeId($categorySetup::CATALOG_PRODUCT_ENTITY_TYPE_ID);
                $attributeSet->setAttributeSetName($attributeSetName);
                $attributeSet->validate();
                $attributeSet->save();
                $attributeSet->initFromSkeleton(4);
                $attributeSet->save();
            }
            $this->loadedAttributeSet[$attributeSetName] = $attributeSet->getId();
        }
        return $this->loadedAttributeSet[$attributeSetName];
    }

    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE_CODE;
    }


    public function validateRow(array $rowData, $rowNumber)
    {
        if (isset($this->_validatedRows[$rowNumber])) {
            // check that row is already validated
            return !$this->getErrorAggregator()->isRowInvalid($rowNumber);
        }
        $this->_validatedRows[$rowNumber] = true;
        if (is_array($rowData['option'])) {
            if (!isset($rowData['option']['value']) || !is_array($rowData['option']['value'])) {
                $this->addRowError(self::ERROR_OPTION_FORMAT_INVALID, $rowNumber);
            }
        } elseif (!empty($rowData['option'])) {
            try {
                $options = json_decode($rowData['option'], true);
                if (empty($options)) {
                    throw new \Exception('Option format is not in JSON');
                }
            } catch (\Exception $e) {
                $this->addRowError(self::ERROR_OPTION_FORMAT_INVALID, $rowNumber);

            }
        }
        return true;
    }

}
