<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Order;

use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\ResourceModel\Groupon\Collection;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class CustomerGroupon
 * @package Magenest\Groupon\Block\Order
 */
class CustomerGroupon extends Template
{
    /**
     * Template
     * @var string
     */
    protected $_template = 'order/customer/groupon.phtml';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var GrouponFactory
     */
    protected $_grouponFactory;

    /**
     * @var DealFactory
     */
    protected $_dealFactory;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var AbstractProduct
     */
    protected $_absProduct;

    protected $_locationFactory;

    protected $_productLoaded = [];

    protected $_vendorHelper;

    /**
     * Catalog product type configurable
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    protected $_orderReposity;

    /**
     * CustomerGroupon constructor.
     *
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param GrouponFactory $grouponFactory
     * @param DealFactory $dealFactory
     * @param ProductFactory $productFactory
     * @param AbstractProduct $abstractProduct
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        GrouponFactory $grouponFactory,
        DealFactory $dealFactory,
        ProductFactory $productFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        AbstractProduct $abstractProduct,
        \Unirgy\Dropship\Helper\Data $vendorHelper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        array $data = []
    ) {
        $this->_orderReposity                  = $orderRepository;
        $this->_vendorHelper                   = $vendorHelper;
        $this->_catalogProductTypeConfigurable = $configurable;
        $this->_locationFactory                = $locationFactory;
        $this->_absProduct                     = $abstractProduct;
        $this->_dealFactory                    = $dealFactory;
        $this->_productFactory                 = $productFactory;
        $this->_grouponFactory                 = $grouponFactory;
        $this->_customerSession                = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Tickets'));
    }

    /**
     * Get Customer ID
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    /**
     * @return int|mixed
     */
    public function getStatus()
    {

        $status = $this->getRequest()->getParam('status');

        if ($status == 'available') {
            $status = 1;
        } elseif ($status == 'used') {
            $status = 2;
        } elseif ($status == 'expired') {
            $status = 3;
        } elseif ($status == 'all') {
            $status = 0;
        } elseif ($status == 'expired_soon') {
            $status = 4;
        } elseif ($status == 'gifted') {
            $status = 5;
        }

        return $status;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->getRequest()->getParam('filter');
    }

    /**
     * @throws \Exception
     */
    public function getProductIdsCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize   = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $productIds = $this->getGroupon()->getColumnValues('product_id');
        $parentIds  = [];
        foreach ($productIds as $productId) {
            $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
            if ($parentId)
                array_push($parentIds, $parentId);
        }
        $collection = $this->_productFactory->create()->getCollection()->addFieldToFilter('entity_id', ['in' => $parentIds]);
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }

        return $collection;
    }

    public function isAllRedeemed($coupons)
    {
        foreach ($coupons as $coupon) {
            if ($coupon->getStatus() === "1" || $coupon->getStatus() === 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get Groupon
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     * @throws \Exception
     */
    public function getGroupon()
    {
        $status     = $this->getStatus();
        $customerId = $this->getCustomerId();
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->getGrouponCollection();
        $collection->addFieldToFilter('customer_id', $customerId);

        $keyword = $this->getKeyWord();
        if ($keyword != '') {
            $collection->addFieldToFilter(['product_name', 'groupon_code'], [['like' => '%' . $keyword . '%'], ['like' => '%' . $keyword . '%']]);
        }

        if (!$status || $status == 0) {
            $collection
                ->addFieldToFilter('main_table.status', ['neq' => Groupon::STATUS_CANCELED]);
        } elseif ($status == 4) {
            $daystart = new \DateTime();
            $today    = $daystart->format('Y-m-d');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d');
            $collection
                ->addFieldToFilter('main_table.status', 1)
                ->addFieldToFilter('main_table.status',
                    ['neq' => Groupon::STATUS_CANCELED]);
            $customerDealIds = $collection->getColumnValues('product_id');
            $customerDealIds = array_unique($customerDealIds);
            $ids             = $this->_dealFactory->create()->getCollection()
                ->addFieldToFilter('product_id', ['in' => $customerDealIds])
                ->addFieldToFilter('groupon_expire', ['gteq' => $today])
                ->addFieldToFilter('groupon_expire', ['lteq' => $dateText])
                ->getColumnValues('product_id');
            $ids             = array_unique($ids);
            $collection->clear()->addFieldToFilter('main_table.product_id', ['in' => $ids]);

        } elseif ($status != 4) {
            if ($status == 5) {
                $collection
                    ->addFieldToFilter('main_table.status',
                        ['neq' => Groupon::STATUS_CANCELED])
                    ->addFieldToFilter('recipient', ['notnull' => true]);
            } elseif ($status === 2) {
                $collection
                    ->addFieldToFilter(['main_table.status', 'main_table.customer_status'], [$status, Groupon::CUSTOMER_STATUS_USED]);
            } else {
                $collection
                    ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => Groupon::CUSTOMER_STATUS_USED], ['null' => true]])
                    ->addFieldToFilter('main_table.status', $status);
            }
        }
        $filter = $this->getFilter();
        if ($filter == 'expire') {
            $collection->getSelect()
                ->join(
                    ['groupon_deal' => $collection->getResource()->getTable('magenest_groupon_deal')],
                    'groupon_deal.product_id = main_table.product_id',
                    ['redeemable_after', 'groupon_expire', 'end_time']
                )->order('groupon_expire DESC');
        }

        if ($filter == 'purchase') {
            $collection->setOrder('created_at', 'DESC');
        }

        return $collection;
    }

    public function getNumberOfStatusGroupon($status)
    {
        /** @var Collection $collection */
        $collection = $this->getGrouponCollection()
            ->addFieldToFilter('customer_id', $this->getCustomerId());
        $keyword    = $this->getKeyWord();
        if ($keyword != '') {
            $collection->addFieldToFilter(['product_name', 'groupon_code'], [['like' => '%' . $keyword . '%'], ['like' => '%' . $keyword . '%']]);
        }
        if ($status == 4) {
            $daystart = new \DateTime();
            $today    = $daystart->format('Y-m-d');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d');
            $collection->addFieldToFilter('main_table.status', 1);
            $customerDealIds = $collection->getColumnValues('product_id');
            $customerDealIds = array_unique($customerDealIds);
            $ids             = $this->_dealFactory->create()->getCollection()
                ->addFieldToFilter('product_id', ['in' => $customerDealIds])
                ->addFieldToFilter('groupon_expire', ['gteq' => $today])
                ->addFieldToFilter('groupon_expire', ['lteq' => $dateText])
                ->getColumnValues('product_id');
            $ids             = array_unique($ids);
            $collection->clear()->addFieldToFilter('main_table.product_id', ['in' => $ids]);
        } elseif ($status == 5) {
            $collection->addFieldToFilter('main_table.status', ['neq' => Groupon::STATUS_CANCELED])
                ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => Groupon::CUSTOMER_STATUS_USED], ['null' => true]])
                ->addFieldToFilter('recipient', ['notnull' => true])
                ->getSize();
        } elseif ($status === 2) {
            $collection->addFieldToFilter('main_table.status', ['neq' => Groupon::STATUS_CANCELED])
                ->addFieldToFilter(['main_table.status', 'main_table.customer_status'], [$status, Groupon::CUSTOMER_STATUS_USED])->getSize();
        } else {
            $collection->addFieldToFilter('main_table.status', $status)
                ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => Groupon::CUSTOMER_STATUS_USED], ['null' => true]])->getSize();
        }

        $grouponProductIds = $collection->getColumnValues('product_id');
        $realProductIds    = $this->_productFactory->create()
            ->getCollection()
            ->addFieldToFilter('entity_id', ['in' => $grouponProductIds])
            ->getAllIds();

        $removedIds = array_diff($grouponProductIds, $realProductIds);

        /** @var Groupon $item */
        foreach ($collection as $item)
            if (in_array($item->getProductId(), $removedIds))
                $collection->removeItemByKey($item->getId());

        return count($collection);
    }

    public function getOrder($orderId)
    {
        return $this->_orderReposity->get($orderId);
    }

    public function getDataByProduct($grouponCollection)
    {
        $resultData = [];
        /** @var  $groupon Groupon */
        foreach ($grouponCollection as $groupon) {
            /** @var  $productModel Product */
            $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($groupon->getProductId());
            if (empty($parentId))
                continue;
            if (!isset($resultData[$parentId[0]])) {
                $resultData[$parentId[0]] = [];
            }
            $resultData[$parentId[0]][] = $groupon;
        }

        return $resultData;
    }

    public function getVendorName($product)
    {
        $vendor = $this->_vendorHelper->getVendor($product);

        return $vendor->getData('vendor_name');
    }

    /**
     * @param $productId
     *
     * @return mixed
     */
    public function getDealData($productId)
    {
        $model = $this->_dealFactory->create()->load($productId, 'product_id');

        return $model;

        return $model->getGrouponExpire();
    }

    /**
     * @param $productId
     *
     * @return mixed
     */
    public function getRedeemable($productId)
    {
        $model = $this->_dealFactory->create()->load($productId, 'product_id');

        $redeemDate = date_create($model->getData("groupon_expire"));

        $today = date_create();

        if ($today <= $redeemDate) {
            if ($redeemDate) {
                $diff     = date_diff($redeemDate, $today);
                $dateDiff = $diff->days;
                if ($diff->h > 0 || $diff->i > 0 || $diff->s > 0) {
                    $dateDiff = $dateDiff + 1;
                }

                return $dateDiff;
            }
        }

        return false;
    }

    /**
     * @param $grouponId
     *
     * @return mixed
     */
    public function getOrderId($grouponId)
    {
        $model = $this->_grouponFactory->create()->load($grouponId, 'groupon_id');

        return $model->getOrderId();
    }

    /**
     * @param $product
     * @param $param
     *
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getProductImage($product, $param)
    {
        $img = $this->_absProduct->getImage($product, $param);

        return $img;
    }

    /**
     * @param $productId
     *
     * @return Product
     */
    public function getItems($productId)
    {
        $product = $this->_productFactory->create()->load($productId);

        return $product;
    }

    /**
     * @param $grouponId
     *
     * @return string
     */
    public function printUrl($grouponId)
    {
        $url = $this->getUrl('groupon/order/createGroupon', ['groupon_id' => $grouponId]);

        return $url;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getProductIdsCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'fme.news.pager'
            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
                $this->getProductIdsCollection()
            );
            $this->setChild('pager', $pager);
            $this->getProductIdsCollection()->load();
        }

        return $this;
    }


    public function getLocationInfo($locationId)
    {
        $location = $this->_locationFactory->create()->load($locationId);
        $result   = $location->getData('street') . ' ' . $location->getData('city') . ' ' . $location->getData('country') . ' ' . $location->getData('phone');

        return $result;
    }

    public function getStatusGrouponUrl($status)
    {
        if ($status == 1) {
            $status = 'available';
        } elseif ($status == 2) {
            $status = 'used';
        } elseif ($status == 3) {
            $status = 'expired';
        } elseif ($status == 0) {
            $status = 'all';
        } elseif ($status == 4) {
            $status = 'expired_soon';
        } elseif ($status == 5) {
            $status = 'gifted';
        }

        return $this->getUrl('groupon/order/groupon') . '?' . http_build_query(['status' => $status]);
    }

    /**
     * @param $sortOrder
     *
     * @return string
     */
    public function getFilterUrl($sortOrder)
    {
        $status = $this->getRequest()->getParam('status');
        $params = [];
        if ($status != '') {
            $param['status'] = $status;
        }
        $params['filter'] = $sortOrder;

        return $this->getUrl('groupon/order/groupon') . '?' . http_build_query($params);
    }

    public function filterSelected($filter)
    {
        if ($filter == $this->getFilter()) return 'selected';

        return '';
    }

    private function getKeyWord()
    {
        return $this->getRequest()->getParam('key_word');
    }

    public function getParentGrouponId($data)
    {
        $result = [];
        foreach ($data as $key => $datum) {
            array_push($result, $key);
        }

        return array_unique($result);
    }

    public function prepareDataSend($grouponIds)
    {
        $result = [];
        foreach ($grouponIds as $key => $groupon) {
            $coupons = [];
            foreach ($groupon as $item) {
                array_push($coupons, $item->getProductId());
            }
            $result[$key] = $coupons;
        }

        return json_encode($result);
    }

    public function getBaseMediaUrl()
    {
        return $this->_storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl            = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    public function isRedeemable($status, $productId, $customerStatus)
    {
        if ($status == Groupon::STATUS_AVAILABLE && $customerStatus !== Groupon::CUSTOMER_STATUS_USED) {
            return true;
        }

        return false;
    }

    public function getDisplayTextNonAvailable($status, $customerStatus)
    {
        if ($customerStatus === Groupon::CUSTOMER_STATUS_USED) {
            return 'Used';
        }
        if ($status == Groupon::STATUS_USED) {
            return __("Used");
        } elseif ($status === Groupon::STATUS_EXPIRED) {
            return __("Expired");
        }
        return 'Used';
    }

    /**
     * @return Collection
     */
    public function getGrouponCollection()
    {
        /** @var Collection $collection */
        $collection = $this->_grouponFactory->create()->getCollection();
        return $collection;
    }
}
