<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Order;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class CustomerCreateGroupon
 * @package Magenest\Groupon\Block\Order
 */
class CustomerCreateGroupon extends Template
{
    public function getCouponPath(){
        $grouponId = (int)$this->getRequest()->getParam('groupon_id');
        $directoryPath = DirectoryList::VAR_DIR . '/pdf/';
        $couponName = 'Coupon_' . $grouponId . '.pdf';
        return $directoryPath . $couponName;
    }

}
