<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Vendor;

use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Groupon\Model\GrouponFactory;
use Magento\Framework\App\ObjectManager;

/**
 * Class Campaigns
 * @package Magenest\Groupon\Block\Vendor
 */
class Campaigns extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'vendor/campaigns.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magenest\Groupon\Model\TicketFactory
     */
    protected $ticket;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $deal;

    /**
     * @var \Magenest\Groupon\Model\GrouponFactory
     */
    protected $groupon;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $country;

    /**
     * @var \Magenest\Groupon\Model\LocationFactory
     */
    protected $location;

    protected $delivery;

    /**
     * @var \Magenest\Groupon\Model\TermFactory
     */
    protected $term;

    protected $productFactory;

    protected $parentIds = [];

    protected $viewCount = null;

    protected $_dealInfoFactory;

    protected $_magentoEventFactory;

    protected $productRepository;

    private $productLoad = [];

    /**
     * Campaigns constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\TermFactory $termFactory
     * @param \Magento\Reports\Model\EventFactory $eventFactory
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magento\Directory\Model\Config\Source\Country $country
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory
     * @param \Magenest\Groupon\Model\DealInfoFactory $dealInfoFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\TermFactory $termFactory,
        \Magento\Reports\Model\EventFactory $eventFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magenest\Groupon\Model\DealInfoFactory $dealInfoFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->_magentoEventFactory = $eventFactory;
        $this->_dealInfoFactory = $dealInfoFactory;
        $this->productFactory = $productFactory;
        $this->location = $locationFactory;
        $this->delivery = $deliveryFactory;
        $this->_coreRegistry = $registry;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->deal = $dealFactory;
        $this->term = $termFactory;
        $this->country = $country;
        parent::__construct($context, $data);
    }

    /**
     * initial construct
     */
    protected function construct()
    {
        parent::_construct();
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Deal\Collection
     * Get Full deal of vendor
     */
    public function getFullDeal()
    {
        $status = $this->getFilter();
        $vendorId = $this->getVendorId();

        //get values of current limit
        /** @var  $collection \Magenest\Groupon\Model\ResourceModel\Deal\Collection */
        $collection = $this->deal->create()->getCollection();
        if ($status === 'all' || $status === null) {
            $collection->addFieldToFilter('product_type', 'configurable')
                ->addFieldToFilter('vendor_id', $vendorId);
        } else {
            $collection->addFieldToFilter('product_type', 'configurable')
                ->addFieldToFilter('status', $status)
                ->addFieldToFilter('vendor_id', $vendorId);
        }
        $collection->getSelect()->joinLeft(['info' => $collection->getTable('magenest_groupon_deal_info')], 'main_table.deal_id=info.deal_id', ['sold', 'revenue', 'view_count', 'info_id']);
        $sortField = $this->getRequest()->getParam('field');
        $sortOrder = $this->getRequest()->getParam('order');
        if (($sortOrder == 'asc' || $sortOrder == 'desc')
            && (true == in_array($sortField, ['start_time', 'end_time', 'sold', 'revenue', 'start_valid', 'end_valid', 'view_count']))) {
            $collection->setOrder($sortField, $sortOrder);
        }

//        $this->parentIds = $collection->getColumnValues('product_id');
        return $collection;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getDeal()
    {
        $status = $this->getFilter();
        $vendorId = $this->getVendorId();

        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 20;
        /** @var  $collection \Magenest\Groupon\Model\ResourceModel\Deal\Collection */
        $collection = $this->deal->create()->getCollection();
        $collection->addFieldToFilter('product_type', 'configurable')
            ->addFieldToFilter('vendor_id', $vendorId);
        if ($status === 'all' || $status === null) {
        } elseif ($status === CampaignStatus::STATUS_PENDING_REVIEW) {
            $collection->addFieldToFilter(['status', 'status'], [['eq' => $status], ['null' => true]]);
        } else {
            $collection->addFieldToFilter('status', $status);
        }
        $collection->getSelect()->joinLeft(['info' => $collection->getTable('magenest_groupon_deal_info')],'main_table.deal_id=info.deal_id',['sold','revenue','view_count','info_id']);
        $sortField = $this->getRequest()->getParam('field');
        $sortOrder = $this->getRequest()->getParam('order');
        if (($sortOrder == 'asc' || $sortOrder == 'desc')
            && (true == in_array($sortField, ['start_time', 'end_time', 'sold', 'revenue', 'start_valid', 'end_valid', 'view_count']))) {
            if ($sortField == 'start_valid') {
                $sortField = 'redeemable_after';
            }
            if ($sortField == 'end_valid') {
                $sortField = 'groupon_expire';
            }
            if ($sortField == 'view_count') {
                $sortField = 'info.view_count';
            }
            $collection->setOrder($sortField, $sortOrder);
        }
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        $this->parentIds = $collection->getColumnValues('product_id');
        if (count($this->parentIds) > 0) {
            $productCollection = $this->productFactory->create()
                ->getCollection()
                ->addAttributeToSelect(['name', 'image', 'campaign_status'])
                ->addFieldToFilter('entity_id', ['in' => $this->parentIds])
                ->getItems();
            $this->productLoad = $productCollection;
        }
        return $collection;
    }

    public function sortOrderSelected($sortField, $sortOrder)
    {
        if ($sortField == $this->getRequest()->getParam('field')
            && $sortOrder == $this->getRequest()->getParam('order')) {
            return 'sort-selected';
        }
        return '';

    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor()->getId();
    }

    public function getFilter()
    {

        $status = $this->getRequest()->getParam('filter');

        if ($status == 'live') {
            $status = CampaignStatus::STATUS_LIVE;
        } elseif ($status == 'active') {
            $status = CampaignStatus::STATUS_ACTIVE;
        } elseif ($status == 'scheduled') {
            $status = CampaignStatus::STATUS_SCHEDULED;
        } elseif ($status == 'pending') {
            $status = CampaignStatus::STATUS_PENDING_REVIEW;
        } elseif ($status == 'sold_out') {
            $status = CampaignStatus::STATUS_SOLD_OUT;
        } elseif ($status == 'expired') {
            $status = CampaignStatus::STATUS_EXPIRED;
        } elseif ($status == 'paused') {
            $status = CampaignStatus::STATUS_PAUSED;
        } elseif ($status == 'rejected') {
            $status = CampaignStatus::STATUS_REJECTED;
        } else $status = 'all';

        return $status;
    }


    public function getAjaxLoadUrl()
    {
        return $this->getUrl('groupon/vendor_campaign/page', $this->getRequest()->getParams());
    }

    /**
     * Get Product
     *
     * @param $productId
     * @return \Magento\Catalog\Model\Product
     */
    public function getItems($productId)
    {
        if (isset($this->productLoad[$productId])) {
            $product = $this->productLoad[$productId];
        } else {
            try {
                $product = $this->productRepository->getById($productId);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                /** @var  $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
                $productCollection = $this->productFactory->create()->getCollection();
                $product = $productCollection
                    ->addFieldToSelect('entity_id')
                    ->addFieldToSelect('sku')
                    ->addAttributeToSelect(['name', 'image', 'campaign_status'])
                    ->addFieldToFilter('entity_id', $productId)->getFirstItem();
            }
            $this->productLoad[$productId] = $product;
        }
        return $product;
    }

    /**
     * @return string
     */
//    public function getPagerHtml()
//    {
//        return $this->getChildHtml('pager');
//    }
//
//    protected function _prepareLayout()
//    {
//        parent::_prepareLayout();
//        if ($this->getDeal()) {
//            $pager = $this->getLayout()->createBlock(
//                'Magento\Theme\Block\Html\Pager',
//                'fme.news.pager'
//            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
//                $this->getDeal()
//            );
//            $this->setChild('pager', $pager);
//            $this->getDeal()->load();
//        }
//
//        return $this;
//    }

    /**
     * @param \Magenest\Groupon\Model\Deal $deal
     * @return mixed
     * @throws \Exception
     */
    public function getDealDetails($deal)
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $children */
        $children = $this->deal->create()->getCollection()
            ->addFieldToFilter('parent_id', $deal->getProductId());
        $data['option_count'] = $children->count();
        $data['start_time'] = $deal->getStartTime();
        $data['end_time'] = $deal->getEndTime();
        $data['start_valid'] = $deal->getRedeemableAfter();
        $data['end_valid'] = $deal->getGrouponExpire();
        $data['view_count'] = $this->getProductCount($deal->getProductId());
        $data['product_id'] = $deal->getProductId();
        //Check info model
        //sold
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $grouponCollection */
        $grouponCollection = $this->groupon->create()->getCollection();
        $grouponCollection->addFieldToFilter('product_id', ['in' => $children->getColumnValues('product_id')]);
        $redeemCollection = clone $grouponCollection;
        $data['sold'] = array_sum($grouponCollection->load()->getColumnValues('qty'));

        //revenue
        $data['revenue'] = array_sum($grouponCollection->load()->getColumnValues('price'));
        $commission = array_sum($grouponCollection->load()->getColumnValues('commission'));

        //redeemed
        $redeemCollection->addFieldToFilter('status', Groupon::STATUS_USED);
        $data['redeemed'] = array_sum($redeemCollection->getColumnValues('qty'));
        $isInfoChange = false;
        if ($deal->getData('info_id') != null && $deal->getData('info_id') != '') {
            if ($data['sold'] != $deal->getData('sold') || $data['revenue'] != $deal->getData('revenue') || $data['view_count'] != $deal->getData('view_count')) {
                $infoModel = $this->_dealInfoFactory->create()->load($deal->getData('info_id'));
                $isInfoChange = true;
            }
        }
        if ($isInfoChange || $deal->getData('info_id') == '' || $deal->getData('info_id') == null) {
            if (!$isInfoChange) {
                $infoModel = $this->_dealInfoFactory->create();
            }
            $infoData = [
                'deal_id' => $deal->getId(),
                'product_id' => $deal->getProductId(),
                'sold' => $data['sold'],
                'revenue' => $data['revenue'],
                'view_count' => $data['view_count'],
                'commission' => $commission,
            ];
            $infoModel->addData($infoData);
            $infoModel->save();
        }
        if (!isset($this->productLoad[$deal->getProductId()])) {
            $productCollection = $this->productFactory->create()
                ->getCollection();
            $productModel = $productCollection->addAttributeToSelect(['name', 'campaign_status', 'image'])
                ->addFieldToFilter('entity_id', $deal->getProductId())
                ->getFirstItem();
            $this->productLoad[$deal->getProductId()] = $productModel;
        }
        $productModel = $this->productLoad[$deal->getProductId()];
        $data['status'] = CampaignStatus::getStatusLabelByCode($productModel->getCampaignStatus());
        return $data;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getProductCount($id)
    {
        /** @var  $eventCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $eventCollection = $this->_magentoEventFactory->create()->getCollection();
        $eventCollection->addFieldToFilter('event_type_id', \Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW)
            ->addFieldToFilter('object_id', $id);
        return $eventCollection->getSize();
//        if ($this->viewCount == null) {
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $productcollection = $objectManager->create('\Magento\Reports\Model\ResourceModel\Product\Collection');
//            $prodData = $productcollection->addViewsCount()->getData();
//            $viewCount = [];
//            if (count($prodData) > 0) {
//                foreach ($prodData as $product)
//                    $viewCount[$product['entity_id']] = $product['views'];
//            }
//            $this->viewCount = $viewCount;
//        }
//        return @$this->viewCount[$id] ?: 0;
    }

    public function getFilterParam()
    {
        return $this->getRequest()->getParam('filter') != '' ? $this->getRequest()->getParam('filter') : 'all';
    }

    public function getProductImages($images) {
        if (isset($images) && $images !== 'no_selection') {
            return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $images;
        } else {
            return $this->getDefaultPlaceholderImageUrl();
        }
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    public function getCounterCampaignStatus()
    {
        $result = [];
        $result[CampaignStatus::STATUS_REJECTED] = 0;
        $result[CampaignStatus::STATUS_DRAFT] = 0;
        $result[CampaignStatus::STATUS_PENDING_REVIEW] = 0;
        $result[CampaignStatus::STATUS_SCHEDULED] = 0;
        $result[CampaignStatus::STATUS_LIVE] = 0;
        $result[CampaignStatus::STATUS_ACTIVE] = 0;
        $result[CampaignStatus::STATUS_EXPIRED] = 0;
        $result[CampaignStatus::STATUS_SOLD_OUT] = 0;
        $result[CampaignStatus::STATUS_PAUSED] = 0;
        $result[CampaignStatus::STATUS_APPROVED] = 0;
        $result['all'] = 0;

        $collection = $this->deal->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('product_type', 'configurable');
        foreach($collection as $item) {
            $campaignStatus = (int)$item->getStatus();
            if ($item->getStatus() === null || $campaignStatus === CampaignStatus::STATUS_PENDING_REVIEW) {
                $result[CampaignStatus::STATUS_PENDING_REVIEW]++;
            } else {
                $result[$campaignStatus]++;
            }
            $result['all']++;
        }

        return $result;
    }
}
