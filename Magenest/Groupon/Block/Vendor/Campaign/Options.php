<?php

namespace Magenest\Groupon\Block\Vendor\Campaign;

use Magento\Framework\View\Element\Template;

class Options extends Detail
{
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory $grouponCollectionFactory, \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory $dealCollectionFactory, \Magento\Framework\Registry $registry, Template\Context $context, array $data = [])
    {
        $this->_eavAttribute = $eavAttribute;
        parent::__construct($grouponCollectionFactory, $dealCollectionFactory, $registry, $context, $data);
    }

    public function getOptions()
    {
        $productId = $this->getRequest()->getParam('id');
        $childIds = \Magenest\Groupon\Helper\Data::getChildIds($productId);

        $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'name');

        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $groupon */
        $groupon = $this->grouponCollectionFactory->create();
        $groupon->getSelect()->reset('columns');
        $groupon->getSelect()
            ->columns(['revenue' => 'sum(main_table.price)'])
            ->columns(['sold' => 'sum(main_table.qty)'])
            ->columns(['redeemed' => 'count(main_table.redeemed_at)'])
            ->columns(['refunded' => 'count(main_table.refunded_at)']);
        $groupon->getSelect()->joinRight(['product' => $groupon->getTable('udropship_vendor_product')], "main_table.product_id = product.product_id", ['product.product_id', 'qty' => 'product.stock_qty']);
        $groupon->getSelect()->joinRight(['name' => $groupon->getTable('catalog_product_entity_varchar')], "product.product_id = name.row_id and name.attribute_id ={$attributeId}", ['name' => 'name.value']);
        $groupon->getSelect()->group(['product.product_id', 'name' => 'name.value']);
        $groupon->addFieldToFilter('product.product_id', ['in' => $childIds]);
        $data = $groupon->getData();
        return $data;
    }
}
