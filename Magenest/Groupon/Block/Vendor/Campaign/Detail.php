<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Vendor\Campaign;

use Magenest\Groupon\Helper\Data as DataHelper;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\ResourceModel\Deal\Collection;
use Magento\Framework\View\Element\Template;

class Detail extends Template
{
    /**
     * @var \Magenest\Groupon\Model\Deal
     */
    protected $deal;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $dealProduct;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var bool
     */
    protected $isOption;

    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    protected $grouponCollectionFactory;

    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection
     */
    protected $dealCollectionFactory;

    /**
     * @var []
     */
    protected $childIds = [];

    public function __construct(
        \Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory $grouponCollectionFactory,
        \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory $dealCollectionFactory,
        \Magento\Framework\Registry $registry,
        Template\Context $context, array $data = []
    )
    {
        $this->grouponCollectionFactory = $grouponCollectionFactory;
        $this->dealCollectionFactory = $dealCollectionFactory;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->deal = $this->_registry->registry('current_deal');
        $this->product = $this->_registry->registry('current_product');
        $this->dealProduct = $this->_registry->registry('deal_product');
        $this->isOption = $this->_registry->registry('deal_product_id') == $this->_registry->registry('product_id') ? false : true;
        if ($this->isOption)
            $this->childIds = [$this->getProductId()];
        else
            $this->childIds = DataHelper::getChildIds($this->getDealProductId());
        parent::_construct();
    }

    /**
     * @return bool
     */
    public function isOption()
    {
        return $this->isOption;
    }

    /**
     * @return mixed
     */
    public function getDealProductId()
    {
        return $this->_registry->registry('deal_product_id');
    }

    /**
     * @return \Magento\Catalog\Model\Product|mixed
     */
    public function getDealProduct()
    {
        return $this->dealProduct;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->_registry->registry('product_id');
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function getCustomerViewUrl()
    {
        return $this->getProduct()->getProductUrl();
    }

    /**
     * @return string
     */
    public function getProductCampaignStatus()
    {
        return CampaignStatus::getStatusLabelByCode($this->deal->getStatus());
    }

    public function getSellingFrom()
    {
        $date = @DataHelper::getDealSaleDate($this->getDealProductId())['from'];
        if (empty($date))
            return '';
        return "from " . date("M j, Y", strtotime($date));
    }

    public function getSellingTo()
    {
        $date = @DataHelper::getDealSaleDate($this->getDealProductId())['to'];
        if (empty($date))
            return '';
        return "until " . date("M j, Y", strtotime($date));
    }

    public function getValidFrom()
    {
        $date = @DataHelper::getDealDate($this->getDealProductId())['from'];
        if (empty($date))
            return '';
        return "from " . date("M j, Y", strtotime($date));
    }

    public function getValidTo()
    {
        $date = @DataHelper::getDealDate($this->getDealProductId())['to'];
        if (empty($date))
            return '';
        return "until " . date("M j, Y", strtotime($date));
    }

    public function getUnpublishedIn()
    {
        $today = $date = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));

        $sellingTo = @DataHelper::getDealSaleDate($this->getDealProductId())['to'];
        if (empty($sellingTo))
            return 9999;
        $sellingTo = \DateTime::createFromFormat("Y-m-d H:i:s", $sellingTo);
        if ($sellingTo < $today) {
            return false;
        }

        return $sellingTo->diff($today)->days;
    }

    public function getInvalidIn()
    {
        $today = $date = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));

        $validTo = @DataHelper::getDealSaleDate($this->getDealProductId())['to'];
        if (empty($validTo))
            return 9999;
        $validTo = \DateTime::createFromFormat("Y-m-d H:i:s", $validTo);
        if ($validTo < $today) {
            return false;
        }

        return $today->diff($validTo)->days;
    }
}
