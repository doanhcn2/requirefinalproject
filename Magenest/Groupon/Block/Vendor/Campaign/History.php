<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Groupon\Block\Vendor\Campaign;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\StatusFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class History extends Detail
{
    /**
     * @var $_statusHistory StatusFactory
     */
    protected $_statusHistory;

    public function __construct(
        StatusFactory $statusFactory,
        \Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory $grouponCollectionFactory,
        \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory $dealCollectionFactory,
        \Magento\Framework\Registry $registry,
        Template\Context $context,
        array $data = [])
    {
        $this->_statusHistory = $statusFactory;
        parent::__construct($grouponCollectionFactory, $dealCollectionFactory, $registry, $context, $data);
    }

    public function getChangeHistory()
    {
        $page = ($this->getRequest()->getParam('p')) ?: 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit')) ?: 20;
        $productId = $this->getDealProductId() ?: $this->getRequest()->getParam('product_id');
        $collection = $this->_statusHistory->create()->getCollection()->addFieldToFilter('product_id', $productId)->setOrder('created_at', 'DESC');
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }

        return $collection;
    }

    public function getStatusLabel($id)
    {
        return CampaignStatus::getStatusLabelByCode($id);
    }
}