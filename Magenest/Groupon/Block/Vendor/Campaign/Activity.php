<?php

namespace Magenest\Groupon\Block\Vendor\Campaign;

use Magenest\Groupon\Helper\Data as DataHelper;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\Groupon;

class Activity extends Detail
{
    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getRedeemedCollection()
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $this->childIds])
            ->addFieldToFilter("status", Groupon::STATUS_USED);
    }

    /**
     * @return int
     */
    public function getQtyRedeemed()
    {
        return $this->getRedeemedCollection()->count();
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getAvailableCollection()
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $this->childIds])
            ->addFieldToFilter("status", Groupon::STATUS_AVAILABLE);
    }

    /**
     * @return int
     */
    public function getQtyAvailable()
    {
        return $this->getAvailableCollection()->count();

    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getRefundedCollection()
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $this->childIds])
            ->addFieldToFilter(["status", "refunded_at"], [Groupon::STATUS_CANCELED, "null" => false]);
    }

    /**
     * @return int
     */
    public function getQtyRefunded()
    {
        return $this->getRefundedCollection()->count();
    }
}