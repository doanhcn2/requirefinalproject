<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Groupon\Block\Vendor\Campaign;

use \Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\ResourceModel\Groupon\Collection;

class SalesChart extends Detail
{

    public function getDays($dayNum)
    {
        $now = new \DateTime('now');
        $list = array();
        if ($dayNum === "all_time") {
            $startTime = $this->product->getCreatedAt();
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);

            $period = new \DatePeriod(
                $startTime,
                new \DateInterval('P1D'),
                $now
            );
            foreach ($period as $item) {
                $list[] = $item->format("Y-m-d");
            }
        } elseif ($dayNum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));
            $now = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'))
                ->add(new \DateInterval('P1M'))
                ->sub(new \DateInterval('P1D'));
            $period = new \DatePeriod(
                $startTime,
                new \DateInterval('P1D'),
                $now
            );
            foreach ($period as $item) {
                $list[] = $item->format("Y-m-d");
            }
        } else {
            for ($i = $dayNum - 1; $i >= 0; $i--) {
                $date = new \DateTime();
                $date->sub(new \DateInterval('P' . $i . 'D'));
                $list[] = $date->format('Y-m-d');
            }
        }

        return $list;
    }

    public function getDate($dayNum)
    {
        if ($dayNum === "all_time") {
            $startTime = $this->product->getCreatedAt();
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);
            return $startTime->format("Y-m-d");
        } elseif ($dayNum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-01", date('Y-m-01'));
            return $startTime->format("Y-m-d");
        } else {
            $date = new \DateTime();
            $date->sub(new \DateInterval('P' . $dayNum . 'D'));
            return $date->format('Y-m-d');
        }
    }

    public function getOption($dayOption)
    {
        $daysList = $this->getDays($dayOption);
        foreach ($daysList as $key => $day) {
            $daysList [$day] = [
                'sales' => 0,
                'qtySold' => 0,
                'refunds' => 0,
                'qtyRefund' => 0
            ];
            unset($daysList[$key]);
        }
        return $this->getOptionData($daysList);
    }

    public function getOptionData($daysList)
    {
        /** @var Collection $collection */
        $collection = $this->grouponCollectionFactory->create()
            ->addFieldToFilter('product_id', ['in' => $this->childIds]);
        if (count($collection) > 0) {
            $collection->clear();
            $collection->addFieldToFilter('created_at', array('gt' => array_keys($daysList)[0]));
        }
        /** @var \Magenest\Groupon\Model\Groupon $item */

        $summary = [
            'sales' => 0,
            'qtySold' => 0,
            'refunds' => 0,
            'qtyRefund' => 0,
            'redeemed' => 0
        ];

        /** @var \Magenest\Groupon\Model\Groupon $item */
        foreach ($collection as $item) {
            if (!empty($item->getRefundedAt())) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $item->getRefundedAt())->format("Y-m-d");
                if (isset($daysList[$date])) {
                    $daysList[$date]['qtyRefund'] += $item->getQty();
                    $daysList[$date]['refunds'] += $item->getPrice();
                    $summary['qtyRefund'] += $item->getQty();
                    $summary['refunds'] += $item->getPrice();
                }
                continue;
            }
            if (!empty($item->getCreatedAt())) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $item->getCreatedAt())->format("Y-m-d");
                if (isset($daysList[$date])) {
                    $daysList[$date]['qtySold'] += $item->getQty();
                    $daysList[$date]['sales'] += $item->getPrice();
                    $summary['qtySold'] += $item->getQty();
                    $summary['sales'] += $item->getPrice();
                }
            }

            if ($item->getStatus() == Groupon::STATUS_USED)
                $summary['redeemed'] += $item->getQty();
        }

        return ['option' => $daysList, 'summary' => $summary];
    }

    public function getQtyRedeemedFrom($dayNum)
    {
        $daysList = $this->getDays($dayNum);
        $qtyRedeemed = 0;
        foreach ($daysList as $day) {
            $purchased = $this->grouponCollectionFactory->create()
                ->addFieldToFilter('product_id', ['in' => $this->childIds])
                ->addFieldToFilter('status', 2)
                ->addFieldToFilter('redeemed_at', array('like' => "%$day%"));
            $qtyRedeemed += $purchased->count();
        }
        return $qtyRedeemed;
    }
}