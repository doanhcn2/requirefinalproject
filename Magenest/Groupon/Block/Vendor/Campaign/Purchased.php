<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Vendor\Campaign;


class Purchased extends Detail
{
    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getGroupon()
    {
        $params = $this->getRequest()->getParams();
        $isOption = $this->isOption();
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        $collection = $this->grouponCollectionFactory->create();
        $page = @$params['p'] ?: 1;

        //get values of current limit
        $pageSize = @$params['limit'] ?: 20;

        if (isset($params['search'])) {
            $pattern = '%' . $params['search'] . '%';
            $collection->addFieldToFilter(['groupon_code', 'customer_name'], [['like' => $pattern], ['like' => $pattern]]);
        }

        if (isset($params['sortpt'], $params['order'])) {
            $collection->setOrder($params['sortpt'], strtoupper($params['order']));
        }
        if ($isOption) {
            $collection->addFieldToFilter('product_id', $this->getProductId());
        } else {
            $collection->addFieldToFilter('product_id', ['in' => $this->childIds]);
        }

        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }
}
