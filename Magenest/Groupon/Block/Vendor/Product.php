<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Vendor;

/**
 * Class Product
 * @package Magenest\Groupon\Block\Vendor
 */
class Product extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'vendor/product.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magenest\Groupon\Model\TicketFactory
     */
    protected $ticket;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $deal;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $country;

    /**
     * @var \Magenest\Groupon\Model\LocationFactory
     */
    protected $location;
    protected $delivery;

    /**
     * @var \Magenest\Groupon\Model\TermFactory
     */
    protected $term;

    protected $productFactory;

    /**
     * Product constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Magento\Directory\Model\Config\Source\Country $country
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magenest\Groupon\Model\TermFactory $termFactory,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->ticket = $ticketFactory;
        $this->deal = $dealFactory;
        $this->country = $country;
        $this->location = $locationFactory;
        $this->delivery = $deliveryFactory;
        $this->term = $termFactory;
        $this->productFactory = $productFactory;
        parent::__construct($context, $data);
    }

    /**
     * initial construct
     */
    protected function construct()
    {
        parent::_construct();
    }

    public function getDeal()
    {
        if ($this->getProductId()) {
            $model = $this->deal->create()->load($this->getProductId(), 'product_id');

            return $model;
        }

        if ($this->isEdit()) {
            $model = $this->deal->create()->load($this->getRequest()->getParam('id'), 'product_id');

            return $model;
        }
        return false;
    }

    /**
     * @param null $deal
     * @return array|null
     */
    public function getLocation($deal = null)
    {
        $dealData = $deal ? $deal->getData() : null;
        $dealLocationIds = @$dealData['location_ids'] == null ? [] : explode(',', $dealData['location_ids']);
        $model = $this->location->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId());
        if ($model->count() > 0) {
            $resultUsed = [];
            $result = [];
            $array = $this->getListCountry();
            $location = $model->getData();
            foreach($location as $arrLocation) {
                $ar['countrylist'] = $array;
                $ar['old'] = '1';
                $ar['used'] = false;
                if (in_array($arrLocation['location_id'], $dealLocationIds)) {
                    $ar['used'] = true;
                }
                $list = array_merge($arrLocation, $ar);

                $attached_file = $list['attached_file'];
                if ($attached_file) {
                    $attachedFileArray = json_decode($attached_file);
                    foreach($attachedFileArray as $key => $value) {
                        $list['attached_file_array'][] = ['menu_name' => $key];
                    }
                } else  $list['attached_file_array'][] = ['menu_name' => ''];
                if (!isset($list['attached_file_array'])) {
                    $list['attached_file_array'][] = ['menu_name' => ''];
                }

                if (in_array($arrLocation['location_id'], $dealLocationIds)) {
                    array_push($resultUsed, $list);
                } else
                    array_push($result, $list);
            }
            return [$resultUsed, $result];
        }
        return null;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    /**
     * @param null $deal
     * @return array|null
     */
    public function getDelivery($deal = null)
    {
        $dealData = $deal ? $deal->getData() : null;
        $dealDeliveryIds = @$dealData['delivery_ids'] == null ? [] : explode(',', $dealData['delivery_ids']);
        $collection = $this->delivery->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId());
        if ($collection->count() > 0) {
            $resultUsed = [];
            $result = [];
            $deliveries = $collection->getData();
            foreach ($deliveries as $delivery) {
                $ar['old'] = '1';
                $ar['used'] = false;
                if (in_array($delivery['delivery_id'], $dealDeliveryIds))
                    $ar['used'] = true;
                $list = array_merge($delivery, $ar);
                if (in_array($delivery['delivery_id'], $dealDeliveryIds))
                    array_push($resultUsed, $list);
                else
                    array_push($result, $list);
            }
            return [$resultUsed, $result];
        }
        return null;
    }

    public function getTicket()
    {
        if ($this->getProductId()) {
            $model = $this->ticket->create()->load($this->getProductId(), 'product_id');

            return $model;
        }

        return false;
    }

    /**
     * @return $this|bool
     */
    public function getCoordinateData()
    {
        if ($this->getProductId()) {
            $model = $this->ticket->create()->load($this->getProductId(), 'product_id');

            return $model;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getProductId()
    {
        $id = $this->getInformation();

        if ($id) {
            return $id;
        }

        return false;
    }

    public function isEdit()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $product = $this->productFactory->create()->load($id);
            return $product;
        }
        return false;
    }


    /**
     * @return bool
     */
    public function getBlockProductType()
    {
        $type = $this->getData("product_type");

        if ($type) {
            return $type;
        }
        return false;
    }

    /**
     * @return string
     */
    public function uploadImage()
    {
        return $this->getUrl('groupon/vendor/upload');
    }

    /**
     * get link image
     * @return mixed
     */
    public function getImageUrl()
    {
        $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $url;
    }

    /**
     * @return bool|string
     */
    public function getBackground()
    {
        $data = $this->getCoordinateData();
        if ($data) {
            $background = unserialize($data->getBackground());
            if (isset($background[0]['file'])) {
                return 'groupon/template/' . $background[0]['file'];
            }
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public function getWidthTicket()
    {
        $data = $this->getCoordinateData();
        if ($data["page_width"] > 0) {
            return true;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function getHeightTicket()
    {
        $data = $this->getCoordinateData();
        if ($data["page_height"] > 0) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getListCountry()
    {
        return $this->country->toOptionArray();
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getCouponTerms()
    {
        $collection = $this->term->create()->getCollection();
        return $collection;
    }
}
