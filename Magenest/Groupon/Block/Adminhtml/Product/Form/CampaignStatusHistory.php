<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */
namespace Magenest\Groupon\Block\Adminhtml\Product\Form;

use Magenest\Groupon\Model\ResourceModel\Status\Collection;
use Magenest\Groupon\Model\ResourceModel\Status\CollectionFactory;
use Magento\Framework\View\Element\Template;

class CampaignStatusHistory extends Template
{
    protected $_template = 'product/form/campaign_status_history.phtml';
    protected $_registry;
    protected $collectionFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_registry = $coreRegistry;
        $this->collectionFactory = $collectionFactory;
    }

    public function getProduct()
    {
        return $this->_registry->registry('current_product');
    }

    public function getCampaignStatusHistories()
    {
        /** @var Collection $histories */
        $histories = $this->collectionFactory->create();
        $histories->getSelect()
            ->where('product_id = ?', $this->getProduct()->getId())
            ->order('status_id DESC')
            ->limit(15);
        return $histories;
    }
}