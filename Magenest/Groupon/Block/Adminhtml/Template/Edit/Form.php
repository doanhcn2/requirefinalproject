<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Adminhtml\Template\Edit;
/**
 * Class Form
 * @package Magenest\Groupon\Block\Adminhtml\Template\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magenest\Groupon\Model\TemplateFactory
     */
    protected $template;

    /**
     * Form constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magenest\Groupon\Model\TemplateFactory $templateFactory,
        array $data = []
    ) {
        $this->template= $templateFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('template_form');
        $this->setTitle(__('Template Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $id = $this->_request->getParam('template_id');
        $model = false;
        /* @var $model \Magenest\Groupon\Model\ResourceModel\Term\ */
        if ($id) {
            $model = $this->template->create()->load($id);
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $fieldset = $form->addFieldset('template_fieldset',array(
            'legend' => __('Template')
        ));
        $fieldset->addField('title','text',array(
            'label' 	=> __('Title'),
            'class' 	=> 'required-entry',
            'required' 	=> true,
            'style' 	=> 'width:700px;',
            'name' 		=> 'title'
        ));


        $form->setValues($model->getData());

        $form->addField('template_id', 'hidden', array
        (
            'name' => 'template_id',
            'value' => $model->getTemplateId(),
        ));

        $form->setUseContainer(true);

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
