<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Adminhtml\Template\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveAndContinue
 * @package Magenest\Groupon\Block\Adminhtml\Template\Edit\Button
 */
class PrintAction extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $request = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\RequestInterface');
        $id = $request->getParam('template_id');
        if (@$id)
            $data = [
                'label' => __('Print PDF'),
                'class' => 'secondary',
                'on_click' => sprintf("location.href = '%s';", $this->getPrintUrl($id)),
                'sort_order' => 80,
            ];
        return $data;
    }

    public function getPrintUrl($id)
    {
        return $this->getUrl('*/*/printaction', ['template_id' => $id]);
    }
}
