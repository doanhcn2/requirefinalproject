<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Adminhtml;

/**
 * Class Term
 * @package Magenest\Groupon\Block\Adminhtml
 */
class Term extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_term';
        $this->_blockGroup = 'Magenest_Groupon';

        parent::_construct();
    }
}
