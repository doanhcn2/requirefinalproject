<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\Factory;

/**
 * Class CountryToStoreView
 * @package Magenest\GeoIp\Block\Adminhtml\System\Config
 */
class OfferDeal extends AbstractFieldArray
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * CountryToStoreView constructor.
     * @param Context $context
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        array $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initial object
     */
    protected function _construct()
    {
        $this->addColumn('category', ['label' => __('Category'), 'class' => 'required-entry', 'style' => 'width: 575px;']);
        $this->addColumn('discount_from', ['label' => __('Discount From'), 'class' => 'requ ired-entry validate-digits-range digits-range-0-100']);
        $this->addColumn('discount_to', ['label' => __('Discount To'), 'class' => 'required-entry validate-digits-range digits-range-0-100']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
        $this->setTemplate('Magenest_Groupon::system/config/form/deal_offers.phtml');
    }

    /**
     *
     * @param $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCategorySelect($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $listCategories = $objectManager->create('Magenest\Groupon\Model\Config\Source\Category')->toOptionArray();

        $start = '<select id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '>';
        $end = '</select>';

        $options = '';
        foreach ($listCategories as $category) {
            $row = '<option value="'. $category['value'] .'">' . $category['label'] . '</option>';
            $options .= $row;
        }

        return $start . $options . $end ;
    }

    public function renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new \Exception('Wrong column name specified.');
        }
        $column = $this->_columns[$columnName];
        $inputName = $this->_getCellInputElementName($columnName);

        if ($column['renderer']) {
            return $column['renderer']->setInputName(
                $inputName
            )->setInputId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setColumnName(
                $columnName
            )->setColumn(
                $column
            )->toHtml();
        }

        return '<input type="number" id="' . $this->_getCellInputElementId(
                '<%- _id %>',
                $columnName
            ) .
            '"' .
            ' name="' .
            $inputName .
            '" value="<%- ' .
            $columnName .
            ' %>" ' .
            ($column['size'] ? 'size="' .
                $column['size'] .
                '"' : '') .
            ' class="' .
            (isset(
                $column['class']
            ) ? $column['class'] : 'input-text') . '"' . (isset(
                $column['style']
            ) ? ' style="' . $column['style'] . '"' : '') . '/>';
    }
}
