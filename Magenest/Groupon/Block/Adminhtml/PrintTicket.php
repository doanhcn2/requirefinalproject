<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Adminhtml;

use Magento\Backend\Block\Widget;
use Magento\Framework\Registry;
use Magento\Backend\Block\Template\Context;

/**
 * Class PrintTicket
 * @package Magenest\Groupon\Block\Adminhtml
 */
class PrintTicket extends Widget
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Set Template
     *
     * @var string
     */
    protected $_template = 'ticket/print.phtml';

    /**
     * @var
     */
    protected $_evenFactory;

    /**
     * PrintTicket constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Get Event Model
     *
     * @return mixed
     */
    public function getEvent()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $id = $product->getId();

        return $id;
    }

    /**
     * @param $productId
     * @return string
     */
    public function getPrintUrl($productId)
    {
        return $this->getUrl('groupon/deal/printTicket', ['id'=>$productId]);
    }

    /**
     * @return mixed
     */
    public function getParamId()
    {
        $id = $this->getRequest()->getParam('id');

        return $id;
    }
}
