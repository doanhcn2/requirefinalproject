<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Customer;

use Magento\Framework\View\Element\Template;

class VirtualReview extends Template
{
    /**
     * @var $_customerSession \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $session,
        array $data = []
    ) {
        $this->_customerSession = $session;
        $this->setTemplate('Magenest_Groupon::customer/virtual_review.phtml');
        parent::__construct($context, $data);
    }

    public function getCouponProductIds()
    {
        $parentId = $this->getCouponParentId();
        $listIds = $this->getGrouponByProductLists();
        $result = [];
        foreach($listIds as $key => $listId) {
            if ($key !== (int)$parentId) {
                continue;
            }
            array_push($result, $key);
            foreach($listId as $item) {
                array_push($result, $item);
            }
        }

        return $result;
    }

    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    private function getCouponParentId()
    {
        return $this->getParentId();
    }

    private function getGrouponByProductLists()
    {
        return $this->getGrouponByProduct();
    }
}