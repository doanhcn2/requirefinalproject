<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Customer;

use Magenest\Groupon\Model\Deal;
use Magenest\Groupon\Model\ResourceModel\Deal\Collection;
use Magento\Framework\View\Element\Template;

class FilterLocation extends Template
{
    /**
     * @var $coreRegistry \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var $_eventHelper \Magenest\Ticket\Helper\Event
     */
    protected $_eventHelper;

    /**
     * @var $_grouponHelper \Magenest\Groupon\Helper\Data
     */
    protected $_grouponHelper;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_cacheInterface \Magento\Framework\App\CacheInterface
     */
    protected $_cacheInterface;

    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Location\Collection
     */
    protected $_grouponLocationCollection;

    /**
     * @var \Magenest\Ticket\Model\ResourceModel\EventLocation\Collection
     */
    protected $_ticketlocationCollection;

    /**
     * FilterLocation constructor.
     *
     * @param Template\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magenest\Groupon\Helper\Data $grouponHelper
     * @param \Magenest\Ticket\Helper\Event $event
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\ResourceModel\Location\Collection $grouponLocationCollection
     * @param \Magenest\Ticket\Model\ResourceModel\EventLocation\Collection $ticketLocationCollection
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magenest\Groupon\Helper\Data $grouponHelper,
        \Magenest\Ticket\Helper\Event $event,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\ResourceModel\Location\Collection $grouponLocationCollection,
        \Magenest\Ticket\Model\ResourceModel\EventLocation\Collection $ticketLocationCollection,
        \Magento\Framework\App\CacheInterface $cache,
        array $data = []
    ) {
        $this->_grouponLocationCollection = $grouponLocationCollection;
        $this->_ticketlocationCollection  = $ticketLocationCollection;
        $this->_cacheInterface            = $cache;
        $this->_dealFactory               = $dealFactory;
        $this->_grouponHelper             = $grouponHelper;
        $this->_eventHelper               = $event;
        $this->_coreRegistry              = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function getLocationToFilters()
    {
        $locationData = $this->getCacheLocationFilter();
        if ($locationData && trim($locationData) !== "") {
            return json_decode($locationData, true);
        } else {
            $grouponLocations = $this->_grouponLocationCollection->getLocationFilterData();
            $ticketLocations  = $this->_ticketlocationCollection->getLocationFilterData();
        }
        $result     = array_merge($grouponLocations, $ticketLocations);
        $identifier = "cache_location_filter";
        $this->_cacheInterface->save(json_encode($result), $identifier, ['BLOCK_HTML']);

        return $result;
    }

    protected function getCacheLocationFilter()
    {
        $cacheIdentifier = "cache_location_filter";

        return $this->_cacheInterface->load($cacheIdentifier);
    }

    public function getCurrentCategory()
    {
        return $this->_coreRegistry->registry('current_category');
    }


    public function getCurrentLocationFilter()
    {
        $locations = $this->getRequest()->getParam('locations');
        if (!$locations) {
            return [];
        }
        $locations = explode('>', $locations);

        return $locations && is_array($locations) ? $locations : [];
    }
}
