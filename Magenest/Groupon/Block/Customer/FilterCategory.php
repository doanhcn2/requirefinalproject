<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Customer;

use Magento\Catalog\Model\Category;
use Magento\Framework\View\Element\Template;

class FilterCategory extends Template
{
    protected $_category;

    /**
     * @var $coreRegistry \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->_coreRegistry    = $coreRegistry;
        parent::__construct($context, $data);
    }

    public function isFeaturedCategory()
    {
        $category = $this->getCurrentCategory();

        return $category->hasChildren();
    }

    public function getCurrentCategory()
    {
        if (!isset($this->_category)) {
            $this->_category = $this->_coreRegistry->registry('current_category');
        }

        return $this->_category;
    }

    public function getChildCategories()
    {
        /** @var \Magento\Catalog\Model\Category $category */
        $category     = $this->getCurrentCategory();
        $categoryId   = $category->getId();
        $categoryPath = $category->getPath();
        $pathArray    = explode('/', $categoryPath);
        $allChildren  = $this->_categoryFactory->create()->getCollection()
            ->addPathsFilter($category->getPath() . '/')
            ->addFieldToFilter('path', ['nlike' => $category->getPath() . '/%/%'])
            ->addFieldToSelect('name');
        $result       = [];
        /** @var Category $children */
        foreach ($allChildren as $children) {
            if ($children->getId() === $categoryId) continue;
            $childrenInformation = $this->getChildCategoryInformation($children, count($pathArray));
            if ($childrenInformation) {
                array_push($result, $childrenInformation);
            }
        }

        return $result;
    }

    public function getChildCategoryInformation($category, $pathCount)
    {
        return ['name' => $category->getName(), 'link' => $category->getUrl()];
    }
}