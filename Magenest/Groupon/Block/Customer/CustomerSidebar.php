<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Customer;

class CustomerSidebar extends \Magento\Customer\Block\Account\Dashboard\Info
{
    public function isCustomerAccountDashboardPage()
    {
        $pageInfo = $this->getRequest()->getPathInfo();
        if ($pageInfo === "/customer/account/") {
            return true;
        }

        return false;
    }
}