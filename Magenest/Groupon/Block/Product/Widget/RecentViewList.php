<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 01/06/2018
 * Time: 10:25
 */
namespace Magenest\Groupon\Block\Product\Widget;

use Magento\Framework\View\Element\Template;

class RecentViewList extends Template implements \Magento\Widget\Block\BlockInterface{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}