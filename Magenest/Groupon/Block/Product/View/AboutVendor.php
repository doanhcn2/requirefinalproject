<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 6/8/18
 * Time: 10:43 AM
 */

namespace Magenest\Groupon\Block\Product\View;


use Magento\Framework\View\Element\Template;

class AboutVendor extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry;

    /**
     * @var \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $uHelper;

    protected $_vendor;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Unirgy\DropshipMicrosite\Helper\Data $uHelper,
        array $data = []
    ) {
        $this->uHelper       = $uHelper;
        $this->_vendor       = $this->uHelper->getCurrentVendor();
        $this->_coreRegistry = $registry;
        $data['title']       = @$data['prefix_title'] . ' ' . $this->getVendorName();
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    public function getVendor()
    {
        return $this->_vendor;
    }

    public function getBusinessDes()
    {
        return $this->getVendor()->getBusinessDes();
    }

    public function getVendorName()
    {
        return $this->getVendor()->getVendorName();
    }
}