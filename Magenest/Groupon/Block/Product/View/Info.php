<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Product\View;

use Magenest\Groupon\Model\DealFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Cart as CustomerCart;

/**
 * Class Info
 * @package Magenest\Groupon\Block\Product\View
 */
class Info extends \Magento\Framework\View\Element\Template
{
    /**
     * Google Map API key
     */
    const XML_PATH_GOOGLE_MAP_API_KEY = 'groupon/general_config/google_api_key';

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var
     */
    protected $data;

    /**
     * @var DealFactory
     */
    protected $_dealsFactory;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * Info constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DealFactory $dealFactory
     * @param OrderFactory $orderFactory
     * @param ProductFactory $productFactory
     * @param StockStateInterface $stockStateInterface
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DealFactory $dealFactory,
        OrderFactory $orderFactory,
        ProductFactory $productFactory,
        StockStateInterface $stockStateInterface,
        CustomerCart $cart,
        array $data = []
    ) {
        $this->stockStateInterface = $stockStateInterface;
        $this->productFactory      = $productFactory;
        $this->_orderFactory       = $orderFactory;
        $this->_dealsFactory       = $dealFactory;
        $this->_coreRegistry       = $coreRegistry;
        $this->cart                = $cart;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getScopeConfig()
    {
        return $this->_scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        $request = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\Request\Http');
        $layout  = $request->getFullActionName();
        if ($layout == 'checkout_cart_configure')
            return $this->getRequest()->getParam('product_id');
        return $this->getRequest()->getParam('id');
    }

    /**
     * @return array
     */
    public function getGiftDetails()
    {
        $result        = ['name' => '', 'email' => '', 'message' => ''];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $objectManager->get('Magento\Framework\App\Request\Http');
        $layout  = $request->getFullActionName();
        if ($layout == 'checkout_cart_configure') {
            $quoteItemId = $this->getRequest()->getParam('id');
            if (!$quoteItemId)
                return $result;
            $quoteItem = $this->cart->getQuote()->getItemById($quoteItemId);

            $additionalOptions = $quoteItem->getBuyRequest()->getAdditionalOptions();
            $result = [
                'name'    => @$additionalOptions['To'] == null ? '' : @$additionalOptions['To'],
                'email'   => @$additionalOptions['Email'] == null ? '' : @$additionalOptions['Email'],
                'message' => @$additionalOptions['Message'] == null ? '' : @$additionalOptions['Message']
            ];

        }
        return $result;
    }

    /**
     * Get time countdown
     * @return array
     */
    public function getTimeDeal()
    {
        $collection = $this->_dealsFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getProductId())
            ->getData();

        return $collection;
    }

    /**
     * Ajax update option
     * @return string
     */
    public function getUpdateOption()
    {
        return $this->getUrl('groupon/update/option');
    }

    /**
     * Get Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * Get Number by Product Id
     */
    public function getNumberOrder()
    {
        $data = $this->_orderFactory->create()
            ->getCollection()->getLastItem()->getData();

        return $data;
    }

    /**
     * @return bool
     */
    public function isConfigurableProduct()
    {
        /** @var \Magenest\Groupon\Model\Deal $model */
        $model = $this->_dealsFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getFirstItem();
        if ($model->getDealId() && $model->getProductType() == 'configurable' && $model->getEnable() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function isCouponProduct()
    {
        /** @var \Magenest\Groupon\Model\Deal $model */
        $model = $this->_dealsFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getFirstItem();
        if ($model->getDealId() && $model->getProductType() == 'coupon' && $model->getEnable() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed|null
     */
    public function getLocationCoupon()
    {
        $model    = $this->_dealsFactory->create()->load($this->getProductId(), 'product_id');
        $location = unserialize($model->getLocation());
        $data     = null;
        if (sizeof($location) > 0) {
            $data = $location;
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getDiscountFormCoupon()
    {
        $modelProduct = $this->productFactory->create()->load($this->getProductId());
        $qtyInStock   = $this->stockStateInterface->getStockQty($this->getProductId(), $modelProduct->getStore()->getWebsiteId());
        $price        = round($modelProduct->getPrice(), 2);
        $finalPrice   = $modelProduct->getFinalPrice();
        $save         = round($price - $finalPrice, 2);
        $discount     = ($price == 0) ? '0' : ((round($save / $price, 4)) * 100) . '%';
        $data         = [
            'value'    => $price,
            'you_save' => $save,
            'discount' => $discount,
            'qty'      => $qtyInStock,
        ];

        return $data;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol = $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    /**
     * @return string
     */
    public function getReturnDate()
    {
        return $this->getUrl('groupon/order/date');
    }

    /**
     * @return $this
     */
    public function getLocation($productId)
    {
        $modelDeal = $this->_dealsFactory->create()->load($productId, 'product_id');

        $location = unserialize($modelDeal->getLocation());

        return $location;
    }

    /**
     * @return string
     */
    public function getReturnLocation()
    {
        return $this->getUrl('groupon/update/location', ['_secure' => true]);
    }

    public function getDataLocation()
    {
        return $this->getUrl('groupon/update/formLocation', ['_secure' => true]);
    }

    /**
     * Get Google Map Api key
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_API_KEY);
    }

    public function getLocationOptionField($location)
    {
        return $this->trimText($this->getLocationOptionTitle($location), 55);
    }

    public function getLocationOptionTitle($location)
    {
        $result = "";

        if ($location->getLocationName()) {
            $result .= $location->getLocationName();
            $result .= ' (';
        }
        if ($location->getStreet() ?: $location->getStreetTwo()) {
            $result .= $location->getStreet() ?: $location->getStreetTwo();
            $result .= ',';
        }
        if ($location->getCity()) {
            $result .= $location->getCity();
            $result .= ',';
        }
        if ($location->getRegion()) {
            $result .= $location->getRegion();
            $result .= ',';
        }
        if ($location->getCountry()) {
            $result .= $location->getCountry();
            $result .= ',';
        }
        if ($location->getPhone()) {
            $result .= $location->getPhone();
        }
        if ($location->getLocationName()) {
            $result .= ')';
        }

        return $result;
    }

    public function trimText($input, $length, $ellipses = true, $strip_html = true)
    {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (strlen($input) <= $length) {
            return $input;
        }
        $last_space   = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }
}
