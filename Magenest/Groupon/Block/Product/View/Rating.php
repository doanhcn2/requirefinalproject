<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 14/05/2018
 * Time: 11:09
 */

namespace Magenest\Groupon\Block\Product\View;

use Magenest\SellYours\Helper\RatingHelper;
use Magento\Framework\DB\Select;
use Unirgy\DropshipVendorRatings\Helper\Data;

class Rating extends \Magento\Review\Block\Product\Review
{

    protected $_dealFactory;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    protected $_reviewFactory;

    protected $_storeManager;

    protected $_voteFactory;

    protected $_ratingInfoFactory;

    protected $_grouponFactory;

    protected $_orderItemRepository;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    protected $ratingHelper;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $collectionFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        RatingHelper $ratingHelper,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $collectionFactory, $data);
        $this->ratingHelper         = $ratingHelper;
        $this->_eventFactory        = $eventFactory;
        $this->_dealFactory         = $dealFactory;
        $this->_reviewFactory       = $reviewFactory;
        $this->_storeManager        = $storeManager;
        $this->_voteFactory         = $voteFactory;
        $this->_ratingInfoFactory   = $ratingInfoFactory;
        $this->_grouponFactory      = $grouponFactory;
        $this->_orderItemRepository = $orderItemRepository;
        $this->_orderFactory        = $orderFactory;
        $this->_ticketFactory = $ticketFactory;
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    public function checkGrouponProduct()
    {
        $product = $this->getProduct();
        if ($product->getTypeId() == 'configurable' || $product->getTypeId() == 'ticket') {
            $dealCollection  = $this->_dealFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $product->getId());
            $eventCollection = $this->_eventFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $product->getId());
            if ($dealCollection->getSize() > 0 || $eventCollection->getSize() > 0) return true;
        }
        if ($product->getTypeId() === 'simple') {
            return true;
        }

        return false;
    }

    public function getCurrentProductOrder()
    {
        $currentProductId = $this->getProduct()->getEntityId();
        $result           = [];

        $reviewCollection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($reviewCollection);
        $this->orders($reviewCollection);
        $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', Data::ENTITY_TYPE_ID)
            ->setDateOrder();
        $listOrderIds = $reviewCollection->getColumnValues('shipment_order_id');

        foreach ($listOrderIds as $orderId) {
            $order      = $this->_orderFactory->create()->load($orderId);
            $orderItems = $order->getItems();
            foreach ($orderItems as $item) {
                if ($currentProductId === $item->getProductId()) {
                    if (@$orderId && $orderId !== '') {
                        array_push($result, $orderId);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getReviewCollection()
    {
        $product         = $this->getProduct();

        if ($this->getProduct()->getTypeId() === 'simple') {
            return $this->ratingHelper->getProductReviewCollection($product->getId());
        } else {
            return $this->ratingHelper->getReviewCollection($product->getId());
        }
    }

    public function getAvgRate($reviewIds)
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->_voteFactory->create()->getCollection()
            ->addFieldToFilter('review_id', ['in' => $reviewIds]);
        $collection->getSelect()->reset('columns');
        $collection->addExpressionFieldToSelect('avg_value', 'AVG({{value}})', ['value' => 'main_table.value']);

        return $collection->getFirstItem()->getAvgValue();
    }

    public function shipments($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['sm' => $collection->getTable('sales_shipment')],
                "main_table.rel_entity_pk_value = sm.entity_id and main_table.entity_id = " . Data::ENTITY_TYPE_ID,
                ['review_entity_id' => 'main_table.entity_id', 'shipment_order_id' => 'sm.order_id']
            );

        return $collection;
    }

    public function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => $collection->getTable('sales_order_item')],
                "main_table.rel_entity_pk_value = soi.item_id and main_table.entity_id = " . \Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID,
                ['product_id']
            );

        return $collection;
    }

    public function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => $collection->getTable('sales_order')],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );

        return $collection;
    }

    /**
     * @return \Magento\Catalog\Model\Category
     */
    public function getSmallestCategory()
    {
        $product        = $this->getProduct();
        $categories     = $product->getCategoryCollection()
            ->addFieldToSelect('name');
        $resultCategory = null;
        /** @var  $category \Magento\Catalog\Model\Category */
        foreach ($categories as $category) {
            if ($resultCategory === null || $category->getLevel() < $resultCategory->getLevel()) {
                $resultCategory = $category;
            }
        }

        return $resultCategory;
    }

    function getAllowedRatingAttribute()
    {
        $category = $this->getSmallestCategory();
        if ($category === null) {
            return [];
        }
        $categoryId = $category->getId();
        /** @var  $ratingInfoCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $ratingInfoCollection = $this->_ratingInfoFactory->create()
            ->getCollection();
        $ratingInfoCollection->addFieldToFilter('category_ids',
            [['like' => '%,' . $categoryId], ['like' => '%,' . $categoryId . ',%'], ['like' => $categoryId . ',%'], $categoryId]);

        return $ratingInfoCollection->getColumnValues('rating_id');
    }

    public function getNewestReview($ratingAttrId, $reviewIds)
    {
        /** @var  $voteCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $voteCollection = $this->_voteFactory->create()->getCollection()
            ->addFieldToFilter('main_table.review_id', ['in' => $reviewIds])
            ->addFieldToFilter('main_table.rating_id', $ratingAttrId);
        $voteCollection->setStoreFilter(
            $this->_storeManager->getStore()->getId()
        );
        $voteCollection->addRatingInfo(
            $this->_storeManager->getStore()->getId()
        );
        $voteCollection->getSelect()->joinLeft(['review_detail' => $voteCollection->getTable('review_detail')], 'main_table.review_id = review_detail.review_id', ['title', 'detail', 'nickname']);

//        $voteCollection->getSelect()->joinLeft(['rating' => $voteCollection->getTable('rating')],'main_table.rating_id = rating.rating_id',['rating_code', 'is_active', 'is_aggregate']);
        return $voteCollection->getLastItem();
    }

    public function getTotalReview($customerId)
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->_reviewFactory->create()->getCollection()
            ->addFieldToFilter('customer_id', $customerId);
        $collection->getSelect()->reset(Select::COLUMNS);
        $collection->addExpressionFieldToSelect('customer_id', 'detail.customer_id', []);
        $collection->addExpressionFieldToSelect('total_review', 'count({{review_id}})', ['review_id' => 'main_table.review_id']);
        $collection->getSelect()->group('customer_id');

        return $collection->getFirstItem() != null ? $collection->getFirstItem()->getTotalReview() : 0;

    }

    public function getRedeemAt($orderItemId)
    {
        $orderItem  = $this->_orderItemRepository->get($orderItemId);
        $orderId    = $orderItem->getOrderId();
        $productId = $orderItem->getProductId();
        if ($orderItem->getProductType() === 'configurable') {
            $childes = $this->_dealFactory->create()->getCollection()
                ->addFieldToFilter('parent_id', $productId)->getColumnValues('product_id');
            array_push($childes, $productId);

            $collection = $this->_grouponFactory->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('product_id', ['in' => $childes]);
            if ($collection->getSize() > 0) {
                $grouponModel = $collection->getLastItem();

                if ($grouponModel->getUpdatedAt()) {
                    return date('F d, Y', strtotime($grouponModel->getUpdatedAt()));
                }
            }
        } elseif ($orderItem->getProductType() === 'ticket') {
            $collection = $this->_ticketFactory->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('product_id', $productId);
            if ($collection->getSize() > 0) {
                $ticketModel = $collection->getLastItem();

                if ($ticketModel->getUpdatedAt()) {
                    return date('F d, Y', strtotime($ticketModel->getUpdatedAt()));
                }
            }
        } elseif ($orderItem->getProductType() === 'simple') {

        }

        return false;
    }
}
