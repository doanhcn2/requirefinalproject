<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Product\View;

use Magento\Catalog\Model\Product;
use Magento\TestFramework\Event\Magento;

/**
 * Class GrouponDeal
 * @package Magenest\Groupon\Block\Product\View
 */
class GrouponDeal extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var null
     */
    protected $_product = null;

    /**
     * GrouponDeal constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     */
    protected function construct()
    {
        parent::_construct();
    }

    /**
     * @return bool
     */
    public function hasDeal()
    {
        $productId = $this->getRequest()->getParam('id');
        return \Magenest\Groupon\Helper\Data::isDeal($productId);
    }

    /**
     * @return mixed
     */
    public function getDeal()
    {
        if ($this->hasDeal()) {
            $productId = $this->getRequest()->getParam('id');
            $objectMaganer = \Magento\Framework\App\ObjectManager::getInstance();
            $deal = $objectMaganer->create('Magenest\Groupon\Model\Deal')->load($productId, 'product_id');
            return $deal;
        }
    }

    /**
     * @param $termId
     * @return mixed
     */
    public function getTermById($termId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $term = $objectManager->create('Magenest\Groupon\Model\Term')->load($termId, 'term_id');
        return $term;
    }

    public function getMaximunRedeemCoupon()
    {
        $maximunRedeemCoupon = 0;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dealCollection = $objectManager->create('Magenest\Groupon\Model\ResourceModel\Deal\Collection');
        $dealCollection->addFieldToFilter('parent_id', $this->getRequest()->getParam('id'));
        /**
         * @var \Magenest\Groupon\Model\Deal $dealModel
         */
        foreach ($dealCollection as $dealModel) {
            if ($maximunRedeemCoupon == 0) $maximunRedeemCoupon = $dealModel->getMaximumBuyersLimit();
            if ($maximunRedeemCoupon > $dealModel->getMaximumBuyersLimit()) $maximunRedeemCoupon = $dealModel->getMaximumBuyersLimit();
        }
        return $maximunRedeemCoupon;
    }
}
