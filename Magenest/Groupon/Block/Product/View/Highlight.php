<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Block\Product\View;

use Magento\Catalog\Model\Product;

/**
 * Class Highlight
 * @package Magenest\Groupon\Block\Product\View
 */
class Highlight extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    
    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $_ticketFactory;

    /**
     * Highlight constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        array $data = []
    ) {
        $this->_ticketFactory = $ticketFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function construct()
    {
        parent::_construct();
    }

    /**
     * Get Deal
     * @return array
     */
    public function getDeal()
    {
        $collection = $this->_ticketFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getProductId())
            ->getData();
        
        return $collection;
    }
    
    /**
     * Get Product Id
     *
     * @return int
     */
    public function getProductId()
    {
        $product = $this->getRequest()->getParam('id');
        
        return $product;
    }
}
