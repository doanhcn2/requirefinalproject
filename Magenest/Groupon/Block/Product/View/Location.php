<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Block\Product\View;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;

/**
 * Class Location
 * @package Magenest\Groupon\Block\Product\View
 */
class Location extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFatory;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory
     */
    protected $_locationCollectionFactory;

    /**
     * @var \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory
     */
    protected $_ticketLocationCollectionFactory;

    /**
     * Location constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory $collectionFactory
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $ticketCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory $collectionFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $ticketCollection,
        array $data = []
    ) {
        $this->_ticketLocationCollectionFactory = $ticketCollection;
        $this->_eventFactory                    = $eventFactory;
        $this->_dealFatory                      = $dealFactory;
        $this->_coreRegistry                    = $registry;
        $this->_locationCollectionFactory       = $collectionFactory;
        parent::__construct($context, $data);
    }

    protected function construct()
    {
        parent::_construct();
    }

    /**
     * Get Deals
     * @return array
     */
    public function getDeal()
    {
        $type = $this->getProductType();
        if ($type === 'ticket') {
            $collection = $this->_eventFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getFirstItem()->getData();
            return $collection;
        } elseif ($type === 'configurable') {
            $collection = $this->_dealFatory->create()
                ->getCollection()
                ->addFieldToFilter('product_id', $this->getProductId())
                ->getFirstItem()
                ->getData();
            return $collection;
        }

        return [];
    }

    public function getLocation($locationIds = null)
    {
        $type = $this->getProductType();
        if ($type === 'ticket') {
            $collection = $this->_ticketLocationCollectionFactory->create()
                ->addFieldToFilter('product_id', $this->getProductId())->getFirstItem();
            return $collection->getData();
        } elseif ($type === 'configurable') {
            $collection = $this->_locationCollectionFactory->create();
            $collection->addFieldToFilter('location_id', ['in' => $locationIds]);
            return $collection->getData();
        }

        return [];
    }

    /**
     * Get Product Id
     * @return int
     */
    public function getProductId()
    {
        $request = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\Request\Http');
        $layout  = $request->getFullActionName();
        if ($layout == 'checkout_cart_configure')
            return $this->getRequest()->getParam('product_id');
        $productId = $this->getRequest()->getParam('id');

        return $productId;
    }

    public function getProductType()
    {
        return ObjectManager::getInstance()->create('Magento\Catalog\Model\Product')->load($this->getProductId())->getTypeId();
    }
}
