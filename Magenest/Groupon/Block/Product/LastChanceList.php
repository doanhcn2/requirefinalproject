<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 30/05/2018
 * Time: 14:59
 */
namespace Magenest\Groupon\Block\Product;
use Magento\Framework\DB\Select;

class LastChanceList extends \Magento\CatalogWidget\Block\Product\ProductsList{

    protected $_dealFactory;

    protected $_udProductFactory;

    private $_childProduct = [];

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_ratingHelper \Magenest\SellYours\Helper\RatingHelper
     */
    protected $_ratingHelper;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Rule\Model\Condition\Sql\Builder $sqlBuilder,
        \Magento\CatalogWidget\Model\Rule $rule,
        \Magento\Widget\Helper\Conditions $conditionsHelper,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $udProductFactory,
        \Magenest\SellYours\Helper\RatingHelper $ratingHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = [],
        $json = null
    ) {
        $this->_productFactory = $productFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_ratingHelper = $ratingHelper;
        $this->_dealFactory = $dealFactory;
        $this->_udProductFactory = $udProductFactory;
        parent::__construct($context, $productCollectionFactory,
            $catalogProductVisibility, $httpContext, $sqlBuilder, $rule,
            $conditionsHelper, $data, $json);
    }

    protected function _beforeToHtml()
    {
        return $this;
    }

    /**
     * @return array
     */
    public function getGrouponProductIds(){
        /** @var  $dealCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection*/
        $dealCollection = $this->_dealFactory->create()->getCollection();
        $dealCollection->addFieldToFilter('product_type', 'configurable');
        $timeStart = date("Y-m-d H:i:s", time());
        $timeEnd = date("Y-m-d H:i:s", time() + 172800);
        $dealCollection->addFieldToFilter('end_time', ['lt' => $timeEnd]);
        $dealCollection->addFieldToFilter('end_time', ['gt' => $timeStart]);
        $dealCollection->getSelect()->limit(1000);
        $productIds = $dealCollection->getColumnValues('product_id');

        return $productIds;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getLastChanceProductCollection()
    {
        $productIds = $this->getGrouponProductIds();
        /** @var  $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect(['price', 'name', 'small_image']);
        $productCollection->getSelect()->joinLeft(['deal_table' => $productCollection->getTable('magenest_groupon_deal')], 'e.entity_id=deal_table.product_id', ['end_time']);
        $productCollection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $productCollection->getSelect()->__toString();
        $productCollection->getSelect()->limit(1000);
        $this->saveChildProduct($productIds);
        return $productCollection;
    }

    private function saveChildProduct($productIds){
        if(count($this->_childProduct)==0){
            $result = [];
            $productCollection = $this->_udProductFactory->create()->getCollection();
            $productCollection->addFieldToSelect(['product_id', 'vendor_price', 'special_price']);;
            $productCollection->getSelect()->joinLeft(['deal_table' => $productCollection->getTable('magenest_groupon_deal')],'main_table.product_id = deal_table.product_id','parent_id');
            $productCollection->addFieldToFilter('deal_table.parent_id', ['in' => $productIds]);
            $data = $productCollection->getData();
            foreach ($data as $item) {
                $result[$item['parent_id']][] = $item;
            }
            $this->_childProduct = $result;
        }
    }

    public function getChildProduct(){
        return $this->_childProduct;
    }

    public function getIsNew($data) {
        $udProduct = $this->_udProductFactory->create()->load($data, 'product_id');
        if ($udProduct->getState() === 'new') {
            return true;
        }

        return false;
    }

    public function getIsSpringSales($id)
    {
        $product = $this->_productFactory->create()->load($id);

        return $product->getSpringsale() !== null && $product->getSpringsale() === '1';
    }

    public function getReviewCollection($id)
    {
        return $this->_ratingHelper->getReviewCollection($id);
    }

    public function getAvgRate($rateIds)
    {
        return $this->_ratingHelper->getAvgRate($rateIds);
    }

    public function getVendorName($productId)
    {
        $product = $this->_udProductFactory->create()->load($productId, 'product_id');
        $vendorId = $product->getVendorId();
        return $this->_vendorFactory->create()->load($vendorId)->getVendorName();
    }
}
