<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model\Product\Type;

use Magento\Catalog\Model\Product\Type\Virtual;

/**
 * Class Coupon
 * @package Magenest\Groupon\Model\Product\Type
 */
class Coupon extends Virtual
{
    const TYPE_CODE = 'coupon';
    /**
     * Check if product has options
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    public function hasOptions($product)
    {
        return true;
    }
}
