<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Feedback
 * @package Magenest\Groupon\Model
 */
class Feedback extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Feedback');
    }
}
