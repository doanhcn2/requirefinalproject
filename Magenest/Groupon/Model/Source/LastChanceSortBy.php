<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 30/05/2018
 * Time: 15:00
 */
namespace Magenest\Groupon\Model\Source;
use Magento\Framework\Option\ArrayInterface;

class LastChanceSortBy implements ArrayInterface{

    public function toOptionArray()
    {
        $data = [
            [
                'value' => 'product_name',
                'label' => 'Product Name'
            ],
            [
                'value' => 'time',
                'label' => 'End Day'
            ]
        ];
        return $data;
    }
}