<?php
namespace Magenest\Groupon\Model\Source;

use Magenest\Groupon\Model\TermFactory;

/**
 * Class Terms
 * @package Magenest\Groupon\Model\Source
 */
class Terms implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var TermFactory
     */
    protected $term;

    /**
     * Terms constructor.
     * @param TermFactory $termFactory
     */
    public function __construct(
        TermFactory $termFactory
    ) {
        $this->term = $termFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $model = $this->term->create()->getCollection();
        $result = [];
        foreach ($model as $temp) {
            $array = [
                'label' => $temp->getLabel(),
                'value' => $temp->getTermId()
            ];
            array_push($result, $array);
        }
        return $result;
    }
}
