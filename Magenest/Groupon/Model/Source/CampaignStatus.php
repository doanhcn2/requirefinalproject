<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model\Source;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus as Status;

class CampaignStatus implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Magenest\Groupon\Model\Product\Attribute\CampaignStatus $_campaignStatus
     */
    protected $_campaignStatus;

    public function __construct(\Magenest\Groupon\Model\Product\Attribute\CampaignStatus $campaignStatus)
    {
        $this->_campaignStatus = $campaignStatus;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = [
                ['label' => __('Rejected'), 'value' => Status::STATUS_REJECTED],
                ['label' => __('Pending Review'), 'value' => Status::STATUS_PENDING_REVIEW],
                ['label' => __('Approved'), 'value' => Status::STATUS_APPROVED],
                ['label' => __('Sold Out'), 'value' => Status::STATUS_SOLD_OUT],
                ['label' => __('Paused'), 'value' => Status::STATUS_PAUSED],
                ['label' => __('Draft'), 'value' => Status::STATUS_DRAFT],
                ['label' => __('Scheduled'), 'value' => Status::STATUS_SCHEDULED],
                ['label' => __('Live'), 'value' => Status::STATUS_LIVE],
                ['label' => __('Active'), 'value' => Status::STATUS_ACTIVE],
                ['label' => __('Expired'), 'value' => Status::STATUS_EXPIRED]
            ];
        }

        return $this->options;
    }
}