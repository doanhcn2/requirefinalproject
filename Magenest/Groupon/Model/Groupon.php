<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model;

use Magenest\Redeem\Model\Redemption;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\App\Area;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Groupon\Helper\Pdf;
use Magenest\Groupon\Model\Mail\Template\TransportBuilder;

/**
 * Class Groupon
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getGrouponId()
 * @method int getProductId()
 * @method string getGrouponCode()
 * @method string getRedemptionCode()
 * @method int getOrderId()
 * @method int getStatus()
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method string getRefundedAt()
 * @method float getPrice()
 * @method int getQty()
 * @method string getProductName()
 * @method string getLocation()
 * @method int getCustomerId()
 * @method int getCustomerEmail()
 * @method int getCustomerName()
 */
class Groupon extends AbstractModel
{
    const STATUS_AVAILABLE = 1;
    const STATUS_USED = 2;
    const STATUS_EXPIRED = 3;
    const STATUS_CANCELED = 4;
    const CUSTOMER_STATUS_USED = 2;

    const STATUS_REDEEMED_TEXT = 'redeemed';
    const STATUS_USED_TEXT = 'used';
    const STATUS_NOTFOUND_TEXT = 'not found';
    const STATUS_EXPIRED_TEXT = 'expired';
    const STATUS_CANCELED_TEXT = 'canceled';
    /**
     * Const Email
     */
    const XML_PATH_EMAIL_SENDER = 'trans_email/ident_general/email';

    /**
     * Const Name
     */
    const XML_PATH_NAME_SENDER = 'trans_email/ident_general/name';

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Pdf
     */
    protected $_pdf;

    /**
     * @var Ticket
     */
    protected $ticket;

    /**
     * @var Option
     */
    protected $option;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Groupon');
    }

    /**
     * Groupon constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param DealFactory $dealFactory
     * @param Pdf $pdf
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        Pdf $pdf,
        array $data = []
    ) {
        parent::__construct($context, $registry);
        $this->_dealFactory = $dealFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_storeManager = $storeManager;
        $this->_pdf = $pdf;
    }

    /**
     * Send Mail to customer
     *
     * @param $eventName
     * @throws \Exception
     */
    public function sendMail($grouponId)
    {
//        if (!$this->getId()) {
//            return;
//        }
        $data = $this->load($grouponId);
        $pdf = $this->_pdf->getPdf(null,$data);
        $file = $pdf->render();
        $this->inlineTranslation->suspend();
        $recipient = null;

        if ($data->getData('recipient')) {
            $recipient = json_decode($data->getData('recipient'), true);
        }

//        if ($recipient && $recipient['type'] == 'mail') {
        if ($recipient) {
            $transport = $this->_transportBuilder->setTemplateIdentifier('groupon_gift_template')->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->_storeManager->getStore()->getId(),
                ]
            )->setTemplateVars(
                [
                    'store' => $this->_storeManager->getStore(),
                    'store URL' => $this->_storeManager->getStore()->getBaseUrl(),
                    'groupon_code' => $data->getGrouponCode(),
                    'message' => $recipient['message'],
                ]
            )->setFrom(
                [
                    'email' => $recipient['mail'],
                    'name' => $recipient['name']
                ]
            )->addTo(
                $this->getCustomerEmail(),
                $this->getCustomerName()
            )->createAttachment($file)->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

            $data->setData('is_sent', 1);
            $data->save();
        }

        if (!$recipient) {
            $transport = $this->_transportBuilder->setTemplateIdentifier('groupon_mail_template')->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->_storeManager->getStore()->getId(),
                ]
            )->setTemplateVars(
                [
                    'store' => $this->_storeManager->getStore(),
                    'store URL' => $this->_storeManager->getStore()->getBaseUrl(),
                    'groupon_code' => $data->getGrouponCode(),
                    'create' => $data->getCreatedAt(),
                    'qty' => $data->getQty(),
                ]
            )->setFrom(
                [
                    'email' => $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER),
                    'name' => $this->_scopeConfig->getValue(self::XML_PATH_NAME_SENDER)
                ]
            )->addTo(
                $this->getCustomerEmail(),
                $this->getCustomerName()
            )->createAttachment($file)->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        }
    }

    /**
     * @param string $code
     * @param $vendorId
     * @return string
     * @throws \Exception
     */
    public function redeem($code = '', $vendorId)
    {
        if ($this->getData('groupon_id') && @$this->getData('status')) {
            if ($this->getData('status') == self::STATUS_AVAILABLE) {
                //available
                $redeem = $this->redeemCoupon();
                if ($redeem) {
                    return $this->saveLog($this->getRedemptionCode(), self::STATUS_REDEEMED_TEXT, __("Voucher is valid for ") . $this->getData('product_name') . '.', $vendorId);
                } else {
                    return $this->saveLog($this->getRedemptionCode(), self::STATUS_EXPIRED_TEXT, __("Voucher was expired."), $vendorId);
                }
            } elseif ($this->getData('status') == self::STATUS_USED) {
                //used
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_USED_TEXT, __("Voucher was previously used on ") . @$this->getData('redeemed_at') . '.', $vendorId);
            } elseif ($this->getData('status') == self::STATUS_EXPIRED) {
                //expired
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_EXPIRED_TEXT, __("Voucher was expired."), $vendorId);
            } else {
                //error
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_NOTFOUND_TEXT, __("Voucher is not valid."), $vendorId);
            }
        }
        return $this->saveLog($code, self::STATUS_NOTFOUND_TEXT, __("Voucher is not valid."), $vendorId);
    }

    /**
     * @throws \Exception
     */
    public function redeemCoupon()
    {
        if ($this->getData('groupon_id') && @$this->getData('status')) {
            if ($this->getData('status') == self::STATUS_AVAILABLE) {
                //available
                $parentId = $this->_dealFactory->create()->load($this->getProductId(), 'product_id')->getParentId();
                $dealEndTime = $this->_dealFactory->create()->load($parentId, 'product_id')->getGrouponExpire();
                if (time() <= strtotime($dealEndTime) + 86400) {
                    $this->setData('status', self::STATUS_USED);
                    $this->setData('redeemed_at', date("Y-m-d H:i:s"));
                    $this->setData('updated_at', date("Y-m-d H:i:s"));
                    $this->save();

                    return true;
                } else {
                    $this->setData('status', self::STATUS_EXPIRED);
                    $this->setData('redeemed_at', date("Y-m-d H:i:s"));
                    $this->setData('updated_at', date("Y-m-d H:i:s"));
                    $this->save();

                    return false;
                }

            }
        }
        return false;
    }

    public function markAsUsed()
    {
        if ($this->getId()) {
            $this->setData('customer_status', self::CUSTOMER_STATUS_USED);
            $this->setData('updated_at', date("Y-m-d H:i:s"));
            try {
                $this->save();
            } catch (\Exception $e) {
                return false;
            }
        }

        return true;
    }

    public function loadByCode($code, $vendorId)
    {
        $this->unsetData();
        $this->load($code, 'redemption_code');
        if ($this->getVendorId() !== $vendorId)
        {
            $this->unsetData();
        }
        return $this;
    }

    public function needLocationConfirmation()
    {
        if ($redemptionLocationId = $this->getRedemptionLocationId()) {
            $redemptionLocation = ObjectManager::getInstance()->create(Location::class)->load($redemptionLocationId);
            return [
                'confirmLocation' => true,
                'message' => "This coupon code must be redeemed at: {$redemptionLocation->getLocationName()}" .
                    " ({$redemptionLocation->getStreet()}, " .
                    " {$redemptionLocation->getCity()}," .
                    " {$redemptionLocation->getRegion()}," .
                    " {$redemptionLocation->getCountry()}," .
                    " {$redemptionLocation->getPhone()})" .
                    " Confirm redeem this coupon?"
            ];
        }
        return false;
    }

    protected function saveLog($code, $status, $mess = '', $vendorId)
    {
        $log = ObjectManager::getInstance()->create(Redemption::class);
        $log->setData('code', @$code);
        $log->setData('vendor_id', $vendorId);
        $log->setData('status', $status);
        $log->setData('message', $mess);
        $log->setRedeemedAt(date('Y-m-d H:i:s', time()));
        $log->save();
        return json_encode($log->getData());
    }

    public static function getStatusLabelByCode($code)
    {
        switch ($code) {
            case self::STATUS_AVAILABLE:
                return "AVAILABLE";
            case self::STATUS_USED:
                return "USED";
            case self::STATUS_EXPIRED:
                return "EXPIRED";
            case self::STATUS_CANCELED:
                return "CANCELED";
            default :
                return "ERROR";
        }
    }

}
