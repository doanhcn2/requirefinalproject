<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Location
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getDeliveryId()
 * @method int getProductId()
 * @method string getAddress()
 * @method int getDistance()
 * @method string getNote()
 *
 */
class Delivery extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Delivery');
    }
}
