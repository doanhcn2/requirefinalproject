<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model;

use Magenest\Groupon\Helper\Data;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Deal
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getDealId()
 * @method int getProductId()
 * @method int getStoreId()
 * @method int getParentId()
 * @method string getProductType()
 * @method string getStartTime()
 * @method string getEndTime()
 * @method string getRedeemableAfter()
 * @method string getReminderTemplate()
 * @method int getVendorId()
 * @method int getMinimumBuyersLimit()
 * @method int getMaximumBuyersLimit()
 * @method int getEnable()
 * @method string getAllowCreditPay()
 * @method int getViewCount()
 * @method int getPurchasedCount()
 * @method int getStatus()
 * @method string getLocation()
 * @method string getGrouponExpire()
 * @method int getPriority()
 * @method int getUserLimit()
 * @method int getTemplateId()
 * @method int getIsBusinessAddress()
 * @method int getIsDeliveryAddress()
 * @method int getIsOnlineCoupon()
 */
class Deal extends AbstractModel
{
    protected $_idFieldName = 'deal_id';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Deal');
    }

    public function getLocationData()
    {
        $ids = explode(',', $this->getLocationIds());
        /** @var \Magenest\Groupon\Model\ResourceModel\Location\Collection $locations */
        $locations = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magenest\Groupon\Model\ResourceModel\Location\Collection::class);
        $locations->addFieldToFilter('location_id', ['in' => $ids]);

        return $locations->getData();
    }

    public function afterSave()
    {
        $ids = explode(',', $this->getLocationIds());
        $objMng = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magenest\Groupon\Model\ResourceModel\DealLocation\Collection $dealLocationCollection */
        $dealLocationCollection = $objMng->create(\Magenest\Groupon\Model\ResourceModel\DealLocation\Collection::class);
        $dealLocationCollection->addFieldToFilter('deal_id', ['eq' => $this->getId()])->walk('delete');

        if (is_array($ids)) {
            foreach ($ids as $id) {
                if (!$id) continue;
                $dealLocation = $objMng->create(\Magenest\Groupon\Model\DealLocation::class);
                $dealLocation->setData([
                    'deal_id' => $this->getId(),
                    'location_id' => $id
                ])->save();
            }
        }

        return parent::afterSave();
    }

    /**
     * Get location array to display frontend
     *
     * @return array
     */
    public function getGrouponLocationData()
    {
        $locations = $this->prepareLocationIds();
        $result = [];
        foreach ($locations as $location) {
            $locationString = Data::getGrouponLocationById($location);
            array_push($result, $locationString);
        }

        return array_unique($result);
    }

    private function prepareLocationIds()
    {
        $ids = explode(',', $this->getLocationIds());
        $locations = array_filter($ids);
        if (count($locations) === 0) {
            return [];
        }

        return $locations;
    }
}
