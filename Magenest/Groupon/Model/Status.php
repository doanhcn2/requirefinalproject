<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

class Status extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Magenest\Groupon\Model\ResourceModel\Status::class);
    }
}