<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model\Quote\Item;

use Magento\Framework\Event\Observer;
use Magento\CatalogInventory\Helper\Data;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Model\DealFactory;

/**
 * Class QuantityValidator
 * @package Magenest\Groupon\Model\Quote\Item
 */
class QuantityValidator
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * QuantityValidator constructor.
     * @param \Magento\Framework\Message\ManagerInterface $managerInterface
     * @param LoggerInterface $loggerInterface
     * @param DealFactory $dealFactory
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        LoggerInterface $loggerInterface,
        DealFactory $dealFactory
    ) {
        $this->deal = $dealFactory;
        $this->messageManager = $managerInterface;
        $this->_logger = $loggerInterface;
    }

    /**
     * Check product option event data when quote item quantity declaring
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function validate(Observer $observer)
    {
        /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
        $quoteItem = $observer->getEvent()->getItem();
        $buyInfo = $quoteItem->getBuyRequest();
        $select = $buyInfo->getSelectedConfigurableOption();
        $productId = $buyInfo->getProduct();
        if (!isset($select)) {
            $productId = $select;
        }
        if (!$quoteItem ||
            !$quoteItem->getProductId() ||
            !$quoteItem->getQuote() ||
            $quoteItem->getQuote()->getIsSuperMode()
        ) {
            return;
        }
        $qty = $buyInfo->getOriginalQty();
        if (!isset($qty)) {
            $qty = $quoteItem->getQty();
        }
//        $this->getErrorMessage($productId, $quoteItem, $qty);
    }

    /**
     * get Error if not enough qty
     * @param $type
     * @param $quoteItem
     */
    public function getErrorMessage($producId, $quoteItem, $qty)
    {

        /** @var \Magenest\Groupon\Model\Deal $model */
        $model = $this->deal->create()->load($producId, 'product_id');
        $minimum = $model->getMinimumBuyersLimit();
        $maximum = $model->getMaximumBuyersLimit();

        if ($minimum > 0 && ($qty < $minimum)) {
            $quoteItem->addErrorInfo(
                'erro_info',
                Data::ERROR_QTY,
                __('You can\'t buy less than ' . $minimum . ' item(s) this option')
            );
            return;
        }

        if ($maximum > 0 && ($qty > $maximum)) {
            $quoteItem->addErrorInfo(
                'erro_info',
                Data::ERROR_QTY,
                __('You can\'t buy more ' . $maximum . ' item(s) this option')
            );
            return;
        }
    }
}
