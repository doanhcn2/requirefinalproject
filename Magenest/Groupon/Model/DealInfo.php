<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 23/04/2018
 * Time: 09:02
 */
namespace Magenest\Groupon\Model;
use Magento\Framework\Model\AbstractModel;

class DealInfo extends AbstractModel{

    public function __construct(
      \Magento\Framework\Model\Context $context,
      \Magento\Framework\Registry $registry,
      \Magenest\Groupon\Model\ResourceModel\DealInfo $resource,
      \Magenest\Groupon\Model\ResourceModel\DealInfo\Collection $resourceCollection,
      array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection,
          $data);
    }
}