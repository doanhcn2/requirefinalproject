<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */
namespace Magenest\Groupon\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Status extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_groupon_status_history', 'status_id');
    }
}