<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 03/10/2018
 * Time: 11:10
 */

namespace Magenest\Groupon\Model\ResourceModel;


class DealLocation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_groupon_deals_locations', 'id');
    }

}