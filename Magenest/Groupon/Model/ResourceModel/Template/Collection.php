<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 */
namespace Magenest\Groupon\Model\ResourceModel\Template;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'template_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_groupon_template_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_groupon_template';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\Template', 'Magenest\Groupon\Model\ResourceModel\Template');
    }
}
