<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model\ResourceModel\Deal;

use Magenest\Groupon\Helper\Data;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\DB\Select;

class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     * @var string
     */
    protected $_idFieldName = 'deal_id';

    /**
     * Define resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\Deal', 'Magenest\Groupon\Model\ResourceModel\Deal');
    }

    /**
     * Get total count.
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }

    /**
     * Get collection size
     *
     * @return int
     * @throws \Zend_Db_Select_Exception
     */
    public function getSize()
    {
        if ($this->_totalRecords === null) {
            $sql = $this->getSelectCountSqlDeal();
            $result = $this->getConnection()->fetchAll($sql, $this->_bindParams);
            $this->_totalRecords = count($result);
        }

        return intval($this->_totalRecords);
    }

    /**
     * Get SQL for get record count
     *
     * @return Select
     * @throws \Zend_Db_Select_Exception
     */
    private function getSelectCountSqlDeal()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Select::ORDER);
        $countSelect->reset(Select::LIMIT_COUNT);
        $countSelect->reset(Select::LIMIT_OFFSET);

        return $countSelect;
    }
}
