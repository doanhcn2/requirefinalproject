<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 03/10/2018
 * Time: 11:10
 */

namespace Magenest\Groupon\Model\ResourceModel\DealLocation;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(\Magenest\Groupon\Model\DealLocation::class, \Magenest\Groupon\Model\ResourceModel\DealLocation::class);
    }
}