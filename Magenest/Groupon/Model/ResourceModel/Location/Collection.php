<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model\ResourceModel\Location;

use Magenest\Groupon\Model\Location;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\Groupon\Model\ResourceModel\Location
 */
class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'location_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\Location', 'Magenest\Groupon\Model\ResourceModel\Location');
    }

    public function getLocationFilterData(){
        $locationValues = [];
        $locationKeys   = [];
        /** @var Location $location */
        foreach ($this as $location) {
            $locationKey = $location->getGrouponLocationKey();
            if (!in_array($locationKey, $locationKeys)) {
                $locationValue = $location->getGrouponLocation();
                array_push($locationKeys, $locationKey);
                array_push($locationValues, $locationValue);
            }
        }

        return array_combine($locationKeys, $locationValues);
    }
}
