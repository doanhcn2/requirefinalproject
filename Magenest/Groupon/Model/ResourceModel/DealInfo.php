<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 23/04/2018
 * Time: 09:03
 */
namespace Magenest\Groupon\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DealInfo extends AbstractDb{

    public function _construct()
    {
        $this->_init($this->getTable('magenest_groupon_deal_info'),'info_id');
        // TODO: Implement _construct() method.
    }
}