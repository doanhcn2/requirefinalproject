<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Backend for serialized array data
 *
 */

namespace Magenest\Groupon\Model\Config\Backend\Serialized;

use Magento\Config\Model\Config\Backend\Serialized;

/**
 * @api
 * @since 100.0.2
 */
class ArraySerialized extends Serialized
{
    public function beforeSave()
    {
        $value = $this->getValue();
        $lastResults = [];
        if (is_array($value)) {
            $keyID = '';
            $endByOneData = 0;
            foreach($value as $itemOne) {
                foreach($itemOne as $keyTwo => $itemTwo) {
                    if ($keyTwo === '__empty') continue;
                    if ($keyID === '') {
                        $keyID = $keyTwo;
                    }
                    foreach($itemTwo as $keyLast => $valueLast) {
                        if ($keyLast === 'category') {
                            $category = $valueLast;
                        } elseif ($keyLast === 'discount_from') {
                            $discountFrom = $valueLast;
                        } elseif ($keyLast === 'discount_to') {
                            $discountTo = $valueLast;
                            $endByOneData = 1;
                        }
                    }
                    if ($endByOneData == 1) {
                        if (@$category && @$discountTo && @$discountFrom) {
                            $lastResults[$keyID] = ['category' => $category, 'discount_from' => $discountFrom, 'discount_to' => $discountTo];
                        }
                        unset($keyID);
                        unset($category);
                        unset($discountFrom);
                        unset($discountTo);
                        $keyID = '';
                        $endByOneData = 0;
                    }
                }
            }
        }

        if (@$lastResults && !empty($lastResults)) {
            $this->setValue($lastResults);
        } else {
            $this->setValue('');
        }

        return parent::beforeSave();
    }
}
