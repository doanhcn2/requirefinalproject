<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model\Config\Source;

use Magento\Catalog\Model\CategoryFactory;

/**
 * Options provider for countries list
 *
 * @api
 * @since 100.0.2
 */
class Category implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Countries
     *
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(CategoryFactory $categoryFactory)
    {
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     * Options array
     *
     * @var array
     */
    protected $_options;

    /**
     * Return options array
     *
     * @param boolean $isMultiselect
     * @param string|array $foregroundCountries
     * @return array
     */
    public function toOptionArray($isMultiselect = false, $foregroundCountries = '')
    {
        if (!$this->_options) {
            $categoryCollection = $this->_categoryFactory->create()->getCollection()->addFieldToSelect(['name']);
            $categories = [];
            foreach($categoryCollection as $item) {
                if (count(explode('/', $item->getPath())) > 1 && count(explode('/', $item->getPath())) < 6) {
                    $category = ['value' => $item->getId(), 'label' => addslashes($item->getName())];
                    array_push($categories, $category);
                }
            }
            $this->_options = $categories;
        }

        $options = $this->_options;
        if (!$isMultiselect) {
            array_unshift($options, ['value' => '', 'label' => '--Please Select--']);
        }

        return $options;
    }
}
