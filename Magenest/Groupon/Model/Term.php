<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Term
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getTermId()
 * @method string getLabel()
 * @method string getContent()
 *
 */
class Term extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Term');
    }
}
