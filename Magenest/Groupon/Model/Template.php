<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Template
 * @package Magenest\Groupon\Model
 *
 * @method int getTemplateId()
 * @method int getEnable()
 * @method string getTitle()
 * @method string getPdfCoordinates()
 * @method int getPdfPageWidth()
 * @method int getPdfPageHeight()
 * @method string getPdfBackground()
 */
class Template extends AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_groupon_template';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_groupon_template';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_groupon_template';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Template');
    }
}
