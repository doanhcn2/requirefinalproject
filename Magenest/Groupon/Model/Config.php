<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

/**
 * Class Configuration
 * @package Magenest\Ticket\Model
 */
class Config
{
    const XML_PATH_PATTERN_CODE = 'groupon/general_config/pattern_code';
    const XML_PATH_GOOGLE_API = 'groupon/general_config/google_api_key';
    const XML_PATH_EMAIL = 'groupon/email_config/email';
    const XML_PATH_TEMPLATE = 'groupon/vendor_config/template';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Connector constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getPatternCode()
    {
        $pattern =  $this->_scopeConfig->getValue(self::XML_PATH_PATTERN_CODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $pattern;
    }

    /**
     * @return mixed
     */
    public function getGooleApi()
    {
        $api =  $this->_scopeConfig->getValue(self::XML_PATH_GOOGLE_API, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $api;
    }

    /**
     * @return mixed
     */
    public function getConfigEmail()
    {
        $emailConfig =  $this->_scopeConfig->getValue(self::XML_PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $emailConfig;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        $template =  $this->_scopeConfig->getValue(self::XML_PATH_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $template;
    }
}
