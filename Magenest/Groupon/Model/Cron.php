<?php

namespace Magenest\Groupon\Model;

class Cron {

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\ObjectManagerInterface */
    protected $objectManager;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\ObjectManagerInterface
        $objectManager
    ) {
        $this->logger = $logger;
        $this->objectManager = $objectManager;
    }

    public function checkStatus() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->get('Magenest\Groupon\Helper\Status')->changeStatus();
    }
}