<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model\Deal;

use Magenest\Groupon\Helper\Coupon;
use Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory;

/**
 * Class DataProvider
 * @package Magenest\Groupon\Model\Deal
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * This is default eav attribute of product name, campaign status, need to change if project change it to sth new
     * @todo Change when needed (Sam)
     */
    const NAME_PRODUCT_EAV_ID = 73;
    const CAMPAIGN_STATUS_EAV_ID = 214;

    /**
     * @var \Magenest\Groupon\Model\DealInfoFactory
     */
    protected $_dealInfo;

    /**
     * @var $_grouponFactory \Magenest\Groupon\Model\GrouponFactory
     */
    protected $_grouponFactory;

    /**
     * @var $collection \Magenest\Groupon\Model\ResourceModel\Deal\Collection
     */
    protected $collection;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    protected $defined;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    protected $_filtered;

    /**
     * @var $_eventHelper \Magenest\Ticket\Helper\Event
     */
    protected $_eventHelper;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\DealInfoFactory $dealInfoFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Ticket\Helper\Event $eventHelper,
        array $meta = [],
        array $data = []
    ) {
        $this->_eventHelper = $eventHelper;
        $this->_locationFactory = $locationFactory;
        $this->_filtered = [];
        $this->_dealInfo = $dealInfoFactory;
        $this->_grouponFactory = $grouponFactory;
        $this->_dealFactory = $dealFactory;
        $this->_productFactory = $productFactory;
        $this->collection = $collectionFactory->create();
        $this->defined = false;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getCollection()
    {
        if (!$this->defined) {
            $collection = parent::getCollection();
            $collection->addFieldToFilter('product_type', 'configurable');

            $udropshipVendorTable = $collection->getTable('udropship_vendor');
            $collection->getSelect()->joinLeft(
                ['uv' => $udropshipVendorTable],
                "main_table.vendor_id = uv.vendor_id",
                ['vendor_name' => "uv.vendor_name"]
            );
            $collection->getSelect()->joinLeft(
                ["cpev" => $collection->getTable('catalog_product_entity_varchar')],
                "cpev.attribute_id  = " . self::NAME_PRODUCT_EAV_ID . " AND cpev.row_id = main_table.product_id",
                ["product_name" => "cpev.value"]
            );
            $collection->getSelect()->joinLeft(
                ["cpev2" => $collection->getTable('catalog_product_entity_varchar')],
                "cpev2.attribute_id  = " . self::CAMPAIGN_STATUS_EAV_ID . " AND cpev2.row_id = main_table.product_id",
                ["campaign_status" => "cpev2.value"]
            );
            $collection->getSelect()->joinLeft(
                ['mgdi' => $collection->getTable('magenest_groupon_deal_info')],
                "mgdi.product_id = main_table.product_id",
                ["number_purchased" => "mgdi.sold", "total_sales" => "mgdi.revenue", "total_commission" => "mgdi.commissions"]
            );
            $collection->getSelect()->joinLeft(
                ['mgdl' => $collection->getTable('magenest_groupon_deals_locations')],
                "`mgdl`.`deal_id` = `main_table`.`deal_id`",
                []
            );
            $collection->getSelect()->joinLeft(
                ['mgl' => $collection->getTable('magenest_groupon_location')],
                "`mgdl`.`location_id` = `mgl`.`location_id`",
                [
                    'locations' => 'GROUP_CONCAT(CONCAT(COALESCE(`mgl`.`city`, ""), " ", COALESCE(`mgl`.`region`, "") , " ", COALESCE(`mgl`.`country`, "")))'
                ]
            );
//            $collection->getSelect()->joinLeft(
//                ['l' => $collection->getTable('magenest_groupon_location')],
//                "FIND_IN_SET(l.location_id, main_table.location_ids)",
//                ['locations' => 'GROUP_CONCAT(CONCAT(COALESCE(l.city, ""), " ", COALESCE(l.region, ""), " ", COALESCE(l.country, "")))']
//            );
            $collection->getSelect()->group('main_table.deal_id');
            $this->defined = true;

            return $collection;
        } else {
            return parent::getCollection();
        }
    }

    public function getData()
    {
        $collectionDates = parent::getData();
        $result = [];
        foreach ($collectionDates['items'] as $data) {
            unset($sold);
            unset($revenue);
            unset($commission);
            $productId = $data['product_id'];
            $chills = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $productId)->getColumnValues('product_id');
            $grouponCollection = $this->_grouponFactory->create()->getCollection()->addFieldToFilter('product_id', ['in' => $chills]);
            $sold = array_sum($grouponCollection->getColumnValues('qty'));
            $dealInfo = $this->_dealInfo->create()->load($productId, 'product_id');
            if (!$dealInfo->getId() || (int)$sold !== (int)$dealInfo->getSold()) {
                $revenue = array_sum($grouponCollection->getColumnValues('price'));
                $commission = array_sum($grouponCollection->getColumnValues('commission'));
                Coupon::reloadDealData($productId, null, $sold, $revenue, $commission);
            }
            if ($data['locations']) {
                $data['locations'] = preg_replace('/(?!\s),(?=\S)/', ', ', trim($data['locations'], ', '));
            }
            $data['number_purchased'] = $sold;
            $data['total_sales'] = isset($revenue) ? number_format($revenue, 2) : (number_format((float)$data['total_sales'], 2));
            $data['total_commission'] = isset($commission) ? number_format($commission, 2) : (number_format((float)$data['total_commission'], 2));
            array_push($result, $data);
        }

        return ['totalRecords' => $collectionDates['totalRecords'], 'items' => $result];
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        $field = $filter->getField();
        if ($this->isFilterAllow($field)) {
            array_push($this->_filtered, $field);
            if ($field === 'number_purchased') {
                $filter->setField('mgdi.sold');
            } elseif ($field === 'total_sales') {
                $filter->setField('mgdi.revenue');
            } elseif ($field === 'total_commission') {
                $filter->setField('mgdi.commissions');
            } elseif ($field === 'vendor_name') {
                $filter->setField('uv.vendor_name');
            } elseif ($field === 'product_name') {
                $filter->setField('cpev.value');
            } elseif ($field === 'campaign_status') {
                $filter->setField('cpev2.value');
            } elseif ($field === 'locations') {
//                $condition = [$filter->getConditionType() => $filter->getValue()];
//                $this->getCollection()->addFieldToFilter(
//                    [
//                        'mgl.city',
//                        'mgl.region',
//                        'mgl.country',
//                        'mgl.deal_id'
//                    ], [
//                        $condition,
//                        $condition,
//                        $condition,
//                        ['eq' => '`main_table`.`deal_id`']
//                    ]
//                );
                $this->getCollection()->getSelect()->having("`{$field}` {$filter->getConditionType()} ?", $filter->getValue());

                return;
            } else {
                $filter->setField('main_table.' . $field);
            }

            return parent::addFilter($filter);
        }
    }

    protected function isFilterAllow($field)
    {
        $counting = array_count_values($this->_filtered);
        if (in_array($field, ['deal_id', 'number_purchased', 'total_sales', 'total_commission', 'start_time', 'end_time', 'groupon_expire'])) {
            if (isset($counting[$field]) && $counting[$field] >= 2) return false;
        } elseif (in_array($field, ['vendor_name', 'product_name', 'campaign_status'])) {
            if (isset($counting[$field]) && $counting[$field] >= 1) return false;
        }

        return true;
    }
}
