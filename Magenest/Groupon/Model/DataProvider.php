<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

/**
 * Class DataProvider
 * @package Magenest\Groupon\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * DataProvider constructor.
     * @param TemplateFactory $templateFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        \Magenest\Groupon\Model\TemplateFactory $templateFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $templateFactory->create()->getCollection();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $data = $this->_loadedData;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get('Magento\Framework\App\RequestInterface');
        $id = $request->getParam('template_id');
        if($id){
            $model = $objectManager->create('Magenest\Groupon\Model\Template')->load($id);
            $data[$id]= $model->getData();
            $data[$id]['pdf_coordinates'] = unserialize($data[$id]['pdf_coordinates']);
            $data[$id]['pdf_background'] =unserialize($data[$id]['pdf_background']);
        }

        return $data;
    }
}
