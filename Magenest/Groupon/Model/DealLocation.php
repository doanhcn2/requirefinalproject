<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 03/10/2018
 * Time: 11:05
 */

namespace Magenest\Groupon\Model;


class DealLocation extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\ResourceModel\DealLocation $resource,
        \Magenest\Groupon\Model\ResourceModel\DealLocation\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}