<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Location
 * @package Magenest\Groupon\Model
 * @method int getLocationId()
 * @method int getProductId()
 * @method string getDealId()
 * @method string getStatus()
 * @method string getStreet()
 * @method string getStreetTwo()
 * @method string getCity()
 * @method string getCountry()
 * @method string getRegion()
 * @method string getPostcode()
 * @method string getPhone()
 */
class Location extends AbstractModel
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Location');
    }

    public function getGrouponLocationKey()
    {
        $locationDetails = "";
        if ($this->getRegion()) {
            $locationDetails .= $this->getRegion() . "||";
        } elseif ($this->getCity()) {
            $locationDetails .= $this->getCity() . "||";
        }
        if ($this->getCountry()) {
            $locationDetails .= $this->getCountry();
        }

        return $locationDetails;
    }

    public function getGrouponLocation()
    {
        $locationDetails = "";
        if ($this->getRegion()) {
            $locationDetails .= $this->getRegion() . ", ";
        } elseif ($this->getCity()) {
            $locationDetails .= $this->getCity() . ", ";
        }
        if ($this->getCountry()) {
            $locationDetails .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($this->getCountry());
        }

        return $locationDetails;
    }
}
