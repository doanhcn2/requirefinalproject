<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Ticket
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getTicketId()
 * @method int getProductId()
 * @method string getTerms()
 * @method string getInstructions()
 * @method string getCoordinates()
 * @method string getPageWidth()
 * @method string getPageHeight()
 * @method string getBackground()
 */
class Ticket extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Ticket');
    }
}
