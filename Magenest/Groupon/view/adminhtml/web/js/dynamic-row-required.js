define([
    'jquery',
    'ko',
    'Magento_Ui/js/dynamic-rows/dynamic-rows',
    'uiRegistry'
], function ($, ko, Component, uiRegistry) {
    return Component.extend({
        defaults: {
            'listens': {
                '${ $.provider }:${ $.parentScope }.info': 'hideRotate setOldValue'
            }
        },

        oldValue: '',

        initialize: function () {
            var self = this;
            this._super();
            uiRegistry.async(this.name)(function () {
                var value = uiRegistry.get(self.provider).get(self.parentScope + '.info');
                self.hideRotate.apply(self, [value]);
                self.setOldValue.apply(self, [value]);
            });
            return this;
        },

        hideRotate: function (value) {
            if (value === this.getOldValue()) {
                return;
            }
            var visible;
            if (value === 'bar_code') {
                visible = true;
                this.reload();
            } else {
                visible = false;
            }

            if (this.childTemplate) {
                this.childTemplate.children.rotate.visible = visible;
                this.childTemplate.children.x2.visible = visible;
                this.childTemplate.children.x2.disabled = !visible;
                this.childTemplate.children.y2.visible = visible;
                this.childTemplate.children.y2.disabled = !visible;

                this.childTemplate.children.length.visible = !visible;
                this.childTemplate.children.length.disabled = visible;
                this.childTemplate.children.color.visible = !visible;
                this.childTemplate.children.color.disabled = visible;
                this.childTemplate.children.size.visible = !visible;
                this.childTemplate.children.size.disabled = visible;

                if (value === 'bar_code' || this.getOldValue() === '' || this.getOldValue() === 'bar_code') {
                    this.reload();
                }
            }
        },

        setOldValue: function (value) {
            this.oldValue = value;
        },

        getOldValue: function () {
            return this.oldValue;
        },
        /**
         * Handler for deleteRecord event
         *
         * @param {Number|String} index - element index
         * @param {Number|String} id
         */
        deleteHandler: function (index, id) {
            var defaultState;
            if (this.checkRowCount()) {
                this.setDefaultState();
                defaultState = this.defaultPagesState[this.currentPage()];
                this.processingDeleteRecord(index, id);
                this.pagesChanged[this.currentPage()] =
                    !compareArrays(defaultState, this.arrayFilter(this.getChildItems()));
                this.changed(_.some(this.pagesChanged));
            }
        },

        checkRowCount: function () {
            var count;
            count = this.recordData().length;
            return count > 1;
        }
    });
});