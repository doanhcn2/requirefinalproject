define([
    'jquery',
    'ko',
    'uiComponent',
    'mageUtils',
    'Magento_Ui/js/modal/modal'
], function($, ko, Component, utils) {
    return Component.extend({
        defaults: {
            template: 'Magenest_Groupon/grid/product/campaign-status-comment',
            comment: '',
            statusChanged: false,
            initialStatus: 2,
            links: {
                comment: '${ $.provider }:data.product.status_comment',
                initialStatus: 'index=campaign_status:initialValue',
                statusChanged: '${ $.provider }:data.product.campaign_status_changed'
            },
            listens: {
                'index=campaign_status:value': 'openCommentModal'
            }
        },

        modal: null,
        action: null,
        selections: null,

        initObservable: function() {
            this._super().observe(['comment', 'statusChanged', 'initialStatus']);
            return this;
        },

        initModal: function() {
            var self = this;
            self.modal = $('#campaign-status-comment-modal').modal({
                type: 'popup',
                clickableOverlay: false,
                title: 'Add Comment to Status Update',
                buttons: [
                    {
                        text: 'Save',
                        class: 'primary',
                        click: function (event) {
                            this.closeModal(event);
                        }
                    }
                ]
            });
        },

        openCommentModal: function(campaignStatus) {
            if (campaignStatus != this.initialStatus()) {
                this.statusChanged(true);
            } else {
                this.statusChanged(false);
            }
            this.modal.modal('openModal');
        }
    });
});