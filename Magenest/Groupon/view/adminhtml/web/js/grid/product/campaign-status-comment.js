define([
    'jquery',
    'ko',
    'uiComponent',
    'mageUtils',
    'Magento_Ui/js/modal/modal'
], function($, ko, Component, utils) {
    return Component.extend({
        defaults: {
            template: 'Magenest_Groupon/grid/product/campaign-status-comment',
            comment: ''
        },

        modal: null,
        action: null,
        selections: null,

        initObservable: function() {
            this._super().observe('comment');
            return this;
        },

        initModal: function() {
            var self = this;
            self.modal = $('#campaign-status-comment-modal').modal({
                type: 'popup',
                clickableOverlay: false,
                title: 'Add Comment to Status Update',
                buttons: [
                    {
                        text: 'Submit',
                        class: 'primary',
                        /**
                         * Default action on button click
                         */
                        click: function (event) {
                            self.submit(false);
                            this.closeModal(event);
                        }
                    },
                    {
                        text: 'Skip',
                        class: '',
                        /**
                         * Default action on button click
                         */
                        click: function (event) {
                            self.submit(true);
                            this.closeModal(event);
                        }
                    }
                ]
            });
        },

        openCommentModal: function(action, selections) {
            this.action = action;
            this.selections = selections;
            this.modal.modal('openModal');
        },

        submit: function (skipComment) {
            var action = this.action,
                data = this.selections;
            var itemsType = data.excludeMode ? 'excluded' : 'selected',
                selections = {};
            var comment = this.comment();
            var url = skipComment ? action.url : action.url+'?comment='+encodeURIComponent(comment);
            selections[itemsType] = data[itemsType];

            if (!selections[itemsType].length) {
                selections[itemsType] = false;
            }

            _.extend(selections, data.params || {});

            utils.submit({
                url: url,
                data: selections
            });
        }
    });
});