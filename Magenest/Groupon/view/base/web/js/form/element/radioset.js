define([
    'underscore',
    'mageUtils',
    'Magento_Ui/js/form/element/checkbox-set'
], function (_, utils, RadioSet) {
    'use strict';

    return RadioSet.extend({
        defaults: {
            template: 'ui/form/element/checkbox-set',
            multiple: false,
            multipleScopeValue: null
        },

        /**
         * @inheritdoc
         */
        initConfig: function () {
            var options = [];
            this._super();

            this.value = this.normalizeData(this.value);

            _.each(this.options, function(value) {
                options.push(value);
            });
            this.options = options;
            return this;
        }
    });
});
