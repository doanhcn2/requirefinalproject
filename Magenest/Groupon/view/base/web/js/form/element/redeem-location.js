define([
    'Magenest_Groupon/js/form/element/single-checkbox',
    'ko'
], function (SingleCheckbox, ko) {
    'use strict';

    return SingleCheckbox.extend({
        is_online_coupon: ko.observable(false),

        initialize: function() {
            this._super();
            this.is_online_coupon.subscribe(this.showFieldBaseOnOnlineCoupon.bind(this));
            this.showFieldBaseOnOnlineCoupon(this.is_online_coupon());
            return this;
        },

        showFieldBaseOnOnlineCoupon: function(is_online) {
            if (is_online) {
                this.visible(false);
                this.checked(false);
                this.disabled(true);
            } else {
                this.visible(true);
                this.disabled(false);
            }
        }
    });
});
