define([
    'Magento_Ui/js/form/element/abstract',
    'mageUtils'
], function (Abstract, utils) {
    'use strict';

    return Abstract.extend({
        defaults: {
            visible: false
        },

        showPriority: function(data) {
            if (data==1) {
                this.visible(true);
            } else {
                this.visible(false);
            }
        }
    });
});
