define([
    'Magento_Ui/js/form/element/single-checkbox',
    'ko'
], function (SingleCheckbox, ko) {
    'use strict';

    return SingleCheckbox.extend({
        defaults: {
            templates: {
                checkbox: 'Magenest_Groupon/form/components/single/checkbox'
            },
            rendered: false
        },

        initObservable: function() {
            this._super().observe('rendered');
            return this;
        },

        afterRender: function() {
            this.rendered(true);
        }
    });
});
