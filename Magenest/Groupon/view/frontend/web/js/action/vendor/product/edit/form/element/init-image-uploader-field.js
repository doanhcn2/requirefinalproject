define([
    'jquery',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/add-save-cancel-buttons'
], function ($, addSaveCancelButtons) {
    var inputSelectors = "button#add_video_button, div.image-placeholder:has(div.uploader), div.draggable-handle, button.action-delete";
    return function (excludedInputElements) {
        $("ul.form-list").has("li#media_gallery-container").each(function () {
            $(this).children('li').each(function () {
                addSaveCancelButtons(this, inputSelectors);
            });
        });
    };
});