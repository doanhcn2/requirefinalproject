define([
    'jquery',
    'uiComponent',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/init-block-input-text',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/init-block-input-checkbox',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/init-image-uploader-field',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/hide-elements'
], function ($, uiComponent, initBlockInputTextForm, initBlockInputCheckbox, initImageUploaderField, hideElements) {
    var excludedInputElements = "input#redeem_immediate, input#redeemable_after_date, input#admin_period, input#vendor_period_start";
    return uiComponent.extend({
        initialize: function () {
            this._super();
            return this;
        },
        initInputFields: function (dependedFieldRendered) {
            $('body').trigger('processStart');
            if (dependedFieldRendered) {
                initBlockInputTextForm(excludedInputElements);
                initImageUploaderField(excludedInputElements);
                initBlockInputCheckbox(excludedInputElements);
                hideElements(excludedInputElements).hide();
            }
            $('body').trigger('processStop');
            return this;
        }
    });
});