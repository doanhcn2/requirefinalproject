define([
    'jquery'
], function ($) {
    return function (elementSelectors) {
        return {
            hide: function() {
                $(elementSelectors).hide();
                $(elementSelectors).each(function () {
                    var id = $(this).attr("id");
                    if (typeof id != "undefined") {
                        $("label[for='" + id + "']").hide();
                    }
                });
            },
            show: function() {
                $(elementSelectors).show();
                $(elementSelectors).each(function () {
                    var id = $(this).attr("id");
                    if (typeof id != "undefined") {
                        $("label[for='" + id + "']").show();
                    }
                });
            },
            showLabel: function() {
                $(elementSelectors).each(function () {
                    var id = $(this).attr("id");
                    if (typeof id != "undefined") {
                        $("label[for='" + id + "']").show();
                    }
                });
            },
            toggle: function() {
                $(elementSelectors).toggle();
            },
            toggleLabel: function() {
                $(elementSelectors).each(function () {
                    var id = $(this).attr("id");
                    if (typeof id != "undefined") {
                        $("label[for='" + id + "']").toggle();
                    }
                });
            },
            hideKeepLabel: function() {
                $(elementSelectors).hide();
            }
    }
    };
});