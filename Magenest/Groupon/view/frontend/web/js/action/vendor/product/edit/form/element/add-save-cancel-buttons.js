define([
    'jquery',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/hide-elements'
], function ($, hideElements) {
    function getCancelButton(element) {
        return $(element).find(".campaign-input-cancel");
    }

    function getSaveButton(element) {
        return $(element).find(".campaign-input-save");
    }

    function getCollapsibleButton(element) {
        return $(element).find(".campaign-input-collapsible");
    }

    function showSaveButtons(element) {
        getCancelButton(element).show();
        getSaveButton(element).show();
    }

    function hideSaveButtons(element) {
        getCancelButton(element).hide();
        getSaveButton(element).hide();
    }

    function toggleSaveButtons(element) {
        getCancelButton(element).toggle();
        getSaveButton(element).toggle();
    }

    function getInputFields(element, inputSelectors, excludedInputElements) {
        return $(element).find(inputSelectors).not(excludedInputElements);
    }

    function getInputValueFields(element) {
        return $(element).find(".campaign-input-value");
    }

    function toggleLabels(element, inputSelectors, excludedInputElements) {
        getInputFields(element, inputSelectors, excludedInputElements).each(function () {
            if ($(this).attr('type') == 'checkbox') {
                hideElements(this).toggleLabel();
                if (typeof ($(this).attr('checked')) !== "undefined") {
                    hideElements(this).showLabel();
                }
            }
        });
    }

    function showInputs(element, inputSelectors, excludedInputElements) {
        getInputFields(element, inputSelectors, excludedInputElements).show();
        getInputValueFields(element).hide();
    }

    function hideInputs(element, inputSelectors, excludedInputElements) {
        getInputFields(element, inputSelectors, excludedInputElements).hide();
        getInputValueFields(element).show();
        toggleLabels(element, inputSelectors, excludedInputElements);
    }

    function toggleInputs(element, inputSelectors, excludedInputElements, toggleInputValues) {
        getInputFields(element, inputSelectors, excludedInputElements).toggle();
        if (toggleInputValues === true) {
            getInputValueFields(element).toggle();
        }
        toggleLabels(element, inputSelectors, excludedInputElements);
    }

    return function (element, inputSelectors, excludedInputElements) {
        if ($(element).find('.campaign-input-collapsible').length > 0) {
            getCollapsibleButton(element).on('click', onCollapsibleClick);
            getCancelButton(element).on('click', onCancelClick);
            getSaveButton(element).on('click', onSaveClick);
        } else {
            $(element)
                .append(
                    $("<span></span>")
                        .addClass("campaign-input-collapsible")
                        .text("Collapsible")
                        .on('click', function (e) {
                            $(element).toggleClass('editing');
                            onCollapsibleClick.apply(this, [e, true, true]);
                        })
                ).append(
                $("<span></span>")
                    .addClass("campaign-input-save")
                    .text("Save")
                    .on('click', function (e) {
                        $(element).toggleClass('editing');
                        onSaveClick.apply(this, [e, true, true]);
                    })
            ).append(
                $("<span></span>")
                    .addClass("campaign-input-cancel")
                    .text("Cancel")
                    .on('click', function (e) {
                        $(element).toggleClass('editing');
                        onCancelClick.apply(this, [e, true, true]);
                    })
            );
        }
        hideInputs(element, inputSelectors, excludedInputElements);
        hideSaveButtons(element);
        setOriginValues();

        var originValues = [];

        function setOriginValues() {
            originValues = [];
            getInputFields(element, inputSelectors, excludedInputElements).each(function () {
                originValues.push($(this).val());
            });
        }

        function resetOriginValues() {
            getInputFields(element, inputSelectors, excludedInputElements).each(function () {
                $(this).val(originValues.shift());
            });
        }

        function setNewValues() {
            getInputFields(element, inputSelectors, excludedInputElements).each(function () {
                var value = $(this).val();
                var inputFieldName = $(this).attr('name') || $(this).attr('id');
                var inputValueField = $("span[inputValueFor='" + inputFieldName + "'");
                if (inputValueField.length > 0) {
                    inputValueField.text(value ? value : '');
                }
            });
        }

        function addNewInputValueFields() {
            getInputFields(element, inputSelectors, excludedInputElements).each(function () {
                if ($(this).attr('type') !== 'checkbox' && $(this).attr('type') !== 'button') {
                    var inputFieldName = $(this).attr('name') || $(this).attr('id');
                    if (inputFieldName != undefined) {
                        if ($("span[inputValueFor='" + inputFieldName + "'").length == 0) {
                            var value = $(this).val();
                            $(this).after(
                                $("<span></span>")
                                    .addClass("campaign-input-value campaign-input-text-value")
                                    .attr('inputValueFor', $(this).attr('name') || $(this).attr('id'))
                                    .text(value ? value : '')
                            );
                        }
                    }
                }
            });
        }

        function onSaveClick() {
            addNewInputValueFields();
            setNewValues();
            setOriginValues();
            hideInputs(element, inputSelectors, excludedInputElements);
            hideSaveButtons(element);
        }

        function onCancelClick() {
            addNewInputValueFields();
            resetOriginValues();
            setOriginValues();
            hideInputs(element, inputSelectors, excludedInputElements);
            hideSaveButtons(element);
        }

        function onCollapsibleClick(e, toggleSave, toggleInputValues) {
            addNewInputValueFields();
            setOriginValues();
            setNewValues();
            toggleInputs(element, inputSelectors, excludedInputElements, toggleInputValues);
            if (typeof toggleSave != "undefined" && toggleSave === true) {
                toggleSaveButtons(element);
            }
        }
    };
});