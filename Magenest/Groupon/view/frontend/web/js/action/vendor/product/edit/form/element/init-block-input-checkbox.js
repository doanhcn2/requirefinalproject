define([
    'jquery',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/add-save-cancel-buttons',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/hide-elements'
], function ($, addSaveCancelButtons, hideElements) {
    var inputSelectors = "input[type='checkbox']";
    return function (excludedInputElements) {
        $("ul.form-list").each(function () {
            var isOptionFields = $(this).find(".cfg-quick-create-item").length > 0;
            $(this).children('li').not('#media_gallery-container').each(function () {
                var inputContainer = this;
                if (!isOptionFields) {
                    addSaveCancelButtons(inputContainer, inputSelectors, excludedInputElements);
                }
            });
            if (isOptionFields) {
                addSaveCancelButtons($(this).closest('.generic-box'), inputSelectors, excludedInputElements);
            }
        });
    };
});