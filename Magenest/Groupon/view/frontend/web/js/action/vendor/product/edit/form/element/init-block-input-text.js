define([
    'jquery',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/add-save-cancel-buttons',
    'Magenest_Groupon/js/action/vendor/product/edit/form/element/hide-elements'
], function ($, addSaveCancelButtons, hideElements) {
    var inputSelectors = "input[type!='hidden'][type!='file'][type!='checkbox'], textarea, button#fa_cfg_quick_create_add, select[id!='product_categories'], div#option_extra_fields";
    return function (excludedInputElements) {
        $("ul.form-list").each(function () {
            var isOptionFields = $(this).find(".cfg-quick-create-item").length > 0;
            $(this).children('li').not('#media_gallery-container').each(function () {
                var inputContainer = this;
                $(this).find(inputSelectors).not(excludedInputElements).each(function () {
                    if ($(this).attr('type') !== 'button') {
                        var value = $(this).val();
                        $(this).after(
                            $("<span></span>")
                                .addClass("campaign-input-value campaign-input-text-value")
                                .attr('inputValueFor', $(this).attr('name') || $(this).attr('id'))
                                .text(value ? value : '')
                        );
                    }
                });
                if (!isOptionFields) {
                    addSaveCancelButtons(inputContainer, inputSelectors, excludedInputElements);
                }
            });
            if (isOptionFields) {
                addSaveCancelButtons($(this).closest('.cfg-quick-create-data'), inputSelectors, excludedInputElements);
            }
        });
    };
});