define([
    'jquery',
    'priceUtils',
    'mage/url'
], function ($, priceUtils, url) {
    'use strict';

    var iScrollPos = 0;

    function getPriceBox(sp, op, bought, product_id) {
        var opt, spt;
        var format = {
            decimalSymbol: ".",
            groupLength: 3,
            groupSymbol: ",",
            integerRequired: 1,
            pattern: "$%s",
            precision: 2,
            requiredPrecision: 2
        };
        var discount = Math.ceil(100 * (parseFloat(op) - parseFloat(sp)) / parseFloat(op));
        opt = priceUtils.formatPrice(op, format);
        spt = priceUtils.formatPrice(sp, format);
        var display = "";
        if (bought <= 0) {
            display = "style='display: none;'";
        }
        var discountHtml = '';
        var displaySpecialPrice = '';
        if (discount > 0) {
            discountHtml += '<span class="option-discount-value">' + discount + '% OFF</span>';
        } else {
            discountHtml += '<span class="option-discount-value passive"></span>';
            displaySpecialPrice += 'style="display: none;"';
        }
        var result = '<div class="price-box price-final_price for-option">' +
            discountHtml +
            '   <span class="old-price" ' + displaySpecialPrice + '>' +
            '       <span class="price-container price-final_price tax weee">' +
            '           <span data-price-amount="____oldPrice____" class="price-wrapper">' +
            '               <span class="price">____oldPriceText____</span>' +
            '           </span>' +
            '        </span>' +
            '    </span>' +
            '   <span class="special-price">' +
            '       <span class="price-container price-final_price tax weee">' +
            '           <span data-price-amount="____specialPrice____" class="price-wrapper">' +
            '               <span class="price">____specialPriceText____</span>' +
            '           </span>' +
            '       </span>' +
            '   </span>' +
            '</div>' +
            '<span class="option-bought-value">' +
            '   <span class="count-bought" id="count-bought-value-' + product_id + '" ' + display + ' >' +
            '   </span>' +
            '</span>';
        result = result.replace("____specialPrice____", sp);
        result = result.replace("____specialPriceText____", spt);
        result = result.replace("____oldPrice____", op);
        result = result.replace("____oldPriceText____", opt);
        return result;
    }

    function show_scroll() {
        var iCurScrollPos = $("#product-options-wrapper .groupon_option").scrollTop();
        if (iCurScrollPos > iScrollPos) {
            //Scrolling Down
        } else {
            //Scrolling Up
        }
        iScrollPos = iCurScrollPos;
        var scrollHeight = $("#product-options-wrapper .groupon_option .swatch-attribute-options").height() - $("#product-options-wrapper .groupon_option").height();
        if (iScrollPos <= 0) {
            $('.groupon-option-scroll .scroll-up > .action').fadeOut();
        }
        else {
            $('.groupon-option-scroll .scroll-up > .action').fadeIn();
        }
        if (iScrollPos >= scrollHeight) {
            $('.groupon-option-scroll .scroll-down > .action').fadeOut();
        }
        else {
            $('.groupon-option-scroll .scroll-down > .action').fadeIn();
        }
    }

    return function (widget) {
        $.widget('mage.SwatchRenderer', widget, {
            _OnClick: function ($this, $widget, eventName) {
                var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                    $wrapper = $this.parents('.' + $widget.options.classes.attributeOptionsWrapper),
                    $label = $parent.find('.' + $widget.options.classes.attributeSelectedOptionLabelClass),
                    attributeId = $parent.attr('attribute-id'),
                    $input = $parent.find('.' + $widget.options.classes.attributeInput);

                if ($widget.inProductList) {
                    $input = $widget.productForm.find(
                        '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                    );
                }

                if ($this.hasClass('disabled')) {
                    return;
                }

                if ($this.hasClass('selected')) {
                    return;
                    // $parent.removeAttr('option-selected').find('.selected').removeClass('selected');
                    // $input.val('');
                    // $label.text('');
                    // $this.attr('aria-checked', false);
                } else {
                    $parent.attr('option-selected', $this.attr('option-id')).find('.selected').removeClass('selected');
                    $label.text($this.attr('option-label'));
                    $input.val($this.attr('option-id'));
                    $input.attr('data-attr-name', this._getAttributeCodeById(attributeId));
                    $this.addClass('selected');
                    $widget._toggleCheckedAttributes($this, $wrapper);
                }

                $widget._Rebuild();

                if ($widget.element.parents($widget.options.selectorProduct)
                    .find(this.options.selectorProductPrice).is(':data(mage-priceBox)')
                ) {
                    $widget._UpdatePrice();
                }

                $widget._loadMedia(eventName);
                $input.trigger('change');

                var optionId = $widget.getProduct();
                if (optionId !== undefined) {
                    $('#coupon-select-option-choose').val(optionId);
                    $('.deal_discount_block').show();
                    $('.location-wrapper').show();
                    $('#gift_recipient').show();
                    $('#coupon_id_gift').show();
                    $('label[for="coupon_id_gift"]').show();
                    if ($('#coupon_id_gift').is(":checked")) {
                        $('#product-addtocart-button > span').html('Add as Gift');
                        $("label[for=coupon_id_gift]").html('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.316 16">\n' +
                            '                <g transform="translate(-603 -28)">\n' +
                            '                    <path d="M21.5,8v2.246h2.246V8Z" transform="translate(587.535 22.246)"></path>\n' +
                            '                    <path d="M8.281,16.1v2.246H12.07v6.989H8.281V18.346H6.035v6.989H2.246V18.346H6.035V16.1H0V27.581H14.316V16.1Z" transform="translate(603 16.419)"></path>\n' +
                            '                    <rect width="2.246" height="2.246" transform="translate(611.281 28)"></rect>\n' +
                            '                    <rect width="2.246" height="2.246" transform="translate(606.789 28)"></rect>\n' +
                            '                </g>\n' +
                            '            </svg>' + ' Cancel');
                    }
                } else {
                    $('#coupon-select-option-choose').val(null);
                    $('.deal_discount_block').hide();
                    $('.location-wrapper').hide();
                    $('#gift_recipient').hide();
                    $('#coupon_id_gift').hide();
                    $('label[for="coupon_id_gift"]').hide();
                    $('#product-addtocart-button > span').html('Add to Cart');
                    $("label[for=coupon_id_gift]").html('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.316 16">\n' +
                        '                <g transform="translate(-603 -28)">\n' +
                        '                    <path d="M21.5,8v2.246h2.246V8Z" transform="translate(587.535 22.246)"></path>\n' +
                        '                    <path d="M8.281,16.1v2.246H12.07v6.989H8.281V18.346H6.035v6.989H2.246V18.346H6.035V16.1H0V27.581H14.316V16.1Z" transform="translate(603 16.419)"></path>\n' +
                        '                    <rect width="2.246" height="2.246" transform="translate(611.281 28)"></rect>\n' +
                        '                    <rect width="2.246" height="2.246" transform="translate(606.789 28)"></rect>\n' +
                        '                </g>\n' +
                        '            </svg>' + ' Give as a Gift');
                }
                var parentProductId = $("input[name=product]").val();

                $("#qty").show();
                if (!optionId) {
                    $("#value").text('0.00');
                    $("#you_save").text('0.00');
                    $("#discount").text('0.00 %');
                    $("#qty").hide();
                } else {
                    $.ajax({
                        type: "GET",
                        url: url.build('/groupon/update/highlight/'),
                        data: {
                            product_id: optionId
                        },
                        showLoader: false,
                        success: function (result) {
                            var term = result.terms;
                            var instruct = result.instruct;
                            $("#terms_groupon").text(term);
                            $("#instructions_groupon").text(instruct);
                        },
                        error: function (e) {
                            // console.log(e);
                        },
                        timeout: 10000
                    });

                    $.ajax({
                        type: "GET",
                        url: url.build('/groupon/update/option'),
                        data: {
                            option_id: optionId,
                            parent_product_id: parentProductId
                        },
                        showLoader: false,
                        success: function (result) {
                            var discount = result.discount;
                            var you_save = result.you_save;
                            var qty = result.qty;
                            var value = result.value;

                            $("#value").text(value);
                            $("#you_save").text(you_save);
                            $("#discount").text(discount);
                            $("#qtyValue").text(qty);
                        },
                        error: function (e) {
                            // console.log(e);
                        },
                        timeout: 10000
                    });
                }
            },
            _init: function () {
                var price = this.options.jsonConfig.optionPrices;
                window.SWATCH_PRICE = price;
                window.productIds = [];
                this._super();
                show_scroll();

                $("#product-options-wrapper .groupon_option").scroll(function () {
                    show_scroll();
                });
                $('.groupon-option-scroll .scroll-up > .action').on({
                    'mousedown touchstart': function () {
                        $("#product-options-wrapper .groupon_option").animate({scrollTop: 0}, 500);
                    },
                    'mouseup touchend': function () {
                        $("#product-options-wrapper .groupon_option").stop(true);
                    }
                });
                $('.groupon-option-scroll .scroll-down > .action').on({
                    'mousedown touchstart': function () {
                        var scrollHeight = $("#product-options-wrapper .groupon_option .swatch-attribute-options").height() - $("#product-options-wrapper .groupon_option").height();
                        $("#product-options-wrapper .groupon_option").animate({
                            scrollTop: scrollHeight
                        }, 500);
                    },
                    'mouseup touchend': function () {
                        $("#product-options-wrapper .groupon_option").stop(true);
                    }
                });

            },
            /**
             * Render swatch options by part of config
             *
             * @param {Object} config
             * @param {String} controlId
             * @returns {String}
             * @private
             */
            _RenderSwatchOptions: function (config, controlId) {
                var self = this;
                var optionConfig = this.options.jsonSwatchConfig[config.id],
                    optionClass = this.options.classes.optionClass,
                    moreLimit = parseInt(this.options.numberToShow, 10),
                    moreClass = this.options.classes.moreButton,
                    moreText = this.options.moreButtonText,
                    countAttributes = 0,
                    html = '';

                if (!this.options.jsonSwatchConfig.hasOwnProperty(config.id)) {
                    return '';
                }

                $.each(config.options, function () {
                    var id,
                        type,
                        value,
                        thumb,
                        label,
                        attr;

                    if (!optionConfig.hasOwnProperty(this.id)) {
                        return '';
                    }

                    // Add more button
                    if (moreLimit === countAttributes++) {
                        html += '<a href="#" class="' + moreClass + '">' + moreText + '</a>';
                    }

                    id = this.id;
                    type = parseInt(optionConfig[id].type, 10);
                    value = optionConfig[id].hasOwnProperty('value') ? optionConfig[id].value : '';
                    thumb = optionConfig[id].hasOwnProperty('thumb') ? optionConfig[id].thumb : '';
                    label = this.label ? self._addslashes(this.label) : '';
                    attr =
                        ' id="' + controlId + '-item-' + id + '"' +
                        ' aria-checked="false"' +
                        ' aria-describedby="' + controlId + '"' +
                        ' tabindex="0"' +
                        ' option-type="' + type + '"' +
                        ' option-id="' + id + '"' +
                        ' option-label="' + label + '"' +
                        ' aria-label="' + label + '"' +
                        ' option-tooltip-thumb="' + thumb + '"' +
                        ' option-tooltip-value="' + value + '"' +
                        ' role="option"';

                    if (!this.hasOwnProperty('products') || this.products.length <= 0) {
                        attr += ' option-empty="true"';
                    }
                    html += '<div class="groupon-option-box">';
                    if (type === 0) {
                        // Text
                        html += '<div class="' + optionClass + ' text" ' + attr + '>' + (value ? value : label) +
                            '</div>';
                    } else if (type === 1) {
                        // Color
                        html += '<div class="' + optionClass + ' color" ' + attr +
                            ' style="background: ' + value +
                            ' no-repeat center; background-size: initial;">' + '' +
                            '</div>';
                    } else if (type === 2) {
                        // Image
                        html += '<div class="' + optionClass + ' image" ' + attr +
                            ' style="background: url(' + value + ') no-repeat center; background-size: initial;">' + '' +
                            '</div>';
                    } else if (type === 3) {
                        // Clear
                        html += '<div class="' + optionClass + '" ' + attr + '></div>';
                    } else {
                        // Default
                        html += '<div class="' + optionClass + '" ' + attr + '>' + label + '</div>';
                    }
                    var product_id = this.products[0];
                    var price = window.SWATCH_PRICE[product_id];
                    var bought = 0;
                    window.productIds.push(product_id);
                    var priceBox = getPriceBox(price.finalPrice.amount, price.oldPrice.amount, bought, product_id);
                    /*html += priceBox;*/
                    html += priceBox + '</div>';
                });

                return html;
            },

            /**
             * Event listener
             *
             * @private
             */
            _EventListener: function () {
                this._super();
                this._updateBoughtOption();
                $('div.swatch-attribute-options.clearfix>div:first-of-type div.swatch-option').click();
            },
            _updateBoughtOption: function () {
                $.ajax({
                    url: url.build('/groupon/update/bought/'),
                    data: {
                        product_ids: window.productIds
                    },
                    showLoader: false,
                    type: "POST",
                    success: function (result) {
                        // console.log(result);
                        for (var i = 0; i < result.length; i++) {
                            var productId = result[i]['product_id'];
                            var productBought = result[i]['product_bought'];
                            if (productBought > 0) {
                                var boughtHtml = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 13.093">' +
                                    '           <g transform="translate(-1070 -4880)">' +
                                    '               <rect width="1.775" height="1.775" transform="translate(1074.128 4891.317)"/>' +
                                    '               <rect width="1.775" height="1.775" transform="translate(1081.118 4891.317)"/>' +
                                    '               <path d="M9.964,2.419V4.194h3.329l-1.576,3.6H5.437l-.954-3.6H8.189V2.419H4.017L3.373,0H0V1.775H2.019L4.083,9.564h8.788L16,2.419Z" transform="translate(1070 4880)"/>' +
                                    '               <rect width="1.775" height="1.775" transform="translate(1079.954 4882.43) rotate(180)"/>' +
                                    '           </g>' +
                                    '        </svg>' +
                                    '        ' + productBought + '<span>&nbsp;bought</span>';
                                $('span#count-bought-value-' + productId).show();
                                $('span#count-bought-value-' + productId).html(boughtHtml);
                            }
                        }
                    },
                    error: function () {
                    }
                });
            },
            _addslashes(string) {
                return string.replace(/'/g, '&apos;').replace(/"/g, '&quot;');
            }
        });

        return $.mage.SwatchRenderer;
    }
});