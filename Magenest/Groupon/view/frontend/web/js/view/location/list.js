/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko'
    ],
    function ($, _, Component, ko) {
        'use strict';
        return Component.extend({
            initObservable: function () {
                this._super()
                    .observe(['locations']);
                return this;
            },
            initialize: function () {
                this._super();
                return this;
            }
        });
    }
);
