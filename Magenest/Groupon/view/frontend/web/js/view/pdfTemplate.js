/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'mage/translate'
    ],
    function ($, _, Component, ko, $t) {

        'use strict';
        return Component.extend({
            /**
             * init data for the menu
             */
            initObservable: function () {
                this._super()
                    .observe({
                        optionPriceType : []
                    });
                var self = this;
                self.pdfViewModel();
                return this;
            },


            pdfViewModel: function () {
                var self = this;
                // Editable data
                self.informations = ko.observableArray(window.coordinate);
                // Operations
                self.addOptionNew = function() {
                    self.informations.push(new optionRule());
                };
                self.removeRow = function(options) {
                    self.informations.remove(options)
                }
            }
        });
        function optionRule() {
            var self = this;
            self.info = '';
            self.title = '';
            self.x = '';
            self.y = '';
            self.size = '';
            self.color = '';
        }
    }
);
