/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'mage/translate'
    ],
    function ($, _, Component, ko, $t) {

        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Groupon/delivery-location',
                locations_not_used: [],
                locations_used: [],
                locations: [],
                countries: [],
                showShowMore: ko.observable(false),
                disabled: ko.observable(true)
            },

            rendered: ko.observable(false),

            /**
             * init data for the menu
             */
            initObservable: function () {
                var self = this;

                this._super()
                    .observe(['disabled', 'locations', 'used_locations', 'locations_not_used']);
                this.locations_used.each(function (value, key) {
                    var used = value.used;
                    value.used = ko.observable(used);
                    self.locations().push(value);
                });
                if (self.locations_not_used().length > 0)
                    self.showShowMore(true);
                if (self.locations().length == 0)
                    self.showMore();
                return this;
            },

            initialize: function () {
                this._super();
                return this;
            },

            removeLocation: function (option) {
                this.locations.remove(option);
            },

            addOptionLocation: function () {
                this.locations.push(new optionLocation());
            },

            locationForm: function () {
                var self = this;
                self.addOptionLocation = function () {
                    self.locations.push(new optionLocation());
                };
                self.remove = function (option) {
                    self.locations.remove(option);
                };
            },

            afterRender: function () {
                this.rendered(true);
            },

            showMore: function () {
                var self = this;
                if (self.locations_not_used().length > 0) {
                    $.each(self.locations_not_used(), function (key, value) {
                        if (value != undefined) {
                            var used = value.used;
                            value.used = ko.observable(used);
                            self.locations().push(value);
                            remove(self.locations_not_used(), value);
                            if (key == 5) {
                                return false;
                            }
                        }
                    });
                    self.locations(self.locations());
                } else
                    self.showShowMore(false);
            }
        });

        function optionLocation() {
            var self = this;
            self.address = ko.observable('');
            self.distance = ko.observable('');
            self.delivery_id = ko.observable('');
            self.used = ko.observable(false);
            self.old = false;
        }

        function remove(array, element) {
            var index = array.indexOf(element);

            if (index !== -1) {
                array.splice(index, 1);
            }
        }
    }
);
