define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'mage/translate',
        'Magento_Ui/js/form/element/single-checkbox'
    ],
    function ($, _, Component, ko, $t, checkbox) {

        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Groupon/redemption_location'
            },

            rendered: ko.observable(false),
            renderedChildCount: 0,

            initialize: function() {
               this._super();
               this.initRedemptionLocationForm();
               return this;
            },

            initRedemptionLocationForm: function () {
                this.elems.push(checkbox.extend({
                    defaults: {
                        prefer: 'toggle',
                        label: 'Online Service / No visitation required'
                    }
                }))
            },

            afterChildRendered: function(rendered) {
                if (rendered) {
                    if (++this.renderedChildCount == this.initChildCount) {
                        this.rendered(true);
                    }
                }
            }
        });
    }
);
