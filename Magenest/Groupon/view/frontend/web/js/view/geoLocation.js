/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        "Magento_Ui/js/modal/confirm",
        'ko',
        'mage/translate'
    ],
    function ($, _, Component, confirm, ko, $t) {
        'use strict';
        var storage = $.initNamespaceStorage('customer_location').localStorage;
        return Component.extend({
            defaults: {
                productId: false,
                productType: false
            },

            initialize: function () {
                this._super();
                var self = this;
                const body = $("body");

                if (storage.get('customer_location') == undefined) {
                    confirm({
                        content: "Let us access your location to provide you the best experience",
                        modalClass: 'confirm_access_location_require',
                        buttons: [
                            {
                                text: $.mage.__('Cancel'),
                                class: 'action-secondary action-dismiss',
                                click: function (event) {
                                    body.removeClass("modal-allow-scroll");
                                    this.closeModal(event);
                                }
                            }, {
                                text: $.mage.__('Accept'),
                                class: 'action-primary action-accept',
                                click: function (event) {
                                    body.removeClass("modal-allow-scroll");
                                    this.closeModal(event, true);
                                }
                            }
                        ],
                        actions: {
                            confirm: function () {
                                self.askCustomerForAccessLocation(false);
                                body.removeClass("modal-allow-scroll");
                            },
                            cancel: function () {
                                self.customerDenyAccessTheirLocation();
                                body.removeClass("modal-allow-scroll");
                                return false;
                            }
                        }
                    });
                    body.addClass("modal-allow-scroll");
                } else if (!storage.get('customer_location') == false) {
                    self.askCustomerForAccessLocation(true);
                }
                return this;
            },
            askCustomerForAccessLocation: function (hasData) {
                var self = this;
                if (hasData === true) {
                    var latlng = storage.get('customer_location').split(",");
                    showPositionByLatLng(latlng[0], latlng[1]);
                } else {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition, showError);
                    } else {
                        console.log("Geolocation is not supported by this browser.");
                    }
                }

                function showPosition(position) {
                    let lat = position.coords.latitude;
                    let lng = position.coords.longitude;
                    showPositionByLatLng(lat, lng);
                }

                function showPositionByLatLng(lat, lng) {
                    console.log('Your current location: lat is ' + lat + ' and long is ' + lng);
                    if (self.productType === "ticket") {
                        self.getDistanceWithLatLngTicket(lat, lng);
                    } else if (self.productType === "groupon") {
                        self.getDistanceWithLatLngGroupon(lat, lng);
                    }
                    storage.set('customer_location', lat + ',' + lng)
                }

                function showError(e) {
                    console.log(e);
                    self.customerDenyAccessTheirLocation();
                }
            },

            getDistanceWithLatLngGroupon: function (lat, lng) {
                var self = this;
                $.ajax({
                    url: window.getDistanceUrl,
                    dataType: 'json',
                    method: 'POST',
                    data: {
                        canAccess: true,
                        startLat: lat,
                        startLng: lng,
                        type: 'groupon',
                        id: self.productId
                    },
                    success: function (response) {
                        if (response) {
                            if (response.success) {
                                let fillField = '#groupon-product-distance-field';
                                if (response.distance !== false && response.distance !== 'null' && response.distance !== 'false' && response.distance !== null) {
                                    $(fillField).html('&bull; ' + response.distance);
                                }
                                if (response.location !== undefined && response.location !== false) {
                                    $('#groupon-product-info-location').html(response.location);
                                }
                            }
                        }
                    },
                    error: function () {
                    }
                });
            },
            getDistanceWithLatLngTicket: function (lat, lng) {
                var self = this;
                $.ajax({
                    url: window.getDistanceUrl,
                    dataType: 'json',
                    method: 'POST',
                    data: {
                        startLat: lat,
                        startLng: lng,
                        type: 'ticket',
                        id: self.productId
                    },
                    success: function (response) {
                        if (response) {
                            if (response.success) {
                                let fillField = '#ticket-product-distance-field';
                                if (response.distance !== false && response.distance !== 'null' && response.distance !== 'false' && response.distance !== null) {
                                    $(fillField).html('&bull; ' + response.distance);
                                }
                            }
                        }
                    },
                    error: function () {
                    }
                });
            },
            customerDenyAccessTheirLocation: function () {
                storage.set('customer_location', false)
            }
        });
    }
);
