/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'mage/translate'
    ],
    function ($, _, Component, ko, $t) {

        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Groupon/location',
                locations_not_used: [],
                locations_used: [],
                locations: [],
                countries: [],
                showShowMore: ko.observable(false),
                disabled: ko.observable(true)
            },

            rendered: ko.observable(false),

            /**
             * init data for the menu
             */
            initObservable: function () {
                var self = this;

                this._super()
                    .observe(['disabled', 'locations', 'used_locations', 'locations_not_used']);
                this.locations_used.each(function (value, key) {
                    var used = value.used;
                    value.used = ko.observable(used);
                    self.locations().push(value);
                });
                if (self.locations_not_used().length > 0)
                    self.showShowMore(true);
                if (self.locations().length == 0)
                    self.showMore();
                return this;
            },

            initialize: function () {
                this._super();
                var self = this;
                $(function () {
                    $('#redemption_location_coupon').on('change', 'input, select', function () {
                        // var location = self.locations();
                        // self.locations([]);
                        // self.locations(location);
                        self.locations.valueHasMutated();
                    });
                });
                return this;
            },

            removeLocation: function (option) {
                this.locations.remove(option);
            },

            addOptionLocation: function () {
                this.locations.push(new optionLocation(this.countries));
            },

            afterRender: function () {
                this.rendered(true);
            },

            showMore: function () {
                var self = this;
                if (self.locations_not_used().length > 0) {
                    $.each(self.locations_not_used(), function (key, value) {
                        if (value != undefined) {
                            var used = value.used;
                            value.used = ko.observable(used);
                            self.locations().push(value);
                            remove(self.locations_not_used(), value);
                            if (key == 5) {
                                return false;
                            }
                        }
                    });
                    self.locations(self.locations());
                } else
                    self.showShowMore(false);
            }
        });

        function remove(array, element) {
            var index = array.indexOf(element);

            if (index !== -1) {
                array.splice(index, 1);
            }
        }

        function optionLocation(countries) {
            var self = this;
            self.status = '';
            self.location_name = '';
            self.street = '';
            self.street_two = '';
            self.city = '';
            self.used = ko.observable(false);
            self.old = false;
            self.country = '';
            self.region = '';
            self.postcode = '';
            self.phone = '';
            self.location_id = '';
            self.countrylist = ko.observableArray(countries);
        }
    }
);
