define([
    'jquery',
    'mage/translate',
    'ko',
    'mage/url',
    'uiComponent',
    'jqueryCountdown',
    'domReady!',
    'js/owl.carousel',
    'Magenest_Groupon/js/jquery/jquery.serialScroll'
],function ($,$t,ko,url,Component) {
    'use strict';

    function changePrdsOwl() {
        var widthItems = $(".recentview .owl-stage").width();
        var widthItemsSlide = $(".recentview .owl-stage-outer").width();
        if ((widthItems / widthItemsSlide) <= 1) {
            $(".recentview + .scroll-wrapper .scroll").width(0);
        } else {
            $(".recentview + .scroll-wrapper .scroll").width("calc(100% / " + (widthItems / widthItemsSlide) + ")");
        }
        var widthScroll = $(".recentview + .scroll-wrapper .scroll").width();

        var widthItemsSlided = -parseInt($(".recentview .owl-stage").css('transform').split(',')[4]);

        var maxLeftScroll = widthItemsSlide - widthScroll;
        var ratio = maxLeftScroll / (widthItems - widthItemsSlide);
        var leftScroll = ratio * widthItemsSlided;

        if (leftScroll < 0) {
            leftScroll = 0;
        } else if (leftScroll > maxLeftScroll) {
            leftScroll = maxLeftScroll;
        }
        ;

        $(".recentview + .scroll-wrapper .scroll").css("left", leftScroll);
    }

    function scrollPrdsOwl(leftScroll) {
        var widthItems = $(".recentview .owl-stage").width();
        var widthItemsSlide = $(".recentview .owl-stage-outer").width();
        var widthScroll = $(".recentview + .scroll-wrapper .scroll").width();
        var maxLeftScroll = widthItemsSlide - widthScroll;
        if (leftScroll < 0) {
            leftScroll = 0;
        } else if (leftScroll > maxLeftScroll) {
            leftScroll = maxLeftScroll;
        }
        ;
        $(".recentview + .scroll-wrapper .scroll").css("left", leftScroll);                                                                                    //left toi da cua thanh scroll
        var ratio = (widthItems - widthItemsSlide) / maxLeftScroll;
        var transformSlide = -ratio * leftScroll;

        $(".recentview .owl-stage").css('transform', 'translate3d(' + transformSlide + 'px, 0px, 0px)');
    }

    return Component.extend({
        default: {
            template: 'Magenest_Groupon/widget/product/recentview',
            productCount: ko.observable(5),
            title1: "Title 1",
            title: ko.observable('Recent Viewed Products')
        },
        initObservable: function () {
            this._super();
            this._super().observe('productInfo');
            this._super().observe('title');
            this._super().observe('productCount');
            this._super().observe('baseUrl');
            this._super().observe('getCurrencyAmount');
            this._super().observe('checkOldPrice');
            url.setBaseUrl(this.baseUrl());
            this.productInfo([]);
            this.loadProduct();
            return this;
        },
        loadProduct: function () {
            var loadUrl = url.build('groupon/widget/loadRecentView');
            var component = this;
            $.ajax({
                method: "POST",
                url: loadUrl,
                cache: false,
                data: {
                    productCount: component.productCount()
                },
                success: function (response) {
                    // console.log(response);
                    component.productInfo(response);
                    component.coundownTimer();
                    if($(".recentview").length){
                        $(".recentview").owlCarousel({
                            margin: 8,
                            loop: false,
                            items: 4,
                            nav: true,
                            dots: false,
                            autoWidth: true,
                            responsive:{
                                0:{
                                    items: 1,
                                },
                                480:{
                                    items: 2,
                                },
                                769:{
                                    items: 3,
                                },
                                1200:{
                                    items: 4,
                                }
                            }
                        });
                        var owl = $(".recentview");
                        var translateOwlToRightEdge = function(){
                            var widthItems = $(".recentview .owl-stage").width();
                            var widthItemsSlide = $(".recentview .owl-stage-outer").width();
                            var widthItemsSlided = - parseInt($(".recentview .owl-stage").css('transform').split(',')[4]);
                            var translatedDistanceSlide = widthItems - widthItemsSlide;
                            var transformDistance = 0;
                            if(translatedDistanceSlide <= 0){
                                return false;
                            }
                            else{
                                if(widthItemsSlided > translatedDistanceSlide){
                                    transformDistance = - translatedDistanceSlide;
                                    $(".recentview .owl-stage").css('transform','translate3d('+transformDistance+'px, 0px, 0px)');
                                }
                            }
                        }
                        owl.on('dragged.owl.carousel',function(){
                            translateOwlToRightEdge();
                        })
                        owl.on('translated.owl.carousel',function(){
                            translateOwlToRightEdge();
                        })

                        var owl = $(".recentview");
                        var pastIndex = 0;
                        var adjustSlideByScrollBar = function(leftScroll){
                            leftScroll = parseInt(leftScroll);
                            var widthItems = $(".recentview .owl-stage").width();
                            var widthItemsSlide = $(".recentview .owl-stage-outer").width();
                            var widthScroll = $(".recentview + .scroll-wrapper .scroll").width();
                            var maxLeftScroll = widthItemsSlide-widthScroll;
                            if(leftScroll <= 0 || leftScroll >= maxLeftScroll){
                                scrollPrdsOwl(leftScroll);
                                return false;
                            }
                            var distanceToTranslateSlide = leftScroll/ widthItemsSlide * widthItems;
                            var slideArray = $(".recentview .owl-stage").children();
                            var slideWidthArray = [];
                            var widthSlide = 0;
                            for(var i= 0; i < slideArray.length; i++){
                                if(i === 0){
                                    widthSlide += parseInt($(slideArray[i]).css("width"));
                                }
                                else{
                                    widthSlide += (parseInt($(slideArray[i]).css("width")) + 8);
                                }
                                slideWidthArray.push(widthSlide);
                            }
                            var widthOfSlidesToTranslate = slideWidthArray.filter(function(w){
                                if(Math.round((distanceToTranslateSlide / w)) === 1){
                                    return true;
                                }
                                return false;
                            })
                            var slideIndexToTranslate = slideWidthArray.indexOf(widthOfSlidesToTranslate[0]) + 1;
                            if(slideIndexToTranslate === pastIndex){
                                if(!widthOfSlidesToTranslate.length){
                                    widthOfSlidesToTranslate[0] = 0;
                                }
                                leftScroll = - widthOfSlidesToTranslate[0];
                                var leftScrollbar =  ((widthOfSlidesToTranslate[0] / widthItems) * widthItemsSlide );
                                $(".recentview.owl-carousel .owl-stage").css('transform','translate3d('+leftScroll+'px, 0px, 0px)');
                                $(".recentview.owl-carousel .owl-stage").css('transition','0.5s');
                                $(".recentview.owl-carousel + .scroll-wrapper .scroll").css("left",leftScrollbar);
                            }
                            else{
                                owl.trigger('to.owl.carousel',[slideIndexToTranslate,500,true]);
                            }
                            pastIndex = slideIndexToTranslate;
                        }
                        changePrdsOwl();
                        $(".recentview").on('initialize.owl.carousel initialized.owl.carousel ' +
                            'initialize.owl.carousel initialize.owl.carousel ' +
                            'resize.owl.carousel resized.owl.carousel ' +
                            'refresh.owl.carousel refreshed.owl.carousel ' +
                            'update.owl.carousel updated.owl.carousel ' +
                            'drag.owl.carousel dragged.owl.carousel ' +
                            'translate.owl.carousel translated.owl.carousel ' +
                            'to.owl.carousel changed.owl.carousel',function(e) {
                            changePrdsOwl();
                        });
                        $( ".recentview + .scroll-wrapper .scroll" ).draggable({
                            drag: function() {
                                scrollPrdsOwl($(this).position().left);
                                $(".recentview .owl-stage").css('transition','0s');
                            },
                            stop: function(){
                                adjustSlideByScrollBar($(this).position().left);
                                // scrollPrdsOwl($(this).position().left);
                            }
                        });
                        var thisclass = $(".recentview .owl-item .prd-item ");
                        setInterval(function() {
                            var i=0;
                            var height=[];
                            var priceInfo = [];
                            thisclass.css('min-height','0');
                            thisclass.each(function(){
                                height[i] = $(this).outerHeight();
                                priceInfo[i] = $(this).find('.prd-price-information').outerHeight();
                                i++;
                            });
                            var maxheight = Math.max.apply(Math, height);
                            var maxPriceInfo = Math.max.apply(Math, priceInfo);
                            thisclass.css({
                                'min-height': maxheight,
                                'padding-bottom' : maxPriceInfo,
                            });
                        }, 500);
                    }
                },
                error: function (error) {

                },
                always: function () {
                },
                showLoader: false
            });
        },
        existProduct: function () {
            return this.productInfo().length > 0;
        },
        getCurrencyAmount: function (amount,component) {
            return component.currencySymbol+amount
        },
        checkOldPrice: function (data) {
            return data['originPrice'] !== undefined &&data['saleRates']>0;
        },

        coundownTimer: function () {
            let self = this;
            $(document).ready(function () {
                $.each(self.productInfo(), function (index, e) {
                    if (e.product_type === "groupon_product_type" && e.countdown_time_id !== "") {
                        var timeLeft = e.groupon_end_time;
                        timeLeft = parseInt(timeLeft);
                        var finalStamp = Date.now() + timeLeft;
                        var finalDate = new Date(finalStamp);
                        $('#' + e.countdown_time_id).first().countdown(finalDate, function (event) {
                            let format = '<div class="box-count box-hours"><div class="number">%H</div><div class="text">h&nbsp</div></div><div class="box-count box-min"><div class="number">%M</div><div class="text">m&nbsp</div></div><div class="box-count box-secs"><div class="number">%S</div><div class="text">s&nbsp</div></div>';
                            let time = event.strftime(format);
                            $('.' + e.countdown_time_display_field).first().html("" + time);
                        });
                    }
                });
                
            }).on('finish.countdown', function () {
                $('#' + e.countdown_time_id).parents('.remaining_time').hide();
                $('button#product-addtocart-button').attr("disabled", "disabled");
            });
        },
        // getTemplate: function() {
        //     console.log('gettemplate');
        //     return 'Magenest_Groupon/widget/product/recentview';
        // }
        
    });
    
});