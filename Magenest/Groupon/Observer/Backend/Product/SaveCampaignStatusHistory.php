<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */
namespace Magenest\Groupon\Observer\Backend\Product;

use Magenest\Groupon\Model\StatusFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveCampaignStatusHistory implements ObserverInterface
{
    protected $statusFactory;

    public function __construct(
        StatusFactory $statusFactory
    ) {
        $this->statusFactory = $statusFactory;
    }

    public function execute(Observer $observer)
    {
        try {
            $product = $observer->getProduct();
            if ($product->getCampaignStatusChanged()) {
                $statusHistory = $this->statusFactory->create();
                $statusHistory->setData([
                    'product_id' => $product->getId(),
                    'status_code' => $product->getCampaignStatus(),
                    'comment' => $product->getStatusComment(),
                ])->save();
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }
}