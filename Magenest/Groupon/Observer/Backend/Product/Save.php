<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Observer\Backend\Product;

use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magenest\Groupon\Model\LocationFactory;
use Magenest\Groupon\Model\DeliveryFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;

/**
 * Class Save
 * @package Magenest\Groupon\Observer\Backend\Product
 */
class Save implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    protected $productFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var LocationFactory
     */
    protected $location;

    /**
     * @var DeliveryFactory
     */
    protected $delivery;

    /**
     * Save constructor.
     * @param RequestInterface $request
     * @param Filesystem $filesystem
     * @param StoreManagerInterface $storeManagerInterface
     * @param LoggerInterface $logger
     * @param DealFactory $dealFactory
     * @param TicketFactory $ticketFactory
     * @param Configurable $configurable
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param StockStateInterface $stockStateInterface
     * @param LocationFactory $locationFactory
     * @param DeliveryFactory $deliveryFactory
     */
    public function __construct(
        RequestInterface $request,
        Filesystem $filesystem,
        StoreManagerInterface $storeManagerInterface,
        LoggerInterface $logger,
        DealFactory $dealFactory,
        TicketFactory $ticketFactory,
        Configurable $configurable,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        StockStateInterface $stockStateInterface,
        LocationFactory $locationFactory,
        DeliveryFactory $deliveryFactory
    ) {
        $this->configurable = $configurable;
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_filesystem = $filesystem;
        $this->_logger = $logger;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->productFactory = $productFactory;
        $this->stockStateInterface = $stockStateInterface;
        $this->location = $locationFactory;
        $this->delivery = $deliveryFactory;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $productTypeId = $product->getTypeId();
        $params = $this->_request->getParams();
        $store = $product->getStore()->getStoreId();
        if (!empty($params['groupon'])) {
            if ($productTypeId == 'configurable' || $productTypeId == 'coupon') {
                $model = $this->deal->create()->load($productId, 'product_id');

                $status = $product->getData('campaign_status');
                $array = $params['groupon'];
                if ($productTypeId == 'coupon') {
                    $data['product_id'] = $productId;
                    $modelConfigurable = $this->configurable->getParentIdsByChild($productId);
                    if (isset($modelConfigurable[0])) {
                        $data['parent_id'] = $modelConfigurable[0];
                    }
                    $couponData = $this->getCfgProductData($params['configurable-matrix-serialized'], $productId);
                    $cfgProductQty = @$couponData['qty'];
                    $data['maximum_buyers_limit'] = (int)@$couponData['maximum_buyers_limit'];
                    $data['qty'] = $cfgProductQty;
                    if (!$model->getData('purchased_qty')) {
                        $data['purchased_qty'] = 0;
                    }
                    $data['available_qty'] = $cfgProductQty;
                }
                $data['product_type'] = $productTypeId;
                $data['enable'] = $array['enable_groupon'];

                if ($array['enable_groupon'] == 1) {
                    /** save deal */
                    $data['product_id'] = $productId;
                    $data['store_id'] = $store;
                    $data['start_time'] = $array['campaign_date']['start_time'] ? $array['campaign_date']['start_time'] : null;
                    $data['end_time'] = $array['campaign_date']['end_time'] ? $array['campaign_date']['end_time'] : null;
                    $data['groupon_expire'] = $array['campaign_date']['groupon_expire'] ? $array['campaign_date']['groupon_expire'] : null;
                    $data['redeemable_after'] = $array['campaign_date']['redeemable_after'] ? $array['campaign_date']['redeemable_after'] : null;
                    $data['vendor_id'] = $product->getUdropshipVendor();
                    $data['status'] = $status;
                    $data['template_id'] = $array['template_id'];
                    $term = $this->beforeSaveTermsData($array['campaign_term']);
                    $data['term'] = json_encode($term);
                }

                $model->addData($data)->save();
                if ($productTypeId == 'configurable') {
                    $dataRedemption['is_business_address'] = 0;
                    $dataRedemption['is_delivery_address'] = 0;
                    if ($array['redeem_location']['is_online_coupon'] && $array['redeem_location']['is_online_coupon'] == 1) {
                        $dataRedemption['is_online_coupon'] = 1;
                        $dataRedemption['location_ids'] = null;
                        $dataRedemption['delivery_ids'] = null;
                    } else {
                        $dataRedemption['is_online_coupon'] = 0;
                        if ($array['redeem_location']['is_business_address']) {
                            $dataRedemption['is_business_address'] = 1;
                            $dataRedemption['customer_specify_location'] = @$array['redeem_location']['customer_specify_location']?:0;
                            $this->saveBusinessAddress($model, @$array['redeem_location']['location_rule']);
                        }
                        if ($array['redeem_location']['is_delivery_address']) {
                            $dataRedemption['is_delivery_address'] = 1;
                            $this->saveDeliveryAddress($model, @$array['redeem_location']['delivery_rule']);
                        }
                    }
                    $model->addData($dataRedemption)->save();
                }
            }
        }

        return;
    }

    protected function getCfgProductData($cfgData, $productId)
    {
        $data = json_decode($cfgData, true);
        if (is_array($data)) {
            foreach($data as $datum) {
                if ($productId === @$datum['id']) {
                    return $datum;
                }
            }
        } else {
            return [];
        }
    }

    /**
     * @param $deal
     * @param $params
     * @throws \Exception
     */
    protected function saveBusinessAddress($deal, $params)
    {
        $location = $this->location->create();
        $locationIds = $deal->getLocationIds();
        if ($locationIds === "") {
            $usedLocations = [];
        } else {
            $usedLocations = explode(',', $locationIds);
        }
        if (is_array($params)) {
            foreach ($params as $locationData) {
                $location->load(@$locationData['location_id'] ?: null);
                unset($locationData['location_id']);
                unset($locationData['record_id']);
                $locationData['status'] = '0';
                $locationData['product_id'] = $deal->getProductId();
                $locationData['deal_id'] = $deal->getId();
                $locationData['vendor_id'] = $deal->getVendorId();
                $useLocation = false;
                if (@$locationData['used']) {
                    if ($locationData['used'] == "1") {
                        $useLocation = true;
                    }
                    unset($locationData['used']);
                }
                $location->addData($locationData)->save();
                if ($useLocation) {
                    $usedLocations[] = $location->getId();
                }
                $location->unsetData();
            }
        }
        $deal->setData('location_ids', implode(',', $usedLocations));
    }

    /**
     * @param $deal
     * @param $params
     * @throws \Exception
     */
    protected function saveDeliveryAddress($deal, $params)
    {
        $deliveryLocation = $this->delivery->create();
        $usedDeliveryLocations = [];
        if (is_array($params)) {
            foreach ($params as $deliveryLocationData) {
                $deliveryLocation->load(@$deliveryLocationData['delivery_id'] ?: null);
                unset($deliveryLocationData['delivery_id']);
                $deliveryLocationData['vendor_id'] = $deal->getVendorId();
                $deliveryLocation->addData($deliveryLocationData)->save();
                if (@$deliveryLocationData['used']) {
                    $usedDeliveryLocations[] = $deliveryLocation->getId();
                }
                $deliveryLocation->unsetData();
            }
        }
        $deal->setData('delivery_ids', implode(',', $usedDeliveryLocations));
    }

    protected function beforeSaveTermsData($data)
    {
        $result = [];
        if ($data['reservation_required'] === "1" && isset($data['required_day'])) {
            $result['reservation']['enable'] = 1;
            $result['reservation']['days'] = intval($data['required_day']);
        } else {
            $result['reservation']['enable'] = 0;
        }
        $result['combine'] = intval($data['combine_coupon']);
        if ($data['age_limit'] === "1" && isset($data['age_limit_number'])) {
            $result['age_limit']['enable'] = 1;
            $result['age_limit']['age_number'] = $data['age_limit_number'];
        } else {
            $result['age_limit']['enable'] = 0;
        }
        $result['sharing'] = intval($data['share_coupon']);
        if (isset($data['addition_terms'])) {
            $result['vendor_specify'] = trim($data['addition_terms']);
        }

        return $result;
    }
}
