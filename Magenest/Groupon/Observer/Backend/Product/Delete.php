<?php
/**
 * Created by PhpStorm.
 * User: heomep
 * Date: 17/06/2017
 * Time: 10:18
 */
namespace Magenest\Groupon\Observer\Backend\Product;

use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;

/**
 * Class Save
 * @package Magenest\Groupon\Observer\Backend\Product
 */
class Delete implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * Save constructor.
     * @param RequestInterface $request
     * @param Filesystem $filesystem
     * @param StoreManagerInterface $storeManagerInterface
     * @param LoggerInterface $logger
     * @param DealFactory $dealFactory
     * @param OptionFactory $optionFactory
     * @param TicketFactory $ticketFactory
     * @param Configurable $configurable
     * @param StockStateInterface $stockStateInterface
     */
    public function __construct(
        RequestInterface $request,
        Filesystem $filesystem,
        StoreManagerInterface $storeManagerInterface,
        LoggerInterface $logger,
        DealFactory $dealFactory,
        TicketFactory $ticketFactory,
        Configurable $configurable,
        StockStateInterface $stockStateInterface
    ) {
        $this->configurable = $configurable;
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_filesystem = $filesystem;
        $this->_logger = $logger;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->stockStateInterface = $stockStateInterface;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        if ($productId) {
            $modelDeal = $this->deal->create()->load($productId, 'product_id');
            $modelDeal->delete();
            $modelTicket = $this->ticket->create()->load($productId, 'product_id');
            $modelTicket->delete();
        }
    }
}
