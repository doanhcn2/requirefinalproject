<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 04/04/2018
 * Time: 14:08
 */

namespace Magenest\Groupon\Observer\Backend\Product;

use Magenest\Hotel\Model\Product\ScreenShot\Config;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Helper\Data as GrouponHelper;

class BeforeSave implements ObserverInterface
{

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var RequestInterface
     */
    protected $_request;


    /**
     * @var ObjectManager
     */
    protected $_objManager;

    protected $config;

    protected $messageManager;

    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManagerInterface,
        Config $config,
        ManagerInterface $messageManager
    ) {
        $this->storeManage    = $storeManagerInterface;
        $this->config         = $config;
        $this->_request       = $request;
        $this->messageManager = $messageManager;
        $this->_objManager    = ObjectManager::getInstance();
    }


    public function execute(Observer $observer)
    {
        try {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $observer->getEvent()->getProduct();
            $params  = $this->_request->getParams();
            if (!GrouponHelper::isDeal($product->getId()))
                return;
            if (@$params['product']['campaign_status'] == null) {
                $params['product']['campaign_status'] = CampaignStatus::STATUS_PENDING_REVIEW;
                $groupon                              = GrouponHelper::getDeal($product->getId());
                $groupon->setData('status', CampaignStatus::STATUS_PENDING_REVIEW);
                $groupon->save();

                return;
            }
            // must fix 1 after add approve campaign in admin
            if (@$params['product']['campaign_status'] == CampaignStatus::STATUS_APPROVED) {
                GrouponHelper::reload($product, $params);
            }
            /** @var \Magento\Catalog\Model\Product $coupons */
            $coupons = GrouponHelper::getChildren($product->getSku());
            foreach ($coupons as $coupon) {
                $coupon->setStatus($product->getStatus());
                $coupon->setCampaignStatus($product->getCampaignStatus());
                $coupon->getResource()->saveAttribute($coupon, 'campaign_status');
                $coupon->getResource()->saveAttribute($coupon, 'status');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Save Groupon Error"));
        }

        return;
    }

}