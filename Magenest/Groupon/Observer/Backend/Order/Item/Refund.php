<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 14/04/2018
 * Time: 08:34
 */

namespace Magenest\Groupon\Observer\Backend\Order\Item;

use Magenest\Groupon\Model\Groupon;
use Magento\Framework\App\ObjectManager;

class Refund implements \Magento\Framework\Event\ObserverInterface
{

    protected $grouponFactory;

    protected $_coreRegistry;

    public function __construct(
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->_coreRegistry = $registry;
        $this->grouponFactory = $grouponFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * Chage status of groupon when the order item is refunded
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var  $item \Magento\Sales\Model\Order\Item */
            $item = $observer->getItem();
            if ($item->getStatusId() == \Magento\Sales\Model\Order\Item::STATUS_REFUNDED) {
                $orderId = $item->getOrderId();
                $productId = $item->getProductId();
                /** @var  $creditMemo  \Magento\Sales\Model\Order\Creditmemo */
                $creditMemo = $this->_coreRegistry->registry('current_creditmemo');
                $grouponCollection = $this->grouponFactory->create()->getCollection()
                    ->addFieldToFilter('order_id', $orderId)
                    ->addFieldToFilter('product_id', $productId);
                foreach ($grouponCollection as $groupon) {
                    if ($groupon->getData('status') != Groupon::STATUS_CANCELED) {
                        $groupon->setData('status', Groupon::STATUS_CANCELED);
                        $groupon->setData('refunded_at', $creditMemo->getCreatedAt());
                        $groupon->save();
                    }
                }
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->critical($e->getMessage());
        }
        // TODO: Implement execute() method.
    }
}