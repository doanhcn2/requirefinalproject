<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Observer\Backend;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magenest\Ticket\Model\Mail\Template\TransportBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\Mail\MessageInterface;

/**
 * Class GenerateGroupon
 * @package Magenest\Groupon\Observer\Backend
 */
class SendEmailToCustomer implements ObserverInterface
{
    const XML_PATH_EMAIL_SENDER = 'trans_email/ident_general/email';

    const XML_PATH_NAME_SENDER = 'trans_email/ident_general/name';

    protected $logger;

    protected $_message;

    protected $_storeManager;

    protected $_inlineTranslation;

    protected $_scopeConfig;

    protected $deal;

    protected $groupon;

    protected $ticket;

    protected $renderCode;

    protected $messageManager;

    protected $_transportBuilder;

    protected $_request;

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magenest\Groupon\Helper\RenderCode $renderCode,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\RequestInterface $request,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        MessageInterface $message
    ) {
        $this->deal = $dealFactory;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $loggerInterface;
        $this->renderCode = $renderCode;
        $this->messageManager = $messageManager;
        $this->_request = $request;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_message = $message;
    }

    public function execute(Observer $observer)
    {
        $orderId = $observer->getEvent()->getInvoice()->getData('order_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $item){
            if($item->getData('product_type') == 'configurable' ){
                $productOptions = $item->getData('product_options');
                $additionalOptions = $productOptions['additional_options'];
                if($additionalOptions != null && $this->isGiftOptions($additionalOptions)){
                    $data = [
                        'dealOption' => @$additionalOptions[0]['value'],
                        'recipientName' => @$additionalOptions[1]['value'],
                        'recipientEmail' => @$additionalOptions[2]['value'],
                        'message' => @$additionalOptions[3]['value'],
                        'vendor' => @$additionalOptions[4]['value']
                    ];
                    $this->sendEmail($data);
                }
            }
        }
        return;
    }

    public function sendEmail($data){
        $this->_inlineTranslation->suspend();
        $emailTemplate = "groupon_gift_mail_template";

        $this->_message->clearFrom()->clearSubject();

        $transport = $this->_transportBuilder->setTemplateIdentifier($emailTemplate)->setTemplateOptions(
            [
                'area' => Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            [
                'deal_option' => $data['dealOption'],
                'recipient_name' => $data['recipientName'],
                'recipient_email' => $data['recipientEmail'],
                'message' => $data['message'],
                'vendor' => $data['vendor']
            ]
        )->setFrom(
            [
                'email' => $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER),
                'name' => $this->_scopeConfig->getValue(self::XML_PATH_NAME_SENDER)
            ]
        )->addTo(
            $data['recipientEmail'],
            $data['recipientName']
        )->getTransport();
        $transport->sendMessage();
        $this->_inlineTranslation->resume();
    }

    private function isGiftOptions($additionalOptions)
    {
        $result = 0;
        foreach($additionalOptions as $item) {
            if ($item['label'] === "Gift's Recipient Email") {
                $result++;
            }
            if ($item['label'] === "Gift's Recipient Name") {
                $result++;
            }
        }

        return $result == 2 ? true : false;
    }
}
