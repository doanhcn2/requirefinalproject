<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Observer\Vendor;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Groupon\Model\DeliveryFactory;
use Magenest\Groupon\Model\LocationFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;

/**
 * Class Product
 * @package Magenest\Groupon\Observer\Vendor
 */
class Product implements ObserverInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $dealFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magenest\Groupon\Model\Config
     */
    protected $config;

    /**
     * @var DeliveryFactory
     */
    protected $delivery;

    /**
     * @var LocationFactory
     */
    protected $location;

    /**
     * Product constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Magenest\Groupon\Model\Config $config
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\Config $config,
        DeliveryFactory $deliveryFactory
    )
    {
        $this->dealFactory = $dealFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->configurable = $configurable;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->productFactory = $productFactory;
        $this->stockStateInterface = $stockStateInterface;
        $this->location = $locationFactory;
        $this->config = $config;
        $this->delivery = $deliveryFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $status = $product->getStatus();
        $productTypeId = $product->getTypeId();
        $productVisibility = $product->getVisibility();
        $params = $observer->getEvent()->getInformation();
        $store = $product->getStore()->getStoreId();
        if (!empty($params['groupon'])) {
            /**
             * @var $model \Magenest\Groupon\Model\Deal
             */
            $model = $this->deal->create()->load($productId, 'product_id');

            if ($productTypeId == 'configurable' || $productTypeId == 'coupon') {
                $array = $params['groupon'];
                $data['product_type'] = $productTypeId;
                $data['status'] = 2;
                if (isset($productVisibility) && !empty($productVisibility)) {
                    $data['visibility'] = $productVisibility;
                } else {
                    $data['visibility'] = 4;
                }
                $data['vendor_id'] = $this->getVendorId();
                $data['template_id'] = $this->config->getTemplate();
                $data['enable'] = (isset($array['enable_groupon']) && $array['enable_groupon'] == 'on') ? 1 : 0;

                if ($productTypeId == 'coupon') {
                    $data['product_id'] = $productId;
                    $modelConfigurable = $this->configurable->getParentIdsByChild($productId);
                    if (isset($modelConfigurable[0])) {
                        $data['parent_id'] = $modelConfigurable[0];
                    }
                }

                if ($array['enable_groupon'] == 'on') {
                    /** save deal */
                    $data['product_id'] = $productId;
                    $data['store_id'] = $store;
                    $data['redeemable_after'] = @$array['redeemable_after'];
                    if ($params['camp_period'] == 'date') {
                        $data['start_time'] = $array['start_time'];
                    }

                    $data['end_time'] = $array['end_time'];

                    if (isset($array['terms'])) {
                        $term = $this->beforeSaveTermsData($array['terms']);
                    }
                    $data['term'] = json_encode(@$term);
                    $data['groupon_expire'] = $array['groupon_expire'];
                    $data['priority'] = 0;

                    if ($productTypeId == 'coupon') {
                        $cfgProductQty = $this->getCfgProductQty($product->getGrouponOption(), $params['product']['_cfg_attribute']['quick_create']);
                        $data['maximum_buyers_limit'] = (int)@$params['groupon'][$product->getGrouponOption()]['maximum_buyers_limit'];
                        $data['qty'] = $cfgProductQty;
                        if (!$model->getData('purchased_qty')) {
                            $data['purchased_qty'] = 0;
                        }
                        $data['available_qty'] = $cfgProductQty;
                    }
                }

                $model->addData($data)->save();

                if ((isset($array['enable_groupon']) && $array['enable_groupon'] == 'on')) {
                    $dataRedemption['is_business_address'] = 0;
                    $dataRedemption['is_delivery_address'] = 0;
                    if (isset($array['is_online_coupon']) && $array['is_online_coupon'] == 1) {
                        $dataRedemption['is_online_coupon'] = 1;
                        $dataRedemption['location_ids'] = null;
                        $dataRedemption['delivery_ids'] = null;
                    } else {
                        $dataRedemption['is_online_coupon'] = 0;
                        if (isset($array['is_business_address']) && $array['is_business_address'] == 1) {
                            $dataRedemption['is_business_address'] = 1;
                            $dataRedemption['customer_specify_location'] = @$array['customer_specify_location'] ?: 0;
                            $this->saveBusinessAddress($model, @$array['location'], $productTypeId);
                        }
                        if (isset($array['is_delivery_address']) && $array['is_delivery_address'] == 1) {
                            $dataRedemption['is_delivery_address'] = 1;
                            $this->saveDeliveryAddress($model, @$array['delivery_location'], $productTypeId);
                        }
                    }
                    $model->addData($dataRedemption)->save();
                }
            }
        }

        return;
    }

    private function getCfgProductQty($couponOptions, $cfgData)
    {
        if (is_array($cfgData)) {
            foreach($cfgData as $key => $datum) {
                if ($key === '$ROW') continue;
                if ($couponOptions === @$datum['groupon_option']) {
                    if ($datum['udmulti']['stock_qty'] === "") {
                        return 10000;
                    } else {
                        return (int)$datum['udmulti']['stock_qty'];
                    }
                }
            }
        }
        return 0;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    /**
     * @param $deal
     * @param $locations
     * @param $productTypeId
     */
    protected function saveBusinessAddress($deal, $locations, $productTypeId)
    {
        if ($productTypeId == 'configurable') {
            $usedLocations = [];
            $location = $this->location->create();
            if (is_array($locations)) {
                foreach ($locations as $locationData) {
                    $location->load(@$locationData['location_id'] ?: null);
                    unset($locationData['location_id']);
                    $locationData['status'] = '0';
                    $locationData['product_id'] = '0';
                    $locationData['vendor_id'] = $deal->getVendorId();
                    $location->addData($locationData)->save();
                    if (@$locationData['used']) {
                        $usedLocations[] = $location->getId();
                    }
                    $location->unsetData();
                }
            }
            $deal->setData('location_ids', implode(',', $usedLocations));
        }
    }

    /**
     * @param $deal
     * @param $deliveries
     * @param $productTypeId
     */
    protected function saveDeliveryAddress($deal, $deliveries, $productTypeId)
    {
        if ($productTypeId == 'configurable') {
            $usedDeliveryLocations = [];
            $deliveryLocation = $this->delivery->create();
            if (is_array($deliveries)) {
                foreach ($deliveries as $deliveryLocationData) {
                    $deliveryLocation->load(@$deliveryLocationData['delivery_id'] ?: null);
                    unset($deliveryLocationData['delivery_id']);
                    $deliveryLocationData['vendor_id'] = $deal->getVendorId();
                    $deliveryLocation->addData($deliveryLocationData)->save();
                    if (@$deliveryLocationData['used']) {
                        $usedDeliveryLocations[] = $deliveryLocation->getId();
                    }
                    $deliveryLocation->unsetData();
                }
            }
            $deal->setData('delivery_ids', implode(',', $usedDeliveryLocations));
        }
    }

    protected function beforeSaveTermsData($data)
    {
        $result = [];
        if (isset($data['reservation'])
            && count($data['reservation']) > 1
            && isset($data['reservation']['required'])
            && isset($data['reservation']['days'])
        ) {
            $result['reservation']['enable'] = 1;
            $result['reservation']['days'] = $data['reservation']['days'];
        } else {
            $result['reservation']['enable'] = 0;
        }
        if (isset($data['combine'])) {
            $result['combine'] = 1;
        } else {
            $result['combine'] = 0;
        }
        if (isset($data['age_limit']['needed'])
            && count($data['age_limit']) > 1
            && isset($data['age_limit']['age_number'])
        ) {
            $result['age_limit']['enable'] = 1;
            $result['age_limit']['age_number'] = $data['age_limit']['age_number'];
        } else {
            $result['age_limit']['enable'] = 0;
        }
        if (isset($data['sharing'])) {
            $result['sharing'] = 1;
        } else {
            $result['sharing'] = 0;
        }
        if (isset($data['vendor_specify'])) {
            $result['vendor_specify'] = ltrim($data['vendor_specify']);
        }

        return $result;
    }
}
