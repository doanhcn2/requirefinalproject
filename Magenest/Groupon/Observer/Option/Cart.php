<?php

namespace Magenest\Groupon\Observer\Option;

use Magento\Catalog\Model\Plugin\QuoteItemProductOption;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Cart
 * @package Magenest\Groupon\Observer\Option
 */
class Cart implements ObserverInterface
{
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $dealFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;


    /**
     * Cart constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param ProductFactory $productFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        ProductFactory $productFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory
    ) {
        $this->_locationFactory = $locationFactory;
        $this->_productRepository = $productRepository;
        $this->dealFactory = $dealFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->productFactory = $productFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getEvent()->getQuoteItem();
            /** @var Product $product */
            $product = $item->getProduct();
            $productId = $product->getId();
            $data = $this->_request->getParams();
            $checkTypeProduct = $this->_productRepository->getById($productId)->getTypeId();
            if ($checkTypeProduct == 'configurable') {
                if (!@$data['additional_options'])                {
                    $optionsByCode = $product->getCustomOption('info_buyRequest')->getData();
                    $data = json_decode($optionsByCode['value'], true);
                }
                if (isset($data['additional_options'])) {
                    $additionalOptions = [];
                    if (isset($data['selected_configurable_option']) && $data['selected_configurable_option']) {
                        $productId = $data['selected_configurable_option'];
                    }
                    if (isset($data['additional_options']['coupon_selected_children'])) {
                        $additionalOptions[] = [
                            'label' => 'Deal Option',
                            'value' => $this->_productRepository->getById((int)$data['additional_options']['coupon_selected_children'])->getName(),
                        ];
                    }
                    if (isset($data['additional_options']['To'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Name',
                            'value' => $data['additional_options']['To'],
                        ];
                    }
                    if (isset($data['additional_options']['Email'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Email',
                            'value' => $data['additional_options']['Email'],
                        ];
                    }
                    if (isset($data['additional_options']['Message'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Message',
                            'value' => $data['additional_options']['Message'],
                        ];
                    }
                    if (isset($data['additional_options']['redemption_location_id'])) {
                        $additionalOptions[] = [
                            'label' => 'Location',
                            'value' => $this->getGrouponLocation($data['additional_options']['redemption_location_id']),
                        ];
                    }
                    $array = [
                        'code' => 'additional_options',
                        'value' => json_encode($additionalOptions)
                    ];
                    $item->addOption($array);
                }
            }

            if ($checkTypeProduct == 'coupon') {

                if (isset($data['additional_options']['is_gifted'])) {
                    $additionalOptions[] = [
                        'label' => 'Gift to',
                        'value' => $data['additional_options']['To'],
                    ];
                    $additionalOptions[] = [
                        'label' => 'Recipient Email',
                        'value' => $data['additional_options']['Email'],
                    ];
                    $additionalOptions[] = [
                        'label' => 'Message',
                        'value' => $data['additional_options']['Mesage'],
                    ];
                    $array = [
                        'code' => 'additional_options',
                        'value' => json_encode($additionalOptions)
                    ];
                    $item->addOption($array);
                }

            }
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Add to cart exception: ' . $e->getMessage());
        }
    }

    private function getGrouponLocation($locationId)
    {
        $location = $this->_locationFactory->create()->load($locationId);

        $result = "";
        if ($location->getLacationName()) {
            $result .= $location->getLacationName() . " - ";
        }
        $locations = [];
        if ($location->getStreet()) {
            array_push($locations, $location->getStreet());
        } else {
            array_push($locations, $location->getStreetTwo());
        }
        if ($location->getCity()) {
            array_push($locations, $location->getCity());
        }
        if ($location->getRegion()) {
            array_push($locations, $location->getRegion());
        }
        if ($location->getCountry()) {
            $country = ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($location->getCountry());
            array_push($locations, $country);
        }

        return $result . implode(', ', $locations);
    }
}
