<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magenest\Groupon\Model\Quote\Item\QuantityValidator;
use \Magento\Framework\Event\Observer;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Model\DealFactory;

/**
 * Class QuantityValidatorObserver
 * @package Magenest\Groupon\Observer
 */
class QuantityValidatorObserver implements ObserverInterface
{
    /**
     * @var \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $quantityValidator
     */
    protected $quantityValidator;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * QuantityValidatorObserver constructor.
     * @param QuantityValidator $quantityValidator
     * @param LoggerInterface $loggerInterface
     * @param DealFactory $dealFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        QuantityValidator $quantityValidator,
        LoggerInterface $loggerInterface,
        DealFactory $dealFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->logger = $loggerInterface;
        $this->quantityValidator = $quantityValidator;
        $this->deal = $dealFactory;
        $this->_request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
        $quoteItem = $observer->getEvent()->getItem();
        if (!$this->isCoupon($quoteItem->getProduct()->getId())) {
            return;
        }

        $this->quantityValidator->validate($observer);
    }

    public function isCoupon($productId)
    {
        $model = $this->deal->create()->load($productId, 'product_id');
        if ($model->getProductType() == 'coupon') {
            return true;
        }

        return false;
    }
}
