<?php

namespace Magenest\Groupon\Helper;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magento\Framework\App\ObjectManager;

class Data
{
    /**
     * @param $productId
     *
     * @return bool
     */
    public static function isDeal($productId)
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $collection */
        $collection = ObjectManager::getInstance()->create('Magenest\Groupon\Model\ResourceModel\Deal\Collection');
        $model      = $collection->addFieldToFilter('product_id', $productId)->addFieldToFilter('product_type', 'configurable')->getFirstItem();

        return empty($model->getData('product_id')) ? false : true;
    }

    public static function getChildren($sku)
    {
        /** @var \Magento\ConfigurableProduct\Api\LinkManagementInterface $linkManagement */
        $linkManagement = ObjectManager::getInstance()->get(\Magento\ConfigurableProduct\Api\LinkManagementInterface::class);
        /** @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepo */
        $productRepo  = ObjectManager::getInstance()->get(\Magento\Catalog\Api\ProductRepositoryInterface::class);
        $childrenData = $linkManagement->getChildren($sku);
        $return       = [];
        foreach ($childrenData as $datum) {
            try {
                $return[] = $productRepo->get($datum->getSku());
            } catch (\Exception $e) {

            }
        }
        return $return;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $params
     */
    public static function reload($product, $params = [])
    {
        $format   = "Y-m-d H:i:s";
        $oldState = $product->getCampaignStatus();
        $now      = (date($format));
        $groupon  = Data::getDeal($product->getEntityId());
        if (!$groupon->getProductId())
            return;

        if (empty($params)) {
            $params = $groupon->getData();

            $expiredDate = (@$params['groupon_expire']);
            if (empty($expiredDate) || $expiredDate === null)
                $expiredDate = $now;
            else
                $expiredDate = date($format, strtotime($expiredDate));

            $redeemableAfter = (@$params['redeemable_after']);
            if (empty($redeemableAfter) || $redeemableAfter === null)
                $redeemableAfter = $now;
            else
                $redeemableAfter = date($format, strtotime($redeemableAfter));

            $saleStart = (@$params['start_time']);
            if (empty($saleStart) || $saleStart === null)
                $saleStart = $now;
            else
                $saleStart = date($format, strtotime($saleStart));
        } else {
            $expiredDate = (@$params['groupon']['campaign_date']['groupon_expire']);
            if (empty($expiredDate) || $expiredDate === null)
                $expiredDate = $now;
            else
                $expiredDate = date($format, strtotime($expiredDate));

            $redeemableAfter = (@$params['groupon']['campaign_date']['redeemable_after']);
            if (empty($redeemableAfter) || $redeemableAfter === null)
                $redeemableAfter = $now;
            else
                $redeemableAfter = date($format, strtotime($redeemableAfter));

            $saleStart = (@$params['groupon']['campaign_date']['start_time']);
            if (empty($saleStart) || $saleStart === null)
                $saleStart = $now;
            else
                $saleStart = date($format, strtotime($saleStart));
        }

        if ($now > $expiredDate) {
            $newState = CampaignStatus::STATUS_EXPIRED;
            $product->setCampaignStatus(CampaignStatus::STATUS_EXPIRED);
        } else {
            if ($now >= $redeemableAfter) {
                $newState = CampaignStatus::STATUS_ACTIVE;
            } else {
                if ($now >= $saleStart)
                    $newState = CampaignStatus::STATUS_LIVE;
                else
                    $newState = CampaignStatus::STATUS_SCHEDULED;
            }
        }
//        if ($newState != $oldState) {
        $product->setData('campaign_status', $newState);
        $product->getResource()->saveAttribute($product, 'campaign_status');
        $groupon->setData('status', $newState);
        $groupon->save();
//        }
    }

    /**
     * @param $productId
     *
     * @return \Magenest\Groupon\Model\Deal
     */
    public static function getDeal($productId)
    {
        $model = ObjectManager::getInstance()->create('\Magenest\Groupon\Model\Deal')
            ->load($productId, 'product_id');

        return $model;
    }

    public static function getDealDate($id)
    {
        $parentId     = self::getParentId($id);
        $grouponModel = self::getDeal($parentId);

        return
            [
                'from' => $grouponModel->getRedeemableAfter(),
                'to'   => $grouponModel->getGrouponExpire()
            ];
    }

    public static function getDealSaleDate($id)
    {
        $parentId     = self::getParentId($id);
        $grouponModel = self::getDeal($parentId);

        return
            [
                'from' => $grouponModel->getStartTime(),
                'to'   => $grouponModel->getEndTime()
            ];
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function getParentId($id)
    {
        $model = self::getDeal($id);
        if ($model->getData('parent_id'))
            return $model->getData('parent_id');
        else return $id;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function getChildIds($id)
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $collection */
        $collection = ObjectManager::getInstance()->create('Magenest\Groupon\Model\ResourceModel\Deal\Collection');
        $collection->addFieldToFilter('parent_id', $id);

        return $collection->getColumnValues('product_id');
    }

    public static function getVendorIdByProductId($productId)
    {
        $deal = self::getDeal($productId);

        return $deal->getVendorId();
    }

    /**
     * Get groupon location by location_ids field
     * Return data: Location Name - Location City, Location Region, Location Country
     *
     * @param $locationIds
     *
     * @return string
     */
    public static function getGrouponLocation($locationIds)
    {
        $locations = explode(',', $locationIds);
        foreach ($locations as $location) {
            if ($location !== null && $location !== '') {
                $locationId = $location;
            }
        }
        if (!isset($locationId)) {
            return "";
        }
        $locationData    = ObjectManager::getInstance()->create('Magenest\Groupon\Model\Location')->load($locationId);
        $locationDetails = "";
//        if ($locationData->getLocationName()) {
//            $locationDetails .= $locationData->getLocationName() . " - ";
//        }
        if ($locationData->getRegion()) {
            $locationDetails .= $locationData->getRegion() . ", ";
        } elseif ($locationData->getCity()) {
            $locationDetails .= $locationData->getCity() . ", ";
        }
        if ($locationData->getCountry()) {
            $locationDetails .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($locationData->getCountry());
        }

        return $locationDetails;
    }

    public static function getGrouponLocationById($locationId)
    {
        $locationData    = ObjectManager::getInstance()->create('Magenest\Groupon\Model\Location')->load($locationId);
        $locationDetails = "";
//        if ($locationData->getLocationName()) {
//            $locationDetails .= $locationData->getLocationName() . " - ";
//        }
        if ($locationData->getRegion()) {
            $locationDetails .= $locationData->getRegion() . ", ";
        } elseif ($locationData->getCity()) {
            $locationDetails .= $locationData->getCity() . ", ";
        }
        if ($locationData->getCountry()) {
            $locationDetails .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($locationData->getCountry());
        }

        return $locationDetails;
    }

    public static function setCategoryDiscountPercent($categories)
    {
        $objectManager    = ObjectManager::getInstance();
        $discountCategory = $objectManager->create('Unirgy\Dropship\Helper\Data')->getScopeConfig('udprod_groupon/deal_offer/offer_per_category');
        $discountPercents = json_decode($discountCategory, true);
        if (is_array($discountPercents)) {
            $vendorSession = $objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession) {
                $vendorSession = $objectManager->create('Unirgy\Dropship\Model\Session');
            }
            if ($vendorSession->hasCategoryDiscountPercent()) {
                $vendorSession->unsCategoryDiscountPercent();
            }
            $lowest        = [];
            $currentLength = -1;
            foreach ($categories as $categoryId) {
                $categoryPath = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryId)->getPath();
                $pathCount    = explode('/', $categoryPath);
                if (count($pathCount) > $currentLength) {
                    $lowest        = [$categoryId];
                    $currentLength = count($pathCount);
                } elseif (count($pathCount) === $currentLength) {
                    $lowest[] = $categoryId;
                }
            }

            foreach ($discountPercents as $item) {
                if (in_array($item['category'], $lowest)) {
                    $data = ['discount_from' => $item['discount_from'], 'discount_to' => $item['discount_to']];
                    $vendorSession->setCategoryDiscountPercent(json_encode($data));
                    break;
                }
            }
        }
    }
}
