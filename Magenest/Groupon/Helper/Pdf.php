<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Helper;

use Magenest\Groupon\Helper\Data as GrouponHelper;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\TemplateFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magenest\Ticket\Helper\Information as EventHelper;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Model\EventLocation;
use Magento\Catalog\Model\ProductFactory;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Pdf
 *
 * @package Magenest\Ticket\Helper
 */
class Pdf extends AbstractHelper
{
    const TYPE_GROUPON = 'groupon';
    const TYPE_TICKET = 'ticket';
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Core string
     *
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    protected $string;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $fileFramework;

    /**
     * @var Template
     */
    protected $template;

    /**
     * @var OptionFactory
     */
    protected $option;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var TemplateFactory
     */
    protected $templateFactory;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var ProductFactory
     */
    protected $product;

    /**
     * @var OrderFactory
     */
    protected $order;

    /**
     * @var Country
     */
    protected $country;

    /** @var \Magenest\Groupon\Model\Groupon | \Magenest\Ticket\Model\Ticket */
    protected $model;

    protected $type;

    /**
     * Pdf constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param File $fileFramework
     * @param Template $template
     * @param GrouponFactory $grouponFactory
     * @param TicketFactory $ticketFactory
     * @param DealFactory $dealFactory
     * @param ProductFactory $productFactory
     * @param OrderFactory $orderFactory
     * @param TemplateFactory $templateFactory
     * @param CountryFactory $country
     * @param \Magento\Framework\Stdlib\StringUtils $string
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        File $fileFramework,
        Template $template,
        GrouponFactory $grouponFactory,
        TicketFactory $ticketFactory,
        DealFactory $dealFactory,
        ProductFactory $productFactory,
        OrderFactory $orderFactory,
        TemplateFactory $templateFactory,
        CountryFactory $country,
        \Magento\Framework\Stdlib\StringUtils $string
    ) {
        parent::__construct($context);
        $this->string = $string;
        $this->_storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->fileFramework = $fileFramework;
        $this->template = $template;
        $this->templateFactory = $templateFactory;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->deal = $dealFactory;
        $this->product = $productFactory;
        $this->order = $orderFactory;
        $this->country = $country;
    }

    /**
     * Print PDF Template Preview
     *
     * @param null $templateId
     * @param null $model
     * @param string $type
     * @return \Zend_Pdf
     * @throws \Exception
     */
    public function getPdf($templateId = null, $model = null, $type = self::TYPE_GROUPON)
    {
        try {
            if ($model != null) {
                $this->setModel($model);
                $this->setType($type);
            }
            if ($templateId == null) {
                if ($type == self::TYPE_GROUPON)
                    $templateId = $this->scopeConfig->getValue('groupon/vendor_config/template');
                else
                    $templateId = $this->scopeConfig->getValue('event_ticket/vendor_config/template');
            }
            $modelTicket = $this->templateFactory->create()->load($templateId, 'template_id');

            $data = $modelTicket->getData();
            $coordinates = [];
            if (isset($data['pdf_coordinates'])) {
                if ($data['pdf_coordinates']) {
                    $coordinates = unserialize($data['pdf_coordinates']);
                }
                $backgroundLink = unserialize($data['pdf_background']);
            }
            $pdf = new \Zend_Pdf();
            $fontRegular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);

            if (isset($data['pdf_page_width'])) {
                $width = $data['pdf_page_width'];
            } else {
                $width = 600;
            }
            if (isset($data['pdf_page_height'])) {
                $height = $data['pdf_page_height'];
            } else {
                $height = 800;
            }

            $size = $width . ':' . $height;
            $page = $pdf->newPage($size);
            if (isset($backgroundLink) && !empty($backgroundLink)) {
                $background = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('groupon/template/' . $backgroundLink['0']['file']);
                if (is_file($background)) {
                    $image = \Zend_Pdf_Image::imageWithPath($background);
                    $page->drawImage($image, 0, 0, $width, $height);
                }
            }

            $page->setFont($fontRegular, 15);
            $tableRowsArr = $coordinates;

            $path = $this->filesystem->getDirectoryRead(
                DirectoryList::MEDIA
            );

            if ($tableRowsArr != null) {
                foreach($tableRowsArr as $param) {
                    /**
                     * Insert QR Code to PDF File
                     */
                    if (!empty($param['info']) && $param['info'] == 'qr_code'
                        && !empty($param['x'])
                        && !empty($param['y'])
                        && !empty($param['size'])
                    ) {
                        $code = $this->replaceByText($param['info']);
                        $fileName = $this->getQrCode($code);
                        $pathQrcode
                            = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)
                            ->getAbsolutePath($fileName);
                        $image
                            = \Zend_Pdf_Image::imageWithPath($pathQrcode);
                        foreach(
                            $param['content']['content_collection'] as $child
                        ) {
                            $page->drawImage(
                                $image,
                                $child['x'],
                                $child['y'],
                                $child['x'] + $child['size'],
                                $child['y'] + $child['size']
                            );
                        }
                        if ($path->isFile($fileName)) {
                            $this->filesystem->getDirectoryWrite(
                                DirectoryList::MEDIA
                            )->delete($fileName);
                        }

                        continue;
                    }

                    /**
                     * Insert Barcode to PDF File
                     */
                    if (!empty($param['info'])
                        && $param['info'] == 'bar_code'
                    ) {
                        $code = $this->replaceByText($param['info']);
                        $barcodeOptions = [
                            'text' => $code,
                            'drawText' => false
                        ];
                        $rendererOptions = [];
                        foreach(
                            $param['content']['content_collection'] as $child
                        ) {
                            $imageResource = \Zend_Barcode::draw("code128",
                                'image', $barcodeOptions, $rendererOptions);
                            $barcode
                                = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)
                                ->getAbsolutePath('barcode.jpg');
                            $rotate = $child['rotate'] == '0'
                                ? $imageResource
                                : imagerotate($imageResource, $child['rotate'],
                                    0);
                            imagejpeg($rotate, $barcode, 100);
                            imagedestroy($imageResource);
                            $image = \Zend_Pdf_Image::imageWithPath($barcode);
                            $page->drawImage(
                                $image,
                                $child['x'],
                                $child['y'],
                                $child['x2'],
                                $child['y2']
                            );
                        }
                        if ($path->isFile('barcode.jpg')) {
                            $this->filesystem->getDirectoryWrite(
                                DirectoryList::MEDIA
                            )->delete('barcode.jpg');
                        }

                        continue;
                    }

                    /**
                     * Insert other element
                     */
                    if (!empty($param['info']) && !empty($param['x'])
                        && !empty($param['y'])
                        && !empty($param['size'])
                        && !empty($param['color'])
                    ) {
                        $page->setFont($fontRegular, $param['size']);
                        $color = new \Zend_Pdf_Color_Html('#' . $param['color']);
                        $page->setFillColor($color);
                        $title = $param['title'];
                        $page->drawText(
                            $title,
                            $param['x'],
                            $param['y'],
                            'UTF-8'
                        );
                        $isPrintMoreLocation = false;

                        foreach(
                            $param['content']['content_collection'] as
                            $child
                        ) {
                            $page->setFont($fontRegular, $child['size']);
                            $color = new \Zend_Pdf_Color_Html('#' . $child['color']);
                            $page->setFillColor($color);

                            $text = $this->replaceByText($param['info']);
                            if ($param['info'] === 'redeem_at' && $isPrintMoreLocation) {
                                $text = $this->getSecondLocationCoupon();
                            }
                            $textArr = $this->string->split($text, (int)$child['length'], true, true);
                            if (count($textArr) > 0) {
                                for($i = 0; $i < count($textArr); $i++) {
                                    $page->drawText(
                                        $textArr[$i],
                                        $child['x'],
                                        $child['y'] - $i
                                        * ((int)$child['size'] + 2),
                                        'UTF-8'
                                    );
                                }
                            } else {
                                $page->drawText(
                                    $text,
                                    $child['x'],
                                    $child['y'],
                                    'UTF-8'
                                );
                            }
                            if ($param['info'] === 'redeem_at') {
                                if ($this->type === self::TYPE_TICKET) {
                                    break;
                                } elseif ($this->type === self::TYPE_GROUPON && !$isPrintMoreLocation) {
                                    if ($model) {
                                        $count = count($this->getGrouponLocations($model));
                                        if ($count > 2) {
                                            $locationText = (int)($count - 2) == 1 ? "location" : "locations";
                                            $colorLocations = new \Zend_Pdf_Color_Html('#535760');
                                            $page->setFillColor($colorLocations);
                                            $page->drawText(
                                                "+ " . (int)($count - 2)
                                                . " more " . $locationText . ".",
                                                $child['x'],
                                                $child['y'] - ((int)@$i + 1)
                                                * (int)$child['size'] + 2,
                                                'UTF-8'
                                            );
                                        }
                                    } else {
                                        $page->drawText(
                                            "+ 3 more location(s).",
                                            $child['x'],
                                            $child['y'] - ($i + 1) * (int)$child['size']
                                            + 2,
                                            'UTF-8'
                                        );
                                    }
                                    $isPrintMoreLocation = true;
                                }
                            }
                        }
                    }
                }
            }
            $pdf->pages[] = $page;
            $this->setModel(null);
            $this->setType(null);
            return $pdf;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    public function getSecondLocationCoupon()
    {
        $locations = $this->getGrouponLocations();
        array_shift($locations);

        return $this->preparePdfLocation($locations);
    }

    /**
     * @param $info
     * @return array|string
     */
    public function replaceByText($info)
    {
        switch ($this->type) {
            case  self::TYPE_GROUPON :
                return $this->replaceByGroupon($info);
            case  self::TYPE_TICKET :
                return $this->replaceByTicket($info);
            default :
                switch ($info) {
                    case 'event_name':
                        return '40% off Triangle Colorful Women\'s Swimsuit ($18 instead of $30)';
                    case 'location_title':
                        return 'Theater';
                    case 'location  _detail':
                        return 'California, USA';
                    case 'date':
                        return '16/11/2016';
                    case 'start_time':
                        return '8:00';
                    case 'end_time':
                        return '11:00';
                    case 'type':
                        return 'Adult';
                    case 'customer_name':
                        return 'Magenest JSC';
                    case 'customer_email':
                        return 'example@gmail.com';
                    case 'order_increment_id':
                        return '00000026';
                    case 'qty':
                        return '6';
                    case 'vendor_name':
                        return 'Gosawa';
                    case 'qr_code':
                    case 'bar_code':
                    case 'redemption_code':
                        return '22233221279';
                    case 'groupon_code':
                        return '9d22ac391fd0';
                    case 'price':
                        return '$98.75';
                    case 'valid_date_range':
                        return 'Valid from xxxxxxxxxxxxxx until  xxxxxxxxxxxxxx';
                    case 'redeem_at':
                        return 'Boulevard Emile Lahoud, Hadath, , LB, 05 458 000';
                    case 'term':
                        return 'Coupons valid until July 17, 2018. May redeem multiple coupons per person. Coupons can be combined for a longer stay. Extra beds are available for an extra $20 charge. Extra $20 charge for chimney wood. 24-Hour Check-in and Check-out. Reservation required on 04 296 696 or 81 256 777, quote Gosawa coupon. Cancellation Policy: reservations must be cancelled 48 hours prior, otherwise coupons are considered as used. No carry over. VAT included in price. Standard terms & conditions apply.';
                    default:
                        return '';
                }
        }
    }

    public function replaceByGroupon($info)
    {
        $model = $this->getModel();
        switch ($info) {
            case 'event_name':
                return $model->getData('product_name');
            case 'price':
                return number_format((float)$model->getData('price'), 2);
            case 'qr_code':
            case 'bar_code':
            case 'redemption_code':
                return $model->getData('redemption_code');
            case 'groupon_code':
                return $model->getData('groupon_code');
            case 'customer_name':
                return $model->getData('customer_name');
            case 'customer_email':
                return $model->getData('customer_email');
            case 'order_increment_id':
                return $model->getData('order_increment_id');
            case 'date':
                return $model->getData('created_at');
            case 'qty':
                return $model->getData('qty');
            case 'vendor_name':
                return $this->getVendorName($model->getData('vendor_id'));
            case 'valid_date_range':
                return $this->getDate($model->getData('product_id'));
            case 'redeem_at':
                return $this->getLocation();
            case 'term':
                return $this->getGrouponTerm($model->getData('product_id'));
            default:
                return '';
        }
    }

    protected function getGrouponTerm($id)
    {
        $dealId = GrouponHelper::getParentId($id);
        $deal = GrouponHelper::getDeal($dealId);
        $term = $deal->getData('term');
        $customerSpecifyLocation = ($deal['is_online_coupon'] == 0) ? $deal['customer_specify_location'] : 0;
        if (empty($term) || !is_array(json_decode($term, true)))
            return "";
        else {
            $term = json_decode($term, true);
            $data = "";
            if (isset($term)) {
                $data = "";
                if (@$term['reservation']['enable'] === '1' || @$term['reservation']['enable'] === 1) {
                    $data .= "Reservation required " . @$term['reservation']['days'] . " days in advance. ";
                }
                if (@$term['combine'] === '1' || @$term['combine'] == 1) {
                    $data .= "May combine coupons for greater value. ";
                }
                if (@$term['age_limit']['enable'] == 1 || @$term['age_limit']['enable'] === '1') {
                    $data .= "No coupon required for kids below " . @$term['age_limit']['age_number'] . " years of age. ";
                }
                if (@$term['sharing'] == 1 || @$term['sharing'] === '1') {
                    $data .= "Coupons cannot be share. ";
                }
                if (@$term['vendor_specify'] && trim($term['vendor_specify']) !== "") {
                    $data .= $term['vendor_specify'];
                }

                if ($customerSpecifyLocation) {
                    $data .= ' Customer need to specify a redemption location on purchase';
                }
            }
        }

        return $this->trimText($data, 360);
    }

    public function trimText($input, $length, $ellipses = true, $strip_html = true)
    {
        if ($strip_html) {
            $input = strip_tags($input);
        }
        if (strlen($input) <= $length) {
            return $input;
        }
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);
        if ($ellipses) {
            $trimmed_text .= '...';
        }
        return $trimmed_text;
    }

    /**
     * @param $model
     * @param $info
     * @return string
     */
    public function replaceByTicket($info)
    {
        $model = $this->getModel();
        switch ($info) {
            case 'event_name':
                return $model->getData('title');
            case 'price':
                return number_format((float)$this->getPrice($model->getData('order_item_id')), 2);
            case 'qr_code':
            case 'bar_code':
            case 'redemption_code':
                return $model->getData('redemption_code');
            case 'groupon_code':
                return $model->getData('code');
            case 'customer_name':
                return $model->getData('customer_name');
            case 'customer_email':
                return $model->getData('customer_email');
            case 'order_increment_id':
                return $model->getData('order_increment_id');
            case 'date':
                return $model->getData('created_at');
            case 'qty':
                return $model->getData('qty');
            case 'vendor_name':
                return $this->getVendorName($model->getData('vendor_id'));
            case 'valid_date_range':
                return $this->getDate($model->getData('product_id'));
            case 'redeem_at':
                return $this->getLocation();
            case 'term':
                return $this->getTicketTerm($model->getData('event_id'));
            default:
                return '';
        }
    }

    /**
     * @return \Magenest\Groupon\Model\Groupon|\Magenest\Ticket\Model\Ticket
     */
    protected function getModel()
    {
        return $this->model;
    }

    /**
     * @param $model
     */
    protected function setModel($model)
    {
        if ($model) {
            $order = $this->order->create()->load($model->getOrderId());
            $model->setData('order_increment_id', $order->getIncrementId());
        }
        $this->model = $model;
    }

    protected function getPrice($orderItemId)
    {
        $model = ObjectManager::getInstance()->create(\Magento\Sales\Model\Order\Item::class);
        $model = $model->load($orderItemId);
        return $model->getBasePrice();

    }

    protected function getVendorName($vendorId)
    {
        /** @var \Unirgy\Dropship\Model\Vendor $model */
        $model = ObjectManager::getInstance()->create(\Unirgy\Dropship\Model\Vendor::class)->load($vendorId);
        return $model->getData('vendor_name');
    }

    /**
     * @param $productId
     * @return string
     */
    protected function getDate($productId)
    {
        if ($this->type == self::TYPE_GROUPON)
            $date = GrouponHelper::getDealDate($productId);
        else
            $date = EventHelper::getSession($productId);
        $text = '';
        if (@$date['from'] || @$date['to'])
            $text = "Valid";

        if (@$date['from'])
            $text .= " from " . date('F d, Y', strtotime($date['from']));
        if (@$date['to'])
            $text .= " until " . date('F d, Y', strtotime($date['to']));
        return $text;
    }

    /**
     * @return string
     */
    protected function getLocation()
    {
        if ($this->type == self::TYPE_GROUPON)
            $locations = $this->getGrouponLocations();
        else {
            /** @var Event $eventModel */
            $eventModel = ObjectManager::getInstance()->create(Event::class)->load($this->getModel()->getEventId());
            $locationModel = ObjectManager::getInstance()->create(EventLocation::class);
            $location = $locationModel->load($this->getModel()->getProductId(), 'product_id');
            if ($eventModel->getIsOnline() == '0')
                return __('Online Event');
            $locations = $this->getTicketLocations($location);
        }
        return $this->preparePdfLocation($locations);
    }

    protected function getGrouponLocations()
    {
        $locationId = $this->getModel()->getData('redemption_location_id');
        if ($locationId) {
            $locations = ObjectManager::getInstance()->create(\Magenest\Groupon\Model\Location::class);
            return [$locations->load($locationId)->getData()];
        } else {
            $productId = $this->getModel()->getData('product_id');
            $dealId = GrouponHelper::getParentId($productId);
            $deal = GrouponHelper::getDeal($dealId);
            $location = null;
            if ($deal->getDealId()) {
                return $locationData = $deal->getLocationData();
            }
        }
        return [];
    }

    protected function getTicketLocations($location)
    {
        if ($location->getId()) {
            $locationData = [
                'location_name' => $location->getVenueTitle(),
                'street' => $location->getAddress(),
                'city' => $location->getCity(),
                'region' => $location->getState(),
                'country' => $location->getCountry(),
                'postcode' => $location->getp(),
            ];
            return [$locationData];
        } else
            return [];
    }

    private function preparePdfLocation($location)
    {
        $result = "";
        if (isset($location[0]['location_name'])) {
            $result .= $location[0]['location_name'] . ", ";
        }
        if (isset($location[0]['street'])) {
            $result .= $location[0]['street'];
            if (isset($location[0]['city'])) {
                $result .= ", " . $location[0]['city'];
            }
        } elseif (isset($location[0]['city'])) {
            $result .= $location[0]['city'];
        }
        if (isset($location[0]['region'])) {
            $result .= " , " . $location[0]['region'];
            if (isset($location[0]['country'])) {
                $result .= ", " . $this->getCountryNameByCode($location[0]['country']);
            }
        } elseif (isset($location[0]['country'])) {
            $result .= " , " . $this->getCountryNameByCode($location[0]['country']);
        }
        if (isset($location[0]['phone'])) {
            $result .= " , " . $location[0]['phone'];
        }

        return $result;
    }

    public function getCountryNameByCode($code)
    {
        $_hlp = ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event');

        return $_hlp->getCountryNameByCode($code);
    }

    protected function getTicketTerm($id)
    {
        $result = "";
        /** @var Event $event */
        $event = ObjectManager::getInstance()->create(Event::class)->load($id);

        if ($event->getShowUp() == 1 && $event->getShowUpTime())
            $result .= 'Must Show up before session start ' . $event->getShowUpTime() . ' minutes before start of session. ';
        if ($event->getCanPurchase() == 1)
            $result .= 'Additional Tickets can be purchased at venue (full price and upon availability). ';

        if ($event->getMustPrint() == 1)
            $result .= 'Tickets must be printed. ';
        if ($event->getAge())
            $result .= 'Age Suitability: ' . $event->getAge() . ". ";
        return $result;
    }

    public function getQrCode($code)
    {
        $url = "http://api.qrserver.com/v1/create-qr-code/?&size=120x120&data=" . $code;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        $path = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath("qr_" . $code . ".png");
        if (file_exists($path)) {
            unlink($path);
        }
        $fp = $this->fileFramework->fileOpen($path, 'x');
        $this->fileFramework->fileWrite($fp, $raw);
        $this->fileFramework->fileClose($fp);
        $file = 'qr_' . $code . ".png";

        return $file;
    }

    /**
     * @return mixed
     */
    protected function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    protected function setType($type)
    {
        $this->type = $type;
    }
}
