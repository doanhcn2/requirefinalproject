<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magenest\Groupon\Helper\Data;

class Coupon extends AbstractHelper
{
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public static function reloadDealData($productId, $dealId = null, $sold = null, $revenue = null, $commission = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /**
         * @var $grouponCollection \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
         */
        $grouponCollection = $objectManager->create('Magenest\Groupon\Model\ResourceModel\Groupon\Collection');

        if (Data::isDeal($productId)) {
            $childIds = $objectManager->create('Magenest\Groupon\Model\Deal')->getCollection()
                ->addFieldToFilter('parent_id', $productId)->getColumnValues('product_id');
            $grouponCollection->addFieldToFilter('product_id', ['in' => $childIds]);
        } else {
            $grouponCollection->addFieldToFilter('product_id', $productId);
            $productId = $objectManager->create('Magenest\Groupon\Model\Deal')->load($productId, 'product_id')->getParentId();
        }
        if (!$sold) {
            $sold = array_sum($grouponCollection->getColumnValues('qty'));
        }
        if (!$revenue) {
            $revenue = array_sum($grouponCollection->getColumnValues('price'));
        }
        if (!$commission) {
            $commission = array_sum($grouponCollection->getColumnValues('commission'));
        }
        $dealInfo = $objectManager->create('Magenest\Groupon\Model\DealInfo')->load($productId, 'product_id');
        if ($dealInfo->getId()) {
            $dealInfo->setSold($sold)->setCommissions($commission)->setRevenue($revenue)->save();
        } else {
            if (!$dealId) {
                $dealId = $objectManager->create('Magenest\Groupon\Model\Deal')->load($productId, 'product_id')->getId();
            }
            $data = [
                'deal_id' => $dealId,
                'product_id' => $productId,
                'sold' => $sold,
                'revenue' => $revenue,
                'commissions' => $commission
            ];
            $objectManager->create('Magenest\Groupon\Model\DealInfo')->setData($data)->save();
        }
    }

    public static function reloadDealLocations($productId) {

    }
}
