<?php

namespace Magenest\Groupon\Helper;

/**
 * Class Status
 * @package Magenest\Groupon\Helper
 */
/**
 * Class Status
 * @package Magenest\Groupon\Helper
 */
class Status extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var
     */
    protected $_templateId;

    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->_scopeConfig = $context;
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
    }

    /**
     * @return mixed
     */
    public function getCurrentDate(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        return $objDate->date()->format('Y-m-d');
    }

    /**
     * @return mixed
     */
    public function getGrouponDealCollection(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $grouponDealCollection = $objectManager->get('Magenest\Groupon\Model\ResourceModel\Deal\Collection');
        $grouponDealCollection->addFieldToSelect('*');
        $grouponDealCollection->addFieldToFilter('groupon_expire', array('like' => $this->getCurrentDate()));
        return $grouponDealCollection;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getGrouponCollectionByProductId($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $grouponCollection = $objectManager->create('Magenest\Groupon\Model\ResourceModel\Groupon\Collection');
        $grouponCollection->addFieldToSelect('*');
        $grouponCollection->addFieldToFilter('product_id', array('eq' => (int)$id));
        return $grouponCollection;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getExpireDateByProductId($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $grouponDeal = $objectManager->get('Magenest\Groupon\Model\Deal')->load($id, 'product_id');
        return $grouponDeal->getData('groupon_expire');
    }

    /**
     *
     */
    public function changeStatus(){
        $grouponDealCollection = $this->getGrouponDealCollection();
        foreach ($grouponDealCollection as $grouponDeal){
            $grouponCollection = $this->getGrouponCollectionByProductId($grouponDeal->getData('product_id'));
            if($grouponCollection != null){
                foreach ($grouponCollection as $groupon){
                    $groupon->setData('status', 3);
                    $groupon->save();
                }
            }
        }
    }

}