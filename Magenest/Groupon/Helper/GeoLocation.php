<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Groupon\Helper;


class GeoLocation
{
    const XML_PATH_CONFIG_GOOGLE_MAP_API_KEY = 'google/google_map/google_map_key';
    const MILESTOKM = 1609.344;

    /**
     * @var \Unirgy\Dropship\Helper\Data $_hlp
     */
    protected $_hlp;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var $grouponFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_eventLocationFactory \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $_eventLocationFactory;

    /**
     * GeoLocation constructor.
     * @param \Unirgy\Dropship\Helper\Data $helper
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     */
    public function __construct(
        \Unirgy\Dropship\Helper\Data $helper,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
    ) {
        $this->_eventLocationFactory = $eventLocationFactory;
        $this->_locationFactory = $locationFactory;
        $this->_eventFactory = $eventFactory;
        $this->_dealFactory = $dealFactory;
        $this->_hlp = $helper;
    }

    /**
     * Get min distance of a groupon product (may has many location(s))
     *
     * @param $startLat
     * @param $startLng
     * @param $productId
     * @return bool|mixed
     */
    public function getGrouponProductDistance($startLat, $startLng, $productId)
    {
        $groupon = $this->_dealFactory->create()->load($productId, 'product_id');
        if ($groupon->getProductType() === 'coupon') {
            $parentId = $groupon->getParentId();
            $groupon = $this->_dealFactory->create()->load($parentId, 'product_id');
        }
        $locationIds = $groupon->getLocationIds();
        $ids = explode(',', $locationIds);
        $distances = [];
        $locations = [];
        foreach($ids as $id) {
            if (isset($id)) {
                if ($dis = $this->getCouponLocationDistance($startLat, $startLng, $id, true)) {
                    array_push($distances, $dis['distance']);
                    array_push($locations, $dis['location']);
                }
            }
        }
        if (count($distances) > 0) {
            $distanceCount = array_search(min($distances), $distances);
            if ($distanceCount) {
                return [
                    'distance' => $this->convertDistanceToMiles(min($distances)),
                    'location' => $locations[$distanceCount]
                ];
            }
            return ['distance' => $this->convertDistanceToMiles(min($distances))];
        }

        return false;
    }

    public function getTicketProductDistance($startLat, $startLng, $productId)
    {
        $eventLocation = $this->_eventLocationFactory->create()->load($productId, 'product_id');
        $eventLng = $eventLocation->getLongitude();
        $eventLat = $eventLocation->getLatitude();
        if (isset($eventLng) && isset($eventLat)) {
            $distance = $this->getDistanceByCoordinates($startLat, $startLng, $eventLat, $eventLng);
            if ($distance) {
                return $this->convertDistanceToMiles($distance);
            }
        } else {
            $street = $eventLocation->getAddress() ?: $eventLocation->getAddress2();
            $distance = $this->getDistanceByAddress($startLat, $startLng, $street, $eventLocation->getCity(), $eventLocation->getState(), $eventLocation->getCountry(), $eventLocation->getPostcode());
            if ($distance) {
                return $this->convertDistanceToMiles($distance);
            }
        }
        return false;
    }

    public function getCouponLocationDistance($startLat, $startLng, $locationId, $inFunction = false)
    {
        $location = $this->_locationFactory->create()->load($locationId);
        if ($maps = $location->getMapInfo()) {
            $coordinates = explode(',', $maps);
            /**
             *  First is lat, second is lng
             */
            $dis = $this->getDistanceByCoordinates($startLat, $startLng, $coordinates[0], $coordinates[1]);
            if ($dis) {
                return $inFunction ? ['distance' => $dis, 'location' => $location] : $this->convertDistanceToMiles($dis);
            }
        } else {
            /*
             * No map info specified
             */
            $street = $location->getStreet() ?: $location->getStreetTwo();
            $distanceByAddress = $this->getDistanceByAddress($startLat, $startLng, $street, $location->getCity(), $location->getRegion(), $location->getCountry(), $location->getPostcode());
            if ($distanceByAddress) {
                return $inFunction ? ['distance' => $distanceByAddress, 'location' => $location] : $this->convertDistanceToMiles($distanceByAddress);
            }
        }

        return false;
    }

    protected function getGoogleMapApiKey()
    {
        return trim($this->_hlp->getScopeConfig(self::XML_PATH_CONFIG_GOOGLE_MAP_API_KEY));
    }

    /**
     * Get coordinate of an address
     *
     * @param null $street
     * @param null $city
     * @param null $region
     * @param null $country
     * @param null $zip
     * @return array|bool
     */
    private function getCoordinates($street = null, $city = null, $region = null, $country = null, $zip = null)
    {
        $address = urlencode($street . ',' . $city . ',' . $region . ' ' . $zip . ',' . $country);
        $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&key=" . $this->getGoogleMapApiKey();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $status = $response_a->status;

        if ($status == 'ZERO_RESULTS') {
            return false;
        } else {
            $result = array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
            return $result;
        }
    }

    private function getDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . trim($lat1) . "," . trim($long1) . "&destinations=" . trim($lat2) . "," . trim($long2) . "&mode=driving&key=" . $this->getGoogleMapApiKey();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        if ($response_a['status'] !== "OK" || $response_a['rows'][0]['elements'][0]['status'] === "ZERO_RESULTS") {
            return false;
        }
        if (isset($response_a['rows'][0]['elements'][0]['distance']['value'])) {
            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            return $dist;
        } else {
            return false;
        }
    }

    /**
     * Get distance between an address and user's current location
     *
     * @param $startLat
     * @param $startLng
     * @param null $street
     * @param null $city
     * @param null $region
     * @param null $country
     * @param null $zip
     * @return bool
     */
    protected function getDistanceByAddress($startLat, $startLng, $street = null, $city = null, $region = null, $country = null, $zip = null)
    {
        $destinationCoordinates = $this->getCoordinates($street, $city, $region, $country, $zip);
        if (!$destinationCoordinates) {
            return false;
        }

        return $this->getDrivingDistance($startLat, $destinationCoordinates['lat'], $startLng, $destinationCoordinates['long']);
    }

    /**
     * Get distance between two coordinates in map
     *
     * @param $startLat
     * @param $startLng
     * @param $desLat
     * @param $desLng
     * @return bool
     */
    protected function getDistanceByCoordinates($startLat, $startLng, $desLat, $desLng)
    {
        return $this->getDrivingDistance($startLat, $desLat, $startLng, $desLng);
    }

    public function convertDistanceToMiles($distance)
    {
        $miles = $distance / self::MILESTOKM;

        return number_format($miles, 1) . " miles away";
    }

    /**
     * Sample data for checking
     *
     * @return mixed
     */
    public function getLocationSample()
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=42.025819,-88.0854497&destinations=41.910088,-87.6929887&key=" . $this->getGoogleMapApiKey();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
