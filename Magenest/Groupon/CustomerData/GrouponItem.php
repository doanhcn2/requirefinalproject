<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 27/09/2018
 * Time: 20:16
 */

namespace Magenest\Groupon\CustomerData;


class GrouponItem extends \Magento\ConfigurableProduct\CustomerData\ConfigurableItem
{
    protected function doGetItemData()
    {
        $itemData = parent::doGetItemData();
        if (isset($itemData['is_visible_in_site_visibility'])) {
            $itemData['is_visible_in_site_visibility'] = $itemData['is_visible_in_site_visibility'] && !\Magenest\Groupon\Helper\Data::isDeal($this->item->getProduct()->getId());
        }

        return $itemData;
    }
}