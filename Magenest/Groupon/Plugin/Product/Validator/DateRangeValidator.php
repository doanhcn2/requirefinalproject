<?php
/**
 * Author: Eric Quach
 * Date: 3/12/18
 */
namespace Magenest\Groupon\Plugin\Product\Validator;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\RequestInterface;

class DateRangeValidator
{
    public function afterValidate(
        $subject,
        $result,
        Product $product,
        RequestInterface $request,
        \Magento\Framework\DataObject $response
    ) {
        if ($grouponData = $request->getParam('groupon')) {
            $startTime = @$grouponData['campaign_date']['start_time'];
            $endTime = @$grouponData['campaign_date']['end_time'];
            $redeemAfter = @$grouponData['campaign_date']['redeemable_after'];
            $grouponExpire = @$grouponData['campaign_date']['groupon_expire'];
            if (!empty($startTime) && !empty($endTime)) {
                if (strtotime($startTime) > strtotime($endTime)) {
                    $response->setError(
                        true
                    )->setMessage(
                        __('Campaign Start Date must be before End Date')
                    );
                }
            }

            if (!empty($redeemAfter) && !empty($grouponExpire)) {
                if (strtotime($redeemAfter) > strtotime($grouponExpire)) {
                    $response->setError(
                        true
                    )->setMessage(
                        __('Campaign Redeemable From Date must be before Redeemable Until Date')
                    );
                }
            }
        }
        return $result;
    }
}