<?php
/**
 * Author: Eric Quach
 * Date: 1/18/18
 */
namespace Magenest\Groupon\Plugin\Product\View;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ObjectManager;

class GrouponOptionLabel
{
    protected $jsonEncoder;
    protected $jsonDecoder;
    protected $productFactory;

    public function __construct(
        \Magento\Framework\Json\EncoderInterface $encoder,
        \Magento\Framework\Json\DecoderInterface $decoder,
        ProductFactory $productFactory
    )
    {
        $this->jsonEncoder = $encoder;
        $this->jsonDecoder = $decoder;
        $this->productFactory = $productFactory;
    }

    public function afterGetJsonConfig($subject, $jsonConfig)
    {
        $originalJsonConfig = $jsonConfig;
        try {
            $config = $this->jsonDecoder->decode($jsonConfig);
            if (isset($config['attributes']) && is_array($config['attributes'])) {
                foreach ($config['attributes'] as &$attribute) {
                    if (isset($attribute['code'])
                        && $attribute['code'] == 'groupon_option'
                        && isset($attribute['options'])) {
                        foreach ($attribute['options'] as &$option) {
                            if (isset($option['products'][0])) {
                                $childProduct = $this->productFactory->create()->load($option['products'][0]);
                                $option['label'] = $childProduct->getName()?:$option['label'];
                            }
                        }
                    }
                }
            }
            $jsonConfig = $this->jsonEncoder->encode($config);
        } catch (\Exception $e) {
            $jsonConfig = $originalJsonConfig;
        }
        return $jsonConfig;
    }
}