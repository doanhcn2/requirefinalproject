<?php

namespace Magenest\Groupon\Plugin\Product\Groupon;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;

class SaveAssociatedProducts
{
    /**
     * SaveAssociatedProducts constructor.
     * @param ProductFactory $productFactory
     */
    public function __construct(
        ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
    }

    /**
     * @param \Magento\ConfigurableProduct\Controller\Adminhtml\Product\Initialization\Helper\Plugin\Configurable $pluginSubject
     * @param Product $product
     * @return mixed
     */
    public function afterAfterInitialize(
        \Magento\ConfigurableProduct\Controller\Adminhtml\Product\Initialization\Helper\Plugin\Configurable $pluginSubject,
        $product
    ) {
        try {
            $configurableMatrix = $product->getData('configurable-matrix');
            if ($product->getTypeId() !== 'coupon') {
                return $product;
            }
            foreach ($configurableMatrix as $item) {
                $data = [];
                $childProduct = $this->productFactory->create()->loadByAttribute('sku', $item['sku']);
                if (isset($item[AssociatedProducts::SHORT_TITLE])) {
                    $data[AssociatedProducts::SHORT_TITLE] = $item[AssociatedProducts::SHORT_TITLE];
                }
                if (isset($item[AssociatedProducts::COMMISSION])) {
                    $data[AssociatedProducts::COMMISSION] = $item[AssociatedProducts::COMMISSION];
                }
                if (isset($item[AssociatedProducts::AFFILIATE])) {
                    $data[AssociatedProducts::AFFILIATE] = $item[AssociatedProducts::AFFILIATE];
                }
                if (isset($item[AssociatedProducts::MINIMUM_BUYERS_LIMIT])) {
                    $data[AssociatedProducts::MINIMUM_BUYERS_LIMIT] = $item[AssociatedProducts::MINIMUM_BUYERS_LIMIT];
                }
                if (isset($item[AssociatedProducts::MAXIMUM_BUYERS_LIMIT])) {
                    $data[AssociatedProducts::MAXIMUM_BUYERS_LIMIT] = $item[AssociatedProducts::MAXIMUM_BUYERS_LIMIT];
                }
                $childProduct->addData($data)->save();
            }

            return $product;
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Save Associated Products Plugin Exception: ' . $e->getMessage());
        }
        return $product;
    }
}
