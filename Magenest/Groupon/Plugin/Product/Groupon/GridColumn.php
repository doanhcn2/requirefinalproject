<?php
namespace Magenest\Groupon\Plugin\Product\Groupon;

use Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurablePanel;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form;

class GridColumn
{
    public function afterModifyMeta(
        \Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurablePanel $subject,
        $result
    ) {
        $result = array_merge_recursive(
            $result,
            $this->getConfigGridData()
        );
        return $result;
    }

    private function getConfigGridData()
    {
        $gridData = [];
        $gridData[ConfigurablePanel::GROUP_CONFIGURABLE]['children']['configurable-matrix']['children']['record']['children'] =  $this->getGrid();
        return $gridData;
    }

    private function getGrid()
    {
        return [
            AssociatedProducts::SHORT_TITLE => $this->getColumn(AssociatedProducts::SHORT_TITLE, __('Short Title')),
            AssociatedProducts::AFFILIATE => $this->getColumn(AssociatedProducts::AFFILIATE, __('Affiliate(%)')),
            AssociatedProducts::COMMISSION => $this->getColumn(AssociatedProducts::COMMISSION, __('Commission(%)')),
            AssociatedProducts::MINIMUM_BUYERS_LIMIT => $this->getColumn(AssociatedProducts::MINIMUM_BUYERS_LIMIT, __('Minimum Buyers Limit')),
            AssociatedProducts::MAXIMUM_BUYERS_LIMIT => $this->getColumn(AssociatedProducts::MAXIMUM_BUYERS_LIMIT, __('Maximum Buyers Limit')),
        ];
    }

    /**
     * Get configuration of column
     *
     * @param string $name
     * @param \Magento\Framework\Phrase $label
     * @param array $editConfig
     * @param array $textConfig
     * @return array
     */
    private function getColumn(
        $name,
        \Magento\Framework\Phrase $label,
        $editConfig = [],
        $textConfig = []
    ) {
        $fieldEdit['arguments']['data']['config'] = [
            'dataType' => Form\Element\DataType\Number::NAME,
            'formElement' => Form\Element\Input::NAME,
            'componentType' => Form\Field::NAME,
            'dataScope' => $name,
            'fit' => true,
            'visibleIfCanEdit' => true,
            'imports' => [
                'visible' => '${$.provider}:${$.parentScope}.canEdit'
            ],
        ];
        $fieldText['arguments']['data']['config'] = [
            'componentType' => Form\Field::NAME,
            'formElement' => Form\Element\Input::NAME,
            'elementTmpl' => 'Magento_ConfigurableProduct/components/cell-html',
            'dataType' => Form\Element\DataType\Text::NAME,
            'dataScope' => $name,
            'visibleIfCanEdit' => false,
            'imports' => [
                'visible' => '!${$.provider}:${$.parentScope}.canEdit'
            ],
        ];
        $fieldEdit['arguments']['data']['config'] = array_replace_recursive(
            $fieldEdit['arguments']['data']['config'],
            $editConfig
        );
        $fieldText['arguments']['data']['config'] = array_replace_recursive(
            $fieldText['arguments']['data']['config'],
            $textConfig
        );
        $container['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => $label,
            'dataScope' => '',
        ];
        $container['children'] = [
            $name . '_edit' => $fieldEdit,
            $name . '_text' => $fieldText,
        ];

        return $container;
    }
}
