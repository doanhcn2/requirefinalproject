<?php

namespace Magenest\Groupon\Plugin\Product\Groupon;

class UpdateAssociatedProducts
{
    /** @var \Magento\Catalog\Api\ProductRepositoryInterface */
    protected $productRepository;

    /** @var \Magento\Framework\App\RequestInterface */
    protected $request;

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
    
        $this->request = $request;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Magento\ConfigurableProduct\Controller\Adminhtml\Product\Initialization\Helper\Plugin\UpdateConfigurations $pluginSubject
     * @param callable $proceed
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $subject
     * @param \Magento\Catalog\Model\Product $configurableProduct
     * @return \Magento\Catalog\Model\Product
     */
    public function aroundAfterInitialize(
        \Magento\ConfigurableProduct\Controller\Adminhtml\Product\Initialization\Helper\Plugin\UpdateConfigurations $pluginSubject,
        callable $proceed,
        \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper $subject,
        \Magento\Catalog\Model\Product $configurableProduct
    ) {
        try {
            $configurations = $this->getConfigurationsFromProduct($configurableProduct);
            if (count($configurations)) {
                foreach ($configurations as $productId => $productData) {
                    /** @var \Magento\Catalog\Model\Product $product */
                    $product = $this->productRepository->getById($productId, false, $this->request->getParam('store', 0));
                    $product->addData($productData);
                    $product->save();
                }
                return $configurableProduct;
            }
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Update Associated Products Plugin Exception: ' . $e->getMessage());
        }
        $result = $proceed($subject, $configurableProduct);
        return $result;
    }

    /**
     * Get configurations from product
     *
     * @param \Magento\Catalog\Model\Product $configurableProduct
     * @return array
     */
    private function getConfigurationsFromProduct(\Magento\Catalog\Model\Product $configurableProduct)
    {
        $result = [];

        $configurableMatrix = $configurableProduct->hasData('configurable-matrix') ?
            $configurableProduct->getData('configurable-matrix') : [];
        foreach ($configurableMatrix as $item) {
            if (isset($item[AssociatedProducts::SHORT_TITLE])) {
                $result[$item['id']][AssociatedProducts::SHORT_TITLE] = $item[AssociatedProducts::SHORT_TITLE];
            }
            if (isset($item[AssociatedProducts::COMMISSION])) {
                $result[$item['id']][AssociatedProducts::COMMISSION] = $item[AssociatedProducts::COMMISSION];
            }
            if (isset($item[AssociatedProducts::AFFILIATE])) {
                $result[$item['id']][AssociatedProducts::AFFILIATE] = $item[AssociatedProducts::AFFILIATE];
            }
            if (isset($item[AssociatedProducts::MINIMUM_BUYERS_LIMIT])) {
                $result[$item['id']][AssociatedProducts::MINIMUM_BUYERS_LIMIT] = $item[AssociatedProducts::MINIMUM_BUYERS_LIMIT];
            }
            if (isset($item[AssociatedProducts::MAXIMUM_BUYERS_LIMIT])) {
                $result[$item['id']][AssociatedProducts::MAXIMUM_BUYERS_LIMIT] = $item[AssociatedProducts::MAXIMUM_BUYERS_LIMIT];
            }
        }

        return $result;
    }
}
