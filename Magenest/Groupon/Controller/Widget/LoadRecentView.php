<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/06/2018
 * Time: 09:07
 */

namespace Magenest\Groupon\Controller\Widget;

use Magenest\Groupon\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;

class LoadRecentView extends Action
{

    /**
     * @var \Magento\Reports\Model\Product\Index\ViewedFactory
     */
    protected $_reportViewFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    protected $_productListBlock;

    protected $_reportView;
    /**
     * @var $_udropshipVendor \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_udropshipVendor;
    /**
     * @var $_ratingHelper \Magenest\SellYours\Helper\RatingHelper
     */
    protected $_ratingHelper;
    /**
     * @var $_ticketHelper \Magenest\Ticket\Helper\Event
     */
    protected $_ticketHelper;
    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;
    /**
     * @var $_storeManagerInterface StoreManagerInterface
     */
    protected $_storeManagerInterface;
    private $__childProduct;
    private $__udProductFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Reports\Model\Product\Index\ViewedFactory $productViewFactory,
        \Magento\Reports\Block\Product\Viewed $reportView,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogWidget\Block\Product\ProductsList $productsListBlock,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $udProductFactory,
        StoreManagerInterface $storeManager,
        \Magenest\SellYours\Helper\RatingHelper $ratingHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Ticket\Helper\Event $eventHelper,
        \Magenest\Groupon\Model\DealFactory $dealFactory
    ) {
        $this->_dealFactory = $dealFactory;
        $this->_ticketHelper = $eventHelper;
        $this->_udropshipVendor = $vendorFactory;
        $this->_ratingHelper = $ratingHelper;
        $this->_storeManagerInterface = $storeManager;
        $this->__udProductFactory = $udProductFactory;
        $this->_productFactory = $productFactory;
        $this->_reportView = $reportView;
        $this->_reportViewFactory = $productViewFactory;
        $this->_productListBlock = $productsListBlock;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $collectionData = $this->getRecentViewProductData();
        $resultJson->setData($collectionData);

        return $resultJson;
    }

    /**
     * @return array
     */
    public function getRecentViewProductData()
    {
        $collection = $this->_reportView->getItemsCollection();
        $entityIds = $collection->getColumnValues('entity_id');
        $this->saveChildProduct($entityIds);
        if (count($entityIds) > 0) {
            $data = [];
            /** @var  $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
            $productCollection = $this->_productFactory->create()->getCollection();
            $productCollection->addAttributeToSelect(['price', 'name', 'small_image']);
            $productCollection->addFieldToFilter('entity_id', ['in' => $entityIds]);
            foreach($productCollection as $product) {
                $data[] = $this->getProductData($product);
            }
            $result = $this->prepareSortOrderData($entityIds, $data);
            return $result;
        } else {
            return [];
        }
    }

    private function prepareSortOrderData($entityIds, $data) {
        $result = [];
        foreach($entityIds as $entityId) {
            $rowData = $this->getProductDataById($entityId, $data);
            if (!empty($rowData)) {
                $result[] = $rowData;
            }
        }

        return $result;
    }

    private function getProductDataById($entityId, $data) {
        foreach($data as $item) {
            if (@$item['row_id'] === $entityId) {
                return $item;
            }
        }
        return [];
    }

    /**
     * @param $productIds
     */
    private function saveChildProduct($productIds)
    {
        if (count($this->__childProduct) == 0) {
            $result = [];
            $productCollection = $this->__udProductFactory->create()->getCollection();
            $productCollection->addFieldToSelect(['product_id', 'vendor_price', 'special_price']);;
            $productCollection->getSelect()->joinLeft(['deal_table' => $productCollection->getTable('magenest_groupon_deal')], 'main_table.product_id = deal_table.product_id', 'parent_id');
            $productCollection->addFieldToFilter('deal_table.parent_id', ['in' => $productIds]);
            $data = $productCollection->getData();
            foreach($data as $item) {
                $result[$item['parent_id']][] = $item;
            }
            $this->__childProduct = $result;
        }
    }

    /**
     * @param $product\Magento\Catalog\Model\Product
     *
     * @return mixed
     */
    protected function getProductData($product)
    {
        $block = $this->_productListBlock;
        $productData = $product->getData();
        $priceInfo = $this->getPriceInfo($product);
        $productData = array_merge($productData, $priceInfo);
        $productUrl = ObjectManager::getInstance()->create('Magenest\SellYours\Helper\PriceHelper')->getSimpleProductUrlByVendor($product->getEntityId());
        $productData['product_url'] = $block->escapeUrl($productUrl);
        $otherProductData = $this->getOtherInformation($product->getEntityId());
        $productData = array_merge($productData, $otherProductData);
        try {
            $productData['image_url'] = $block->getImage($product, 'new_products_content_widget_grid')->getImageUrl();
        } catch (\Exception $e) {
            $productData['image_url'] = $this->getDefaultPlaceholderImageUrl();
        }
        return $productData;
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @return array
     */
    protected function getPriceInfo($product)
    {
        $oriPrice = 0;
        $salePrice = PHP_INT_MAX;
        $changeFlag = false;
        if (Data::isDeal($product->getId())) {
            if (isset($this->__childProduct[$product->getId()])) {
                $childProducts = $this->__childProduct[$product->getId()];
                foreach($childProducts as $_p) {
                    if ($_p['special_price'] !== null
                        && ($_p['special_price'] < $salePrice
                            || ($_p['special_price'] == $salePrice
                                && $_p['vendor_price'] > $oriPrice))
                    ) {
                        $salePrice = $_p['special_price'];
                        $oriPrice = $_p['vendor_price'];
                        $changeFlag = true;
                    }
                }

            }
            if (!$changeFlag) {
                if ($product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())
                    $oriPrice = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue() ?: 0;
                //                                elseif (count($_ps) > 0)
                //                                    $oriPrice = $_item->getPrice() ?: 0;
                else $oriPrice = 0;

                if ($product->getMinimalPrice())
                    $salePrice = $product->getMinimalPrice();
                elseif ($product->getFinalPrice())
                    $salePrice = $product->getFinalPrice();
                else $salePrice = 0;
            }
        } elseif ($product->getTypeId() === 'ticket') {
            /** @var  $ticketPrices \Magenest\Ticket\Controller\Product\Price */
            $ticketPrices = $this->_objectManager->create('Magenest\Ticket\Controller\Product\Price');
            $ticketPrice = $ticketPrices->getTicketPrice($product->getEntityId());
            $salePrice = $ticketPrice['sale_price'];
            $oriPrice = isset($ticketPrice['original_value']) ? $ticketPrice['original_value'] : 0;
        } else if ($product->getTypeId() === 'simple' || $product->getTypeId() === 'Simple') {
            $simplePrice = $this->getSimpleProductPrice($product->getEntityId());
            if (@$simplePrice['old_price']) {
                $oriPrice = $simplePrice['old_price'];
                $salePrice = $simplePrice['current_price'];
            } else {
                $salePrice = $simplePrice['current_price'];
                $oriPrice = $simplePrice['current_price'];
            }
        } else {
            if ($product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())
                $oriPrice = $product->getPrice() ?: 0;
            else $oriPrice = 0;

            if ($product->getMinimalPrice())
                $salePrice = $product->getMinimalPrice();
            elseif ($product->getFinalPrice())
                $salePrice = $product->getFinalPrice();
            else $salePrice = 0;
        }
        $sale = $oriPrice == 0 ? 0 : ($oriPrice - $salePrice) * 100 / $oriPrice;
        $data = [
            'originPrice' => number_format($oriPrice, 2),
            'price' => number_format($salePrice, 2),
            'saleRates' => number_format($sale, 0)
        ];
        return $data;
    }

    public function getSimpleProductPrice($productId)
    {
        return $this->_objectManager->create('Magenest\SellYours\Helper\PriceHelper')->getSimpleProductPrice($productId);
    }

    protected function getOtherInformation($productId)
    {
        $result = [];
        /**
         * @var $product \Magento\Catalog\Model\Product
         */
        $product = $this->_productFactory->create()->load($productId);
        $type = $product->getTypeId();
        if ($type === 'simple') {
            $result['product_type'] = 'simple_product_type';
        } elseif ($type === 'ticket') {
            $result['product_type'] = 'ticket_product_type';
        } else {
            $result['product_type'] = 'groupon_product_type';
        }
        if ($product->getSpringsale() === "1") {
            $result['spring_sale'] = "SPRING SALE";
        } else {
            $result['spring_sale'] = "";
        }

        if (Data::isDeal($productId)) {
            $dealInformation = $this->_dealFactory->create()->load($productId, 'product_id');
            $endTime = $dealInformation->getEndTime();
            $date = date_create_from_format('Y-m-d H:i:s', $endTime);
            $endTime = $date->format('Y-m-d H:i:s');
            if ($endTime) {
                if (\Magenest\CountdownTimer\Helper\CountdownHelper::canShowCountDown($endTime, $productId)) {
                    $result['groupon_end_time'] = (strtotime($endTime) - time()) * 1000;
                    $result['countdown_time_display_field'] = "countdown-time-display-field-rv-" . $productId;
                    $result['countdown_time_id'] = "countdown-time-rv-" . $productId;
                } else {
                    $result['groupon_end_time'] = "";
                    $result['countdown_time_display_field'] = "";
                    $result['countdown_time_id'] = "";
                }
            } else {
                $result['groupon_end_time'] = "";
                $result['countdown_time_display_field'] = "";
                $result['countdown_time_id'] = "";
            }
            $locationData = [];
            if ($dealInformation->getDealId()) {
                $locationData = $dealInformation->getGrouponLocationData();
            }
            if (isset($locationData) && count($locationData) > 1) {
                $result['groupon_location'] = "Multiple locations";
                foreach($locationData as $locationDatum) {
                    $result['more_locations'][] = $locationDatum;
                }
            } elseif (isset($locationData[0])) {
                $result['groupon_location'] = $locationData[0];
                $result['more_locations'] = "";
            } else {
                $result['groupon_location'] = "";
                $result['more_locations'] = "";
            }
        } else {
            $result['more_locations'] = "";
            $result['groupon_end_time'] = "";
            $result['countdown_time_display_field'] = "";
            $result['countdown_time_id'] = "";
            $result['groupon_location'] = "";
        }

        if (Data::isDeal($productId) || $type === 'ticket') {
            if ($this->getIsNew($productId)) {
                $result['is_new'] = "NEW";
            } else {
                $result['is_new'] = "";
            }
        } else {
            $result['is_new'] = "";
        }
        if ($product->getFreeshipping() === "1" && $type === 'simple') {
            $result['free_shipping'] = "Free Shipping";
        } else {
            $result['free_shipping'] = "";
        }
        if ($product->getTypeId() === 'simple') {
            $rateCollection = $this->_ratingHelper->getProductReviewCollection($productId);
        } else {
            $rateCollection = $this->_ratingHelper->getReviewCollection($productId);
        }
        $averageRate = $this->_ratingHelper->getAvgRate($rateCollection->getColumnValues('review_id'));
        if ($averageRate) {
            $result['rate_average'] = number_format(((float)$averageRate / 5) * 100, 2) . "%";
            $result['rate_average_percent'] = "width: " . $result['rate_average'];
            $result['rate_count'] = "(" . $rateCollection->getSize() . ")";
        } else {
            $result['rate_average'] = "";
            $result['rate_average_percent'] = "";
            $result['rate_count'] = "";
        }
        $udProduct = $this->__udProductFactory->create()->load($productId, 'product_id');
        $result['vendor_name'] = $this->_udropshipVendor->create()->load($udProduct->getVendorId())->getVendorName();
        if ($type === 'ticket') {
            if ($this->_ticketHelper->isFreeTicketType($productId)) {
                $result['is_free_ticket_type'] = "free_ticket_type";
            } else {
                $result['is_free_ticket_type'] = "";
            }
            $result['ticket_location'] = $this->_ticketHelper->getTicketLocation($productId);
            $eventInfo = \Magenest\Ticket\Helper\Event::getTicketInfo($productId);
            $result['ticket_time_week_day'] = $eventInfo['week_day'];
            if (isset($eventInfo['day']) && isset($eventInfo['month'])) {
                $result['ticket_time_day_month'] = $eventInfo['month'] . ' ' . $eventInfo['day'];
            } else {
                $result['ticket_time_day_month'] = "";
            }
            foreach($eventInfo['more_sessions'] as $session) {
                $result['more_sessions'][] = ['weekday' => date("D",$session), 'month' => date("M j",$session)];
            }
            if (count($eventInfo['more_sessions']) === 0) {
                $result['more_sessions'] = "";
            }
            if (isset($eventInfo['number_session']) && intval($eventInfo['number_session']) > 0) {
                $result['ticket_time_view_more'] = "+ " . $eventInfo['number_session'] . " more";
            } else {
                $result['ticket_time_view_more'] = "";
            }
        } else {
            $result['is_free_ticket_type'] = "";
            $result['ticket_time_week_day'] = "";
            $result['ticket_time_day_month'] = "";
            $result['ticket_time_view_more'] = "";
            $result['ticket_location'] = "";
            $result['more_sessions'] = "";
        }

        return $result;
    }

    private function getIsNew($data)
    {
        $udProduct = $this->__udProductFactory->create()->load($data, 'product_id');
        if ($udProduct->getState() === 'new') {
            return true;
        }

        return false;
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManagerInterface->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManagerInterface->getStore()->getConfig('catalog/placeholder/small_image_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }
}
