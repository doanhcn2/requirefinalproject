<?php
/**
 * Created by PhpStorm.
 */
namespace Magenest\Groupon\Controller\Vendor;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;

/**
 * Class Upload
 * @package Magenest\Groupon\Controller\Vendor
 */
class Upload extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Registry $registry
     * @param Filesystem $filesystem
     * @param UploaderFactory $fileUploaderFactory
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Registry $registry,
        Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory,
        LoggerInterface $loggerInterface
    ) {
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        $this->logger = $loggerInterface;
        $this->_coreRegistry = $registry;
        $this->_resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        if (isset($_FILES["file"]["type"])) {
            $validate = array("jpeg", "jpg", "png");
            $temporary = explode(".", $_FILES["file"]["name"]);
            $file_extension = end($temporary);
            if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
                ) && ($_FILES["file"]["size"] < 5000000)
                && in_array($file_extension, $validate)) {
                if ($_FILES["file"]["error"] > 0) {
                    $data = "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
                } else {
                    if (file_exists("upload/" . $_FILES["file"]["name"])) {
                        $data = $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
                    } else {
                        $path = $this->_filesystem->getDirectoryRead(
                            DirectoryList::MEDIA
                        )->getAbsolutePath(
                            'groupon/template/'
                        );

                        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'file']);
                        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                        $uploader->setAllowRenameFiles(false);
                        $result = $uploader->save($path);
                        $resultE[0] =  $result;
                        $name = serialize($resultE);
                        $html = "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
                        $html .= "<input data-form-part='customer_form' id='image_upload' name='groupon[ticket][background]' value='$name' type='text' hidden><br/>";
                        $data = $html;
                    }
                }
            } else {
                $data = "<span id='invalid'>***Invalid file Size or Type***<span>";
            }
            return $resultPage->setData($data);
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
