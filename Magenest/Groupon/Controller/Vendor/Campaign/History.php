<?php

namespace Magenest\Groupon\Controller\Vendor\Campaign;

class History extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Ticket\Block\Vendor\Campaign\ChangeHistoryPage $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('campaign-history-page');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}