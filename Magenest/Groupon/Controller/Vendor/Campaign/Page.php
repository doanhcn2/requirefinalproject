<?php

namespace Magenest\Groupon\Controller\Vendor\Campaign;

class Page extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Payments\Block\Vendor\Payout\Info $infoBlock */
        /** @var  $infoBlock \Magenest\Groupon\Block\Vendor\Campaigns */
//        $infoBlock = $this->_objectManager->create('\Magenest\Groupon\Block\Vendor\Campaigns');
//        $infoBlock->setTemplate('Magenest_Groupon::vendor/campaign/page.phtml');
        $infoBlock = $view->getLayout()->getBlock('campaign-page');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}