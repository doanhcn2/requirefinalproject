<?php

namespace Magenest\Groupon\Controller\Vendor\Campaign;

class Purchased extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{
    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        $dealId = $this->getRequest()->getParam('deal_product_id');
        $productId = $this->getRequest()->getParam('product_id');
        $this->_registry->register('deal_product_id', $dealId);
        $this->_registry->register('product_id', $productId);
        /** @var \Magenest\Groupon\Block\Vendor\Campaign\Purchased $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('campaign-purchased-coupon-page');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}