<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:10
 */

namespace Magenest\Groupon\Controller\Vendor\Campaign;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;
use Magenest\Groupon\Helper\Data as DataHelper;

class Detail extends AbstractVendor
{
    protected $productFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        $this->productFactory = $productFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        $productId = $this->getRequest()->getParam('id');
        if (empty($productId) || !DataHelper::isDeal($productId))
            return $this->_redirect('groupon/vendor/campaign', ['filter' => 'active']);

        $productModel = $this->productFactory->create()->load($productId);
        if (empty($productModel->getId()))
            return $this->_redirect('groupon/vendor/campaign', ['filter' => 'active']);
        $this->_registry->register('deal_product', $productModel);

        $optionId = $this->getRequest()->getParam('option_id');
        if (empty($optionId)) {
            $this->_registry->register('current_product', $productModel);
            $this->_registry->register('deal_product_id', $productId);
            $this->_registry->register('product_id', $productId);
        } else {
            $productModel = $this->productFactory->create()->load($optionId);
            if (empty($productModel->getId()))
                return $this->_redirect('groupon/vendor/campaign', ['filter' => 'active']);
            $this->_registry->register('current_product', $productModel);
            $this->_registry->register('deal_product_id', $productId);
            $this->_registry->register('product_id', $optionId);
        }

        $collection = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magenest\Groupon\Model\ResourceModel\Deal\Collection::class);
        $model = $collection->addFieldToFilter('product_id', $productId)->getFirstItem();
        if (empty($model->getProductId()))
            return $this->_redirect('groupon/vendor/campaign', ['filter' => 'active']);

        $this->_registry->register('current_deal', $model);
        $this->_renderPage(null, 'magenest_campaign');
    }
}