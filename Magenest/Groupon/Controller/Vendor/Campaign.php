<?php

namespace Magenest\Groupon\Controller\Vendor;

use Magento\Framework\App\ObjectManager;
use Unirgy\DropshipVendorProduct\Controller\Vendor\AbstractVendor;

class Campaign extends AbstractVendor
{
    public function execute()
    {
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $session->setUdprodLastGridUrl($this->_url->getUrl('*/*/*', ['__vp'=>true,'_current'=>true]));
        $this->_renderPage(null, 'magenest_campaign');
    }
}
