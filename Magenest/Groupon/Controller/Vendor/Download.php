<?php
namespace Magenest\Groupon\Controller\Vendor;

use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\LocationFactory;
use Magenest\Groupon\Model\DealFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;

class Download extends Action
{
    /**
     * @var CollectionFactory $_grouponCollection
     */
    protected $_grouponCollection;

    /**
     * @var $_dealFactory DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_locationFactory LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_campaignStatus CampaignStatus
     */
    protected $_campaignStatus;

    /**
     * @var WriteInterface
     */
    protected $directory;

    protected $fileFactory;

    /**
     * Download constructor.
     * @param CollectionFactory $collectionFactory
     * @param CampaignStatus $campaignStatus
     * @param DealFactory $dealFactory
     * @param LocationFactory $locationFactory
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        CampaignStatus $campaignStatus,
        DealFactory $dealFactory,
        LocationFactory $locationFactory,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Context $context
    ) {
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_dealFactory = $dealFactory;
        $this->_locationFactory = $locationFactory;
        $this->_campaignStatus = $campaignStatus;
        $this->_grouponCollection = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $parentProductId = $this->getRequest()->getParam('product_id');
            if (empty($parentProductId)) {
                $couponId = $this->getRequest()->getParam('coupon_id');
                if (empty($parentProductId) && empty($couponId)) {
                    $couponId = $this->getRequest()->getParam('child_product_id');
                    $couponIds = explode(',', $couponId);
                    $collection = $this->_grouponCollection->create()->addFieldToFilter('product_id', ['in' => $couponIds]);
                } else {
                    $couponIds = explode(',', $couponId);
                    $collection = $this->_grouponCollection->create()->addFieldToFilter('groupon_id', ['in' => $couponIds]);
                }
            } else {
                $parentProductId = explode(',', $parentProductId);
                $childLists = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', ['in' => $parentProductId]);
                $listParams = $childLists->getColumnValues('product_id');
                $collection = $this->_grouponCollection->create()->addFieldToFilter('vendor_id', $this->getVendorId())->addFieldToFilter('product_id', ['in' => $listParams]);
            }
            $heading = [
                ('groupon_id'),
                ('product_name'),
                ('groupon_code'),
                ('order_id'),
                ('status'),
                ('recipient'),
                ('ordered_at'),
                ('price'),
                ('qty'),
                ('customer_name'),
                ('redeemed_at'),
                ('redemption_location')
            ];

            $items = [];
            foreach($collection as $log) {
                $redeemLocation = null;
                if (@$log->getData('redemption_location_id')) {
                    $redeem = $this->_locationFactory->create()->load($log->getData('redemption_location_id'));
                    $redeemLocation = "";
                    $redeemLocation .= @$redeem->getLocationName() ? $redeem->getLocationName() . " - " : "";
                    $redeemLocation .= @$redeem->getStreet() . " ";
                    $redeemLocation .= @$redeem->getStreetTwo() ? ", " . $redeem->getStreetTwo() : ", ";
                    $redeemLocation .= @$redeem->getCity();
                    $redeemLocation .= ", " . @$redeem->getCountry();
                } else {
                    $redeemLocation = 'Any';
                }
                $row = [
                    $log->getData('groupon_id'),
                    $log->getData('product_name'),
                    $log->getData('groupon_code'),
                    $log->getData('order_id'),
                    Groupon::getStatusLabelByCode($log->getData('status')),
                    $log->getData('recipient'),
                    $log->getData('created_at'),
                    $log->getData('price'),
                    $log->getData('qty'),
                    $log->getData('customer_name'),
                    $log->getData('redeemed_at'),
                    $redeemLocation
                ];
                array_push($items, $row);
            }
            $file = 'var/tmp/' . "Campaign_Log_" . date('Ymd_His') . ".csv";
            $this->directory->create('export');
            $stream = $this->directory->openFile($file, 'w+');
            $stream->lock();
            $stream->writeCsv($heading);
            foreach($items as $item) {
                $stream->writeCsv($item);

            }

            return $this->fileFactory->create("Ticket_Log_" . date('Ymd_His') . ".csv", [
                'type' => 'filename',
                'value' => $file,
                'rm' => true  // can delete file after use
            ], 'var');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }

        return $resultRedirect->setPath('*/*/*');
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
