<?php
namespace Magenest\Groupon\Controller\Test;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/***
 * Class Test
 * @package Magenest\Groupon\Controller\Test
 */
class Test extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
//        $model = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Groupon\Model\Order');
//        $parent = $model->getCollection()->addFieldToFilter('order_id','22')->addFieldToFilter('product_id','50')->getData();
//        var_dump($parent) ;

        $model = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Catalog\Model\Product');
        $product = $model->load(13);
        $attributeId = $product->getGrouponOption();
        $attributeValue = $product->getCustomAttribute('groupon_option');

    }

}
