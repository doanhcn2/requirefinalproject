<?php

namespace Magenest\Groupon\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\Session;
use Magenest\Groupon\Model\GrouponFactory;
use Magento\Framework\Controller\ResultFactory;

class SendMail extends Action
{
    protected $_resultJsonFactory;
    protected $_session;
    protected $_groupon;
    protected $resultRedirect;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Session $session,
        GrouponFactory $grouponFactory,
        ResultFactory $result
    )
    {
        $this->_resultJsonFactory = $jsonFactory;
        $this->_session = $session;
        $this->_groupon = $grouponFactory;
        $this->resultRedirect = $result;
        parent::__construct($context);
    }


    public function execute()
    {
        $grouponId = (int)$this->getRequest()->getParam('id');
        if ($grouponId) {
            try {
                $this->_groupon->create()->sendMail($grouponId);
            } catch (\Exception $e) {
            }
        }
        $resultPage = $this->resultRedirectFactory->create();
        $this->messageManager->addSuccessMessage('Your Email has been sent');
        return $resultPage->setPath('groupon/order/groupon');


    }
}