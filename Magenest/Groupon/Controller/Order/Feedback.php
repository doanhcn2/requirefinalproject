<?php

namespace Magenest\Groupon\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\Session;
use Magenest\Groupon\Model\FeedbackFactory;
use Magenest\Groupon\Model\GrouponFactory;

class Feedback extends Action
{
    protected $_resultJsonFactory;
    protected $_session;
    protected $_feedback;
    protected $_groupon;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Session $session,
        FeedbackFactory $feedbackFactory,
        GrouponFactory $grouponFactory
    ) {
        $this->_resultJsonFactory = $jsonFactory;
        $this->_session = $session;
        $this->_feedback = $feedbackFactory;
        $this->_groupon = $grouponFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $requestParams = $this->getRequest()->getParams();
        $result = $this->_resultJsonFactory->create();

        if (isset($requestParams['feedback_now'])) {
            $groupon = $this->_groupon->create()->load($requestParams['coupon-ticket-id']);
            if ($this->checkPermission($groupon->getCustomerId()))
            {
                try {
                    $groupon->redeemCoupon();
                    $this->messageManager->addSuccessMessage('Your coupon has been redeemed.');
                    return $result->setData(['success' => true]);
                } catch (\Exception $e) {
                    $this->messageManager->addSuccessMessage('Something went wrong, please try again later.');
                    return $result->setData(['success' => false]);
                }
            } else {
                $this->messageManager->addSuccessMessage('You have not permission to do that.');
                return $result->setData(['success' => false]);
            }
        }
        $redeemed = false;
        $groupon = $this->_groupon->create()->load($requestParams['coupon-ticket-id']);
        if ($this->checkPermission($groupon->getCustomerId()))
        {
            try {
                $groupon->redeemCoupon();
                $redeemed = true;
            } catch (\Exception $e) {}
        }


    }

    private function checkPermission($couponCustomerId)
    {
        $currentCustomerId = $this->_session->getCustomerId();
        if ((int)$couponCustomerId === $currentCustomerId) {
            return true;
        } else {
            return false;
        }
    }
}
