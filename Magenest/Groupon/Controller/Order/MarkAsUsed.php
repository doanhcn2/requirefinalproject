<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Order;

use Magenest\Groupon\Model\Groupon;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class MarkAsUsed extends Action
{
    /**
     * @var $_grouponFactory \Magenest\Groupon\Model\GrouponFactory
     */
    protected $_grouponFactory;

    /**
     * @var $_customerSession \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var $_jsonFactory JsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var $_unirgyRatingHelper \Unirgy\DropshipVendorRatings\Helper\Data
     */
    private $_unirgyRatingHelper;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $session,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $ratingHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        JsonFactory $jsonFactory
    ) {
        $this->_vendorFactory = $vendorFactory;
        $this->_unirgyRatingHelper = $ratingHelper;
        $this->_jsonFactory = $jsonFactory;
        $this->_customerSession = $session;
        $this->_grouponFactory = $grouponFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();
        if ($data = $this->getRequest()->getParams()) {
            $groupon = $this->_grouponFactory->create()->load($data['groupon_id']);
            if ((int)$groupon->getStatus() === Groupon::STATUS_AVAILABLE && $this->isCustomerPermitted($groupon->getCustomerId())) {
                try {
                    $redeem = $groupon->markAsUsed();
                    if ($redeem) {
                        $this->messageManager->addSuccessMessage('Your coupon has been marked as Used.');
                    } else {
                        $this->messageManager->addSuccessMessage('Something went wrong, please try again later.');
                    }
                    if ($redeem && $this->vendorAllowRating($groupon->getVendorId()) && $this->couponHasReview($this->getCouponProductIds($data['parent_id'], json_decode($data['groupon_list'], true)), $groupon->getCustomerId())) {
                        $modalHtml = $this->getReviewModalBlock(json_decode($data['groupon_list'], true), $data['parent_id']);
                        return $result->setData(['success' => true, 'review_modal' => $modalHtml, 'has_review' => true]);
                    } else {
                        return $result->setData(['success' => true, 'has_review' => false]);
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage('Something went wrong, please try again later.');
                    return $result->setData(['success' => false]);
                }
            }
        }
        $this->messageManager->addErrorMessage('You has no permission to do this.');
        return $result->setData(['success' => false]);
    }

    private function isCustomerPermitted($customerId)
    {
        $customerSessionId = $this->_customerSession->getCustomerId();

        return (int)$customerSessionId === (int)$customerId;
    }

    public function couponHasReview($couponIds, $customerId)
    {
        $items = ObjectManager::getInstance()->create('Magenest\VendorApi\Helper\OrderHelper')->getRatingItemsByProductId($couponIds, $customerId);
        if (count($items->getData()) !== 0) {
            return true;
        } else {
            return false;
        }
    }

    private function getCouponProductIds($parentId, $listIds)
    {
        $result = [];
        foreach($listIds as $key => $listId) {
            if ($key !== (int)$parentId) {
                continue;
            }
            array_push($result, $key);
            foreach($listId as $item) {
                array_push($result, $item);
            }
        }
        if (!$this->isProductHasReviewYet($result))
        {
            return $result;
        }
        return [];
    }

    protected function getReviewModalBlock($grouponByProduct, $parentId)
    {
        $view = ObjectManager::getInstance()->create('\Magento\Framework\App\ViewInterface');
        $view->addActionLayoutHandles();
        $reviewBlock = $view->getLayout()->createBlock('Magenest\Groupon\Block\Customer\VirtualReview')
            ->setGrouponByProduct($grouponByProduct)
            ->setParentId($parentId);
        $view->getLayout()->initMessages();
        return $reviewBlock->toHtml();
    }

    protected function isProductHasReviewYet($productIds)
    {
        $result = [];
        $reviews = $this->_unirgyRatingHelper->getCustomerReviewsCollection();

        foreach($reviews->getItems() as $review) {
            if ($review->getProductId()) {
                array_push($result, $review->getProductId());
            }
        }
        foreach($productIds as $item) {
            if (in_array($item, $result)) {
                return true;
            }
        }
        return false;
    }

    protected function vendorAllowRating($vendorId)
    {
        $isAllowed = $this->_vendorFactory->create()->load($vendorId)->getAllowUdratings();

        return $isAllowed === 1 || $isAllowed === '1';
    }
}
