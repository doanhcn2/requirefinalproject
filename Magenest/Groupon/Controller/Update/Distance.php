<?php

namespace Magenest\Groupon\Controller\Update;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Controller\ResultFactory;

class Distance extends Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var $_geoHelper \Magenest\Groupon\Helper\GeoLocation
     */
    protected $_geoHelper;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * Option constructor.
     * @param Context $context
     * @param \Magenest\Groupon\Helper\GeoLocation $geoLocation
     * @param ResultJsonFactory $resultJsonFactory
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        \Magenest\Groupon\Helper\GeoLocation $geoLocation,
        ResultJsonFactory $resultJsonFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_vendorFactory    = $vendorFactory;
        $this->_geoHelper        = $geoLocation;
        $this->logger            = $logger;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $data       = $this->_getDataJson();
        $resultPage->setData($data);
        return $resultPage;

    }

    /**
     * @return array
     */
    protected function _getDataJson()
    {
        $data = $this->getRequest()->getParams();
        if (!(isset($data['startLat']) && isset($data['startLng']) && isset($data['type']) && isset($data['id']))) {
            return ['success' => false];
        }
        if ($data['type'] === 'groupon') {
            $distance = $this->_geoHelper->getGrouponProductDistance($data['startLat'], $data['startLng'], $data['id']);
        } elseif ($data['type'] === 'coupon') {
            $distance = $this->_geoHelper->getCouponLocationDistance($data['startLat'], $data['startLng'], $data['id']);
        } elseif ($data['type'] === 'ticket') {
            $distance = $this->_geoHelper->getTicketProductDistance($data['startLat'], $data['startLng'], $data['id']);
        } else {
            return ['success' => false];
        }
        if ($data['type'] === 'groupon') {
            /**
             * @var $location \Magenest\Groupon\Model\Location | bool
             */
            $location = isset($distance['location']) ? $distance['location'] : false;
            if ($location) {
                $locationField = $this->getLocationDisplayField($location);
                $data          = [
                    'success'  => true,
                    'distance' => $distance['distance'],
                    'location' => $locationField
                ];
            } else {
                $data = [
                    'success'  => true,
                    'distance' => $distance['distance']
                ];
            }
        } else {
            $data = [
                'success'  => true,
                'distance' => $distance
            ];
        }

        return $data;
    }

    /**
     * @param $location \Magenest\Groupon\Model\Location
     * @return bool|string
     */
    private function getLocationDisplayField($location)
    {
        $result        = "";
        $objectManager = ObjectManager::getInstance();
        if (!$location->getId()) {
            return false;
        }
        $result .= $this->_vendorFactory->create()->load($location->getVendorId())->getVendorName();
        $result .= " - ";
        $locations = [];
        if (@$location['city']) {
            array_push($locations, @$location['city']);
        }
        if (@$location['region']) {
            array_push($locations, @$location['region']);
        }
        if (@$location['country']) {
            $countryName = $objectManager->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($location['country']);
            array_push($locations, $countryName);
        }

        return $result . implode(', ', $locations);
    }
}
