<?php
namespace Magenest\Groupon\Controller\Update;

use FontLib\Table\Type\loca;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Controller\ResultFactory;

class Cookie extends Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    protected $logger;

    /**
     * @var $_geoHelper \Magenest\Groupon\Helper\GeoLocation
     */
    protected $_geoHelper;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * Option constructor.
     * @param Context $context
     * @param \Magenest\Groupon\Helper\GeoLocation $geoLocation
     * @param ResultJsonFactory $resultJsonFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        \Magenest\Groupon\Helper\GeoLocation $geoLocation,
        ResultJsonFactory $resultJsonFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_vendorFactory = $vendorFactory;
        $this->_locationFactory = $locationFactory;
        $this->_geoHelper = $geoLocation;
        $this->logger = $logger;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $allowAccess = $this->getRequest()->getParam('ask_location');
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($allowAccess) {
            $customerCookie = $this->getCustomerCookie()->get();
            if ($customerCookie === null) {
                $customerCookie = 'no_location';
            }
            $resultPage->setData(['success' => true, 'customer_access' => $customerCookie]);
            return $resultPage;
        } else {
            $resultPage->setData(['success' => false, 'customer_access' => 'no_location']);
            return $resultPage;
        }
    }

    protected function getCustomerCookie()
    {
        $objectManager = ObjectManager::getInstance();
        return $objectManager->create('Magenest\Groupon\Cookie\AllowAccessLocationCookie');
    }
}
