<?php

namespace Magenest\Groupon\Controller\Update;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Sales\Model\Order\ItemFactory;

/**
 * Class Bought
 * @package Magenest\Groupon\Controller\Update
 */
class Bought extends \Magento\Framework\App\Action\Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    private $_itemFactory;

    private $_productFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        ItemFactory $itemFactory
    ) {
        $this->_itemFactory = $itemFactory;
        $this->_productFactory = $productFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $productIds = $this->getRequest()->getParam('product_ids');
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $result = [];
        foreach($productIds as $productId) {
            $product = $this->_productFactory->create()->load($productId);
            if (empty($product->getId())) {
                $result[] = ['product_id' => $productId, 'product_bought' => 0];
            } else {
                $itemCollection = $this->_itemFactory->create()->getCollection()
                    ->addFieldToFilter('product_id', $product->getId());
                $itemCollection->getSelect()
                    ->columns('SUM(qty_ordered) as total_qty')
                    ->group('product_id');
                $total = $itemCollection->getFirstItem()->getTotalQty();
                $result[] = ['product_id' => $productId, 'product_bought' => (int)$total];
            }
        }

        return $resultPage->setData($result);
    }
}
