<?php

namespace Magenest\Groupon\Controller\Recipient;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\Session;
use Magenest\Groupon\Model\GrouponFactory;

class Get extends Action
{
    protected $_resultJsonFactory;
    protected $_session;
    protected $_groupon;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Session $session,
        GrouponFactory $grouponFactory
    )
    {
        $this->_resultJsonFactory = $jsonFactory;
        $this->_session = $session;
        $this->_groupon = $grouponFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $is_sent = false;
        $id = $this->getRequest()->getParam('id');
        $return = $this->_resultJsonFactory->create()->setData(['success' => false]);
        $grouponModel = $this->_groupon->create()->load($id);
        $recipient = json_decode($grouponModel->getData('recipient'),true);
        $is_sent = $grouponModel->getData('is_sent');
        $name = $recipient['name'];
        $mail = $recipient['mail'];
        $msg = $recipient['message'];

        $data = [
            'success'  => true,
            'sent' => $is_sent,
            'name' => $name,
            'mail' => $mail,
            'msg' => $msg,
            'from' => $grouponModel->getData('customer_name')
        ];

        $return->setData(json_encode($data));
        return $return;



    }
}