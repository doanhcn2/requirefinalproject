<?php

namespace Magenest\Groupon\Controller\Recipient;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\Session;
use Magenest\Groupon\Model\GrouponFactory;

class Update extends Action
{
    protected $_resultJsonFactory;
    protected $_session;
    protected $_groupon;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Session $session,
        GrouponFactory $grouponFactory
    )
    {
        $this->_resultJsonFactory = $jsonFactory;
        $this->_session = $session;
        $this->_groupon = $grouponFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $requestParams = $this->getRequest()->getParams();

        $return = $this->_resultJsonFactory->create()->setData(['success' => false]);
        $grouponModel = $this->_groupon->create()->load($requestParams['coupon-gift-id']);
        $recipient = json_decode($grouponModel->getData('recipient'),true);
        $recipient['name'] = $requestParams['recipient_name'];
        $recipient['mail'] = $requestParams['recipient_mail'];
        $recipient['message'] = $requestParams['recipient_msg'];
        $grouponModel->setData('recipient', json_encode($recipient));
        try {
            $grouponModel->save();
            $return->setData(['success' => true]);
            $this->messageManager->addSuccessMessage('Recipient Detail has been updated');
            return $return;
        } catch (\Exception $e) {
            $return->setData(['success' => false]);
            return $return;
        }
    }
}