<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Deal;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magenest\Groupon\Helper\Pdf as PdfHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magenest\Groupon\Model\DealFactory;

/**
 * Class PrintTicket
 * @package Magenest\Groupon\Controller\Adminhtml\Deal
 */
class PrintTicket extends Action
{
    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $_directory;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DealFactory
     */
    protected $deal;


    /**
     * PrintTicket constructor.
     * @param Context $context
     * @param PdfHelper $pdfHelper
     * @param Filesystem $filesystem
     * @param LoggerInterface $loggerInterface
     * @param StoreManagerInterface $storeManager
     * @param Store $store
     * @param PageFactory $resultPageFactory
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @param DealFactory $dealFactory
     */
    public function __construct(
        Context $context,
        PdfHelper $pdfHelper,
        Filesystem $filesystem,
        LoggerInterface $loggerInterface,
        StoreManagerInterface $storeManager,
        Store $store,
        PageFactory $resultPageFactory,
        DateTime $dateTime,
        FileFactory $fileFactory,
        DealFactory $dealFactory
    ) {
        parent::__construct($context);
        $this->_pdfHelper = $pdfHelper;
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
        $this->logger = $loggerInterface;
        $this->store = $store;
        $this->resultPageFactory = $resultPageFactory;
        $this->dateTime = $dateTime;
        $this->fileFactory = $fileFactory;
        $this->deal = $dealFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $id = $this->getRequest()->getParam('id');
        $model = $this->deal->create()->load($id, 'product_id');
        if (!empty($model->getData())) {
            return $this->fileFactory->create(
                sprintf('ticket%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
                $this->_pdfHelper->getPrintPdfPreview($model->getData())->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }

        return $resultPage;
    }

    /**
     * @return string
     */
    public function getUrlFile()
    {
        $url = $this->store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $url;
    }
    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::deal');
    }
}
