<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Deal;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Option
 *
 * @package Magenest\Groupon\Controller\Adminhtml\Deal
 */
class Option extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor
     *
     *  @param Context $context
     *  @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Groupon::deal_option');
        $resultPage->addBreadcrumb(__('Campaign Option'), __('Campaign Option'));
        $resultPage->addBreadcrumb(__('Manage Campaign Option'), __('Manage Campaign Option'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Campaign Option'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::deal_option');
    } //end
}
