<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 12/05/2018
 * Time: 13:24
 */
namespace Magenest\Groupon\Controller\Adminhtml\Location;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
class InlineEdit extends Action{

    protected $_locationFactory;

    protected $_dealFactory;
    public function __construct(
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->_dealFactory =$dealFactory;
        $this->_locationFactory = $locationFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        try {
            if ($this->getRequest()->getParam('isAjax') == true && count($this->getRequest()->getParam('items')) > 0) {
                foreach ($this->getRequest()->getParam('items') as $locationId => $itemData) {
                    $this->_dealFactory->create();
                    $newData = array_filter($itemData, function ($value) {
                        return $value !== '';
                    });
                    $locationModel = $this->_locationFactory->create()->load($locationId);
                    $dealId = $itemData['current_product_id'];
                    unset($newData['current_product_id']);
                    $dealModel = $this->_dealFactory->create()->load($dealId, 'product_id');
                    $locationIds = $dealModel->getLocationIds();
                    if ($locationIds === "") {
                        $locationArray = [];
                    } else {
                        $locationArray = explode(',', $locationIds);
                    }
                    if ($itemData['used'] === "1") {
                        if (in_array($locationId, $locationArray) === false) {
                            $locationArray[] = $locationId;
                        }
                    } elseif ($itemData['used'] === "0") {
                        if (($key = array_search($locationId, $locationArray)) !== false) {
                            unset($locationArray[$key]);
                        }
                    }
                    $saveLocationIds = implode(',', $locationArray);
                    $dealModel->setLocationIds($saveLocationIds);
                    $dealModel->save();
                    if ($itemData)
                        $locationModel->addData($newData);
                    $locationModel->save();

                }
                $result->setData(
                    [
                        'error' => false,
                        'message' => "Save Location Success"
                    ]
                );
                return $result;
            } else {
                $result->setData(
                    [
                        'error' => true,
                        'message' => "Invalid Data"
                    ]
                );
                return $result;
            }
        } catch (\Exception $e) {
            $result->setData(
                [
                    'error' => true,
                    'message' => "Can't saved this Location"
                ]
            );
            return $result;
        }
    }
}