<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Term;

use Magento\Backend\App\Action;
use Magenest\Groupon\Controller\Adminhtml\Term;

/**
 * Class NewAction
 * @package Magenest\Groupon\Controller\Adminhtml\Term
 */
class NewAction extends Term
{
    /**
     * forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
