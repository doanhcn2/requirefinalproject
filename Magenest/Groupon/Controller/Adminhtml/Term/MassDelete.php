<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Term;

use Magenest\Groupon\Controller\Adminhtml\Term;
use Magenest\Groupon\Model\TermFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magenest\VendorTicket\Model\ResourceModel\VenueMap\CollectionFactory;

/**
 * Class MassDelete
 */
class MassDelete extends Term
{
    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param TermFactory $attrRuleFactory
     * @param LayoutFactory $layoutFactory
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Filter $filter
     * @param \Magenest\Groupon\Model\ResourceModel\Term\CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Registry $coreRegistry, TermFactory $attrRuleFactory, LayoutFactory $layoutFactory, PageFactory $resultPageFactory, ForwardFactory $resultForwardFactory, Filter $filter, \Magenest\Groupon\Model\ResourceModel\Term\CollectionFactory $collectionFactory)
    {
        parent::__construct($context, $coreRegistry, $attrRuleFactory, $layoutFactory, $resultPageFactory, $resultForwardFactory, $filter, $collectionFactory);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $collectionSize = 0;
        $termCollections = $this->_attrRuleFactory->create();
        if (array_key_exists('selected', $params) && isset($params['selected'])) {
            foreach ($params['selected'] as $recordId) {
                $termCollections->load($recordId)->delete();
                $collectionSize++;
            }
        } elseif (array_key_exists('excluded', $params) && isset($params['excluded'])) {
            $termCollections->getCollection();
            if ($params['excluded'] == false) {
                foreach ($termCollections->getItems() as $item) {
                    $item->delete();
                    $collectionSize++;
                }
            } else {
                foreach ($termCollections->getItems() as $item) {
                    if (!in_array($item->getDataByKey('term_id'), $params['excluded'])) {
                        $item->delete();
                        $collectionSize++;
                    }
                }
            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
