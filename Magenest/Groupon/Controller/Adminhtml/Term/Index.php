<?php

namespace Magenest\Groupon\Controller\Adminhtml\Term;

use Magenest\Groupon\Controller\Adminhtml\Term;
/**
 * Class Index
 * @package Magenest\Odoo\Controller\Adminhtml\Attribute
 */
class Index extends Term
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }
}
