<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Term;

use Magenest\Groupon\Model\ResourceModel\Term\CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magenest\Groupon\Model\TermFactory;
use Magenest\Groupon\Controller\Adminhtml\Term as AbstractTerm;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

class Delete extends AbstractTerm
{
    /**
     * Delete constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param TermFactory $attrRuleFactory
     * @param LayoutFactory $layoutFactory
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Registry $coreRegistry, TermFactory $attrRuleFactory, LayoutFactory $layoutFactory, PageFactory $resultPageFactory, ForwardFactory $resultForwardFactory, Filter $filter, CollectionFactory $collectionFactory)
    {
        parent::__construct($context, $coreRegistry, $attrRuleFactory, $layoutFactory, $resultPageFactory, $resultForwardFactory, $filter, $collectionFactory);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id', null);

        try {
            $objectInstance = $this->_attrRuleFactory->create()->load($id);
            if ($objectInstance->getId()) {
                $objectInstance->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        
        return $resultRedirect->setPath('*/*');
    }
}
