<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Groupon\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory as GrouponCollectionFactory;

/**
 * Class Groupon
 * @package Magenest\Groupon\Controller\Adminhtml
 */
abstract class Groupon extends Action
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Page result factory
     *
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     *
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     *
     * @var Filter
     */
    protected $_filter;

    /**
     * @var GrouponFactory
     */
    protected $groupon;

    /**
     * @var GrouponCollectionFactory
     */
    protected $grouponCollection;

    /**
     * Groupon constructor.
     * @param Registry $coreRegistry
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Filter $filter
     * @param GrouponFactory $grouponFactory
     * @param GrouponCollectionFactory $collectionFactory
     */
    public function __construct(
        Registry $coreRegistry,
        Context $context,
        PageFactory $resultPageFactory,
        Filter $filter,
        GrouponFactory $grouponFactory,
        GrouponCollectionFactory $collectionFactory
    ) {
        $this->grouponCollection = $collectionFactory;
        $this->groupon = $grouponFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }
    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Magenest_Groupon::groupon');
        $resultPage->getConfig()->getTitle()->prepend((__('Groupon')));
        return $this;
    }

    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::groupon');
    }
}
