<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Template;

use Magenest\Groupon\Controller\Adminhtml\Template as TemplateController;

/**
 * Class Index
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class Index extends TemplateController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }
}
