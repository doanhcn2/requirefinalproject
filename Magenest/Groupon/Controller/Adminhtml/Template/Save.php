<?php
/**
 * Created by PhpStorm.
 */

namespace Magenest\Groupon\Controller\Adminhtml\Template;

use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magenest\Groupon\Helper\Pdf as PdfHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Save
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * Save constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        PdfHelper $pdfHelper
    )
    {
        $this->fileFactory = $fileFactory;
        $this->_pdfHelper = $pdfHelper;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        if (!isset($post['pdf_background'])) {
            $post['pdf_background'] = null;
        }
        if (isset($post['pdf_coordinates'])) {
            foreach ($post['pdf_coordinates'] as $key => $pdf_coordinate) {
                if (array_key_exists("is_delete", $pdf_coordinate))
                    unset($post['pdf_coordinates'][$key]);
            }
            $post['pdf_coordinates'] = array_values($post['pdf_coordinates']);
        } else {
            $post['pdf_coordinates'] = null;
        }
        if (!$post) {
            return $resultRedirect->setPath('*/*/');
        }
        try {
            $array = [
                'title' => $post['title'],
                'enable' => $post['enable'],
                'pdf_page_width' => $post['pdf_page_width'],
                'pdf_page_height' => $post['pdf_page_height'],
                'pdf_background' => serialize($post['pdf_background']),
                'pdf_coordinates' => serialize($post['pdf_coordinates'])
            ];
            $model = $this->_objectManager->create('Magenest\Groupon\Model\Template');
            if ($post['template_id'] != null) {
                $model->load($post['template_id']);
            }
            $model->addData($array);
            $model->save();
            $this->messageManager->addSuccessMessage(__('The template has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
            if ($this->getRequest()->getParam('back') && !empty($this->getRequest()->getParam('template_id')))
                return $resultRedirect->setPath('*/*/edit', ['template_id' => $this->getRequest()->getParam('template_id')]);
            return $resultRedirect->setPath('*/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Something went wrong while saving the rule.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($post);
            return $resultRedirect->setPath('*/*/edit', ['template_id' => $this->getRequest()->getParam('template_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
