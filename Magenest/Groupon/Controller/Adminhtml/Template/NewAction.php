<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magenest\Groupon\Controller\Adminhtml\Template;

/**
 * Class NewAction
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class NewAction extends Template
{
    /**
     * forward to edit
     *
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
