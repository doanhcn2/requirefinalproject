<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Groupon\Controller\Adminhtml\Tempalte;

use Magento\Framework\Controller\ResultFactory;
use Magenest\Groupon\Controller\Adminhtml\Template as TemplateController;

/**
 * Class MassSendMail
 * @package Magenest\Groupon\Controller\Adminhtml\Tempalte
 */
class MassSendMail extends TemplateController
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());

        $i = 0;
        foreach ($collection as $item) {
            /** @var \Magenest\Ticket\Model\Ticket $item */
            $this->_ticketFactory->create()->sendMail($item->getId());
            $i++;
        }
        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been sent.', $i));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
