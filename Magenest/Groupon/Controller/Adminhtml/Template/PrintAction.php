<?php
/**
 * Created by PhpStorm.
 */

namespace Magenest\Groupon\Controller\Adminhtml\Template;

use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magenest\Groupon\Helper\Pdf as PdfHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Save
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class PrintAction extends \Magento\Backend\App\Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * Save constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        PdfHelper $pdfHelper
    )
    {
        $this->fileFactory = $fileFactory;
        $this->_pdfHelper = $pdfHelper;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $id = $this->getRequest()->getParam('template_id');
            $template = \Magento\Framework\App\ObjectManager::getInstance()->create(\Magenest\Groupon\Model\Template::class)->load($id)->getData();
            return $this->fileFactory->create(
                sprintf('template-%s.pdf', $template['title']),
                $this->_pdfHelper->getPdf($id)->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($template);
            return $resultRedirect->setPath('*/*/edit', ['template_id' => $id]);
        }
    }
}
