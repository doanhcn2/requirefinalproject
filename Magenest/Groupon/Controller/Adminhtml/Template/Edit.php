<?php
namespace Magenest\Groupon\Controller\Adminhtml\Template;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Magenest\Groupon\Controller\Adminhtml\Template
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $id = $this->getRequest()->getParam('template_id');
        if($id){
            $this->messageManager->addNoticeMessage(__("Remember to save template before Preview template. "));
            $model = $this->_objectManager->create('Magenest\Groupon\Model\Template')->load($id);
            $title = "Edit Template: ".$model->getTitle();
        }else{
            $title = 'New Template';
        }

        // 4. Register model to use later in blocks
        $this->_objectManager->create('Magento\Framework\Registry')->register('ticket_template', @$model);

        $resultPage->getConfig()->getTitle()
            ->prepend($title);
        return $resultPage;
    }
}