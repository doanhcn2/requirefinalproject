<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Groupon;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/***
 * Class Index
 * @package Magenest\Groupon\Controller\Adminhtml\Groupon
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Groupon::groupon');
        $resultPage->addBreadcrumb(__('Groupon'), __('Groupon'));
        $resultPage->addBreadcrumb(__('Manage Groupon'), __('Manage Groupon'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Groupon'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::deal');
    }
}
