<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Groupon;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magenest\Groupon\Helper\Pdf as PdfHelper;
use Magenest\Groupon\Model\GrouponFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Store\Model\Store;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class PrintTicket
 * @package Magenest\Groupon\Controller\Adminhtml\Groupon
 */
class PrintTicket extends Action
{
    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * @var Filesystem\Directory\WriteInterface
     */
    protected $_directory;

    /**
     * @var GrouponFactory
     */
    protected $_groupon;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var FileFactory
     */
    protected $fileFactory;


    /**
     * PrintTicket constructor.
     * @param Context $context
     * @param PdfHelper $pdfHelper
     * @param GrouponFactory $grouponFactory
     * @param Filesystem $filesystem
     * @param LoggerInterface $loggerInterface
     * @param StoreManagerInterface $storeManager
     * @param Store $store
     * @param PageFactory $resultPageFactory
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        PdfHelper $pdfHelper,
        GrouponFactory $grouponFactory,
        Filesystem $filesystem,
        LoggerInterface $loggerInterface,
        StoreManagerInterface $storeManager,
        Store $store,
        PageFactory $resultPageFactory,
        DateTime $dateTime,
        FileFactory $fileFactory
    ) {
        parent::__construct($context);
        $this->_pdfHelper = $pdfHelper;
        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->_groupon = $grouponFactory;
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
        $this->logger = $loggerInterface;
        $this->store = $store;
        $this->resultPageFactory = $resultPageFactory;
        $this->dateTime = $dateTime;
        $this->fileFactory = $fileFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $grouponId = (int)$this->getRequest()->getParam('id');
//        $this->logger->debug(print_r($grouponId, true));
        if ($grouponId) {
            $model = $this->_groupon->create()->load($grouponId);
            if ($model->getId()) {
                try {
                    return $this->fileFactory->create(
                        sprintf('Coupon_%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
                        $this->_pdfHelper->getPdf(null, $model)->render(),
                        DirectoryList::VAR_DIR,
                        'application/pdf'
                    );
                } catch (\Zend_Pdf_Exception $e) {
                } catch (\Exception $e) {
                }
            }
        }

        return $resultPage;
    }

    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::groupon');
    }
}
