<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Adminhtml\Groupon;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magenest\Groupon\Controller\Adminhtml\Groupon as GrouponController;

/**
 * Class MassStatus
 * @package Magenest\Groupon\Controller\Adminhtml\Groupon
 */
class MassStatus extends GrouponController
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->grouponCollection->create());
        $status = (int) $this->getRequest()->getParam('status');
        $totals = 0;
        try {
            foreach ($collection as $item) {
                /** @var \Magenest\Groupon\Model\Groupon $item */
                if ($status == 2) {
                    $item->redeemCoupon();
                } else {
                    $data = [
                        'status' =>  $status,
                        'redeemed_at' => date('d-m-Y H:i:s'),
                        'updated_at' => date('d-m-Y H:i:s')
                    ];
                    $item->addData($data)->save();
                }
                $totals++;
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been updated.', $totals));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->_getSession()->addException($e, __('Something went wrong while updating the product(s) status.'));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
