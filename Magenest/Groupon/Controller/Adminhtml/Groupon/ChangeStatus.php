<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */
namespace Magenest\Groupon\Controller\Adminhtml\Groupon;

use Magenest\Groupon\Model\Status;
use Magenest\Groupon\Model\StatusFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\ObjectManager;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Psr\Log\LoggerInterface;

class ChangeStatus extends Action
{
    /**
     * Massactions filter
     *
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * ChangeStatus constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param StatusFactory $statusFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StatusFactory $statusFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->statusFactory = $statusFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        try {
            $comment = $this->getRequest()->getParam('comment');
            $status = $this->getRequest()->getParam('status');

            if (is_null($status)) {
                throw new \Exception('Please choose Campaign Status');
            }
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $processedProduct = 0;
            /** @var Status $statusHistory */
            $statusHistory = $this->statusFactory->create();
            /** @var \Magento\Catalog\Model\Product $product */
            foreach ($collection->getItems() as $product) {
                $statusHistory->setData([
                    'product_id' => $product->getId(),
                    'status_code' => $status,
                    'comment' => $comment
                    ])->save();
                $product->setCampaignStatus($status);
                $product->getResource()->saveAttribute($product, 'campaign_status');
                $processedProduct++;
                $statusHistory->unsetData();
            }
            $this->messageManager->addSuccess(
                __('A total of %1 campaign(s) have been updated.', $processedProduct)
            );
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }

        return $this->resultRedirectFactory->create()->setPath('catalog/product');
    }

}