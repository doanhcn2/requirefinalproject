<?php

namespace Magenest\Groupon\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Magenest\Groupon\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_PREFIX = 'magenest_groupon_';

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            // Get module table
            $tableName = $setup->getTable('magenest_groupon_deal');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'is_business_address' => [
                        'type' => Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'comment' => 'Is Business Address'
                    ],
                    'is_delivery_address' => [
                        'type' => Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'comment' => 'Is Delivery Address'
                    ],
                    'is_online_coupon' => [
                        'type' => Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'comment' => 'Is Online Coupon'
                    ],
                    'template_id' => [
                        'type' => Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Template Id'
                    ],
                    'term' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Term'
                    ],
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
            $this->createGrouponTerm($setup->startSetup());
            $this->createGrouponTemplate($setup->startSetup());
            $this->createGrouponDelivery($setup->startSetup());
            $this->createGrouponFeedback($setup->startSetup());
            $this->createGrouponLocation($setup->startSetup());
        }

        if (version_compare($context->getVersion(), '1.1.1') < 0) {
            $tableName = $setup->getTable('magenest_groupon_groupon');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'vendor_id' => [
                        'type' => Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'comment' => 'Vendor Id'
                    ],
                    'redeemed_at' => [
                        'type' => Table::TYPE_DATETIME,
                        'nullable' => true,
                        'comment' => 'redeemed_at'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        if (version_compare($context->getVersion(), '1.1.2') < 0) {
            $tableName = $setup->getTable('magenest_groupon_deal');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'location_ids' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Location Ids'
                    ],
                    'delivery_ids' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Delivery Ids'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        if (version_compare($context->getVersion(), '1.1.3') < 0) {
            $tableName = $setup->getTable('magenest_groupon_deal');
            $connection = $setup->getConnection();
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection->changeColumn(
                    $tableName,
                    'groupon_expire',
                    'groupon_expire',
                    [
                        'type' => Table::TYPE_DATE
                    ]
                );
                $connection->changeColumn(
                    $tableName,
                    'reminder_before_day',
                    'reminder_before_day',
                    [
                        'type' => Table::TYPE_DATE
                    ]
                );
                $connection->changeColumn(
                    $tableName,
                    'start_time',
                    'start_time',
                    [
                        'type' => Table::TYPE_DATE
                    ]
                );
                $connection->changeColumn(
                    $tableName,
                    'end_time',
                    'end_time',
                    [
                        'type' => Table::TYPE_DATE
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.1.4') < 0) {
            $this->createCampaignHistoryTable($setup);
        }

        if (version_compare($context->getVersion(), '1.1.5') < 0) {
            $this->addCustomerSpecifyLocationColumn($setup);
            $this->checkLocationNameColumn($setup);
            $this->addRedemptionLocationIdColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.1.8') < 0) {
            $tableName = $setup->getTable('magenest_groupon_deal');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $setup->getConnection()->changeColumn(
                    $tableName,
                    'reminder_before_day',
                    'redeemable_after',
                    [
                        'type' => Table::TYPE_DATE,
                        'nullable' => true,
                        'comment' => 'Redeemable after'
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.1.9') < 0) {
            $this->modifyGrouponPrice($setup);
        }

        if (version_compare($context->getVersion(), '1.1.10') < 0) {
            $this->createDealInfoTable($setup);
        }

        if (version_compare($context->getVersion(), '1.1.11') < 0) {
            $this->addRefundedAtCollumn($setup);
        }

        if (version_compare($context->getVersion(), '1.2.2') < 0) {
            $tableName = $setup->getTable('magenest_groupon_deal_info');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'commissions' => [
                        'type' => Table::TYPE_FLOAT,
                        'nullable' => true,
                        'comment' => 'Commission'
                    ],
                    'location_ids' => [
                        'type' => Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Location Ids'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
            $grouponTable = $setup->getTable('magenest_groupon_groupon');
            if ($setup->getConnection()->isTableExists($grouponTable) == true) {
                $setup->getConnection()->addColumn(
                    $grouponTable,
                    'commission',
                    [
                        'type' => Table::TYPE_FLOAT,
                        'nullable' => true,
                        'comment' => 'Commission Percent'
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.2.3') < 0) {
            $tableName = $setup->getTable('magenest_groupon_groupon');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'customer_status' => [
                        'type' => Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Customer Status of Coupon'
                    ]
                ];
                $connection = $setup->getConnection();
                foreach($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }

        if (version_compare($context->getVersion(), '1.3.0') < 0) {
            $this->modifyGrouponTime($setup);
        }

        if (version_compare($context->getVersion(), '1.2.4') < 0) {
            $this->createDeal2LocationTable($setup);
        }
        $setup->endSetup();
    }

    private function createDeal2LocationTable(SchemaSetupInterface $setup) {
        $table = $setup->getConnection()->newTable($setup->getTable('magenest_groupon_deals_locations'));
        $table->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Identifier column'
        )->addColumn(
            'deal_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Deal ID'
        )->addColumn(
            'location_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Location_id'
        )->addForeignKey(
            $setup->getFkName(
                'magenest_groupon_deals_locations',
                'deal_id',
                'magenest_groupon_deal',
                'deal_id'
            ),
            'deal_id',
            $setup->getTable('magenest_groupon_deal'),
            'deal_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $setup->getFkName(
                'magenest_groupon_deals_locations',
                'location_id',
                'magenest_groupon_location',
                'location_id'
            ),
            'location_id',
            $setup->getTable('magenest_groupon_location'),
            'location_id',
            Table::ACTION_CASCADE
        );
        $setup->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponTerm($installer)
    {
        $tableName = self::TABLE_PREFIX . 'term';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'term_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Term Id'
        )->addColumn(
            'label',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false,],
            'Label'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true,],
            'Content'
        )->setComment(
            'Term Review'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponTemplate($installer)
    {
        $tableName = self::TABLE_PREFIX . 'template';

        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'template_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Location ID'
        )->addColumn(
            'enable',
            Table::TYPE_INTEGER,
            2,
            ['nullable' => true, 'default' => 0],
            'Enable'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Title'
        )->addColumn(
            'pdf_coordinates',
            Table::TYPE_TEXT,
            '64K',
            [],
            'PDF Ticket Coordinates'
        )->addColumn(
            'pdf_page_width',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Width'
        )->addColumn(
            'pdf_page_height',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Height'
        )->addColumn(
            'pdf_background',
            Table::TYPE_TEXT,
            null,
            [],
            'PDF Ticket Background'
        )->setComment('PDF Template');

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponDelivery($installer)
    {
        $tableName = self::TABLE_PREFIX . 'delivery';

        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn('delivery_id', Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,], 'Delivery ID'
        )->addColumn('address', Table::TYPE_TEXT, null, ['nullable' => true], 'Address'
        )->addColumn('distance', Table::TYPE_FLOAT, 255, ['nullable' => true], 'Distance'
        )->addColumn('product_id', Table::TYPE_INTEGER, null, ['nullable' => true], 'Product Id'
        )->addColumn('note', Table::TYPE_TEXT, null, ['nullable' => true], 'Note'
        )->addColumn('vendor_id', Table::TYPE_INTEGER, null, ['nullable' => true], 'Vendor Id'
        )->setComment('Delivery Table');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createGrouponFeedback($installer)
    {
        $tableName = self::TABLE_PREFIX . 'feedback';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'feedback_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Deal Id'
        )->addColumn(
            'coupon_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'coupon Id'
        )->addColumn(
            'ratings',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false,],
            'Ratings'
        )->addColumn(
            'feedback',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'Feedback'
        )->setComment(
            'Feedback Review'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponLocation($installer)
    {
        $tableName = self::TABLE_PREFIX . 'location';
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn('location_id', Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Location Id')
            ->addColumn('product_id', Table::TYPE_INTEGER, null, ['nullable' => true,], 'product Id')
            ->addColumn('deal_id', Table::TYPE_INTEGER, null, ['nullable' => true,], 'Deal Id')
            ->addColumn('location_name', Table::TYPE_TEXT, null, ['nullable' => true,], 'Location / Branch Name')
            ->addColumn('street', Table::TYPE_TEXT, null, ['nullable' => true,], 'Street')
            ->addColumn('street_two', Table::TYPE_TEXT, null, ['nullable' => true,], 'Street 2')
            ->addColumn('city', Table::TYPE_TEXT, null, ['nullable' => true,], 'City')
            ->addColumn('country', Table::TYPE_TEXT, null, ['nullable' => true,], 'Country')
            ->addColumn('region', Table::TYPE_TEXT, null, ['nullable' => true,], 'Region')
            ->addColumn('postcode', Table::TYPE_TEXT, null, ['nullable' => true,], 'Postcode')
            ->addColumn('phone', Table::TYPE_TEXT, null, ['nullable' => true,], 'Phone')
            ->addColumn('description', Table::TYPE_TEXT, null, ['nullable' => true,], 'Description')
            ->addColumn('status', Table::TYPE_INTEGER, 2, ['nullable' => false,], 'Status')
            ->addColumn('map_info', Table::TYPE_TEXT, 255, ['default' => ''], 'Map Info')
            ->addColumn('business_hour', Table::TYPE_TEXT, 255, ['default' => ''], 'Business Hours')
            ->addColumn('attached_file', Table::TYPE_TEXT, 255, ['default' => ''], 'Vendor Id')
            ->addColumn('vendor_id', Table::TYPE_INTEGER, 10, ['unsigned' => true], 'Vendor registration Id')
            ->addColumn('vendor_registration_id', Table::TYPE_INTEGER, 10, ['unsigned' => true])
            ->setComment('Location Review');
        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     */
    private function createCampaignHistoryTable($installer)
    {
        $tableName = self::TABLE_PREFIX . 'status_history';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName))
            ->addColumn(
                'status_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true
                ],
                'Status ID'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'unsigned' => true
                ],
                'Product ID'
            )->addColumn(
                'status_code',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default' => 2
                ],
                'Status Code'
            )->addColumn(
                'comment',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                'Comment'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT
                ],
                'Created At'
            )->addColumn(
                'is_customer_notified',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default' => 0
                ],
                'Customer is notified'
            )->addForeignKey(
                $installer->getFkName('magenest_groupon_status_history', 'product_id', 'catalog_product_entity', 'entity_id'),
                'product_id',
                $installer->getTable('catalog_product_entity'),
                'entity_id',
                Table::ACTION_CASCADE
            )
            ->setComment('Campaign History Table');
        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addCustomerSpecifyLocationColumn($setup)
    {
        $existed = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_PREFIX . 'deal'),
            'customer_specify_location'
        );
        if (!$existed) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_PREFIX . 'deal'),
                'customer_specify_location',
                [
                    'type' => Table::TYPE_SMALLINT,
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Customer must specify location'
                ]
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function checkLocationNameColumn($setup)
    {
        $existed = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_PREFIX . 'location'),
            'location_name'
        );
        if (!$existed) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_PREFIX . 'location'),
                'location_name',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Location Name'
                ]
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addRedemptionLocationIdColumn($setup)
    {
        $existed = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_PREFIX . 'groupon'),
            'redemption_location_id'
        );
        if (!$existed) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_PREFIX . 'groupon'),
                'redemption_location_id',
                [
                    'type' => Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Redemption Location ID',
                    'unsigned' => true
                ]
            );
        }
    }

    private function modifyGrouponPrice(SchemaSetupInterface $setup)
    {
        $tableName = self::TABLE_PREFIX . 'groupon';
        $setup->startSetup();
        $setup->getConnection()->modifyColumn(
            $setup->getTable($tableName),
            'price',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '10,2',
                'nullable' => true,
                'default' => 0.00,
                'comment' => 'Groupon Price'
            ]
        );
    }

    private function createDealInfoTable(SchemaSetupInterface $setup)
    {
        $tableName = self::TABLE_PREFIX . 'deal_info';
        $table = $setup->getConnection()->newTable($setup->getTable($tableName))
            ->addColumn('info_id', Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Deal Info Id')
            ->addColumn('deal_id', Table::TYPE_INTEGER, null, ['nullable' => true,], 'Deal ID')
            ->addColumn('product_id', Table::TYPE_INTEGER, null, ['nullable' => true,], 'Product Id')
            ->addColumn('sold', Table::TYPE_DECIMAL, '10,2', ['nullable' => true,], 'Sold Number')
            ->addColumn('revenue', Table::TYPE_DECIMAL, '10,2', ['nullable' => true,], 'Revenue')
            ->addColumn('view_count', Table::TYPE_INTEGER, null, ['nullable' => true,], 'View Number');;
        $setup->getConnection()->createTable($table);
    }

    private function addRefundedAtCollumn(SchemaSetupInterface $setup)
    {
        $tableName = self::TABLE_PREFIX . 'groupon';
        $setup->getConnection()->addColumn($setup->getTable($tableName),
            'refunded_at',
            [
                'type' => Table::TYPE_DATETIME,
                'length' => null,
                'nullable' => true,
                'comment' => 'Refunded At'
            ]);

    }

    private function modifyGrouponTime(SchemaSetupInterface $setup)
    {
        $tableName = self::TABLE_PREFIX . 'deal';
        $setup->startSetup();
        $times = [
            'start_time' => "Start Time",
            'end_time' => "End Time",
            'groupon_expire' => "Groupon Expire",
            'redeemable_after' => "Redeemable After"
        ];
        foreach($times as $key => $time) {
            $setup->getConnection()->modifyColumn(
                $setup->getTable($tableName),
                $key,
                [
                    'type' => Table::TYPE_DATETIME,
                    'nullable' => true,
                    'comment' => $time
                ]
            );
        }
    }
}
