<?php
namespace Magenest\Groupon\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Magenest\Groupons\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    const TABLE_PREFIX = 'magenest_groupon_';

    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createDealTable($installer);
        $this->createTicketTable($installer);
        $this->createDealOptionTable($installer);
        $this->createGrouponTable($installer);
//        $this->createGrouponFeedback($installer);
//        $this->createGrouponLocation($installer);
//        $this->createGrouponTerm($installer);
//        $this->createGrouponTemplate($installer);
//        $this->createGrouponDelivery($installer);

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createDealTable($installer)
    {
        $tableName = self::TABLE_PREFIX.'deal';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'deal_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Deal Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'Product Id'
        )->addColumn(
            'parent_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true,],
            'Parent Id'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'Store Id'
        )->addColumn(
            'product_type',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false,],
            'Product Type'
        )->addColumn(
            'start_time',
            Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'Start Time'
        )->addColumn(
            'end_time',
            Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'End Time'
        )->addColumn(
            'groupon_expire',
            Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'Groupon Expire'
        )->addColumn(
            'reminder_before_day',
            Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'Reminder Before Day'
        )->addColumn(
            'reminder_template',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Reminder Template'
        )->addColumn(
            'enable',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Enable'
        )->addColumn(
            'vendor_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Vendor Id'
        )->addColumn(
            'minimum_buyers_limit',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Minimum Buyers Limit'
        )->addColumn(
            'maximum_buyers_limit',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Maximum Buyers Limit'
        )->addColumn(
            'allow_credit_pay',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Allow Credit Pay'
        )->addColumn(
            'view_count',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'View Count'
        )->addColumn(
            'purchased_count',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Purchased Count'
        )->addColumn(
            'status',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Status'
        )->addColumn(
            'location',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Location'
        )->addColumn(
            'priority',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Priority'
        )->addColumn(
            'user_limit',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'User Limit'
        )->addColumn(
            'qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Qty'
        )->addColumn(
            'available_qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Available Qty'
        )->addColumn(
            'purchased_qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Purchased Qty'
        )->setComment(
            'Deal Table'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createTicketTable($installer)
    {
        $tableName = self::TABLE_PREFIX.'deal_ticket';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'ticket_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Ticket Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Product Id'
        )->addColumn(
            'terms',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Terms'
        )->addColumn(
            'instructions',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Instructions'
        )->addColumn(
            'coordinates',
            Table::TYPE_TEXT,
            '64K',
            [],
            'Coordinates'
        )
        ->addColumn(
            'page_width',
            Table::TYPE_INTEGER,
            null,
            [],
            'Page Width'
        )->addColumn(
            'page_height',
            Table::TYPE_INTEGER,
            null,
            [],
            'Page Height'
        )->addColumn(
            'background',
            Table::TYPE_TEXT,
            null,
            [],
            'Background'
        )
        ->setComment(
            'Ticket Table'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createDealOptionTable($installer)
    {
        $tableName = self::TABLE_PREFIX.'deal_option';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'option_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Option Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Product Id'
        )->addColumn(
            'enabled',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Enabled'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Store Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Name'
        )->addColumn(
            'discount',
            Table::TYPE_DECIMAL,
            '12,2',
            ['nullable' => true],
            'Discount'
        )->addColumn(
            'groupon_price',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Groupon Price'
        )->addColumn(
            'deal_price',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Deal Price'
        )->addColumn(
            'qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Qty'
        )->addColumn(
            'available_qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Available Qty'
        )->addColumn(
            'purchased_qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Purchased Qty'
        )->addColumn(
            'affiliate',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Affiliate'
        )->addColumn(
            'sku',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Sku'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Description'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Title'
        )->addColumn(
            'deal_value',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Deal Value'
        )->addColumn(
            'commission',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Commission'
        )->addColumn(
            'credit_on_purchase',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Credit On Purchase'
        )->addColumn(
            'quantity_purchase',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Quantity Purchase'
        )->addColumn(
            'as_main',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'As Main'
        )->addColumn(
            'sequence',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Sequence'
        )->addColumn(
            'max_limit',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Max Limit'
        )->addColumn(
            'max_person_limit',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Max Person Limit'
        )->addColumn(
            'code_type',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Code Type'
        )->addColumn(
            'code_value',
            Table::TYPE_FLOAT,
            null,
            ['nullable' => true],
            'Code Value'
        )->addColumn(
            'active',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Active'
        )->setComment(
            'Option Table'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createGrouponTable($installer)
    {
        $tableName = self::TABLE_PREFIX.'groupon';
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'groupon_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Groupon Id'
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Product Id'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Customer Id'
        )->addColumn(
            'vendor_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Vendor Id'
        )->addColumn(
            'product_name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Product Name'
        )->addColumn(
            'groupon_code',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Groupon Code'
        )->addColumn(
            'redemption_code',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Redemption Code'
        )->addColumn(
            'order_id',
            Table::TYPE_INTEGER,
            255,
            ['nullable' => true],
            'Order Id'
        )->addColumn(
            'status',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Status'
        )->addColumn(
            'recipient',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Recipient'
        )->addColumn(
            'created_at',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Created At'
        )->addColumn(
            'redeem_at',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Redeemed At'
        )->addColumn(
            'updated_at',
            Table::TYPE_DATETIME,
            null,
            ['nullable' => true],
            'Updated At'
        )->addColumn(
            'price',
            Table::TYPE_DECIMAL,
            null,
            ['nullable' => true],
            'Price'
        )->addColumn(
            'qty',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Qty'
        )->addColumn(
            'option_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Option Id'
        )->addColumn(
            'location',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Location'
        )->addColumn(
            'customer_email',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Customer Email'
        )->addColumn(
            'customer_name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Customer Name'
        )->addColumn(
            'is_sent',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Is Sent'
        )->addColumn(
            'is_customer_used',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Is Customer Used'
        )->setComment(
            'Groupon Table'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createGrouponFeedback($installer)
    {
        $tableName = self::TABLE_PREFIX.'feedback';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'feedback_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Deal Id'
        )->addColumn(
            'coupon_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'coupon Id'
        )->addColumn(
            'ratings',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false,],
            'Ratings'
        )->addColumn(
            'feedback',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'Feedback'
        )->setComment(
            'Feedback Review'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponLocation($installer)
    {
        $tableName = self::TABLE_PREFIX . 'location';
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn('location_id',  Table::TYPE_INTEGER,    null,  ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Location Id')
            ->addColumn('product_id',   Table::TYPE_INTEGER,    null,  ['nullable' => true,], 'product Id')
            ->addColumn('deal_id',      Table::TYPE_INTEGER,    null,  ['nullable' => true,],   'Deal Id')
            ->addColumn('location_name',Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Location / Branch Name')
            ->addColumn('street',       Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Street')
            ->addColumn('street_two',   Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Street 2')
            ->addColumn('city',         Table::TYPE_TEXT,       null,  ['nullable' => true,],   'City')
            ->addColumn('country',      Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Country')
            ->addColumn('region',       Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Region')
            ->addColumn('postcode',     Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Postcode')
            ->addColumn('phone',        Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Phone')
            ->addColumn('description',  Table::TYPE_TEXT,       null,  ['nullable' => true,],   'Description')
            ->addColumn('status',       Table::TYPE_INTEGER,    2,     ['nullable' => true,],  'Status')
            ->addColumn('map_info',     Table::TYPE_TEXT,       null,   ['default' => ''],       'Map Info')
            ->addColumn('business_hour',Table::TYPE_TEXT,       null,   ['default' => ''],       'Business Hours')
            ->addColumn('attached_file',Table::TYPE_TEXT,       null,   ['default' => ''],       'Vendor Id')
            ->addColumn('vendor_id',    Table::TYPE_INTEGER,    10,    ['unsigned' => true],    'Vendor registration Id')
            ->addColumn('vendor_registration_id',Table::TYPE_INTEGER, 10, ['unsigned' => true])
            ->setComment('Location');
        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponTerm($installer)
    {
        $tableName = self::TABLE_PREFIX.'term';

        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'term_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Term Id'
        )->addColumn(
            'label',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false,],
            'Label'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true,],
            'Content'
        )->setComment(
            'Term Review'
        );

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponTemplate($installer)
    {
        $tableName = self::TABLE_PREFIX.'template';

        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'template_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary'  => true,
                'unsigned' => true,
            ],
            'Location ID'
        )->addColumn(
            'enable',
            Table::TYPE_INTEGER,
            2,
            ['nullable' => true, 'default' => 0],
            'Enable'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Title'
        ) ->addColumn(
            'pdf_coordinates',
            Table::TYPE_TEXT,
            '64K',
            [],
            'PDF Ticket Coordinates'
        )->addColumn(
            'pdf_page_width',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Width'
        )->addColumn(
            'pdf_page_height',
            Table::TYPE_INTEGER,
            null,
            [],
            'PDF Ticket Page Height'
        )->addColumn(
            'pdf_background',
            Table::TYPE_TEXT,
            null,
            [],
            'PDF Ticket Background'
        )->setComment('PDF Template');

        $installer->getConnection()->createTable($table);
    }

    /**
     * /**
     * @param $installer
     */
    private function createGrouponDelivery($installer)
    {
        $tableName = self::TABLE_PREFIX.'delivery';

        if ($installer->tableExists($tableName)) {
            return;
        }
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName))
            ->addColumn('delivery_id',     Table::TYPE_INTEGER,    null, ['identity' => true,  'nullable' => false, 'primary'  => true, 'unsigned' => true,], 'Delivery ID')
            ->addColumn('address',         Table::TYPE_TEXT,       null, ['nullable' => true], 'Address')
            ->addColumn('distance',        Table::TYPE_FLOAT,      null, ['nullable' => true], 'Distance')
            ->addColumn('product_id',      Table::TYPE_INTEGER,    null, ['nullable' => true], 'Product Id')
            ->addColumn('note',            Table::TYPE_TEXT,       null, ['nullable' => true], 'Note')
            ->addColumn('map_info',        Table::TYPE_TEXT,       null,  ['default' => ''],    'Map Info')
            ->addColumn('vendor_id',       Table::TYPE_INTEGER,    10,   ['unsigned' => true],    'Vendor registration Id')
            ->addColumn('vendor_registration_id',Table::TYPE_INTEGER, 10,['unsigned' => true])
            ->setComment('Delivery Table');
        $installer->getConnection()->createTable($table);
    }
}
