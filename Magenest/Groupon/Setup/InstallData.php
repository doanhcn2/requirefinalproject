<?php
namespace Magenest\Groupon\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type as ProductType;

/**
 * Class InstallData
 * @package Magenest\Groupon\Setup
 */
class InstallData implements InstallDataInterface
{
    const PRODUCT_TYPE = 'coupon';
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory
     */
    private $attributeFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Helper\Product
     */
    private $productHelper;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory
     */
    private $attrOptionCollectionFactory;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\SetFactory
     */
    private $attributeSetFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory
     * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
    
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeFactory = $attributeFactory;
        $this->productHelper = $productHelper;
        $this->eavConfig = $eavConfig;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->storeManager = $storeManager;
        $this->attrOptionCollectionFactory = $attrOptionCollectionFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $fieldList = [
            'price',
            'special_price',
            'special_from_date',
            'special_to_date',
            'minimal_price',
            'cost',
            'tier_price',
            'weight',
        ];

        foreach ($fieldList as $field) {
            $applyTo = explode(
                ',',
                $eavSetup->getAttribute(Product::ENTITY, $field, 'apply_to')
            );
            if (!in_array('coupon', $applyTo)) {
                $applyTo[] = 'coupon';
                $eavSetup->updateAttribute(
                    Product::ENTITY,
                    $field,
                    'apply_to',
                    implode(',', $applyTo)
                );
            }
        }

        try {
            $this->createConfigurableAttribute();
            $this->createVirtualProductAttribute($setup);
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Install Data Error: '.$e->getMessage());
        }
    }

    private function createVirtualProductAttribute($setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'short_title',
            [
                'type' => 'text',
                'visible' => true,
                'required' => false,
                'input' => 'text',
                'user_defined' => false,
                'filterable' => true,
                'visible_on_front' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Short Title',
                'system' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'commission',
            [
                'type' => 'decimal',
                'visible' => true,
                'required' => false,
                'input' => 'text',
                'user_defined' => false,
                'filterable' => true,
                'visible_on_front' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Commission (%)',
                'system' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'campaign_status',
            [
                'type' => 'varchar',
                'sort_order' => 10,
                'label' => 'Campaign Status',
                'input' => 'select',
                'source' => 'Magenest\Groupon\Model\Product\Attribute\CampaignStatus',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => true,
                'user_defined' => false,
                'default' => 2,
                'filterable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to'=>'configurable,coupon'
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'affiliate',
            [
                'type' => 'decimal',
                'visible' => true,
                'required' => false,
                'input' => 'text',
                'user_defined' => false,
                'filterable' => true,
                'visible_on_front' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Affiliate (%)',
                'system' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'featured_deal',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Featured Deal',
                'input' => 'boolean',
                'class' => '',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => 1,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'minimum_buyers_limit',
            [
                'type' => 'decimal',
                'visible' => true,
                'required' => false,
                'input' => 'text',
                'user_defined' => false,
                'filterable' => true,
                'visible_on_front' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Minimum Buyers Limit',
                'system' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'maximum_buyers_limit',
            [
                'type' => 'decimal',
                'visible' => true,
                'required' => false,
                'input' => 'text',
                'user_defined' => false,
                'filterable' => true,
                'visible_on_front' => false,
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Maximum Buyers Limit',
                'system' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'apply_to' => join(',', [self::PRODUCT_TYPE])
            ]
        );
    }

    private function createConfigurableAttribute()
    {
        $attributeCount = 0;

        $data = $this->getAttributeData();
        $data['attribute_set'] = explode(",", $data['attribute_set']);

        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->eavConfig->getAttribute('catalog_product', $data['attribute_code']);
        if (!$attribute) {
            $attribute = $this->attributeFactory->create();
        }

        $frontendLabel = explode(",", $data['frontend_label']);
        if (count($frontendLabel) > 1) {
            $data['frontend_label'] = [];
            $data['frontend_label'][\Magento\Store\Model\Store::DEFAULT_STORE_ID] = $frontendLabel[0];
            $data['frontend_label'][$this->storeManager->getDefaultStoreView()->getStoreId()] =
                $frontendLabel[1];
        }
        $data['option'] = $this->getOption($attribute, $data);
        $data['source_model'] = $this->productHelper->getAttributeSourceModelByInputType(
            $data['frontend_input']
        );
        $data['backend_model'] = $this->productHelper->getAttributeBackendModelByInputType(
            $data['frontend_input']
        );
        $data += ['is_filterable' => 0, 'is_filterable_in_search' => 0];
        $data['backend_type'] = $attribute->getBackendTypeByInput($data['frontend_input']);

        $attribute->addData($data);
        $attribute->setIsUserDefined(1);

        $attribute->setEntityTypeId($this->getEntityTypeId());
        $attribute->save();
        $attributeId = $attribute->getId();

        if (is_array($data['attribute_set'])) {
            foreach ($data['attribute_set'] as $setName) {
                $setName = trim($setName);
                $attributeCount++;
                $attributeSet = $this->processAttributeSet($setName);
                $attributeGroupId = $attributeSet->getDefaultGroupId();

                $attribute = $this->attributeFactory->create()->load($attributeId);
                $attribute
                    ->setAttributeGroupId($attributeGroupId)
                    ->setAttributeSetId($attributeSet->getId())
                    ->setEntityTypeId($this->getEntityTypeId())
                    ->setSortOrder($attributeCount + 999)
                    ->save();
            }
        }

        $this->eavConfig->clear();
    }

    private function getAttributeData()
    {
        //Set up Attribute
        $attributeData = [
            'frontend_label' => 'Groupon Option Number',
            'frontend_input' => 'select',
            'is_required' => '1',
            'option' => 'Option 1',
            'default' => null,
            'attribute_code' => 'groupon_option',
            'is_global' => '1',
            'default_value_text' => null,
            'default_value_yesno' => null,
            'default_value_date' => null,
            'default_value_textarea' => null,
            'is_unique' => 0,
            'is_searchable' => 0,
            'is_visible_in_advanced_search' => 0,
            'is_comparable' => null,
            'is_filterable' => 1,
            'is_filterable_in_search' => 0,
            'position' => null,
            'is_used_for_promo_rules' => 1,
            'is_html_allowed_on_front' => 1,
            'is_visible_on_front' => 1,
            'used_in_product_listing' => 0,
            'used_for_sort_by' => 0,
            'attribute_set' => 'Default'
        ];
        return $attributeData;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute
     * @param array $data
     * @return array
     */
    protected function getOption($attribute, $data)
    {
        $result = [];
        $data['option'] = explode(",", $data['option']);
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $options */
        $options = $this->attrOptionCollectionFactory->create()
            ->setAttributeFilter($attribute->getId())
            ->setPositionOrder('asc', true)
            ->load();
        foreach ($data['option'] as $value) {
            if (!$options->getItemByColumnValue('value', $value)) {
                $result[] = $value;
            }
        }
        return $result ? $this->convertOption($result) : $result;
    }

    /**
     * Converting attribute options from csv to correct sql values
     *
     * @param array $values
     * @return array
     */
    protected function convertOption($values)
    {
        $result = ['order' => [], 'value' => []];
        $i = 0;
        foreach ($values as $value) {
            $result['order']['option_' . $i] = (string)$i;
            $result['value']['option_' . $i] = [0 => $value, 1 => ''];
            $i++;
        }

        return $result;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getEntityTypeId()
    {
        return $this->eavConfig->getEntityType(\Magento\Catalog\Model\Product::ENTITY)->getId();
    }

    /**
     * Loads attribute set by name if attribute with such name exists
     * Otherwise creates the attribute set with $setName name and return it
     *
     * @param $setName
     * @return \Magento\Eav\Model\Entity\Attribute\Set
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function processAttributeSet($setName)
    {
        /** @var \Magento\Eav\Model\Entity\Attribute\Set $attributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $setCollection = $attributeSet->getResourceCollection()
            ->addFieldToFilter('entity_type_id', $this->getEntityTypeId())
            ->addFieldToFilter('attribute_set_name', $setName)
            ->load();
        $attributeSet = $setCollection->fetchItem();

        if (!$attributeSet) {
            $attributeSet = $this->attributeSetFactory->create();
            $attributeSet->setEntityTypeId($this->getEntityTypeId());
            $attributeSet->setAttributeSetName($setName);
            $attributeSet->save();
            $defaultSetId = $this->eavConfig->getEntityType(\Magento\Catalog\Model\Product::ENTITY)
                ->getDefaultAttributeSetId();
            $attributeSet->initFromSkeleton($defaultSetId);
            $attributeSet->save();
        }
        return $attributeSet;
    }
}
