<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */

namespace Magenest\Groupon\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $categorySetupFactory;
    private $eavSetupFactory;
    private $eavConfig;
    private $dealLocationFactory;
    private $dealCollectionFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig,
        CategorySetupFactory $categorySetupFactory,
        \Magenest\Groupon\Model\DealLocationFactory $dealLocationFactory,
        \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory $dealCollectionFactory
    ) {
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->dealLocationFactory = $dealLocationFactory;
        $this->dealCollectionFactory = $dealCollectionFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.4') < 0) {
            $this->updateCampaignStatusAttribute();
        }
        if (version_compare($context->getVersion(), '1.1.7') < 0) {
            $this->updateCampaignStatusAttr();
        }
        if (version_compare($context->getVersion(), '1.2.0') < 0) {
            $this->updateGrouponAttr();
        }
        if (version_compare($context->getVersion(), '1.2.5') < 0) {
            $this->addDataTableDealLocation();
        }
        $setup->endSetup();
    }

    private function addDataTableDealLocation()
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $dealCollection */
        $dealCollection = $this->dealCollectionFactory->create();
        $dealCollection->addFieldToFilter('location_ids', ['notnull' => true]);
        foreach ($dealCollection as $deal) {
            $rawIds = $deal->getData('location_ids');
            if (!is_string($rawIds)) {
                continue;
            }
            $locationIds = explode(',', $rawIds);
            if (!is_array($locationIds)) {
                continue;
            }
            foreach ($locationIds as $locationId) {
                /** @var \Magenest\Groupon\Model\DealLocation $deal */
                $model = $this->dealLocationFactory->create();
                $model->setData([
                    'deal_id' => $deal->getId(),
                    'location_id' => $locationId
                ])->save();
            }
        }
    }

    private function updateCampaignStatusAttribute()
    {
        /** @var CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create();
        $categorySetup->updateAttribute(
            Product::ENTITY,
            'campaign_status',
            [
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'is_filterable_in_grid' => 1
            ]
        );
    }

    private function updateCampaignStatusAttr()
    {
        /** @var CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create();
        $categorySetup->updateAttribute(
            Product::ENTITY,
            'campaign_status',
            [
                'apply_to' => 'configurable,ticket,hotel,simple,virtual,giftcard'
            ]
        );

    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function updateGrouponAttr()
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'groupon_option');
        if (!$attribute) {
            return;
        }
        $attributeData['frontend_input'] = 'select';
        $attributeData['swatch_input_type'] = 'visual';
        $attributeData['update_product_preview_image'] = 1;
        $attributeData['use_product_image_for_swatch'] = 0;
        $attribute->addData($attributeData);
        $attribute->save();
    }
}