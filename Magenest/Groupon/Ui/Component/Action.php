<?php
namespace Magenest\Groupon\Ui\Component;

/**
 * Class Action
 */
class Action extends \Magento\Ui\Component\Action
{

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        if (!empty($this->actions)) {
            if ($callback = $this->getData('config/callback')) {
                foreach ($this->actions as &$action) {
                    $action['callback'] = $callback;
                }
            }
        }

        parent::prepare();
    }
}
