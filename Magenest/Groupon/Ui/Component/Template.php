<?php
namespace Magenest\Groupon\Ui\Component;

use Magenest\Groupon\Model\TemplateFactory;

/**
 * Class Country
 * @package Magenest\Groupon\Ui\Component
 */
class Template implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var
     */
    protected $template;

    /**
     * Template constructor.
     * @param TemplateFactory $template
     */
    public function __construct(
        TemplateFactory $template
    ) {
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $model = $this->template->create()->getCollection();
        $result = [];
        foreach ($model as $temp) {
            $array = [
                'label' => $temp->getTitle(),
                'value' => $temp->getTemplateId()
            ];
            array_push($result, $array);
        }
        return $result;
    }
}
