<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Ui\Component\Coordinates\Information;

/**
 * Class Options
 * @package Magenest\Groupon\Ui\Component\Coordinates\Information
 */
class Options implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Name'),
                'value' => 'event_name'
            ],
            [
                'label' => __('Price'),
                'value' => 'price'
            ],
            [
                'label' => __('QR Code'),
                'value' => 'qr_code'
            ],
            [
                'label' => __('Bar Code'),
                'value' => 'bar_code'
            ],
            [
                'label' => __('Redemption Code'),
                'value' => 'redemption_code'
            ],
            [
                'label' => __('Groupon Code'),
                'value' => 'groupon_code'
            ],
            [
                'label' => __('Customer Name'),
                'value' => 'customer_name'
            ],
            [
                'label' => __('Customer Email'),
                'value' => 'customer_email'
            ],
            [
                'label' => __('Order #'),
                'value' => 'order_increment_id'
            ],
            [
                'label' => __('Order Date'),
                'value' => 'order_date'
            ],
            [
                'label' => __('Quantity'),
                'value' => 'qty'
            ],
            [
                'label' => __('Vendor Name'),
                'value' => 'vendor_name'
            ],
            [
                'label' => __('Valid Date Range'),
                'value' => 'valid_date_range'
            ],
            [
                'label' => __('Redeem At'),
                'value' => 'redeem_at'
            ],
            [
                'label' => __('Terms'),
                'value' => 'term'
            ]
        ];
    }
}
