<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Ui\Component\Coordinates\Information;

/**
 * Class Color
 * @package Magenest\Groupon\Ui\Component\Coordinates\Information
 */
class Color implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Black'),
                'value' => 'black'
            ],
            [
                'label' => __('Red'),
                'value' => 'red'
            ],
            [
                'label' => __('Yellow'),
                'value' => 'yellow'
            ],
            [
                'label' => __('Blue'),
                'value' => 'blue'
            ],
            [
                'label' => __('Green'),
                'value' => 'green'
            ]
        ];
    }
}
