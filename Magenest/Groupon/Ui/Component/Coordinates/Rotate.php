<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Ui\Component\Coordinates;

/**
 * Class Options
 * @package Magenest\Groupon\Ui\Component\Coordinates\Information
 */
class Rotate implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('None'),
                'value' => '0'
            ],
            [
                'label' => __('90 degrees'),
                'value' => '90'
            ],
            [
                'label' => __('180 degrees'),
                'value' => '180'
            ],
            [
                'label' => __('270 degrees'),
                'value' => '270'
            ]
        ];
    }
}
