<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 12/05/2018
 * Time: 10:55
 */
namespace Magenest\Groupon\Ui\Component\Listing\Columns;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns;
class LocationUsed extends Columns{

    protected $dealFactory;
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        $components = [],
        array $data = []
    ) {
        $this->dealFactory = $dealFactory;
        parent::__construct($context, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $vendorIdsArray = [];
            foreach ($dataSource['data']['items'] as &$item){
                if(isset($item['vendor_id'])){
                    $vendorIdsArray[] = $item['vendor_id'];
                }
            }
            $dealsArrayItems = $this->dealFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', ['in' => $vendorIdsArray])
                ->getItems();
            $dealsArray = [];
            foreach ($dealsArrayItems as $deal){
                if($deal->getVendorId()!=''){
                    $dealsArray[$deal->getVendorId()] = explode(',',$deal->getLocationIds());
                }
            }
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['vendor_id'])&&isset($item['location_id'])) {
                    if(isset($dealsArray[$item['vendor_id']])){
                        $item ['used'] = in_array($item['location_id'], $dealsArray)?'1':'0';
                    }
                }
            }
        }
        return $dataSource;
    }

}