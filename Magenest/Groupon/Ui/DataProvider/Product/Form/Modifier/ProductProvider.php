<?php

namespace Magenest\Groupon\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductOptions\ConfigInterface;
use Magento\Catalog\Model\Config\Source\Product\Options\Price as ProductOptionsPrice;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\RequestInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magenest\Groupon\Model\LocationFactory;
use Magenest\Groupon\Model\DeliveryFactory;
use Magento\Ui\Component\Form\Fieldset;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Ui\Component\Country;
use \Magenest\Groupon\Model\Product\Attribute\CampaignStatus;

/**
 * Data provider for "Customizable Options" panel
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductProvider extends AbstractModifier
{
    const PRODUCT_TYPE = 'configurable';
    const PRODUCT_TYPE_COUPON = 'coupon';
    const CONTROLLER_ACTION_EDIT_PRODUCT = 'catalog_product_edit';
    const CONTROLLER_ACTION_NEW_PRODUCT = 'catalog_product_new';
    const EVENT_TICKET_TAB = 'event';

    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;
    /**#@+
     * Group values
     */
    const GROUP_CUSTOM_OPTIONS_NAME = 'location_options';
    const GROUP_CUSTOM_OPTIONS_SCOPE = 'data.groupon';
    /**#@-*/
    /**#@-*/


    /**#@+
     * Grid values
     */
    const GRID_TYPE_LOCATION = 'location';
    /**#@-*/

    /**#@+
     * Field values
     */

    const FIELD_STREET_NAME = 'street';
    const FIELD_CITY_NAME = 'city';
    const FIELD_COUNTRY_NAME = 'country';
    const FIELD_IS_REQUIRE = 'is_require';
    const FIELD_SORT_ORDER_NAME = 'sort_order';
    const FIELD_IS_DELETE = 'is_delete';

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    protected $locator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductOptions\ConfigInterface
     */
    protected $productOptionsConfig;

    /**
     * @var \Magento\Catalog\Model\Config\Source\Product\Options\Price
     */
    protected $productOptionsPrice;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var CurrencyInterface
     */
    protected $localeCurrency;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Country
     */
    protected $country;

    /**
     * @var LocationFactory
     */
    protected $location;

    /**
     * @var DeliveryFactory
     */
    protected $delivery;

    protected $approveState = [];

    /**
     * ProductProvider constructor.
     * @param LocatorInterface $locator
     * @param StoreManagerInterface $storeManager
     * @param ConfigInterface $productOptionsConfig
     * @param ProductOptionsPrice $productOptionsPrice
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     * @param DealFactory $dealFactory
     * @param TicketFactory $ticketFactory
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     * @param Country $country
     * @param LocationFactory $locationFactory
     * @param DeliveryFactory $deliveryFactory
     */
    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        ConfigInterface $productOptionsConfig,
        ProductOptionsPrice $productOptionsPrice,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
        DealFactory $dealFactory,
        TicketFactory $ticketFactory,
        RequestInterface $request,
        LoggerInterface $logger,
        Country $country,
        LocationFactory $locationFactory,
        DeliveryFactory $deliveryFactory
    )
    {
        $this->logger = $logger;
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->productOptionsConfig = $productOptionsConfig;
        $this->productOptionsPrice = $productOptionsPrice;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->request = $request;
        $this->country = $country;
        $this->location = $locationFactory;
        $this->delivery = $deliveryFactory;
        $this->approveState = [
            CampaignStatus::STATUS_ACTIVE => CampaignStatus::getStatusLabelByCode(CampaignStatus::STATUS_ACTIVE),
            CampaignStatus::STATUS_LIVE => CampaignStatus::getStatusLabelByCode(CampaignStatus::STATUS_LIVE),
            CampaignStatus::STATUS_EXPIRED => CampaignStatus::getStatusLabelByCode(CampaignStatus::STATUS_EXPIRED),
            CampaignStatus::STATUS_SCHEDULED => CampaignStatus::getStatusLabelByCode(CampaignStatus::STATUS_SCHEDULED),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        $campaignStatus = @$data[$productId]['product']['campaign_status'];
        $data[$productId]['product']['campaign_state'] = false;
        if ($campaignStatus && array_key_exists($campaignStatus, $this->approveState)) {
            $data[$productId]['product']['campaign_status'] = -1;
            $data[$productId]['product']['campaign_state'] = $this->approveState[$campaignStatus];
        }
        /** @var \Magenest\Groupon\Model\Deal $modelDeal */
        $modelDeal = $this->deal->create()->load($productId, 'product_id');
        if ($modelDeal && !empty($modelDeal->getData())) {
            //data deal
            $data[strval($productId)]['groupon'] = [
                'enable_groupon' => $modelDeal->getEnable(),
                'template_id' => $modelDeal->getTemplateId(),
            ];

            //data campaign date
            $data[strval($productId)]['groupon']['campaign_date'] = [
                'start_time' => $modelDeal->getStartTime(),
                'end_time' => $modelDeal->getEndTime(),
                'redeemable_after' => $modelDeal->getRedeemableAfter(),
                'groupon_expire' => $modelDeal->getGrouponExpire(),
            ];

            //data campaign term
            if ($modelDeal->getTerm()) {
                $termData = json_decode($modelDeal->getTerm(), true);
                $data[strval($productId)]['groupon']['campaign_term']['addition_terms'] = @$termData['vendor_specify'];
                $data[strval($productId)]['groupon']['campaign_term']['reservation_required'] = strval(@$termData['reservation']['enable']);
                $data[strval($productId)]['groupon']['campaign_term']['required_day'] = @$termData['reservation']['days'];
                $data[strval($productId)]['groupon']['campaign_term']['combine_coupon'] = strval(@$termData['combine']);
                $data[strval($productId)]['groupon']['campaign_term']['age_limit'] = strval(@$termData['age_limit']['enable']);
                $data[strval($productId)]['groupon']['campaign_term']['age_limit_number'] = @$termData['age_limit']['age_number'];
                $data[strval($productId)]['groupon']['campaign_term']['share_coupon'] = strval(@$termData['sharing']);
            }

            // data redemption location
            $data[$productId]['groupon']['redeem_location']['is_online_coupon'] = $modelDeal->getIsOnlineCoupon();
            $data[$productId]['groupon']['redeem_location']['is_business_address'] = $modelDeal->getIsBusinessAddress();
            $data[$productId]['groupon']['redeem_location']['customer_specify_location'] = $modelDeal->getCustomerSpecifyLocation();
            $data[$productId]['groupon']['redeem_location']['is_delivery_address'] = $modelDeal->getIsDeliveryAddress();

            $usedLocations = explode(',', $modelDeal->getLocationIds());
            $modelLocation = $this->location->create()->getCollection()
                ->addFieldToFilter('vendor_id', $modelDeal->getVendorId());
            if ($modelLocation->count() && @$modelDeal->getIsBusinessAddress() === '1') {
                $data[$productId]['groupon']['redeem_location']['is_online_coupon'] = '0';
                $data[$productId]['groupon']['redeem_location']['is_business_address'] = '1';
            }

            $usedDeliveryLocations = explode(',', $modelDeal->getDeliveryIds());
            $modelDelivery = $this->delivery->create()->getCollection()
                ->addFieldToFilter('vendor_id', $modelDeal->getVendorId());
            if ($modelDelivery->count() && @$modelDeal->getIsDeliveryAddress() === '1') {
                $data[$productId]['groupon']['redeem_location']['is_online_coupon'] = '0';
                $data[$productId]['groupon']['redeem_location']['is_delivery_address'] = '1';
                foreach ($modelDelivery as $delivery) {
                    $delivery->setData('used', '0');
                    if (in_array($delivery->getId(), $usedDeliveryLocations)) {
                        $delivery->setData('used', '1');
                    }
                    $delivery->setData('old', '1');
                    $data[$productId]['groupon']['redeem_location']['delivery_rule'][] = $delivery->getData();
                }
            }

        }

        return $data;
    }

    public function sortByUsedAndName($location1, $location2)
    {
        if (@$location1['used']) {
            return false;
        }
        if (@$location2['used']) {
            return true;
        }
        return strcmp(@$location1['location_name'], @$location2['location_name']);
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $product = $this->locator->getProduct();

        if ($product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $meta['groupon']['arguments']['data']['config'] = [
              'disabled' => true,
              'visible'  => false,
            ];
        }

        if ($product->getTypeId() == self::PRODUCT_TYPE_COUPON) {
            $meta['groupon']['arguments']['data']['config'] = [
              'disabled' => true,
              'visible'  => true,
            ];
        }

        $meta['groupon']['children']['redeem_location']['children']['business_location']['children']['business_location_list'] = [
            'children' => [
                'groupon_location_listing' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => true,
                                'componentType' => 'insertListing',
                                'dataScope' => 'groupon_location_listing',
                                'externalProvider' => 'groupon_location_listing.groupon_location_listing_data_source',
                                'selectionsProvider' => 'groupon_location_listing.groupon_location_listing.location_columns.ids',
                                'ns' => 'groupon_location_listing',
                                'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                'realTimeLink' => false,
                                'behaviourType' => 'simple',
                                'externalFilterMode' => true,
                                'imports' => [
                                    'product_id' => '${ $.provider }:data.product.current_product_id'
                                ],
                                'exports' => [
                                    'product_id' => 'groupon_location_listing.groupon_location_listing_data_source:params.product_id'
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Location List'),
                        'collapsible' => true,
                        'opened' => true,
                        'componentType' => Fieldset::NAME,
                        'sortOrder' =>'20',
                        'imports' => [
                            'visible' => 'ns = ${ $.ns }, index = is_business_address :checked'
                        ],
                    ],
                ],
            ],
        ];
        $meta['groupon']['children']['redeem_location']['children']['business_location']['children']['location_rule']
        ['arguments']['data']['config']['pageSize'] = 5;

        $meta['groupon']['children']['redeem_location']['children']['delivery_location']['children']['delivery_rule']
        ['arguments']['data']['config']['pageSize'] = 5;

        return $meta;
    }

    /**
     * Get currency symbol
     *
     * @return string
     */
    protected function getCurrencySymbol()
    {
        return $this->storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    /**
     * @return bool
     */
    protected function useGroupon()
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        $modelDeal = $this->deal->create()->load($productId, 'product_id');
        $actionName = $this->request->getFullActionName();
        $isGroupon = false;
        if ($modelDeal && $modelDeal->getEnable() == 1) {
            if ($actionName == self::CONTROLLER_ACTION_EDIT_PRODUCT) {
                /** @var \Magento\Catalog\Model\Product $product */
                $product = $this->locator->getProduct();
                if ($product->getTypeId() == self::PRODUCT_TYPE) {
                    $isGroupon = true;
                }
            } elseif ($actionName == self::CONTROLLER_ACTION_NEW_PRODUCT) {
                if (self::PRODUCT_TYPE == $this->request->getParam('type')) {
                    $isGroupon = true;
                }
            }
        }

        return $isGroupon;
    }

    /**
     * @return bool
     */
    protected function isCoupon()
    {
        $actionName = $this->request->getFullActionName();
        $isGroupon = false;
        if ($actionName == 'catalog_product_new') {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->locator->getProduct();
            if ($product->getTypeId() == self::PRODUCT_TYPE_COUPON) {
                $isGroupon = true;
            }
        } elseif ($actionName == 'catalog_product_edit') {
            if (self::PRODUCT_TYPE_COUPON == $this->request->getParam('type')) {
                $isGroupon = true;
            }
        }

        return $isGroupon;
    }
}
