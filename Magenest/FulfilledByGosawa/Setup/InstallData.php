<?php
/**
 * Author: Eric Quach
 * Date: 5/12/18
 */
namespace Magenest\FulfilledByGosawa\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{
    protected $categorySetupFactory;


    public function __construct(
        CategorySetupFactory $categorySetupFactory
    ) {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->addFulfilledByGosawaAttribute();
    }

    private function addFulfilledByGosawaAttribute()
    {
        /** @var CategorySetup $categorySetup */
        $categorySetup = $this->categorySetupFactory->create();
        $categorySetup->addAttribute(
            Product::ENTITY,
            'is_fbg',
            [
                'type' => 'static',
                'user_defined' => 0,
                'input' => 'select',
                'required' => true,
                'default' => Boolean::VALUE_NO,
                'label' => 'Fulfilled By Gosawa?',
                'source' => \Magenest\FulfilledByGosawa\Model\Entity\Attribute\Source\FulfilledByGosawa::class,
                'is_global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'is_filterable_in_grid' => 1,
                'used_for_promo_rules' => 1,
            ]
        );
    }
}