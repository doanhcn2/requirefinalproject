<?php
/**
 * Author: Eric Quach
 * Date: 5/12/18
 */
namespace Magenest\FulfilledByGosawa\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->addIsFbgColumn($setup);
        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addIsFbgColumn($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('catalog_product_entity'),
            'is_fbg',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Is Fulfilled By Gosawa'
            ]
        );
    }
}