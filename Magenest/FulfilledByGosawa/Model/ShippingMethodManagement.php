<?php
/**
 * Author: Eric Quach
 * Date: 5/18/18
 */
namespace Magenest\FulfilledByGosawa\Model;

class ShippingMethodManagement extends \Unirgy\DropshipSplit\Model\ShippingMethodManagement
{
    protected function getEstimatedRates2(\Magento\Quote\Model\Quote $quote, $country, $postcode, $regionId, $region, $city = null)
    {
        $outputByVendor = [];
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setCountryId($country);
        $shippingAddress->setPostcode($postcode);
        $shippingAddress->setRegionId($regionId);
        $shippingAddress->setRegion($region);
        $shippingAddress->setCollectShippingRates(true);
        $shippingAddress->setCity($city);
        $this->totalsCollector->collectAddressTotals($quote, $shippingAddress);
        $details = $shippingAddress->getUdropshipShippingDetails();
        if ($details) {
            $details = $this->_hlp->unserializeArr($details);
            $methods = isset($details['methods']) ? $details['methods'] : array();
        }
        $shippingRates = $shippingAddress->getGroupedAllShippingRates();
        $output = [];
        foreach ($shippingAddress->getAllItems() as $item) {
            if ($item->getIsVirtual() === "1") {
                continue;
            }
            $outputByVendor[$item->getUdropshipVendor()] = [];
        }
        foreach ($shippingRates as $carrierRates) {
            foreach ($carrierRates as $rate) {
                if (!$rate->getUdropshipVendor() && $rate->getCarrier()!='udsplit') {
                    continue;
                }
                $vId = $rate->getUdropshipVendor();
                $__out = $this->converter->modelToDataObject($rate, $quote->getQuoteCurrencyCode());
                $__out->setUdropshipVendor($vId);
                if ($rate->getCarrier()=='udsplit') {
                    $__out->setUdropshipVendor(0);
                    $output[] = $__out;
                } else {
                    if (empty($outputByVendor[$vId])) {
                        $outputByVendor[$vId][] = $this->_getVendorHeader($vId, $rate->getCarrierTitle() === Carrier::FBG_CARRIER_TITLE);
                    }
                    if ($methods[$vId] && $rate->getCarrier().'_'.$rate->getMethod() == @$methods[$vId]['code']) {
                        $__out->setUdropshipDefault(1);
                    } else {
                        $__out->setUdropshipDefault(0);
                    }
                    $outputByVendor[$vId][] = $__out;
                }
            }
        }
        $storeId = $shippingAddress->getQuote()->getStoreId();
        foreach ($outputByVendor as $vId => $__outByVid) {
            if (empty($__outByVid)) {
                $__outByVid[] = $this->_getVendorHeader($vId);
                $error = $this->_hlp->createObj('\Magento\Quote\Model\Quote\Address\RateResult\Error');
                $error->setCarrier('udsplit');
                $error->setCarrierTitle($this->_hlp->getScopeConfig('carriers/udropship/title', $storeId));
                $defMessage = $this->_hlp->getScopeConfig('carriers/udropship/specificerrmsg', $storeId);
                $error->setErrorMessage($defMessage);
                $error->setUdropshipVendor($vId);
                $__outByVid[] = $error;
            }
            foreach ($__outByVid as $out) {
                $output[] = $out;
            }
        }
        return $output;
    }

    protected function _getVendorHeader($vId, $fbg = false)
    {
        $__outHeader = parent::_getVendorHeader($vId);
        if ($fbg) {
            $__outHeader->setCarrierTitle('FBG');
        }
        return $__outHeader;
    }

    /**
     * Estimate shipping
     *
     * @param int $cartId The shopping cart ID.
     * @param int $addressId The estimate address id
     * @return \Unirgy\DropshipSplit\Api\ShippingMethodInterface[] An array of shipping methods.
     */
    public function estimateByAddressId($cartId, $addressId)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);

        // no methods applicable for empty carts or carts with virtual products
        if ($quote->isVirtual() || 0 == $quote->getItemsCount()) {
            return [];
        }
        $address = $this->addressRepository->getById($addressId);

        return $this->getEstimatedRates2(
            $quote,
            $address->getCountryId(),
            $address->getPostcode(),
            $address->getRegionId(),
            $address->getRegion(),
            $address->getCity()
        );
    }

    public function estimateByExtendedAddress(
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);

        // no methods applicable for empty carts or carts with virtual products
        if ($quote->isVirtual() || 0 == $quote->getItemsCount()) {
            return [];
        }

        return $this->getEstimatedRates2(
            $quote,
            $address->getCountryId(),
            $address->getPostcode(),
            $address->getRegionId(),
            $address->getRegion(),
            $address->getCity()
        );
    }
}