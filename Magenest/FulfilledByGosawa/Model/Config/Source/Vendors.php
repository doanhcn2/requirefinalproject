<?php
namespace Magenest\FulfilledByGosawa\Model\Config\Source;

use Magento\Framework\App\ObjectManager;

class Vendors implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [];
        $vendors = ObjectManager::getInstance()->get(\Unirgy\Dropship\Helper\Data::class)->src()->setPath('vendors')->toOptionHash(true);
        foreach ($vendors as $vendorId => $vendorName) {
            if (is_array($vendorName)) {
                $options[] = $vendorName;
            } else {
                $options[] = [
                    'value' => $vendorId,
                    'label' => $vendorName
                ];
            }
        }
        return $options;
    }
}
