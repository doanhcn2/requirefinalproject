<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Helper\DropshipMultiPrice;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DB\Select;
use Magento\Framework\DataObject;
use Magento\Framework\Profiler;
use Magento\Framework\Registry;
use Magento\Framework\View\Layout;
use Unirgy\DropshipMulti\Helper\Data as DropshipMultiHelperData;
use Unirgy\Dropship\Helper\Catalog;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Helper\Item;

class Data extends \Unirgy\DropshipMultiPrice\Helper\Data
{
    public function getMultiPriceDataJson($prodBlock)
    {
        $product        = $prodBlock->getProduct();
        $product        = !$product ? $this->_registry->registry('current_product') : $product;
        $product        = !$product ? $this->_registry->registry('product') : $product;
        $requestedPath  = $product->getRequestPath();
        $result         = [];
        $udmHlp         = $this->_multiHlp;
        $simpleProducts = [];
        if ($product->getTypeId() == 'configurable') {
            $simpleProducts = $product->getTypeInstance(true)->getUsedProducts($product);
        }
        array_unshift($simpleProducts, $product);
        $udmHlp->attachMultivendorData($simpleProducts, true);
        $minSimpleProductPrice = PHP_INT_MAX;
        $minSimpleProduct      = $product;
        foreach ($simpleProducts as $simpleProduct) {
            if ($simpleProduct->getId() == $product->getId()) continue;
            if ($simpleProduct->getUdmultiBestPrice() < $minSimpleProductPrice) {
                $minSimpleProduct      = $simpleProduct;
                $minSimpleProductPrice = $simpleProduct->getUdmultiBestPrice();
            }
        }
        foreach ($simpleProducts as $simpleProduct) {
            $mvData       = $simpleProduct->getMultiVendorData();
            $priceProduct = $simpleProduct;
            if ($simpleProduct->getId() == $product->getId()) {
                $priceProduct = $minSimpleProduct;
            }
            $cfgMvData = $product->getMultiVendorData();
            if (!empty($mvData) && is_array($mvData) && !empty($cfgMvData) && is_array($cfgMvData)) {
                foreach ($mvData as &$_v) {
                    $_foundCfg = false;
                    foreach ($cfgMvData as $_vCfg) {
                        if ($_vCfg['vendor_id'] == $_v['vendor_id']) {
                            $_foundCfg             = true;
                            $_v['__price_product'] = $priceProduct;
                            $_v['__price_data']    = $_v;
                        }
                    }
                    if (!$_foundCfg) {
                        $_v['__price_product'] = $priceProduct;
                        $_v['__price_data']    = $_v;
                    }
                }
                unset($_v);
                $simpleProduct->setMultiVendorData($mvData);
            }
            $_result = $this->prepareMultiVendorHtmlData($prodBlock, $simpleProduct);
            if ($_result) {
                foreach ($_result['mvData'] as &$__mvd) {
                    unset($__mvd['__price_product']);
                    unset($__mvd['__price_data']);
                    $__mvd['vendor_product_link'] = @$__mvd['vendor_base_url'] . $requestedPath;
                }
                unset($__mvd);
                if ($_result['bestOffer']) {
                    $_result['bestOffer']['main_product_id'] = $product->getId();
                }
                $result[$simpleProduct->getId()] = $_result;
            }
        }

        return $this->_hlp->jsonEncode($result);
    }

    public function prepareMultiVendorHtmlData($prodBlock, $product)
    {
        Profiler::start('udmulti_prepareMultiVendorHtmlData');
        $udHlp  = $this->_hlp;
        $mpHlp  = $this;
        $udmHlp = $this->_multiHlp;
        if (($isMicro = $udHlp->isModuleActive('Unirgy_DropshipMicrosite'))) {
            $msHlp = $this->_hlp->getObj('Unirgy\DropshipMicrosite\Helper\Data');
        }
        $mvData  = $product->getMultiVendorData();
        $mvData  = $mpHlp->getSortedVendors($mvData, $product);
        $gmpData = $mpHlp->getGroupedMultipriceData($product);

        $bestOfferPrice = PHP_INT_MAX;
        $bestOffer      = null;
        foreach ($mvData as $__mvId => $mv) {
            $mv['state']                = $this->getExtState($mv['state'], 'code', true);
            $mv['canonic_state']        = $this->getExtCanonicState($mv['state'], 'code', true);
            $_mv                        = [];
            $v                          = $udHlp->getVendor($mv['vendor_id']);
            $_mv['shipping_price_html'] = $this->getShippingPrice($product, $mv, 1);
            $_mv['freeshipping']        = (bool)$mv['freeshipping'];
            $_mv['is_in_stock']         = (bool)$udmHlp->isSalableByVendorData($product, $mv['vendor_id'], $mv);
            $_mv['is_certified']        = (bool)$v->getIsCertified();
            $_mv['is_featured']         = (bool)$v->getIsFeatured();
            $_mv['is_pro']              = (bool)$v->getIsProAccount();
            $_mv['vendor_name']         = $v->getVendorName();
            $_mv['review_html']         = '';
            $_mv['vendor_base_url']     = '';
            $_mv['offer_note']          = $mv['offer_note'];
            $_mv['vendor_product_link'] = ObjectManager::getInstance()->create('Unirgy\DropshipMicrosite\Helper\Data')->getVendorUrl($v, $product);
            $_mv['vendor_logo']         = $v->getLogo() ? $this->_hlp->getResizedVendorLogoUrl($v, 80, 65) : '';
            if ($this->_hlp->isModuleActive('Unirgy_DropshipVendorRatings')) {
                $_mv['review_html'] = ObjectManager::getInstance()->create('Magenest\FixUnirgy\Helper\Data')->getRatingSummaryHtml($product->getEntityId(), $v->getId());
            }
            if ($isMicro && $msHlp->isAllowedAction('microsite', $v)) {
                $_mv['vendor_base_url'] = $msHlp->getVendorBaseUrl($v);
            }
            $_mv['is_allowed_microsite'] = $isMicro && $msHlp->isAllowedAction('microsite', $v);
            $priceProduct                = isset($mv['__price_product'])
                ? $mv['__price_product'] : $product;
            $priceData                   = isset($mv['__price_data'])
                ? $mv['__price_data'] : $mv;

            $_mv['price_product_id'] = $priceProduct->getId();

            $origPrice = $priceProduct->getPrice();
            $mpHlp->useVendorPrice($priceProduct, $priceData);

            $priceProduct->setFinalPrice(null);
            $priceProduct->reloadPriceInfo();

            $idSuffix = sprintf('_udmp_%d_%d', $priceProduct->getId(), $mv['vendor_id']);

            $priceBlockId = 'udmp.price.render-' . $priceProduct->getId() . '-' . $v->getId();
            $priceBlock   = $prodBlock->getLayout()->getBlock($priceBlockId);
            if (!$priceBlock) {
                $priceBlock = $prodBlock->getLayout()->createBlock('Magento\Catalog\Pricing\Render', $priceBlockId,
                    ['data' => [
                        'price_render'    => 'product.price.render.default',
                        'price_type_code' => 'final_price',
                        'zone'            => 'item_view',
                        'price_id_suffix' => $idSuffix,
                    ]]
                );
            }
            $prodBlock->setProductItem($priceProduct);
            $prodBlock->setChild($priceBlockId, $priceBlock);
            $_mv['price_html'] = $priceBlock->toHtml();

            $tpPriceBlockId = 'udmp.price.tier.render-' . $priceProduct->getId() . '-' . $v->getId();
            $tpPriceBlock   = $prodBlock->getLayout()->getBlock($tpPriceBlockId);
            if (!$tpPriceBlock) {
                $tpPriceBlock = $prodBlock->getLayout()->createBlock('Magento\Catalog\Pricing\Render', $tpPriceBlockId,
                    ['data' => [
                        'price_render'    => 'product.price.render.default',
                        'price_type_code' => 'tier_price',
                        'zone'            => 'item_view',
                        'price_id_suffix' => $idSuffix,
                    ]]
                );
            }
            $_mv['tier_price_html'] = $tpPriceBlock->toHtml();

            $_mv['idSuffix'] = $idSuffix;

            $isBetterOffer = false;
            if ($priceProduct->getFinalPrice() < $bestOfferPrice) {
                $isBetterOffer  = true;
                $bestOfferPrice = $priceProduct->getFinalPrice();
            }

            $mpHlp->revertVendorPrice($priceProduct);
            $priceProduct->setPrice($origPrice);
            $product->setFinalPrice(null);

            $_mv['state_label'] = $gmpData['vendorStates'][$mv['state']]['html_label'];
            $mv                 = $_mv + $mv;
            if ($isBetterOffer) {
                $bestOffer = [
                    'price_product_id' => $mv['price_product_id'],
                    'vendor_id'        => $mv['vendor_id'],
                ];
            }
            $mvData[$__mvId] = $mv;
        }
        if (empty($mvData)) return false;
        $_result = [
            'product_id'              => $product->getId(),
            'grouped_multiprice_data' => $gmpData,
            'mvData'                  => $mvData,
            'bestOffer'               => $bestOffer
        ];
        Profiler::stop('udmulti_prepareMultiVendorHtmlData');

        return $_result;
    }
}