<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 21/05/2018
 * Time: 10:34
 */

namespace Magenest\FixUnirgy\Helper\CheckUnique;

use Unirgy\Dropship\Model\Source;
use Unirgy\Dropship\Model\Vendor\ProductFactory as UnirgyProductFactory;

/**
 * Class Data
 * @package Magenest\FixUnirgy\Helper\CheckUnique
 */
class Data extends \Unirgy\DropshipMicrositePro\Helper\Data
{

    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Unirgy\DropshipMicrosite\Model\RegistrationFactory
     */
    protected $_registrationFactory;

    /**
     * @var \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * Data constructor.
     *
     * @param \Unirgy\Dropship\Model\EmailTransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\View\DesignInterface $viewDesign
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Unirgy\Dropship\Helper\Data $helperData
     * @param \Magento\Email\Model\TemplateFactory $modelTemplateFactory
     * @param \Unirgy\DropshipMicrositePro\Helper\ProtectedCode $micrositeProHelperProtected
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Unirgy\DropshipMicrosite\Model\RegistrationFactory $registrationFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory
     */
    public function __construct(
        \Unirgy\Dropship\Model\EmailTransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\View\DesignInterface $viewDesign,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\Dropship\Helper\Data $helperData,
        \Magento\Email\Model\TemplateFactory $modelTemplateFactory,
        \Unirgy\DropshipMicrositePro\Helper\ProtectedCode $micrositeProHelperProtected,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Unirgy\DropshipMicrosite\Model\RegistrationFactory $registrationFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        UnirgyProductFactory $unirgyProductFactory
    ) {
        parent::__construct($transportBuilder, $inlineTranslation, $context,
            $viewDesign, $storeManager, $helperData, $modelTemplateFactory,
            $micrositeProHelperProtected);
        $this->_vendorFactory        = $vendorFactory;
        $this->_registrationFactory  = $registrationFactory;
        $this->_locationFactory      = $locationFactory;
        $this->_unirgyProductFactory = $unirgyProductFactory;
    }

    /**
     * @param $vendor_name
     *
     * @param $vendorEmail
     * @return bool
     */
    public function checkVendorNameUniqueGroupon($vendor_name, $vendorEmail)
    {
        try {
            if (!empty($vendor_name)) {
                if ($this->checkBankInfoUpdateGroupon('vendor_name', $vendor_name, $vendorEmail)) {
                    return true;
                }
            }
        } catch (\Exception $exception) {
        }

        return parent::checkVendorNameUnique($vendor_name);
    }

    /**
     * if vendor finish onboarding, the value of Bank Iban is exist. If not, vendor has no Bank IBAN data
     *
     * @param $field
     * @param $value
     *
     * @param $email
     * @return bool
     */
    public function checkBankInfoUpdateGroupon($field, $value, $email)
    {
        $vendorCollection       = $this->_vendorFactory->create()
            ->getCollection()
            ->addFieldToFilter($field, $value)
            ->addFieldToFilter('email', ['neq' => $email]);
        $registrationCollection = $this->_registrationFactory->create()
            ->getCollection()
            ->addFieldToFilter($field, $value)
            ->addFieldToFilter('email', ['neq' => $email]);
        if ($vendorCollection->getSize() != 0 || $registrationCollection->getSize() != 0) {
            $vendorModel = $vendorCollection->getSize() == 0 ? $registrationCollection->getFirstItem() : $vendorCollection->getFirstItem();

            return $vendorModel->getBankIban() == '';
        }

        return false;
    }

    /**
     * @param $vendor_name
     *
     * @return bool
     */
    public function checkVendorNameUnique($vendor_name)
    {
        try {
            if (!empty($vendor_name)) {
                if ($this->checkBankInfoUpdate('vendor_name', $vendor_name)) {
                    return true;
                }
            }
        } catch (\Exception $exception) {
        }

        return parent::checkVendorNameUnique($vendor_name);
    }

    /**
     * @param $email
     *
     * @return bool
     */
    public function checkEmailUnique($email)
    {
        try {
            if (!empty($email)) {
                if ($this->checkBankInfoUpdate('email', $email)) {
                    return true;
                }
            }
        } catch (\Exception $e) {

        }

        return parent::checkEmailUnique($email); // TODO: Change the autogenerated stub
    }

    /**
     * @param $urlkey
     *
     * @return bool
     */
    public function checkUrlkeyUnique($urlkey)
    {
        try {
            if (!empty($email)) {
                if ($this->checkBankInfoUpdate('url_key', $urlkey)) {
                    return true;
                }
            }
        } catch (\Exception $e) {

        }

        return parent::checkUrlkeyUnique($urlkey); // TODO: Change the autogenerated stub
    }

    /**
     * if vendor finish onboarding, the value of Bank Iban is exist. If not, vendor has no Bank IBAN data
     *
     * @param $field
     * @param $value
     *
     * @return bool
     */
    public function checkBankInfoUpdate($field, $value)
    {
        $vendorCollection       = $this->_vendorFactory->create()
            ->getCollection()
            ->addFieldToFilter($field, $value);
        $registrationCollection = $this->_registrationFactory->create()
            ->getCollection()
            ->addFieldToFilter($field, $value);
        if ($vendorCollection->getSize() != 0 || $registrationCollection->getSize() != 0) {
            $vendorModel = $vendorCollection->getSize() == 0 ? $registrationCollection->getFirstItem() : $vendorCollection->getFirstItem();

            return $vendorModel->getBankIban() == '' || $vendorModel->getBankIban() === null;
        }

        return false;
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function getVendorByEmail($email)
    {
        $vendorCollection       = $this->_vendorFactory->create()
            ->getCollection()
            ->addFieldToFilter('email', $email)
            ->addFieldToFilter('status', Source::VENDOR_STATUS_INACTIVE);
        $registrationCollection = $this->_registrationFactory->create()
            ->getCollection()
            ->addFieldToFilter('email', $email);
        $model                  = $vendorCollection->getSize() != 0 ? $vendorCollection->getFirstItem() : $registrationCollection->getFirstItem();

        return $model;
    }

    /**
     * @param $vendor
     *
     * @return bool
     */
    public function isProductCreated($vendor)
    {
        if ($vendor instanceOf \Unirgy\DropshipMicrosite\Model\Registration) return false;
        if ($vendor->getId()) {
            $collection = $this->_unirgyProductFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $vendor->getId());

            return $collection->getSize() > 0;
        }

        return false;
    }

    /**
     * @param $vendor
     *
     * @return bool
     */
    public function isLocationCreated($vendor)
    {
        if ($vendor instanceOf \Unirgy\DropshipMicrosite\Model\Registration) return false;
        if ($vendor->getId()) {
            $collection = $this->_locationFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $vendor->getId());

            return $collection->getSize() > 0;
        }

        return false;
    }

    /**
     * @param $vendor
     *
     * @return bool
     */
    public function isAddressCreated($vendor)
    {
        if ($vendor instanceOf \Unirgy\DropshipMicrosite\Model\Registration) return false;

        return !!$vendor->getCity();
    }
}
