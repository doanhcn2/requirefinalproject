<?php

namespace Magenest\FixUnirgy\Helper\Review;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Review\Model\ReviewFactory;
use Magento\Sales\Model\Order\ItemFactory;
use Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Shipment\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Config\ScopeConfigInterface;


class Data extends AbstractHelper
{
    const ENTITY_TYPE_ID = 7;
    /**
     * @var ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    protected $itemFactory;

    protected $shipmentFactory;

    protected $_resource;

    protected $scopeConfig;

    public function __construct(
        Context $context,
        ReviewFactory $modelReviewFactory,
        StoreManagerInterface $modelStoreManagerInterface,
        ItemFactory $itemFactory,
        CollectionFactory $shipmentFactory,
        ResourceConnection $resource,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_reviewFactory = $modelReviewFactory;
        $this->_storeManager = $modelStoreManagerInterface;
        $this->itemFactory = $itemFactory;
        $this->shipmentFactory = $shipmentFactory;
        $this->_resource = $resource;
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    public function getPendingVirtualItemReview()
    {
        $itemCollection = $this->itemFactory->create()->getCollection();
        $itemCollection = $this->addPendingFilter($itemCollection);
        return $itemCollection;
    }

    public function addPendingFilter($orderCollection)
    {
        $orderCollection = $this->addOrderFilter($orderCollection);
        $orderCollection = $this->addReviewFilter($orderCollection);
        $this->addGrouponFilter($orderCollection);
        return $this->addTicketFilter($orderCollection);
    }


    public function addOrderFilter($orderCollection)
    {
        $saleOrder = $this->_resource->getTableName('sales_order');
        $orderCollection->getSelect()
            ->joinLeft(
                ['so' => $saleOrder],
                "main_table.order_id = so.entity_id",
                []
            );
        return $orderCollection;
    }

    public function addReviewFilter($orderCollection)
    {
        $review = $this->_resource->getTableName('review');
        $orderCollection->getSelect()
            ->joinLeft(
                ['rv' => $review],
                "main_table.item_id = rv.rel_entity_pk_value AND rv.entity_id = ".$this::ENTITY_TYPE_ID,
                []
            )->where('rv.rel_entity_pk_value is null');


        return $orderCollection;
    }

    public function addGrouponFilter($orderCollection)
    {
        $groupon = $this->_resource->getTableName('magenest_groupon_groupon');
        $orderCollection->getSelect()
            ->joinLeft(
                ['mgg' => $groupon],
                "main_table.order_id = mgg.order_id AND main_table.product_id = mgg.product_id",
                ['mgg_status' => 'mgg.status', 'mgg_redeemed_at' => 'mgg.redeemed_at']
            );

        return $orderCollection;
    }

    public function addTicketFilter($orderCollection)
    {
        $ticket = $this->_resource->getTableName('magenest_ticket_ticket');
        $day = (int)$this->scopeConfig->getValue('udropship/vendor_rating/virtual_rating');
        if ($day == 0) {
            $day = 7;
        }
        $t = new \DateTime();
        $t->sub(new \DateInterval('P'.$day.'D'));
        $orderCollection->getSelect()
            ->joinLeft(
                ['mtt' => $ticket],
                "main_table.order_id = mtt.order_id AND main_table.product_id = mtt.product_id",
                ['mtt_status' => 'mtt.status', 'mtt_redeemed_at' => 'mtt.redeemed_at']
            )->where('(
                mgg.status = 2 AND 
                (
                    product_type = "coupon" or
                    product_type = "ticket"    
                ) AND
                mgg.redeemed_at > "'.$t->format('Y-m-d h:m:s').'" 
            ) 
            OR (
                    mtt.status = 2 AND 
                    product_type = "ticket" AND 
                    mtt.redeemed_at > "'.$t->format('Y-m-d h:m:s').'" 
            ) 
            OR (
                    mgg.status is NULL AND 
                    mtt.status is NULL AND 
                    product_type != "coupon" AND 
                    product_type != "ticket"
            )
            OR (
                    (mgg.customer_status = 2 AND product_type = "coupon") OR 
                    (product_type = "ticket" AND mtt.customer_status = 2)
            )');

        return $orderCollection;
    }

    public function getListAllRatingCollection($vendorId = null)
    {
        $model = $this->_reviewFactory->create();
        /**
         * @var $col \Magento\Review\Model\ResourceModel\Review\Collection
         */
        $col = $model->getCollection();
        $col->addFieldToFilter('main_table.entity_id', ['in' => [7, 10]]);

        $col->getSelect()->joinLeft(
            ['ss' => $col->getTable('sales_shipment')],
            "main_table.rel_entity_pk_value = ss.entity_id and main_table.entity_id = 10",
            ['sales_shipment_id' => 'ss.entity_id', 'ss.increment_id']
        );
        $col->getSelect()->joinLeft(
            ['udv' => $col->getTable('udropship_vendor')],
            "main_table.entity_pk_value = udv.vendor_id",
            ['udropship_vendor' => 'udv.vendor_id', 'udv.vendor_name', 'vendor_email' => 'udv.email']
        );
        $col->getSelect()->joinLeft(
            ['ssi' => $col->getTable('sales_shipment_item')],
            "ss.entity_id = ssi.parent_id",
            ['ssi.order_item_id', 'ssi.parent_id']
        );
        $col->getSelect()->joinLeft(
            ['soi' => $col->getTable('sales_order_item')],
            "(ssi.order_item_id = soi.item_id) OR (main_table.rel_entity_pk_value = soi.item_id AND main_table.entity_id = 7)",
            ['product_name_list' => 'soi.name', 'product_sku_list' => 'soi.sku']
        );
        if ($vendorId) {
            $col->addFieldToFilter('udv.vendor_id', $vendorId);
            $col->setFlag('AddRateVotes', true)
                ->setFlag('AddAddressData', true);
        }
        $col->getSelect()->group(['main_table.review_id']);

        return $col;
    }
}