<?php

namespace Magenest\FixUnirgy\Helper;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipVendorProduct\Helper\Data as UnirgyHelper;
use Unirgy\DropshipVendorProduct\Model\ProductStatus;

/**
 * Class Data
 * @package Magenest\FixUnirgy\Helper
 */
class Data extends UnirgyHelper
{
    protected $_productToEdit;

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getProductToEdit()
    {
        if (null === $this->_productToEdit) {

            $this->_productToEdit = $this->initProductEdit([
                'id' => $this->_request->getParam('id'),
                'vendor' => $this->getVendor()
            ]);

            $this->_coreRegistry->register('current_product', $this->_productToEdit);
            $this->_coreRegistry->register('product', $this->_productToEdit);
        }
        return $this->_productToEdit;
    }

    /**
     * @param $config
     * @return mixed
     * @throws \Exception
     */
    public function initProductEdit($config)
    {
        $r = $this->_request;
        if (array_key_exists('data', $config) && !array_key_exists('category_ids', $config['data']))
            $config['data']['category_ids'] = [];
        $udSess = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');

        $pId = array_key_exists('id', $config) ? $config['id'] : $r->getParam('id');
        $prTpl = !empty($config['template_id']) ? $config['template_id'] : null;
        $typeId = array_key_exists('type_id', $config) ? $config['type_id'] : $r->getParam('type_id');
        $setId = array_key_exists('set_id', $config) ? $config['set_id'] : $r->getParam('set_id');
        $skipCheck = !empty($config['skip_check']);
        $skipPrepare = !empty($config['skip_prepare']);
        $vendor = !empty($config['vendor']) ? $config['vendor'] : $udSess->getVendor();
        $productData = !empty($config['data']) ? $config['data'] : [];
        $productPostData = !empty($config['data']['udmulti']) ? $config['data']['udmulti'] : [];

        list($_setId) = explode('-', $setId);

        $vendor = $this->_hlp->getVendor($vendor);

        if (!$vendor->getId()) {
            throw new \Exception('Vendor not specified');
        }

        $product = $this->_uProductFactory->create()->setStoreId(0);
        if ($pId) {
            if (!$skipCheck) $this->checkProduct($pId);
            $product->load($pId);

            //fix canot show vendor data in vendor page

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productUnirgy = $objectManager->create('\Unirgy\Dropship\Model\ResourceModel\Vendor\Product\Collection')->addProductFilter([$pId]);
            if ($productUnirgy->count() > 0) {
                $vendorData = $productUnirgy->getFirstItem()->getData();
                foreach ($vendorData as $key => $value) {
                    $productData['udmulti'][$key] = $value;
                }
                foreach ($productPostData as $key => $postDatum) {
                    $productData['udmulti'][$key] = $postDatum;
                }
            }
        }
        if (!$product->getId()) {
            if (null === $prTpl) {
                $prTpl = $this->getTplProdBySetId($vendor, $setId);
            } else {
                $prTpl = $this->_uProductFactory->create()->load($prTpl);
            }
            if ($setId) {
                $prTpl->setUdprodAttributeSetKey($setId);
                $prTpl->setAttributeSetId($_setId);
            }
            $stockItem = $this->_hlp->getStockItem($product);
            if (!$stockItem) {
                $stockItem = $this->_stockItemFactory->create();
            }
            $prTpl->setStockItem($stockItem);
            $tplStockData = $this->_hlp->getStockItem($prTpl)->getData();
            unset($tplStockData['item_id']);
            unset($tplStockData['product_id']);
            if (empty($productData['stock_data'])) {
                $productData['stock_data'] = [];
            }
            $productData['is_in_stock'] = !isset($productData['is_in_stock']) ? 1 : $productData['is_in_stock'];
            $productData['stock_data'] = array_merge($tplStockData, $productData['stock_data']);
            if (!isset($productData['stock_data']['use_config_manage_stock'])) {
                $productData['stock_data']['use_config_manage_stock'] = 1;
            }
            if (isset($productData['stock_data']['qty']) && (float)$productData['stock_data']['qty'] > self::MAX_QTY_VALUE) {
                $productData['stock_data']['qty'] = self::MAX_QTY_VALUE;
            }
            $this->prepareTplProd($prTpl);
            $product->setData($prTpl->getData());
            $product->setData('__tpl_product', $prTpl);
            if (!$product->getAttributeSetId()) {
                $product->setAttributeSetId(
                    $product->getResource()->getEntityType()->getDefaultAttributeSetId()
                );
            }
            if ($typeId) {
                $product->setTypeId($typeId);
            } elseif (!$product->getTypeId()) {
                $product->setTypeId('simple');
            }
            if (!$product->hasData('status')) {
                $product->setData('status', ProductStatus::STATUS_PENDING);
            }
            if (!$product->hasData('visibility')) {
                $product->setData('visibility', Visibility::VISIBILITY_BOTH);
            }
        }
        if (isset($productData['stock_data'])) {
            $qtyAndStockStatusFields = ['qty', 'is_in_stock'];
            foreach ($qtyAndStockStatusFields as $__qssf) {
                if (array_key_exists($__qssf, $productData['stock_data'])) {
                    $productData['quantity_and_stock_status'][$__qssf] = $productData['stock_data'][$__qssf];
                }
            }
        }
        $product->setData('_edit_in_vendor', true);
        $product->setData('_edit_mode', true);


        if (isset($productData['udmulti']['freeshipping'])) {
            if ($productData['udmulti']['freeshipping'] === "0") {
                $productData['udmulti']['freeshipping'] = 1;
            }
        } else {
            $productData['udmulti']['freeshipping'] = 0;
        }
        if (is_array($productData)) {
            if (!$skipPrepare) $this->prepareProductPostData($product, $productData);
            $udmulti = @$productData['udmulti'];
            if (!isset($productData['price']) && is_array($udmulti) && isset($udmulti['vendor_price'])) {
                $productData['price'] = $udmulti['vendor_price'];
            }
            $product->addData($productData);
        }
        if (!$product->getId()) {
            $product->setUdropshipVendor($vendor->getId());
        }
        $product->setStoreId(0);
        $product->getAttributes();
        return $product;
    }


    public function processQuickCreate($prod, $isNew, $parentType = null, $information = null)
    {
        if ('configurable' != $prod->getTypeId()) return $this;

        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        $hlp = $this->_hlp;
        $prHlp = $this;
        $newPids = [];
        if ('configurable' == $prod->getTypeId()) {
            $cfgFirstAttrs = $this->getCfgFirstAttributes($prod, $isNew);
            $cfgFirstAttr = $this->getCfgFirstAttribute($prod, $isNew);
            $cfgFirstAttrId = $cfgFirstAttr->getId();
            $cfgFirstAttrCode = $cfgFirstAttr->getAttributeCode();
            $existingPids = $prod->getTypeInstance(true)->getUsedProductIds($prod);
            $quickCreate = $prod->getData('_cfg_attribute/quick_create');
            $cfgPrices = $prod->getData('__cfg_prices');
            $existingQC = $this->getEditSimpleProductData($prod);
            $observerData = [];
            if (is_array($quickCreate)) {
                $allExistingQC = $this->getEditSimpleProductData($prod, true);
                foreach ($quickCreate as $_qcKey => $qc) {
                    $cfgFirstAttrKey = '';
                    foreach ($cfgFirstAttrs as $__ca) {
                        $__id = $__ca->getAttributeId();
                        $__code = $__ca->getAttributeCode();
                        $cfgFirstAttrKey .= $__id . '-' . $qc[$__code] . '-';
                    }
                    $cfgFirstAttrKey = rtrim($cfgFirstAttrKey, '-');
                    if ($_qcKey == '$ROW') continue;
                    $pId = @$qc['simple_id'];
                    $qcMP = (array)@$qc['udmulti'];
                    $qcSD = (array)@$qc['stock_data'];
                    unset($qc['udmulti']);
                    unset($qc['stock_data']);
                    $qc['is_existing'] = @$qc['is_existing'] || $pId;
                    if (!$pId && !empty($qc['sku'])) {
                        $pId = $this->_helperCatalog->getPidBySku($qc['sku']);
                    }
                    if (!$pId && !empty($qcMP['vendor_sku'])) {
                        $pId = $this->_helperCatalog->getPidByVendorSku($qcMP['vendor_sku'], $v->getId());
                    }
                    if (!empty($qc['is_existing']) && !$pId) continue;
                    $superAttrKey = [];
                    foreach ($prod->getTypeInstance(true)->getUsedProductAttributes($prod) as $cfgAttr) {
                        $superAttrKey[] = $cfgAttr->getId() . '=' . @$qc[$cfgAttr->getAttributeCode()];
                    }
                    $superAttrKey = implode('-', $superAttrKey);
                    foreach ($allExistingQC as $eqcPid => $eqcData) {
                        if ($eqcData['super_attr_key'] == $superAttrKey) {
                            $pId = $eqcPid;
                            $qc['is_existing'] = true;
                            break;
                        }
                    }
                    if ($pId) {
                        $newPids[] = $pId;
                    }
                    if (!empty($qc['is_existing']) && $pId && isset($existingQC[$pId])) {
                        $_eqc = $existingQC[$pId];
                        $_eqcMP = (array)@$_eqc['udmulti'];
                        $_eqcSD = (array)@$_eqc['stock_data'];
                        unset($_eqc['udmulti']);
                        unset($_eqc['stock_data']);
                        $qcNoChanges = true;
                        if ($information['groupon']) {
                            $qcNoChanges = false;
                        } else {
                            foreach ($qc as $_k => $_v) {
                                if ($_v != @$_eqc[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                            foreach ($qcMP as $_k => $_v) {
                                if ($_v != @$_eqcMP[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                            foreach ($qcSD as $_k => $_v) {
                                if ($_v != @$_eqcSD[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                        }
                        if ($qcNoChanges && !$prod->getData('udprod_cfg_media_changed/' . $cfgFirstAttrKey)) {
                            continue;
                        }
                    }
                    $qcProdData = [];
                    if (!$this->_hlp->isUdmultiActive()) {
                        $qcSD['is_in_stock'] = !isset($qcSD['is_in_stock']) ? 1 : $qcSD['is_in_stock'];
                        $qcProdData['stock_data'] = $qcSD;
                    }
                    try {
                        if ($pId) {
                            $qcProdData['options'] = [];
                            $qcProdData['has_options'] = false;
                            $qcProdData['required_options'] = false;

                            //Magenest
                            if ($parentType == "Groupon") {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => $pId,
                                    'type_id' => 'coupon',
                                    'data' => $qcProdData,
                                    'skip_check' => true
                                ]);
                            } else {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => $pId,
                                    'type_id' => 'simple',
                                    'data' => $qcProdData,
                                    'skip_check' => true
                                ]);
                            }
                            $qcProd->uclearOptions();
                            $qcProd->setProductOptions([]);
                            $qcProd->setCanSaveCustomOptions(false);
                        } else {
                            $qcProdData['website_ids'] = $prod->getWebsiteIds();
                            $qcProdData['category_ids'] = $prod->getCategoryIds();
                            $qcProdData['options'] = [];
                            $qcProdData['has_options'] = false;
                            $qcProdData['required_options'] = false;
                            if ($parentType == "Groupon") {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => false,
                                    'type_id' => 'coupon',
                                    'template_id' => $prod->getId(),
                                    'data' => $qcProdData,
                                ]);
                            } else {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => false,
                                    'type_id' => 'simple',
                                    'template_id' => $prod->getId(),
                                    'data' => $qcProdData,
                                ]);
                            }

                            $qcProd->uclearOptions();
                            $qcProd->setProductOptions([]);
                            $qcProd->setCanSaveCustomOptions(false);
                        }
                        if ($prHlp->isMyProduct($qcProd)) {
                            foreach ($this->getQuickCreateAllowedAttributes() as $_k) {
                                if (isset($qc[$_k])) {
                                    $qcProd->setData($_k, $qc[$_k]);
                                }
                            }
                        }
                        $autogenerateOptions = [];
                        if (!$this->scopeConfig->isSetFlag('udprod/general/disable_name_check', ScopeInterface::SCOPE_STORE)) {
                            $ufName = $prod->formatUrlKey(@$qc['name']);
                        } else {
                            $ufName = @$qc['name'];
                        }
                        $ufName = trim($ufName);
                        foreach ($this->getConfigurableAttributes($prod, $isNew) as $attribute) {
                            if ($attribute->getAttributeCode() != 'name' || $ufName) {
                                $qcProd->setData($attribute->getAttributeCode(), @$qc[$attribute->getAttributeCode()]);
                            }
                            $value = $qcProd->getAttributeText($attribute->getAttributeCode());
                            $autogenerateOptions[] = $value;
                        }
                        if (!$pId) {
                            if (empty($qc['name']) || !$ufName || !empty($qc['name_auto'])) {
                                $autoName = $prod->getName() . '-' . implode('-', $autogenerateOptions);
                                if (!$this->scopeConfig->isSetFlag('udprod/general/disable_name_check', ScopeInterface::SCOPE_STORE)) {
                                    $autoName = $prod->formatUrlKey($autoName);
                                }
                                $qcProd->setName($autoName);
                            }
                            $qcProd->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
                        }

                        if ($this->_hlp->isUdmultiActive()) {
                            $qcMP['vendor_sku'] = trim(@$qcMP['vendor_sku']);
                            if (empty($qcMP['vendor_sku'])) {
                                $qcVsku = $qcMP['vendor_sku'] = $this->_helperCatalog->getVendorSkuByPid($prod->getId(), $v->getId()) . '-' . implode('-', $autogenerateOptions);
                                $qcVskuIdx = 0;
                                while ($this->_helperCatalog->getPidByVendorSku($qcVsku, $v->getId(), $pId)) {
                                    $qcVsku = $qcMP['vendor_sku'] . '-' . (++$qcVskuIdx);
                                }
                                $qcMP['vendor_sku'] = $qcVsku;
                            }
                            if ($this->scopeConfig->isSetFlag('udprod/general/unique_vendor_sku', ScopeInterface::SCOPE_STORE)) {
                                if (empty($qcMP['vendor_sku'])) {
                                    throw new \Exception('Vendor SKU is empty');
                                } elseif ($this->_helperCatalog->getPidByVendorSku($qcMP['vendor_sku'], $v->getId(), $pId)) {
                                    throw new \Exception(__('Vendor SKU "%1" is already used', $qcMP['vendor_sku']));
                                }
                            }
                        }

                        if (!$qcProd->getSku() && $this->scopeConfig->isSetFlag('udprod/general/auto_sku', ScopeInterface::SCOPE_STORE)) {

                            $__skuAuto = $__skuAuto1 = $prod->getSku() . '-' . implode('-', $autogenerateOptions);
//                            $__skuAutoIdx = 0;
//                            while ($this->_helperCatalog->getPidBySku($__skuAuto, $qcProd->getId())) {
//                                $__skuAuto = $__skuAuto1.'-'.(++$__skuAutoIdx);
//                            }
                            $optionId = $qcProd->getData('groupon_option');
                            $_skuAuto = $prod->getSku() . '-' . $optionId;
                            $qcProd->setSku($_skuAuto);
                        }

                        if ($this->_hlp->getObj('\Unirgy\DropshipVendorProduct\Model\Source')->isCfgUploadImagesSimple()) {
                            $this->processQcMediaChange($prod, $qcProd, $isNew);
                        }


                        $qcProd->setData('_allow_use_renamed_image', true);
                        $qcProd->setUdprodIsQcNew(!$qcProd->getId());
                        $qcProd->save();
                        try {
//                            $newSku = $prod->getSku().'-'.$qcProd->getId();
                            $newSku = $qcProd->getId();
                            $qcProd->setSku($newSku);
                            $qcProd->save();
                        } catch (\Exception $e) {
                            try {
                                $newSku = $prod->getSku() . '-' . $qcProd->getId();
                                $qcProd->setSku($newSku);
                                $qcProd->save();
                            } catch (\Exception $e) {
                                try {
                                    $newSku = $qcProd->getId() . '-' . time();
                                    $qcProd->setSku($newSku);
                                    $qcProd->save();
                                } catch (\Exception $e) {

                                }
                            }
                        }
                        $this->processAfterSave($qcProd);
                        if ($qcProd->getUdprodNeedToUnpublish()) {
                            $this->addUnpublishPids($prod, [$qcProd->getId()]);
                        }

                        if ($this->_hlp->isUdmultiActive()) {
                            $this->_hlp->rHlp()->insertIgnore(
                                'udropship_vendor_product',
                                ['vendor_id' => $v->getId(), 'product_id' => $qcProd->getId(), 'status' => $this->_hlp->getObj('Unirgy\DropshipMulti\Helper\Data')->getDefaultMvStatus()]
                            );
                            $udmultiUpdate = $qcMP;
                            $udmultiUpdate['isNewFlag'] = $isNew;

                            //Change sku
                            if ($qcProd->getTypeId() == 'coupon') {
                                $udmultiUpdate['vendor_sku'] = $qcProd->getSku();
                            }

                            $this->_hlp->processDateLocaleToInternal(
                                $udmultiUpdate,
                                ['special_from_date', 'special_to_date'],
                                $this->_hlp->getDefaultDateFormat()
                            );
                            $this->_hlp->getObj('Unirgy\DropshipMulti\Helper\Data')->saveThisVendorProductsPidKeys(
                                [$qcProd->getId() => $udmultiUpdate], $v
                            );
                        }
                    } catch (\Exception $e) {
                        $this->_hlp->getObj('\Magento\Framework\Message\ManagerInterface')->addError($e->getMessage());
                        continue;
                    }
                    $newPids[] = $qcProd->getId();

                    //Magenest
                    if ($qcProd->getTypeId() == 'coupon') {
                        $parentType = 'Groupon';
                    }
                    //Add data for event observer
                    if ($parentType === 'Groupon') {
                        $observerData[] = ["product_id" => $qcProd->getId(), "information" => $information, 'product' => $qcProd];
                    }
                }
            }
            $delSimplePids = array_diff($existingPids, $newPids);
            $addSimplePids = array_diff($newPids, $existingPids);

            foreach ($addSimplePids as $addSimplePid) {
                $this->_helperCatalog->linkCfgSimple($prod->getId(), $addSimplePid, true);
            }

            foreach ($observerData as $key => $observerDatum) {
                $this->_eventManager->dispatch('unirgy_save_coupon_product_after', $observerDatum);
            }

            if (!empty($addSimplePids)) {
                $this->_addCfgSimplesDescrData($prod, $isNew, $addSimplePids, 'udprod_cfg_simples_added');
            }
            if (!empty($delSimplePids)) {
                $this->_addCfgSimplesDescrData($prod, $isNew, $delSimplePids, 'udprod_cfg_simples_removed');
            }

            $delProd = $this->_modelProductFactory->create();
            foreach ($delSimplePids as $delSimplePid) {
                $this->_helperCatalog->unlinkCfgSimple($prod->getId(), $delSimplePid, true);
                $delProd->load($delSimplePid)->delete();
            }
            if (!empty($delSimplePids)) {
                $this->setNeedToUnpublish($prod, 'cfg_simple_removed');
            }
            if (!empty($addSimplePids)) {
                $this->setNeedToUnpublish($prod, 'cfg_simple_added');
            }
            $reindexPids = array_merge($newPids, $existingPids);
            $reindexPids[] = $prod->getId();
            $this->processCfgPriceChanges($prod, $reindexPids);
            $this->addReindexPids($prod, $reindexPids);
        }
    }

    public function processSaveOtherData($prod, $productId, $vendorId)
    {
        $data = $prod->getData();
        /** @var \Unirgy\Dropship\Model\Vendor\Product $vendorProduct */
        $vendorProduct = ObjectManager::getInstance()->create(\Unirgy\Dropship\Model\Vendor\Product::class)->getCollection()->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('product_id', $productId)->getFirstItem();
        if (@$data['type_id'] === \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
            $vendorProduct->setOfferNote(@$data['udmulti']['offer_note']);
            $vendorProduct->setHandlingTime(@$data['udmulti']['handling_time']);
            $vendorProduct->save();
        }
    }

    public function getRatingSummaryHtml($productId, $vendorId)
    {
        $ratingHelper = ObjectManager::getInstance()->create('Magenest\SellYours\Helper\RatingHelper');
        $ratingCollection = $ratingHelper->getReviewCollection($productId, false, $vendorId);
        $avg = $ratingHelper->getAvgRate($ratingCollection->getColumnValues('review_id'));
        $result = "";
        if ($avg) {
            $result .= '<div class="catalog-header-rating">
                                    <div class="product-reviews-summary short">
                                        <div class="rating-summary">';
            $result .= '<div class="rating-result" title="' . number_format(((float)$avg / 5) * 100, 2) . '%">
                                    <span style="width: ' . number_format(((float)$avg / 5) * 100, 2) . '%">
                                        <span>' . number_format(((float)$avg / 5) * 100, 2) . '%</span>
                                    </span>
                                </div>
                            </div>
                            <div class="reviews-actions">';
            $result .= '<span class="action view">(' . $ratingCollection->getSize() . ')</span>
                            </div>
                        </div>
                    </div>';

            return $result;
        }
        return $result;
    }

    public function processQuickCreates($prod, $isNew, $parentType = null, $information = null)
    {
        if ('configurable' != $prod->getTypeId()) return $this;

        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        $hlp = $this->_hlp;
        $prHlp = $this;
        $newPids = [];
        if ('configurable' == $prod->getTypeId()) {
            $cfgFirstAttrs = $this->getCfgFirstAttributes($prod, $isNew);
            $cfgFirstAttr = $this->getCfgFirstAttribute($prod, $isNew);
            $cfgFirstAttrId = $cfgFirstAttr->getId();
            $cfgFirstAttrCode = $cfgFirstAttr->getAttributeCode();
            $existingPids = $prod->getTypeInstance(true)->getUsedProductIds($prod);
            $quickCreate = @$information["product"]["_cfg_attribute"]["quick_create"];
            $cfgPrices = $prod->getData('__cfg_prices');
            $existingQC = $this->getEditSimpleProductData($prod);
            $observerData = [];
            if (is_array($quickCreate)) {
                $allExistingQC = $this->getEditSimpleProductData($prod, true);
                foreach ($quickCreate as $_qcKey => $qc) {
                    if ($_qcKey == '$ROW') continue;
                    if (@$qc['simple_id'] && $qc['simple_id'] !== "") continue;
                    $cfgFirstAttrKey = '';
                    foreach ($cfgFirstAttrs as $__ca) {
                        $__id = $__ca->getAttributeId();
                        $__code = $__ca->getAttributeCode();
                        $cfgFirstAttrKey .= $__id . '-' . $qc[$__code] . '-';
                    }
                    $cfgFirstAttrKey = rtrim($cfgFirstAttrKey, '-');
                    $pId = @$qc['simple_id'];
                    $qcMP = (array)@$qc['udmulti'];
                    $qcSD = (array)@$qc['stock_data'];
                    unset($qc['udmulti']);
                    unset($qc['stock_data']);
                    $qc['is_existing'] = @$qc['is_existing'] || $pId;
                    if (!$pId && !empty($qc['sku'])) {
                        $pId = $this->_helperCatalog->getPidBySku($qc['sku']);
                    }
                    if (!$pId && !empty($qcMP['vendor_sku'])) {
                        $pId = $this->_helperCatalog->getPidByVendorSku($qcMP['vendor_sku'], $v->getId());
                    }
                    if (!empty($qc['is_existing']) && !$pId) continue;
                    $superAttrKey = [];
                    foreach ($prod->getTypeInstance(true)->getUsedProductAttributes($prod) as $cfgAttr) {
                        $superAttrKey[] = $cfgAttr->getId() . '=' . @$qc[$cfgAttr->getAttributeCode()];
                    }
                    $superAttrKey = implode('-', $superAttrKey);
                    foreach ($allExistingQC as $eqcPid => $eqcData) {
                        if ($eqcData['super_attr_key'] == $superAttrKey) {
                            $pId = $eqcPid;
                            $qc['is_existing'] = true;
                            break;
                        }
                    }
                    if ($pId) {
                        $newPids[] = $pId;
                    }
                    if (!empty($qc['is_existing']) && $pId && isset($existingQC[$pId])) {
                        $_eqc = $existingQC[$pId];
                        $_eqcMP = (array)@$_eqc['udmulti'];
                        $_eqcSD = (array)@$_eqc['stock_data'];
                        unset($_eqc['udmulti']);
                        unset($_eqc['stock_data']);
                        $qcNoChanges = true;
                        if ($information['groupon']) {
                            $qcNoChanges = false;
                        } else {
                            foreach ($qc as $_k => $_v) {
                                if ($_v != @$_eqc[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                            foreach ($qcMP as $_k => $_v) {
                                if ($_v != @$_eqcMP[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                            foreach ($qcSD as $_k => $_v) {
                                if ($_v != @$_eqcSD[$_k]) {
                                    $qcNoChanges = false;
                                    break;
                                }
                            }
                        }
                        if ($qcNoChanges && !$prod->getData('udprod_cfg_media_changed/' . $cfgFirstAttrKey)) {
                            continue;
                        }
                    }
                    $qcProdData = [];
                    if (!$this->_hlp->isUdmultiActive()) {
                        $qcSD['is_in_stock'] = !isset($qcSD['is_in_stock']) ? 1 : $qcSD['is_in_stock'];
                        $qcProdData['stock_data'] = $qcSD;
                    }
                    try {
                        if ($pId) {
                            $qcProdData['options'] = [];
                            $qcProdData['has_options'] = false;
                            $qcProdData['required_options'] = false;

                            //Magenest
                            if ($parentType == "Groupon") {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => $pId,
                                    'type_id' => 'coupon',
                                    'data' => $qcProdData,
                                    'skip_check' => true
                                ]);
                            } else {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => $pId,
                                    'type_id' => 'simple',
                                    'data' => $qcProdData,
                                    'skip_check' => true
                                ]);
                            }
                            $qcProd->uclearOptions();
                            $qcProd->setProductOptions([]);
                            $qcProd->setCanSaveCustomOptions(false);
                        } else {
                            $qcProdData['website_ids'] = $prod->getWebsiteIds();
                            $qcProdData['category_ids'] = $prod->getCategoryIds();
                            $qcProdData['options'] = [];
                            $qcProdData['has_options'] = false;
                            $qcProdData['required_options'] = false;
                            if ($parentType == "Groupon") {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => false,
                                    'type_id' => 'coupon',
                                    'template_id' => $prod->getId(),
                                    'data' => $qcProdData,
                                ]);
                            } else {
                                $qcProd = $prHlp->initProductEdit([
                                    'id' => false,
                                    'type_id' => 'simple',
                                    'template_id' => $prod->getId(),
                                    'data' => $qcProdData,
                                ]);
                            }

                            $qcProd->uclearOptions();
                            $qcProd->setProductOptions([]);
                            $qcProd->setCanSaveCustomOptions(false);
                        }
                        if ($prHlp->isMyProduct($qcProd)) {
                            foreach ($this->getQuickCreateAllowedAttributes() as $_k) {
                                if (isset($qc[$_k])) {
                                    $qcProd->setData($_k, $qc[$_k]);
                                }
                            }
                        }
                        $autogenerateOptions = [];
                        if (!$this->scopeConfig->isSetFlag('udprod/general/disable_name_check', ScopeInterface::SCOPE_STORE)) {
                            $ufName = $prod->formatUrlKey(@$qc['name']);
                        } else {
                            $ufName = @$qc['name'];
                        }
                        $ufName = trim($ufName);
                        foreach ($this->getConfigurableAttributes($prod, $isNew) as $attribute) {
                            if ($attribute->getAttributeCode() != 'name' || $ufName) {
                                $qcProd->setData($attribute->getAttributeCode(), @$qc[$attribute->getAttributeCode()]);
                            }
                            $value = $qcProd->getAttributeText($attribute->getAttributeCode());
                            $autogenerateOptions[] = $value;
                        }
                        if (!$pId) {
                            if (empty($qc['name']) || !$ufName || !empty($qc['name_auto'])) {
                                $autoName = $prod->getName() . '-' . implode('-', $autogenerateOptions);
                                if (!$this->scopeConfig->isSetFlag('udprod/general/disable_name_check', ScopeInterface::SCOPE_STORE)) {
                                    $autoName = $prod->formatUrlKey($autoName);
                                }
                                $qcProd->setName($autoName);
                            }
                            $qcProd->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE);
                        }

                        if ($this->_hlp->isUdmultiActive()) {
                            $qcMP['vendor_sku'] = trim(@$qcMP['vendor_sku']);
                            if (empty($qcMP['vendor_sku'])) {
                                $qcVsku = $qcMP['vendor_sku'] = $this->_helperCatalog->getVendorSkuByPid($prod->getId(), $v->getId()) . '-' . implode('-', $autogenerateOptions);
                                $qcVskuIdx = 0;
                                while ($this->_helperCatalog->getPidByVendorSku($qcVsku, $v->getId(), $pId)) {
                                    $qcVsku = $qcMP['vendor_sku'] . '-' . (++$qcVskuIdx);
                                }
                                $qcMP['vendor_sku'] = $qcVsku;
                            }
                            if ($this->scopeConfig->isSetFlag('udprod/general/unique_vendor_sku', ScopeInterface::SCOPE_STORE)) {
                                if (empty($qcMP['vendor_sku'])) {
                                    throw new \Exception('Vendor SKU is empty');
                                } elseif ($this->_helperCatalog->getPidByVendorSku($qcMP['vendor_sku'], $v->getId(), $pId)) {
                                    throw new \Exception(__('Vendor SKU "%1" is already used', $qcMP['vendor_sku']));
                                }
                            }
                        }

                        if (!$qcProd->getSku() && $this->scopeConfig->isSetFlag('udprod/general/auto_sku', ScopeInterface::SCOPE_STORE)) {

                            $__skuAuto = $__skuAuto1 = $prod->getSku() . '-' . implode('-', $autogenerateOptions);
//                            $__skuAutoIdx = 0;
//                            while ($this->_helperCatalog->getPidBySku($__skuAuto, $qcProd->getId())) {
//                                $__skuAuto = $__skuAuto1.'-'.(++$__skuAutoIdx);
//                            }
                            $optionId = $qcProd->getData('groupon_option');
                            $_skuAuto = $prod->getSku() . '-' . $optionId;
                            $qcProd->setSku($_skuAuto);
                        }

                        if ($this->_hlp->getObj('\Unirgy\DropshipVendorProduct\Model\Source')->isCfgUploadImagesSimple()) {
                            $this->processQcMediaChange($prod, $qcProd, $isNew);
                        }


                        $qcProd->setData('_allow_use_renamed_image', true);
                        $qcProd->setUdprodIsQcNew(!$qcProd->getId());
                        $qcProd->save();
                        try {
//                            $newSku = $prod->getSku().'-'.$qcProd->getId();
                            $newSku = $qcProd->getId();
                            $qcProd->setSku($newSku);
                            $qcProd->save();
                        } catch (\Exception $e) {
                            try {
                                $newSku = $prod->getSku() . '-' . $qcProd->getId();
                                $qcProd->setSku($newSku);
                                $qcProd->save();
                            } catch (\Exception $e) {
                                try {
                                    $newSku = $qcProd->getId() . '-' . time();
                                    $qcProd->setSku($newSku);
                                    $qcProd->save();
                                } catch (\Exception $e) {

                                }
                            }
                        }
                        $this->processAfterSave($qcProd);
                        if ($qcProd->getUdprodNeedToUnpublish()) {
                            $this->addUnpublishPids($prod, [$qcProd->getId()]);
                        }

                        if ($this->_hlp->isUdmultiActive()) {
                            $this->_hlp->rHlp()->insertIgnore(
                                'udropship_vendor_product',
                                ['vendor_id' => $v->getId(), 'product_id' => $qcProd->getId(), 'status' => $this->_hlp->getObj('Unirgy\DropshipMulti\Helper\Data')->getDefaultMvStatus()]
                            );
                            $udmultiUpdate = $qcMP;
                            $udmultiUpdate['isNewFlag'] = $isNew;

                            //Change sku
                            if ($qcProd->getTypeId() == 'coupon') {
                                $udmultiUpdate['vendor_sku'] = $qcProd->getSku();
                            }

                            $this->_hlp->processDateLocaleToInternal(
                                $udmultiUpdate,
                                ['special_from_date', 'special_to_date'],
                                $this->_hlp->getDefaultDateFormat()
                            );
                            $this->_hlp->getObj('Unirgy\DropshipMulti\Helper\Data')->saveThisVendorProductsPidKeys(
                                [$qcProd->getId() => $udmultiUpdate], $v
                            );
                        }
                    } catch (\Exception $e) {
                        $this->_hlp->getObj('\Magento\Framework\Message\ManagerInterface')->addError($e->getMessage());
                        continue;
                    }
                    $newPids[] = $qcProd->getId();

                    //Magenest
                    if ($qcProd->getTypeId() == 'coupon') {
                        $parentType = 'Groupon';
                    }
                    //Add data for event observer
                    if ($parentType === 'Groupon') {
                        $observerData[] = ["product_id" => $qcProd->getId(), "information" => $information, 'product' => $qcProd];
                    }
                }
            }
            $delSimplePids = array_diff($existingPids, $newPids);
            $addSimplePids = array_diff($newPids, $existingPids);

            foreach($observerData as $key => $observerDatum) {
                $this->_eventManager->dispatch('unirgy_save_coupon_product_after', $observerDatum);
            }

            $reindexPids = array_merge($newPids, $existingPids);
            $reindexPids[] = $prod->getId();
            $this->processCfgPriceChanges($prod, $reindexPids);
            $this->addReindexPids($prod, $reindexPids);

            return ['delCoupon' => $delSimplePids, 'addCoupon' => $addSimplePids];
        }
    }

    public function _addCfgSimplesDescrDataApproval($prod, $isNew, $simplePids, $descrAttr)
    {
        $cfgSimplesDescrDataCol = $this->_modelProductFactory->create()->getCollection()
            ->addAttributeToSelect(['name','sku']);
        foreach ($this->getConfigurableAttributes($prod, $isNew) as $cfgAttr) {
            $cfgSimplesDescrDataCol->addAttributeToSelect($cfgAttr->getAttributeCode());
        }
        $cfgSimplesDescrDataCol->addIdFilter($simplePids);

        $simplesDescrData = [];
        foreach ($cfgSimplesDescrDataCol as $csdProd) {
            $csdSI = $this->_hlp->getStockItem($csdProd);
            $siHlp = $this->_helperData;
            $_descrText = sprintf('id: %s; sku: %s; stock qty: %s; stock status: %s;',
                $csdProd->getId(), $csdProd->getSku(),
                $csdSI->getQty(),
                ($csdSI->getIsInStock() ? __('In Stock') : __('Out of Stock'))
            );
            foreach ($this->getConfigurableAttributes($prod, $isNew) as $cfgAttr) {
                $_descrText .= sprintf('%s [%s]: %s;',
                    $cfgAttr->getStoreLabel(), $cfgAttr->getAttributeCode(),
                    $cfgAttr->getSource()->getOptionText($prod->getData($cfgAttr->getAttributeCode()))
                );
            }
            $simplesDescrData[$csdProd->getId()] = substr($_descrText, 0, -1);
        }
        $exisSimplesDescr = $prod->getData($descrAttr);
        if (!is_array($exisSimplesDescr)) {
            try {
                $exisSimplesDescr = $this->_hlp->unserialize($exisSimplesDescr);
            } catch (\Exception $e) {
                $exisSimplesDescr = [];
            }
        }
        if (!is_array($exisSimplesDescr)) {
            $exisSimplesDescr = [];
        }
        $exisSimplesDescr = array_merge($exisSimplesDescr, $simplesDescrData);
        $prod->setData($descrAttr, $this->_hlp->serialize($exisSimplesDescr));
        $prod->getResource()->saveAttribute($prod, $descrAttr);
        $prod->setData($descrAttr, $exisSimplesDescr);
    }
}