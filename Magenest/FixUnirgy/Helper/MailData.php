<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Helper;

use Magento\Framework\App\ObjectManager;
use Magenest\Groupon\Helper\Data;
use Magento\Framework\DataObject;
use Magento\Framework\UrlInterface;

class MailData
{
    const HEADER_PRODUCT_NUMBER = 1;
    const FEATURED_PRODUCT_NUMBER = 3;

    /**
     * @var $_objectFactory DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_productCollectionFactory \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var $_imageFactory \Magento\Catalog\Helper\ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_udProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_udProductFactory;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var $_priceHelper \Magenest\SellYours\Helper\PriceHelper
     */
    protected $_priceHelper;

    /**
     * @var $_vesBlock \Ves\PageBuilder\Model\BlockFactory
     */
    protected $_vesBlock;

    protected $_categoryList = [4, 5, 3, 6, 524];

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magenest\SellYours\Helper\PriceHelper $priceHelper,
        \Ves\PageBuilder\Model\BlockFactory $blockFactory,
        DataObject $dataObject
    ) {
        $this->_objectFactory = $dataObject;
        $this->_vesBlock = $blockFactory;
        $this->_priceHelper = $priceHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_udProductFactory = $unirgyProductFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productRepository = $productRepository;
        $this->_dealFactory = $dealFactory;
        $this->_productCollectionFactory = $collectionFactory;
        $this->_productFactory = $productFactory;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getGosawaMailData()
    {
        $result = [];
        $result['time_sent'] = date('d/m/Y');
        $result['currency'] = ['currency_code' => $this->getCurrencyCode(), 'currency_symbol' => $this->getCurrencySymbol()];
        $result['last_chance_list'] = $this->getLastChanceList();
        $result['category_list'] = $this->getCategoryListName();
        foreach($this->_categoryList as $list) {
            $result['featured_list']['featured_product_' . $list] = $this->getHighlightsProduct(self::FEATURED_PRODUCT_NUMBER, $list);
        }

        return $result;
    }

    /**
     * @param $productAmount
     * @param null $categoryId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getHighlightsProduct($productAmount, $categoryId = null)
    {
        $result = [];
        $category = $this->_categoryFactory->create()->load($categoryId);
        $result['category_name'] = $category->getName();
        $result['category_link'] = $category->getUrl();
        $collectionIds = $this->_productCollectionFactory->create()->addCategoryFilter($category)->getAllIds($productAmount);
        $items = [];
        foreach($collectionIds as $item) {
            $product = $this->_productRepository->getById($item);
            array_push($items, $this->getProductEmailData($product, true));
        }
        $result['category_items'] = $items;

        return $result;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getLastChanceList()
    {
        $collection = $this->getLastChanceProductCollection();
        return $this->prepareLastChanceProduct($collection);
    }

    /**
     * @return array
     */
    protected function getGrouponProductIds()
    {
        /** @var  $dealCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $dealCollection = $this->_dealFactory->create()->getCollection();
        $dealCollection->addFieldToFilter('product_type', 'configurable');
        $endateDiff = new \Zend_Db_Expr('DATEDIFF( main_table.end_time, date(now()))');
        $dealCollection->addFieldToFilter($endateDiff, 0);
        $dealCollection->getSelect()->limit(20);
        $productIds = $dealCollection->getColumnValues('product_id');
        return $productIds;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getLastChanceProductCollection()
    {
        $productIds = $this->getGrouponProductIds();
        /** @var  $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToSelect(['price', 'name', 'thumbnail']);
        $productCollection->getSelect()->joinLeft(['deal_table' => $productCollection->getTable('magenest_groupon_deal')], 'e.entity_id=deal_table.product_id', ['end_time']);
        $productCollection->addFieldToFilter('entity_id', ['in' => $productIds]);
        $productCollection->getSelect()->__toString();
        $productCollection->getSelect()->limit(20);
        return $productCollection;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function prepareLastChanceProduct($collection)
    {
        $result = [];
        $result['number_product'] = $collection->getSize();
        foreach($collection as $item) {
            $product = $this->_productRepository->getById($item->getEntityId());
            $items = $this->getProductEmailData($product);

            $result['product_list'][] = $items;
        }

        return $result;
    }

    private function getProductVendorName($vendorId)
    {
        return $this->_vendorFactory->create()->load($vendorId)->getVendorName();
    }

    private function getLastChanceProductLocation($entityId)
    {
        $locationIds = $this->_dealFactory->create()->load($entityId, 'product_id')->getLocationData();
        $result = "";
        foreach($locationIds as $location) {
            if (isset($location['city']) && trim($location['city']) !== "") {
                $result .= $location['city'] . ", ";
            } else if (isset($location['region']) && trim($location['region']) !== "") {
                $result .= $location['region'] . ", ";
            }
            if (isset($location['country']) && trim($location['country']) !== "") {
                $result .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($location['country']);
            }
            if (isset($location['country']) && (isset($location['region']) || isset($location['city']))) {
                break;
            }
        }

        return $result;
    }

    private function getChildCouponProduct($entityId)
    {
        $result = [];
        $productCollection = $this->_udProductFactory->create()->getCollection();
        $productCollection->addFieldToSelect(['product_id', 'vendor_price', 'special_price']);;
        $productCollection->getSelect()->joinLeft(['deal_table' => $productCollection->getTable('magenest_groupon_deal')], 'main_table.product_id = deal_table.product_id', 'parent_id');
        $productCollection->addFieldToFilter('deal_table.parent_id', $entityId);
        $data = $productCollection->getData();
        foreach($data as $item) {
            $result[] = $item;
        }
        return $result;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol = $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    public function getCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    private function getProductEmailData($product, $description = false)
    {
        /**
         * @var $product \Magento\Catalog\Model\Product
         */
        $items = [];
        $thumbnail = $product->getData('thumbnail');
        if (isset($thumbnail)) {
            $items['product_image'] = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $thumbnail;;
        } else {
            $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
            if ($placeholderImageUrl !== null) {
                $items['product_image'] = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product/placeholder/' . $placeholderImageUrl;
            } else {
                $items['product_image'] = '';
            }
        }
        $items['product_name'] = $product->getName();
        $items['product_vendor'] = $this->getProductVendorName($product->getUdropshipVendor());
        $items['product_location'] = $this->getProductLocation($product->getEntityId(), $product->getTypeId(), $product->getUdropshipVendor());
        $items['product_url'] = $product->getProductUrl();
        if ($description) {
            $items['product_description'] = $this->trimText($product->getDescription(), 197);
        }
        $productPrice = $this->getPriceInfo($product);
        $items['product_price'] = $productPrice['originPrice'] ?: 0;
        $items['product_special_price'] = $productPrice['price'] ?: 0;

        return $items;
    }

    public function trimText($input, $length, $ellipses = true, $strip_html = true)
    {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @return array
     */
    protected function getPriceInfo($product)
    {
        $oriPrice = 0;
        $salePrice = PHP_INT_MAX;
        $changeFlag = false;
        if (Data::isDeal($product->getId())) {
            $childProducts = $this->getChildCouponProduct($product->getId());
            foreach($childProducts as $_p) {
                if ($_p['special_price'] !== null
                    && ($_p['special_price'] < $salePrice
                        || ($_p['special_price'] == $salePrice
                            && $_p['vendor_price'] > $oriPrice))
                ) {
                    $salePrice = $_p['special_price'];
                    $oriPrice = $_p['vendor_price'];
                    $changeFlag = true;
                }
            }
            if (!$changeFlag) {
                if ($product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())
                    $oriPrice = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue() ?: 0;
                else $oriPrice = 0;

                if ($product->getMinimalPrice())
                    $salePrice = $product->getMinimalPrice();
                elseif ($product->getFinalPrice())
                    $salePrice = $product->getFinalPrice();
                else $salePrice = 0;
            }
        } else if ($product->getTypeId() === 'simple' || $product->getTypeId() === 'Simple') {
            $simplePrice = $this->_priceHelper->getSimpleProductPrice($product->getEntityId());
            if (@$simplePrice['old_price']) {
                $oriPrice = $simplePrice['old_price'];
                $salePrice = $simplePrice['current_price'];
            } else {
                $salePrice = $simplePrice['current_price'];
                $oriPrice = $simplePrice['current_price'];
            }
        } else {
            if ($product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())
                $oriPrice = $product->getPrice() ?: 0;
            else $oriPrice = 0;

            if ($product->getMinimalPrice())
                $salePrice = $product->getMinimalPrice();
            elseif ($product->getFinalPrice())
                $salePrice = $product->getFinalPrice();
            else $salePrice = 0;
        }
        $sale = $oriPrice == 0 ? 0 : ($oriPrice - $salePrice) * 100 / $oriPrice;
        $data = [
            'originPrice' => number_format($oriPrice, 2),
            'price' => number_format($salePrice, 2),
            'saleRates' => number_format($sale, 0)
        ];
        return $data;
    }

    private function getProductLocation($entityId, $typeId, $vendorId = null)
    {
        if ($typeId === 'configurable') {
            return $this->getLastChanceProductLocation($entityId);
        } elseif ($typeId === 'simple') {
            return $this->getSimpleProductLocation($vendorId);
        } elseif ($typeId === 'ticket') {
            return $this->getTicketLocation($entityId);
        } else {
            return '';
        }
    }

    private function getSimpleProductLocation($vendorId)
    {
        return '';
    }

    private function getTicketLocation($entityId)
    {
        $location = ObjectManager::getInstance()->create('Magenest\Ticket\Model\EventLocation')->load($entityId, 'product_id')->getData();
        $result = "";
        if (isset($location['city']) && trim($location['city']) !== "") {
            $result .= $location['city'] . ", ";
        } else if (isset($location['state']) && trim($location['state']) !== "") {
            $result .= $location['state'] . ", ";
        }
        if (isset($location['country']) && trim($location['country']) !== "") {
            $result .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($location['country']);
        }

        return $result;
    }

    private function getCategoryListName()
    {
        $result = [];
        foreach($this->_categoryList as $item) {
            $categories = $this->_categoryFactory->create()->load($item);
            array_push($result, ['category_name' => $categories->getName(), 'category_link' => $categories->getUrl()]);
        }

        return $result;
    }
}
