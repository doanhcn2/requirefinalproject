<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Helper;

use \Magenest\Groupon\Model\ResourceModel\Deal\Collection as DealCollection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory as DealCollectionFactory;
use \Magenest\Groupon\Model\Product\Attribute\CampaignStatus as CampaignStatus;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\CollectionFactory as ExpiredProductCollectionFactory;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\Collection as ExpiredProductCollection;

class ReloadHiddenIds
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var DealCollectionFactory
     */
    protected $dealCollectionFactory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ExpiredProductCollectionFactory
     */
    protected $expiredProductCollectionFactory;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute
     */
    protected $_eavAttribute;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Index constructor.
     * @param DealCollectionFactory $dealCollectionFactory
     * @param CollectionFactory $collectionFactory
     * @param ExpiredProductCollectionFactory $expiredProductCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
     * @param Context $context
     */
    public function __construct(
        DealCollectionFactory $dealCollectionFactory,
        CollectionFactory $collectionFactory,
        ExpiredProductCollectionFactory $expiredProductCollectionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute
    )
    {
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_eavAttribute = $eavAttribute;
        $this->expiredProductCollectionFactory = $expiredProductCollectionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->dealCollectionFactory = $dealCollectionFactory;
    }
    public function reloadHiddenIds()
    {
        try {
            $grouponIds = $this->getHiddenGrouponIds();
            $nonLiveProduct = $this->getNonLiveProductIds();
            $ids =
                array_unique(
                    array_merge(
                        array_values($grouponIds),
                        array_values($nonLiveProduct)
                    )
                );
            /** @var ExpiredProductCollection $collection */
            $collection = $this->expiredProductCollectionFactory->create();
            $model = $collection->getFirstItem();
            $model->setData('product_ids', implode(',', $ids));
            $model->setData('updated_at', date("Y-m-d H:i:s"));
            $model->save();
            $this->logger->info("Successfully update restricted product ids.");
        } catch (\Exception $exception) {
            $this->logger->critical("Something went wrong while updating retricted product ids. See below");
            $this->logger->critical($exception->getMessage());
        }
    }

    /**
     * @return array
     */
    public function getHiddenGrouponIds()
    {
        /** @var DealCollection $collection */
        $collection = $this->dealCollectionFactory->create();
        $collection = $this->getExpiredGrouponIds($collection);
        return $collection->getColumnValues('product_id');
    }

    /**
     * @param $dealCollection DealCollection
     * @return mixed
     */
    public function getExpiredGrouponIds($dealCollection)
    {
        $dealCollection
            ->addFieldToFilter('end_time', ['lt' => date('Y-m-d')])
            ->addFieldToFilter('redeemable_after', ['lt' => date('Y-m-d')]);
        return $dealCollection;
    }

    /**
     * @return array
     */
    public function getNonLiveProductIds()
    {
        /** @var Collection $productCollection */
        $productCollection = $this->collectionFactory->create();
        $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'campaign_status');
//        $productCollection->addFieldToSelect('campaign_status')->addFieldToFilter('campaign_status', [ 'nin' =>[CampaignStatus::STATUS_LIVE, CampaignStatus::STATUS_ACTIVE]]);
        $productCollection->getSelect()
            ->joinLeft(
                ['at_campaign_status' => $productCollection->getTable('catalog_product_entity_varchar')],
                '`at_campaign_status`.`row_id` = `e`.`row_id` AND `at_campaign_status`.`attribute_id` = ' . $attributeId . ' AND `at_campaign_status`.`store_id` = 0',
                'at_campaign_status.value AS campaign_status')
            ->where("((`at_campaign_status`.`value` NOT IN (" . CampaignStatus::STATUS_LIVE . "," . CampaignStatus::STATUS_ACTIVE . ", -1)) OR (`at_campaign_status`.`value` IS NULL) )");
        return $productCollection->getAllIds();
    }
}