/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry',
        'mage/url',
    ], function ($, $t, ko, Component, registry, urlBuilder) {
        'use strict';

        return Component.extend({
            defaults: {
                vendorId: 0,
                isShipping: 'no',
                currency: '',
                currencyCode: 'USD',
                subtotal: 0,
                shipping: 0,
                tax: 0,
                shippingMethod: [],
                isShippingNeeded: false,
                total: 0
            },
            initObservable: function () {
                this._super().observe('shippingMethod');
                this._super().observe('shipping');
                this._super().observe('total');
                this._super().observe('tax');
                this._super().observe('subtotal');
                return this;
            },
            initialize: function () {
                this._super();
                let self = this;
                $('select[id*=select-shipping-vendor-]').prop("disabled", true);
                $('div[id*=shipping-error-]').hide();
                self.isShippingNeeded = self.isShipping === 'yes';
                self.calculateTotal();
                Number.prototype.formatMoney = function (c, d, t) {
                    var n = this,
                        c = isNaN(c = Math.abs(c)) ? 2 : c,
                        d = d === undefined ? "." : d,
                        t = t === undefined ? "," : t,
                        s = n < 0 ? "-" : "",
                        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                        j = (j = i.length) > 3 ? j % 3 : 0;
                    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
                };
                return self;
            },
            calculateTotal: function () {
                let self = this;
                let totalFee = self.subtotal() + self.shipping() + self.tax();
                self.total(totalFee);
            },
            reloadShippingData: function (address, vendorId) {
                let self = this;
                let postData;
                if (address == null)
                    return;
                if (address.regionId !== undefined) {
                    postData = {
                        city: address.city,
                        region_id: address.regionId,
                        region: address.region,
                        country_id: address.countryId,
                        postcode: address.postcode
                    };
                } else {
                    postData = {
                        city: address.city,
                        region: address.region,
                        country_id: address.countryId,
                        postcode: address.postcode
                    };
                }
                let url = urlBuilder.build('fixunirgy/cart/shipping');
                let subtotalBLock = $('.subtotal-vendor-checkout-cart');
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'JSON',
                    data: postData,
                    success: function (response) {
                        if (response.success === true) {
                            let methods = response.methods;
                            if (vendorId == null) {
                                for (let key in methods) {
                                    if (methods.hasOwnProperty(key)) {
                                        self.reloadShippingMethod(key, methods[key]);
                                    }
                                }
                            } else if (methods.hasOwnProperty(vendorId)) {
                                self.reloadShippingMethod(vendorId, methods[vendorId], true);
                            }
                        }
                    },
                    error: function (error) {
                    },
                    context: subtotalBLock,
                    showLoader: true
                });
            },
            reloadShippingMethod: function (vendorId, methods, selectFirst) {
                let self = this;
                let selectBox = $('#select-shipping-vendor-' + vendorId);
                let errorDiv = $('#shipping-error-' + vendorId);
                errorDiv.html('');
                let newHtml = '';
                let noQuote = false, noQuoteHtml = '';
                if (selectFirst === true){
                    var selectedOrNot = 'selected';
                }
                $.each(methods, function (e, v) {
                    let optionTemplate;
                    if (v.carrier_title !== 'FBG') {
                        if (!v.error_message && v.available) {
                            optionTemplate = "<option " + selectedOrNot + " value='" + v.amount + "' data-method='" + JSON.stringify(v) + "'>" +
                                v.carrier_title + " - " + v.method_title + " " + v.currency_symbol + parseFloat(v.amount).toFixed(2)
                                + "</option>";
                            selectBox.prop("disabled", false);
                            if (selectedOrNot != "")
                                selectedOrNot = "";
                        } else if (v.error_message.error !== undefined) {
                            // optionTemplate = '<option disabled>' + v.error_message.error + '</option>';
                            errorDiv.html(v.error_message.error);
                            errorDiv.show();
                        } else if (v.available === null) {
                            noQuote = true;
                            // noQuoteHtml = '<option value="-2">' + $t('Select shipping method') + '</option>';
                            noQuoteHtml = '<option value="-1" disabled selected>' + $t('Shipping is not available') + '</option>';
                            errorDiv.html(v.error_message);
                            errorDiv.show();
                        }
                        newHtml += optionTemplate;
                    }
                });
                newHtml = "<option value='-1' selected>Select Shipping Method</option>" + newHtml;
                if (noQuote) {
                    selectBox.html(noQuoteHtml);
                } else {
                    selectBox.html(newHtml);
                }
                selectBox.change();
            },
            selectVendorShippingMethod: function (method) {
                let self = this;
                let shippingSelected = $('#select-shipping-vendor-' + self.vendorId + ' :selected');
                let shippingFee = shippingSelected.val();
                if (shippingFee === -1 || shippingFee === "-1" || shippingFee === -2 || shippingFee === "-2" || shippingFee === undefined) {
                    shippingFee = 0;
                    return true;
                }
                self.shipping(parseFloat(shippingFee));
                self.calculateTotal();
                let shippings = shippingSelected.data('method');
                delete shippings.currency_symbol;
                let uu = registry.get('block-summary.block-rates');
                uu.selectShippingMethod(shippings);
            }
        });
    }
);
