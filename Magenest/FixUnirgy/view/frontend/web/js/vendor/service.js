/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Ui/js/modal/alert',
        'Magento_Ui/js/modal/modal',
        'mage/mage'
    ], function ($, ko, Component, alert, modal) {
        'use strict';
        return Component.extend({
            popup: null,
            url: ko.observable(''),
            initialize: function () {
                this._super();
                return this;
            },
            initObservable: function () {
                this._super();
                return this;
            },
            initPopupModal: function () {
                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    buttons: [{
                        text: 'Close',
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };
                this.popup = modal(options, $('#popup-modal-view'));
            },
            popupModal: function (url) {
                this.url(url);
                this.popup.openModal();
            },
            addEvent: function () {
                $(".location-item .location-header .location-text").click(function () {
                    $(this).parents(".location-item").toggleClass("active");
                });
            },
            redirectAfterClickAction: function (path) {
                var link = "https://www.google.com/maps/dir/_CURL_/" + path;
                var storage = $.initNamespaceStorage('customer_location').localStorage;

                if (!storage.get('customer_location') == false) {
                    link = link.replace('_CURL_', storage.get('customer_location'));
                } else {
                    link = link.replace('_CURL_', '');
                }
                window.open(link, '_blank');
            }
        });
    }
);