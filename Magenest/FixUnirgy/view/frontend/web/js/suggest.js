define([
    'jquery',
    'mage/url'
], function ($, url) {
    'use strict';

    let isNeedCommission = BASE_URL.search('umicrosite');
    url.setBaseUrl(BASE_URL.replace(/udprod\/$/, ''));

    function remove(array, element) {
        var index = array.indexOf(element);

        if (index !== -1) {
            array.splice(index, 1);
        }
    }

    function setCommissionAndRate() {
        if (isNeedCommission === -1) {
            $.ajax({
                type: "POST",
                url: url.build('/fixunirgy/vendor/getcommission'),
                data: {
                    ids: window.selected_category_ids
                },
                showLoader: true,
                success: function (result) {
                    if (result.commission !== undefined)
                        window.commission_rate['commission'] = result.commission;
                    if (result.rate !== undefined)
                        window.commission_rate['rate'] = result.rate;
                },
                error: function () {
                    window.commission_rate['commission'] = 0;
                    window.commission_rate['rate'] = 0;
                }
            });
        }
    }

    function regenerateCategory(ids, action) {
        if (isNeedCommission === -1) {
            if (action === 'remove') {
                $('div#breadcrumb_' + ids).remove();
            } else if (action === 'add') {
                $.ajax({
                    type: "POST",
                    url: url.build('fixunirgy/vendor/getcategorybreadcrumb'),
                    data: {
                        ids: ids
                    },
                    showLoader: true,
                    success: function (result) {
                        if (result.category_id !== undefined && result.category_breadcrumb !== undefined) {
                            var strCategory = '';
                            for (var j = 0; j < result.category_breadcrumb.length; j++) {
                                if (j === 0) {
                                    strCategory = result.category_breadcrumb[j];
                                } else {
                                    strCategory += '->' + result.category_breadcrumb[j];
                                }
                            }
                            $('#category_breadcrumb .category-list').append('<div class="category-choosed" id="breadcrumb_' + result.category_id + '">' + strCategory + '</div>');
                        }
                    }
                });
            }
        }
    }

    return function (widget) {

        $.widget('mage.suggest', widget, {
            _create: function () {
                this._super();
                var selectCategory = $('#product_categories').find(':selected');
                window.selected_category_ids = [];
                window.commission_rate = [];
                selectCategory.each(function () {
                    window.selected_category_ids.push($(this).val())
                });
                setCommissionAndRate();
            },

            _addOption: function (e, item) {
                this._super(e, item);
                window.selected_category_ids.push(item['id']);
                setCommissionAndRate();
                regenerateCategory(item['id'], 'add');
            },
            removeOption: function (e, item) {
                this._super(e, item);
                remove(window.selected_category_ids, item.id);
                setCommissionAndRate();
                regenerateCategory(item['id'], 'remove');
            }
        });

        return $.mage.configurable;
    }
});