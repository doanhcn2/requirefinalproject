<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Plugin\Controller\Adminhtml\Product;

use Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper;

class Save
{
    public function beforeInitializeFromData(Helper $helper, \Magento\Catalog\Model\Product $product, array $productData)
    {
        if ($product->getTypeId() === 'coupon') {
            $udmultiData = @$productData['udmulti_vendors'][0];
            $useSimple = $useSpecial = false;
            if ($udmultiData) {
                $vendorPrice = @$udmultiData['vendor_price'];
                $specialPrice = @$udmultiData['special_price'];
                if ($specialPrice) {
                    $specialFromDate = $udmultiData['special_from_date'];
                    $specialToDate = $udmultiData['special_to_date'];
                    if ($specialFromDate && $specialToDate) {
                        if (time() >= strtotime($specialFromDate) && time() <= strtotime($specialToDate)) {
                            $useSpecial = true;
                        } else {
                            $useSimple = true;
                        }
                    } elseif (!$specialFromDate && $specialToDate) {
                        if (time() <= strtotime($specialToDate)) {
                            $useSpecial = true;
                        } else {
                            $useSimple = true;
                        }
                    } elseif ($specialFromDate && !$specialToDate) {
                        if (time() >= strtotime($specialFromDate)) {
                            $useSpecial = true;
                        } else {
                            $useSimple = true;
                        }
                    } else {
                        $useSpecial = true;
                    }
                } elseif ($vendorPrice) {
                    $useSimple = true;
                }
            }
            if ($useSpecial) {
                $productData['price'] = @$specialPrice;
                unset($productData['special_price']);
                unset($productData['cost']);
            } elseif ($useSimple) {
                $productData['price'] = @$vendorPrice;
                unset($productData['special_price']);
                unset($productData['cost']);
            }
        }

        return [$product, $productData];
    }
}