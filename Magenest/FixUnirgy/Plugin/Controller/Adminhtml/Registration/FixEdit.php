<?php
namespace Magenest\FixUnirgy\Plugin\Controller\Adminhtml\Registration;

use Unirgy\DropshipMicrosite\Controller\Adminhtml\Registration\Edit;
use Unirgy\DropshipMicrosite\Controller\Adminhtml\Registration\AbstractRegistration;
use Unirgy\Dropship\Model\VendorFactory;

class FixEdit extends AbstractRegistration
{
    private $_vendorFactory;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Backend\App\Action\Context $context,
        VendorFactory $vendorFactory
    )
    {
        $this->_vendorFactory = $vendorFactory;
        parent::__construct($udropshipHelper, $micrositeHelper, $registry, $resultForwardFactory, $resultPageFactory, $resultRedirectFactory, $context);
    }


    public function aroundExecute(Edit $subject, callable $proceed)
    {
        return $this->execute();
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('reg_id');
        $reg = $this->_vendorFactory->create()->load($id);
        if (!$reg) {
            return $this->resultRedirectFactory->create()->setPath('umicrosite/registration/index');
        }
        $reg->setStatus('A');
        $reg->setData('reg_id', $reg->getVendorId());
        $this->_registry->register('vendor_data', $reg);

        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        $resultForward->setModule('udropship')->setController('vendor')->forward('edit');
        return $resultForward;
    }
}