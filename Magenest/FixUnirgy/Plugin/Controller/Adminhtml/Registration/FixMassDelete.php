<?php
namespace Magenest\FixUnirgy\Plugin\Controller\Adminhtml\Registration;

use Unirgy\DropshipMicrosite\Controller\Adminhtml\Registration\AbstractRegistration;
use Unirgy\Dropship\Model\VendorFactory;
use Magento\Backend\App\Action;
use Unirgy\DropshipMicrosite\Controller\Adminhtml\Registration\MassDelete;

class FixMassDelete extends AbstractRegistration
{
    private $_vendorFactory;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        Action\Context $context,
        VendorFactory $vendorFactory
    )
    {
        $this->_vendorFactory = $vendorFactory;
        parent::__construct($udropshipHelper, $micrositeHelper, $registry, $resultForwardFactory, $resultPageFactory, $resultRedirectFactory, $context);
    }

    public function aroundExecute(MassDelete $subject, $proceed)
    {
        return $this->execute();
    }

    public function execute()
    {
        $registrationIds = $this->getRequest()->getParam('vendor');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!is_array($registrationIds)) {
            $this->messageManager->addError(__('Please select registration(s)'));
        }
        else {
            try {
                /** @var \Unirgy\DropshipMicrosite\Model\Registration $registration */
                $registration = $this->_hlp->createObj('\Unirgy\DropshipMicrosite\Model\Registration');
                foreach ($registrationIds as $registrationId) {
                    $registration->setId($registrationId)->delete();
                }
                $vendors = $this->_vendorFactory->create()->getCollection();
                $vendors->addFieldToFilter('new_reg', 1)
                    ->addFieldToFilter('vendor_id', ['IN' => $registrationIds]);
                foreach ($vendors as $vendor){
                    $vendor->delete();
                }
                $this->messageManager->addSuccess(
                    __('Total of %1 record(s) were successfully deleted', count($registrationIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $resultRedirect->setPath('*/*/index');
    }
}