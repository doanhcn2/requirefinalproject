<?php
namespace Magenest\FixUnirgy\Plugin\Controller\Vendor;

use Unirgy\DropshipVendorProduct\Controller\Vendor\ProductNew;
use Unirgy\DropshipVendorProduct\Controller\Vendor\AbstractVendor;

class FixProductNew extends AbstractVendor
{
    public function aroundExecute(ProductNew $subject, callable $proceed)
    {
        return $this->execute();
    }

    public function execute()
    {
        $this->_request->setParam('id', null);
        try {
            $this->_renderPage(null, 'udnewprod');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_redirectAfterPost();
        }
    }
}