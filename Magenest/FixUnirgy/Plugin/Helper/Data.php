<?php

namespace Magenest\FixUnirgy\Plugin\Helper;

use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\FixUnirgy\Helper\Review\Data as FUData;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Context;

class Data
{
    private $_reviewFactory;

    private $_storeManager;

    private $_urlBuilder;

    public function __construct(
        Context $context,
        ReviewFactory $reviewFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->_reviewFactory = $reviewFactory;
        $this->_storeManager = $storeManager;
        $this->_urlBuilder = $context->getUrlBuilder();
    }

    public function afterGetCustomerReviewsCollection(HelperData $subject, $result)
    {
        $collection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($collection);
        $this->orderItems($collection);
        $this->orders($collection);
        $collection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [$subject->myEt(), FUData::ENTITY_TYPE_ID]])
            ->addCustomerFilter(ObjectManager::getInstance()->get('Magento\Customer\Model\Session')->getCustomerId())
            ->setDateOrder()
            ->addRateVotes();
        return $collection;
    }

    public function afterGetVendorReviewsCollection(HelperData $subject, $result, $vendor)
    {
        /** @var \Unirgy\Dropship\Helper\Data $udHlp */
        $udHlp = ObjectManager::getInstance()->get(\Unirgy\Dropship\Helper\Data::class);
        $vendor = $udHlp->getVendor($vendor);
        $collection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($collection);
        $this->orderItems($collection);
        $this->orders($collection);
        $collection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [$subject->myEt(), FUData::ENTITY_TYPE_ID]])
            ->addFieldToFilter('main_table.entity_pk_value', $vendor->getId())
            ->setDateOrder()
            ->addRateVotes();
        return $collection;
    }

    public function shipments($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['sm' => 'sales_shipment'],
                "main_table.rel_entity_pk_value = sm.entity_id",
                ['review_entity_id' => 'main_table.entity_id', 'shipment_order_id' => 'sm.order_id', '*']
            );
        return $collection;
    }

    public function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'sales_order_item'],
                "main_table.rel_entity_pk_value = soi.item_id"
            );
        return $collection;
    }

    public function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => 'sales_order'],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );
        return $collection;
    }
}
