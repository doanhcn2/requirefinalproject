<?php

namespace Magenest\FixUnirgy\Plugin\Helper\Data;

use \Unirgy\DropshipPo\Helper\Data;

class HideGrouponPO
{
    public function afterGetVendorPoCollection(Data $subject, $result)
    {
        $tmp = $result;
        $from = $tmp->getSelect()->getPart(\Magento\Framework\DB\Select::FROM);
        if (!isset($from['HGPO'])) {
            $tmp->getSelect()->joinLeft(
                ['HGPO' => $tmp->getTable('udropship_po_item')],
                "main_table.entity_id = HGPO.parent_id",
                ['HGPO_id' => 'HGPO.entity_id','product_id']
            )->group(['main_table.entity_id']);
            $tmp->getSelect()->joinLeft(
                ['CPE' => $tmp->getTable('catalog_product_entity')],
                "HGPO.product_id = CPE.entity_id",
                ['CPE_id' => 'CPE.entity_id','type_id']
            )->where('CPE.type_id != "coupon" AND CPE.type_id != "ticket"');
            $tmp->getSelect()->joinLeft(
                ['CPR' => $tmp->getTable('catalog_product_relation')],
                "HGPO.product_id = CPR.parent_id",
                ['CPR_id' => 'CPR.parent_id','child_id']
            );
            $tmp->getSelect()->joinLeft(
                ['CPE2' => $tmp->getTable('catalog_product_entity')],
                "CPR.child_id = CPE2.entity_id",
                ['CPE2_id' => 'CPE2.entity_id','CPE2_type_id' => 'CPE2.type_id']
            )->where('CPE2.type_id != "coupon" OR CPE2.type_id is NULL');
        }
        return $result;
    }
}