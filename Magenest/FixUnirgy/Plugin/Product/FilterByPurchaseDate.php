<?php

namespace Magenest\FixUnirgy\Plugin\Product;

use \Magenest\Groupon\Model\ResourceModel\Deal\Collection as DealCollection;
use \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory as DealCollectionFactory;
use \Magento\Framework\App\Request\Http;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\CollectionFactory as ExpiredProductCollectionFactory;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\Collection as ExpiredProductCollection;

class FilterByPurchaseDate
{
    /**
     * @var Http
     */
    protected $_request;

    /**
     * @var DealCollectionFactory
     */
    protected $dealCollectionFactory;
    protected $expiredProductCollectionFactory;

    private $_frontendController = [
        'catalog_category_view',
        'catalogsearch_result_index',
        'cms_index_index'
    ];

    public function __construct(
        Http $request,
        ExpiredProductCollectionFactory $expiredProductCollectionFactory,
        DealCollectionFactory $dealCollectionFactory
    )
    {
        $this->expiredProductCollectionFactory = $expiredProductCollectionFactory;
        $this->dealCollectionFactory = $dealCollectionFactory;
        $this->_request = $request;
    }

    public function beforeLoad(\Magento\Catalog\Model\ResourceModel\Product\Collection $subject, $printQuery = false, $logQuery = false)
    {
        return [$printQuery, $logQuery];
        $handle = $this->_request->getFullActionName();
        if (in_array($handle, $this->_frontendController)) {
            $ids = $this->expiredProductCollectionFactory->create()->getFirstItem()->getData('product_ids');
            $ids = explode(',', $ids);
            if (!empty($ids))
                $subject->getSelect()->where("e.entity_id NOT IN(?)", $ids);
        }
        return [$printQuery, $logQuery];
    }
}