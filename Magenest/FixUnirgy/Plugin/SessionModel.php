<?php
/**
 * Author: Eric Quach
 * Date: 1/22/18
 */
namespace Magenest\FixUnirgy\Plugin;

class SessionModel
{
    public function aroundLoginById($subject, $proceed, $vendorId)
    {
        /* @var \Unirgy\Dropship\Model\Vendor $vendor*/
        $vendor = \Magento\Framework\App\ObjectManager::getInstance()->create('\Unirgy\Dropship\Model\Vendor')->load($vendorId);
        if ($vendor->getId()) {
            $subject->setVendorAsLoggedIn($vendor);
            return true;
        }
        return false;
    }
}