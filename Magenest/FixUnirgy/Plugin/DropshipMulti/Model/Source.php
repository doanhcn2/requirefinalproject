<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 23/08/2018
 * Time: 08:45
 */

namespace Magenest\FixUnirgy\Plugin\DropshipMulti\Model;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;

class Source
{
    protected $_coreRegistry;

    protected $_request;

    protected $statusSource;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\RequestInterface $request,
        \Magenest\SellYours\Model\Status\Source $statusSource
    ) {
        $this->statusSource  = $statusSource;
        $this->_coreRegistry = $registry;
        $this->_request      = $request;
    }

    public function aroundToOptionHash(\Unirgy\DropshipMulti\Model\Source $subject, callable $proceed, $selector = false)
    {
        if ($subject->getPath() === 'vendor_product_status' && $prod = $this->_coreRegistry->registry('current_product')) {
            if ($prod->getTypeId() === 'simple' && $this->_request->getFullActionName() === 'catalog_product_edit') {
                $return = array_slice($this->statusSource->getSatusArray(), 0, 5, 1);

                return $return;
            }
        }

        return $proceed($selector);
    }
}
