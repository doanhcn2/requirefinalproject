<?php
/**
 * Author: Eric Quach
 * Date: 1/17/18
 */
namespace Magenest\FixUnirgy\Plugin;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\RequestInterface;

class InitGrouponOption
{
    protected $attributeFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory
    )
    {
        $this->attributeFactory = $attributeFactory;
    }

    public function beforeExecute(
        \Unirgy\DropshipVendorProduct\Controller\Vendor\CfgQuickCreateAttribute $subject
    )
    {
        $this->initExistedAttributes($subject->getRequest());
        return [];
    }

    /**
     * @param RequestInterface $request
     */
    private function initExistedAttributes($request)
    {
        $cfg_attr_values = null;
        $grouponOptionNumber = $request->getParam('groupon_option_number');
        $attribute = $this->attributeFactory->create();
        $attribute->loadByCode(Product::ENTITY, 'groupon_option');
        $existedAttributes = $attribute->getSource()->getAllOptions(false);
        $existed = count($existedAttributes);
        if ($grouponOptionNumber > $existed) {
            $optionNumber = $existed + 1;
            $optionLabel = "Option {$optionNumber}";
            $attribute->setOption(
                [
                    'value' => ['option_0' => [$optionLabel]],
                    'order' => ['option_0' => $optionNumber],
                ]
            );
            $attribute->save();
            $attribute = $this->attributeFactory->create();
            $attribute->loadByCode(Product::ENTITY, 'groupon_option');
            $cfg_attr_values = isset(end($attribute->getSource()->getAllOptions(false))['value'])?:null;
        } else {
            $cfg_attr_values = isset($existedAttributes[array_keys($existedAttributes)[$grouponOptionNumber-1]]['value'])
                ? $existedAttributes[array_keys($existedAttributes)[$grouponOptionNumber-1]]['value']
                : null;
        }
        $request->setParams(array_merge($request->getParams(), ['cfg_attr_values' => $cfg_attr_values]));
    }
}