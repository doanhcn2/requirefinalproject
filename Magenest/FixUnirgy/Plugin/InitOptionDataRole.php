<?php
/**
 * Author: Eric Quach
 * Date: 1/22/18
 */
namespace Magenest\FixUnirgy\Plugin;

class InitOptionDataRole
{
    public function aroundGetUdmultiField($subject, $proceed, $field, $mvData)
    {
        $fieldDef = $proceed($field, $mvData);
        $fieldDef['data-role'] = $field;
        return $fieldDef;
    }

    public function afterGetAttributeField($subject, $fieldDef)
    {
        $fieldDef['data-role'] = isset($fieldDef['id']) ? $fieldDef['id']:null;
        return $fieldDef;
    }
}