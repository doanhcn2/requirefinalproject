<?php

namespace Magenest\FixUnirgy\Block\Adminhtml\Registration;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as HelperData;
use Unirgy\DropshipMicrosite\Helper\Data as DropshipMicrositeHelperData;
use Unirgy\DropshipMicrosite\Model\RegistrationFactory;
use Unirgy\Dropship\Model\VendorFactory;
use Unirgy\Dropship\Model\Source;
use Unirgy\DropshipMicrosite\Block\Adminhtml\Registration\Grid as UGrid;

class Grid extends UGrid
{
    /**
     * @var VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var DropshipMicrositeHelperData
     */
    protected $_msHlp;

    /**
     * @var Source
     */
    protected $_src;


    public function __construct(
        Context $context,
        HelperData $backendHelper,
        VendorFactory $vendorFactory,
        DropshipMicrositeHelperData $helperData,
        Source $modelSource,
        array $data = []
    )
    {
        $this->_vendorFactory = $vendorFactory;
        $this->_msHlp = $helperData;
        $this->_src = $modelSource;

        \Magento\Backend\Block\Widget\Grid\Extended::__construct($context, $backendHelper, $data);
        $this->setId('registrationGrid');
        $this->setDefaultSort('vendor_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('reg_filter');
    }

    protected function _prepareCollection()
    {
        $collection = $this->_vendorFactory->create()
            ->getCollection()
            ->addFieldToFilter('new_reg', 1);
        $this->setCollection($collection);
        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = $this->_msHlp;
        $this->addColumn('vendor_id', [
            'header'    => __('Registration ID'),
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'vendor_id',
            'type'      => 'number',
        ]);

        $this->addColumn('vendor_name', [
            'header'    => __('Vendor Name'),
            'index'     => 'vendor_name',
        ]);

        $this->addColumn('email', [
            'header'    => __('Email'),
            'index'     => 'email',
        ]);

        $this->addColumn('carrier_code', [
            'header'    => __('Used Carrier'),
            'index'     => 'carrier_code',
            'type'      => 'options',
            'options'   => $this->_src->setPath('carriers')->toOptionHash(),
        ]);

        $this->addExportType('*/*/exportCsv', __('CSV'));
        $this->addExportType('*/*/exportXml', __('XML'));
        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['reg_id' => $row->getId()]);
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('vendor');

        $this->getMassactionBlock()->addItem('delete', [
            'label'=> __('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => __('Are you sure?')
        ]);

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current'=>true]);
    }
}
