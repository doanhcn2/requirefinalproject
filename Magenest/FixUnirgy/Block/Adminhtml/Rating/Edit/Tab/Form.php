<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 25/04/2018
 * Time: 13:42
 */
namespace Magenest\FixUnirgy\Block\Adminhtml\Rating\Edit\Tab;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\DB\Helper as DbHelper;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;

class Form extends \Unirgy\DropshipVendorRatings\Block\Adminhtml\Rating\Edit\Tab\Form{
    /**#@+
     * Category tree cache id
     */
    const CATEGORY_TREE_ID = 'CATALOG_PRODUCT_CATEGORY_TREE';
    /**#@-*/

    /**
     * @var CategoryCollectionFactory
     * @since 101.0.0
     */
    protected $categoryCollectionFactory;

    /**
     * @var DbHelper
     * @since 101.0.0
     */
    protected $dbHelper;

    /**
     * @var array
     * @deprecated 101.0.3
     * @since 101.0.0
     */
    protected $categoriesTrees = [];

    /**
     * @var LocatorInterface
     * @since 101.0.0
     */
    protected $locator;

    /**
     * @var UrlInterface
     * @since 101.0.0
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     * @since 101.0.0
     */
    protected $arrayManager;

    /**
     * @var CacheInterface
     */
    private $cacheManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    protected $_ratingInfoFactory;
    public function __construct(
      \Magento\Backend\Block\Template\Context $context,
      \Magento\Framework\Registry $registry,
      \Magento\Framework\Data\FormFactory $formFactory,
      \Unirgy\Dropship\Model\Source $modelSource,
      \Magento\Store\Model\System\Store $systemStore,
      LocatorInterface $locator,
      CategoryCollectionFactory $categoryCollectionFactory,
      DbHelper $dbHelper,
      UrlInterface $urlBuilder,
      ArrayManager $arrayManager,
      \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
      SerializerInterface $serializer = null,
      array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $modelSource,
          $systemStore, $data);
        $this->locator = $locator;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->dbHelper = $dbHelper;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(SerializerInterface::class);
        $this->_ratingInfoFactory = $ratingInfoFactory;
    }

    public function _prepareForm()
    {
       parent::_prepareForm();
       $form = $this->getForm();
//       $form->addElement()
       $field = $form->addField(
         'category_ids',
         '\Magenest\FixUnirgy\Block\Adminhtml\Rating\Element\Categories',
         [
           'formElement' => 'select',
           'label' => 'Allowed Categories',
           'name' => 'category_ids',
           'componentType' => 'field',
           'component' => 'Magento_Catalog/js/components/new-category',
           'filterOptions' => true,
           'chipsEnabled' => true,
           'disableLabel' => true,
           'levelsVisibility' => '1',
           'elementTmpl' => 'ui/grid/filters/elements/ui-select',
           'options' => $this->getCategoriesTree(),
           'listens' => [
             'index=create_category:responseData' => 'setParsed',
             'newOption' => 'toggleOptionSelected'
           ],
           'config' => [
             'dataScope' => 'category_ids',
             'sortOrder' => 10,
           ],
         ]);
       $form->getElement('category_ids')->setValue($this->getCategoryIds());
    }

    /* Retrieve categories tree
    *
    * @param string|null $filter
    * @return array
    * @since 101.0.0
    */
    protected function getCategoriesTree($filter = null)
    {
        $categoryTree = $this->getCacheManager()->load('CATALOG_PRODUCT_CATEGORY_TREE'.'_'.$filter);
        if ($categoryTree) {
            return $this->serializer->unserialize($categoryTree);
        }

        $storeId = $this->locator->getStore()->getId();
        /* @var $matchingNamesCollection \Magento\Catalog\Model\ResourceModel\Category\Collection */
        $matchingNamesCollection = $this->categoryCollectionFactory->create();

        if ($filter !== null) {
            $matchingNamesCollection->addAttributeToFilter(
              'name',
              [
                'like' => $this->dbHelper->addLikeEscape($filter,
                  ['position' => 'any']),
              ]
            );
        }

        $matchingNamesCollection->addAttributeToSelect('path')
          ->addAttributeToFilter('entity_id',
            ['neq' => CategoryModel::TREE_ROOT_ID])
          ->setStoreId($storeId);

        $shownCategoriesIds = [];

        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($matchingNamesCollection as $category) {
            foreach (explode('/', $category->getPath()) as $parentId) {
                $shownCategoriesIds[$parentId] = 1;
            }
        }

        /* @var $collection \Magento\Catalog\Model\ResourceModel\Category\Collection */
        $collection = $this->categoryCollectionFactory->create();

        $collection->addAttributeToFilter('entity_id',
          ['in' => array_keys($shownCategoriesIds)])
          ->addAttributeToSelect(['name', 'is_active', 'parent_id'])
          ->setStoreId($storeId);

        $categoryById = [
          CategoryModel::TREE_ROOT_ID => [
            'value'    => CategoryModel::TREE_ROOT_ID,
            'optgroup' => null,
          ],
        ];

        foreach ($collection as $category) {
            foreach (
              [$category->getId(), $category->getParentId()] as $categoryId
            ) {
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = ['value' => $categoryId];
                }
            }

            $categoryById[$category->getId()]['is_active']
              = $category->getIsActive();
            $categoryById[$category->getId()]['label']
              = $category->getName();
            $categoryById[$category->getParentId()]['optgroup'][]
              = &$categoryById[$category->getId()];
        }

        $this->getCacheManager()->save(
          $this->serializer->serialize($categoryById[CategoryModel::TREE_ROOT_ID]['optgroup']),
          self::CATEGORY_TREE_ID.'_'.$filter,
          [
            \Magento\Catalog\Model\Category::CACHE_TAG,
            \Magento\Framework\App\Cache\Type\Block::CACHE_TAG,
          ]
        );

        return $categoryById[CategoryModel::TREE_ROOT_ID]['optgroup'];
    }

    private function getCacheManager(){
        if (!$this->cacheManager) {
            $this->cacheManager = ObjectManager::getInstance()
              ->get(CacheInterface::class);
        }
        return $this->cacheManager;
    }

    public function getCategoryIds(){
        /** @var  $ratingData \Magento\Review\Model\Rating*/
        $ratingData = $this->_coreRegistry->registry('rating_data');
        if($ratingData!=null){
            $ratingId = $ratingData->getId();
            $collection = $this->_ratingInfoFactory->create()->getCollection()
              ->addFieldToFilter('rating_id', $ratingId);
            if($collection->getSize()!=0){
                $ratingInfoModel = $collection->getFirstItem();
                return explode(',', $ratingInfoModel->getData('category_ids'));
            }
            return [];
        }
        return [];
    }
}