<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 08:25
 */

namespace Magenest\FixUnirgy\Block\Adminhtml\Rating\Element;


class Categories extends \Unirgy\Dropship\Block\Categories
{

    protected function _getSelectorOptions()
    {
        return [
            'source' => $this->_url->getUrl('udropship/index/suggestCategories/type/admin'),
            'valueField' => '#' . $this->getHtmlId(),
            'className' => 'category-select',
            'multiselect' => true,
            'showAll' => true
        ];
    }
}