<?php

namespace Magenest\FixUnirgy\Block\Adminhtml\Vendor\Grid;

use Magento\Backend\Block\Widget\Grid\Column\Filter\Select;

class Options extends Select
{
    protected $map = [
        1 => 'Groupon',
        2 => 'Accommodation',
        3 => 'Event',
        4 => 'Product',
    ];

    protected function _getOptions()
    {
        $options[] = [
            'value' => null,
            'label' => " "
        ];
        foreach ($this->map as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }
        return $options;
    }
}