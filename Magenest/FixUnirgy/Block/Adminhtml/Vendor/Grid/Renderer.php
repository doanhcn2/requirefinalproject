<?php
namespace Magenest\FixUnirgy\Block\Adminhtml\Vendor\Grid;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Renderer extends AbstractRenderer
{
    protected $map = [
            1 => 'Groupon',
            2 => 'Accommodation',
            3 => 'Event',
            4 => 'Product',
        ];
    public function render(DataObject $row)
    {
        if (!$row->getPromoteProduct() || $row->getPromoteProduct() < 1 || $row->getPromoteProduct() > 4)
            $result = 'None';
        else {
            $result = $this->map[$row->getPromoteProduct()];
        }
        return $result;
    }

}