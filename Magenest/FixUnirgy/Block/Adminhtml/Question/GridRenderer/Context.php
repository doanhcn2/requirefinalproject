<?php

namespace Magenest\FixUnirgy\Block\Adminhtml\Question\GridRenderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\Text;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;

class Context extends Text
{
    public function render(DataObject $row)
    {
        $html = '';
        if ($row->getShipmentId()) {
            $html .= sprintf('<h5 style="font-size: 1.3rem">Shipment: <a onclick="this.target=\'blank\'" href="%sshipment_id/%s/">#%s</a></h5>', $this->getUrl('sales/shipment/view'), $row->getShipmentId(), $row->getShipmentIncrementId());
        }
        if ($row->getProductId()) {
            $html .= sprintf('<br><h5 style="font-size: 1.3rem">Product: SKU: %s <a onclick="this.target=\'blank\'" href="%sid/%s/">%s</a></h5>', $row->getProductSku(), $this->getUrl('catalog/product/edit'), $row->getProductId(), $row->getProductName());
        }
        if ($row->getMagentoOrderId()) {
            $html .= sprintf('<br><h5 style="font-size: 1.3rem">Order <a onclick="this.target=\'blank\'" href="%sorder_id/%s/">#%s</a></h5>', $this->getUrl('sales/order/view'), $row->getOrderId(), $row->getOrderIncrementId());
        }
        return $html;
    }
}