<?php

namespace Magenest\FixUnirgy\Block\Adminhtml\Question;

use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;

class Grid extends \Unirgy\DropshipVendorAskQuestion\Block\Adminhtml\Question\Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $prefix = $this->uIsMassactionAvailable() ? '' : 'udquestion_grid_';

        if (!$this->getCustomerId()) {
            $this->getColumn($prefix . 'customer_name')->unsetData('format');
        }

        if (!$this->getVendorId()) {
            $this->getColumn($prefix . 'vendor_id')->setData('format', sprintf('$vendor_name'));
        }

        return $this;
    }
}
