<?php

namespace Magenest\FixUnirgy\Block;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Model\RatingFactory;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class ReviewForm extends Template
{
    /**
     * @var HelperData
     */
    protected $_rateHlp;

    /**
     * @var RatingFactory
     */
    protected $_ratingFactory;

    protected $_hlp;

    protected $_shipment;

    protected $_productRepository;

    protected $_ratingInfoFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    public function __construct(
        \Unirgy\DropshipVendorRatings\Helper\Data $udropshipHelper,
        HelperData $helperData,
        RatingFactory $modelRatingFactory,
        Context $context,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_dealFactory = $dealFactory;
        $this->_productRepository = $productRepository;
        $this->_hlp = $udropshipHelper;
        $this->_rateHlp = $helperData;
        $this->_ratingFactory = $modelRatingFactory;
        $this->_ratingInfoFactory = $ratingInfoFactory;
        parent::__construct($context, $data);
        $this->setTemplate('Magenest_FixUnirgy::ratings/customer/review_form.phtml');
    }

    protected function _beforeToHtml()
    {
        $data = $this->_rateHlp->fetchFormData($this->getRelEntityPkValue());
        $data = is_array($data) ? $data : [];
        $data = new DataObject($data);

        // add logged in customer name as nickname
        if (!$data->getNickname()) {
            $customer = ObjectManager::getInstance()->get('Magento\Customer\Model\Session')->getCustomer();
            if ($customer && $customer->getId()) {
                $data->setNickname($customer->getFirstname());
            }
        }
        $this->assign('data', $data);
        return parent::_beforeToHtml();
    }

    public function getAction()
    {
        $id = $this->getEntityPkValue();
        $relId = $this->getRelEntityPkValue();
        return $this->_urlBuilder->getUrl('fixunirgy/customer/post', ['id'=>$id, 'rel_id'=>$relId]);
    }

    protected function _getRatingsCollection()
    {
        $ratingCollection = $this->_ratingFactory->create()
            ->getResourceCollection()
            ->addEntityFilter('udropship_vendor')
            ->setPositionOrder()
            ->addRatingPerStoreName($this->_storeManager->getStore()->getId())
            ->setStoreFilter($this->_storeManager->getStore()->getId());
        return $ratingCollection;
    }
    protected $_aggregateRatings;
    public function getAggregateRatings()
    {
        if (null === $this->_aggregateRatings) {
            $this->_aggregateRatings = $this->_getRatingsCollection()
                ->addFieldToFilter('is_aggregate', 1)
                ->addOptionToItems();
        }
        return $this->_aggregateRatings;
    }
    protected $_nonAggregateRatings;
    public function getNonAggregateRatings()
    {
        if (null === $this->_nonAggregateRatings) {
            $this->_nonAggregateRatings = $this->_getRatingsCollection()
                ->addFieldToFilter('is_aggregate', 0)
                ->addOptionToItems();
        }
        return $this->_nonAggregateRatings;
    }

    public function checkCategory($rateModel)
    {
        /** @var  $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $this->getOrderItem();
        if ($orderItem != null) {
            $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
                ->addFieldToFilter('rating_id', $rateModel->getId());
            $categoriesId = $this->getProductCategoryIds($orderItem->getProduct());
            if ($ratingInfo->getSize() > 0) {
                $rating = $ratingInfo->getFirstItem();
                $allowedCategory = $rating->getData('category_ids');
                $allowedCategorIdsArray = explode(',', $allowedCategory);
                if (count(array_intersect($allowedCategorIdsArray, $categoriesId)) == 0) return false;
                return true;
            }
            return false;
        }
        return false;
    }
    public function checkCategoryForShipment($rateModel,$shipmentItem){
        /** @var $shipmentItem \Magento\Sales\Model\Order\Shipment */
        if($shipmentItem != null){
            $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
                ->addFieldToFilter('rating_id', $rateModel->getId());
            foreach($shipmentItem->getAllItems() as $_item){
                $product = $this->_productFactory->create()->load($_item->getProductId());
                $categoriesId = $this->getProductCategoryIds($product);
                if ($ratingInfo->getSize() > 0) {
                    $rating = $ratingInfo->getFirstItem();
                    $allowedCategory = $rating->getData('category_ids');
                    $allowedCategorIdsArray = explode(',', $allowedCategory);
                    if (count(array_intersect($allowedCategorIdsArray, $categoriesId)) == 0) return false;
                    return true;
                }
            }
            return false;
        }
    }

    protected function getProductCategoryIds($product)
    {
        if ($product->getTypeId()&&$product->getTypeId() === 'coupon') {
            $parentProductId = $this->_dealFactory->create()->load($product->getEntityId(), 'product_id')->getParentId();
            $childrenCategory = $product->getCategoryIds();
            $parentCategory = $this->_productFactory->create()->load($parentProductId)->getCategoryIds();
            return array_merge($childrenCategory, $parentCategory);
        } else {
            return $product->getCategoryIds();
        }
    }
}
