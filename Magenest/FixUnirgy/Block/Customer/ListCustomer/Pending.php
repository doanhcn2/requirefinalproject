<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Block\Customer\ListCustomer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;
use Magento\Review\Model\ReviewFactory;
use Unirgy\DropshipVendorRatings\Block\Customer\ListCustomer\AbstractList;
use Magento\Store\Model\ScopeInterface;

class Pending extends AbstractList
{
    /**
     * @var HelperData
     */
    protected $_rateHlp;

    /**
     * @var ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var $_hlp \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    public function __construct(
        Context $context,
        HelperData $helperData,
        ReviewFactory $reviewFactory,
        \Unirgy\Dropship\Helper\Data $hlp,
        array $data = []
    ) {
        $this->_hlp = $hlp;
        $this->_reviewFactory = $reviewFactory;
        $this->_rateHlp = $helperData;
        parent::__construct($context, $data);
    }

    protected $_reviewCollection;

    public function getReviewsCollection()
    {
        if (null === $this->_reviewCollection) {
            $this->_reviewCollection = $this->getPendingCustomerReviewsCollection();
        }
        return $this->_reviewCollection;
    }

    protected function getPendingCustomerReviewsCollection()
    {
        $col = $this->_hlp->createObj('\Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Shipment\Collection')
            ->addCustomerFilter(ObjectManager::getInstance()->get('Magento\Customer\Model\Session')->getCustomerId());
        $this->addPendingFilter($col);
        $this->setDateOrder($col);
        return $col;
    }

    private function addPendingFilter($col)
    {
        $reviewTable = $this->_hlp->rHlp()->getTableName('review');
        $col->getSelect()
            ->joinLeft(['rt' => $reviewTable],
                'rt.rel_entity_pk_value = main_table.entity_id and rt.entity_id=' . $this->myEt(),
                [])
            ->where('rt.review_id is null');
        $readyStatuses = $this->_scopeConfig->getValue('udropship/vendor_rating/ready_status', ScopeInterface::SCOPE_STORE);
        if (!is_array($readyStatuses)) {
            $readyStatuses = explode(',', $readyStatuses);
        }
        if (empty($readyStatuses)) {
            $col->getSelect()->where('false');
        } else {
            $col->getSelect()->where('main_table.udropship_status in (?)', $readyStatuses);
        }
        return $col;
    }

    private function setDateOrder($col, $dir='DESC')
    {
        $col->setOrder('main_table.created_at', $dir);
        return $col;
    }

    const PRODUCT_RATING_ENTITY = 1;
    protected $_myEt = self::PRODUCT_RATING_ENTITY;
    private function myEt()
    {
        return $this->_myEt;
    }

    /**
     * @param $shipment \Magento\Sales\Model\Order\Shipment
     * @return array
     */
    public function getShipmentItemHasNoReview($shipment)
    {
        $allItems = $shipment->getAllItems();
        $shipmentReviewIds = $this->getShipmentReviewCollection($shipment->getId());
        $result = [];
        foreach($allItems as $item) {
            if (!in_array($item->getProductId(), $shipmentReviewIds)) {
                $result[] = $item->getProductId();
            }
        }

        return $result;
    }

    protected function getShipmentReviewCollection($shipmentId)
    {
        $collection = $this->_reviewFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', 1)
            ->addFieldToFilter('rel_entity_pk_value', $shipmentId);

        return $collection->getColumnValues('entity_pk_value');
    }
}
