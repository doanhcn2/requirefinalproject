<?php

namespace Magenest\FixUnirgy\Block\Customer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Collection as ShipmentCollection;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Sales\Model\ResourceModel\Order\Item\Collection as OrderItemCollection;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory as OrderItemCollectionFactory;
use Unirgy\Dropship\Model\Source;

class QAForm extends \Unirgy\DropshipVendorAskQuestion\Block\Customer\Form
{
    protected $_orders;

    /**
     * @var OrderCollection
     */
    protected $_ordersCollection;

    /**
     * @var OrderItemCollectionFactory
     */
    protected $_orderItemCollection;

    /**
     * QAForm constructor.
     *
     * @param Context $context
     * @param ShipmentCollection $shipmentCollection
     * @param Source $modelSource
     * @param OrderCollection $orderCollection
     * @param OrderItemCollectionFactory $orderItemCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        ShipmentCollection $shipmentCollection,
        Source $modelSource,
        OrderCollection $orderCollection,
        OrderItemCollectionFactory $orderItemCollection,
        array $data = []
    ) {
        $this->_ordersCollection    = $orderCollection;
        $this->_orderItemCollection = $orderItemCollection;
        parent::__construct($context, $shipmentCollection, $modelSource, $data);
    }

    /**
     * @return OrderCollection
     */
    public function getOrders()
    {
        if (null === $this->_orders) {
            $this->_orders = $this->_ordersCollection;
            $this->_orders->addFieldToFilter('customer_id', ObjectManager::getInstance()->get('Magento\Customer\Model\Session')->getCustomerId());
        }

        return $this->_orders;
    }

    /**
     * @param $orders
     *
     * @return array
     */
    public function getOrderVendors($orders)
    {
        $orderVendors = [];
        foreach ($orders as $order) {
            /** @var OrderItemCollection $orderItemCollection */
            $orderItemCollection = $this->_orderItemCollection->create();
            $orderItemCollection->addFieldToFilter('order_id', $order->getId());
            $vendorIds = $orderItemCollection->getColumnValues('udropship_vendor');
            $vendorIds = array_unique($vendorIds);
            $vendorIds = array_values($vendorIds);

            $orderVendors[$order->getId()]['items'] = $vendorIds;
        }

        return $orderVendors;
    }
}
