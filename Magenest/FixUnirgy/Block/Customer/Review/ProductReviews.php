<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Block\Customer\Review;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;
use Unirgy\DropshipVendorRatings\Block\Customer\ListCustomer\AbstractList;

class ProductReviews extends AbstractList
{
    /**
     * @var $reviewFactory \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_reviewFactory = $reviewFactory;
        parent::__construct($context, $data);
    }

    public function getReviewsCollection()
    {
        $collections = $this->_reviewFactory->create()->getCollection()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', $this->myEt())
            ->addCustomerFilter(ObjectManager::getInstance()->get('Magento\Customer\Model\Session')->getCustomerId())
            ->setDateOrder()
            ->addRateVotes();
        return $collections;
    }

    /**
     * @param $review \Unirgy\DropshipVendorRatings\Model\Review
     * @return string
     */
    public function getProductReviewUrl($review)
    {
        $coreReview  = $this->_reviewFactory->create()->load($review->getId());

        return $review->getProductUrl($review->getEntityPkValue(), $coreReview->getStoreId());
    }

    public function getProductReviewName($productId)
    {
        $collection = $this->_productFactory->create()->load($productId);

        return $collection->getName();
    }

    const PRODUCT_REVIEW_ENTITY = 1;
    protected $_myEt = self::PRODUCT_REVIEW_ENTITY;
    protected function myEt()
    {
        return $this->_myEt;
    }
}