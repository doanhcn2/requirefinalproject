<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 15:13
 */
namespace Magenest\FixUnirgy\Block\Customer\Review;
use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Model\RatingFactory;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class SimplePendingForm extends \Unirgy\DropshipVendorRatings\Block\ReviewForm{
    public function __construct(
      \Unirgy\DropshipVendorRatings\Helper\Data $udropshipHelper,
      \Unirgy\DropshipVendorRatings\Helper\Data $helperData,
      \Magento\Review\Model\RatingFactory $modelRatingFactory,
      \Magento\Framework\View\Element\Template\Context $context,
      array $data = []
    ) {
        parent::__construct($udropshipHelper, $helperData, $modelRatingFactory,
          $context, $data);
//        $this->setTemplate('Magenest_FixUnirgy::ratings/customer/review_form.phtml');
    }
}