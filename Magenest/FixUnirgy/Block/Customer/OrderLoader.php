<?php

namespace Magenest\FixUnirgy\Block\Customer;

use \Magento\Catalog\Block\Product\View\AbstractView;
use \Magento\Framework\Stdlib\ArrayUtils;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Customer\Model\SessionFactory;
use \Magento\Sales\Model\OrderFactory;
use \Unirgy\Dropship\Model\VendorFactory;
use Magenest\FixUnirgy\Helper\Review\Data;

class OrderLoader extends AbstractView
{

    private $customerSession;

    private $order;

    private $vendor;

    private $helper;

    /**
     * @var $_unirgyRatingHelper \Unirgy\DropshipVendorRatings\Helper\Data
     */
    private $_unirgyRatingHelper;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        SessionFactory $sessionFactory,
        OrderFactory $orderFactory,
        VendorFactory $vendorFactory,
        Data $datahelper,
        \Unirgy\DropshipVendorRatings\Helper\Data $unirgyHelper,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        array $data = []
    ) {
        $this->_dealFactory = $dealFactory;
        $this->_unirgyRatingHelper = $unirgyHelper;
        $this->customerSession = $sessionFactory;
        $this->order = $orderFactory;
        $this->vendor = $vendorFactory;
        $this->helper = $datahelper;
        parent::__construct($context, $arrayUtils, $data);
    }

    /**
     * @return mixed
     */

    public function getRatingItemCollection()
    {
        $customerId = $this->customerSession->create()->getCustomerId();
        $itemCollection = $this->helper->getPendingVirtualItemReview();
        $itemCollection->addFieldToFilter('so.customer_id', $customerId);
        if (count($this->getListProductHasReview()) > 0) {
            $itemCollection->addFieldToFilter('main_table.product_id', ['nin' => $this->getListProductHasReview()]);
        }
        $itemCollection->addFieldToFilter('product_type', ['IN' => ['virtual', 'coupon', 'ticket']]);
        $itemCollection->getSelect()->group('main_table.product_id');
        $itemCollection->load();

        return $itemCollection;
    }

    protected function getListProductHasReview()
    {
        $result = [];
        $reviews = $this->_unirgyRatingHelper->getCustomerReviewsCollection();

        foreach($reviews->getItems() as $review) {
            if ($review->getProductId()) {
                $productType = $review->getProductType();
                if (\Magenest\Groupon\Helper\Data::isDeal($review->getProductId())) {
                    array_push($result, $review->getProductId());
                    $dealCollection = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $review->getProductId());
                    foreach($dealCollection->getItems() as $item) {
                        array_push($result, $item->getProductId());
                    }
                } elseif ($productType === 'coupon') {
                    $deal = $this->_dealFactory->create()->load($review->getProductId(), 'product_id');
                    $parentId = $deal->getParentId();
                    array_push($result, $parentId);
                    $dealCollection = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $parentId);
                    foreach($dealCollection->getItems() as $item) {
                        array_push($result, $item->getProductId());
                    }
                } else {
                    array_push($result, $review->getProductId());
                }
            }
        }

        return $result;
    }
}
