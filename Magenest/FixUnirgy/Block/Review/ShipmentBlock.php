<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 16:14
 */
namespace Magenest\FixUnirgy\Block\Review;
use Magento\Review\Model\RatingFactory;
class ShipmentBlock extends \Unirgy\DropshipVendorRatings\Block\ReviewForm{


    protected $_ratingInfoFactory;

    protected $_productRepository;
    public function __construct(
      \Unirgy\DropshipVendorRatings\Helper\Data $udropshipHelper,
      \Unirgy\DropshipVendorRatings\Helper\Data $helperData,
      \Magento\Review\Model\RatingFactory $modelRatingFactory,
      \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
      \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
      \Magento\Framework\View\Element\Template\Context $context,
      array $data = []
    ) {
        parent::__construct($udropshipHelper, $helperData, $modelRatingFactory,
          $context, $data);
        $this->_productRepository = $productRepository;
        $this->_ratingInfoFactory = $ratingInfoFactory;
        $this->setTemplate('Magenest_FixUnirgy::ratings/customer/review_form.phtml');
    }
    public function checkCategory($rateModel){
        /** @var  $shipment \Magento\Sales\Model\Order\Shipment*/
        $shipment = $this->getShipment();
        $shipmentItems = $shipment->getItems();
        $categoriesId = [];
        /** @var  $item \Magento\Sales\Model\Order\Shipment\Item*/
        foreach ($shipmentItems as $item){
            $productId = $item->getProductId();
            /** @var  $product \Magento\Catalog\Model\Product*/
            $product = $this->_productRepository->getById($productId);
            $tempIds = $product->getCategoryIds();
            foreach ($tempIds as $tempId){
                if(!in_array($tempId,$categoriesId)){
                    $categoriesId[] = $tempId;
                }
            }
        }
        $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
          ->addFieldToFilter('rating_id', $rateModel->getId());
        if($ratingInfo->getSize()>0){
            $rating = $ratingInfo->getFirstItem();
            $allowedCategory = $rating->getData('category_ids');
            $allowedCategorIdsArray = explode(',',$allowedCategory);
            if(count(array_intersect($allowedCategorIdsArray, $categoriesId))==0) return false;
            return true;
        }
        if($ratingInfo->getSize()==0) return false;
        if(count($categoriesId)==0) return false;
        return true;
    }

}