<?php
namespace Magenest\FixUnirgy\Block\Email;

use Magenest\FixUnirgy\Helper\MailData;
use Magento\Framework\View\Element\Template;

class NewsletterSubscription extends Template
{
    /**
     * @var $_mailHelper \Magenest\FixUnirgy\Helper\MailData
     */
    protected $_mailHelper;

    public function __construct(
        Template\Context $context,
        MailData $mailData,
        array $data = []
    ) {
        $this->_mailHelper = $mailData;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMailData()
    {
        return $this->_mailHelper->getGosawaMailData();
    }

    public function trimText($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }
}