<?php

namespace Magenest\FixUnirgy\Block\Html;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;

/**
 * Html page breadcrumbs block
 *
 * @api
 * @since 100.0.2
 */
class Breadcrumbs extends \Magento\Theme\Block\Html\Breadcrumbs
{
    protected $_template = 'product/breadcrumbs.phtml';

    /**
     * @var $_registry \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory $_categoryFactory
     */
    protected $_categoryFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = [],
        Json $serializer = null
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->_registry = $registry;
        parent::__construct($context, $data, $serializer);
    }

    protected function _toHtml()
    {
        if ($this->getRequest()->getFullActionName() === 'catalog_product_view' || $this->getRequest()->getFullActionName() === 'checkout_cart_configure') {
            if (count($this->_crumbs) <= 2)
                $this->regenerateBreadcrumbsProduct();
        }

        return parent::_toHtml();
    }

    private function regenerateBreadcrumbsProduct()
    {
        $crumbs = $this->_crumbs;
        $product = $this->_registry->registry('current_product');
        $categories = $product->getCategoryIds();
        $result = [];
        if (is_array($categories) && isset($categories[0])) {
            $result[] = $crumbs['home'];
            $category = $this->_categoryFactory->create()->load($categories[0]);
            $categoryPath = str_replace('1/2/', '', $category->getPath());
            $paths = explode('/', $categoryPath);
            foreach($paths as $path) {
                $result[] = $this->createBreadcrumbsByCategoryId($path);
            }
            $result[] = $crumbs['product'];
            $this->_crumbs = $result;
        } else {
            return;
        }
    }

    protected function createBreadcrumbsByCategoryId($id)
    {
        $objectManager = ObjectManager::getInstance();
        $result = [
            'first' => null,
            'last' => null,
            'readonly' => null
        ];
        $category = $this->_categoryFactory->create()->load($id);
        $result['link'] = $category->getUrl();
        $label = $objectManager->create('Magento\Framework\Phrase', ['text' => $category->getName()]);
        $result['label'] = $label;
        $result['title'] = $objectManager->create('Magento\Framework\Phrase', ['text' => "Go to " . $category->getName() . " category."]);

        return $result;
    }
}
