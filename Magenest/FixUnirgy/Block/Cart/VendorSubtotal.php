<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Block\Cart;

use Magento\Framework\View\Element\Template;

class VendorSubtotal extends Template
{
    protected $_template = 'Magenest_FixUnirgy::cart/subtotal.phtml';

    /**
     * @var $_storeInterface \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeInterface;

    public function __construct(
        Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->_storeInterface = $storeManager;
        parent::__construct($context, $data);
    }

    public function needShipping()
    {
        $items = $this->getVendorItems();
        foreach($items as $item) {
            if ($item->getIsVirtual() === "0" || $item->getIsVirtual() == 0) {
                return 'yes';
            }
        }

        return 'no';
    }

    public function getVendorItems()
    {
        return $this->getItems();
    }

    public function getBaseTotalExclTax()
    {
        $baseTotal = 0;
        $items = $this->getVendorItems();
        foreach($items as $item) {
            $baseTotal += (float)$item->getRowTotal();
        }

        return $baseTotal;
    }

    public function getBaseTotalTax()
    {
        $baseTotal = 0;
        $items = $this->getVendorItems();
        foreach($items as $item) {
            $baseTotal += (float)$item->getTaxAmount();
        }

        return $baseTotal;
    }

    public function getCurrentCurrencyCode()
    {
        return $this->_storeInterface->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    public function getCurrentCurrencySymbol()
    {
        return $this->_storeInterface->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    public function isValid()
    {
        return $this->getVendors() && $this->getVendorItems();
    }

    public function getVendors()
    {
        return $this->getVendor();
    }

    public function getVendorId()
    {
        return $this->getVendors()->getId();
    }
}
