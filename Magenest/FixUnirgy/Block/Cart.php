<?php

namespace Magenest\FixUnirgy\Block;

use Magento\Framework\View\Element\Template;
use Magento\Checkout\Model\Session;

class Cart extends Template
{
    public function getText()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->get(\Magento\Checkout\Model\Session::class)->getQuote();
        $items = $quote->getAllVisibleItems();
        $number = count($items);
        if ($number > 1)
            return $number . " items";
        return $number . " item";
    }
}
