<?php

namespace Magenest\FixUnirgy\Block\Vendor\Product\Renderer;

use Magenest\Groupon\Helper\Data;
use Unirgy\DropshipVendorProduct\Block\Vendor\Product\Renderer\QuickCreate as UnirgyQuickCreate;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Framework\Data\Form as DataForm;
use Magento\Framework\Data\Form\AbstractForm;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipVendorProduct\Helper\Data as HelperData;
use Unirgy\DropshipVendorProduct\Helper\Form;
use Unirgy\DropshipVendorProduct\Model\Source;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\Dropship\Model\Source as DropshipModelSource;

class QuickCreate extends UnirgyQuickCreate
{
    public function render(AbstractElement $element)
    {
        $isRegister = ObjectManager::getInstance()->get('Magento\Framework\Registry')->registry('is_edit_groupon_options');
        if (!isset($isRegister) && Data::isDeal($this->getProduct()->getId()) && $this->getRequest()->getFullActionName() === 'udprod_vendor_productEdit') {
            ObjectManager::getInstance()->get('Magento\Framework\Registry')->register('is_edit_groupon_options', true);
        }
        $this->setElement($element);
        $html = $this->toHtml();
        return $html;
    }

    protected $_columnsForm;
    public function getColumnsForm()
    {
        if (null !== $this->_columnsForm) {
            return $this->_columnsForm;
        }
        $htmlId = $this->_element->getId();
        $prod = $this->getProduct();
        $hideFields = $this->_helperData->getHideEditFields();
        $skipInputType = ['media_image'];
        if ('configurable' == $prod->getTypeId()) {
            $skipInputType[] = 'gallery';
        }
        $attributes = $this->_helperData->getQuickCreateAttributes();
        $fsIdx = 0;
        /** @var \Magento\Framework\Data\FormFactory $formFactory */
        $formFactory = $this->_hlp->getObj('\Magento\Framework\Data\FormFactory');
        $this->_columnsForm = $formFactory->create();
        $columnsConfig = $this->_scopeConfig->getValue('udprod/quick_create_layout/columns', ScopeInterface::SCOPE_STORE);
        ObjectManager::getInstance()->get('Magento\Framework\Registry')->register('is_create_groupon_options', $this->getRequest()->getFullActionName());
        if (!is_array($columnsConfig)) {
            $columnsConfig = $this->_hlp->unserialize($columnsConfig);
            if (is_array($columnsConfig)) {
            foreach ($columnsConfig as $fsConfig) {
            if (is_array($fsConfig)) {
                $fields = [];
                foreach (['columns'] as $colKey) {
                if (isset($fsConfig[$colKey]) && is_array($fsConfig[$colKey])) {
                    $requiredFields = (array)@$fsConfig['required_fields'];
                    foreach ($fsConfig[$colKey] as $fieldCode) {
                        if (!$this->_isFieldApplicable($prod, $fieldCode, $fsConfig)) continue;
                        $field = [];
                        if (strpos($fieldCode, 'product.') === 0
                            && !in_array(substr($fieldCode, 8), $hideFields)
                            && isset($attributes[substr($fieldCode, 8)])
                            && $this->isMyProduct()
                        ) {
                            $field = $this->_getAttributeField($attributes[substr($fieldCode, 8)]);
                        } elseif (strpos($fieldCode, 'udmulti.') === 0) {
                            $field = $this->_getUdmultiField(substr($fieldCode, 8), []);
                        } elseif (strpos($fieldCode, 'stock_data.') === 0) {
                            $field = $this->_getStockItemField(substr($fieldCode, 11), []);
                        }
                        if (!empty($field) && !in_array($field['type'], $skipInputType)) {
                            if (in_array($fieldCode, $requiredFields)) {
                                $field['required'] = true;
                            } else {
                                $field['required'] = false;
                                if (!empty($field['class'])) {
                                    $field['class'] = str_replace('required-entry', '', $field['class']);
                                }
                            }
                            $field['value'] = $this->prepareIdSuffix('$'.strtoupper($field['name']));
                            $field['id'] = $this->prepareIdSuffix($this->_columnsForm->addSuffixToName(
                                $field['name'],
                                $this->_element->getName().'[$ROW]'
                            ));
                            if (isset($field['class'])) {
                                $field['class'] = str_replace(
                                    'udmulti_special_date',
                                    $this->prepareIdSuffix($this->_columnsForm->addSuffixToName(
                                        'udmulti_special_date',
                                        'udsell_cfgsell[$ROW]'
                                    )),
                                    $field['class']
                                );
                            }
                            $fields[] = $field;
                        }
                    }
                }}

                if (!empty($fields)) {
                    $fsIdx++;
                    $fieldset = $this->_columnsForm->addFieldset('group_fields'.$fsIdx,
                        [
                            'legend'=>$fsConfig['title'],
                            'class'=>'fieldset-wide',
                    ]);
                    $this->_addElementTypes($fieldset);
                    foreach ($fields as $field) {
                        if (!empty($field['input_renderer']) && !$this->_hasCustomInputType($field['type'])) {
                            $fieldset->addType($field['type'], $field['input_renderer']);
                        }
                        $formField = $fieldset->addField($field['id'], $field['type'], $field);
                        if (!empty($field['renderer'])) {
                            $formField->setRenderer($field['renderer']);
                        }
                    }
                    $this->_prepareFieldsetColumns($fieldset);
                    $emptyForm = false;
                }
            }}}
        }
        $this->_columnsForm->setDataObject($prod);
        $this->_columnsForm->addFieldNameSuffix($this->_element->getName().'[$ROW]');
        ObjectManager::getInstance()->get('Magento\Framework\Registry')->unregister('is_create_groupon_options');
        ObjectManager::getInstance()->get('Magento\Framework\Registry')->unregister('is_edit_groupon_options');
        return $this->_columnsForm;
    }

    public function getChildrenProductData($productId)
    {
        $objectManager = ObjectManager::getInstance();
        $collection = $objectManager->create('Magenest\Groupon\Model\Deal')->getCollection();
        $childsData = $collection->addFieldToFilter('parent_id', $productId)->getData();
        $result = [];
        foreach($childsData as $child) {
            $childProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($child['product_id']);
            $childId = $childProduct->getGrouponOption();
            $childQty = $childProduct->getQuantityAndStockStatus();
            $child['groupon_option'] = $childId;
            $child['groupon_qty'] = $childQty['qty'];
            $result[$childId] = $child;
        }

        return $result;
    }
}
