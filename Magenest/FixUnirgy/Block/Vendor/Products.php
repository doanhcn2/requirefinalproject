<?php

namespace Magenest\FixUnirgy\Block\Vendor;

use Magento\CatalogInventory\Model\Stock;
use Magento\CatalogInventory\Model\StockFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection as SetCollection;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\App;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;

class Products extends \Unirgy\DropshipVendorProduct\Block\Vendor\Products
{
    public function getProductCollection()
    {
        if (!$this->_collection) {
            $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
            if (!$v || !$v->getId()) {
                return [];
            }
            $r = $this->_request;
            $res = $this->_hlp->rHlp();
            #$read = $res->getConnection('catalog_product');
            $stockTable = $res->getTableName('cataloginventory_stock_item');
            $stockStatusTable = $res->getTableName('cataloginventory_stock_status');
            $wId = (int)$this->_storeManager->getDefaultStoreView()->getWebsiteId();
            if ($this->_hlp->hasMageFeature('stock_website')) {
                $wId = 0;
            }
            $collection = $this->_hlp->createObj('\Unirgy\Dropship\Model\ResourceModel\ProductCollection')
                ->setFlag('udskip_price_index',1)
                ->setFlag('has_group_entity', 1)
                ->setFlag('has_stock_status_filter', 1)
                ->addAttributeToFilter('type_id', ['in'=>['simple','configurable','downloadable','virtual','coupon','ticket']])
                ->addAttributeToSelect(['sku', 'name', 'status', 'price'])
            ;
            $collection->addAttributeToFilter('entity_id', ['in'=>$v->getAssociatedProductIds()]);
            $collection->addAttributeToFilter('visibility', ['in'=>$this->_productVisibility->getVisibleInSiteIds()]);
            $conn = $collection->getConnection();
            $wIdsSql = $conn->quote([$wId]);
            //$collection->addAttributeToFilter('entity_id', array('in'=>array_keys($v->getAssociatedProducts())));
            $collection->getSelect()
                ->join(
                ['cisi' => $stockTable],
                $conn->quoteInto('cisi.product_id=e.entity_id AND cisi.stock_id=?', Stock::DEFAULT_STOCK_ID),
                    []
                )
                ->joinLeft(
                    ['ciss' => $stockStatusTable],
                    $conn->quoteInto('ciss.product_id=e.entity_id AND ciss.website_id in ('.$wIdsSql.') AND ciss.stock_id=?', Stock::DEFAULT_STOCK_ID),
                ['_stock_status'=>$this->_getStockField('status')]
            );
            if ($this->_hlp->isUdmultiAvailable()) {
                $collection->getSelect()->joinLeft(
                    ['uvp' => $res->getTableName('udropship_vendor_product')],
                    $conn->quoteInto('uvp.product_id=e.entity_id AND uvp.vendor_id=?', $v->getId()),
                    ['_stock_qty'=>$this->_getStockField('qty'), 'vendor_sku'=>'uvp.vendor_sku', 'vendor_cost'=>'uvp.vendor_cost']
                );
                //$collection->getSelect()->columns(array('_stock_qty'=>'IFNULL(uvp.stock_qty,cisi.qty'));
            } else {
                if (($vsAttrCode = $this->_scopeConfig->getValue('udropship/vendor/vendor_sku_attribute', ScopeInterface::SCOPE_STORE)) && $this->_hlp->checkProductAttribute($vsAttrCode)) {
                    $collection->addAttributeToSelect([$vsAttrCode]);
                }
                $collection->getSelect()->columns(['_stock_qty'=>$this->_getStockField('qty')]);
            }
            $collection->addAttributeToFilter('udropship_vendor', $v->getId());

            $this->_applyRequestFilters($collection);

            $collection->getSelect()->order('e.entity_id desc');
            $collection->getSelect()->group('e.entity_id');
            $collection->getSize();

            #$this->_modelStockFactory->create()->addItemsToProducts($collection);
            $this->_collection = $collection;
        }
        return $this->_collection;
    }
}
