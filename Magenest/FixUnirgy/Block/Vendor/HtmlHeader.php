<?php

namespace Magenest\FixUnirgy\Block\Vendor;

use \Magento\Framework\View\Element\Template;

class HtmlHeader extends Template
{

    public function getActivePage()
    {
        return $this->getLayout()->getBlock('udropship.header')->getData('active_page');
    }
}