<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 24/05/2018
 * Time: 15:23
 */
namespace Magenest\FixUnirgy\Block\Vendor;
class MembershipOptionsRenderer extends \Unirgy\DropshipVendorMembership\Block\Vendor\MembershipOptionsRenderer{


    protected function _construct()
    {
       $this->setTemplate('Magenest_FixUnirgy::unirgy/udmember/vendor/membership_options.phtml');
    }
}