<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 16/04/2018
 * Time: 13:51
 */
namespace Magenest\FixUnirgy\Block\Order\Rma;
class Items extends \Unirgy\Rma\Block\Order\Rma\Items{

    public function getPrintRmaUrl($rma)
    {
        return $this->_urlBuilder->getUrl('fixunirgy/rma/printPdf', ['rma_id' => $rma->getId()]);
    }
}