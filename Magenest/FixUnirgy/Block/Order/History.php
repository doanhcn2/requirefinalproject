<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Block\Order;

use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use Unirgy\Dropship\Model\Source as ModelSource;

class History extends \Magento\Sales\Block\Order\History
{
    const AWAITING_PAYMENT = 1;
    const AWAITING_SHIPMENT = 2;
    const AWAITING_DELIVERY = 3;
    const PRODUCT_RATING_ENTITY = 1;

    protected $_stateStatuses = [
        Order::STATE_NEW,
        Order::STATE_PROCESSING,
        Order::STATE_COMPLETE,
        Order::STATE_CLOSED,
        Order::STATE_CANCELED,
        Order::STATE_HOLDED,
    ];

    /**
     * @var AbstractProduct
     */
    protected $_absProduct;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_imageGallery Gallery
     */
    protected $_imageGallery;

    /**
     * @var \Unirgy\DropshipMicrosite\Helper\Data $_micrositeHelper
     */
    protected $_micrositeHelper;

    /**
     * @var CollectionFactoryInterface
     */
    private $orderCollectionFactory;

    /**
     * @var $_saleOrderItemsCollection \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory
     */
    protected $_saleOrderItemsCollection;

    /**
     * @var $_poFactory \Unirgy\DropshipPo\Model\PoFactory
     */
    protected $_poFactory;

    /**
     * @var $_reviewFactory \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    protected $_poByOrderId = [];

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        Config $orderConfig,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        AbstractProduct $abstractProduct,
        \Unirgy\DropshipMicrosite\Helper\Data $microSiteHelper,
        Gallery $gallery,
        \Unirgy\DropshipPo\Model\PoFactory $poFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemsCollectionFactory,
        array $data = []
    ) {
        $this->_reviewFactory = $reviewFactory;
        $this->_poFactory = $poFactory;
        $this->_saleOrderItemsCollection = $itemsCollectionFactory;
        $this->_micrositeHelper = $microSiteHelper;
        $this->_imageGallery = $gallery;
        $this->_absProduct = $abstractProduct;
        $this->_vendorFactory = $vendorFactory;
        $this->_productFactory = $productFactory;
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Your Orders'));
    }

    /**
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     * @throws \Exception
     */
    public function getOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->orders) {
            $this->orders = $this->getOrderCollectionFactory()->create($customerId)->addFieldToSelect(
                '*'
            );
            if ($this->getRequest()->getParam('status') && $this->getRequest()->getParam('status') !== 'all_order') {
                $this->orders->addFieldToFilter(
                    'state',
                    $this->getRequest()->getParam('status')
                );
            } else {
                $this->orders->addFieldToFilter(
                    'status',
                    ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
                );
            }
            if ($this->getRequest()->getParam('date') && $this->getRequest()->getParam('date') !== 'all_time') {
                $date = $this->getRequest()->getParam('date');
                $today = new \DateTime();
                if ($date === '7_days') {
                    $today->sub(new \DateInterval('P7D'));
                    $today = $today->format('Y-m-d h:m:s');
                    $this->orders->addFieldToFilter('created_at', ['gt' => $today]);
                } elseif ($date === '1_month') {
                    $today->sub(new \DateInterval('P1M'));
                    $today = $today->format('Y-m-d h:m:s');
                    $this->orders->addFieldToFilter('created_at', ['gt' => $today]);
                } elseif ($date === '3_months') {
                    $today->sub(new \DateInterval('P3M'));
                    $today = $today->format('Y-m-d h:m:s');
                    $this->orders->addFieldToFilter('created_at', ['gt' => $today]);
                }
            }
            if ($this->getRequest()->getParam('key_word')) {
                $keyWord = $this->getRequest()->getParam('key_word');
                $orderIds = $this->searchOrderIdsByName($keyWord, $customerId);
                $this->orders->addFieldToFilter('entity_id', ['in' => $orderIds]);
            }
            $this->orders->setOrder(
                'created_at',
                'desc'
            );
        }

        return $this->orders;
    }

    private function getOrderCollectionFactory()
    {
        if ($this->orderCollectionFactory === null) {
            $this->orderCollectionFactory = ObjectManager::getInstance()->get(CollectionFactoryInterface::class);
        }

        return $this->orderCollectionFactory;
    }

    public function getOrderItemProductInfo($productId)
    {
        $product = $this->_productFactory->create()->load($productId);
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        if ($product->getThumbnail()) {
            $_imageUrl = $this->getBaseMediaDirPath() . 'catalog/product' . DIRECTORY_SEPARATOR;
            if (file_exists($_imageUrl . $product->getThumbnail()))
                $productImage = $mediaUrl . 'catalog/product' . $product->getThumbnail();
            else {
                $imageHelper = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Catalog\Helper\Image::class);
                $productImage = $imageHelper->getDefaultPlaceholderUrl('thumbnail');
            }
        } else {
            $productImage = $this->getDefaultPlaceholderImageUrl();
        }
        if ($product->getTypeId() === 'simple' && $product->getBrand()) {
            $productBrand = $this->getAttributeOptionText($product, $product->getBrand());
        } else {
            $productBrand = "";
        }

        return ['image' => $productImage, 'brand' => $productBrand];
    }

    public function getBaseMediaDirPath()
    {
        return $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . '/catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    private function getAttributeOptionText($product, $attributeOptionId)
    {
        $attributeId = $product->getResource()->getAttribute('brand');
        if ($attributeId->usesSource()) {
            $data = $attributeId->getSource()->getAttribute()->getData();
            if ($attributeOptionId) {
                $optionText = $attributeId->getSource()->getOptionText($attributeOptionId);
            } else {
                $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
            }
        }

        return @$optionText;
    }

    public function getOrderItemVendorInfo($vendorId)
    {
        $vendor = $this->_vendorFactory->create()->load($vendorId);
        if ($url = $this->_micrositeHelper->getVendorBaseUrl($vendor)) {
            return ['link' => $url, 'name' => $vendor->getVendorName()];
        } else {
            return ['link' => "javascript:void(0)", 'name' => $vendor->getVendorName()];
        }
    }

    public function getOrderStatus($status)
    {
        return 'Awaiting Payment';
    }

    public function getOrderStates()
    {
        $result = [];
        foreach($this->_stateStatuses as $status) {
            $result[$status] = $this->_orderConfig->getStateLabel($status);
        }

        return $result;
    }

    public function getOrderFilterUrl($status)
    {
        return "javascript:void(0)";
    }

    public function getNumberOfStatusOrder($status)
    {
        return 0;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol = $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    public function getCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    private function searchOrderIdsByName($keyWord, $customerId)
    {
        $key = "%" . $keyWord . "%";
        $collection = $this->_saleOrderItemsCollection->create();
        $collection->getSelect()->joinLeft(
            $collection->getTable('sales_order'),
            "main_table.order_id = sales_order.entity_id",
            "sales_order.customer_id"
        );
        $collection->addFieldToFilter('sales_order.customer_id', $customerId);
        $collection->addFieldToFilter('main_table.name', ['like' => $key]);
        $orderIds = $collection->getColumnValues('order_id');

        return array_unique($orderIds);
    }

    /**
     * Check if item has shipment and not review yet
     * return array of product id can be review
     *
     * @param $orderId
     * @return array
     */
    public function itemHasReview($orderId)
    {
        $allShippedItems = [];
        $allShipmentIds = [];
        $poItems = $this->getPoItemsByOrderId($orderId);
        foreach($poItems as $po) {
            /**
             * @var $po \Unirgy\DropshipPo\Model\Po
             */
            if (!$po->hasShippedItem()) continue;
            $shipments = $po->getShipments();
            foreach($shipments as $shipment) {
                if ($shipment->getUdropshipStatus() != ModelSource::SHIPMENT_STATUS_DELIVERED) continue;
                $shipmentItems = $shipment->getAllItems();
                array_push($allShipmentIds, $shipment->getId());
                foreach($shipmentItems as $item) {
                    array_push($allShippedItems, $item->getProductId());
                }
            }
        }
        $reviewByOrder = $this->_reviewFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', self::PRODUCT_RATING_ENTITY)
            ->addFieldToFilter('rel_entity_pk_value', ['in' => $allShipmentIds])->getColumnValues('entity_pk_value');

        return array_diff($allShippedItems, $reviewByOrder);
    }

    public function getProductShipmentId($productId, $orderId)
    {
        $poItems = $this->getPoItemsByOrderId($orderId);
        foreach($poItems as $po) {
            /**
             * @var $po \Unirgy\DropshipPo\Model\Po
             */
            if (!$po->hasShippedItem()) continue;
            $shipments = $po->getShipments();
            foreach($shipments as $shipment) {
                if ($shipment->getUdropshipStatus() != ModelSource::SHIPMENT_STATUS_DELIVERED) continue;
                foreach($shipment->getAllItems() as $item) {
                    if ($productId == $item->getProductId()) {
                        return $shipment->getId();
                    }
                }
            }
        }

        return null;
    }

    protected function getPoItemsByOrderId($orderId)
    {
        if (!isset($this->_poByOrderId[$orderId])) {
            $poCollection = $this->_poFactory->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)->getItems();
            $this->_poByOrderId[$orderId] = $poCollection;
        }

        return $this->_poByOrderId[$orderId];
    }
}
