<?php

namespace Magenest\FixUnirgy\Block\Product;

use \Magento\Framework\View\Element\Template\Context;
use \Magento\Catalog\Block\Product\View\Description;
use \Magento\Framework\Registry;
use \Magenest\Groupon\Model\DealFactory;
use \Magenest\Groupon\Model\LocationFactory;

class Service extends Description
{
    private $_dealFactory;

    private $_locationFactory;

    /**
     * @var $_ticketLocationFactory \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory
     */
    protected $_ticketLocationFactory;

    /**
     * Service constructor.
     * @param Context $context
     * @param Registry $registry
     * @param DealFactory $dealFactory
     * @param LocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct
    (
        Context $context,
        Registry $registry,
        DealFactory $dealFactory,
        LocationFactory $locationFactory,
        \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $collectionFactory,
        array $data = []
    )
    {
        $this->_ticketLocationFactory = $collectionFactory;
        $this->_dealFactory = $dealFactory;
        $this->_locationFactory = $locationFactory;
        parent::__construct($context, $registry, $data);
    }

    public function getDealDetails()
    {
        $product = $this->getProduct();
        $productType = $product->getTypeId();
        if ($productType == 'ticket') {
            $locationIds = $product->getId();

            return $this->getLocationsDetails(@$locationIds);
        }
        $dealCollection = $this->_dealFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $product->getId());
        $locationIds = $dealCollection->getFirstItem()->getLocationIds();
        $locationIds = array_unique(explode(',', $locationIds));

        return $this->getLocationsDetails(@$locationIds);
    }

    public function getLocationsDetails($locationIds)
    {
        $product = $this->getProduct();
        $productType = $product->getTypeId();
        if ($productType == 'ticket') {
            return $this->_ticketLocationFactory->create()->addFieldToFilter('product_id', $product->getId())->getFirstItem();
        }
        $locationCollection = $this->_locationFactory->create()->getCollection()
            ->addFieldToFilter('location_id', ['IN' => $locationIds]);
        return $locationCollection;
    }

    public function convertTime($time)
    {
        $hour = (int)substr($time, 0, 2);
        $period = "AM";
        if ($hour > 12) {
            $hour -= 12;
            $period = "PM";
        }
        return $hour.substr($time, 2).' '.$period;
    }

    public function getArtifact($location)
    {
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $artifactUrl = $baseMediaUrl.'upload/vendor_'.$location->getVendorId().'/';
        return $artifactUrl;
    }
}