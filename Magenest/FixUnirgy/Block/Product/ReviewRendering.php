<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Block\Product;

use Magento\Catalog\Model\Product;

class ReviewRendering extends \Magento\Review\Block\Product\ReviewRenderer
{
    /**
     * @var $_ratingHelper \Magenest\SellYours\Helper\RatingHelper
     */
    protected $_ratingHelper;

    /**
     * Array of available template name
     *
     * @var array
     */
    protected $_availableTemplates = [
        self::FULL_VIEW => 'Magento_Review::helper/summary.phtml',
        self::SHORT_VIEW => 'Magento_Review::helper/summary_short.phtml',
    ];

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Review\Model\ReviewFactory $reviewFactory,
                                \Magenest\SellYours\Helper\RatingHelper $ratingHelper,
                                array $data = [])
    {
        $this->_ratingHelper = $ratingHelper;
        parent::__construct($context, $reviewFactory, $data);
    }

    /**
     * Get ratings summary
     *
     * @return string
     */
    public function getRatingSummary()
    {
        $collection = $this->getRatingCollection();
        return $this->_ratingHelper->getAvgRate($collection->getColumnValues('review_id'));
    }

    /**
     * Get count of reviews
     *
     * @return int
     */
    public function getReviewsCount()
    {
        return $this->getRatingCollection()->getSize();
    }

    public function getRatingCollection()
    {
        $productId = $this->getProduct()->getEntityId();
        return $this->_ratingHelper->getReviewCollection($productId);
    }

    public function detectDeviceToShow()
    {
        return true;
    }

    public function renderWidgetShortcode()
    {
        return;
    }
}
