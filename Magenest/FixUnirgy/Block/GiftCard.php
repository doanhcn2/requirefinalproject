<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\FixUnirgy\Block;

use Magento\Framework\Pricing\PriceCurrencyInterface;

class GiftCard extends \Magento\GiftCard\Block\Catalog\Product\View\Type\Giftcard
{
    /**
     * @var array
     */
    protected $minMaxCache = [];

    /**
     * @var array
     */
    protected $amountsCache = [];

    /**
     * GiftCard constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\Customer\Model\Session $customerSession
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Customer\Model\Session $customerSession,
        PriceCurrencyInterface $priceCurrency, array $data = []
    ) {
        parent::__construct($context, $arrayUtils, $customerSession, $priceCurrency, $data);
    }

    public function getProductId()
    {
        return parent::getProduct()->getEntityId();
    }

    public function isGiftCardProductType()
    {
        return parent::getProduct()->getTypeId() === "giftcard";
    }

    /**
     * @return bool
     */
    public function isRegularPrice()
    {
        return !$this->isOpenAmountAvailable($this->getProduct()) && (count($this->getAmounts($this->getProduct())) === 1);
    }

    /**
     * @param float $amount
     * @param bool $includeContainer
     * @return string
     */
    public function convertAndFormatCurrency($amount, $includeContainer = true)
    {
        return $this->priceCurrency->convertAndFormat($amount, $includeContainer);
    }

    /**
     * @param float $amount
     * @return float
     */
    public function convertCurrency($amount)
    {
        return $this->priceCurrency->convert($amount);
    }

    /**
     * @return float|null
     */
    public function getOpenAmountMin()
    {
        return $this->getProduct()->getOpenAmountMin();
    }

    /**
     * @return float|null
     */
    public function getOpenAmountMax()
    {
        return $this->getProduct()->getOpenAmountMax();
    }

    /**
     * @return bool|float
     */
    public function getRegularPrice()
    {
        $amount = $this->getAmounts($this->getProduct());
        return count($amount) === 1 ? array_shift($amount) : false;
    }
}
