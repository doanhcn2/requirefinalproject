<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Setup;


use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Upgrade Data script
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\CollectionFactory
     */
    private $questionCollectionFactory;

    /**
     * @var \Magento\Sales\Model\Order\ShipmentRepository
     */
    private $shipmentRepository;

    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    /**
     * UpgradeData constructor.
     *
     * @param \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\CollectionFactory $questionCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository
     */
    public function __construct(
        \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\CollectionFactory $questionCollectionFactory,
        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository
    ) {
        $this->questionCollectionFactory = $questionCollectionFactory;
        $this->shipmentRepository        = $shipmentRepository;
        $this->state                     = $state;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $this->insertReviewEntity($setup, $context);
        }

        $setup->startSetup();
//        if (version_compare($context->getVersion(), '1.0.4') < 0) {
//            $connection = $setup->getConnection();
//            $tableName  = $setup->getTable('magenest_groupon_deal');
//            $sql        = "UPDATE  {$tableName} SET groupon_expire=(@temp:=groupon_expire), groupon_expire = reminder_before_day, reminder_before_day = @temp";
//            $connection->query($sql);
//            $setup->endSetup();
//        }

        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $this->state->setAreaCode('adminhtml');
            /** @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection $qaCollection */
            $qaCollection = $this->questionCollectionFactory->create();
            $qaCollection->getSelect()->where('shipment_id IS NOT NULL');
            /** @var \Unirgy\DropshipVendorAskQuestion\Model\Question $qa */
            foreach ($qaCollection as $qa) {
                try {
                    $shipment = $this->shipmentRepository->get($qa->getShipmentId());
                    $qa->setMagentoOrderId($shipment->getOrderId());
                    $qa->save();
                } catch (\Exception $exception) {
                }
            }
        }

        if (version_compare($context->getVersion(), '1.0.10') < 0) {
            $this->updateUrlRewrite();
        }
    }

    private function updateUrlRewrite(){
        /** @var \Magento\UrlRewrite\Model\UrlRewrite $urlModel */
        $urlModel = ObjectManager::getInstance()->create(\Magento\UrlRewrite\Model\UrlRewrite::class);
        /** @var \Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection $collection */
        $collection = $urlModel->getCollection();
        $collection->addFieldToFilter('request_path','register-vendor-getstarted');
        if (is_array($collection->getItems())){
            /** @var \Magento\UrlRewrite\Model\UrlRewrite $item */
            foreach ($collection->getItems() as $item){
                $item->setRequestPath('merchant');
                $item->save();
            }
        }
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Exception
     */
    private function insertReviewEntity(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $conn      = $setup->getConnection();

        $myEt = \Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID;

        if (($row = $installer->getTableRow('rating_entity', 'entity_id', $myEt))) {
            if ($row['entity_code'] != 'vprating_vendor') {
                throw new \Exception(__("entity_id=%s is already used in rating_entity. Change it in %s.",
                    $myEt, '\Magenest\FixUnirgy\Helper\Review\Data::$_myEt'
                ));
            }
        } else {
            $conn->insert($installer->getTable('rating_entity'), array('entity_id' => $myEt, 'entity_code' => 'vprating_vendor'));
        }
        if (($row = $installer->getTableRow('review_entity', 'entity_id', $myEt))) {
            if ($row['entity_code'] != 'vprating_vendor') {
                throw new \Exception(__("entity_id=%s is already used in rating_entity. Change it in %s.",
                    $myEt, '\Magenest\FixUnirgy\Helper\Review\Data::$_myEt'
                ));
            }
        } else {
            $conn->insert($installer->getTable('review_entity'), array('entity_id' => $myEt, 'entity_code' => 'vprating_vendor'));
        }
    }
}
