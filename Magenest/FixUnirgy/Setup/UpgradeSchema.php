<?php

namespace Magenest\FixUnirgy\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface

{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            // Get module table
            $tableName = $setup->getTable('udropship_vendor');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName)) {
                // Declare data
                $columns = [
                    'new_reg' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'default' => 1,
                        'comment' => 'New Register'
                    ],
                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            // Get module table
            $tableName = $setup->getTable('udropship_vendor');
            $setup->getConnection()->dropColumn($tableName, "new_reg");

            $setup->endSetup();
            $setup->startSetup();

            if ($setup->getConnection()->isTableExists($tableName)) {
                // Declare data
                $columns = [
                    'new_reg' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'nullable' => true,
                        'default' => 0,
                        'comment' => 'New Register'
                    ],
                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }

            $setup->endSetup();
            $setup->startSetup();

            $connection->modifyColumn(
                $tableName,
                'new_reg',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'New Register'

                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            // Get module table
            $tableName = $setup->getTable('magenest_expired_product_ids');
            $table = $setup->getConnection()->newTable(
                $setup->getTable($tableName)
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )->addColumn(
                'product_ids',
                Table::TYPE_TEXT,
                Table::MAX_TEXT_SIZE,
                ['nullable' => false,],
                'Product Ids'
            )->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated At'
            )->setComment(
                'Expired Product IDs Table'
            );

            $setup->getConnection()->createTable($table);
        }
        if(version_compare($context->getVersion(),'1.0.6') < 0){
            $this->createRateInfoTable($setup);
        }
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $table = $setup->getTable('udropship_vendor_product');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'handling_time',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Vendor Handling Time',
                    'nullable' => true
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $table = $setup->getTable('udropship_vendor_product');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'offer_note',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Offer Note',
                    'nullable' => true
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $table      = $setup->getTable('udqa_question');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'magento_order_id',
                [
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment'  => 'Magento Order Id',
                    'nullable' => true,
                    'unsigned' => true,
                    'after'    => 'shipment_id'
                ]);
        }

        $setup->endSetup();
    }

    public function createRateInfoTable(SchemaSetupInterface $setup){
        $tableName = $setup->getTable('magenest_rating_info');
        $table = $setup->getConnection()->newTable($tableName)
        ->addColumn('id',
          Table::TYPE_INTEGER,
          null,
          [
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
          ])->addColumn('rating_id',
            Table::TYPE_INTEGER,
            null,
            [
              'unsigned' => true,
              'nullable' => false
            ])->addColumn('category_ids',
            Table::TYPE_TEXT,
            null,
            [
              'default' => '',
              'nullable' => true
            ]);
        $setup->getConnection()->createTable($table);
    }
}