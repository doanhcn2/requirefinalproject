<?php

namespace Magenest\FixUnirgy\Model;

use Magento\Framework\Model\AbstractModel;

class PoGrid extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\FixUnirgy\Model\ResourceModel\PoGrid');
    }
}
