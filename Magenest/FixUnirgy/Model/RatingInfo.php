<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 10:01
 */
namespace Magenest\FixUnirgy\Model;
use Magento\Framework\Model\AbstractModel;

class RatingInfo extends AbstractModel{

    public function __construct(
      \Magento\Framework\Model\Context $context,
      \Magento\Framework\Registry $registry,
      \Magenest\FixUnirgy\Model\ResourceModel\RatingInfo $resource,
      \Magenest\FixUnirgy\Model\ResourceModel\RatingInfo\Collection $resourceCollection,
      array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection,
          $data);
    }
}