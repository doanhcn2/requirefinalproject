<?php
namespace Magenest\FixUnirgy\Model;

class DefaultConfigProvider extends \Unirgy\DropshipSplit\Model\DefaultConfigProvider
{
    public function getConfig()
    {
        $output = parent::getConfig();
        $quote = $this->myCoSession->getQuote();
        $shippingMethodByVendor = [];
        $udropshipVendors = $udropshipVendorIds = $udropshipVendorInfo = [];
        if ($quote->getId()) {
            $shippingAddress = $quote->getShippingAddress();
            $methods = array();
            $details = $shippingAddress->getUdropshipShippingDetails();
            if ($details) {
                $details = $this->_hlp->unserializeArr($details);
                $methods = isset($details['methods']) ? $details['methods'] : array();
            }
            /*
            foreach ($methods as $vId => $method) {
                $shippingMethodByVendor[$vId] = $method['code'];
            }
            */
            $shippingMethodByVendor = $methods;
            $items = $shippingAddress->getAllItems();
            $hlpPr = $this->_hlp->getObj('\Unirgy\Dropship\Helper\ProtectedCode');
            try {
                $hlpPr->prepareQuoteItems($items);
                $hlpPr->fixQuoteItemsWeight($items);
            } catch (\Exception $e) {
                $this->_hlp->addMessageOnce($e->getMessage());
                return [];
            }
            foreach ($items as $item) {
                $udropshipVendorInfo[$item->getUdropshipVendor()] = $item->getUdropshipVendor();
                if ($item->getIsVirtual() === "1") {
                    continue;
                }
                $udropshipVendorIds[$item->getUdropshipVendor()] = $item->getUdropshipVendor();
            }
            $udropshipVendorIds = array_values($udropshipVendorIds);
            foreach ($udropshipVendorInfo as $vId) {
                $v = $this->_hlp->getVendor($vId);
                $udropshipVendors[$vId] = array(
                    'name' => $v->getVendorName(),
                    'address' => $v->getFormatedAddress('text_small')
                );
            }
        }
        $output['selectedShippingMethodByVendor'] = $shippingMethodByVendor;
        $output['udropshipVendorIds'] = $udropshipVendorIds;
        $output['udropshipVendors'] = $udropshipVendors;
        return $output;
    }
}
