<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\FixUnirgy\Model;

use Magento\Framework\Model\AbstractModel;

class ExpiredProduct extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct');
    }
}
