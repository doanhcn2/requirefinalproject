<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 17/04/2018
 * Time: 08:05
 */
namespace Magenest\FixUnirgy\Model\Pdf\RmaItems;
class DefaultRmaItems extends \Unirgy\Rma\Model\Pdf\RmaItems\DefaultRmaItems{

    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $lines  = [];

        // draw Product name
        $lines[0] = [[
          'text' => $this->_strHlp->split($item->getName(), 60, true, true),
          'feed' => 35,
        ]];

        // draw SKU
        $lines[0][] = [
          'text'  => $this->_strHlp->split($this->getSku($item), 25),
          'feed'  => 255
        ];

        // draw Item Condition Name
        $lines[0][] = [
          'text'  => $item->getItemConditionName(),
          'feed'  => 410,
          'align' => 'right'
        ];

        // draw QTY
        $lines[0][] = [
          'text'  => $item->getQty()*1,
          'feed'  => 475
        ];

        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = [
                  'text' => $this->_strHlp->split(strip_tags($option['label']), 70, true, true),
                  'font' => 'italic',
                  'feed' => 35
                ];

                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        $lines[][] = [
                          'text' => $this->_strHlp->split($value, 50, true, true),
                          'feed' => 40
                        ];
                    }
                }
            }
        }

        $lineBlock = [
          'lines'  => $lines,
          'height' => 10
        ];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }
}