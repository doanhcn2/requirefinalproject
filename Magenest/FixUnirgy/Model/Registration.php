<?php

namespace Magenest\FixUnirgy\Model;

use Unirgy\DropshipMicrositePro\Model\Registration as ModelRegistration;

class Registration extends ModelRegistration
{
    public function validate()
    {
        $hlp = $this->_msHlp;
        $dhlp = $this->_hlp;
        $hasBillingCountry = in_array('billing_country_id', array_keys($this->getRegFields()));
        extract($this->getData());

        $hasPasswordField = false;
        foreach($this->getRegFields() as $rf) {
            $rfName = str_replace('[]', '', $rf['name']);
            if (!empty($rf['required'])
                && in_array($this->getData($rfName), ['', null], true)
                && !in_array($rf['type'], ['image', 'file'])
                && !in_array($rfName, ['payout_paypal_email'])
                && !in_array($rfName, ['country_id', 'region_id', 'zip', 'city', 'street1'])
            ) {
                if ($rfName != 'region_id' || !$this->getData('region')) {
                    throw new \Exception(__('Incomplete form data'));
                }
            }
            $hasPasswordField = $hasPasswordField || in_array($rfName, ['password_confirm', 'password']);
            if ($rfName == 'password_confirm'
                && $this->getData('password') != $this->getData('password_confirm')
            ) {
                throw new \Exception(__('Passwords do not match'));
            }
        }

        $this->setStreet(@$street1 . "\n" . @$street2);
        if ($hasBillingCountry) {
            $this->setBillingStreet(@$billing_street1 . "\n" . @$billing_street2);
        }
        $this->initPassword(@$password);
        $this->initUrlKey(@$url_key);
        $this->setRemoteIp($_SERVER['REMOTE_ADDR']);
        $this->setRegisteredAt($this->_hlp->now());
        $this->setStoreId($this->_storeManager->getStore()->getId());
        $dhlp->processCustomVars($this);
        $this->attachLabelVars();

        return $this;
    }
}