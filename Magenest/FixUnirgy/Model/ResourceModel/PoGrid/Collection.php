<?php

namespace Magenest\FixUnirgy\Model\ResourceModel\PoGrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init(
            'Magenest\FixUnirgy\Model\PoGrid',
            'Magenest\FixUnirgy\Model\ResourceModel\PoGrid'
        );
    }
}
