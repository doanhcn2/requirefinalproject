<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 08/05/2018
 * Time: 18:50
 */
namespace Magenest\FixUnirgy\Model\ResourceModel\Vendor\Product;
class Collection extends \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\Collection{


    public function getSelectCountSql()
    {
        $groupByArray = $this->getSelect()->getPart(\Magento\Framework\DB\Select::GROUP);
        if(in_array('main_table.vendor_product_id',$groupByArray)&&
          in_array('campain_status_table.value',$groupByArray)&&
          in_array('product_table.updated_at',$groupByArray)&&
          in_array('sold_table.sold',$groupByArray)
        ) {
            $this->_renderFilters();

            $countSelect = clone $this->getSelect();
            $countSelect->reset(\Magento\Framework\DB\Select::ORDER);
            $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
            $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);
            $countSelect->reset(\Magento\Framework\DB\Select::COLUMNS);

            if (!count($this->getSelect()
              ->getPart(\Magento\Framework\DB\Select::GROUP))
            ) {
                $countSelect->columns(new \Zend_Db_Expr('COUNT(*)'));

                return $countSelect;
            }

            $countSelect->reset(\Magento\Framework\DB\Select::GROUP);
            $group = $this->getSelect()
              ->getPart(\Magento\Framework\DB\Select::GROUP);
            $countSelect->columns(new \Zend_Db_Expr(("COUNT(DISTINCT main_table.vendor_product_id)")));

            return $countSelect;
        };
        return parent::getSelectCountSql();
    }
}