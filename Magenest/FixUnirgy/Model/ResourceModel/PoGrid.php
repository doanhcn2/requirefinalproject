<?php

namespace Magenest\FixUnirgy\Model\ResourceModel;

class PoGrid extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('udropship_po_grid', 'entity_id');
    }
}
