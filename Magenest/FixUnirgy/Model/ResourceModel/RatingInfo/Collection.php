<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 10:11
 */
namespace Magenest\FixUnirgy\Model\ResourceModel\RatingInfo;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{

    public function _construct()
    {
        $this->_init('Magenest\FixUnirgy\Model\RatingInfo','Magenest\FixUnirgy\Model\ResourceModel\RatingInfo');
    }
}