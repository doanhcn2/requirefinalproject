<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 10:03
 */
namespace Magenest\FixUnirgy\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RatingInfo extends AbstractDb{

    public function _construct()
    {
        $this->_init($this->getTable('magenest_rating_info'),'id');
    }
}