<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 04/04/2018
 * Time: 15:52
 */

namespace Magenest\FixUnirgy\Cron;

use Magenest\Hotel\Logger\Logger;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Helper\Data as GrouponHelper;

class CheckCampaignStatus
{
    /**
     * @var Logger
     */
    protected $_logger;

    protected $_hotelOrderCollectionFactory;

    protected $_packageDetailsCollectionFactory;

    protected $_productCollectionFactory;

    protected $_ticketsFactory;

    protected $_sessionFactory;

    public function __construct(
        Logger $logger,
        \Magento\Catalog\Model\ProductFactory $productCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageDetails\CollectionFactory $packageDetailsCollectionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory
    ) {
        $this->_logger                          = $logger;
        $this->_productCollectionFactory        = $productCollectionFactory;
        $this->_hotelOrderCollectionFactory     = $hotelOrderCollectionFactory;
        $this->_packageDetailsCollectionFactory = $packageDetailsCollectionFactory;
        $this->_ticketsFactory                  = $ticketsFactory;
        $this->_sessionFactory                  = $sessionFactory;
    }

    public function execute()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create()->getCollection();
        $productCollection
            ->addAttributeToSelect(['id', 'campaign_status', 'type_id'])
            ->addAttributeToFilter('type_id', ['configurable']);
        $productCollection->setFlag('has_stock_status_filter', true);
        /** @var \Magento\Catalog\Model\Product[] $products */
        $products = $productCollection->getData();
        $this->_logger->info("Update campaign status began.");
        foreach ($products as $item) {
            try {
                $product = $this->_productCollectionFactory->create()->load($item['entity_id']);
                if (in_array($product->getCampaignStatus(), CampaignStatus::$approvedState)) {
                    if ($product->getTypeId() == "ticket") {
                        $now = strtotime(date('Y-m-d H:m:s'));
                        $this->checkProductTicket($product, $now);
                    } elseif ($product->getTypeId() == "hotel") {
//                    $now = strtotime(date('Y-m-d'));
                        // @todo: uncomment below line if module Hotel is enabled
//                    $this->checkProductHotel($product);
                    } elseif ($product->getTypeId() == "configurable" && GrouponHelper::isDeal($product->getId())) {
                        GrouponHelper::reload($product);
                    }
                } elseif ($product->getCampaignStatus() == null || empty($product->getCampaignStatus())) {
                    if ($product->getTypeId() == "configurable" && GrouponHelper::isDeal($product->getId())) {

                        $format  = "Y/m/d H:i:s";
                        $groupon = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Groupon\Model\ResourceModel\Deal\Collection')
                            ->addFieldToFilter('product_id', $product->getId())->getFirstItem();
                        $params  = $groupon->getData();

                        $now         = (date($format));
                        $expiredDate = (@$params['groupon_expire']);
                        if (empty($expiredDate) || $expiredDate === null)
                            $expiredDate = $now;
                        else
                            $expiredDate = \DateTime::createFromFormat("Y-m-d H:i:s", $expiredDate)->format($format);

                        if ($now > $expiredDate)
                            $newState = (CampaignStatus::STATUS_EXPIRED);
                        else
                            $newState = (CampaignStatus::STATUS_PENDING_REVIEW);
                        $product->setCampaignStatus($newState);
                        $product->getResource()->saveAttribute($product, 'campaign_status');

                        $groupon->setData('status', $newState);
                        $groupon->save();
                    }
                }
            } catch (\Exception $e) {
                $this->_logger->info("Check Campaign Status Error: {$e->getMessage()}");
                $this->_logger->info("Product ID: {$item['entity_id']}");
            }
        }
        $this->_logger->info("Update campaign status successfully.");
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int $pubDate
     * @param int $unPubDate
     * @param int $bookFrom
     * @param int $bookTo
     *
     * @return int
     */
    public function getHotelCampaignStatus($product, $pubDate = null, $unPubDate = null, $bookFrom = null, $bookTo = null)
    {
        /** @var \Magenest\Hotel\Model\PackageDetails $package */
        $package = $this->_packageDetailsCollectionFactory->create()->getItemByProductId($product->getId());
        $this->_logger->info("Update campaign status for Hotel product began.");
        $now = strtotime(date('Y-m-d'));

        $liveDate = $pubDate ?: strtotime($package->getPublishDate());

//        $unPubDate = $unPubDate ?: strtotime($package->getUnpublishDate());
        $bookFrom   = $bookFrom ?: strtotime($package->getBookFrom());
        $activeDate = $bookFrom;

        $expiredDate = $bookTo ?: strtotime($package->getBookTo());

        // set Scheduled
        if ($now < $liveDate) {
            return CampaignStatus::STATUS_SCHEDULED;
        }

        // set Live
        if ($now >= $liveDate && $now < $activeDate) {
            return CampaignStatus::STATUS_LIVE;
        }

        // set Active
        if ($now >= $activeDate && $now < $expiredDate) {
            return CampaignStatus::STATUS_ACTIVE;
        }

        // set Expired
        if ($now > $expiredDate) {
            return CampaignStatus::STATUS_EXPIRED;
        }

        return CampaignStatus::STATUS_SCHEDULED;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     */
    private function checkProductHotel($product)
    {
        $product->setCampaignStatus($this->getHotelCampaignStatus($product))->save();
    }

    public function compareTime($time1, $time2)
    {
        return strtotime($time1) > strtotime($time2);
    }

    private function checkProductTicket($product, $now)
    {
        $this->_logger->info("Update campaign status for Ticket product began.");
        $productId = $product->getId();
        $tickets   = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter("product_id", $productId);
        $sessions  = $this->_sessionFactory->create()->getCollection()->addFieldToFilter("tickets_id", $tickets->getAllIds());
        if ($sessions->count() > 0) {
            $latestSessionEnd     = null;
            $earliestSessionStart = null;
            $earliestSaleStart    = null;
            foreach ($sessions as $session) {
                if ($earliestSaleStart == null || $this->compareTime($earliestSaleStart, $session->getSellFrom()))
                    $earliestSaleStart = strtotime($session->getSellFrom());
                if ($earliestSessionStart == null || $this->compareTime($earliestSessionStart, $session->getSessionFrom()))
                    $earliestSessionStart = strtotime($session->getSessionFrom());
                if ($latestSessionEnd == null || !$this->compareTime($latestSessionEnd, $session->getSessionTo()))
                    $latestSessionEnd = strtotime($session->getSessionTo());
            }
            if ($now >= $latestSessionEnd) {
                $product->setCampaignStatus(CampaignStatus::STATUS_EXPIRED)->save();
            } else {
                if ($now >= $earliestSessionStart) {
                    $product->setCampaignStatus(CampaignStatus::STATUS_ACTIVE)->save();
                } else {
                    if ($now >= $earliestSaleStart) {
                        $product->setCampaignStatus(CampaignStatus::STATUS_LIVE)->save();
                    } else
                        $product->setCampaignStatus(CampaignStatus::STATUS_SCHEDULED)->save();
                }
            }
        }
    }
}
