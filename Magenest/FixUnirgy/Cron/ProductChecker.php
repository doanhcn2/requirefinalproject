<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Cron;

class ProductChecker
{
    protected $reloadHiddenIdsHelper;
    public function __construct(
        \Magenest\FixUnirgy\Helper\ReloadHiddenIds $reloadHiddenIdsHelper
    )
    {
        $this->reloadHiddenIdsHelper = $reloadHiddenIdsHelper;
    }

    public function execute()
    {
        $this->reloadHiddenIdsHelper->reloadHiddenIds();
    }
}