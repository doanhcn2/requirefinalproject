<?php

namespace Magenest\FixUnirgy\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Unirgy\Dropship\Observer\AbstractObserver;
use \Magento\Framework\App\Request\Http;
use \Unirgy\Dropship\Observer\Context;
use \Magenest\Groupon\Model\ResourceModel\Deal\Collection as DealCollection;
use \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory as DealCollectionFactory;

class CatalogProductCollectionLoadAfter extends AbstractObserver implements ObserverInterface
{
    /**
     * @var Http
     */
    protected $_request;

    /**
     * @var DealCollectionFactory
     */
    protected $dealCollectionFactory;

    private $_frontendController = [
        'catalog_category_view',
        'catalogsearch_result_index',
        'cms_index_index'
    ];

    public function __construct(
        Http $request,
        DealCollectionFactory $dealCollectionFactory,
        Context $context,
        array $data = []
    )
    {
        $this->dealCollectionFactory = $dealCollectionFactory;
        $this->_request = $request;
        parent::__construct($context, $data);
    }

    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $observer->getEvent()->getCollection();
        if ($productCollection instanceof \Magento\Catalog\Model\ResourceModel\Product\Collection) {
            $handle = $this->_request->getFullActionName();
            if (in_array($handle, $this->_frontendController)) {
                $ids = $productCollection->getAllIds();
                /** @var DealCollection $dealCollection */
                $dealCollection = $this->dealCollectionFactory->create();
                $dealCollection->addFieldToFilter('product_id', ['in' => implode(',', $ids)])
                    ->addFieldToFilter('end_time', ['lt' => date('Y-m-d')]);
                $expiredProductIds = $dealCollection->getColumnValues('product_id');
                if (!empty($expiredProductIds))
                    foreach ($expiredProductIds as $id) {
                        $productCollection->removeItemByKey($id);
                    }
            }
        }
    }
}
