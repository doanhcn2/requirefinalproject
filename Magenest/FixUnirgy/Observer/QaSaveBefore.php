<?php

namespace Magenest\FixUnirgy\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class QaSaveBefore implements ObserverInterface
{
    /**
     * @var \Magento\Sales\Model\Order\ShipmentRepository
     */
    private $shipmentRepository;

    public function __construct(
        \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository

    ) {
        $this->shipmentRepository = $shipmentRepository;
    }

    public function execute(EventObserver $observer)
    {
        $question = $observer->getQuestion();
        if (!empty($question->getShipmentId()) && empty($question->getMagentoOrderId())) {
            $shipment = $this->shipmentRepository->get($question->getShipmentId());
            $question->setMagentoOrderId($shipment->getOrderId());
        }

        return;
    }
}