<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Observer;

use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RefreshCacheAfterProductSave implements ObserverInterface
{
    /**
     * @var $__cacheTypeList TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magenest\FixUnirgy\Helper\ReloadHiddenIds
     */
    protected $reloadHiddenIdsHelper;

    /**
     * @var Pool $_cacheFrontendPool
     */
    protected $_cacheFrontendPool;

    /**
     * RefreshCacheAfterProductSave constructor.
     * @param \Magenest\FixUnirgy\Helper\ReloadHiddenIds $reloadHiddenIdsHelper
     * @param TypeListInterface $cacheTypeList
     * @param Pool $cacheFrontendPool
     */
    public function __construct(
        \Magenest\FixUnirgy\Helper\ReloadHiddenIds $reloadHiddenIdsHelper,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool
    )
    {
        $this->reloadHiddenIdsHelper = $reloadHiddenIdsHelper;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    public function execute(Observer $observer)
    {
//        $types = array('config', 'layout', 'block_html', 'collections', 'reflection', 'db_ddl', 'eav', 'config_integration', 'config_integration_api', 'full_page', 'translate', 'config_webservice');
//        foreach ($types as $type) {
//            if ($type === 'full_page') {
//                $this->_cacheTypeList->cleanType($type);
//            }
//        }
//
//        $this->reloadHiddenIdsHelper->reloadHiddenIds();
    }
}
