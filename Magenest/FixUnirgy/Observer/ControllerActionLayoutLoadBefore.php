<?php

namespace Magenest\FixUnirgy\Observer;

use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipMultiPrice\Helper\Data as DropshipMultiPriceHelperData;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipMultiPrice\Observer\AbstractObserver;

class ControllerActionLayoutLoadBefore extends AbstractObserver implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $_frameworkRegistry;

    /**
     * ControllerActionLayoutLoadBefore constructor.
     *
     * @param HelperData $helperData
     * @param DropshipMultiPriceHelperData $dropshipMultiPriceHelperData
     * @param Registry $frameworkRegistry
     */
    public function __construct(
        HelperData $helperData,
        DropshipMultiPriceHelperData $dropshipMultiPriceHelperData,
        Registry $frameworkRegistry
    ) {
        $this->_frameworkRegistry = $frameworkRegistry;
        parent::__construct($helperData, $dropshipMultiPriceHelperData);
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (!$this->_hlp->isUdmultiActive())
            return;
        if (in_array($observer->getFullActionName(), ['catalog_product_view', 'checkout_cart_configure'])
            && ($product = $this->_frameworkRegistry->registry('current_product'))
            && in_array($product->getTypeId(), ['simple', 'configurable', 'virtual'])
        ) {
            $bestVendor = $product->getUdmultiBestVendor();
            $product->setUdropshipVendor($bestVendor);
        }
    }
}
