<?php
namespace Magenest\FixUnirgy\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\DropshipPo\Model\PoFactory;
use Unirgy\DropshipPo\Model\Po\ItemFactory;
use Unirgy\Dropship\Helper\Data;
use Magenest\FixUnirgy\Model\PoGridFactory;

class AutoGenPO implements ObserverInterface
{
    /**
     * @var Data
     */
    private $_hlp;

    /**
     * @var PoFactory
     */
    private $_poFactory;

    /**
     * @var ItemFactory
     */
    private $_poItemFactory;

    /**
     * @var PoGridFactory
     */
    private $_poGridFactory;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    public function __construct
    (
        Data $data,
        PoFactory $poFactory,
        ItemFactory $itemFactory,
        PoGridFactory $poGridFactory
    )
    {
        $this->_hlp = $data;
        $this->_poFactory = $poFactory;
        $this->_poItemFactory = $itemFactory;
        $this->_poGridFactory = $poGridFactory;
    }

    public function execute(EventObserver $observer)
    {
        /** @var OrderInterface $order */
        $order = $observer->getOrder();
        $items = $order->getItems();
        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($items as $item) {
            if ($item->getProductType() == "coupon" || $item->getProductType() == "ticket") {
                $poItem = $this->getPOItem($order->getId(), $item->getProductId());
                $noPOtoCreate = $item->getQtyInvoiced();
                if ($poItem->getEntityId() && $poItem->getQtyInvoiced() > 0) {
                    $noPOtoCreate -= $poItem->getQtyInvoiced();
                }

                $id = $item->getParentItemId() ? $item->getParentItemId() : $item->getItemId();

                $qty = [
                    $id => "1"
                ];
                for ($i = 0; $i < $noPOtoCreate; $i++) {
                    $this->_hlp->getObj('\Unirgy\DropshipPo\Helper\ProtectedCode')->splitOrderToPos($order, $qty, '');
                }
            }
        }
    }

    public function getPOItem($orderId, $productId)
    {
        /** @var \Unirgy\DropshipPo\Model\ResourceModel\Po\Item\Collection $itemCollection */
        $itemCollection = $this->_poItemFactory->create()->getCollection();
        $itemCollection->addFieldToFilter('order_item_id', $orderId)
            ->addFieldToFilter('product_id', $productId);
        $item = $itemCollection->getFirstItem();
        return $item;
    }
}