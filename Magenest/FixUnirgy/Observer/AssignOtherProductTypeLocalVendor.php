<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ProductFactory;
use Unirgy\Dropship\Helper\Data;
use Unirgy\SimpleLicense\Exception;

class AssignOtherProductTypeLocalVendor implements ObserverInterface
{
    /**
     * @var $_productFactory ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_unirgyHelper Data
     */
    protected $_unirgyHelper;

    /**
     * AssignOtherProductTypeLocalVendor constructor.
     * @param ProductFactory $productFactory
     * @param Data $data
     */
    public function __construct(
        ProductFactory $productFactory,
        Data $data
    ) {
        $this->_unirgyHelper = $data;
        $this->_productFactory = $productFactory;
    }

    public function execute(Observer $observer)
    {
        $productId = $observer->getProduct()->getEntityId();
        $productType = $observer->getProduct()->getTypeId();
        $productVendor = $observer->getProduct()->getUdropshipVendor();

        if (in_array($productType, ['wrapper', 'wrapper_gift_card'])) {
            $localVendor = $this->_unirgyHelper->getLocalVendorId();
            if ($localVendor !== $productVendor) {
                $products = $this->_productFactory->create()->load($productId)->setData('udropship_vendor', $localVendor);
                try {
                    $products->save();
                } catch (Exception $e) {
                }
            }
        }
    }
}
