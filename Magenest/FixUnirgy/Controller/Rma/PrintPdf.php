<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 16/04/2018
 * Time: 14:07
 */
namespace Magenest\FixUnirgy\Controller\Rma;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
class PrintPdf extends AbstractAccount{

    protected $_rmaPdf;

    protected $_hlp;
    public function __construct(
      \Magenest\FixUnirgy\Helper\Rma\Waybill $rmaPdf,
      \Unirgy\Dropship\Helper\Data $udropshipHelper,
      \Magento\Framework\App\Action\Context $context
    )
    {
        $this->_hlp = $udropshipHelper;
        $this->_rmaPdf = $rmaPdf;
        parent::__construct($context);
    }

    public function execute()
    {
            $data = $this->getRequest()->getParams();
            if($this->getRequest()->getParam('rma_id')!=''){
                $pdf = $this->_rmaPdf->getPdf($this->getRequest()->getParam('rma_id'));
                $this->_hlp->sendDownload('order_rma_'.$this->_hlp->now().'.pdf',
                  $pdf->render(), 'application/pdf');
            }
//        return $this->_redirect($this->_redirect->getRefererUrl());
        // TODO: Implement execute() method.
    }
}