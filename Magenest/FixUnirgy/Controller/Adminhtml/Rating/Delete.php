<?php

namespace Magenest\FixUnirgy\Controller\Adminhtml\Rating;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Rating\EntityFactory;
use Unirgy\DropshipVendorRatings\Controller\Adminhtml\Rating\Delete as UnirgyRatingDelete;

class Delete extends UnirgyRatingDelete
{
    public function execute()
    {
        if($this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = $this->_ratingFactory->create();
                /* @var $model \Magento\Review\Model\Rating */
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                $objectManager = ObjectManager::getInstance();
                $ratingId = $this->getRequest()->getParam('id');
                $ratingModel = $objectManager->create('Magenest\FixUnirgy\Model\RatingInfo')->load($ratingId, 'rating_id');
                $ratingModel->delete();
                $this->messageManager->addSuccess(__('The rating has been deleted.'));
                $this->_redirect('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->_redirect('*/*/');
    }
}
