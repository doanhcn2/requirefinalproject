<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Controller\Adminhtml\Rating\Product;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

class Save extends \Magento\Review\Controller\Adminhtml\Rating\Save
{
    /**
     * @var $_ratingInfoFactory \Magenest\FixUnirgy\Model\RatingInfoFactory
     */
    protected $_ratingInfoFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory
    ) {
        $this->_ratingInfoFactory = $ratingInfoFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Save rating
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $this->initEnityId();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        if ($this->getRequest()->getPostValue()) {
            try {
                /** @var \Magento\Review\Model\Rating $ratingModel */
                $ratingModel = $this->_objectManager->create(\Magento\Review\Model\Rating::class);
                $stores = $this->getRequest()->getParam('stores');
                $position = (int)$this->getRequest()->getParam('position');
                $stores[] = 0;
                $isActive = (bool)$this->getRequest()->getParam('is_active');

                $ratingModel->setRatingCode($this->getRequest()->getParam('rating_code'))
                    ->setRatingCodes($this->getRequest()->getParam('rating_codes'))
                    ->setStores($stores)
                    ->setPosition($position)
                    ->setId($this->getRequest()->getParam('id'))
                    ->setIsActive($isActive)
                    ->setEntityId($this->coreRegistry->registry('entityId'))
                    ->save();

                $options = $this->getRequest()->getParam('option_title');

                $params = $this->getRequest()->getParams();
                $ratingInfoCollection = $this->_ratingInfoFactory->create()
                    ->getCollection()->addFieldToFilter('rating_id', $ratingModel->getId());
                if ($ratingInfoCollection->getSize() == 0) {
                    $ratingInfoModel = $this->_ratingInfoFactory->create();
                } else {
                    $ratingInfoModel = $ratingInfoCollection->getFirstItem();
                }
                if (isset($params['category_ids']) && is_array($params['category_ids'])) {
                    $ratingInfoModel->setData('rating_id', $ratingModel->getId());
                    $ratingInfoModel->setData('category_ids', implode(',', $params['category_ids']));
                    $ratingInfoModel->save();
                } else {
                    $ratingInfoModel->setCategoryIds('');
                    $ratingInfoModel->save();
                }

                if (is_array($options)) {
                    $i = 1;
                    foreach($options as $key => $optionCode) {
                        $optionModel = $this->_objectManager->create(\Magento\Review\Model\Rating\Option::class);
                        if (!preg_match("/^add_([0-9]*?)$/", $key)) {
                            $optionModel->setId($key);
                        }

                        $optionModel->setCode($optionCode)
                            ->setValue($i)
                            ->setRatingId($ratingModel->getId())
                            ->setPosition($i)
                            ->save();
                        $i++;
                    }
                }

                $this->messageManager->addSuccess(__('You saved the rating.'));
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)->setRatingData(false);
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get(\Magento\Backend\Model\Session::class)
                    ->setRatingData($this->getRequest()->getPostValue());
                $resultRedirect->setPath('review/rating/edit', ['id' => $this->getRequest()->getParam('id')]);
                return $resultRedirect;
            }
        }
        $resultRedirect->setPath('review/rating/');
        return $resultRedirect;
    }
}
