<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 26/04/2018
 * Time: 08:36
 */
namespace Magenest\FixUnirgy\Controller\Adminhtml\Rating;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Review\Model\Rating\EntityFactory;
use Magento\Review\Model\Rating\OptionFactory;
use Magento\Review\Model\RatingFactory;
class Save extends \Unirgy\DropshipVendorRatings\Controller\Adminhtml\Rating\Save{


    protected $_ratingInfoFactory;
    public function __construct(
      \Magento\Backend\App\Action\Context $context,
      \Magento\Framework\Registry $frameworkRegistry,
      \Magento\Review\Model\Rating\EntityFactory $ratingEntityFactory,
      \Magento\Review\Model\RatingFactory $modelRatingFactory,
      \Magento\Review\Model\Rating\OptionFactory $ratingOptionFactory,
      \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory
    ) {
        parent::__construct($context, $frameworkRegistry, $ratingEntityFactory,
          $modelRatingFactory, $ratingOptionFactory);
        $this->_ratingInfoFactory = $ratingInfoFactory;
    }

    public function execute()
    {
        $this->_initEnityId();
        if ( $this->getRequest()->getPost() ) {
            try {
                $ratingModel = $this->_ratingFactory->create();

                $stores = $this->getRequest()->getParam('stores');
                $stores[] = 0;
                if (!($rNew = !$this->getRequest()->getParam('id'))) {
                    $ratingModel->load($this->getRequest()->getParam('id'));
                    if (!$ratingModel->getId()) {
                        throw new \Exception(__('Rating with %1 id not found', $this->getRequest()->getParam('id')));
                    }
                } else {
                    $ratingModel->setIsAggregate($this->getRequest()->getParam('is_aggregate'));
                }
                $ratingModel->setRatingCode($this->getRequest()->getParam('rating_code'))
                  ->setRatingCodes($this->getRequest()->getParam('rating_codes'))
                  ->setStores($stores)
                  ->setEntityId($this->_coreRegistry->registry('entityId'))
                  ->save();

                $options = $this->getRequest()->getParam('option_title');

                if( is_array($options) ) {
                    $i = $ratingModel->getIsAggregate() && !$rNew ? 1 : 0;
                    $iStart = $ratingModel->getIsAggregate() ? 1 : 0;
                    foreach( $options as $key => $optionCode ) {
                        $optionModel = $this->_ratingOptionFactory->create();
                        $roNew = true;
                        if( !preg_match("/^add_([0-9]*?)$/", $key) ) {
                            $roNew = false;
                            $optionModel->setId($key);
                        }

                        if (!$roNew || $i>=$iStart) {
                            $optionModel->setCode($optionCode)
                              ->setValue($i)
                              ->setRatingId($ratingModel->getId())
                              ->setPosition($i)
                              ->save();
                        }
                        $i++;
                        if (!$ratingModel->getIsAggregate() && $i>1) {
                            break;
                        }
                    }
                }

                $params = $this->getRequest()->getParams();
                $ratingInfoCollection = $this->_ratingInfoFactory->create()
                  ->getCollection()->addFieldToFilter('rating_id',$ratingModel->getId());
                if($ratingInfoCollection->getSize()==0){
                    $ratingInfoModel = $this->_ratingInfoFactory->create();
                }else{
                    $ratingInfoModel = $ratingInfoCollection->getFirstItem();
                }
                if(isset($params['category_ids'])&&is_array($params['category_ids'])){
                    $ratingInfoModel->setData('rating_id',$ratingModel->getId());
                    $ratingInfoModel->setData('category_ids', implode(',',$params['category_ids']));
                    $ratingInfoModel->save();
                }else{
                    $ratingInfoModel->setCategoryIds('');
                    $ratingInfoModel->save();
                }
                $this->messageManager->addSuccess(__('The rating has been saved.'));
                $this->_session->setRatingData(false);

                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_session->setRatingData((array)$this->getRequest()->getPost());
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->_redirect('*/*/');

    }
}