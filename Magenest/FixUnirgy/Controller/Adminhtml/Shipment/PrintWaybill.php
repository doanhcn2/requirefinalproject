<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 20/04/2018
 * Time: 10:05
 */
namespace Magenest\FixUnirgy\Controller\Adminhtml\Shipment;
use Magento\Backend\App\Action;
class PrintWaybill extends Action{

    protected $_orderFactory;

    protected $_poFactory;
    public function __construct(
      \Magento\Sales\Model\OrderFactory $orderFactory,
      \Unirgy\DropshipPo\Model\PoFactory $poFactory,
      \Magento\Backend\App\Action\Context $context)
    {
        $this->_poFactory = $poFactory;
        $this->_orderFactory = $orderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $allPo = $this->_poFactory->create()->getCollection()
          ->addFieldToFilter('order_id',$orderId);
        /** @var  $po \Unirgy\DropshipPo\Model\Po */
        foreach ($allPo as $po){
            $po->getItems();
        }
        // TODO: Implement execute() method.
    }
}