<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 17/04/2018
 * Time: 10:00
 */
namespace Magenest\FixUnirgy\Controller\Adminhtml\Rma;
use Magento\Backend\App\Action;
class MassPrint extends Action{


    protected $uRmaFactory;

    protected $waybillHelper;

    protected $_hlp;

    protected $pdf;
    public function __construct(
      \Unirgy\Rma\Model\RmaFactory $uRmaFactory,
      \Magenest\FixUnirgy\Helper\Rma\Waybill $waybillHelper,
      \Unirgy\Dropship\Helper\Data $udropshipHelper,
      \Magento\Backend\App\Action\Context $context)
    {
        $this->_hlp = $udropshipHelper;
        $this->waybillHelper = $waybillHelper;
        $this->uRmaFactory = $uRmaFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if($this->getRequest()->getParam('massaction')!=null && is_array($this->getRequest()->getParam('massaction'))) {
            $selectedCollection = $this->uRmaFactory->create()->getCollection()->addFieldToFilter('entity_id',
                ['in' => $this->getRequest()->getParam('massaction')]);
            $pdf = $this->waybillHelper->getMassactionPdf($selectedCollection);
            $this->_hlp->sendDownload('order_rma_'.$this->_hlp->now().'.pdf', $pdf->render(), 'application/pdf');
        }else{
            $this->messageManager->addError('No Record Selected');
        }
        return $this->_redirect($this->_redirect->getRefererUrl());
    }
}