<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 17/04/2018
 * Time: 09:26
 */
namespace Magenest\FixUnirgy\Controller\Adminhtml\Rma;
use Magento\Backend\App\Action;
class PrintPdf extends Action{
    protected $_rmaPdf;

    protected $_hlp;

    protected $_waybillHelper;
    public function __construct(
      \Magenest\FixUnirgy\Helper\Rma\Waybill $rmaPdf,
      \Unirgy\Dropship\Helper\Data $udropshipHelper,
      \Magenest\FixUnirgy\Helper\Rma\Waybill $waybill,
      \Magento\Backend\App\Action\Context $context
    )
    {
        parent::__construct($context);
        $this->_hlp = $udropshipHelper;
        $this->_rmaPdf = $rmaPdf;
        $this->_waybillHelper = $waybill;
    }

    public function execute()
    {
        if($this->getRequest()->getParam('rma_id')!='') {
            $pdf = $this->_rmaPdf->getPdf($this->getRequest()->getParam('rma_id'));
            $this->_hlp->sendDownload('order_rma_'.$this->_hlp->now().'.pdf', $pdf->render(), 'application/pdf');
        }
        // TODO: Implement execute() method.
    }
}