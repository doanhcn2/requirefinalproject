<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magento\Framework\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;

class GetCommission extends Action\Action
{
    protected $collectionFactory;

    protected $jsonFactory;

    /**
     * @var $_hlp \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
        Action\Context $context,
        JsonFactory $resultJsonFactory,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Unirgy\Dropship\Helper\Data $data
    ) {
        $this->_hlp = $data;
        $this->_categoryFactory = $categoryFactory;
        $this->_vendorSession = $vendorSession;
        $this->jsonFactory = $resultJsonFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $discountCategory = $this->_hlp->getScopeConfig('udprod_groupon/deal_offer/offer_per_category');
        $ids = $this->getRequest()->getParam('ids');
        if ($ids) {
            $lowest = $this->getLowestCategory($ids);
            $items = json_decode($discountCategory, true);
            if (!is_array($items)) {
                $items = [];
            }
            foreach($items as $item) {
                if (in_array($item['category'], $lowest)) {
                    $data = ['discount_from' => $item['discount_from'], 'discount_to' => $item['discount_to']];
                    if ($this->_vendorSession->hasCategoryDiscountPercent()) {
                        $this->_vendorSession->unsCategoryDiscountPercent();
                    }
                    $this->_vendorSession->setCategoryDiscountPercent(json_encode($data));
                    $this->_vendorSession->setNewCategoryAdded(true);
                    break;
                }
            }
        }
        return $this->jsonFactory->create()->setData($this->getCommissionAndRate($ids));
    }

    public function getLowestCategory($categories) {
        $lowest = [];
        $currentLength = -1;
        foreach($categories as $categoryId) {
            $categoryPath = $this->_categoryFactory->create()->load($categoryId)->getPath();
            $pathCount = explode('/', $categoryPath);
            if (count($pathCount) > $currentLength) {
                $lowest = [$categoryId];
                $currentLength = count($pathCount);
            } elseif (count($pathCount) === $currentLength) {
                $lowest[] = $categoryId;
            }
        }

        return $lowest;
    }

    /**
     * @param $catIds
     *
     * @return array
     */
    public function getCommissionAndRate($catIds) {
        try {
            /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $collection */
            $collection = $this->collectionFactory->create();
            $collection->addAttributeToSelect('custom_commission');
            $collection->addIdFilter($catIds);
            $commission = 0;
            $rate       = 0;
            foreach ($collection as $item) {
                if ($customCommission = $item->getCustomCommission()) {
                    $customCommission = json_decode($customCommission, true);
                    if ((int)$customCommission['default_commission'] > $commission) {
                        $commission = (int)$customCommission['default_commission'];
                        $commission = empty($commission) ? 0 : $commission;
                        $rate       = (int)$customCommission['default_fixed_rate'];
                        $rate       = empty($rate) ? 0 : $rate;
                    }
                }
            }

            return [
                'commission' => $commission,
                'rate'       => $rate
            ];
        } catch (\Exception $e) {
            return [
                'commission' => 0,
                'rate'       => 0,
                'error'      => $e->getMessage()
            ];
        }
    }
}
