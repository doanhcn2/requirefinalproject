<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Unirgy\DropshipMicrositePro\Controller\Vendor\AbstractVendor;

class CheckUrlkeyUnique extends AbstractVendor
{
    public function execute()
    {
        $urlkey = $this->getRequest()->getParam('urlkey');
        if (empty($urlkey)) {
            return $this->returnResult([
                'error' => true,
                'success' => false,
            ]);
        } else {
            if (!$this->_mspHlp->checkUrlkeyUnique($this->formatUrlKey($urlkey))) {
                return $this->returnResult([
                    'error' => true,
                    'success' => false,
                ]);
            } else {
                return $this->returnResult([
                    'error' => false,
                    'success' => true,
                ]);
            }
        }
    }

    public function formatUrlKey($str)
    {
        /* @var \Magento\Framework\Filter\FilterManager $filter */
        $filter = $this->_hlp->getObj('\Magento\Framework\Filter\FilterManager');
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $filter->translitUrl($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }
}
