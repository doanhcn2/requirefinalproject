<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magento\Framework\App\Action;
use Magento\Framework\View\Element\Template;
use \Magento\Framework\Controller\Result\JsonFactory;

class GetCategoryBreadcrumb extends Action\Action
{
    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    protected $jsonFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        JsonFactory $resultJsonFactory
    ) {
        $this->jsonFactory = $resultJsonFactory;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $ids = $this->getRequest()->getParam('ids');
            if ($ids) {
                $category = $this->_categoryFactory->create()->load($ids);
                $breadcrumb = $this->getCategoryBreadcrumb($category->getPath());

                return $this->jsonFactory->create()->setData([
                    'category_id' => $ids,
                    'category_breadcrumb' => $breadcrumb
                ]);
            }
        } catch (\Exception $exception) {
        }
    }

    protected function getCategoryBreadcrumb($path)
    {
        $pathCut = substr($path, 2);
        $pathExplode = explode('/', $pathCut);
        $result = [];
        foreach ($pathExplode as $item) {
            $categoryName = $this->_categoryFactory->create()->load($item)->getName();
            array_push($result, $categoryName);
        }

        return $result;
    }
}
