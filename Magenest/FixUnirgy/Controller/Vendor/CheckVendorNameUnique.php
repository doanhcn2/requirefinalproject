<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\View\DesignInterface;
use Unirgy\DropshipMicrositePro\Controller\Vendor\AbstractVendor as MicroAbstract;

class CheckVendorNameUnique extends MicroAbstract
{
    /**
     * @var $_grouponHelper \Magenest\FixUnirgy\Helper\CheckUnique\Data
     */
    protected $_grouponHelper;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        RawFactory $resultRawFactory,
        EncoderInterface $jsonEncoder,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper,
        \Unirgy\DropshipMicrositePro\Helper\Data $micrositeProHelper,
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magenest\FixUnirgy\Helper\CheckUnique\Data $data
    ) {
        $this->_grouponHelper = $data;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $resultRawFactory, $jsonEncoder, $micrositeHelper, $micrositeProHelper, $udropshipHelper, $resultPageFactory);
    }

    public function execute()
    {
        $vendor_name = $this->getRequest()->getParam('vendor_name');
        $vendorEmail = $this->getRequest()->getParam('vendor_email');
        if (empty($vendor_name)) {
            return $this->returnResult([
                'error' => true,
                'success' => false,
                'message' => 'Empty Shop Name'
            ]);
        } else {
            if (!$this->_grouponHelper->checkVendorNameUniqueGroupon($vendor_name, $vendorEmail)) {
                return $this->returnResult([
                    'error' => true,
                    'success' => false,
                    'message' => 'Shop Name is used'
                ]);
            } else {
                return $this->returnResult([
                    'error' => false,
                    'success' => true,
                    'message' => 'Shop Name is not used'
                ]);
            }
        }
    }
}
