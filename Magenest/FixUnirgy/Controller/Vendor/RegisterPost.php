<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipMicrosite\Model\Source as ModelSource;
use Unirgy\Dropship\Model\Source;
use Magento\Captcha\Helper\Data as HelperData;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\DropshipMicrosite\Helper\Data as DropshipMicrositeHelperData;
use Unirgy\DropshipMicrosite\Model\RegistrationFactory;
use Unirgy\DropshipVendorMembership\Model\MembershipFactory;
use Unirgy\Dropship\Helper\Catalog;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;

class RegisterPost extends \Unirgy\DropshipVendorMembership\Controller\Vendor\AbstractVendor
{
    /**
     * @var MembershipFactory
     */
    protected $_membershipFactory;

    /**
     * @var RegistrationFactory
     */
    protected $_registrationFactory;

    /**
     * @var QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * @var Catalog
     */
    protected $_helperCatalog;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    protected $_uniqueHelper;

    protected $_unirgyProductFactory;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        LayoutFactory $viewLayoutFactory,
        HelperData $captchaHelperData,
        DropshipHelperData $udropshipHelper,
        DropshipMicrositeHelperData $micrositeHelper,
        PageFactory $resultPageFactory,
        StoreManagerInterface $modelStoreManagerInterface,
        MembershipFactory $modelMembershipFactory,
        RegistrationFactory $modelRegistrationFactory,
        QuoteFactory $modelQuoteFactory,
        Catalog $helperCatalog,
        ProductFactory $modelProductFactory,
        Registry $frameworkRegistry,
        \Magenest\FixUnirgy\Helper\CheckUnique\Data $uniqueHelper
    ) {
        $this->_membershipFactory   = $modelMembershipFactory;
        $this->_registrationFactory = $modelRegistrationFactory;
        $this->_quoteFactory        = $modelQuoteFactory;
        $this->_helperCatalog       = $helperCatalog;
        $this->_productFactory      = $modelProductFactory;
        $this->_coreRegistry        = $frameworkRegistry;
        $this->_uniqueHelper        = $uniqueHelper;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $viewLayoutFactory, $captchaHelperData, $udropshipHelper, $micrositeHelper, $resultPageFactory, $modelStoreManagerInterface);

    }

    public function execute()
    {
        $params      = $this->getRequest()->getParams();
        $vendorName  = $this->getRequest()->getParam('vendor_name');
        $vendorEmail = $this->getRequest()->getParam('email');
        if ($this->_uniqueHelper->checkVendorNameUnique($vendorName)
            && $this->_uniqueHelper->checkEmailUnique($vendorEmail)
        ) {
            $vendor = $this->_uniqueHelper->getVendorByEmail($vendorEmail);
            if ($vendor->getId() && $vendor instanceof \Unirgy\Dropship\Model\Vendor) {
                $preProductType       = $vendor->getPromoteProduct();
                $params['created_at'] = date('Y-m-d H:i:s', time());
                $params['allow_udratings'] = 1;
                $vendor->addData($params);
                if ($this->_uniqueHelper->isProductCreated($vendor)) {
                    $vendor->setPromoteProduct($preProductType);
                }
                try {
                    $vendor->save();
                } catch (\Exception $e) {
                }
                ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->loginById($vendor->getId());
                $promoteProduct = $vendor->getData('promote_product');
                switch ($promoteProduct) {
                    case '1':
                        if ($this->_uniqueHelper->isLocationCreated($vendor)) {
                            if ($this->_uniqueHelper->isProductCreated($vendor)) {
                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/registerfinal'));
                            } else {
                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew',
                                    [
                                        'set_id'          => '4-Groupon',
                                        'type_of_product' => 'Groupon',
                                        'type_id'         => 'configurable'
                                    ]
                                ));
                            }
                        } else {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/location'));
                        }
                        break;
                    case '2':
                        if ($this->_uniqueHelper->isProductCreated($vendor)) {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/registerfinal'));
                        } else {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew',
                                [
                                    'set_id'          => '4-HotelBooking',
                                    'type_of_product' => 'HotelBooking',
                                ]
                            ));
                        }
                        break;
                    case '3':
                        if ($this->_uniqueHelper->isProductCreated($vendor)) {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/registerfinal'));
                        } else {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew',
                                [
                                    'set_id'          => '4-Event',
                                    'type_of_product' => 'Event',
                                ]
                            ));
                        }
                        break;
                    case '4':
                        if ($this->_uniqueHelper->isProductCreated($vendor)) {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/registerfinal'));
                        } else {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew',
                                [
                                    'set_id'          => '4-Simple',
                                    'type_of_product' => 'Simple',
                                ]
                            ));
                        }
                        break;
                    default :
                        $this->_getVendorSession()
                            ->setBeforeAuthUrl($this->_url->getUrl('udropship'));
                }

                if (!$this->_uniqueHelper->isAddressCreated($vendor)) {
                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl(
                        'umicrosite/vendor/register',
                        ['type' => $vendor->getData('promote_product')]
                    ));
                }

                return $this->_loginPostRedirect();
            }
        }
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $hlp     = $this->_msHlp;
        try {
            $data = $this->getRequest()->getParams();

            $membership = $this->_membershipFactory->create()->load(@$data['udmember']['membership']);

            if (!$membership->getId()) {
                throw new \Exception(__('Unknown membership'));
            }

            $session->setRegistrationFormData($data);
            $this->checkCaptcha();

            /** @var \Unirgy\DropshipMicrosite\Model\Registration $reg */
            if (isset($vendor) && $vendor instanceof \Unirgy\DropshipMicrositePro\Model\Registration) {
                $reg = $vendor;
                $reg->addData($data);
            } else {
                $reg = $this->_registrationFactory->create()->setData($data);
            }
            $reg->setData('udmember_limit_products', $membership->getLimitProducts());
            $reg->setData('udmember_membership_code', $membership->getMembershipCode());
            $reg->setData('udmember_membership_title', $membership->getMembershipTitle());
            $reg->setData('udmember_allow_microsite', $membership->getAllowMicrosite());
            $reg->setData('udmember_billing_type', $membership->getBillingType());
            if (!in_array($membership->getBillingType(), ['paypal'])) {
                $reg->setData('udmember_profile_sync_off', 1);
            }

            $reg->validate()
                ->save();
            if (!$this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE)) {
                $hlp->sendVendorSignupEmail($reg);
            }
            $hlp->sendVendorRegistration($reg);
            $session->unsRegistrationFormData();
            if ($this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE)) {
                $vendor = $reg->toVendor();
                $vendor->setStatus(Source::VENDOR_STATUS_INACTIVE);
                $vendor->setCreatedAt(date('Y-m-d H:i:s', time()));
                $vendor->setNewReg(1);
                if ($this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE) == ModelSource::AUTO_APPROVE_YES_ACTIVE) {
                    $vendor->setStatus(Source::VENDOR_STATUS_ACTIVE);
                }
                if (!$this->_scopeConfig->isSetFlag('udropship/microsite/skip_confirmation', ScopeInterface::SCOPE_STORE)) {
                    $vendor->setSendConfirmationEmail(1);
                    $vendor->save();
                    $this->messageManager->addSuccess(__('Thank you for application. Instructions were sent to your email to confirm it'));
                } else {
                    $vendor->save();
                    ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->loginById($vendor->getId());
                    if (!$this->_getVendorSession()->getBeforeAuthUrl()) {
                        if (!$this->_uniqueHelper->isAddressCreated($vendor)) {
                            $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl(
                                'umicrosite/vendor/register',
                                ['type' => $vendor->getData('promote_product')]
                            ));
                        } else {
                            $promoteProduct = $vendor->getData('promote_product');
                            switch ($promoteProduct) {
                                case '1':
                                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/location'));
                                    break;
                                case '2':
                                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-HotelBooking', 'type_of_product' => 'HotelBooking')));
                                    break;
                                case '3':
                                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-Event', 'type_of_product' => 'Event')));
                                    break;
                                case '4':
                                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-Simple', 'type_of_product' => 'Simple')));
                                    break;
                                default :
                                    $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udropship'));
                            }
                        }
                    }
                }
            } else {
                $this->messageManager->addSuccess(__('Thank you for application. As soon as your registration has been verified, you will receive an email confirmation'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            if ($this->getRequest()->getParam('quick')) {
                return $this->_redirect('udropship/vendor/login');
            } else {
                if (@$params['promote_product']) {
                    return $this->_redirect('umicrosite/vendor/register/type/' . $params['promote_product']);
                } else {
                    return $this->_redirect('register/vendor/registersecond/');
                }
            }
        }

        return $this->_loginPostRedirect();
    }
}
