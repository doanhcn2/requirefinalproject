<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magenest\Groupon\Helper\Data;
use Magenest\Groupon\Helper\Data as GrouponHepler;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Option;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Model\App;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorProduct\Helper\Data as DropshipVendorProductHelperData;
use Magento\Catalog\Api\CategoryLinkManagementInterface;

class ProductPost extends \Unirgy\DropshipVendorProduct\Controller\Vendor\ProductPost
{
    /**
     * @var $categoryLinkManagement CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * @var $_campaignEdit \Magenest\Profile\Model\CampaignEditingFactory
     */
    protected $_campaignEditFactory;

    public function __construct(
        DropshipVendorProductHelperData $helperData,
        Context $context, ScopeConfigInterface $scopeConfig,
        DesignInterface $viewDesignInterface,
        StoreManagerInterface $storeManager,
        LayoutFactory $viewLayoutFactory,
        Registry $registry,
        ForwardFactory $resultForwardFactory,
        HelperData $helper, PageFactory $resultPageFactory,
        RawFactory $resultRawFactory, Header $httpHeader,
        \Unirgy\Dropship\Helper\Catalog $helperCatalog,
        \Unirgy\DropshipVendorProduct\Model\ProductFactory $modelProductFactory,
        \Magento\MediaStorage\Helper\File\Storage\Database $storageDatabase,
        CategoryLinkManagementInterface $categoryLinkManagement,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Profile\Model\CampaignEditingFactory $campaignEditingFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory
    ) {
        $this->_eventFactory = $eventFactory;
        $this->_campaignEditFactory = $campaignEditingFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->_dealFactory = $dealFactory;
        parent::__construct($helperData, $context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader, $helperCatalog, $modelProductFactory, $storageDatabase);
    }

    public function execute()
    {
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        $hlp = $this->_hlp;
        $prHlp = $this->_prodHlp;
        $r = $this->getRequest();
        $params = $this->getRequest()->getParams();
        $productId = (int)$this->getRequest()->getParam('id');
        if ($productId !== "" && isset($productId) && GrouponHepler::isDeal($productId)) {
            $this->saveImage($params);
            $grouponData = @$params['groupon'];
            if (isset($grouponData['customer_specify_location'])) {
                $grouponData['customer_specify_location'] = '1';
            } else {
                $grouponData['customer_specify_location'] = '0';
            }
            unset($params['groupon']);
            $locationData = @$grouponData['location'];
            unset($grouponData['location']);
            $data = [
                'product_id' => $productId,
                'vendor_id' => $v->getId(),
                'new_product_data' => json_encode($params),
                'new_coupon_data' => json_encode($grouponData),
                'new_coupon_location' => json_encode($locationData),
                'status' => 0,
                'created_at' => date('Y/m/d H:i:s', time())
            ];
            $campaignEdit = $this->_campaignEditFactory->create()->load($productId, 'product_id');

            $isNewQc = false;
            if (is_array(@$params["product"]["_cfg_attribute"]["quick_create"])) {
                foreach($params["product"]["_cfg_attribute"]["quick_create"] as $key => $param) {
                    if ($key === '$ROW') continue;
                    if (!isset($param['simple_id']) || $param['simple_id'] === "") {
                        $isNewQc = true;
                    }
                }
            }
            if ($isNewQc) {
                $objectManager = ObjectManager::getInstance();
                $prodCoupon = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
                $changeProductIds = $prHlp->processQuickCreates($prodCoupon, false, "Groupon", $r->getParams());
            } else {
                $changeProductIds = ['addCoupon' => [], 'delCoupon' => []];
            }

            $oldChangeCoupon = json_decode($campaignEdit->getCouponDealChanges(), true);
            if (@$oldChangeCoupon) {
                if (@$oldChangeCoupon['addCoupon'] && is_array($oldChangeCoupon['addCoupon'])) {
                    foreach($oldChangeCoupon['addCoupon'] as $item) {
                        if (is_array($changeProductIds['addCoupon']) && !in_array($item, $changeProductIds['addCoupon'])) {
                            $changeProductIds['addCoupon'][] = $item;
                        }
                    }
                } elseif (!empty($oldChangeCoupon['addCoupon'])) {
                    $changeProductIds['addCoupon'][] = $oldChangeCoupon['addCoupon'];
                }
                if (@$oldChangeCoupon['delCoupon'] && is_array($oldChangeCoupon['delCoupon'])) {
                    foreach($oldChangeCoupon['delCoupon'] as $item) {
                        if (is_array($changeProductIds['delCoupon']) && !in_array($item, $changeProductIds['delCoupon'])) {
                            $changeProductIds['delCoupon'][] = $item;
                        }
                    }
                } elseif (!empty($oldChangeCoupon['delCoupon'])) {
                    $changeProductIds['delCoupon'][] = $oldChangeCoupon['delCoupon'];
                }
            }

            $data['coupon_deal_changes'] = json_encode($changeProductIds);
            $campaignEdit->addData($data)->save();

            $this->messageManager->addNoticeMessage('Your changes for campaign is submitted, waiting for approval from admin.');
            $this->_redirect('groupon/vendor_campaign/detail', ['id' => $productId]);
            return true;
        } elseif ($this->isEventProduct($productId)) {
            $this->saveImage($params);
            $eventData = @$params['event'];
            unset($params['event']);
            $ticketData = @$eventData['ticket_info'];
            unset($eventData['ticket_info']);

            $data = [
                'product_id' => $productId,
                'vendor_id' => $v->getId(),
                'new_product_data' => json_encode($params),
                'new_coupon_data' => json_encode($eventData),
                'ticket_info_change' => json_encode($ticketData),
                'status' => 0,
                'created_at' => date('Y/m/d H:i:s', time())
            ];
            $campaignEdit = $this->_campaignEditFactory->create()->load($productId, 'product_id');
            $campaignEdit->addData($data)->save();

            $this->messageManager->addNoticeMessage('Your changes for campaign is submitted, waiting for approval from admin.');
            $this->_redirect('ticket/vendor_campaign/detail', ['id' => $productId]);
            return true;
        }

        $oldStoreId = $this->_storeManager->getStore()->getId();
        $this->_storeManager->setCurrentStore(0);
        if ($r->isPost()) {
            try {
                $prod = $this->_initProduct();
                $isNew = !$prod->getId();
                if (!$this->_hlp->getScopeFlag('udprod/general/disable_name_check')) {
                    $ufName = $prod->formatUrlKey($prod->getName());
                    if (!trim($ufName)) {
                        throw new \Exception(__('Product name is invalid'));
                    }
                }
                if (!$isNew && GrouponHepler::isDeal($prod->getId())) {
                    $this->resetCouponParentId($prod->getId());
                }
                if ('configurable' != $prod->getTypeId() && 'Event' != $this->getRequest()->getParam('type_of_product')) {
                    $prHlp->checkUniqueVendorSku($prod, $v);
                }
                if ($isNew) {
                    $prod->setUdprodIsNew(true);
                }
                if ($downloadable = $r->getPost('downloadable')) {
                    $prod->setDownloadableData($downloadable);
                }
                if ($links = $this->getRequest()->getPost('links')) {
                    if (isset($links['grouped'])) {
                        $prod->setGroupedLinkData($this->_hlp->getObj('\Magento\Backend\Helper\Js')->decodeGridSerializedInput($links['grouped']));
                    }
                }
                $canSaveCustOpt = $prod->getCanSaveCustomOptions();
                $custOptAll = [];
                if (!$isNew && $canSaveCustOpt) {
                    $__custOptAll = $prod->getOptions();
                    foreach ($__custOptAll as $__custOpt) {
                        $__cov = $__custOpt->getData();
                        if ($__custOpt->getGroupByType() == Option::OPTION_GROUP_SELECT) {
                            foreach ($this->getCustomOptionValues($__custOpt) as $__optValue) {
                                $__cov['optionValues'][] = is_array($__optValue) ? $__optValue : $__optValue->getData();
                            }
                        }
                        $custOptAll[] = $__cov;
                    }
                }
                $prod->setData('campaign_status', CampaignStatus::STATUS_PENDING_REVIEW);
                $prod->setWebsiteIds([1]);
                $prod->save();

                $productId = $prod->getEntityId();
                $checkType = null;
                if (isset($productId)) {
                    $modelPr = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($productId);
                    $checkType = $modelPr->getTypeId();
                    $categoryLinkManagement = \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\Magento\Catalog\Api\CategoryLinkManagementInterface::class);
                    $categoryLinkManagement->assignProductToCategories(
                        $modelPr->getSku(),
                        $prod->getCategoryIds()
                    );
                }

                if ($checkType == 'configurable') {
                    $productTypeInstance = $prod->getTypeInstance();
                    $usedProducts = $productTypeInstance->getUsedProducts($prod);

                    foreach ($usedProducts as $child) {
                        $childType = $child->getTypeId();
                        if ($childType == 'coupon') {
                            $checkType = 'Groupon'; //Child Product Id
                        }
                    }
                }

                $parentType = $r->getParam("type_of_product");
                if ($r->getParam("type_of_product") == "Groupon"
                    || $r->getParam("type_of_product") == "Coupon"
                    || $checkType == 'coupon'
                    || $checkType == 'Groupon') {
                    $this->_eventManager->dispatch('unirgy_save_product_after',
                        [
                            "product_id" => $prod->getId(),
                            "information" => $r->getParams(),
                            'product' => $prod]
                    );
                }

                if ($r->getParam("type_of_product") == "Event" || $checkType == 'ticket') {
                    $this->_eventManager->dispatch('unirgy_save_product_ticket',
                        [
                            "product_id" => $prod->getId(),
                            "information" => $r->getParams(),
                            'product' => $prod
                        ]);
                }

                $prHlp->processAfterSave($prod);
                if ('configurable' == $prod->getTypeId() || 'ticket' == $prod->getTypeId() && $isNew) {
                    $prod->setData('udmulti', ['vendor_sku' => $prod->getSku()]);
                }
                $prHlp->processUdmultiPost($prod, $v);
                if ($isNew) {
                    $prHlp->processNewConfigurable($prod, $v);
                }
                $prHlp->processQuickCreate($prod, $isNew, $parentType, $r->getParams());
                $prHlp->processSaveOtherData($prod, $prod->getId(), $v->getId());
                if (!$isNew && $canSaveCustOpt) {
                    if ($canSaveCustOpt) {
                        $custOptAllNew = [];
                        $prod->uclearOptions();
                        if ($prod->getHasOptions()) {
                            foreach ($prod->getProductOptionsCollection() as $option) {
                                $option->setProduct($prod);
                                $prod->addOption($option);
                            }
                        }
                        $__custOptAll = $prod->getOptions();
                        foreach ($__custOptAll as $__custOpt) {
                            $__cov = $__custOpt->getData();
                            if ($__custOpt->getGroupByType() == Option::OPTION_GROUP_SELECT) {
                                foreach ($this->getCustomOptionValues($__custOpt) as $__optValue) {
                                    $__cov['optionValues'][] = is_array($__optValue) ? $__optValue : $__optValue->getData();
                                }
                            }
                            $custOptAllNew[] = $__cov;
                        }
                        if ($custOptAllNew != $custOptAll) {
                            $this->_prodHlp->setNeedToUnpublish($prod, 'custom_options_changed');
                        }
                    }
                }

                $prHlp->reindexProduct($prod);

                if ($r->getParam("type_of_product") == "Event" || $checkType == 'ticket') {
                    $this->_eventManager->dispatch('unirgy_save_product_ticket_after', ["product_id" => $prod->getId(), "information" => $r->getParams(), 'product' => $prod]);
                }

                $this->getCategoryLinkManagement()->assignProductToCategories(
                    $prod->getSku(),
                    $prod->getCategoryIds()
                );
                $this->messageManager->addSuccessMessage(__('Product has been saved'));
            } catch (\Exception $e) {
                if ($r->getParam('id')) {
                    $session->setUdprodFormData($r->getPost('product'));
                }
                $this->messageManager->addErrorMessage($e->getMessage());
                $param = [];
                if ($prod->getId()) {
                    $param['id'] = $prod->getId();
                    return $this->_redirect('udsell/inventory/index');
                }
            }
        }
        $this->_storeManager->setCurrentStore($oldStoreId);
        $isFinishOnboarding = $v->getData('finish_onboarding') === '1' ? true : false;
        if ((!$isFinishOnboarding)) {
            $this->_redirect('register/vendor/registerfinal');
        } else {
            $this->_redirectAfterPost(@$prod);
        }
    }

    protected function isEventProduct($productId)
    {
        $event = $this->_eventFactory->create()->load($productId, 'product_id');

        return $event->getId() ? true : false;
    }

    private function saveImage(&$params)
    {
        if (!isset($params['product'], $params['product']['media_gallery'], $params['product']['media_gallery']['images'])
            || !is_array($params['product']['media_gallery']['images'])) {
            return;
        }
        $images = &$params['product']['media_gallery']['images'];
        /** @var \Magento\MediaStorage\Helper\File\Storage\Database $storage */
        $storage = $this->_objectManager->get(\Magento\MediaStorage\Helper\File\Storage\Database::class);
        $mediaBaseDir = $storage->getMediaBaseDir();
        $productDir = $mediaBaseDir . '/catalog/product';
        $tmpDir = $mediaBaseDir . '/tmp/catalog/product';
        foreach ($images as &$image) {
            if (@$image['removed'] || @$image['value_id']) {
                continue;
            }
            $file = $image['file'] = trim(@$image['file'], '.tmp');
            $source = $tmpDir . $file;
            $dest = $productDir . $file;
            $destPath = pathinfo($dest);
            if (!is_dir($destPath['dirname'])) {
                mkdir($destPath['dirname'], 0777, true);
            }
            copy($source, $dest);
        }
    }

    protected function _redirectAfterPost($prod = null)
    {
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $r = $this->getRequest();
        if (!$r->getParam('continue_edit')) {
            if (Data::isDeal($prod->getId())) {
                $this->_redirect('groupon/vendor_campaign/detail', ['id' => $prod->getId()]);
            } elseif ($prod->getTypeId() === 'ticket') {
                $this->_redirect('ticket/vendor/manage');
            } elseif ($prod->getTypeId() === 'simple') {
                $this->_redirect('udsell/inventory/index');
            } elseif ($session->getUdprodLastGridUrl()) {
                $this->_redirectUrl($session->getUdprodLastGridUrl());
            } else {
                $this->_redirect('udprod/vendor/products');
            }
        } else {
            if (isset($prod) && Data::isDeal($prod->getId())) {
                $this->_redirect('udprod/vendor/productEdit', ['id' => $prod->getId()]);
            } elseif ($prod->getTypeId() === 'ticket') {
                $this->_redirect('udprod/vendor/productEdit', ['id' => $prod->getId()]);
            } elseif ($prod->getTypeId() === 'simple') {
                $this->_redirect('udprod/vendor/productEdit', ['id' => $prod->getId()]);
            } else {
                $this->_redirect('overview/index/index');
            }
        }
    }


    private function getCategoryLinkManagement()
    {
        if (null === $this->categoryLinkManagement) {
            $this->categoryLinkManagement = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Catalog\Api\CategoryLinkManagementInterface');
        }
        return $this->categoryLinkManagement;
    }

    private function resetCouponParentId($productId)
    {
        $allCoupons = $this->_dealFactory->create()->getCollection()->addFieldToFilter('product_type', 'coupon')
            ->addFieldToFilter('parent_id', $productId);
        foreach ($allCoupons as $coupon) {
            $this->_dealFactory->create()->load($coupon->getDealId())->setParentId(null)->save();
        }
    }

    /**
     * @return \Magento\Catalog\Model\Product
     * @throws \Exception
     */
    protected function _initProduct()
    {
        $r = $this->getRequest();
        $v = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
        $productId = (int)$this->getRequest()->getParam('id');
        $productData = $r->getPost('product');
        if (!isset($productData['udmulti']['vendor_sku'])) {
            $productData['udmulti']['vendor_sku'] = strtolower(str_replace(' ', '_', trim($productData['name']))) . '_vsku';
        }
        $product = $this->_prodHlp->initProductEdit([
            'id' => $productId,
            'data' => $productData,
            'vendor' => $v
        ]);
        if (isset($productData['options'])) {
            $productOptions = $productData['options'];
            $product->setProductOptions($productOptions);
            $options = $this->mergeProductOptions(
                $productOptions,
                $r->getPost('options_use_default')
            );
            $customOptions = [];
            foreach ($options as $customOptionData) {
                if (empty($customOptionData['is_delete'])) {
                    if (isset($customOptionData['values'])) {
                        $customOptionData['values'] = array_map(function ($valueData) {
                            if ($valueData['option_type_id'] == -1) {
                                unset($valueData['option_type_id']);
                            }
                            return $valueData;
                        }, array_filter($customOptionData['values'], function ($valueData) {
                            return empty($valueData['is_delete']);
                        }));
                    }
                    $customOption = $this->getCustomOptionFactory()->create(['data' => $customOptionData]);
                    $customOption->setProductSku($product->getSku());
//                    $customOption->setOptionId(null);
                    $customOptions[] = $customOption;
                }
            }
            $product->setOptions($customOptions);
        }
        $product->setCanSaveCustomOptions(
            (bool)$this->getRequest()->getPost('affect_product_custom_options')
        );
        return $product;
    }
}
