<?php

namespace Magenest\FixUnirgy\Controller\Vendor;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use WeProvide\Dompdf\Controller\Result\Dompdf;
use WeProvide\Dompdf\Controller\Result\DompdfFactory;
use Magento\Framework\View\LayoutFactory;
use Unirgy\Dropship\Helper\Data as HelperData;
use Magento\Cms\Model\Template\Filter;

class Download extends Action
{
    protected $dompdfFactory;
    protected $layoutFactory;

    protected $_hlp;

    protected $_filter;

    public function __construct
    (
        Context $context,
        DompdfFactory $dompdfFactory,
        LayoutFactory $layoutFactory,
        HelperData $helperData,
        Filter $filter
    ) {
        $this->dompdfFactory = $dompdfFactory;
        $this->layoutFactory = $layoutFactory;
        $this->_hlp = $helperData;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    public function getHtmlForPdf()
    {
        /** @var \Magento\Framework\View\Element\Template $block */
        $block = $this->layoutFactory->create()->createBlock('Magento\Cms\Block\Block');
        $block->setBlockId('local_service_merchant_contract');
        return $block->toHtml();
    }

    public function injectData($content, $vendorId)
    {

    }

    public function execute()
    {
        $vendorId = $this->_hlp->session()->getVendorId();
        if ($vendorId <= 0) {
            return false;
        }
        $vendor = $this->_hlp->getVendor($vendorId);
        $options = json_decode($this->getRequest()->getParam('data'), true);

        $response = $this->dompdfFactory->create();
        $content = $this->getHtmlForPdf();

        preg_match('/BEGINOFFERING--<\/span><\/span>(.*)<p.*?>ENDOFFERING--<\/p>/s', $content, $optionPart);
        if (count($optionPart) > 1) {
            $content = str_replace($optionPart[0], "</span></span>".$this->prepareOptions($optionPart[1], $options), $content);
        }

        $variables['campaign_creation_date'] = date('d/m/Y');
        $variables['vendor_name'] = $vendor->getVendorName();
        $country = $this->_hlp->getCountry($vendor->getCountryId());
        $street = $vendor->getStreet();
        if (count($street) > 0 ) {
            $street = $street[0];
        } else {
            $street = "Unknown";
        }
        $date=date_create($options['expire_date']);
        $variables['vendor_address_street'] = $street;
        $variables['vendor_address_city'] = $vendor->getCity();
        $variables['vendor_address_country'] = $country->getName();
        $variables['term_url'] = "www.gosawa.com";
        $variables['max_coupons_per_customer'] = isset($options['max_redeem']) && $options['max_redeem'] > 0 ? $options['max_redeem'] : "None";
        $variables['coupon_expiry_date'] = date_format($date,"d/m/Y");
        $variables['reservation'] = isset($options['reservation']) && $options['reservation'] > 0 ? $options['reservation'] : "None";
        $variables['can_combine'] = isset($options['combine']) && $options['combine'] == true ? "Yes" : "No";
        $variables['sharing'] = isset($options['share']) && $options['share'] == true ? "Yes" : "No";
        $variables['age_limit'] = isset($options['age_limit']) && $options['age_limit'] > 0 ? $options['age_limit'] : "None";
        $variables['commission'] = @$options['commission'];
        $variables['name'] = isset($options['contract_name']) && $options['contract_name'] ? $options['contract_name'] : "None" ;

        $this->_filter->setVariables($variables);
        $pdf = $this->_filter->filter($content);
        $response->setFileName('Local Services Merchant Contract');
        $response->setData($pdf);
        return $response;
    }

    protected function prepareOptions($content, $options)
    {
        $str = "";
        $i = 1;
        $optionData = json_decode($options['option_data'], true);

        foreach ($optionData as $option) {
            $variables = [];
            $variables['option_no'] = $i;
            $variables['max_coupons_per_customer'] = isset($options['max_redeem']) && $options['max_redeem'] > 0 ? $options['max_redeem'] : "None";;
            foreach($option as $key => $data) {
                if (strpos($key, '__name') != false) {
                    $variables['price_option_title'] = $data;
                } elseif (strpos($key, '__price') != false) {
                    $variables['price_option_value'] = $data;
                } elseif (strpos($key, '__special_price') != false) {
                    $variables['price_option_sale_price'] = $data;
                } elseif (strpos($key, '__stock_qty') != false) {
                    $variables['maximum_number_of_coupon_to_be_sold'] = $data;
                } elseif ($key == 'receive') {
                    $variables['receive_amount'] = $data;
                }
            }
            $this->_filter->setVariables($variables);
            $str .= $this->_filter->filter($content);
            $i++;
        }
        return $str;
    }
}