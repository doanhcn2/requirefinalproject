<?php

namespace Magenest\FixUnirgy\Controller\Index;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magenest\Groupon\Model\ResourceModel\Deal\Collection as DealCollection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magenest\Groupon\Model\ResourceModel\Deal\CollectionFactory as DealCollectionFactory;
use \Magenest\Groupon\Model\Product\Attribute\CampaignStatus as CampaignStatus;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\CollectionFactory as ExpiredProductCollectionFactory;
use \Magenest\FixUnirgy\Model\ResourceModel\ExpiredProduct\Collection as ExpiredProductCollection;
use Magenest\Groupon\Helper\Data as GrouponHelper;

class Index extends Action
{
    protected $reloadHiddenIdsHelper;
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magenest\FixUnirgy\Helper\ReloadHiddenIds $reloadHiddenIdsHelper,
        Context $context)
    {
        parent::__construct($context);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->reloadHiddenIdsHelper = $reloadHiddenIdsHelper;
    }

    public function execute()
    {
        try {
            $starttime = microtime(true);
//        $this->reloadHiddenIdsHelper->reloadHiddenIds();
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
            $productCollection = $this->_productCollectionFactory->create();
            $productCollection
                ->addAttributeToSelect(['id', 'campaign_status', 'type_id'])
                ->addAttributeToFilter('type_id', ['configurable']);
            $productCollection->setFlag('has_stock_status_filter', true);
            /** @var \Magento\Catalog\Model\Product[] $products */
            $products = $productCollection->getItems();
            foreach ($products as $product) {
                if (in_array($product->getCampaignStatus(), [3, 4, 5, -1])) {
                    if ($product->getTypeId() == "ticket") {
                        $now = strtotime(date('Y-m-d H:m:s'));
                        $this->checkProductTicket($product, $now);
                    } elseif ($product->getTypeId() == "hotel") {
                        $now = strtotime(date('Y-m-d'));
                        $this->checkProductHotel($product, $now);
                    } elseif ($product->getTypeId() == "configurable" && GrouponHelper::isDeal($product->getId())) {
                        GrouponHelper::reload($product, []);
                    }
                } elseif ($product->getCampaignStatus() == null || empty($product->getCampaignStatus())) {
                    if ($product->getTypeId() == "configurable" && GrouponHelper::isDeal($product->getId())) {
                        $format = "Y/m/d";
                        $groupon = GrouponHelper::getDeal($product->getId());
                        $params = $groupon->getData();

                        $now = (date($format));
                        $expiredDate = (@$params['groupon_expire']);
                        if (empty($expiredDate) || $expiredDate === null)
                            $expiredDate = $now;
                        else
                            $expiredDate = \DateTime::createFromFormat("Y-m-d H:i:s", $expiredDate)->format($format);

                        if ($now > $expiredDate)
                            $newState = (CampaignStatus::STATUS_EXPIRED);
                        else
                            $newState = (CampaignStatus::STATUS_PENDING_REVIEW);
                        $product->setCampaignStatus($newState);
                        $product->getResource()->saveAttribute($product, 'campaign_status');

                        $groupon->setData('status', $newState);
                        $groupon->save();
                    }

                }
            }


            echo "Save product duration: " . (microtime(true) - $starttime) . " seconds\n";
        }catch (\Exception $exception){
            echo $product->getId();
        }
    }
}