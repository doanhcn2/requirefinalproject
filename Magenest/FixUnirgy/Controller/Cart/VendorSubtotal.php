<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Controller\Cart;


use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Context;

class VendorSubtotal extends \Magento\Framework\App\Action\Action
{
    protected $vendorItem = null;

    protected $vendorId = null;

    protected $cart;

    protected $jsonFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Context $context,
        Cart $cart
    ) {
        $this->jsonFactory = $resultJsonFactory;
        $this->cart        = $cart;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->vendorId = $this->getRequest()->getParam('vendor_id', 0);
            $return         = [
                'success'  => true,
                'subtotal' => $this->getBaseTotalExclTax(),
                'tax'      => $this->getBaseTotalTax()
            ];

            return $this->jsonFactory->create()->setData($return);
        } catch (\Exception $exception) {
            return $this->jsonFactory->create()->setData(['success' => false, 'error_message' => $exception->getMessage()]);
        }
    }

    public function getVendorItems()
    {
        if ($this->vendorItem == null) {
            $items = $this->cart->getQuote()->getItems();
            /**
             * @var \Magento\Quote\Model\Quote\Item $item
             */
            foreach ($items as $key => $item) {
                if ($item->getData('udropship_vendor') != $this->vendorId)
                    unset($items[$key]);
            }
            $this->vendorItem = $items;
        }

        return $this->vendorItem;
    }

    public function getBaseTotalExclTax()
    {
        $baseTotal = 0;
        $items     = $this->getVendorItems();
        foreach ($items as $item) {
            $baseTotal += (float)$item->getRowTotal();
        }

        return $baseTotal;
    }

    public function getBaseTotalTax()
    {
        $baseTotal = 0;
        $items     = $this->getVendorItems();
        foreach ($items as $item) {
            $baseTotal += (float)$item->getTaxAmount();
        }

        return $baseTotal;
    }
}
