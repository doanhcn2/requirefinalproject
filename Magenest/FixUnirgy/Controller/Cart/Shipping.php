<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Controller\Cart;

use Magenest\FulfilledByGosawa\Model\ShippingMethodManagement;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use \Magento\Checkout\Model\Session as CheckoutSession;

/**
 * For estimate shipping for vendor subtotal in checkout cart page (Sam)
 *
 * Class Shipping
 * @package Magenest\FixUnirgy\Controller\Cart
 */
class Shipping extends Action
{
    /**
     * @var $_estimateShipping ShippingMethodManagement
     */
    protected $_estimateShipping;

    /** @var CheckoutSession $_checkoutSession */
    protected $_checkoutSession;

    public function __construct(
        Context $context,
        ShippingMethodManagement $shippingMethodManagement,
        CheckoutSession $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_estimateShipping = $shippingMethodManagement;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultFactory->create('json');
        $params = $this->getRequest()->getParams();
        if ($params) {
            $quote = $this->_checkoutSession->getQuote();
            $cartId = $quote->getId();
            $address = $this->_objectManager->create('Magento\Quote\Api\Data\AddressInterface');
            $address->setCity($params['city'])->setRegion($params['region'])->setCountryId($params['country_id'])->setPostcode($params['postcode']);
            if (isset($params['region_id'])) {
                $address->setRegionId($params['region_id']);
            }
            $shippingResults = $this->_estimateShipping->estimateByExtendedAddress($cartId, $address);
            $methodsByVendor = [];
            foreach($shippingResults as $shipping) {
                if ((int)$shipping->getUdropshipVendor() <= 0) continue;
                $resultData = [
                    "carrier_code" => $shipping->getCarrierCode(),
                    "method_code" => $shipping->getMethodCode(),
                    "carrier_title" => $shipping->getCarrierTitle(),
                    "method_title" => $shipping->getMethodTitle(),
                    "amount" => $shipping->getAmount(),
                    "base_amount" => $shipping->getBaseAmount(),
                    "available" => $shipping->getAvailable(),
                    "error_message" => $shipping->getErrorMessage(),
                    "price_excl_tax" => $shipping->getPriceExclTax(),
                    "price_incl_tax" => $shipping->getPriceInclTax(),
                    "udropship_vendor" => $shipping->getUdropshipVendor(),
                    "udropship_default" => (string)$shipping->getUdropshipDefault(),
                    "currency_symbol" => $this->getCurrencyCode() . ' ' . $this->getCurrencySymbol()
                ];
                $methodsByVendor[(int)$resultData['udropship_vendor']][] = $resultData;
            }
             return $result->setData(['success' => true, 'methods' => $methodsByVendor]);
        }

        return $result->setData(['success' => false]);
    }

    protected function getCurrencySymbol()
    {
        return $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    protected function getCurrencyCode()
    {
        return $this->_objectManager->create('Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseCurrency()->getCurrencyCode();
    }
}
