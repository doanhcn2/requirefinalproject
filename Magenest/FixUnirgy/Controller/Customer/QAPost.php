<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\FixUnirgy\Controller\Customer;

use Magento\Captcha\Helper\Data as CaptchaHelperData;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception;
use Unirgy\DropshipVendorAskQuestion\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Customer\AbstractCustomer as UnirgyAbstractCustomer;

class QAPost extends UnirgyAbstractCustomer
{
    /**
     * @var QuestionFactory
     */
    protected $_questionFactory;

    public function __construct(Context $context,
                                HelperData $helperData,
                                DropshipHelperData $dropshipHelperData,
                                CaptchaHelperData $captchaHelperData,
                                QuestionFactory $modelQuestionFactory)
    {
        $this->_questionFactory = $modelQuestionFactory;

        parent::__construct($context, $helperData, $dropshipHelperData, $captchaHelperData);
    }

    public function execute()
    {
        if ((array)$this->getRequest()->getPost()) {
            $question = (array)$this->getRequest()->getPost();
            $cSess = ObjectManager::getInstance()->get('Magento\Customer\Model\Session');

            $customer = $cSess->getCustomer();

            $error = false;
            if (!empty($question)) {
                unset($question['question_id']);
                $qModel   = $this->_questionFactory->create()
                    ->setData($question)
                    ->setQuestionDate($this->_hlp->now());
                if ($cSess->isLoggedIn()) {
                    $qModel
                        ->setCustomerEmail($customer->getEmail())
                        ->setCustomerName($customer->getFirstname().' '.$customer->getLastname())
                        ->setCustomerId($customer->getId());
                }
                $validate = $qModel->validate();
                if ($validate === true) {
                    try {
                        $this->checkCaptcha();
                        $qModel->save();
                        $this->messageManager->addSuccessMessage(__('Your question has been accepted for moderation.'));
                    }
                    catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $error = true;
                        $this->messageManager->addErrorMessage($e->getMessage());
                    }
                    catch (\Exception $e) {
                        $error = true;
                        $this->messageManager->addErrorMessage(__('Unable to post the question.'));
                    }
                } else {
                    $error = true;
                    if (is_array($validate)) {
                        foreach ($validate as $errorMessage) {
                            $this->messageManager->addErrorMessage($errorMessage);
                        }
                    }
                    else {
                        $this->messageManager->addErrorMessage(__('Unable to post the question.'));
                    }
                }
            }
        }

        return false;
    }
}
