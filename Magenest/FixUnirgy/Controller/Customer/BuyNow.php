<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\FixUnirgy\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class BuyNow extends Action
{
    /**
     * @var $_checkoutSession \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var $_itemModel \Magento\Quote\Model\Quote\Item
     */
    protected $_itemModel;

    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Magento\Quote\Model\Quote\Item $item,
        Context $context
    ) {
        $this->_itemModel = $item;
        $this->_checkoutSession = $session;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultFactory->create('json');
        $productId = $this->getRequest()->getParam('product_id');
        if (isset($productId)) {
            try {
//                $this->deleteQuoteItems();
                return $result->setData(['success' => true]);
            } catch (\Exception $e) {
                return $result->setData(['success' => false]);
            }
        }
        return $result->setData(['success' => false]);
    }

    public function deleteQuoteItems()
    {
        $checkoutSession = $this->_checkoutSession;
        $allItems = $checkoutSession->getQuote()->getAllVisibleItems();
        foreach($allItems as $item) {
            $itemId = $item->getItemId();
            $quoteItem = $this->_itemModel->load($itemId);
            try {
                $quoteItem->delete();
            } catch (\Exception $e) {
            }
        }
    }
}
