<?php

namespace Magenest\Register\Plugin\Model\Vendor;

use Unirgy\Dropship\Model\Session;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor;

class FixLogin extends AbstractVendor
{
    public function afterLogin(Session $subject, $result)
    {
        if ($result) {
            if ($subject->getVendor()->getNewReg() == 1) {
                return $this->execute();
            };
        }
        return $result;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_getSession()->logout();
        $this->messageManager->addNoticeMessage("Thank you for application. As soon as your registration has been verified, you will receive an email confirmation");
        return 'not verified';
    }
}