<?php

namespace Magenest\Register\Controller\Vendor;

class Location extends AbstractController
{
    public function execute()
    {
        $session = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $data = $this->getRequest()->getParams();
        $session->setRegistrationFormData($data);
        parent::execute();
    }
}