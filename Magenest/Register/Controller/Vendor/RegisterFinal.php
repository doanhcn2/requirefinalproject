<?php

namespace Magenest\Register\Controller\Vendor;


class RegisterFinal extends \Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor
{
    public function execute()
    {
        // TODO: Implement execute() method.
        $this->_renderPage(null, 'magenest_register');
    }

    protected function _renderPage($handles=null, $active=null)
    {
        $this->_setTheme();
        $resultPage = $this->resultPageFactory->create(true);
        $resultPage->addHandle($resultPage->getDefaultLayoutHandle());
        $resultPage->addHandle($handles);

        $title = $resultPage->getConfig()->getTitle();
        $title->prepend(__('Merchant Center'));

        $this->_hlp->getObj('\Magento\Framework\View\Page\Config')->addBodyClass('udropship-vendor');

        if ($active && ($head = $resultPage->getLayout()->getBlock('udropship.header'))) {
            $head->setActivePage($active);
        }
        $resultPage->getLayout()->initMessages();
        $resultPage->renderResult($this->_response);
        $html = $this->_response->getBody();
        //$newHtml = preg_replace('`<link[^>]+?media/styles.css[^>]+?/>`', '', $html);
        //$newHtml = preg_replace('`<link[^>]+?css/styles-m.css[^>]+?/>`', '', $newHtml);
        //$newHtml = preg_replace('`<link[^>]+?styles-l.css[^>]+?/>`', '', $newHtml);
        //$this->_response->setBody($newHtml);
        return $this->_response;
    }
}