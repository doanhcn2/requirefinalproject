<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 21/08/2018
 * Time: 15:50
 */

namespace Magenest\Register\Controller\Vendor;


class PostAddress extends AbstractController
{
    public function execute()
    {
        $vendor = $this->_getVendorSession()->getVendor();
        $data   = $this->getRequest()->getParams();
        $data   = array_filter($data);

        $data['street'] = @$data['street1'];
        $promoteProduct = $vendor->getData('promote_product');
        switch ($promoteProduct) {
            case '1':
                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/location'));
                break;
            case '2':
                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-HotelBooking', 'type_of_product' => 'HotelBooking')));
                break;
            case '3':
                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-Event', 'type_of_product' => 'Event')));
                break;
            case '4':
                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-Simple', 'type_of_product' => 'Simple')));
                break;
            default :
                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udropship'));
        }

        $vendor->addData($data)->save();

        return $this->_loginPostRedirect();
    }
}
