<?php

namespace Magenest\Register\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();
        $table = $setup->getTable('udropship_vendor');
        $connection->addColumn($table, 'promote_product',       ['TYPE' => Table::TYPE_SMALLINT,    'nullable' => true, 'COMMENT' => 'promote_product']);
        $connection->addColumn($table, 'finish_onboarding',     ['TYPE' => Table::TYPE_SMALLINT,    'nullable' => false,'COMMENT' => 'finish_onboarding',   'DEFAULT' => '0']);
        $connection->addColumn($table, 'business_category',     ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_category']);
        $connection->addColumn($table, 'website',               ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'website']);
        $connection->addColumn($table, 'firstname',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'firstname']);
        $connection->addColumn($table, 'lastname',              ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'lastname']);
        $connection->addColumn($table, 'business_role',         ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_role']);
        $connection->addColumn($table, 'business_des',          ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_des']);
        $connection->addColumn($table, 'bank_name',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_name']);
        $connection->addColumn($table, 'bank_swiftcode',        ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_swiftcode']);
        $connection->addColumn($table, 'bank_iban',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_iban']);
        $connection->addColumn($table, 'bank_account_name',     ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_account_name']);
        $connection->addColumn($table, 'legal_business_name',   ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'legal_business_name']);
        $connection->addColumn($table, 'tax_number',            ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'tax_number']);
        $connection->addColumn($table, 'primary_location_id',   ['TYPE' => Table::TYPE_INTEGER,     'nullable' => true, 'COMMENT' => 'primary_location_id']);
        $connection->addColumn($table, 'mailling_street',       ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_street']);
        $connection->addColumn($table, 'mailling_city',         ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_city']);
        $connection->addColumn($table, 'mailling_region',       ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_region']);
        $connection->addColumn($table, 'mailling_zip',          ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_zip']);
        $connection->addColumn($table, 'mailling_telephone',    ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_telephone']);
        $connection->addColumn($table, 'mailling_country_id',   ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_country_id']);
        $setup->endSetup();

        $setup->startSetup();
        $connection = $setup->getConnection();
        $table = $setup->getTable('udropship_vendor_registration');
        $connection->addColumn($table, 'promote_product',       ['TYPE' => Table::TYPE_SMALLINT,    'nullable' => true, 'COMMENT' => 'promote_product']);
        $connection->addColumn($table, 'finish_onboarding',     ['TYPE' => Table::TYPE_SMALLINT,    'nullable' => false,'COMMENT' => 'finish_onboarding',   'DEFAULT' => '0']);
        $connection->addColumn($table, 'business_category',     ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_category']);
        $connection->addColumn($table, 'website',               ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'website']);
        $connection->addColumn($table, 'firstname',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'firstname']);
        $connection->addColumn($table, 'lastname',              ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'lastname']);
        $connection->addColumn($table, 'business_role',         ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_role']);
        $connection->addColumn($table, 'business_des',          ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'business_des']);
        $connection->addColumn($table, 'bank_name',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_name']);
        $connection->addColumn($table, 'bank_swiftcode',        ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_swiftcode']);
        $connection->addColumn($table, 'bank_iban',             ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_iban']);
        $connection->addColumn($table, 'bank_account_name',     ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'bank_account_name']);
        $connection->addColumn($table, 'legal_business_name',   ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'legal_business_name']);
        $connection->addColumn($table, 'tax_number',            ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'tax_number']);
        $connection->addColumn($table, 'primary_location_id',   ['TYPE' => Table::TYPE_INTEGER,     'nullable' => true, 'COMMENT' => 'primary_location_id']);
        $connection->addColumn($table, 'mailling_street',       ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_street']);
        $connection->addColumn($table, 'mailling_city',         ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_city']);
        $connection->addColumn($table, 'mailling_region',       ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_region']);
        $connection->addColumn($table, 'mailling_zip',          ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_zip']);
        $connection->addColumn($table, 'mailling_telephone',    ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_telephone']);
        $connection->addColumn($table, 'mailling_country_id',   ['TYPE' => Table::TYPE_TEXT,        'nullable' => true, 'COMMENT' => 'mailling_country_id']);$setup->endSetup();
    }
}