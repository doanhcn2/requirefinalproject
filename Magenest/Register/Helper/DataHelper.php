<?php

namespace Magenest\Register\Helper;

class DataHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CONFIG_GOOGLE_MAP_API_KEY = 'google/google_map/google_map_key';

    public function getGoogleMapApiKey()
    {
        return trim($this->scopeConfig->getValue(self::XML_PATH_CONFIG_GOOGLE_MAP_API_KEY));
    }
}
