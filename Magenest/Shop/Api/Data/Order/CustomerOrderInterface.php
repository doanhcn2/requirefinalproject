<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 03/02/2018
 * Time: 08:52
 */
namespace  Magenest\Shop\Api\Data\Order;
/**
 * @api
 */
interface CustomerOrderInterface
{
    const RESULT = 'result';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getResult();
}