<?php


namespace Magenest\Shop\Api\Data\Category;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @api
 * @since 100.0.2
 */
interface CategoryProductLinkInterface extends ExtensibleDataInterface
{
    /**
     * @return string|null
     */
    public function getSku();

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * @return int|null
     */
    public function getPosition();

    /**
     * @param int $position
     * @return $this
     */
    public function setPosition($position);

    /**
     * Get category id
     *
     * @return string
     */
    public function getCategoryId();

    /**
     * Set category id
     *
     * @param string $categoryId
     * @return $this
     */
    public function setCategoryId($categoryId);

    /**
     * Retrieve existing extension attributes object.
     *
     * @return \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface $extensionAttributes
    );

    /**
     * @param string $entity_id
     * @return $this
     */
    public function setProductId($entity_id);
    /**
     *
     * @return string
     */
    public function getProductId();
    /**
     * @param string $row_id
     * @return $this
     */
    public function setId($row_id);
    /**
     * @return string
     */
    public function getId();
    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     *
     * @return string
     */
    public function getName();
    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url);
    /**
     * @return string
     */
    public function getUrl();
    /**
     * @param int $price
     * @return $this
     */
    public function setPrice($price);
    /**
     *
     * @return int
     */
    public function getPrice();
    /**
     * @param string $price_formatted
     * @return $this
     */
    public function setPriceFormatted($price_formatted);
    /**
     *
     * @return string
     */
    public function getPriceFormatted();
    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency);
    /**
     *
     * @return string
     */
    public function getCurrency();
    /**
     * @param string $sku
     * @return $this
     */
    public function setCode($sku);
    /**
     *
     * @return string
     */
    public function getCode();
    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);
    /**
     *
     * @return string
     */
    public function getDescription();
    /**
     * @param string $small_image
     * @return $this
     */
    public function setMainImage($small_image);
    /**
     *
     * @return string
     */
    public function getMainImage();
    /**
     * @param string $main_image
     * @return $this
     */
    public function setMainImageHighRes($main_image);
    /**
     *
     * @return string
     */
    public function getMainImageHighRes();
    /**
     * @param string $entity_id
     * @return $this
     */
    public function setBrand($entity_id);
    /**
     *
     * @return string
     */
    public function getBrand();
    /**
     * @param int $discount_price
     * @return $this
     */
    public function setDiscountPrice($discount_price);
    /**
     *
     * @return int
     */
    public function getDiscountPrice();
    /**
     * @param string $discount_price_formatted
     * @return $this
     */
    public function setDiscountPriceFormatted($discount_price_formatted);
    /**
     *
     * @return string
     */
    public function getDiscountPriceFormatted();
    /**
     * @param string $entity_id
     * @return $this
     */
    public function setRemoteId($entity_id);

    /**
     *
     * @return string
     */
    public function getRemoteId();
//    /**
//     * @param string $entity_id
//     * @return $this
//     */
//    public function setBrand($entity_id);
//    /**
//     *
//     * @return string
//     */
//    public function getBrand();
//    /**
//     * @param string $entity_id
//     * @return $this
//     */
//    public function setBrand($entity_id);
//    /**
//     *
//     * @return string
//     */
//    public function getBrand();
//    /**
//     * @param string $entity_id
//     * @return $this
//     */
//    public function setBrand($entity_id);
//    /**
//     *
//     * @return string
//     */
//    public function getBrand();
//    /**
//     * @param string $entity_id
//     * @return $this
//     */
//    public function setBrand($entity_id);
//    /**
//     *
//     * @return string
//     */
//    public function getBrand();
}
