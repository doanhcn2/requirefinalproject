<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 01/02/2018
 * Time: 11:14
 */
namespace Magenest\Shop\Api\Google;

interface GoogleLoginInterface
{
    /**
     * login or create new customer via google login
     *
     * @param string[] $userInfo
     * @return mixed
     */
    public function loginOrCreateNewCustomer($userInfo);
}