<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Shop\Api\Category;

/**
 * @api
 * @since 100.0.2
 */
interface CategoryLinkManagementInterface
{
    /**
     * Get products assigned to category
     *
     * @param int $categoryId
     * @param int $num
     * @param int $size
     * @return \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterface[]
     */
    public function getAssignedProducts($categoryId,$num,$size);

//    /**
//     * Assign product to given categories
//     *
//     * @param string $productSku
//     * @param int[] $categoryIds
//     * @return bool
//     * @since 101.0.0
//     */
//    public function assignProductToCategories($productSku, array $categoryIds);
}
