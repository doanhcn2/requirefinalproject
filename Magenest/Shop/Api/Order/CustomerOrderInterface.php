<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 02/02/2018
 * Time: 20:26
 */
namespace Magenest\Shop\Api\Order;

interface CustomerOrderInterface
{
    /**
     * get all orders of a customer
     *
     * @param string $customerId
     * @return \Magenest\Shop\Api\Data\Order\CustomerOrderInterface
     */
    public function getAllCustomerOrders($customerId);

    /**
     * get a specific order of a customer
     *
     * @param string $customerId
     * @param string $id
     * @return \Magenest\Shop\Api\Data\Order\CustomerOrderInterface
     */
    public function getSpecificCustomerOrder($customerId, $id);
}