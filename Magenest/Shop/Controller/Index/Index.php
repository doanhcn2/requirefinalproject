<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 00:46
 */
namespace Magenest\Shop\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     *  private long id;
    private String name;
    private String description;
    private String url;
    private String logo;

    @SerializedName("google_ua")
    private String googleUa;
    private String language;
    private String currency;

    @SerializedName("flag_icon")
    private String flagIcon;
     */
    public function execute()
    {
        $objectManager = ObjectManager::getInstance();
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();

        /** @var \Magento\Framework\Serialize\Serializer\Json $json */
        $json  =  $objectManager->create('Magento\Framework\Serialize\Serializer\Json');

        $ava = ['username'=> 'roni@gmail.com', 'password' => '1'];

        $de = $json->serialize($ava);
        $ata = "{\"username\":\"roni@example.com\" , \"password\":\"1\"}";
        $t =  $json->unserialize($ata);
        $out = [];
        $out[]=[
            'shopID' => 1,
            'name' => "brandName",
            'desc' => 'Mockshop',
           // 'url' => 'http://127.0.0.1/magento/ee2/',
            'google_ua' => 'google_ua',
            'logo' => 'logo',
            'language' => 'en',
            'currency' => 'USD',
            'flag_icon' => 'icon'
        ];

        $k['records'] = $out;
       // $k = $out;
        //$result->setData($out);
        //return $result;
        echo json_encode($k);
    }
}