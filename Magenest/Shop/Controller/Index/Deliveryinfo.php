<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 06/01/2018
 * Time: 15:53
 */

namespace Magenest\Shop\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;
class Deliveryinfo  extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    public function execute ( ) {
        $k = array (
            'delivery' =>array('shipping'=>
                array (
                    0 =>
                        array (
                            'id' => 1,
                            'deliveryId' => 1,
                            'name' => 'Shipping method 1',
                            'deliveryDescription' => 'Shipping method 1',
                            'personal_pickup' => false,
                            'price_formatted' => '120.5',
                            'total_price' => '120.5',
                            'total_price_formatted' => '120.5',
                            'min_cart_amount' => 0,
                            'price' => 120,
                            'currency' => 'CZK',
                            'payment' =>     array (
                                0 =>
                                    array (
                                        'id' => 1,
                                        'name' => 'Payment method 1',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>1,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',
                                        'price' => 10,
                                        'currency' => 'CZK',
                                    ),
                                1 =>
                                    array (
                                        'id' => 2,
                                        'name' => 'Payment method 2',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>2,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',    'price' => 20,
                                        'currency' => 'CZK',
                                    ),
                            ),
                            'payments' =>     array (
                                0 =>
                                    array (
                                        'id' => 1,
                                        'name' => 'Payment method 1',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>1,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',
                                        'price' => 10,
                                        'currency' => 'CZK',
                                    ),
                                1 =>
                                    array (
                                        'id' => 2,
                                        'name' => 'Payment method 2',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>2,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',    'price' => 20,
                                        'currency' => 'CZK',
                                    ),
                            )
                        ),
                    1 =>
                        array (
                            'id' => 2,
                            'deliveryId' => 2,
                            'name' => 'Shipping method 2',
                            'deliveryDescription' => 'Shipping method 1',
                            'personal_pickup' => false,
                            'price_formatted' => '120.5',
                            'total_price' => '120.5',
                            'total_price_formatted' => '120.5',
                            'min_cart_amount' => 199,
                            'price' => 100,
                            'currency' => 'CZK',
                            'payment' =>     array (
                            0 =>
                                array (
                                    'id' => 1,
                                    'name' => 'Payment method 1',
                                    'payment_description' => 'Bank transfer',
                                    'payment_id' =>1,
                                    'price_formatted' => '10.8',
                                    'total_price'=> 28,
                                    'total_price_formatted' => '28',
                                    'price' => 10,
                                    'currency' => 'CZK',
                                ),
                            1 =>
                                array (
                                    'id' => 2,
                                    'name' => 'Payment method 2',
                                    'payment_description' => 'Bank transfer',
                                    'payment_id' =>2,
                                    'price_formatted' => '10.8',
                                    'total_price'=> 28,
                                    'total_price_formatted' => '28',    'price' => 20,
                                    'currency' => 'CZK',
                                ),
                        )
                        ),
                    2 =>
                        array (
                            'id' => 3,
                            'deliveryId' => 2,

                            'name' => 'Free',
                            'min_cart_amount' => 999,
                            'deliveryDescription' => 'Shipping method 1',
                            'personal_pickup' => false,
                            'price_formatted' => '120.5',
                            'total_price' => '120.5',
                            'total_price_formatted' => '120.5',
                            'price' => 0,
                            'currency' => 'CZK',
                            'payment' =>     array (
                                0 =>
                                    array (
                                        'id' => 1,
                                        'name' => 'Payment method 1',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>1,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',
                                        'price' => 10,
                                        'currency' => 'CZK',
                                    ),
                                1 =>
                                    array (
                                        'id' => 2,
                                        'name' => 'Payment method 2',
                                        'payment_description' => 'Bank transfer',
                                        'payment_id' =>2,
                                        'price_formatted' => '10.8',
                                        'total_price'=> 28,
                                        'total_price_formatted' => '28',    'price' => 20,
                                        'currency' => 'CZK',
                                    ),
                            )
                        ),
                )),
        );
        echo json_encode($k);

    }
}