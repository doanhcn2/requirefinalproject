<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 06/01/2018
 * Time: 15:53
 */

namespace Magenest\Shop\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;
class Carts  extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    public function execute ( ) {
        $k =  array (
            'total_count' => 2,
            'total_price' => 300,
            'currency' => 'CZK',
            'discounts' =>
                array (
                    0 =>
                        array (
                            'id' => 1,
                            'quantity' => 1,
                            'discount' =>
                                array (
                                    'id' => 5,
                                    'value' => 200,
                                ),
                        ),
                    1 =>
                        array (
                            'id' => 2,
                            'quantity' => 1,
                            'discount' =>
                                array (
                                    'id' => 5,
                                    'value' => 200,
                                ),
                        ),
                ),
            'products' =>
                array (
                    0 =>
                        array (
                            'id' => 1,
                            'quantity' => 1,
                            'product' =>
                                array (
                                    'id' => 5,
                                    'name' => 'Blue shirt',
                                    'price' => 150,
                                    'discount_price' => NULL,
                                    'image' => 'http://images.com/image001.jpg',
                                    'color_id' => 2,
                                    'size_id' => 8,
                                ),
                        ),
                    1 =>
                        array (
                            'id' => 1,
                            'quantity' => 3,
                            'product' =>
                                array (
                                    'id' => 5,
                                    'name' => 'Blue shirt',
                                    'price' => 150,
                                    'discount_price' => NULL,
                                    'image' => 'http://images.com/image001.jpg',
                                    'color_id' => 2,
                                    'size_id' => 8,
                                ),
                        ),
                ),
        );
        echo json_encode($k);

    }
}