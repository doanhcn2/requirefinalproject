<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 09:18
 */

namespace Magenest\Shop\Controller\Index;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class NavigationDrawer extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     *  private long id;
    private String name;
    private String description;
    private String url;
    private String logo;

    @SerializedName("google_ua")
    private String googleUa;
    private String language;
    private String currency;

    @SerializedName("flag_icon")
    private String flagIcon;
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out[]=[
            'id' => 1,
            'name' => 'Men',
            'url' => 'http://127.0.0.1/magento/ee2/',
            'children' =>[
                [ 'id' => 2,
                    'name' => 'Coat',
                    'url' => 'http://127.0.0.1/magento/ee2/',],
                [ 'id' => 3,
                    'name' => 'Sweater',
                    'url' => 'http://127.0.0.1/magento/ee2/',],

            ]

        ]; $out[]=[
        'id' => 4,
        'name' => 'Womane',
        'url' => 'http://127.0.0.1/magento/ee2/',
        'children' =>[
            [ 'id' => 5,
                'name' => 'Coat',
                'url' => 'http://127.0.0.1/magento/ee2/',],
            [ 'id' => 6,
                'name' => 'Sweater',
                'url' => 'http://127.0.0.1/magento/ee2/',],

        ]

    ];
        $k['records'] = $out;
        //$result->setData($out);

        //return $result;
        echo json_encode($k);
    }
}