<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 10:34
 */

namespace Magenest\Shop\Controller\Index;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class Banners extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     *  private long id;
    private String name;
    private String description;
    private String url;
    private String logo;

    @SerializedName("google_ua")
    private String googleUa;
    private String language;
    private String currency;

    @SerializedName("flag_icon")
    private String flagIcon;
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out[]=[
            'id' => 1,
            'bannerId' => 1,

            'name' => 'Xmas sales',
            'iamgeUrl' => 'http://92d823a9.ngrok.io/magento/ee2/pub/media/wysiwyg/home/home-t-shirts.png',
            'target' => 'google_ua',
        ];
        $out[]=[
            'id' => 2,
            'bannerId' => 2,
            'name' => 'Clearance',
            'iamgeUrl' => 'http://92d823a9.ngrok.io/magento/ee2/pub/media/wysiwyg/home/home-eco.jpg',
            'target' => 'google_ua',
        ];
        $k['records'] = $out;
        //$result->setData($out);

        //return $result;
        echo json_encode($k);
    }
}