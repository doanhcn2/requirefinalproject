<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 10:18
 */

namespace Magenest\Shop\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class Products extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out[]=[
            'product_id' => 1,
            'id' => 1,
            'name' => 'Men',
            'url' => 'http://127.0.0.1/magento/ee2/',
            'price' => 30,
            'priceFormatted' => '56.45$',
            'currency'=> 'USD',
            'code'=>'sku1',
            'description' => 'Description',
            'main_image' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
            'main_image_high_res' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
            'category' =>1,
            'brand' => 'Sony',
            'discount_price' => 9,
            'discount_price_formatted' => '9.0',
            'remote_id' =>1,
            'variants' =>  array (
                0 =>
                    array (
                        'id' => 9,
                        'code' => 'sku',
                        'images' => 'na',
                        'productVariantId' => '9',
                        'product_variant_id' => '9',
                        'name' =>'Tee with Purple',
                        'price' =>  5,
                        'price_formatted' => '10.8',
                        'discount_price' => 3.5,
                        'discount_price_formatted' =>'3.5',
                        'category' => 1,
                        'productVariantDescription' => 'Product Variant Description',
                        'product_variant_description' => 'Product Variant Description',
                        'imageUrl' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
                        'productId'=> 1,
                        'remoteId' => 1,
                        'color' => 1,
                        'size' => 2,
                        'total_price'=> 28,
                        'total_price_formatted' => '28',
                        'price' => 10,
                        'currency' => 'CZK',

                    ),
                1 =>
                    array (
                        'id' => 10,
                        'code' => 'sku2',
                        'images' => 'na',
                        'productVariantId' => '10',
                        'product_variant_id' => '10',
                        'name' =>'Tee with Purple',
                        'price' =>  5,
                        'price_formatted' => '10.8',
                        'discount_price' => 3.5,
                        'discount_price_formatted' =>'3.5',
                        'category' => 1,
                        'productVariantDescription' => 'Product Variant Description',
                        'product_variant_description' => 'Product Variant Description',
                        'imageUrl' => 'http://pubvn.tv/movie/pictures/movie/18696/poster_01_mpf25295.jpg',
                        'image_url' => 'http://pubvn.tv/movie/pictures/movie/18696/poster_01_mpf25295.jpg',
                        'productId'=> 1,
                        'product_id'=> 1,
                        'remoteId' => 1,
                        'color' => 1,
                        'size' => 3,
                        'total_price'=> 28,
                        'total_price_formatted' => '28',
                        'price' => 10,
                        'currency' => 'CZK',
                    )
            )

        ];
      /*  $out[]=[
            'id' => 2,
            'name' => 'Men',
            'url' => 'http://127.0.0.1/magento/ee2/',
            'price' => '30',
            'priceFormatted' => '56.45$',
            'currency'=> 'USD',
            'code'=>'sku1',
            'description' => 'Description',
            'main_image' => '',
            'mainImageHighRes' =>'',
            'category' =>1

        ];*/

        $metadata = [];

        $metadata[] = [
            'type' => 'color',
             'values' => [
                 [
            'id' =>1,
            'code' =>'purple',
            'img' => 'http://lghttp.47954.nexcesscdn.net/801F3A5/barnett_v2/skin/frontend/planosynergy/default/images/swatches/pink-swatch.png',
            'value' => 'purple',
            'colorId'=>1,
            'name'=>'White',
            'hexValue'=> '1233',
            'product_variant' =>[9,10],
            'ProductVariant' =>[9,10],
            'productVariants' =>[9,10]
             ]
             ]
        ];

      /*  $metadata['sorting'] = [
            'price' =>'price',
            'relevance' =>'relevance',

        ];*/
        //$k['products'][] = [$product1,$product2];
       /* $k['filters'][] =  [
            'id' =>1,
            'name'=>'Color',
            'type'=>'select',
            'label'=>'Color',
            'values' =>['green','red','purple']
        ];;*/
       // $k['records']['products'] =[$product1,$product2];
        //$k = [];
        $sizeData = [
            ['id' => 1, 'value' => 'S'],
            ['id' => 2, 'value' => 'M'],
            ['id' => 3, 'value' => 'L'],


        ];
        $k['records'] =$out;
        $k['metadata.filters']= $metadata;

        $k['metadata.filters'][] =[
            'type' =>'select',
            'values' =>[
            [
                'id' => 2, 'value' => 'S',
                'product_variant' =>[[9]],
                'ProductVariant' =>[[9]],
                'productVariants' =>[[9]]
            ]
            ]
        ];
        $k['metadata.filters'][] =[
            'type' =>'select',
            'values' =>[
            [
                'id' => 3, 'value' => 'M',
                'product_variant' =>[[10]],
                'ProductVariant' =>[[10]],
                'productVariants' =>[[10]]
            ]
            ]
        ];
        $k['metadata.filters'][] =[
            'type' =>'range',
            'values' =>[

                0 => 10,
                1 => 200

            ]
        ];

        echo json_encode($k);
    }
}