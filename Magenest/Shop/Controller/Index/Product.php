<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 04/01/2018
 * Time: 16:07
 */

namespace Magenest\Shop\Controller\Index;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class Product extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *@property (nullable, nonatomic, retain) NSString *code;
@property (nullable, nonatomic, retain) id images;
@property (nullable, nonatomic, retain) NSNumber *productVariantID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSString *priceFormatted;
@property (nullable, nonatomic, retain) NSNumber *discountPrice;
@property (nullable, nonatomic, retain) NSString *discountPriceFormatted;
@property (nullable, nonatomic, retain) NSNumber *category;
@property (nullable, nonatomic, retain) NSString *currency;
@property (nullable, nonatomic, retain) NSString *productVariantDescription;
@property (nullable, nonatomic, retain) NSString *imageURL;
@property (nullable, nonatomic, retain) NSNumber *productID;
@property (nullable, nonatomic, retain) NSNumber *remoteID;
@property (nullable, nonatomic, retain) BFProductVariantColor *color;
@property (nullable, nonatomic, retain) BFCartProductItem *inCart;
@property (nullable, nonatomic, retain) NSSet<BFOrderItem *> *inOrders;
@property (nullable, nonatomic, retain) BFWishlistItem *inWishlist;
@property (nullable, nonatomic, retain) BFProduct *product;
@property (nullable, nonatomic, retain) BFProductVariantSize *size;

     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out[]=[
            'product_id' => 1,
            'id' => 1,
            'name' => 'Men',
            'url' => 'http://127.0.0.1/magento/ee2/',
            'price' => 30,
            'priceFormatted' => '56.45$',
            'currency'=> 'USD',
            'code'=>'sku1',
            'imageUrl' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
            'main_image' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
            'main_image_high_res' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',

            'description' => 'Description',
            'main_image' => '',
            'main_image_high_res'=>'',
            'category' =>1,
            'brand' => 'Sony',
            'discount_price' => 9,
            'discount_price_formatted' => '9.0',
            'remote_id' =>1,
            'variants' =>  array (
                0 =>
                    array (
                        'code' => 'sku',
                        'images' => 'na',
                        'productVariantId' => '9',
                        'id' =>9,
                        'name' =>'Tee with Purple',
                        'price_formatted' => '10.8',
                        'discount_price' => 3.5,
                        'discount_price_formatted' =>'3.5',
                        'category' => 1,
                        'description' => 'Product Variant Description',
                        'imageUrl' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
                        'main_image' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
                        'images' => ['http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg'],
                        'remoteId' => 1,
                        'color' => 1,
                        'size' =>1,
                        'total_price'=> 28,
                        'total_price_formatted' => '28',

                        'price' =>  5,            'price' => 10,
                        'currency' => 'CZK',
                    ),
                1 =>
                    array (
                        'code' => 'sku2',
                        'images' => 'na',
                        'id' => 10,
                        'product_variant_id' => '10',
                        'name' =>'Tee with Purple',
                        'price' =>  5,
                        'price_formatted' => '10.8',
                        'discount_price' => 3.5,
                        'discount_price_formatted' =>'3.5',
                        'category' => 1,
                        'description' => 'Product Variant Description',
                        'imageUrl' => 'http://pubvn.tv/movie/pictures/movie/18696/poster_01_mpf25295.jpg',
                        'main_image' => 'http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg',
                        'images' => ['http://pubvn.tv/movie/pictures/movie/18901/poster_01_ntn53949.jpg'],
                        'image_url' => 'http://pubvn.tv/movie/pictures/movie/18696/poster_01_mpf25295.jpg',

                        'product_id'=> 1,
                        'remoteId' => 1,
                        'color' => 1,
                        'size' => 2,
                        'total_price'=> 28,
                        'total_price_formatted' => '28',
                        'price' => 10,
                        'currency' => 'CZK',
                    )
            )

        ];
        /*  $out[]=[
              'id' => 2,
              'name' => 'Men',
              'url' => 'http://127.0.0.1/magento/ee2/',
              'price' => '30',
              'priceFormatted' => '56.45$',
              'currency'=> 'USD',
              'code'=>'sku1',
              'description' => 'Description',
              'main_image' => '',
              'mainImageHighRes' =>'',
              'category' =>1

          ];*/

        $metadata = [];
        $metadata[] = [
            'id' =>1,
            'colorId'=>1,
            'name'=>'White',
            'hexValue'=> '1233',
            'product_variants' =>[1]
        ];

        /*  $metadata['sorting'] = [
              'price' =>'price',
              'relevance' =>'relevance',

          ];*/
        //$k['products'][] = [$product1,$product2];
        /* $k['filters'][] =  [
             'id' =>1,
             'name'=>'Color',
             'type'=>'select',
             'label'=>'Color',
             'values' =>['green','red','purple']
         ];;*/
        // $k['records']['products'] =[$product1,$product2];
        //$k = [];
        $k =$out;
       // $k['metadata.filters'] = $metadata;
        //$k['metadata'][] =$metadata;
        //$result->setData($out);

        //return $result;
        echo json_encode($k);
    }
}