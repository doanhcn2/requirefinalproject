<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 10:43
 */
namespace Magenest\Shop\Controller\Users;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class Shop extends\Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     *  private long id;
    private String name;
    private String description;
    private String url;
    private String logo;

    @SerializedName("google_ua")
    private String googleUa;
    private String language;
    private String currency;

    @SerializedName("flag_icon")
    private String flagIcon;
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out=[
            'id' => 1,
            'description' => 'Mockshop',
            'url' => 'http://127.0.0.1/magento/ee2/',
            'google_ua' => 'google_ua',
            'logo' => 'logo',
            'language' => 'en',
            'currency' => 'USD',
            'flag_icon' => 'icon'];

        //$result->setData($out);

        //return $result;
        echo json_encode($out);
    }
}