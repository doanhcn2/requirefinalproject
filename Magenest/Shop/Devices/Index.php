<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 21/12/2017
 * Time: 11:27
 */
namespace Magenest\Shop\Controller\Devices;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;
class Index extends\Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory

    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $out = [];
        $out[]=[
            'token' => 'thetoken',
            'id' => '2',
         ];

        echo json_encode($out);
    }
}