<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Shop\Model\Category;

/**
 * @codeCoverageIgnore
 */
class CategoryProductLink extends \Magento\Framework\Api\AbstractExtensibleObject implements
    \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterface
{
    /**#@+
     * Constant for confirmation status
     */
    const KEY_SKU = 'sku';
    const KEY_POSITION = 'position';
    const KEY_CATEGORY_ID = 'category_id';
    const KEY_PRODUCT_ID = 'product_id';
    /**#@-*/

    /**
     * {@inheritdoc}
     */
    public function getSku()
    {
        return $this->_get(self::KEY_SKU);
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return $this->_get(self::KEY_POSITION);
    }

    /**
     * {@inheritdoc}
     */
    public function getCategoryId()
    {

        return $this->_get(self::KEY_CATEGORY_ID);
    }

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku)
    {
        return $this->setData(self::KEY_SKU, $sku);
    }

    /**
     * @param int $position
     * @return $this
     */
    public function setPosition($position)
    {
        return $this->setData(self::KEY_POSITION, $position);
    }


    public function setCategoryId($categoryId)
    {
        $xxx = 1;
        return $this->setData(self::KEY_CATEGORY_ID, $categoryId);
    }

    /**
     * {@inheritdoc}
     *
     * @return \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * {@inheritdoc}
     *
     * @param \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Magento\Catalog\Api\Data\CategoryProductLinkExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
//        return $this->_get('id');
        return $this->_get(self::KEY_CATEGORY_ID);
    }

    /**
     *
     * @param string $row_id
     * @return $this
     */
    public function setId($row_id)
    {
        return $this->setData('id', $row_id);
    }

    /**
     *
     * @return string
     */
    public function getCode()
    {
//        return $this->_get('code');
        return $this->_get(self::KEY_CATEGORY_ID);
    }
    /**
     * Set category id
     *
     * @param string $sku
     * @return $this
     */
    public function setCode($sku)
    {
        return $this->setData('code', $sku);
    }
    /**
     * {@inheritdoc}
     */
    public function getProductId()
    {
        return $this->_get('product_id');
    }
    /**
     * Set category id
     *
     * @param string $entity_id
     * @return $this
     */
    public function setProductId($entity_id)
    {
        return $this->setData('product_id', $entity_id);
    }
    /**
     * {@inheritdoc}
     */
    public function getUrl()
    {
        return $this->_get('url');
    }
    /**
     * Set category id
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        return $this->setData('url', $url);
    }
    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->_get('name');
    }
    /**
     * Set category id
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData('name', $name);
    }
    /**
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->_get('price');
    }
    /**
     * Set category id
     *
     * @param int $price
     * @return $this
     */
    public function setPrice($price)
    {
        return $this->setData('price', $price);
    }
    /**
     *
     * @return string
     */
    public function getPriceFormatted()
    {
        return $this->_get('price_formatted');
    }
    /**
     * Set category id
     *
     * @param string $price_formatted
     * @return $this
     */
    public function setPriceFormatted($price_formatted)
    {
        return $this->setData('price_formatted', $price_formatted);
    }
    /**
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->_get('currency');
    }
    /**
     * Set category id
     *
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        return $this->setData('currency', $currency);
    }
    /**
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_get('description');
    }
    /**
     * Set category id
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }
    /**
     *
     * @return string
     */
    public function getMainImage()
    {
        return $this->_get('main_image');
    }
    /**
     * Set category id
     *
     * @param string $small_image
     * @return $this
     */
    public function setMainImage($small_image)
    {
        return $this->setData('main_image', $small_image);
    }
    /**
     *
     * @return string
     */
    public function getMainImageHighRes()
    {
        return $this->_get('main_image_high_res');
    }
    /**
     * Set category id
     *
     * @param string $main_image
     * @return $this
     */
    public function setMainImageHighRes($main_image)
    {
        return $this->setData('main_image_high_res', $main_image);
    }
    /**
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->_get('brand');
    }
    /**
     * Set category id
     *
     * @param string $entity_id
     * @return $this
     */
    public function setBrand($entity_id)
    {
        return $this->setData('brand', $entity_id);
    }
    /**
     *
     * @return int
     */
    public function getDiscountPrice()
    {
        return $this->_get('discount_price');
    }
    /**
     * Set category id
     *
     * @param int $discount_price
     * @return $this
     */
    public function setDiscountPrice($discount_price)
    {
        return $this->setData('discount_price', $discount_price);
    }
    /**
     *
     * @return string
     */
    public function getDiscountPriceFormatted()
    {
        return $this->_get('discount_price_formatted');
    }
    /**
     * Set category id
     *
     * @param string $discount_price_formatted
     * @return $this
     */
    public function setDiscountPriceFormatted($discount_price_formatted)
    {
        return $this->setData('discount_price_formatted', $discount_price_formatted);
    }
    /**
     * Set category id
     *
     * @param string $entity_id
     * @return $this
     */
    public function setRemoteId($entity_id)
    {
        return $this->setData('remote_id', $entity_id);
    }
    /**
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->_get('remote_id');
    }
}
