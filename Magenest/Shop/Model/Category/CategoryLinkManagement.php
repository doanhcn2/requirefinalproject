<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Shop\Model\Category;

/**
 * Class CategoryLinkManagement
 */
class CategoryLinkManagement implements \Magenest\Shop\Api\Category\CategoryLinkManagementInterface
{
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $productResource;

    /**
     * @var \Magento\Catalog\Api\CategoryLinkRepositoryInterface
     */
    protected $categoryLinkRepository;

    /**
     * @var \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterfaceFactory
     */
    protected $productLinkFactory;

    /**
     * @var \Magento\Framework\Indexer\IndexerRegistry
     */
    protected $indexerRegistry;

    protected $storeManagerInterFace;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Search\Api\SearchInterface
     */
    protected $_search;

    /**
     * @var \Magento\Framework\Api\Search\SearchCriteriaInterface
     */
    protected $_searchCriteria;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroup
     */
    protected $_filterGroup;

    /**
     * CategoryLinkManagement constructor.
     *
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterfaceFactory $productLinkFactory
     */
    public function __construct(
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterFace,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\Api\Search\SearchCriteriaInterface $searchCriteria,
        \Magento\Search\Api\SearchInterface $search,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->productLinkFactory = $productLinkFactory;
        $this->storeManagerInterFace = $storeManagerInterFace;
        $this->_filterGroup = $filterGroup;
        $this->_searchCriteria = $searchCriteria;
        $this->_search = $search;
        $this->_objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignedProducts($categoryId,$num,$size)
    {
        $category = $this->categoryRepository->get($categoryId);

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $products */
        $products = $category->getProductCollection();
        $products->addFieldToSelect('*');
        $products->setPage($num,$size);
        /** @var \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterface[] $links */
        $links = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($products->getItems() as $product) {
            /** @var \Magenest\Shop\Api\Data\Category\CategoryProductLinkInterface $link */
            $link = $this->productLinkFactory->create();
            $smallImage = $product->getSmallImage() ?
                $this->storeManagerInterFace->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getSmallImage() : '';
            $brand = $product->getVendor() ? $product->getVendor() : '';
            $imageUrl = $product->getSmallImage() ?
                $this->storeManagerInterFace->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage() : '';
            $currency = $this->storeManagerInterFace->getStore()->getCurrentCurrency()->getCode();
            $link->setSku($product->getSku())
                ->setPosition($product->getData('cat_index_position'))
                ->setCategoryId($category->getId())
                ->setId($product->getRowId())
                ->setProductId($product->getEntityId())
                ->setName($product->getName())
                ->setUrl($product->getProductUrl())
                ->setPrice(intval($product->getPrice()))
                ->setPriceFormatted(number_format($product->getPrice(),2))
                ->setDescription($product->getDescription())
                ->setCurrency($currency)
                ->setRemoteId($product->getEntityId())
                ->setBrand($brand)
                ->setCode($product->getSku())
                ->setMainImage($smallImage)
                ->setMainImageHighRes($imageUrl)
                ->setDiscountPrice(intval($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice()))
                ->setDiscountPriceFormatted(number_format($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice(),2))
            ;
            $links[] = $link;
        }
        return $links;
    }

}
