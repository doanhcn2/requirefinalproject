<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 02/02/2018
 * Time: 20:29
 */
namespace Magenest\Shop\Model\Order;

use Magenest\Shop\Api\Order\CustomerOrderInterface;

class CustomerOrder implements CustomerOrderInterface
{
    protected $orderCollectionFactory;

    protected $objectFactory;

    protected $vendorFactory;

    protected $typeConfigurable;

    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
    )
    {
        $this->objectFactory = $objectFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->vendorFactory = $vendorFactory;
        $this->typeConfigurable = $catalogProductTypeConfigurable;
    }

    /**
     * get all orders of a customer
     *
     * @param string $customerId
     * @return \Magenest\Shop\Api\Data\Order\CustomerOrderInterface
     */
    public function getAllCustomerOrders($customerId)
    {
        $collection = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $customerId)->load();
        $orders = [];

        foreach ($collection as $item) {
            /** @var \Magento\Sales\Model\Order $item */
            $orderData = $item->getData();
            $allOrderItems = $item->getItemsCollection();

            $orderItemsData = [];
            foreach ($allOrderItems as $orderItem) {
                /** @var \Magento\Sales\Model\Order\Item $orderItem */
                $parentByChild = $this->typeConfigurable->getParentIdsByChild($orderItem->getProductId());
                if (isset($parentByChild[0])){
                    continue;
                }

                $itemData = $orderItem->getData();
                $vendor = $this->vendorFactory->create()->load($orderItem->getData('udropship_vendor'));

                $itemData['vendor_name'] = $vendor->getVendorName();
                $orderItemsData[] = $itemData;
            }
            $orderData['items'] = $orderItemsData;
            $orders[] = $orderData;
        }

        return $this->objectFactory->addData([
                'result' => $orders
            ]);
    }

    /**
     * get a specific order of a customer
     *
     * @param string $customerId
     * @param string $id
     * @return mixed
     */
    public function getSpecificCustomerOrder($customerId, $id)
    {
        $collection = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('entity_id', $id)->load();
        $orders = [];

        foreach ($collection as $item) {
            /** @var \Magento\Sales\Model\Order $item */
            $orderData = $item->getData();
            $allOrderItems = $item->getItemsCollection();

            $orderItemsData = [];
            foreach ($allOrderItems as $orderItem) {
                $orderItemsData[] = $orderItem->getData();
            }
            $orderData['items'] = $orderItemsData;
            $orders[] = $orderData;
        }

        return $this->objectFactory->addData([
            'result' => $orders
        ]);
    }
}