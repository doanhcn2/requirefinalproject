<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:24
 */
namespace Magenest\Shop\Model\Facebook;

class FacebookLogin implements \Magenest\Shop\Api\Facebook\FacebookLoginInterface
{
    protected $helper;

    protected $customerFactory;

    protected $tokenFactory;

    public function __construct(
        \Magenest\SocialLogin\Helper\SocialLogin $helper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenFactory
    )
    {
        $this->helper = $helper;
        $this->customerFactory = $customerFactory;
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * @param string[] $userInfo
     * @return mixed
     */
    public function loginOrCreateNewCustomer($userInfo)
    {
        /** @var \Magento\Integration\Model\Oauth\Token $customerToken */
        $customerToken = $this->tokenFactory->create();
        if (is_array($userInfo)) {
            $username = $userInfo['name'];
            $userEmail = $userInfo['email'];
            $userFacebookId = $userInfo['id'];

            $customer = $this->helper->getCustomerByEmail($userEmail);

            if ($customer->getId()) {
                $tokenKey = $customerToken->createCustomerToken($customer->getId())->getToken();
                $customerData = $customer->getData();
                $customerData['access_token'] = $tokenKey;

                return [$customerData];
            } else {

                $data = [
                    'sendemail' => 0,
                    'confirmation' => 0,
                    'magenest_sociallogin_id' => $userInfo['id'],
                    'magenest_sociallogin_type' => 'facebook'
                ];

                $nameExploded = explode(' ', $username);

                $data['email'] = $userEmail;
                $data['firstname'] = $nameExploded[0];
                $data['lastname'] = $nameExploded[1];

                $newCustomer = $this->customerFactory->create();
                $newCustomer->setData($data);
                $newCustomer->save();

                $tokenKey = $customerToken->createCustomerToken($newCustomer->getId())->getToken();

                $customerData = $newCustomer->getData();
                $customerData['access_token'] = $tokenKey;

                return [$customerData];
            }
        } else {
            return false;
        }
    }
}