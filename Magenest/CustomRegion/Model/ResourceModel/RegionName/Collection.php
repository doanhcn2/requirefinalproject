<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CustomRegion\Model\ResourceModel\RegionName;

/**
 * Class Collection
 * @package Magenest\CustomRegion\Model\ResourceModel\RegionName
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'region_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Magenest\CustomRegion\Model\RegionName', 'Magenest\CustomRegion\Model\ResourceModel\RegionName');
    }
}
