<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CustomRegion\Model\ResourceModel;

/**
 * Class RegionName
 * @package Magenest\CustomRegion\Model\ResourceModel
 */
class RegionName extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('directory_country_region_name', 'region_id');
    }
}
