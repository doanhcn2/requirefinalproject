<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CustomRegion\Controller\Adminhtml\Region;

/**
 * Class NewAction
 * @package Magenest\Region\Controller\Adminhtml\Region
 */
class NewAction extends \Magento\Backend\App\Action
{
    protected $coreRegistry = null;
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magenest_CustomRegion::region_create';

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultPageFactory;

    /**
     * NewAction constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Create new Region
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
        $resultPageFactory = $this->resultPageFactory->create();
        $FormData = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($FormData)) {
            $model = $this->_objectManager->create('Magenest\CustomRegion\Model\Region');
            $model->setData($FormData);
            $this->coreRegistry->register('magenest_region', $model);
        }
        $countryHelper = $this->_objectManager->get('Magento\Directory\Model\Config\Source\Country');
        $this->coreRegistry->register('magenest_region_country_list', $countryHelper->toOptionArray());

        return $resultPageFactory;
    }
}
