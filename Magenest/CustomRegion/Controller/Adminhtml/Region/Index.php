<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CustomRegion\Controller\Adminhtml\Region;

/**
 * Class Index
 * @package Magenest\CustomRegion\Controller\Adminhtml\Region
 */
class Index extends \Magenest\CustomRegion\Controller\Adminhtml\Region
{
    /**
     * @return $this
     */
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            'Regions Manager',
            'Regions Manager'
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Customers'));
        $resultPage->getConfig()->getTitle()
            ->prepend('Regions Manager');

        return $resultPage;
    }
}
