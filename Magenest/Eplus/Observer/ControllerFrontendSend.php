<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 26/10/2017
 * Time: 08:27
 */
namespace Magenest\Eplus\Observer;

use Magento\Framework\Event\Observer;

class ControllerFrontendSend implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magenest\Eplus\Model\ResourceModel\ProductResult\CollectionFactory
     */
    protected $_productResultFactory;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * ControllerFrontendSend constructor.
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magenest\Eplus\Model\ResourceModel\ProductResult\CollectionFactory $productResultFactory
     */
    public function __construct(
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magenest\Eplus\Model\ResourceModel\ProductResult\CollectionFactory $productResultFactory
    ) {
        $this->_debugHelper = $debugHelper;
        $this->_productResultFactory = $productResultFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $request = $observer->getData('request');
            if (strpos($request->getPathInfo(), '/rest/eplus/store') !== false) {
                $pathInfoArr = explode('/', $request->getPathInfo());
                if (sizeof($pathInfoArr) > 6
                    && $pathInfoArr[1] == 'rest'
                    && $pathInfoArr[2] == 'eplus'
                    && $pathInfoArr[3] == 'store'
                    && $pathInfoArr[5] == 'product') {
                    $oldData = $this->_productResultFactory->create()
                        ->addFieldToFilter('store_id', $pathInfoArr[4])
                        ->addFieldToFilter('product_id', $pathInfoArr[6])
                        ->load()
                        ->getFirstItem();
                    if ($oldData->getId()) {
                        $observer->getData('response')->setBody($oldData->getResultValue());
                    } else {
                        $headerContentType = $observer->getData('response')->getHeader('Content-Type');
                        if ($headerContentType != null && $headerContentType->getMediaType() == 'application/json') {
//                            $this->_productResultFactory->create()
//                                ->getNewEmptyItem()
//                                ->setData([
//                                    'store_id' => $pathInfoArr[4],
//                                    'product_id' => $pathInfoArr[6],
//                                    'result_value' => $observer->getData('response')->getBody()
//                                ])
//                                ->save();
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('event_error-', $e->getMessage());
        }
    }
}