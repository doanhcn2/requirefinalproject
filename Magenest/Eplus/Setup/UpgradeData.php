<?php

namespace Magenest\Eplus\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        if (version_compare($context->getVersion(), '1.4.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $this->addBanners($eavSetup);
            $this->addBannerImage($eavSetup);
            $setup->endSetup();
        }
    }
    private function addBannerImage($eavSetup)
    {
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'category_type', [
            'type' => 'int',
            'label' => 'Category Type',
            'input' => 'select',
            'source' => '\Magenest\Eplus\Model\Source\Category\Type',
            'visible' => true,
            'required' => false,
            'sort_order' => 11,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group' => 'General Information',
        ]);
    }
    private function addBanners($eavSetup)
    {
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'banner', [
            'type' => 'varchar',
            'label' => 'Category Banner',
            'input' => 'media_image',
            'backend' => 'Magenest\Eplus\Model\Category\Attribute\Backend\Banner',
            'visible' => true,
            'required' => false,
            'sort_order' => 11,
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
            'group' => 'General Information',
        ]);
    }
}