<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class CategorySuggestApi
 * @package Magenest\Eplus\Model
 */
class SearchSuggestApi implements \Magenest\Eplus\Api\SearchSuggestApiInterface
{
    /**
     * @var \Magento\Search\Model\AutocompleteInterface
     */
    protected $_autocomplete;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * SearchSuggestApi constructor.
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Search\Model\AutocompleteInterface $autocomplete
     * @param \Magento\Framework\DataObject $dataObject
     */
    public function __construct(
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Search\Model\AutocompleteInterface $autocomplete,
        \Magento\Framework\DataObject $dataObject
    ) {
        $this->_debugHelper = $debugHelper;
        $this->_autocomplete = $autocomplete;
        $this->_dataObject = $dataObject;
    }

    public function getSearchSuggest()
    {
        try {
            $result = [
                'api_status' => 0
            ];
            $items = $this->_autocomplete->getItems();
            if (sizeof($items) > 0) {
                $autocompleteData = [];
                $autocompleteNData = [];
                $autocompleteAData = [];
                foreach ($items as $item) {
                    $title = $item->getTitle();
                    if (ctype_alpha($title[0])) {
                        $autocompleteAData[] = [
                            'key' => $title
                        ];
                    } else {
                        $autocompleteNData[] = [
                            'key' => $title
                        ];
                    }
                }
                usort($autocompleteAData, function ($a, $b) {
                    return strcmp($a['key'], $b['key']);
                });
                usort($autocompleteNData, function ($a, $b) {
                    return strcmp($a['key'], $b['key']);
                });
                foreach ($autocompleteAData as $item) {
                    $autocompleteData[] = $item;
                }
                foreach ($autocompleteNData as $item) {
                    $autocompleteData[] = $item;
                }
                $result = [
                    'api_status' => 1,
                    'search' => $autocompleteData
                ];
            }
            return $this->_dataObject->addData($result);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('category', $e->getMessage());
            return $this->_dataObject->addData([
                'api_status' => 0
            ]);
        }
    }
}
