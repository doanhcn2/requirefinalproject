<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class CategoryProductApi
 * @package Magenest\Eplus\Model
 */
class CategoryProductApi implements \Magenest\Eplus\Api\CategoryProductApiInterface
{
    /**
     * @var \Magenest\Eplus\Helper\CategoryHelper
     */
    protected $_magenestCategoryHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * CategoryProductApi constructor.
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $dataObject
     * @param \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
     */
    public function __construct(
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $dataObject,
        \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
    )
    {
        $this->_debugHelper = $debugHelper;
        $this->_dataObject = $dataObject;
        $this->_magenestCategoryHelper = $magenestCategoryHelper;
    }

    /**
     * @param $store
     * @param $category
     * @return $this
     */
    public function getCategoryProduct($store, $category)
    {
        try {
            $categoryData = $this->_magenestCategoryHelper->getCategoryDetail($store, $category);
            return $this->_dataObject
                ->addData($categoryData);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('category', $e->getMessage());
            return $this->_dataObject
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
