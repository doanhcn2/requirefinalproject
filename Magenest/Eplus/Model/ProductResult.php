<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:03
 */
namespace Magenest\Eplus\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class ProductResult
 * @package Magenest\Eplus\Model
 */
class ProductResult extends  AbstractModel
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_eplus_product_result';

    /**
     * Event Object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_eplus_product_result';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Eplus\Model\ResourceModel\ProductResult');
    }
}
