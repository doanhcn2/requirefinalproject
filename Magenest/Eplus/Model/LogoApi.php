<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class LogoApi
 * @package Magenest\Eplus\Model
 */
class LogoApi implements \Magenest\Eplus\Api\LogoApiInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_fileStorageHelper;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var SimpleResultFactory
     */
    protected $_simpleResultFactory;

    /**
     * LogoApi constructor.
     * @param SimpleResultFactory $simpleResultFactory
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     */
    public function __construct(
        \Magenest\Eplus\Model\SimpleResultFactory $simpleResultFactory,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Store\Model\StoreFactory $storeFactory
    )
    {
        $this->_simpleResultFactory = $simpleResultFactory;
        $this->_debugHelper = $debugHelper;
        $this->_filesystem = $filesystem;
        $this->_fileStorageHelper = $fileStorageHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_objectFactory = $objectFactory;
        $this->_storeFactory = $storeFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getLogo($store)
    {
        try {
            $cachedResult = $this->_simpleResultFactory->create()
                ->getCollection()
                ->addFieldToFilter('result_key', 'store_logo_' . $store)
                ->getFirstItem();
            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue()) > 0) {
                return $this->_objectFactory->addData([
                    'logo' => $cachedResult->getResultValue(),
                    'api_status' => 1
                ]);
            } else {
                $currentStore = $this->_storeFactory->create()
                    ->load($store);
                $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
                $storeLogoPath = $this->_scopeConfig->getValue(
                    'design/header/logo_src',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $path = $folderName . '/' . $storeLogoPath;
                $logoUrl = $currentStore
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;

                if ($storeLogoPath !== null && $this->_isFile($path)) {
                    $url = $logoUrl;
                } elseif ($this->getLogoFile()) {
                    $url = $this->getViewFileUrl($this->getLogoFile());
                } else {
                    $url = $this->getViewFileUrl('images/logo.svg');
                }
                $this->_simpleResultFactory->create()
                    ->setData([
                        'result_key' => 'store_logo_' . $store,
                        'result_value' => $url
                    ])
                    ->save();
                return $this->_objectFactory->addData([
                    'logo' => $url,
                    'api_status' => 1
                ]);
            }

        } catch (\Exception $e) {
            $this->_debugHelper->debugString('logo', $e->getMessage());
            return $this->_objectFactory->addData([
                'api_status' => 0
            ]);
        }
    }

    protected function _isFile($filename)
    {
        if ($this->_fileStorageHelper->checkDbUsage() && !$this->getMediaDirectory()->isFile($filename)) {
            $this->_fileStorageHelper->saveFileToFilesystem($filename);
        }

        return $this->getMediaDirectory()->isFile($filename);
    }

    protected function getMediaDirectory()
    {
        return $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }
}
