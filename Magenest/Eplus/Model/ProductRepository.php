<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Eplus\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * Class ProductRepository
 * @package Magenest\Eplus\Model
 */
class ProductRepository implements \Magenest\Eplus\Api\ProductRepositoryInterface
{
    protected $_dataObject;

    protected $_productHelper;

    protected $collectionFactory;

    protected $collectionProcessor;

    protected $_catalogProductTypeConfigurable;

    protected $_layerResolver;

    /**
     * ProductRepository constructor.
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magenest\Eplus\Helper\ProductHelper $productHelper
     * @param \Magento\Framework\DataObjectFactory $dataObject
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface|null $collectionProcessor
     */
    public function __construct(
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magenest\Eplus\Helper\ProductHelper $productHelper,
        \Magento\Framework\DataObjectFactory $dataObject,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->_layerResolver = $layerResolver;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_productHelper = $productHelper;
        $this->_dataObject = $dataObject;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
    }

    /**
     * {@inheritdoc}
     */
    public function getList($store, $num, $size, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $searchResult = [
            'api_status' => 1,
            'product' => []
        ];
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $collection->load();
        $productResult = [];
        foreach ($collection->getItems() as $product) {
            $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($product->getEntityId());
            if (sizeof($parents) > 0) {
                if (!in_array($parents[0], $productResult)) {
                    $productResult[] = $parents[0];
                }
            } else {
                if (!in_array($product->getEntityId(), $productResult)) {
                    $productResult[] = $product->getEntityId();
                }

            }
        }
        $productResult = $this->getPage($num, $size, $productResult);
        foreach ($productResult as $item) {
            $productDetail = $this->_productHelper->getProductFewDetail($store, $item);
            if ($productDetail != null && !in_array($productDetail, $searchResult['product'])) {
                $searchResult['product'][] = $productDetail;
            }
        }
        $filters = [];
        $layerFilter = $this->_layerResolver->get();
        $layerFilter->prepareProductCollection($collection);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $fill = $objectManager->create('Magento\Catalog\Model\Layer\Category\FilterableAttributeList');
        $filterList = new \Magento\Catalog\Model\Layer\FilterList($objectManager,$fill);
        foreach ($filterList->getFilters($layerFilter) as $filter) {
            if ($filter->getItemsCount() > 0) {
                $values = [];
                $values[] = [
                    'label' => 'Select Option',
                    'value' => 'null',
                    'count' => 0
                ];
                foreach ($filter->getItems() as $item ) {
                    $label = strip_tags($item->getLabel());
                    $values[] = [
                        'label' => $label,
                        'value' => $item->getValue(),
                        'count' => $item->getCount()
                    ];
                };
                $filterName = $filter->getName();
                if (!is_string($filterName)) {
                    $filterName = $filterName->getText();
                }
                $filters[] = [
                    'label' => $filterName,
                    'name' => $filter->getRequestVar(),
                    'value' => $values,
                    'type' => 'select'
                ];
            }
        }
        $newFilters = [];
        foreach ($filters as $filtersItem) {
            if (sizeof($newFilters) > 0) {
                $exist = false;
                foreach ($newFilters as &$newFiltersItem) {
                    if ($newFiltersItem['label'] == $filtersItem['label']) {
                        $exist = true;
//                        $oldValues = $newFiltersItem['value'];
//                        foreach ($filtersItem['value'] as $value) {
//                            $oldValues[] = $value;
//                        }
//                        $newFiltersItem['value'] = $oldValues;
                        break;
                    }
                }
                if (!$exist) {
                    if (sizeof($filtersItem['value'] > 2) && $filtersItem['name'] != 'cat') {
                        $newFilters[] = $filtersItem;
                    }
                }
            } else {
                if (sizeof($filtersItem['value'] > 2)) {
                    $newFilters[] = $filtersItem;
                }
            }
        }
        $filters = $newFilters;

        if (sizeof($filters) > 0) {
            usort($filters, function ($a, $b) {
                if (strpos($a['name'], 'cat') !== false) {
                    return -1;
                }
                if ($a['name'] == 'price') {
                    return 1;
                }
                return 0;
            });
        }
        $searchResult['metadata'] = $this->_dataObject->create()
            ->addData([
                'filters' => $filters
            ]);
        $banners = [];
        foreach ($searchCriteria->getFilterGroups() as $filters){
            foreach ($filters->getFilters() as $filter){
                if($filter->getField() == 'category_id' && $filter->getConditionType() == 'eq'){
                    $banners = $this->getBanner($store, $filter->getValue());
                    break;
                }
            }
        }
        if(isset($banners)) $searchResult['banners'] = $banners;
        $xx = $this->_dataObject->create()
            ->addData($searchResult);
        return $xx;
    }

    private function getCollectionProcessor()
    {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                'Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor'
            );
        }
        return $this->collectionProcessor;
    }

    protected function getPage($pageNum, $pageSize, $list) {
        $result = [];
        if (sizeof($list) > $pageSize * ($pageNum - 1)) {
            $result = array_slice($list, $pageSize * ($pageNum - 1), $pageSize);
        }
        return $result;
    }

    protected function getBanner($store_id,$category_id){//\Magento\Catalog\Model\CategoryFactory

        $result = [];
        $category = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Catalog\Model\CategoryFactory')
            ->create()
            ->load($category_id);
        $store = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Store\Model\StoreManagerInterface')
            ->getStore($store_id);
        $banners = $category->getBanner();
        if(isset($banners)){
            $banners = json_decode($banners);
            foreach ($banners as $banner){
                $result[] = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $banner;
            }
        }
        return $result;
    }
}
