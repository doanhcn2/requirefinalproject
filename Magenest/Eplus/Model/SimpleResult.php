<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:03
 */
namespace Magenest\Eplus\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class SimpleResult
 * @package Magenest\Eplus\Model
 */
class SimpleResult extends  AbstractModel
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_eplus_simple_result';

    /**
     * Event Object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_eplus_simple_result';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Eplus\Model\ResourceModel\SimpleResult');
    }
}
