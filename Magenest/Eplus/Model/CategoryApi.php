<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class CategoryApi
 * @package Magenest\Eplus\Model
 */
class CategoryApi implements \Magenest\Eplus\Api\CategoryApiInterface
{
    /**
     * @var \Magenest\Eplus\Helper\CategoryHelper
     */
    protected $_magenestCategoryHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * HomeApi constructor.
     * @param \Magento\Framework\DataObject $dataObject
     * @param \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
     */
    public function __construct(
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $dataObject,
        \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
    )
    {
        $this->_debugHelper = $debugHelper;
        $this->_dataObject = $dataObject;
        $this->_magenestCategoryHelper = $magenestCategoryHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getCategory($store)
    {
        try {
            $categoryData = $this->_magenestCategoryHelper->getHomeCategoryByStore($store);
            if (sizeof($categoryData) > 0) {
                return $this->_dataObject
                    ->addData([
                        'api_status' => 1,
                        'category' => $categoryData
                    ]);
            } else {
                return $this->_dataObject
                    ->addData([
                        'api_status' => 0
                    ]);
            }
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('category', $e->getMessage());
            return $this->_dataObject
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
