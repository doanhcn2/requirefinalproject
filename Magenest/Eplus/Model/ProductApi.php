<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class ProductApi
 * @package Magenest\Eplus\Model
 */
class ProductApi implements \Magenest\Eplus\Api\ProductApiInterface
{
    /**
     * @var \Magenest\Eplus\Helper\ProductHelper
     */
    protected $_magenestProductHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var ResourceModel\ProductResult\CollectionFactory
     */
    protected $_productResultFactory;

    /**
     * ProductApi constructor.
     * @param ResourceModel\ProductResult\CollectionFactory $productResultFactory
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magenest\Eplus\Model\ResourceModel\ProductResult\CollectionFactory $productResultFactory,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_productResultFactory = $productResultFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_magenestProductHelper = $magenestProductHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct($store, $id)
    {
        try {
//            $cachedResult = $this->_productResultFactory->create()
//                ->addFieldToFilter('store_id', $store)
//                ->addFieldToFilter('product_id', $id)
//                ->load()
//                ->getFirstItem();
//            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue()) > 0) {
//                $result = $this->_objectFactory
//                    ->addData([
//                        'api_status' => 2
//                    ]);
//                return $result;
//            } else {
//                $productInfo = $this->_magenestProductHelper->getProductFullDetail($store, $id);
//                return $this->_objectFactory
//                    ->addData([
//                        'result' => $productInfo,
//                        'api_status' => 1
//                    ]);
//            }
            $productInfo = $this->_magenestProductHelper->getProductFullDetail($store, $id);
            return $this->_objectFactory
                ->addData([
                    'result' => $productInfo,
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
