<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:58
 */
namespace Magenest\Eplus\Model;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\CredentialsValidator;
use Magento\Integration\Model\Oauth\Token as Token;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Oauth\Helper\Oauth as OauthHelper;
use Unirgy\SimpleLicense\Exception;

/**
 * Class HomeApi
 * @package Magenest\Eplus\Model
 */
class CustomerTokenService implements \Magenest\Eplus\Api\CustomerTokenServiceInterface
{
    /**
     * Token Modeluse Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\CredentialsValidator;
use Magento\Integration\Model\Oauth\Token as Token;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Framework\Exception\AuthenticationException;
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    protected $_oauthHelper;

    /**
     * Customer Account Service
     *
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @var \Magento\Integration\Model\CredentialsValidator
     */
    private $validatorHelper;

    /**
     * Token Collection Factory
     *
     * @var TokenCollectionFactory
     */
    private $tokenModelCollectionFactory;

    /**
     * @var RequestThrottler
     */
    private $requestThrottler;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    protected $_objectFactory;

    /**
     * Initialize service
     *
     * @param \Magento\Framework\DataObject $objectFactory
     * @param TokenModelFactory $tokenModelFactory
     * @param AccountManagementInterface $accountManagement
     * @param TokenCollectionFactory $tokenModelCollectionFactory
     * @param \Magento\Integration\Model\CredentialsValidator $validatorHelper
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     */
    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        TokenModelFactory $tokenModelFactory,
        AccountManagementInterface $accountManagement,
        TokenCollectionFactory $tokenModelCollectionFactory,
        CredentialsValidator $validatorHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        OauthHelper $oauthHelper
    ) {
        $this->_oauthHelper = $oauthHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->accountManagement = $accountManagement;
        $this->tokenModelCollectionFactory = $tokenModelCollectionFactory;
        $this->validatorHelper = $validatorHelper;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerAccessToken($username, $password)
    {
        $this->validatorHelper->validate($username, $password);
        $this->getRequestThrottler()->throttle($username, RequestThrottler::USER_TYPE_CUSTOMER);
        try {
            $customerDataObject = $this->accountManagement->authenticate($username, $password);
        } catch (\Exception $e) {
            $this->getRequestThrottler()->logAuthenticationFailure($username, RequestThrottler::USER_TYPE_CUSTOMER);
            throw new AuthenticationException(
                __('You did not sign in correctly or your account is temporarily disabled.')
            );
        }
        $this->getRequestThrottler()->resetAuthenticationFailuresCount($username, RequestThrottler::USER_TYPE_CUSTOMER);
        $token = $this->tokenModelFactory->create()->createCustomerToken($customerDataObject->getId())->getToken();
        return $this->_objectFactory
            ->addData([
                'access_token' => $token
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function createVendorAccessToken($username, $password)
    {
        $this->validatorHelper->validate($username, $password);
        $this->getRequestThrottler()->throttle($username, 3);
        try {
            /**
             * @var $vendor \Unirgy\Dropship\Model\Vendor
             */
            $vendor = $this->_vendorFactory->create();
            $vendorResult = $vendor->authenticate($username, $password);
            if (!$vendorResult) {
                throw new Exception();
            }
        } catch (\Exception $e) {
            $this->getRequestThrottler()->logAuthenticationFailure($username, 3);
            throw new AuthenticationException(
                __('You did not sign in correctly or your account is temporarily disabled.')
            );
        }
        $this->getRequestThrottler()->resetAuthenticationFailuresCount($username, 3);
        $token = $this->tokenModelFactory->create();
        $token->setVendorId($vendor->getId());
        $token->setUserType(5);
        $token->setType(Token::TYPE_ACCESS);
        $token->setToken($this->_oauthHelper->generateToken());
        $token->setSecret($this->_oauthHelper->generateTokenSecret());
        $token->save();
        $vendorToken = $token->getToken();
        return $this->_objectFactory
            ->addData([
                'access_token' => $vendorToken
            ]);
    }

    /**
     * Revoke token by merchant id.
     *
     * The function will delete the token from the oauth_token table.
     *
     * @param int $merchantId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function revokeVendorAccessToken($merchantId)
    {
        $tokenCollection = $this->tokenModelCollectionFactory->create()->addFieldToFilter('vendor_id', $merchantId);
        if ($tokenCollection->getSize() == 0) {
            return true;
        }
        try {
            foreach ($tokenCollection as $token) {
                $token->delete();
            }
        } catch (\Exception $e) {
            throw new LocalizedException(__('The tokens could not be revoked.'));
        }
        return true;
    }

    /**
     * Get request throttler instance
     *
     * @return RequestThrottler
     * @deprecated 100.0.4
     */
    private function getRequestThrottler()
    {
        if (!$this->requestThrottler instanceof RequestThrottler) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(RequestThrottler::class);
        }
        return $this->requestThrottler;
    }
}
