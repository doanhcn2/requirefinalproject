<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class HomeApi
 * @package Magenest\Eplus\Model
 */
class HomeApi implements \Magenest\Eplus\Api\HomeApiInterface
{
    /**
     * @var \Magenest\Eplus\Helper\CategoryHelper
     */
    protected $_magenestCategoryHelper;

    /**
     * @var \Magenest\Eplus\Helper\ProductHelper
     */
    protected $_magenestProductHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * HomeApi constructor.
     * @param \Magento\Framework\DataObject $dataObject
     * @param \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper
     * @param \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
     */
    public function __construct(
        \Magento\Framework\DataObject $dataObject,
        \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper,
        \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
    )
    {
        $this->_dataObject = $dataObject;
        $this->_magenestProductHelper = $magenestProductHelper;
        $this->_magenestCategoryHelper = $magenestCategoryHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getHome($store)
    {
        try {
            $categoryData = $this->_magenestCategoryHelper->getHomeCategoryByStore($store);
            $mostViewedProducts = $this->_magenestProductHelper->getHomeMostViewedProducts($store);
            $bestsellingProducts = $this->_magenestProductHelper->getHomeBestSellingProducts($store);
            return $this->_dataObject
                ->addData([
                    'api_status' => 1,
                    'categories' => $categoryData,
                    'most_viewed_products' => $mostViewedProducts,
                    'best_selling_products' => $bestsellingProducts
                ]);
        } catch (\Exception $e) {
            return $this->_dataObject
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
