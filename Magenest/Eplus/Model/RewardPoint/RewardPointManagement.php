<?php
/**
 * Contributor company: iPragmatech solution Pvt Ltd.
 * Contributor Author : Manish Kumar
 * Date: 23/5/16
 * Time: 11:55 AM
 */

namespace Magenest\Eplus\Model\RewardPoint;

use Magenest\Eplus\Api\RewardPoint\RewardPointManagementInterface;
use Magenest\Eplus\Api\Wishlist\WishlistManagementInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory;
use Magento\Wishlist\Model\WishlistFactory;

/**
 * Defines the implementaiton class of the RewardPointManagementInterface
 */
class RewardPointManagement implements RewardPointManagementInterface
{

    /**
     * @var CollectionFactory
     */
    protected $_rewardHistoryCollectionFactory;

    /**
     * Wishlist item collection
     *
     * @var \Magento\Wishlist\Model\ResourceModel\Item\Collection
     */
    protected $_itemCollection;

    /**
     * @var WishlistRepository
     */
    protected $_wishlistRepository;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;
    /**
     * @var WishlistFactory
     */
    protected $_wishlistFactory;
    /**
     * @var Item
     */
    protected $_itemFactory;

    protected $_objectFactory;

    /**
     * WishlistManagement constructor.
     * @param \Magento\Framework\DataObject $objectFactory
     * @param CollectionFactory $wishlistCollectionFactory
     * @param WishlistFactory $wishlistFactory
     * @param WishlistFactory $wishlistRepository
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Wishlist\Model\ItemFactory $itemFactory
     */
    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        CollectionFactory $rewardHistoryCollectionFactory,
        WishlistFactory $wishlistFactory,
        \Magento\Wishlist\Model\WishlistFactory $wishlistRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Wishlist\Model\ItemFactory $itemFactory
    ) {
        $this->_objectFactory = $objectFactory;
        $this->_rewardHistoryCollectionFactory = $rewardHistoryCollectionFactory;
        $this->_wishlistRepository = $wishlistRepository;
        $this->_productRepository = $productRepository;
        $this->_wishlistFactory = $wishlistFactory;
        $this->_itemFactory = $itemFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getRewardPointForCustomer($customerId)
    {

        if (empty($customerId) || !isset($customerId) || $customerId == "") {
            throw new InputException(__('Id required'));
        } else {
            $collection =
                $this->_rewardHistoryCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addCustomerFilter($customerId)
                    ->skipExpiredDuplicates()
                    ->setDefaultOrder();
            
            $rewardData = [];
            /** @var \Magento\Reward\Model\Reward\History $item */
            foreach ($collection as $item) {
                $data = [
                    "balance" => $item->getPointsBalance(),
                    "amount"      => $item->getCurrencyAmount(),
                    "points"       => $item->getPointsDelta(),
                    "create_at"         => $item->getCreatedAt(),
                    "expired_at"         => $item->getExpiresAt(),
                    "comment"      => $item->getComment(),
                    "reason"      => ($item->getMessage()->getText()) ? $item->getMessage()->getText() : '',

                ];
                $rewardData[] = $data;
            }
            return $this->_objectFactory->addData([
                'result' => $rewardData
            ]);
        }
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function addWishlistForCustomer($customerId, $productId)
//    {
//        $result = false;
//        try {
//            $product = $this->_productRepository->getById($productId);
//        } catch (NoSuchEntityException $e) {
//            return $this->_objectFactory->addData([
//                'result' => $result
//            ]);
//        }
//        try {
//            $wishlist = $this->_wishlistRepository->create()->loadByCustomerId
//            ($customerId, true);
//            $wishlist->addNewItem($product);
//            $returnData = $wishlist->save();
//            $result = $returnData;
//        } catch (NoSuchEntityException $e) {
//        }
//        return $this->_objectFactory->addData([
//            'result' => (boolean)$result
//        ]);
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function deleteWishlistForCustomer($customerId, $wishlistItemId)
//    {
//        $result = false;
//        if ($wishlistItemId != null && $customerId !=null) {
//            $item = $this->_itemFactory->create()->load($wishlistItemId);
//            if ($item->getId()) {
//                $wishlistId = $item->getWishlistId();
//                $wishlist = $this->_wishlistFactory->create();
//
//                if ($wishlistId) {
//                    $wishlist->load($wishlistId);
//                } elseif ($customerId) {
//                    $wishlist->loadByCustomerId($customerId, true);
//                }
//                if ($wishlist && $wishlist->getId() && $wishlist->getCustomerId() == $customerId)
//
//                try {
//                    $item->delete();
//                    $wishlist->save();
//                    $result = true;
//                } catch (\Exception $e) {
//
//                }
//            }
//        }
//        return $this->_objectFactory->addData([
//            'result' => (boolean)$result
//        ]);
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getWishlistInfo($customerId){
//
//        if (empty($customerId) || !isset($customerId) || $customerId == "") {
//            throw new InputException(__('Id required'));
//        } else {
//            $collection =
//                $this->_wishlistCollectionFactory->create()
//                    ->addCustomerIdFilter($customerId);
//
//            $totalItems = count($collection);
//
//            $data = [
//                "total_items" => $totalItems
//            ];
//
//            $wishlistData[] = $data;
//
//            return $wishlistData;
//        }
//    }

}
