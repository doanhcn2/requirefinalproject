<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:03
 */
namespace Magenest\Eplus\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class SearchResult
 * @package Magenest\Eplus\Model
 */
class SearchResult extends  AbstractModel
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_eplus_search_result';

    /**
     * Event Object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_eplus_search_result';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Eplus\Model\ResourceModel\SearchResult');
    }
}
