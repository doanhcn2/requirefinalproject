<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class CategoryProductApi
 * @package Magenest\Eplus\Model
 */
class SearchProductApi implements \Magenest\Eplus\Api\SearchProductApiInterface
{
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var ResourceModel\SearchResult\Collection
     */
    protected $_searchResultFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Search\Api\SearchInterface
     */
    protected $_search;

    /**
     * @var \Magento\Framework\Api\Search\SearchCriteriaInterface
     */
    protected $_searchCriteria;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroup
     */
    protected $_filterGroup;

    /**
     * @var \Magenest\Eplus\Helper\ProductHelper
     */
    protected $_productHelper;

    /**
     * SearchProductApi constructor.
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Magento\Framework\Api\Search\SearchCriteriaInterface $searchCriteria
     * @param \Magento\Search\Api\SearchInterface $search
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param ResourceModel\SearchResult\CollectionFactory $searchResultFactory
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magenest\Eplus\Helper\ProductHelper $productHelper
     * @param \Magento\Framework\DataObject $dataObject
     */
    public function __construct(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\Api\Search\SearchCriteriaInterface $searchCriteria,
        \Magento\Search\Api\SearchInterface $search,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magenest\Eplus\Model\ResourceModel\SearchResult\CollectionFactory $searchResultFactory,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magenest\Eplus\Helper\ProductHelper $productHelper,
        \Magento\Framework\DataObject $dataObject
    ) {
        $this->_productHelper = $productHelper;
        $this->_filterGroup = $filterGroup;
        $this->_searchCriteria = $searchCriteria;
        $this->_search = $search;
        $this->_objectManager = $objectManager;
        $this->_searchResultFactory = $searchResultFactory;
        $this->_debugHelper = $debugHelper;
        $this->_dataObject = $dataObject;
    }

    public function getSearchResult($store, $search)
    {
        try {
            $cachedResult = $this->_searchResultFactory->create()
                ->addFieldToFilter('store_id', $store)
                ->addFieldToFilter('search_term', $search)
                ->load()
                ->getFirstItem();
            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue()) > 0) {
                return $this->_dataObject->addData(json_decode($cachedResult->getResultValue(), true));
            } else {
                $filter_group = $this->_filterGroup;
                $search_criteria = $this->_searchCriteria;
                $search_criteria->setRequestName('quick_search_container');
                $filter = $this->_objectManager->create('Magento\Framework\Api\Filter');
                $filter->setData('field', 'search_term');
                $filter->setData('value', $search);
                $filter->setData('condition_type', 'like');
                $filter_group->setFilters([$filter]);
                $search_criteria->setFilterGroups([$filter_group]);
                $searchResult = $this->_search->search($search_criteria);
                $productIds = [];
                foreach ($searchResult->getItems() as $searchItem) {
                    $productIds[] = $searchItem->getId();
                }
                $productList = $this->_productHelper->getProductFewDetailByIds($store, $productIds);
                $this->_searchResultFactory->create()
                    ->getNewEmptyItem()
                    ->setData([
                        'store_id' => $store,
                        'search_term' => $search,
                        'result_value' => json_encode([
                            'api_status' => 1,
                            'search' => $productList
                        ])
                    ])
                    ->save();
                return $this->_dataObject->addData([
                    'api_status' => 1,
                    'search' => $productList
                ]);
            }
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('searchproductapi', $e->getMessage());
            return $this->_dataObject->addData([
                'api_status' => 0
            ]);
        }
    }

    public function getSearchResultTest($store, $search)
    {
        $search = '%' . str_replace('/[^A-Za-z0-9\-]/', '', $search) . '%';
        $result = [];
        $filter_group = $this->_objectManager->create('Magento\Framework\Api\Search\FilterGroup');
        $check = 1;
        do {
            switch ($check) {
                case 1:
                    $filter = $this->_objectManager->create('Magento\Framework\Api\Filter');
                    $filter->setData('field', 'name');
                    $filter->setData('value', $search);
                    $filter->setData('condition_type', 'like');
                    $filter_group->setData('filters', [$filter]);
                    $search_criteria = $this->_objectManager->create('Magento\Framework\Api\SearchCriteriaInterface');
                    $search_criteria->setFilterGroups([$filter_group]);
                    $repo = $this->_objectManager->get('Magento\Catalog\Model\ProductRepository');
                    $result = $repo->getList($search_criteria);
                    if ($result->getTotalCount() > 0) {
                        $check = -1;
                    } else {
                        $check ++;
                    }
                    break;
                case 2:
                    $filter = $this->_objectManager->create('Magento\Framework\Api\Filter');
                    $filter->setData('field', 'sku');
                    $filter->setData('value', $search);
                    $filter->setData('condition_type', 'like');
                    $filter_group->setData('filters', [$filter]);
                    $search_criteria = $this->_objectManager->create('Magento\Framework\Api\SearchCriteriaInterface');
                    $search_criteria->setFilterGroups([$filter_group]);
                    $repo = $this->_objectManager->get('Magento\Catalog\Model\ProductRepository');
                    $result = $repo->getList($search_criteria);
                    if ($result->getTotalCount() > 0) {
                        $check = -1;
                    } else {
                        $check ++;
                    }
                    break;
                case 3:
                    $filter = $this->_objectManager->create('Magento\Framework\Api\Filter');
                    $filter->setData('field', 'meta_description');
                    $filter->setData('value', $search);
                    $filter->setData('condition_type', 'like');
                    $filter_group->setData('filters', [$filter]);
                    $search_criteria = $this->_objectManager->create('Magento\Framework\Api\SearchCriteriaInterface');
                    $search_criteria->setFilterGroups([$filter_group]);
                    $repo = $this->_objectManager->get('Magento\Catalog\Model\ProductRepository');
                    $result = $repo->getList($search_criteria);
                    if ($result->getTotalCount() > 0) {
                        $check = -1;
                    } else {
                        $check ++;
                    }
                    break;
                default:
                    $check = -1;
                    break;
            }

        } while ($check > 0);
        if (sizeof($result) > 0) {

        } else {

        }
        $a = 0;
        return $this->_dataObject->addData([
            'api_status' => 1,
            'search' => $result->getTotalCount()
        ]);
    }
}
