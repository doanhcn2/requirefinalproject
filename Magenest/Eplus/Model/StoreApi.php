<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class StoreApi
 * @package Magenest\Eplus\Model
 */
class StoreApi implements \Magenest\Eplus\Api\StoreApiInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_currencySymbolFactory;

    /**
     * @var \Magenest\Eplus\Helper\CategoryHelper
     */
    protected $_categoryHelper;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magenest\Eplus\Helper\StoreHelper
     */
    protected $_storeHelper;

    /**
     * StoreApi constructor.
     * @param \Magenest\Eplus\Helper\StoreHelper $storeHelper
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     */
    public function __construct(
        \Magenest\Eplus\Helper\StoreHelper $storeHelper,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Store\Model\StoreFactory $storeFactory
    )
    {
        $this->_storeHelper = $storeHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getStore()
    {
        try {
            $result = $this->_storeHelper->getAllStoreData();
            return $this->_objectFactory
                ->addData($result);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('store', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
