<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class BannerApi
 * @package Magenest\Eplus\Model
 */
class BannerApi implements \Magenest\Eplus\Api\BannerApiInterface
{
    protected $_objectFactory;

    public function __construct(
        \Magento\Framework\DataObject $objectFactory
    ) {
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getBanner($store)
    {
        $banners = [];
        $banners[] = [
            'id' => 1,
            'name' => 'category name',
            'target' => 'link:1',
            'image_url' => 'https://store.magenest.com/media/banner/combo-package-banner.jpg'
        ];
        $banners[] = [
            'id' => 2,
            'name' => 'banner2',
            'target' => 'detail:32',
            'image_url' => 'https://store.magenest.com/media/banner/combo-package-banner.jpg'
        ];
        $banners[] = [
            'id' => 3,
            'name' => 'banner3',
            'target' => 'detail:321123',
            'image_url' => 'https://store.magenest.com/media/banner/combo-package-banner.jpg'
        ];
        return $this->_objectFactory->addData([
            'records' => $banners,
            'api_status' => 1
        ]);
    }
}
