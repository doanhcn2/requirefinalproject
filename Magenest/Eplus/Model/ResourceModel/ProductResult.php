<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:06
 */
namespace Magenest\Eplus\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class ProductResult
 *
 * @package Magenest\Eplus\Model\ResourceModel
 */
class ProductResult extends  AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_eplus_product_result', 'id');
    }
}
