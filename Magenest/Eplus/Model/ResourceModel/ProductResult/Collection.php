<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:07
 */
namespace Magenest\Eplus\Model\ResourceModel\ProductResult;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\Eplus\Model\ResourceModel\ProductResult
 */
class Collection extends AbstractCollection
{

    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_eplus_product_result';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_eplus_product_result';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Eplus\Model\ProductResult', 'Magenest\Eplus\Model\ResourceModel\ProductResult');
    }
}
