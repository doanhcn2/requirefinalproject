<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class NavApi
 * @package Magenest\Eplus\Model
 */
class NavApi implements \Magenest\Eplus\Api\NavApiInterface
{
    protected $_objectFactory;

    protected $_magenestCategoryHelper;

    public function __construct(
        \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper,
        \Magento\Framework\DataObject $objectFactory
    ) {
        $this->_magenestCategoryHelper = $magenestCategoryHelper;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getNav($store)
    {
        $pages = [];
        $pages[] = [
            'id' => 1,
            'name' => 'asdasd123123'
        ];
        $pages[] = [
            'id' => 2,
            'name' => 'sdasd'
        ];
        $pages[] = [
            'id' => 3,
            'name' => '345345'
        ];



        return $this->_objectFactory->addData([
            'navigation' => $this->_magenestCategoryHelper->getHomeCategoryByStore($store),
            'pages' => $pages,
            'api_status' => 1
        ]);
    }
}
