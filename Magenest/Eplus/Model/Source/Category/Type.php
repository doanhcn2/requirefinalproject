<?php
namespace Magenest\Eplus\Model\Source\Category;

class Type extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory
     */
    protected $_eavAttrEntity;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $eavAttrEntity
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $eavAttrEntity
    ) {
        $this->_eavAttrEntity = $eavAttrEntity;
    }
    /**
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                [
                    'value' => '1',
                    'label' => __('Product'),
                ],
                [
                    'value' => '2',
                    'label' => __('Groupon'),
                ],
                [
                    'value' => '3',
                    'label' => __('Event'),
                ],
                [
                    'value' => '4',
                    'label' => __("Accommodation")
                ]
            ];
        }
        return $this->_options;
    }
}
