<?php

namespace Magenest\Eplus\Model\Category;

use \Magento\Catalog\Model\Category\DataProvider as Provider;
use Magento\Catalog\Model\Category\Attribute\Backend\Image as ImageBackendModel;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Model\Category\FileInfo;
use Magento\Store\Api\Data\StoreInterface;


class DataProvider extends Provider
{

    private $fileInfo;

    public function getData()
    {
        $loadedData = Provider::getData();
        $category = parent::getCurrentCategory();
        $banner = $category->getBanner();
        if ($loadedData[$category->getId()]&& isset($banner)) {
            $loadedData[$category->getId()]['banner'] = $banner;
            $categoryData = DataProvider::convertValues($category, $loadedData[$category->getId()]);

            $loadedData[$category->getId()] = $categoryData;
        }
        return $loadedData;
    }
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'category_type'; // custom image field
        $fields['content'][] = 'banner';
        return $fields;
    }

    private function convertValues($category, $categoryData)
    {
        foreach ($category->getAttributes() as $attributeCode => $attribute) {
            if (!isset($categoryData[$attributeCode])) {
                continue;
            }

            if ($attributeCode == 'banner' && $attribute->getBackend() instanceof ImageBackendModel) {
                unset($categoryData[$attributeCode]);

                $fileNames = json_decode($category->getData($attributeCode),true);
                foreach ($fileNames as $key => $fileName){
                    if ($fileName && $this->getFileInfo()->isExist($fileName)) {
                        $stat = $this->getFileInfo()->getStat($fileName);
                        $mime = $this->getFileInfo()->getMimeType($fileName);

                        $url = \Magento\Framework\App\ObjectManager::getInstance()
                                ->get('\Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl(
                                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                            ) . 'catalog/category/' . $fileName;

                        $categoryData[$attributeCode][$key]['name'] = $fileName;
                        $categoryData[$attributeCode][$key]['url'] = $url;
                        $categoryData[$attributeCode][$key]['size'] = isset($stat) ? $stat['size'] : 0;
                        $categoryData[$attributeCode][$key]['type'] = $mime;
                    }
                }

            }
        }

        return $categoryData;
    }
    private function getFileInfo()
    {
        if ($this->fileInfo === null) {
            $this->fileInfo = ObjectManager::getInstance()->get(FileInfo::class);
        }
        return $this->fileInfo;
    }
}