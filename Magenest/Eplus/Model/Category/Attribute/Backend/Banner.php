<?php
/**
 * Copyright © Magenest, Inc. All rights reserved.
 */
namespace Magenest\Eplus\Model\Category\Attribute\Backend;

/**
 * Catalog category image attribute backend model
 *
 * @api
 * @since 100.0.2
 */
class Banner extends \Magento\Catalog\Model\Category\Attribute\Backend\Image
{
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     *
     */
    protected $_uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem
     *
     */
    protected $_filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     *
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     *
     */
    protected $_logger;

    /**
     * @var \Magento\Catalog\Model\ImageUploader
     */
    private $imageUploader;

    /**
     * @var string
     */
    private $additionalData = '_additional_data_';

    /**
     * Gets image name from $value array.
     * Will return empty string in a case when $value is not an array
     *
     * @param array $value Attribute value
     * @return string
     */
    private function getUploadedImageName($values)
    {
        if (is_array($values) && isset($values[0]['name'])) {
            $name = [];
            foreach ($values as $value){
                if(isset($value['name']))
                    $name[] = $value['name'];
            }
            return is_array($name)? json_encode($name) : '';
        }

        return '';
    }

    /**
     * Avoiding saving potential upload data to DB
     * Will set empty image attribute value if image was not uploaded
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    public function beforeSave($object)
    {
        $attributeName = $this->getAttribute()->getName();
        $value = $object->getData($attributeName);

        if ($imageName = $this->getUploadedImageName($value)) {
            $object->setData($this->additionalData . $attributeName, $value);
            $object->setData($attributeName, $imageName);
        } elseif (!is_string($value)) {
            $object->setData($attributeName, '');
        }

        return parent::beforeSave($object);
    }

    /**
     * @return \Magento\Catalog\Model\ImageUploader
     *
     * @deprecated 101.0.0
     */
    private function getImageUploader()
    {
        if ($this->imageUploader === null) {
            $this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Catalog\CategoryImageUpload::class);
        }

        return $this->imageUploader;
    }

    /**
     * Check if temporary file is available for new image upload.
     *
     * @param array $value
     * @return bool
     */
    private function isTmpFileAvailable($value)
    {
        return is_array($value) && isset($value[0]['tmp_name']);
    }

    /**
     * Save uploaded file and set its name to category
     *
     * @param \Magento\Framework\DataObject $object
     * @return \Magenest\Eplus\Model\Category\Attribute\Backend\Banner
     */
    public function afterSave($object)
    {
        $value = $object->getData($this->additionalData . $this->getAttribute()->getName());

        if ($this->isTmpFileAvailable($value)) {
            $imageNames = json_decode($this->getUploadedImageName($value));
            if($imageNames)
            foreach ($imageNames as $imageName){
                try {
                    $this->getImageUploader()->moveFileFromTmp($imageName);
                } catch (\Exception $e) {
                    $this->_logger->critical($e);
                }
            }

        }
        return $this;
    }
}
