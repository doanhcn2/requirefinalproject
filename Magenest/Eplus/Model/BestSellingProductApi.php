<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\Eplus\Model;

/**
 * Class BestSellingProductApi
 * @package Magenest\Eplus\Model
 */
class BestSellingProductApi implements \Magenest\Eplus\Api\BestSellingProductApiInterface
{
    /**
     * @var \Magenest\Eplus\Helper\CategoryHelper
     */
    protected $_magenestCategoryHelper;

    /**
     * @var \Magenest\Eplus\Helper\ProductHelper
     */
    protected $_magenestProductHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var \Magenest\Eplus\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var ResourceModel\SimpleResult\CollectionFactory
     */
    protected $_simpleResultFactory;

    /**
     * BestSellingProductApi constructor.
     * @param ResourceModel\SimpleResult\CollectionFactory $simpleResultFactory
     * @param \Magenest\Eplus\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $dataObject
     * @param \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper
     * @param \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
     */
    public function __construct(
        \Magenest\Eplus\Model\ResourceModel\SimpleResult\CollectionFactory $simpleResultFactory,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $dataObject,
        \Magenest\Eplus\Helper\ProductHelper $magenestProductHelper,
        \Magenest\Eplus\Helper\CategoryHelper $magenestCategoryHelper
    )
    {
        $this->_simpleResultFactory = $simpleResultFactory;
        $this->_debugHelper = $debugHelper;
        $this->_dataObject = $dataObject;
        $this->_magenestProductHelper = $magenestProductHelper;
        $this->_magenestCategoryHelper = $magenestCategoryHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getBestSellingProduct($store)
    {
        try {
            $cachedResult = $this->_simpleResultFactory->create()
                ->addFieldToFilter('result_key', 'store_' . $store . '_bestsellingproduct')
                ->load()
                ->getFirstItem();
            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue()) > 0) {
                return $this->_dataObject
                    ->addData([
                        'api_status' => 1,
                        'best_selling_product' => json_decode($cachedResult->getResultValue(), true)
                    ]);
            } else {
                $bestsellingProducts = $this->_magenestProductHelper->getHomeBestSellingProducts($store);
                if (sizeof($bestsellingProducts) > 0) {
                    $this->_simpleResultFactory->create()
                        ->getNewEmptyItem()
                        ->setData([
                            'result_key' => 'store_' . $store . '_bestsellingproduct',
                            'result_value' => json_encode($bestsellingProducts)
                        ])
                        ->save();
                    return $this->_dataObject
                        ->addData([
                            'api_status' => 1,
                            'best_selling_product' => $bestsellingProducts
                        ]);
                } else {
                    return $this->_dataObject
                        ->addData([
                            'api_status' => 0
                        ]);
                }
            }
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('bestselling', $e->getMessage());
            return $this->_dataObject
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
