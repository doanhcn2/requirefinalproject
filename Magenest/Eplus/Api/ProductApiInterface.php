<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface ProductApiInterface
 * @package Magenest\Eplus\Api
 */
interface ProductApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\Eplus\Api\Data\EplusProductInterface
     */
    public function getProduct($store, $id);
}