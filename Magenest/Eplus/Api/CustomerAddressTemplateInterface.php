<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 05/02/2018
 * Time: 14:38
 */
namespace Magenest\Eplus\Api;

/**
 * Interface providing token generation for Customers
 *
 * @api
 * @since 100.0.2
 */
interface CustomerAddressTemplateInterface
{
    /**
     * Create access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $password
     * @return \Magenest\Eplus\Api\Data\CustomerInfoInterface
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createCustomerAccessToken($username, $password);
}