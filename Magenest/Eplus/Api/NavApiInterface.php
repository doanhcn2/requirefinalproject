<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface NavApiInterface
 * @package Magenest\Eplus\Api
 */
interface NavApiInterface
{
    /**
     * @api
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\NavInterface
     */
    public function getNav($store);
}