<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface CategoryApiInterface
 * @package Magenest\Eplus\Api
 */
interface CategoryApiInterface
{
    /**
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\CategoryInterface
     */
    public function getCategory($store);
}