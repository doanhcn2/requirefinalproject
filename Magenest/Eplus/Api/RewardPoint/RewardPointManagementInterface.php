<?php

namespace Magenest\Eplus\Api\RewardPoint;

/**
 * Interface WishlistManagementInterface
 * @package Magenest\Eplus\Api\RewardPoint
 */
interface RewardPointManagementInterface
{

    /**
     * Return Wishlist items.
     *
     * @param int $customerId
     * @return \Magenest\Eplus\Api\Data\RewardPoint\RewardPointInterface
     */
    public function getRewardPointForCustomer($customerId);

//    /**
//     * @param int $customerId
//     * @param int $productId
//     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
//     */
//    public function addWishlistForCustomer($customerId, $productId);
//
//    /**
//     * Return Added wishlist item.
//     *
//     * @param int $customerId
//     * @param int $wishlistItemId
//     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
//     *
//     */
//    public function deleteWishlistForCustomer($customerId, $wishlistItemId);
//
//    /**
//     * Return Added wishlist info.
//     *
//     * @param int $customerId
//     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
//     *
//     */
//    public function getWishlistInfo($customerId);

}

