<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface LogoApiInterface
 * @package Magenest\Eplus\Api
 */
interface SearchProductApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $search
     * @return \Magenest\Eplus\Api\Data\SearchProductInterface
     */
    public function getSearchResult($store, $search);
}