<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface CategoryApiInterface
 * @package Magenest\Eplus\Api
 */
interface CategoryProductApiInterface
{
    /**
     * @param string $store
     * @param string $category
     * @return \Magenest\Eplus\Api\Data\CategoryProductInterface
     */
    public function getCategoryProduct($store, $category);
}