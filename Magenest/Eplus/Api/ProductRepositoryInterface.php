<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 26/12/2017
 * Time: 10:05
 */
namespace Magenest\Eplus\Api;

interface ProductRepositoryInterface {

    /**
     * Get product list
     *
     * @param int $store
     * @param int $num
     * @param int $size
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magenest\Eplus\Api\Data\CategoryProductInterface
     */
    public function getList($store, $num, $size, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}