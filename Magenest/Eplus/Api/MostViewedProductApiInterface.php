<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface MostViewedProductApiInterface
 * @package Magenest\Eplus\Api
 */
interface MostViewedProductApiInterface
{
    /**
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\MostViewedProductInterface
     */
    public function getMostViewedProduct($store);
}