<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface StoreApiInterface
 * @package Magenest\Eplus\Api
 */
interface StoreApiInterface
{
    /**
     * @api
     * @return \Magenest\Eplus\Api\Data\StoreInterface
     */
    public function getStore();
}