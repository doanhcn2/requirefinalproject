<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface LogoApiInterface
 * @package Magenest\Eplus\Api
 */
interface LogoApiInterface
{
    /**
     * @api
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\LogoInterface
     */
    public function getLogo($store);
}