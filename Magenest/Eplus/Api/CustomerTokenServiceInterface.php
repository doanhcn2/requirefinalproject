<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:53
 */
namespace Magenest\Eplus\Api;

/**
 * Interface providing token generation for Customers
 *
 * @api
 * @since 100.0.2
 */
interface CustomerTokenServiceInterface
{
    /**
     * Create access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $password
     * @return \Magenest\Eplus\Api\Data\CustomerInfoInterface
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createCustomerAccessToken($username, $password);

    /**
     * Create access token for admin given the vendor credentials.
     *
     * @param string $username
     * @param string $password
     * @return \Magenest\Eplus\Api\Data\CustomerInfoInterface
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createVendorAccessToken($username, $password);

    /**
     * Revoke token by merchant id.
     *
     * The function will delete the token from the oauth_token table.
     *
     * @param int $merchantId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function revokeVendorAccessToken($merchantId);
}