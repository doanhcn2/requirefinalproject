<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface BannerApiInterface
 * @package Magenest\Eplus\Api
 */
interface BannerApiInterface
{
    /**
     * @api
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\BannerInterface
     */
    public function getBanner($store);
}