<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 17/01/2018
 * Time: 17:05
 */
namespace Magenest\Eplus\Api;

/**
 * Interface CartManagementInterface
 * @package Magenest\Eplus\Api
 */
interface CartManagementInterface
{

    /**
     * Returns information for the cart for a specified customer.
     *
     * @param int $customerId The customer ID.
     * @return \Magenest\Eplus\Api\Data\CartInterface Cart object.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified customer does not exist.
     */
    public function getCartForCustomer($customerId);


    /**
     * Removes the specified item from the specified cart.
     *
     * @param int $cartId The cart ID.
     * @param int $itemId The item ID of the item to be removed.
     * @return \Magenest\Eplus\Api\Data\Cart\ModifyCartStatusInterface
     */
    public function deleteById($cartId, $itemId);
}