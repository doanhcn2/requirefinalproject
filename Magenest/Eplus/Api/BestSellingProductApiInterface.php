<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface BestSellingProductApiInterface
 * @package Magenest\Eplus\Api
 */
interface BestSellingProductApiInterface
{
    /**
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\BestSellingProductInterface
     */
    public function getBestSellingProduct($store);
}