<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 26/12/2017
 * Time: 15:21
 */

namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface Filters
{
    const FILTERS = 'filters';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getFilters();
}