<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface SearchSuggestInterface
{
    const API_STATUS = 'api_status';
    const PRODUCT = 'search';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSearch();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
