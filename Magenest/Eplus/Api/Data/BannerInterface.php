<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface BannerInterface
{
    const RECORDS = 'records';
    const API_STATUS = 'api_status';

    /**
     * @return  \Magento\Framework\DataObject[]
     */
    public function getRecords();

    /**
     * @param string $data
     * @return $this
     */
    public function setRecords($data);

    /**
     * @return int
     */
    public function getApiStatus();
}
