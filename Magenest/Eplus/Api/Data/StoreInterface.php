<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface StoreInterface
{
    const STORE = 'store';
    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getStore();

    /**
     * @param
     * @return $this
     */
    public function setStore($store);

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
