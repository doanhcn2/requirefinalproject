<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 05/02/2018
 * Time: 08:41
 */
namespace Magenest\Eplus\Api\Data\Cart;

/**
 * Interface ModifyCartStatusInterface
 * @package Magenest\Eplus\Api\Data\Cart
 */
interface ModifyCartStatusInterface
{
    const RESULT = 'result';

    /**
     * @return bool
     */
    public function getResult();

}
