<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\Eplus\Api\Data\Wishlist;

/**
 * @api
 */
interface WishlistInterface
{
    const RESULT = 'result';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getResult();
}