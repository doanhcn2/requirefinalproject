<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\Eplus\Api\Data\Wishlist;

/**
 * @api
 */
interface WishlistStatusInterface
{
    const RESULT = 'result';

    /**
     * @return bool
     */
    public function getResult();

}