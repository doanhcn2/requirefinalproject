<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface BestSellingProductInterface
{
    const API_STATUS = 'api_status';
    const BEST_SELLING_PRODUCT = 'best_selling_product';

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getBestSellingProduct();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
