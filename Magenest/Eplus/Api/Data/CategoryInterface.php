<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface CategoryInterface
{
    const API_STATUS = 'api_status';
    const CATEGORY = 'category';

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getCategory();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
