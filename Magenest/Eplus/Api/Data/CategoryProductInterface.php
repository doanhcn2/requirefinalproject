<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface CategoryProductInterface
{
    /**
     * @return \Magenest\Eplus\Api\Data\Filters
     */
    public function getMetadata();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getProduct();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getBanners();
}
