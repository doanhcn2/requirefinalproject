<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface EplusProductInterface
{
    const CORE_PRODUCT = 'result';

    const API_STATUS = 'api_status';

    /**
     * @return \Magenest\Eplus\Api\Data\CoreProductInterface
     */
    public function getResult();

    /**
     * @return int
     */
    public function getApiStatus();

}
