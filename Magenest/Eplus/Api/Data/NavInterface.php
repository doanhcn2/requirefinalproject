<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface NavInterface
{
    const NAVIGATION = 'navigation';
    const PAGES = 'pages';
    const API_STATUS = 'api_status';

    /**
     * @return  \Magento\Framework\DataObject[]
     */
    public function getNavigation();

    /**
     * @param array $data
     * @return $this
     */
    public function setNavigation($data);

    /**
     * @return  \Magento\Framework\DataObject[]
     */
    public function getPages();

    /**
     * @param array $data
     * @return $this
     */
    public function setPages($data);

    /**
     * @return int
     */
    public function getApiStatus();
}
