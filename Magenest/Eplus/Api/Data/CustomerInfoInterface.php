<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface CustomerInfoInterface
{
    const ACCESS_TOKEN = 'access_token';

    /**
     * @return string
     */
    public function getAccessToken();
}