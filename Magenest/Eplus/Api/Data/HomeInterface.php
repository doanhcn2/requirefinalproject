<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface HomeInterface
{
    const CATEGORIES = 'categories';
    const MOST_VIEWED_PRODUCTS = 'most_viewed_products';
    const BEST_SELLING_PRODUCTS = 'best_selling_products';
    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getCategories();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getMostViewedProducts();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getBestSellingProducts();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
