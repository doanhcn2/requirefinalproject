<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface LogoInterface
{
    const LOGO = 'logo';
    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getLogo();

    /**
     * @param string $data
     * @return $this
     */
    public function setLogo($data);

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
