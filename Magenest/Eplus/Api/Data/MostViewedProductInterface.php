<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\Eplus\Api\Data;

/**
 * @api
 */
interface MostViewedProductInterface
{
    const MOST_VIEWED_PRODUCT = 'most_viewed_product';
    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getMostViewedProduct();

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getApiStatus();
}
