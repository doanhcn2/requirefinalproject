<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\Eplus\Api;

/**
 * Interface HomeApiInterface
 * @package Magenest\Eplus\Api
 */
interface HomeApiInterface
{
    /**
     * @param string $store
     * @return \Magenest\Eplus\Api\Data\HomeInterface
     */
    public function getHome($store);
}