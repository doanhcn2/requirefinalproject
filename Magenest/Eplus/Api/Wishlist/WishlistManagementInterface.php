<?php

namespace Magenest\Eplus\Api\Wishlist;

/**
 * Interface WishlistManagementInterface
 * @package Magenest\Eplus\Api\Wishlist
 */
interface WishlistManagementInterface
{

    /**
     * Return Wishlist items.
     *
     * @param int $customerId
     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistInterface
     */
    public function getWishlistForCustomer($customerId);

    /**
     * @param int $customerId
     * @param int $productId
     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
     */
    public function addWishlistForCustomer($customerId, $productId);

    /**
     * Return Added wishlist item.
     *
     * @param int $customerId
     * @param int $wishlistItemId
     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
     *
     */
    public function deleteWishlistForCustomer($customerId, $wishlistItemId);
    
    /**
     * Return Added wishlist info.
     *
     * @param int $customerId
     * @return \Magenest\Eplus\Api\Data\Wishlist\WishlistStatusInterface
     *
     */
    public function getWishlistInfo($customerId);

}

