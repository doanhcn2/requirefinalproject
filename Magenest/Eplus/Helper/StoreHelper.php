<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:22
 */

namespace Magenest\Eplus\Helper;

/**
 * Class StoreHelper
 * @package Magenest\Eplus\Helper
 */
class StoreHelper
{
    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_categoryHelper;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var ImageHelper
     */
    protected $_imageHelper;

    /**
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_currencySymbolFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_fileStorageHelper;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\Catalog\Api\CategoryManagementInterface
     */
    protected $_categoryManagement;

    /**
     * StoreHelper constructor.
     * @param \Magento\Catalog\Api\CategoryManagementInterface $categoryManagement
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Locale\CurrencyInterface $currencySymbolFactory
     * @param DebugHelper $debugHelper
     * @param ImageHelper $imageHelper
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Catalog\Api\CategoryManagementInterface $categoryManagement,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\CurrencyInterface $currencySymbolFactory,
        \Magenest\Eplus\Helper\DebugHelper $debugHelper,
        \Magenest\Eplus\Helper\ImageHelper $imageHelper,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    )
    {
        $this->_categoryManagement = $categoryManagement;
        $this->_filesystem = $filesystem;
        $this->_fileStorageHelper = $fileStorageHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_currencySymbolFactory = $currencySymbolFactory;
        $this->_debugHelper = $debugHelper;
        $this->_imageHelper = $imageHelper;
        $this->_storeFactory = $storeFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFactory = $categoryFactory;
    }

    public function getAllStoreData()
    {
        try {
            $result = null;
            $storeData = [];
            $stores = $this->_storeFactory->create()
                ->getCollection();
            foreach ($stores as $store) {
                $baseCurrencyCode = $store->getBaseCurrencyCode();
                $currencyData = [];
                $currencies = $store->getAvailableCurrencyCodes();
                foreach ($currencies as $currency) {
                    $symbolModel = $this->_currencySymbolFactory->getCurrency($currency);
                    $currentSymbol = $symbolModel->getSymbol();
                    if (!$currentSymbol) {
                        $currentSymbol = $currency;
                    }
                    $currencyData[] = [
                        'code' => $currency,
                        'symbol' => $currentSymbol,
                        'name' => $symbolModel->getName(),
                        'rate' => $store->getBaseCurrency()->getRate($currency)
                    ];
                }
                $rootCategory = $this->_categoryFactory->create()
                    ->load($store->getRootCategoryId());
                $mediaPath = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $language = $this->_scopeConfig->getValue('general/locale/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getEntityId());
                $google_ua = $this->_scopeConfig->getValue('google/analytics/account', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getEntityId());
                $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
                $storeLogoPath = $this->_scopeConfig->getValue(
                    'design/header/logo_src',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $path = $folderName . '/' . $storeLogoPath;
                $logoUrl = $store
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;
//                    $storeData[] = [
//                        'id' => $store->getId(),
//                        'name' => $store->getName(),
//                        'base_currency' => $baseCurrencyCode,
//                        'currency' => $currencyData,
//                        'category' => $this->getCategoryChildrenData($rootCategory, $mediaPath),
//                        'language' => $language,
//                        'logo' => $logoUrl,
//                        'flag_icon' => $logoUrl,
//                        'google_ua' => $google_ua
//                    ];
                $categoriesData = [];
                $categoryItems = $this->_categoryManagement->getTree($rootCategory->getEntityId(), $this->_categoryManagement->getCount());
                foreach($categoryItems->getChildrenData() as $item) {
                    if ($item->getIsActive()) {
                        $currentCategory = $this->_categoryFactory->create()
                            ->load($item->getEntityId());
                        $children_data_temp = $this->getChildrenData($item, 'in ' . $item->getName());
                        if (sizeof($children_data_temp) > 0) {
                            $categoriesData[] = [
                                'name' => $item->getName(),
                                'id' => $item->getEntityId(),
                                'type' => $currentCategory->getCategoryType(),
                                'children' => $children_data_temp,
                                'address' => 'in ' . $item->getName()
                            ];
                        } else {
                            $categoriesData[] = [
                                'name' => $item->getName(),
                                'type' => $currentCategory->getCategoryType(),
                                'id' => $item->getEntityId(),
                                'address' => 'in ' . $item->getName()
                            ];
                        }
                    }
                }
                $storeData[] = [
                    'id' => $store->getId(),
                    'name' => $store->getName(),
                    'base_currency' => $baseCurrencyCode,
                    'category' => $categoriesData,
                    'currency' => $currencyData,
                    'language' => $language,
                    'logo' => $logoUrl,
                    'flag_icon' => $logoUrl,
                    'google_ua' => $google_ua,
                    'media_url' => $mediaPath
                ];
            }
            if (sizeof($storeData) > 0) {
//                    $this->_simpleResultFactory->create()
//                        ->setData([
//                            'result_key' => 'stores',
//                            'result_value' => json_encode($storeData)
//                        ])->save();
                $result = [
                    'api_status' => 1,
                    'store' => $storeData
                ];
            } else {
                $result =  [
                    'api_status' => 0
                ];
            }
            return $result;
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('store', $e->getMessage());
            return [
                    'api_status' => 0
                ];
        }
    }

    public function getChildrenData($item, $address)
    {
        $result = [];
        $children_data = $item->getChildrenData();
        if (sizeof($children_data) > 0) {
            foreach ($children_data as $children_data_item) {
                if ($children_data_item->getIsActive()) {
                    $currentCategory = $this->_categoryFactory->create()
                        ->load($children_data_item->getEntityId());
                    $children_data_temp = $this->getChildrenData($children_data_item, $address . ' -> ' . $children_data_item->getName());
                    if (sizeof($children_data_temp) > 0) {
                        $result[] = [
                            'name' => $children_data_item->getName(),
                            'type' => $currentCategory->getCategoryType(),
                            'id' => $children_data_item->getEntityId(),
                            'children' => $children_data_temp,
                            'address' => $address . ' -> ' . $children_data_item->getName()
                        ];
                    } else {
                        $result[] = [
                            'name' => $children_data_item->getName(),
                            'type' => $currentCategory->getCategoryType(),
                            'id' => $children_data_item->getEntityId(),
                            'address' => $address . ' -> ' . $children_data_item->getName()
                        ];
                    }

                }
            }
        }
        return $result;
    }

    protected function _isFile($filename)
    {
        if ($this->_fileStorageHelper->checkDbUsage() && !$this->getMediaDirectory()->isFile($filename)) {
            $this->_fileStorageHelper->saveFileToFilesystem($filename);
        }

        return $this->getMediaDirectory()->isFile($filename);
    }

    protected function getMediaDirectory()
    {
        return $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }
}