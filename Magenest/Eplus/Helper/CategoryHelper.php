<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:22
 */

namespace Magenest\Eplus\Helper;

/**
 * Class CategoryHelper
 * @package Magenest\Eplus\Helper
 */
class CategoryHelper
{
    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_categoryHelper;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var ImageHelper
     */
    protected $_imageHelper;

    /**
     * @var \Magenest\Eplus\Model\SimpleResultFactory
     */
    protected $_simpleResultFactory;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @var ProductHelper
     */
    protected $_productHelper;

    /**
     * CategoryHelper constructor.
     * @param ProductHelper $productHelper
     * @param \Magento\Framework\DataObject $dataObject
     * @param \Magenest\Eplus\Model\SimpleResultFactory $simpleResultFactory
     * @param ImageHelper $imageHelper
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magenest\Eplus\Helper\ProductHelper $productHelper,
        \Magento\Framework\DataObject $dataObject,
        \Magenest\Eplus\Model\SimpleResultFactory $simpleResultFactory,
        \Magenest\Eplus\Helper\ImageHelper $imageHelper,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    )
    {
        $this->_productHelper = $productHelper;
        $this->_dataObject = $dataObject;
        $this->_simpleResultFactory = $simpleResultFactory;
        $this->_imageHelper = $imageHelper;
        $this->_storeFactory = $storeFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFactory = $categoryFactory;
    }

    public function getHomeCategoryByStore($store)
    {
        try {

            if (!$store) {
                return null;
            }
            $cachedResult = $this->_simpleResultFactory->create()
                ->getCollection()
                ->addFieldToFilter('result_key', 'store_' . $store . '_category')
                ->load()
                ->getFirstItem();
            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue())) {
                return json_decode($cachedResult->getResultValue(), true);
            } else {
                $categoryData = [];
                $currentStore = $this->_storeFactory
                    ->create()
                    ->load($store);
                $rootCategoryId = $currentStore->getRootCategoryId();
                $rootCategory = $this->_categoryFactory->create()
                    ->load($rootCategoryId);
                $mediaPath = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $categories = $this->_categoryFactory
                    ->create()
                    ->getCollection()
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
                    ->addFieldToFilter('level', 2);
                foreach ($categories as $category) {
                    $category = $this->_categoryFactory->create()
                        ->load($category->getEntityId());
                    $categoryImage = $this->_imageHelper->resizeCategory($category->getImage(), 120, 120);
                    if ($categoryImage) {
                        $categoryImage = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $categoryImage;
                    }
                    $categoryData[] = [
                        'id' => $category->getEntityId(),
                        'original_id' => $category->getEntityId(),
                        'name' => $category->getName(),
                        'image' => $categoryImage,
                        'children' => $this->getCategoryChildrenData($rootCategory, $mediaPath)
                    ];
                }
                if (sizeof($categoryData) > 0) {
                    $this->_simpleResultFactory->create()
                        ->setData([
                            'result_key' => 'store_' . $store . '_category',
                            'result_value' => json_encode($categoryData, true)
                        ])
                        ->save();
                    return $categoryData;
                }
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getCategoryDetail($store, $category)
    {
        $result = [
                'api_status' => 0
            ];
        if ($store) {
            $cachedResult = $this->_simpleResultFactory->create()
                ->addFieldToFilter('result_key', 'store_' . $store . '_category_' . $category)
                ->load()
                ->getFirstItem();
            if ($cachedResult->getId() != null && strlen($cachedResult->getResultValue())) {
                $result = json_decode($cachedResult->getResultValue(), true);
            } else {
                $productResult = [];
                $currentCategory = $this->_categoryFactory->create()
                    ->load($category);
                $productList = $currentCategory->getProductCollection()
                    ->setPage(1, 1000)
                    ->setOrder('name','ASC');
                foreach ($productList as $product) {
                    $productInfo = $this->_productHelper->getProductFewDetail($store, $product->getEntityId());
                    if ($productInfo) {
                        $productResult[] = $productInfo;
                    }
                }
                if (sizeof($productResult) > 0) {
                    $result = [
                        'api_status' => 1,
                        'product' => $productResult
                    ];
                    $this->_simpleResultFactory->create()
                        ->getNewEmptyItem()
                        ->setData([
                            'result_key' => 'store_' . $store . '_category_' . $category,
                            'result_value' => json_encode($result)
                        ])
                        ->save();
                }
            }
        }
        return $result;
    }

    private function getCategoryChildrenData($category, $mediaPath)
    {
        $result = null;
        if ($category->getIncludeInMenu()) {
            $result = [];
            $children_data = $category->getChildren();
            $children_array = [];
            if ($children_data != null && strlen($children_data) > 0) {
                $children_array = explode(',', $children_data);
            }
            if (sizeof($children_array) > 0) {
                foreach ($children_array as $children_data_item) {
                    $children_data_item = $this->_categoryFactory->create()
                        ->load($children_data_item);
                    if ($children_data_item->getIsActive()) {
                        $children_data_temp = $this->getCategoryChildrenData($children_data_item, $mediaPath);

                        $categoryImage = $this->_imageHelper->resizeCategory($category->getImage(), 120, 120);
                        if ($categoryImage) {
                            $categoryImage = $mediaPath . $categoryImage;
                        }
                        $resultTemp = [
                            'name' => $children_data_item->getName(),
                            'id' => $children_data_item->getEntityId(),
                            'original_id' => $children_data_item->getEntityId(),
                            'image' => $categoryImage
                        ];
                        if (sizeof($children_data_temp) > 0) {
                            $resultTemp['children'] = $children_data_temp;
                        }
                        $result[] = $resultTemp;

                    }
                }
            }
        }
        return $result;
    }
}