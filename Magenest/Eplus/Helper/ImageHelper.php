<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 14/10/2017
 * Time: 14:02
 */

namespace Magenest\Eplus\Helper;

/**
 * Class ImageHelper
 * @package Magenest\Eplus\Helper
 */
class ImageHelper
{
    protected $_filesystem;
    protected $_imageFactory;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory
    )
    {
        $this->_filesystem = $filesystem;
        $this->_imageFactory = $imageFactory;
    }

    public function resizeCategory($image, $width = null, $height = null)
    {
        if ($image) {
            $oldImageAbsolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('eplus/catalog/category/') . $width . '/' . $image;
            if (!file_exists($oldImageAbsolutePath)) {
                $absolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('catalog/category/') . $image;
                $imageResized = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('eplus/catalog/category/' . $width . '/') . $image;
                $imageResize = $this->_imageFactory->create();
                $imageResize->open($absolutePath);
                $imageResize->constrainOnly(true);
                $imageResize->keepTransparency(true);
                $imageResize->keepFrame(true);
                $imageResize->keepAspectRatio(true);
                $imageResize->backgroundColor(array(235, 235, 235));
                $imageResize->resize($width, $height);
                $destination = $imageResized;
                $imageResize->save($destination);
            }
            return 'eplus/catalog/category/' . $width . '/' . $image;
        }
        return null;
    }
}