<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:22
 */

namespace Magenest\Eplus\Helper;

use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeFactory;

/**
 * Class ProductHelper
 * @package Magenest\Eplus\Helper
 */
class ProductHelper
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    protected $_reportFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * ProductHelper constructor.
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreFactory $storeManager
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param ImageFactory $imageFactory
     * @param ProductFactory $productFactory
     */
    public function __construct(
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreFactory $storeManager,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Swatches\Helper\Data $swatchHelper
    )
    {
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_eavConfig = $eavConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productAttributeFactory = $productAttributeFactory;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_reportFactory = $reportFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_swatchHelper = $swatchHelper;
    }

    public function getHomeMostViewedProducts($storeId)
    {
        $result = null;
        $done = false;
        $count = 0;
        $mostViewedProducts = null;
        while (!$done) {
            switch ($count) {
                case 0:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('month')
                        ->setDateRange(
                            date('Y-m-d', strtotime("first day of last month")),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 1:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('year')
                        ->setDateRange(
                            date('Y-m-d', strtotime('-4 month')),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 2:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPageSize(20)
                        ->load();
                    break;
                default:
                    $done = true;
                    break;
            }
            $count++;
            if ($mostViewedProducts != null && $mostViewedProducts->getSize() > 0) {
                $result = [];
                foreach ($mostViewedProducts as $mostViewedProduct) {
                    $resultTemp = $this->getProductFewDetail($storeId, $mostViewedProduct->getData('product_id'));
                    if ($resultTemp) {
                        $resultTemp['views_num'] = $mostViewedProduct->getData('views_num');
                        $resultTemp['period'] = $mostViewedProduct->getData('period');
                        $result[] = $resultTemp;
                    }
                }
                if (sizeof($result) > 10) {
                    $done = true;
                }
            }
        }
        return $result;
    }

    public function getHomeBestSellingProducts($storeId)
    {
        $result = null;
        $done = false;
        $count = 0;
        $bestsellingProducts = null;

        while (!$done) {
            switch ($count) {
                case 0:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('month')
                        ->setDateRange(
                            date('Y-m-d', strtotime("first day of last month")),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 1:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('year')
                        ->setDateRange(
                            date('Y-m-d', strtotime('-4 month')),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 2:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPageSize(20)
                        ->load();
                    break;
                default:
                    $done = true;
                    break;
            }
            $count++;
            if ($bestsellingProducts->getSize() > 0) {
                $result = [];
                foreach ($bestsellingProducts as $bestsellingProduct) {
                    $resultTemp = $this->getProductFewDetail($storeId, $bestsellingProduct->getData('product_id'));
                    if ($resultTemp) {
                        $resultTemp['qty_ordered'] = $bestsellingProduct->getData('qty_ordered');
                        $resultTemp['period'] = $bestsellingProduct->getData('period');
                        $result[] = $resultTemp;
                    }
                }
                if (sizeof($result) > 10) {
                    $done = true;
                }
            }
        }
        return $result;
    }

    public function getProductFewDetailByIds($storeId, $productIds)
    {
        $result = null;
        if ($storeId != null && sizeof($productIds) > 0) {
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            foreach ($productIds as $productId) {
                $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
                if (sizeof($parents) > 0) {
                    $currentProduct = $this->_productFactory->create()
                        ->load($parents[0]);
                } else {
                    $currentProduct = $this->_productFactory->create()
                        ->load($productId);
                }
                if ($currentProduct->getId()) {
//            $productImage = $this->_imageFactory->create()
//                ->init($currentProduct, 'category_page_grid')
//                ->constrainOnly(FALSE)
//                ->keepAspectRatio(TRUE)
//                ->keepFrame(FALSE)
//                ->resize(400)
//                ->getUrl();
                    $productImage = null;
                    if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                        if (substr($currentProduct->getImage(), 0, 1) == '/') {
                            $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                        } else {
                            $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                        }
                    }

                    $this->_reviewFactory
                        ->create()
                        ->getEntitySummary($currentProduct, $storeId);
                    $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
                    $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();

                    $result[] = [
                        'id' => $currentProduct->getEntityId(),
                        'name' => $currentProduct->getName(),
                        'image' => $productImage,
                        'sku' => $currentProduct->getSku(),
                        'rating' => $ratingSummary,
                        'reviews_count' => $ratingCount,
                        'price' => $currentProduct->getFinalPrice()
                    ];
                }
            }
        }
        return $result;
    }

    public function getProductFewDetail($storeId, $productId)
    {
        $result = null;
        $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
        if (sizeof($parents) > 0) {
            $currentProduct = $this->_productFactory->create()->setStoreId($storeId)
                ->load($parents[0]);
        } else {
            $currentProduct = $this->_productFactory->create()->setStoreId($storeId)
                ->load($productId);
        }
        if ($currentProduct->getId()) {
            $price = 0;
            $finalPrice = 0;
            if ($currentProduct->getTypeId() == 'grouped') {
                $childrenIds = $currentProduct->getTypeInstance()->getChildrenIds($currentProduct->getId());
                foreach ($childrenIds as $key => $value) {
                    foreach ($value as $valueProductId) {
                        $firstChild =  $this->_productFactory->create()->setStoreId($storeId)
                            ->load($valueProductId);
                        $price = $firstChild->getFinalPrice();
                        $finalPrice = $firstChild->getFinalPrice();
                        break;
                    }
                    break;
                }

            } else {
                $price = $currentProduct->getPrice();
                $finalPrice = $currentProduct->getFinalPrice();
            }

//            $productImage = $this->_imageFactory->create()
//                ->init($currentProduct, 'category_page_grid')
//                ->constrainOnly(FALSE)
//                ->keepAspectRatio(TRUE)
//                ->keepFrame(FALSE)
//                ->resize(400)
//                ->getUrl();
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory
                ->create()
                ->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();

            $result = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
            ];
        }
        return $result;
    }

    public function getProductFullDetail($store, $id)
    {
        $finalProductData = $this->_productFactory->create()
            ->load($id);
        if ($finalProductData->getId()) {
            $this->_reviewFactory
                ->create()
                ->getEntitySummary($finalProductData, $store);
            $ratingSummary = $finalProductData->getRatingSummary()->getRatingSummary();
            $ratingCount = $finalProductData->getRatingSummary()->getReviewsCount();
            $finalProductData->addData([
                'rating_summary' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'quantity_and_stock_status' => $finalProductData->getData('quantity_and_stock_status')['is_in_stock'],
                'qty' => $finalProductData->getData('quantity_and_stock_status')['qty']
            ]);

            $topReviews = $this->_reviewCollectionFactory->create()
                ->addStoreFilter($store)
                ->addStatusFilter(
                    \Magento\Review\Model\Review::STATUS_APPROVED
                )
                ->addEntityFilter(
                    'product',
                    $finalProductData->getId()
                )
                ->setPageSize(30)
                ->getData();
            $finalProductData->addData([
                'top_reviews' => $topReviews
            ]);
            $finalProductData->addData([
                'media_url' => $this->_storeManager->create()
                    ->load($store)
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
            ]);
            $all_custom_attributes = $this->getCustomAttributesValue();
            $custom_attributes = $finalProductData->getCustomAttributes();
            $custom_attributes_result = [];
            foreach ($custom_attributes as $custom_attributes_item) {
                $fixed = false;
                foreach ($all_custom_attributes as $all_custom_attributes_item) {
                    if ($all_custom_attributes_item['code'] == $custom_attributes_item->getAttributeCode() && $fixed == false) {
                        if (array_key_exists('value', $all_custom_attributes_item)) {
                            foreach ($all_custom_attributes_item['value'] as $all_custom_attributes_item_value) {
                                if (intval($all_custom_attributes_item_value['value']) == intval($custom_attributes_item->getValue())
                                    && $fixed == false) {
                                    $custom_attributes_result[] = [
                                        'attribute_code' => $custom_attributes_item->getAttributeCode(),
                                        'value' => $all_custom_attributes_item_value['label'],
                                        'visible' => $all_custom_attributes_item['visible']
                                    ];
                                    $fixed = true;
                                    break;
                                }
                            }
                        } else {
                            $custom_attributes_result[] = [
                                'attribute_code' => $custom_attributes_item->getAttributeCode(),
                                'value' => $custom_attributes_item->getValue(),
                                'visible' => $all_custom_attributes_item['visible']
                            ];
                        }
                    }
                }
            }

            $finalProductData->addData([
                'custom_attribute_value' => $custom_attributes_result
            ]);
            $productLinks = $finalProductData->getProductLinks();
            if (sizeof($productLinks) > 0) {
                $productLinksResult = [];
                foreach ($productLinks as $productLink) {
                    $productLinkData = [];
                    $productLinkData['link_type'] = $productLink->getLinkType();
                    $productLinkData['position'] = $productLink->getPosition();
                    $currentProduct = $this->_productRepository->get($productLink->getLinkedProductSku());
                    $currentProduct = $this->_productFactory->create()->load($currentProduct->getId());
                    $productLinkData['image'] = $currentProduct->getImage();
                    $productLinkData['id'] = $currentProduct->getId();
                    $productLinkData['price'] = $currentProduct->getPrice();
                    $productLinkData['final_price'] = $currentProduct->getFinalPrice();
                    $productLinksResult[] = $productLinkData;
                }
                $finalProductData->setProductLinks([]);
                $finalProductData->addData([
                    'product_link' => $productLinksResult
                ]);
            }
            $finalProductData->addData([
                'final_price' => $finalProductData->getFinalPrice()
            ]);
            if ($finalProductData->getTypeId() == "configurable") {
                $configurable_product_links = $finalProductData->getExtensionAttributes()->getConfigurableProductLinks();
                if ($configurable_product_links != null && sizeof($configurable_product_links) > 0) {
                    $configurable_product_links_info = [];
                    foreach ($configurable_product_links as $configurable_product_links_item) {
                        $configurable_product_options_product = $this->_productFactory->create()
                            ->load($configurable_product_links_item);
                        $imageList = [];
                        $media_gallery_images = $configurable_product_options_product->getMediaGalleryImages();
                        foreach ($media_gallery_images as $media_gallery_image) {
                            $imageList[] = $media_gallery_image->getFile();
                        }
                        $configurable_product_links_info[] = [
                            'id' => $configurable_product_options_product->getEntityId(),
                            'status' => $configurable_product_options_product->getStatus(),
                            'image' => $imageList,
                            'qty' => $configurable_product_options_product->getData('quantity_and_stock_status')['qty'],
                            'final_price' => $configurable_product_options_product->getFinalPrice()
                        ];
                    }
                    if (sizeof($configurable_product_links_info) > 0) {
                        $finalProductData->addData([
                            'configurable_product_links_info' => $configurable_product_links_info
                        ]);
                    }
                    $configurable_product_options = $finalProductData->getExtensionAttributes()->getConfigurableProductOptions();
                    $configurable_product_options_value = [];
                    foreach ($configurable_product_options as $configurable_product_options_item) {
                        foreach ($configurable_product_options_item->getOptions() as $option) {
                            $newConfigurable_product_options_value = [
                                'value_index' => $option['value_index'],
                                'label' => $option['store_label']
                            ];
                            $realValue = $this->_swatchHelper->getSwatchesByOptionsId([$option['value_index']]);
                            if ($realValue != null && array_key_exists($option['value_index'], $realValue)) {
                                $newConfigurable_product_options_value['value'] = $realValue[$option['value_index']]['value'];
                            } else {
                                $newConfigurable_product_options_value['value'] = $option['store_label'];
                            }
                            $configurable_product_options_value[] = $newConfigurable_product_options_value;
                        }
                    }
                    $finalProductData->addData([
                        'configurable_product_option_value' => $configurable_product_options_value
                    ]);
                }
                $configurable_product_option_type = [];
                foreach ($finalProductData->getExtensionAttributes()
                             ->getConfigurableProductOptions() as $extensionAttribute) {
                    $additionalData = $extensionAttribute->getProductAttribute()->getAdditionalData();
                    $additionalData = json_decode($additionalData, true);
                    $swatch_input_type = 'text';
                    if ($additionalData != null && array_key_exists('swatch_input_type', $additionalData)) {
                        $swatch_input_type = $additionalData['swatch_input_type'];
                    }
                    $configurable_product_option_type[] = [
                        'id' => $extensionAttribute->getAttributeId(),
                        'swatch_input_type' => $swatch_input_type
                    ];
                }
                $finalProductData->addData([
                    'configurable_product_option_type' => $configurable_product_option_type
                ]);
            }

            $finalProductData->addData([
                'url' => $finalProductData->getProductUrl()
            ]);
            return $finalProductData;
        } else {
            return $finalProductData;
        }
    }

    private function getCustomAttributesValue()
    {
        $result = null;
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        $attributes = $this->_productAttributeFactory
            ->getList('catalog_product', $searchCriteria)
            ->getItems();
        foreach ($attributes as $attribute) {

//            if (
//                $attribute->getAttributeCode() != 'name'
//                && $attribute->getAttributeCode() != 'description'
//                && $attribute->getAttributeCode() != 'short_description'
//                && $attribute->getAttributeCode() != 'sku'
//                && $attribute->getAttributeCode() != 'price'
//                && $attribute->getAttributeCode() != 'special_price'
//                && $attribute->getAttributeCode() != 'special_from_date'
//                && $attribute->getAttributeCode() != 'special_to_date'
//                && $attribute->getAttributeCode() != 'cost'
//                && $attribute->getAttributeCode() != 'weight'
//                && $attribute->getAttributeCode() != 'meta_title'
//                && $attribute->getAttributeCode() != 'meta_keyword'
//                && $attribute->getAttributeCode() != 'meta_description'
//                && $attribute->getAttributeCode() != 'image'
//                && $attribute->getAttributeCode() != 'small_image'
//                && $attribute->getAttributeCode() != 'thumbnail'
//                && $attribute->getAttributeCode() != 'media_gallery'
//                && $attribute->getAttributeCode() != 'old_id'
//                && $attribute->getAttributeCode() != 'tier_price'
//                && $attribute->getAttributeCode() != 'has_options'
//                && $attribute->getAttributeCode() != 'custom_design_from'
//                && $attribute->getAttributeCode() != 'custom_design_to'
//                && $attribute->getAttributeCode() != 'gallery'
//                && $attribute->getAttributeCode() != 'status'
//                && $attribute->getAttributeCode() != 'url_key'
//                && $attribute->getAttributeCode() != 'url_path'
//                && $attribute->getAttributeCode() != 'minimal_price'
//                && $attribute->getAttributeCode() != 'custom_layout_update'
//                && $attribute->getAttributeCode() != 'page_layout'
//                && $attribute->getAttributeCode() != 'category_ids'
//                && $attribute->getAttributeCode() != 'required_options'
//                && $attribute->getAttributeCode() != 'image_label'
//                && $attribute->getAttributeCode() != 'small_image_label'
//                && $attribute->getAttributeCode() != 'thumbnail_label'
//                && $attribute->getAttributeCode() != 'created_at'
//                && $attribute->getAttributeCode() != 'updated_at'
//                && $attribute->getAttributeCode() != 'country_of_manufacture'
//                && $attribute->getAttributeCode() != 'msrp_display_actual_price_type'
//                && $attribute->getAttributeCode() != 'msrp'
//                && $attribute->getAttributeCode() != 'gift_message_available'
//                && $attribute->getAttributeCode() != 'price_type'
//                && $attribute->getAttributeCode() != 'sku_type'
//                && $attribute->getAttributeCode() != 'weight_type'
//                && $attribute->getAttributeCode() != 'price_view'
//                && $attribute->getAttributeCode() != 'shipment_type'
//                && $attribute->getAttributeCode() != 'links_purchased_separately'
//                && $attribute->getAttributeCode() != 'samples_title'
//                && $attribute->getAttributeCode() != 'links_title'
//                && $attribute->getAttributeCode() != 'links_exist'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//                && $attribute->getAttributeCode() != 'custom_layout'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//            ) {
//                $result[] = $attribute->getData();
//            }
            if ($attribute->getFrontendInput() == 'select') {
                $allAttributeOptions = $this->_eavConfig->getAttribute('catalog_product', $attribute->getAttributeCode())
                    ->getSource()->getAllOptions();
                if (sizeof($allAttributeOptions) > 0) {
                    $allAttributeOptionsArray = [];
                    foreach ($allAttributeOptions as $allAttributeOption) {
                        if ($allAttributeOption['value']
                            && strlen($allAttributeOption['value']) > 0
                            && strlen($allAttributeOption['label']) > 0) {
                            $allAttributeOptionsArray[] = [
                                'label' => is_string($allAttributeOption['label']) ? $allAttributeOption['label'] : $allAttributeOption['label']->getText(),
                                'value' => $allAttributeOption['value']
                            ];
                        }
                    }
                    if (sizeof($allAttributeOptionsArray) > 0) {
                        $result[] = [
                            'code' => $attribute->getAttributeCode(),
                            'value' => $allAttributeOptionsArray,
                            'visible' => $attribute->getIsVisibleOnFront()
                        ];
                    }
                }
            } else if ($attribute->getFrontendInput() == 'boolean') {
                $result[] = [
                    'code' => $attribute->getAttributeCode(),
                    'value' => [
                        [
                            'value' => 0,
                            'label' => 'false'
                        ],
                        [
                            'value' => 1,
                            'label' => 'true'
                        ]
                    ],
                    'visible' => $attribute->getIsVisibleOnFront()
                ];
            } else {
                $result[] = [
                    'code' => $attribute->getAttributeCode(),
                    'visible' => $attribute->getIsVisibleOnFront()
                ];
            }
        }
        return $result;
    }
}
