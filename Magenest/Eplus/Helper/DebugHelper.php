<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 06/08/2017
 * Time: 14:10
 */
namespace Magenest\Eplus\Helper;

/**
 * Class DebugHelper
 * @package Magenest\Eplus\Helper
 */
class DebugHelper
{
    /**
     * @param string $errorCode
     * @param string $data
     */
    public function debugString($errorCode, $data)
    {
        try {
            $errorCodeFile = strtolower($errorCode);
            $errorCodeFile = str_replace(' ', '_', $errorCodeFile);
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/epluserror' . $errorCodeFile . '.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($errorCode . ':' . $data);
        } catch (\Exception $e) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/myepluserror.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('debugString:' . $e->getMessage());
        }
    }
}

