<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\CountdownTimer\Helper;


use Magento\Framework\App\ObjectManager;

class CountdownHelper
{
    /**
     * Check if time is < 24h to now
     * @param $time
     * @param $productId
     * @return bool
     */
    public static function canShowCountDown($time, $productId)
    {
        $objectManager = ObjectManager::getInstance();
        $productChildren = $objectManager->create('Magenest\Groupon\Model\Deal')->getCollection()->addFieldToFilter('parent_id', $productId)->getData();
        return isset($productChildren[0])
            && $productChildren[0]['product_type'] == 'coupon'
            && (strtotime($time) > time())
            && strtotime($time) - time() < 172800;
    }
}