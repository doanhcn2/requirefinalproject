<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CountdownTimer\Block\Product;

/***
 * Class ListProduct
 * @package Magenest\CountdownTimer\Block\Product
 */
class ListProduct
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager=$objectManager;
    }

    /**
     * @param $subject
     * @param $product
     */
    public function beforeGetProductDetailsHtml($subject, $product)
    {
        $this->product=$product;
    }

    /**
     * @param \Magento\Catalog\Block\Product\ListProduct $subject
     * @return string
     */
    public function afterGetProductDetailsHtml($subject,$result)
    {
        $blockProduct = $subject->getLayout()->createBlock('Magenest\CountdownTimer\Block\Product\Category');
        $blockProduct->setProduct($this->product);
        $html = $blockProduct->setTemplate('Magenest_CountdownTimer::category.phtml')->toHtml();

        return $result.$html;
    }
}
