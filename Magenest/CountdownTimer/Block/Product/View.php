<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CountdownTimer\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Catalog\Block\Product\Context;

/**
 * Class View
 * @package Magenest\CountdownTimer\Block\Product
 */
class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * @var
     */
    protected $countdown;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    public function __construct(Context $context,
                                \Magento\Framework\Url\EncoderInterface $urlEncoder,
                                \Magento\Framework\Json\EncoderInterface $jsonEncoder,
                                \Magento\Framework\Stdlib\StringUtils $string,
                                \Magento\Catalog\Helper\Product $productHelper,
                                \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
                                \Magento\Framework\Locale\FormatInterface $localeFormat,
                                \Magento\Customer\Model\Session $customerSession,
                                ProductRepositoryInterface $productRepository,
                                \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
                                \Magenest\Groupon\Model\DealFactory $dealFactory,
                                array $data = [])
    {
        $this->_dealFactory = $dealFactory;
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);
    }

    /***
     * @return mixed
     */
    public function isCountdownTimerEnabled()
    {
        return $this->getProduct()->getData('countdown_enabled');
    }

    /**
     * get final date
     * @return mixed
     */
    public function getToDate()
    {
        $product = $this->getCurrentProduct();
        $productType = $product->getTypeId();
        if ($productType === 'configurable') {
            $deal = $this->_dealFactory->create()->load($product->getId(), 'product_id');
            $endTime = $deal->getEndTime();
            if (@$endTime) {
                return $endTime;
            }
        }
//        elseif ($productType === 'simple') {
//            $vendorId = $this->getProduct()->getCustomAttribute('udropship_vendor')->getValue();
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
//            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
//            $connection = $resource->getConnection();
//            $tableName = $resource->getTableName('udropship_vendor_product'); //gives table name with prefix
//            $sql = "Select special_to_date FROM " . $tableName . " Where product_id = " . $this->getProduct()->getId() . " and vendor_id = " . $vendorId;
//            $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
//
//            if (@$result[0]['special_to_date']) {
//                return $result[0]['special_to_date'];
//            }
//        }

        return false;
    }

    /***
     * get start date
     * @return mixed
     */
    public function getFromDate()
    {
        $product = $this->getCurrentProduct();
        $productType = $product->getTypeId();
        if ($productType === 'configurable') {
            $deal = $this->_dealFactory->create()->load($product->getId(), 'product_id');
            $endTime = $deal->getStartTime();
            if (@$endTime) {
                return $endTime;
            }
        }
//        elseif ($productType === 'simple') {
//            $vendorId = $this->getProduct()->getCustomAttribute('udropship_vendor')->getValue();
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
//            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
//            $connection = $resource->getConnection();
//            $tableName = $resource->getTableName('udropship_vendor_product'); //gives table name with prefix
//            $sql = "Select special_from_date FROM " . $tableName . " Where product_id = " . $this->getProduct()->getId() . " and vendor_id = " . $vendorId;
//            $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
//
//            if ($result[0]['special_from_date']) {
//                return $result[0]['special_from_date'];
//            }
//        }

        return false;
    }

    /***
     * check show countdown
     * @return bool
     */
    public function getReadyCountDown()
    {
        if ($this->getToDate()) {
            $currentDate =  date('d-m-Y H:i:s');
            $todate      =  $this->getToDate();
            $date = date_create_from_format('Y-m-d H:i:s', $todate);
            $dateShow = $date->format('Y-m-d H:i:s');
            if (\Magenest\CountdownTimer\Helper\CountdownHelper::canShowCountDown($dateShow, $this->getProductId()) && strtotime($todate) > strtotime($currentDate)) {
                return true;
            }
        }

        return false;
    }

    /***
     * @return int
     */
    public function getProductId()
    {
        return $this->getProduct()->getId();
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getCurrentProduct()
    {
        $result = $this->getProduct();

        return $result;
    }
}
