<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CountdownTimer\Block\Product;

/***
 * Class Category
 * @package Magenest\CountdownTimer\Block\Product
 */
class Category extends \Magento\Framework\View\Element\Template
{
    /***
     * @var \Magento\Catalog\Model\Product;
     */
    protected $product;

    /***
     * @param $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /***
     * @return int
     */
    public function getProductId()
    {
        return $this->product->getId();
    }

    /***
     * @return mixed
     */
    public function isCountdownTimerEnabled()
    {
        return $this->product->getData('countdown_enabled');
    }

    /***
     * @return mixed
     */
    public function getToDate()
    {
        $vendorId = $this->product->getCustomAttribute('udropship_vendor')->getValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('udropship_vendor_product'); //gives table name with prefix

        $sql = "Select special_to_date FROM " . $tableName ." Where product_id = " .$this->product->getId(). " and vendor_id = ".$vendorId;
        $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.

        if($result[0]['special_to_date']){
            return $result[0]['special_to_date'];
        }

        return false;
    }

    /***
     * @return mixed
     */
    public function getFromDate()
    {
        $vendorId = $this->product->getCustomAttribute('udropship_vendor')->getValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('udropship_vendor_product'); //gives table name with prefix

        $sql = "Select special_from_date FROM " . $tableName ." Where product_id = " .$this->product->getId(). " and vendor_id = ".$vendorId;
        $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.

        if($result[0]['special_from_date']){
            return $result[0]['special_from_date'];
        }

        return false;
    }

    /***
     * @return bool
     */
    public function getReadyCountDown()
    {
        if ($this->getToDate() && $this->getFromDate()) {
            $currentDate =  date('d-m-Y');
            $todate      =  $this->getToDate();
            $fromdate    =  $this->getFromDate();
            if (strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)) {
                return true;
            }
        }

        return false;
    }


    /***
     * @return bool
     */
    public function getCountdownTimer()
    {
        return true;
    }
}
