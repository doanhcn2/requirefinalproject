<?php

namespace Magenest\VendorApi\Model;

use Magenest\VendorApi\Api\SaleApiInterface;
use Magento\Framework\DataObject;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory as ShipmentCollectionFactory;
use Magenest\VendorApi\Helper\SaleHelper;

class SaleApi implements SaleApiInterface
{
    /** @var DataObject  */
    protected $_objectFactory;

    /** @var ShipmentCollectionFactory */
    protected $shipmentCollectionFactory;

    /** @var SaleHelper */
    protected $saleHelper;

    /**
     * Constructor.
     *
     * @param DataObject $dataObject
     * @param ShipmentCollectionFactory $shipmentCollectionFactory
     * @param SaleHelper $saleHelper
     */
    function __construct(
        DataObject $dataObject,
        ShipmentCollectionFactory $shipmentCollectionFactory,
        SaleHelper $saleHelper
    )
    {
        $this->_objectFactory = $dataObject;
        $this->shipmentCollectionFactory = $shipmentCollectionFactory;
        $this->saleHelper = $saleHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerShipmentList($customerId)
    {
        $result = [];
        $now = date("Y-m-d h:i:s");
        $fromDate = date('Y-m-d h:i:s', strtotime('-90 day', strtotime($now)));

        $shipmentCollection = $this->shipmentCollectionFactory->create()
            ->addFieldToFilter('customer_id', array('eq' => $customerId))
            ->addFieldToFilter('created_at', array('from' => $fromDate, 'to' => $now));

        /** @var \Magento\Sales\Model\Order\Shipment $shipment */
        foreach ($shipmentCollection as $shipment) {
            $data = $shipment->getData();
            array_push($result, [
                'general_information' => [
                    'order_information' => [
                        'shipment_id' => $data['entity_id'],
                        'order_id' => $data['order_id'],
                        'status' => $data['shipment_status'],
                        'created_at' => $data['created_at']
                    ],
                    'status_information' => $this->saleHelper->getShipmentHistory($shipment),
                    'item_collection' => $this->saleHelper->getShipmentItems($shipment)
                ],
                'detail_information' => $this->saleHelper->getShipmentDetailsInformation($shipment)
            ]);
        }

        return $this->_objectFactory
            ->addData([
                'result' => $result,
                'api_status' => 1
            ]);
    }
}
