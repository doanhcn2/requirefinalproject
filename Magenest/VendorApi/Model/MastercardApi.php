<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/6/18
 * Time: 11:09 AM
 */

namespace Magenest\VendorApi\Model;

use Magento\Framework\Exception\LocalizedException;
use Zend\Http\Request;

class MastercardApi implements \Magenest\VendorApi\Api\MastercardApiInterface
{
    const INIT_SESSION_URL = '%sapi/rest/version/%s/merchant/%s/session';
    const UPDATE_FORM_URL = '%sform/version/%s/merchant/%s/session/%s';

    /**
     * @var \OnTap\MasterCard\Gateway\Config\ConfigFactory
     */
    protected $configFactory;

    /**
     * @var \OnTap\MasterCard\Gateway\Config\Config
     */
    protected $config;

    public function __construct(
        \OnTap\MasterCard\Gateway\Config\ConfigFactory $mspgConfigFactory
    ) {
        $this->configFactory = $mspgConfigFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function sessionize($cardNumber, $expiryMonth, $expiryYear, $securityCode = null)
    {
        try {
            $sessionId = $this->initSessionId();
            $this->updateSessionForm($sessionId, $cardNumber, $expiryMonth, $expiryYear, $securityCode = null);
            return $sessionId;
        } catch (\Zend_Http_Client_Exception $e) {
            throw new LocalizedException(__("Error occurred during sessionization"), $e);
        }
    }

    /**
     * @param string $sessionId
     * @param string $cardNumber
     * @param string $expiryMonth
     * @param string $expiryYear
     * @param string|null $securityCode
     * @return mixed
     * @throws \Zend_Http_Client_Exception
     */
    protected function updateSessionForm($sessionId, $cardNumber, $expiryMonth, $expiryYear, $securityCode = null)
    {
        $config = $this->getMspgConfig();

        $endpoint = $config->getApiAreaUrl();
        $version = $config->getValue('api_version');
        $merchantId = $config->getMerchantId();

        $url = sprintf(self::UPDATE_FORM_URL, $endpoint, $version, $merchantId, $sessionId);

        $bodyForm = [];
        $bodyForm[] = ['field' => 'cardNumber', 'value' => $cardNumber];
        $bodyForm[] = ['field' => 'expiryMonth', 'value' => $expiryMonth];
        $bodyForm[] = ['field' => 'expiryYear', 'value' => $expiryYear];
        if ($securityCode !== null) {
            $bodyForm[] = ['field' => 'securityCode', 'value' => $securityCode];
        }

        $bodyString = json_encode($bodyForm);

        $client = new \Zend\Http\Client();
        $client->setUri($url);
        $client->setMethod(Request::METHOD_POST);
        $client->setHeaders(['Content-Type' => 'application/json']);
        $client->setRawBody($bodyString);

        $res = $client->send();
        if ($res->getStatusCode() === 200) {
            $content = json_decode($res->getContent(), true);
            if ($content["status"] === "ok") {
                return $this->frameEmbeddingMitigation($url);
            }
        }
        throw new \Zend_Http_Client_Exception($res->getContent());
    }

    /**
     * @param $url
     * @throws \Zend_Http_Client_Exception
     */
    protected function frameEmbeddingMitigation($url)
    {
        $body = '{"frameEmbeddingMitigation":["x-frame-options"]}';

        $client = new \Zend\Http\Client();
        $client->setUri($url);
        $client->setMethod(Request::METHOD_PUT);
        $client->setHeaders(['Content-Type' => 'application/json']);
        $client->setRawBody($body);

        $res = $client->send();
        if ($res->getStatusCode() === 200) {
            $content = json_decode($res->getContent(), true);
            if ($content["status"] === "ok") {
                return;
            }
        }
        throw new \Zend_Http_Client_Exception($res->getContent());
    }

    /**
     * @return string
     * @throws \Zend_Http_Client_Exception
     */
    protected function initSessionId()
    {
        $config = $this->getMspgConfig();

        $endpoint = $config->getApiAreaUrl();
        $version = $config->getValue('api_version');
        $merchantId = $config->getMerchantId();
        $merchantPassword = $config->getMerchantPassword();

        $url = sprintf(self::INIT_SESSION_URL, $endpoint, $version, $merchantId);

        $client = new \Zend\Http\Client();
        $client->setUri($url);
        $client->setMethod(Request::METHOD_POST);
        $client->setAuth("merchant.$merchantId", $merchantPassword);

        $res = $client->send();
        if ($res->getStatusCode() === 201) {
            $content = json_decode($res->getContent(), true);
            if ($content["result"] === "SUCCESS") {
                return $content["session"]["id"];
            }
        }
        throw new \Zend_Http_Client_Exception($res->getContent());
    }

    /**
     * @return \OnTap\MasterCard\Gateway\Config\Config
     */
    protected function getMspgConfig()
    {
        if (!$this->config) {
            $this->config = $this->configFactory->create();
            $this->config->setMethodCode(\OnTap\MasterCard\Model\Ui\Hpf\ConfigProvider::METHOD_CODE);
        }
        return $this->config;
    }
}
