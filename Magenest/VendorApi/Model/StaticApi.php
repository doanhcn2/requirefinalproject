<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 09/04/2018
 * Time: 09:36
 */

namespace Magenest\VendorApi\Model;

use Magento\Framework\Exception\LocalizedException;
use Zend\Http\Request;

class StaticApi implements \Magenest\VendorApi\Api\StaticInterface
{
    protected $_objectFactory;
    protected $_scopeConfig;
    protected $pageCollectionFactory;

    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_objectFactory = $objectFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->pageCollectionFactory = $pageCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getForCustomer($storeId)
    {
        return $this->getData('customer', $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getForVendor($storeId){
        return $this->getData('vendor', $storeId);
    }

    public function getData($type, $storeId)
    {
        return $this->_objectFactory
            ->addData([
                'preferences' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/preferences', 'stores', $storeId),
                'help' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/help', 'stores', $storeId),
                'term_of_use' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/term_of_use', 'stores', $storeId),
                'privacy_policy' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/privacy_policy', 'stores', $storeId),
                'license' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/license', 'stores', $storeId),
                'reward_program' => $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/reward_program', 'stores', $storeId),
            ]);
    }
    public function getMobileConfigurableForCustomer($storeId){
        return $this->getMobileConfigurable('customer',$storeId);
    }
    public function getMobileConfigurableForVendor($storeId){
        return $this->getMobileConfigurable('vendor',$storeId);
    }
    public function getMobileConfigurable($type,$storeId){
        $preferences = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/preferences', 'stores', $storeId);
        $array = explode("/",$preferences);
        $url_key_preferences = end($array);

        $help = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/help', 'stores', $storeId);
        $array = explode("/",$help);
        $url_key_help = end($array);

        $term_of_use = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/term_of_use', 'stores', $storeId);
        $array = explode("/",$term_of_use);
        $url_key_term_of_use = end($array);

        $privacy_policy = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/privacy_policy', 'stores', $storeId);
        $array = explode("/",$privacy_policy);
        $url_key_privacy_policy = end($array);

        $license = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/license', 'stores', $storeId);
        $array = explode("/",$license);
        $url_key_license = end($array);

        $reward_program = $this->_scopeConfig->getValue('vendorapi/static/' . $type . '/reward_program', 'stores', $storeId);
        $array = explode("/",$reward_program);
        $url_key_reward_program = end($array);

        /** @var \Magento\Cms\Model\ResourceModel\Page\Collection $collection */
        $collection = $this->pageCollectionFactory->create();
        $result = [];
        foreach ($collection as $cl){
            $data = $cl->getData();
            $identifier = $data['identifier'];
            if($identifier == $url_key_preferences){
                $result['preferences'] = $data['page_id'];
            }
            if($identifier == $url_key_help){
                $result['help'] = $data['page_id'];
            }
            if($identifier == $url_key_term_of_use){
                $result['term_of_use'] = $data['page_id'];
            }
            if($identifier == $url_key_privacy_policy){
                $result['privacy_policy'] = $data['page_id'];
            }
            if($identifier == $url_key_license){
                $result['license'] = $data['page_id'];
            }
            if($identifier == $url_key_reward_program){
                $result['reward_program'] = $data['page_id'];
            }
        }
        return $this->_objectFactory
            ->addData($result);
    }
}
