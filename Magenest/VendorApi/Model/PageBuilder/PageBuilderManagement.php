<?php
/**
 * Contributor company: iPragmatech solution Pvt Ltd.
 * Contributor Author : Manish Kumar
 * Date: 23/5/16
 * Time: 11:55 AM
 */

namespace Magenest\VendorApi\Model\PageBuilder;

use Magenest\VendorApi\Api\PageBuilder\PageBuilderManagementInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Ves\PageBuilder\Model\BlockFactory;
use Ves\PageBuilder\Model\ResourceModel\Block\CollectionFactory;
use Magenest\Groupon\Helper\Data as GrouponDataHelper;

/**
 * Defines the implementaiton class of the PageBuilderManagementInterface
 */
class PageBuilderManagement implements PageBuilderManagementInterface
{
    protected $_pageBuilderCollectionFactory;

    protected $_pageBuilderFactory;

    protected $_objectFactory;

    protected $storeManagerInterFace;
    protected $newWidget;
    protected $reviewFactory;
    protected $_modelCategoryFactory;
    protected $_sellYourRatingHelper;
    private $productCollectionFactory;
    protected $eventTicketHelper;
    protected $_reviewHelper;

    /**
     * PageBuilderManagement constructor.
     * @param DataObject $objectFactory
     * @param BlockFactory $pageBuilderFactory
     * @param CollectionFactory $pageBuilderCollectionFactory
     */
    public function __construct(
        DataObject $objectFactory,
        BlockFactory $pageBuilderFactory,
        CollectionFactory $pageBuilderCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterFace,
        \Magento\Catalog\Block\Product\Widget\NewWidget $newWidget,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Catalog\Model\CategoryFactory $modelCategoryFactory,
        \Magenest\SellYours\Helper\RatingHelper $sellYourRatingHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $_reviewHelper
    )
    {
        $this->_objectFactory = $objectFactory;
        $this->_pageBuilderFactory = $pageBuilderFactory;
        $this->_pageBuilderCollectionFactory = $pageBuilderCollectionFactory;
        $this->storeManagerInterFace = $storeManagerInterFace;
        $this->newWidget = $newWidget;
        $this->reviewFactory = $reviewFactory;
        $this->_modelCategoryFactory = $modelCategoryFactory;
        $this->_sellYourRatingHelper = $sellYourRatingHelper;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->eventTicketHelper = $eventTicketHelper;
        $this->_reviewHelper = $_reviewHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getPageBuilderById($pageId, $store = null)
    {
        $collection = $this->_pageBuilderFactory->create()->load($pageId);
        $params = json_decode($collection->getParams(), true);

        $rawData = $this->getRows($params, $store);
        $result = $this->_objectFactory->addData([
            'result' => $rawData
        ]);
        return $result;
    }

    public function getCols($cols, $store = null)
    {
        $rawData = [];
        foreach ($cols as $col) {
            $data['number_rows'] = count($col['rows']);
            $data['number_widgets'] = count($col['widgets']);
            $data['rows'] = $this->getRows($col['rows'],$store);
            $data['widgets'] = $this->getWidgets($col['widgets'], $store);
            $rawData[] = $data;
        }
        return $rawData;
    }

    public function getWidgets($widgets, $store = null)
    {
        $rawData = [];
        foreach ($widgets as $widget) {
            $connection = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            /** @var \Magento\Framework\DB\Adapter\Pdo\Mysql $conn */
            $conn = $connection->getConnection();
            $select = $conn->select()
                ->from(
                    ['o' => 'ves_blockbuilder_widget']
                )->columns('widget_shortcode')
                ->where('o.widget_key=?', $widget['wkey'])->limit(1);
            $data = $conn->fetchAssoc($select);
            $data = reset($data);
            $widget['shortcode'] = isset($data) ? $data['widget_shortcode'] : '';
            $widgetXml = '<widget ' . str_replace(['{{widget ', '}}'], [''], $widget['shortcode']) . ' />';
            $data = simplexml_load_string($widgetXml);
            $result = [];
            foreach ($data->attributes() as $a => $b) {
                $result[$a] = "" . $b;
            }
            $result2 = $this->getCollection($result, $store);
            $rawData[] = $result2;
        }
        return $rawData;
    }

    public function getRows($rows, $store = null)
    {
        $rawData = [];
        if (count($rows) > 0) {

            foreach ($rows as $row) {
                $data['number_cols'] = count($row['cols']);
                $data['cols'] = $this->getCols($row['cols'], $store);
                $rawData[] = $data;
            }
        }
        return $rawData;
    }

    public function getCollection($widget, $store = null)
    {
        $collectionFactory = \Magento\Framework\App\ObjectManager::getInstance()->get($widget['type']);
        switch ($widget['type']) {
            case 'Ves\BaseWidget\Block\Widget\Carousel':
                $result = $widget;
                $carouselCollection = [];
                for($i=1; $i <=18 ; $i ++){

                    if(isset($widget['content_'.$i])) {
                        $content = base64_decode($widget['content_'.$i]);
                        $carouselCollection[] = $this->getSliderInfo($content);

                        unset($result['content_'.$i]);
                    }
                    if(isset($widget['content_'.$i]))
                        unset($result['cms_'.$i]);
                }
                $result['slider_items'] = $carouselCollection;
                foreach ($result as $key => $value){
                    if($key != 'type' && $key != 'slider_items'){
                        unset($result[$key]);
                    }
                }
                return $result;
                break;
            case 'Ves\BaseWidget\Block\Widget\Dealproduct':
                $result = $widget;
                $tabs = unserialize(base64_decode($widget['tabs']));
                $result['products'] = [];
                $categoryId = '';
                /** @var \Ves\BaseWidget\Block\Widget\Dealproduct $collectionFactory */
                foreach ($tabs as $tab){
                    if(isset($tab['category_id'])){
                        $categoryId = $tab['category_id'];
                        $productCollection = $collectionFactory->getProductsBySource($widget['productsource'], ['categories' => [$tab['category_id']]]);
                        $result =  $this->getProductsInfo($productCollection, $result, $store);
                        break;
                    }
                }

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                $categoryFactory = $objectManager->create('\Magento\Catalog\Model\CategoryFactory');
                $category = $categoryFactory->create()->getCollection()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', (int)$categoryId)->getFirstItem();
                $categoryType = $category->getCategoryType();
                /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCategoryCollection */
                $productCategoryCollection = $this->productCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addCategoryFilter($category);
                $count = $productCategoryCollection->getSize();
                $result['see_more'] = ['category_id' => $categoryId, 'total_count' => $count, 'category_type' => $categoryType];
                return $result;
                break;
            case 'Ves\BaseWidget\Block\Widget\Html':
                $widget['html'] = base64_decode($widget['html']);
                return $widget;
                break;
            default:
                return $widget;
        }
    }
    public function getSliderInfo($content){
        $filterManager = ObjectManager::getInstance()->get('Magento\Cms\Model\Template\FilterProvider');
        $content = $filterManager->getPageFilter()->filter($content);
        $doc = new \DOMDocument();
        @$doc->loadHTML($content);;
        $a = $doc->getElementsByTagName('a')[0];
        $img = $doc->getElementsByTagName('img')[0];
        $text = strip_tags($content);

        $result['target'] = isset($a) ? $a->getAttribute('href') : '#';
        $result['img'] = isset($img) ? $img->getAttribute('src') : '#';
        $result['text'] = trim($text);
        return $result;
    }
    public function getProductsInfo($products, $result, $store)
    {
        $result['products'] = isset($result['products'])? $result['products'] : [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($products->getItems() as $product) {
            $vendor_id = $product->getUdropshipVendor() ? $product->getUdropshipVendor() : null;
            $vendor_name = 'Unknown Vendor';
            if(isset($vendor_id)){
                $_v = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data')->getVendor($vendor_id);
                $vendor_name = $_v->getVendorName();
            }
            $imageUrl = $product->getSmallImage() ?
                $this->storeManagerInterFace->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage() : '';
            $currency = $this->storeManagerInterFace->getStore()->getCurrentCurrency()->getCode();
            $ratingSummary = null;
            $ratingCount = null;
            $reviewCollection = $this->_sellYourRatingHelper->getReviewCollection($product->getEntityId(),$isApi = false, $vendorId = null);
            $reviewIds = [];
            foreach ($reviewCollection as $review){
                $reviewIds[] = $review->getReviewId();
            }
            $rating = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\View\Rating');
            $avgRate = $rating->getAvgRate($reviewIds);
            $ratingSummary = intval($avgRate*20);
            $ratingCount = $reviewCollection->count();
            $product_type = $product->getTypeId();
            $product_id = $product->getEntityId();
            $link = ['sku' => ($product->getSku()),
                'product_type' => ($product_type),
                'position' => ($product->getData('cat_index_position')),
                'id' => ($product_id),
                'name' => ($product->getName()),
                'price' => (intval($product->getPrice())),
                'final_price' => (intval($product->getFinalPrice())),
                'currency' => ($currency),
                'remote_id' => ($product_id),
                'brand' => ($product->getBrand()),
                'vendor_name' => ($vendor_name),
                'code' => ($product->getSku()),
                'main_image' => ($imageUrl),
                'discount_price' => (intval($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice())),
                'discount_price_formatted' => (number_format($product->getSpecialPrice() ? $product->getSpecialPrice() : $product->getPrice(), 2)),
                'rating_summary' => $ratingSummary,
                'reviews_count' => $ratingCount,
            ];
            $location = null;
            if($product_type == 'ticket'){
                $location = $this->eventTicketHelper->getTicketLocation($product_id);
                $locationArr = [];
                if($location){
                    $locationArr[] = $location;
                    $link['location'] = $location['location_detail'];
                }else{
                    $link['location'] = "";
                }
                $ticketType = $this->eventTicketHelper->getTicketType($product_id);
                $sessionData = \Magenest\Ticket\Helper\Event::getTicketInfo($product_id);
                $link['event_start'] = $sessionData['first_session'];
                $link['session_qty'] = $sessionData['number_session'];
                $link['ticket_type'] = $ticketType;
                $ticketPrices = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Ticket\Controller\Product\Price');
                $ticketPrice = $ticketPrices->getTicketPrice($product_id);
                $salePrice = $ticketPrice['sale_price'];
                $oriPrice = isset($ticketPrice['original_value']) ? $ticketPrice['original_value'] : 0;
                if($ticketType == "free"){
                    $link['price'] = 0;
                    $link['final_price'] = 0;
                }else{
                    $link['price'] = $oriPrice;
                    $link['final_price'] = $salePrice;
                }
            }elseif($product_type == 'configurable' && GrouponDataHelper::isDeal($product_id)){
                $link['product_type'] = 'groupon';
                $childIds = GrouponDataHelper::getChildIds($product_id);
                $price = $this->_reviewHelper->getPriceProductGroupon($childIds);
                $link['price'] = $price['vendor_price'];
                $link['final_price'] = $price['vendor_special_price'];
                $locationIds = GrouponDataHelper::getDeal($product_id)->getLocationIds();
                $location = GrouponDataHelper::getGrouponLocation($locationIds);
                $locationIdsArr = explode(',', $locationIds);
                $locationArr = [];
                foreach ($locationIdsArr as $locationId){
                    $arr = $this->_reviewHelper->getGrouponLocationById($locationId);
                    if($arr['location_detail'] != "" || $arr['lat_long'] != ""){
                        $locationArr[] = $arr;
                    }
                }
                if(!empty($locationIdsArr)) $link['locations'] = $locationArr;
                $link['location'] = $location;
            }

            array_push($result['products'], $link);
        }
        return $result;
    }
    public function getCategoriesInfo($categories)
    {
        $rawData = [];
        foreach ($categories->getItems() as $category) {
            $data = $category->getData();
            $rawData[] = $data;
        }
        return $rawData;
    }
}
