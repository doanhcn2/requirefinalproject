<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\VendorApi\Model;

/**
 * Class StoreApi
 * @package Magenest\VendorApi\Model
 */
class StoreApi implements \Magenest\VendorApi\Api\StoreApiInterface
{
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_currencySymbolFactory;

    /**
     * @var \Magenest\VendorApi\Helper\CategoryHelper
     */
    protected $_categoryHelper;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magenest\VendorApi\Helper\StoreHelper
     */
    protected $_storeHelper;
    protected $simpleResultFactory;

    /**
     * StoreApi constructor.
     * @param \Magenest\VendorApi\Helper\StoreHelper $storeHelper
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\StoreHelper $storeHelper,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magenest\VendorApi\Model\SimpleResultFactory $simpleResultFactory
    )
    {
        $this->_storeHelper = $storeHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->simpleResultFactory = $simpleResultFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getStore()
    {
        $simpleResultModel = $this->simpleResultFactory->create();
        $storeDataOld = $simpleResultModel
            ->getCollection()
            ->addFieldToFilter('result_key', 'vendorapi/store')->getFirstItem();
        $simpleId = $storeDataOld->getId();
        if ($storeDataOld->getId() != null && ($simpleId != 22 && $simpleId != 23 )){
            $simpleResultModel->load($storeDataOld->getId());
            $result = json_decode($simpleResultModel->getData('result_value'), true);
        } elseif($simpleId == 22 || $simpleId == 23){
            $simpleResultModel->load($simpleId)->delete();
            $result = $this->_storeHelper->getAllStoreData();
            $resultValue =  json_encode($result);
            $this->simpleResultFactory->create()->setData(['result_key' => 'vendorapi/store', 'result_value' => $resultValue])->save();
        }else{
            $result = $this->_storeHelper->getAllStoreData();
            $resultValue =  json_encode($result);
            $this->simpleResultFactory->create()->setData(['result_key' => 'vendorapi/store', 'result_value' => $resultValue])->save();
        }
        return $this->_objectFactory
            ->addData($result);
    }
}
