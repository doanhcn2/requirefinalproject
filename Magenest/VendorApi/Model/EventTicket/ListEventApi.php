<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\EventTicket;

/**
 * Class ListEventApi
 * @package Magenest\VendorApi\Model
 */
class ListEventApi implements \Magenest\VendorApi\Api\ListEventApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\EventTicketHelper
     */
    protected $_eventHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * ListEventApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_eventHelper = $eventTicketHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getEvent($store, $id)
    {
        try {
            $productInfo = $this->_eventHelper->getEventDetail($store, $id);
            return $this->_objectFactory->addData([
                    'event' => $productInfo,
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getListEvent($store, $size)
    {
        try {
            $productInfo = $this->_eventHelper->getListEventDetail($store, $size);
            return $this->_objectFactory->addData([
                'list_event' => $productInfo,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory->addData(['api_status' => 0]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVenueMap($id)
    {
        try {
            $venueMap = $this->_eventHelper->getVenueMapDetail($id);
            return $this->_objectFactory->addData([
                'venue_map' => $venueMap,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory->addData(['api_status' => 0]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVenueMapData($id, $product_id)
    {
        try {
            $venueMap = $this->_eventHelper->getVenueMapDataDetail($id, $product_id);
            return $this->_objectFactory->addData([
                'venue_map_data' => $venueMap,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory->addData(['api_status' => 0]);
        }
    }
}
