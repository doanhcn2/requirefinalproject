<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\EventTicket;

use Unirgy\Dropship\Model\VendorFactory;
/**
 * Class TicketApi
 * @package Magenest\VendorApi\Model
 */
class TicketApi implements \Magenest\VendorApi\Api\TicketApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\EventTicketHelper
     */
    protected $_eventHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * ListEventApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory
    ) {
        $this->_ticketFactory = $ticketFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_eventHelper = $eventTicketHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getTicket($id, $customer_id)
    {

        try {
            $ticket = $this->_eventHelper->getTicketDetail($id, $customer_id);
            return $this->_objectFactory->addData([
                'ticket' => $ticket,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketByEmail($email, $size)
    {
        try {
            $ticket = $this->_eventHelper->getTicketByEmailDetail($email, $size);
            return $this->_objectFactory->addData([
                'ticket_by_email' => $ticket,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketByProduct($id, $size)
    {
        try {
            $ticket = $this->_eventHelper->getTicketByProductDetail($id, $size);
            return $this->_objectFactory->addData([
                'ticket_by_product' => $ticket,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllTicketByCustomerId($customerId,$cur_page,$size,$status){
        try{
            $ticketInfo = $this->_eventHelper->getTicketByCustomerId($customerId,$cur_page,$size,$status);
            return $this->_objectFactory->addData([
                'all_ticket_by_customer_id' => $ticketInfo,
                'api_status' => 1
            ]);
        }catch (\Exception $e){
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function redeemTickets($customerId, $listTickets)
    {
        try {
            $coupons = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('customer_id', $customerId)->addFieldToFilter('redemption_code', ['in' => $listTickets]);
            $redeemed = 0;
            foreach ($coupons as $coupon)
            {
                /**
                 * @var $coupon \Magenest\Ticket\Model\Ticket
                 */
                $redeem = $coupon->redeemATicket();
                if ($redeem){
                    $redeemed++;
                }
            }
            return $this->_objectFactory->addData([
                'redeem_status' => true,
                'redeem_message' => $redeemed . ' of ' . count($listTickets) . ' has been redeemed.',
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'redeem_message' => $e->getMessage(),
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketDetail($customerId, $ticketId)
    {
        try {
            $ticket = $this->_eventHelper->getTicketDetailsById($customerId, $ticketId);
            return $this->_objectFactory->addData([
                'ticket_data' => [$ticket['ticket_data']],
                'session_data' => [$ticket['session_data']],
                'ticket_location' => $ticket['ticket_location'],
                'ticket_term' => [$ticket['ticket_term']],
                'rating_option' => $ticket['rating_option'],
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    public function customerMarkAvailableTicket($customerId,$listTicketRedeemCode){
        if($listTicketRedeemCode){
            $tickets = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('redemption_code', ['in' => $listTicketRedeemCode]);
            $flag = false;
            foreach ($tickets as $ticket){
                $status = (int)$ticket->getStatus();
                $statusText = $status == 3 ? 'refund':'canceled';
                if($status != \Magenest\Ticket\Model\Ticket::STATUS_REFUNDED && $status != \Magenest\Ticket\Model\Ticket::STATUS_CANCELED ){
                    try{
                        /**
                         * @var $ticket \Magenest\Ticket\Model\Ticket
                         */
                        $ticket->setStatus(\Magenest\Ticket\Model\Ticket::STATUS_AVAILABLE);
                        $ticket->setUpdatedAt(date("Y-m-d H:i:s"));
                        $ticket->save();
                        $flag = true;
                    }catch (\Exception $exception){
                        $this->_debugHelper->debugString($exception->getCode(),$exception->getMessage());
                        $flag = false;
                    }
                }
            }
            return $flag;
        }else{
            $this->_debugHelper->debugString('ticketId','ticketId empty');
            return false;
        }
    }
}
