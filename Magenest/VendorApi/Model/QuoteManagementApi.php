<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 17/01/2018
 * Time: 17:08
 */

namespace Magenest\VendorApi\Model;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Quote\Model\Quote\Address\ToOrderAddress as ToOrderAddressConverter;
use Magento\Quote\Model\Quote as QuoteEntity;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\Payment\ToOrderPayment as ToOrderPaymentConverter;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Sales\Api\OrderManagementInterface as OrderManagement;
use Magento\Store\Model\StoreManagerInterface;
use function Symfony\Component\DependencyInjection\Tests\Fixtures\factoryFunction;

class QuoteManagementApi implements \Magenest\VendorApi\Api\Cart\CartManagementInterface
{
    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * @var QuoteValidator
     */
    protected $quoteValidator;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var OrderManagement
     */
    protected $orderManagement;

    /**
     * @var CustomerManagement
     */
    protected $customerManagement;

    /**
     * @var ToOrderConverter
     */
    protected $quoteAddressToOrder;

    /**
     * @var ToOrderAddressConverter
     */
    protected $quoteAddressToOrderAddress;

    /**
     * @var ToOrderItemConverter
     */
    protected $quoteItemToOrderItem;

    /**
     * @var ToOrderPaymentConverter
     */
    protected $quotePaymentToOrderPayment;

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerModelFactory;

    /**
     * @var \Magento\Quote\Model\Quote\AddressFactory
     */
    protected $quoteAddressFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    private $quoteIdMaskFactory;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var array
     */
    private $addressesToSync = [];

    protected $_objectFactory;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;
    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $session;
    /**
     * QuoteManagementApi constructor.
     * @param EventManager $eventManager
     * @param \Magento\Quote\Model\QuoteValidator $quoteValidator
     * @param OrderFactory $orderFactory
     * @param OrderManagement $orderManagement
     * @param \Magento\Quote\Model\CustomerManagement $customerManagement
     * @param ToOrderConverter $quoteAddressToOrder
     * @param ToOrderAddressConverter $quoteAddressToOrderAddress
     * @param ToOrderItemConverter $quoteItemToOrderItem
     * @param ToOrderPaymentConverter $quotePaymentToOrderPayment
     * @param UserContextInterface $userContext
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerModelFactory
     * @param QuoteEntity\AddressFactory $quoteAddressFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Quote\Model\QuoteIdMaskFactory|null $quoteIdMaskFactory
     * @param \Magento\Customer\Api\AddressRepositoryInterface|null $addressRepository
     */
    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        EventManager $eventManager,
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        OrderFactory $orderFactory,
        OrderManagement $orderManagement,
        \Magento\Quote\Model\CustomerManagement $customerManagement,
        ToOrderConverter $quoteAddressToOrder,
        ToOrderAddressConverter $quoteAddressToOrderAddress,
        ToOrderItemConverter $quoteItemToOrderItem,
        ToOrderPaymentConverter $quotePaymentToOrderPayment,
        UserContextInterface $userContext,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerModelFactory,
        \Magento\Quote\Model\Quote\AddressFactory $quoteAddressFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory = null,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository = null,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory
    ) {
        $this->_objectFactory = $objectFactory;
        $this->eventManager = $eventManager;
        $this->quoteValidator = $quoteValidator;
        $this->orderFactory = $orderFactory;
        $this->orderManagement = $orderManagement;
        $this->customerManagement = $customerManagement;
        $this->quoteAddressToOrder = $quoteAddressToOrder;
        $this->quoteAddressToOrderAddress = $quoteAddressToOrderAddress;
        $this->quoteItemToOrderItem = $quoteItemToOrderItem;
        $this->quotePaymentToOrderPayment = $quotePaymentToOrderPayment;
        $this->userContext = $userContext;
        $this->quoteRepository = $quoteRepository;
        $this->customerRepository = $customerRepository;
        $this->customerModelFactory = $customerModelFactory;
        $this->quoteAddressFactory = $quoteAddressFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->accountManagement = $accountManagement;
        $this->customerSession = $customerSession;
        $this->quoteFactory = $quoteFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory ?: ObjectManager::getInstance()
            ->get(\Magento\Quote\Model\QuoteIdMaskFactory::class);
        $this->addressRepository = $addressRepository ?: ObjectManager::getInstance()
            ->get(\Magento\Customer\Api\AddressRepositoryInterface::class);
        $this->_productRepository = $productRepository;
        $this->tickets = $ticketsFactory;
        $this->session = $sessionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getCartForCustomer($customerId)
    {
        $result = $this->quoteRepository->getActiveForCustomer($customerId);
        $items = $result->getItems();
        $address = $result->getShippingAddress();
        $shipping_amount = 0;
        if($address){
            $shipping_amount = $address->getShippingAmount();
        }
        $discountAmount = 0;
        foreach ($items as $key => $item){
            $discountAmount += $item->getDiscountAmount();
            $product = $item->getProduct();
            $options = $product->getTypeInstance(true)->getOrderOptions($product);
            $vendor_id = $item->getUdropshipVendor();
            if(isset($vendor_id)){
                $_v = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data')->getVendor($vendor_id);
                /** @var \Unirgy\Dropship\Model\Vendor\Product $_vpm */
                $_vpm = \Magento\Framework\App\ObjectManager::getInstance()->create(\Unirgy\Dropship\Model\Vendor\Product::class);
                $_vpc = $_vpm->getCollection()
                    ->addFieldToSelect('*')->addFieldToFilter('product_id', $product->getId())
                    ->addFieldToFilter('vendor_id', ['eq'=> $vendor_id])
                    ->getFirstItem();
                $seller_note = $_vpc->getStateDescr();
                $vendor_name = $_v->getVendorName();
            }
            $small_image = $product->getSmallImage() ? $product->getSmallImage() : '';
            $product->addData([
                'image'   =>  $small_image
            ]);
            $price = $item->getPrice();
            $additionalOptions = [];
            if(isset($options['info_buyRequest']['additional_options'])){
                $data = $options['info_buyRequest'];
                if(isset($data['additional_options']['is_gifted'])||isset($data['additional_options']['Type'])){
                    if (isset($data['additional_options']['To'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Name',
                            'value' => $data['additional_options']['To'],
                        ];
                    }
                    if (isset($data['additional_options']['Email'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Email',
                            'value' => $data['additional_options']['Email'],
                        ];
                    }
                    if (isset($data['additional_options']['Message'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Message',
                            'value' => $data['additional_options']['Message'],
                        ];
                    }
                }
                if (isset($data['additional_options']['coupon_selected_children'])) {
                    $couponChildren = $this->_productRepository->getById((int)$data['additional_options']['coupon_selected_children']);
                    $additionalOptions[] = [
                        'label' => 'Deal Option',
                        'value' => $couponChildren->getName(),
                    ];
                    if($couponChildren->getSpecialPrice()&&$couponChildren->getSpecialPrice()!=""){
                        $price = $couponChildren->getSpecialPrice();
                    }else{
                        $price = $couponChildren->getPrice();
                    }

                }
                if(isset($data['additional_options']['ticket']) && (!empty($data['additional_options']['ticket']))){
                    $ticketModel = $this->tickets->create()->load($data['additional_options']['ticket']);
                    $additionalOptions[] = array(
                        'label' => 'Option',
                        'value' => $ticketModel->getTitle()
                    );
                }
                if (isset($data['additional_options']['ticket_session']) && (!empty($data['additional_options']['ticket_session']))) {
                    $ticketModel = $this->session->create()->load($data['additional_options']['ticket_session']);
                    $additionalOptions[] = array(
                        'label' => 'Session',
                        'value' => $this->convertTime($ticketModel->getSessionFrom()) . ' - ' . $this->convertTime($ticketModel->getSessionTo())
                    );
                }


            }
            if($price){
                $item->addData([
                    'price' => $price,
                    'item_options'   => isset($options['attributes_info']) ? $options['attributes_info'] : [],
                    'custom_options' => isset($options['options']) ? $options['options'] : [],
                    'additional_options' => $additionalOptions,
                    'vendor_id'   => isset($vendor_id) ? $vendor_id : '',
                    'vendor_name'   => isset($vendor_name) ? $vendor_name : 'Unknown Vendor',
                    'seller_note'   => isset($seller_note) ? $seller_note : '',
                    'shipping' => $item->getShipping(),
                    'tax' => $item->getTaxAmount(),
                    'discount_amount' => $item->getDiscountAmount(),
                ]);
            }else{
                $item->addData([
                    'item_options'   => isset($options['attributes_info']) ? $options['attributes_info'] : [],
                    'custom_options' => isset($options['options']) ? $options['options'] : [],
                    'additional_options' => $additionalOptions,
                    'vendor_id'   => isset($vendor_id) ? $vendor_id : '',
                    'vendor_name'   => isset($vendor_name) ? $vendor_name : 'Unknown Vendor',
                    'seller_note'   => isset($seller_note) ? $seller_note : '',
                    'shipping' => $item->getShipping(),
                    'tax' => $item->getTaxAmount(),
                    'discount_amount' => $item->getDiscountAmount(),
                ]);
            }

        }
        $result->addData([
            'discount_total' => $discountAmount,
            'shipping_amount' => $shipping_amount
        ]);
        return $result;
    }
    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);

        return date('d-m-Y H:i:s', $strTime);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($cartId, $itemId)
    {
        $result = false;
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        $quoteItem = $quote->getItemById($itemId);
        if ($quoteItem) {
            try {
                $quote->removeItem($itemId);
                $this->quoteRepository->save($quote);
                $result = true;
            } catch (\Exception $e) {
                $result = false;
            }
        }
        return $this->_objectFactory
            ->addData([
                'result' => boolval($result)
            ]);
    }
}
