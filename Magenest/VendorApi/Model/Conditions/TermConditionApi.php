<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Conditions;

use Magenest\VendorApi\Api\TermLicenseApiInterface;
use Magenest\VendorApi\Helper\ConditionHelper;
use Magenest\VendorApi\Helper\DebugHelper;
use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class OrderInformationInterface
 * @package Magenest\VendorApi\Model\Groupon
 */
class TermConditionApi implements TermLicenseApiInterface
{
    /**
     * @var ConditionHelper
     */
    protected $_conditionHelper;

    /**
     * @var DataObject
     */
    protected $_objectFactory;

    /**
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * GrouponApi constructor.
     * @param DebugHelper $debugHelper
     * @param DataObject $objectFactory
     * @param ConditionHelper $conditionHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        DebugHelper $debugHelper,
        DataObject $objectFactory,
        ConditionHelper $conditionHelper,
        StoreManagerInterface $storeManager
    )
    {
        $this->_conditionHelper = $conditionHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllTermCondition()
    {
        // TODO: Implement getAllTermCondition() method.
        try {
            $conditions = $this->_conditionHelper->getAllTermAndConditions();
            return $this->_objectFactory->addData([
                'groupon_term' => $conditions['groupon'],
                'checkout_term' => $conditions['checkout'],
                'paypal_term' => $conditions['paypal'],
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllLicense()
    {
        // TODO: Implement getAllLicense() method.
        try {
            $licenses = $this->_conditionHelper->getAllLicenses();
            return $this->_objectFactory->addData([
                'ves_license' => $licenses['ves'],
                'unirgy_license' => $licenses['unirgy'],
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
