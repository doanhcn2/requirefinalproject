<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model;

use Magenest\VendorApi\Api\VendorApiInterface;
use Magenest\VendorApi\Helper\DebugHelper;
use Magento\Framework\DataObject;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Review\SummaryFactory;
use Magento\Review\Model\ReviewFactory;
use Unirgy\Dropship\Model\VendorFactory;
use Unirgy\DropshipVendorRatings\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ProductApi
 * @package Magenest\VendorApi\Model
 */
class VendorApi implements VendorApiInterface
{
    /**
     * @var DataObject $_objectFactory
     */
    protected $_objectFactory;

    /**
     * @var DebugHelper $_debugHelper
     */
    protected $_debugHelper;

    /**
     * @var $_vendorFactory VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_ratingFactory RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var $_reviewFactory ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var $_ratingHelper Data
     */
    protected $_ratingHelper;

    /**
     * @var $_reviewSummaryFactory SummaryFactory
     */
    protected $_reviewSummaryFactory;

    /**
     * @var $_storeManagement StoreManagerInterface
     */
    protected $_storeManagement;

    protected $_vendorApiProductHelper;

    /**
     * ProductApi constructor.
     * @param DebugHelper $debugHelper
     * @param DataObject $objectFactory
     * @param RatingFactory $ratingFactory
     * @param VendorFactory $vendorFactory
     * @param ReviewFactory $reviewFactory
     * @param Data $helperData
     * @param SummaryFactory $summaryModFactory
     * @param StoreManagerInterface $manager
     */
    public function __construct(
        DebugHelper $debugHelper,
        DataObject $objectFactory,
        RatingFactory $ratingFactory,
        VendorFactory $vendorFactory,
        ReviewFactory $reviewFactory,
        Data $helperData,
        SummaryFactory $summaryModFactory,
        StoreManagerInterface $manager,
        \Magenest\VendorApi\Helper\ProductHelper $vendorApiProductHelper
    ) {
        $this->_storeManagement = $manager;
        $this->_reviewSummaryFactory = $summaryModFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_ratingHelper = $helperData;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_vendorApiProductHelper = $vendorApiProductHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendor($id)
    {
        try {
            $vendor = $this->_vendorFactory->create()->load($id);
            $vendorName = $vendor->getVendorName();
            $vendorDescription = $vendor->getBusinessDes();

//            $ratingOptions = $this->_ratingFactory->create()->getCollection()->getColumnValues('rating_code');

            $ratingDetails = [];
            $vendorRating = $this->_vendorApiProductHelper->getVendorReviewsCollection($vendor->getId());
            $ratingOptionsVote = [];
            $reviewVendorIds = [];
            foreach ($vendorRating as $rating) {
                $ratingData = $rating->getData();
                $ratingVotes = $rating->getRatingVotes();
                $ratingVote = [];
                $reviewVendorIds[] = $rating->getReviewId();
                foreach ($ratingVotes as $vote) {
                    $voteItem = [
                        'rating_code' => $vote->getRatingCode(),
                        'value' => $vote->getValue(),
                        'rating_summary' => $vote->getPercent(),
                        'store_id' => $vote->getStoreId(),
                        'is_aggregate' => $vote->getIsAggregate()
                    ];

                    array_push($ratingVote, $voteItem);
                }
                $ratingOptionsVote[] = $ratingVote;

                unset($ratingData['rating_votes']);
                unset($ratingData['shipping_address']);
                $data = $ratingData;
                $data['rating_options'] = $ratingVote;
                array_push($ratingDetails, $data);
            }
            $ratingGeneral = $this->countingRatingOptions($reviewVendorIds, $ratingOptionsVote);
            $vendor_street = $vendor->getStreet();
            if($this->recursive_array_search($vendor_street)){
                $street = null;
            }
            $dataResults = [
                'vendor_name' => $vendorName,
                'vendor_description' => $vendorDescription,
                'vendor_email' => $vendor->getEmail(),
                'vendor_phone' => $vendor->getPhone(),
                'vendor_street' => $street,
                'vendor_zip' => $vendor->getZip(),
                'vendor_country' => $vendor->getCountryId(),
                'rating_general' => $ratingGeneral,
                'rating_details' => empty($ratingDetails)?null:$ratingDetails,
                'api_status' => 1
            ];
            return $this->_objectFactory
                ->addData($dataResults);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('error', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    private function countingRatingOptions($reviewVendorIds, $voteData)
    {
        $summing = [];
        $naming = [];
        $allCount = 0;
        foreach ($voteData as $voteDatum) {
            foreach ($voteDatum as $vote) {
                if ($vote['is_aggregate'] === '0') continue;
                if (!isset($summing[$vote['rating_code']])) {
                    $summing[$vote['rating_code']] = intval($vote['value']);
                    $naming[$vote['rating_code']] = 1;
                    $allCount++;
                } else {
                    $summing[$vote['rating_code']] += intval($vote['value']);
                    $naming[$vote['rating_code']]++;
                    $allCount++;
                }
            }
        }
        $voteCount = count($voteData);
        $countTotal = 0;
        $optionAverage = [];

        foreach ($summing as $key => $item) {
            $countTotal += $item;
            $option = [];
            if (intval($naming[$key]) !== 0) {
                $count = intval($naming[$key]);
                $option['label'] = $key;
                $option['value'] = number_format($item / intval($count) , 2);
                $optionAverage[] = $option;
            }
        }
        $rating = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\View\Rating');
        $avgRate = $rating->getAvgRate($reviewVendorIds);
        $avgRate = intval($avgRate*20);
        $result = [];
        $result['vote_average'] = $avgRate;
//        if (intval($allCount) !== 0) {
//            $result['vote_average'] = number_format( intval($countTotal) / intval($allCount), 2);
//        }
        $result['vote_count_total'] = $allCount;
        if(empty($optionAverage)){
            $result['rating_option_average'] = null;
        }else{
            $result['rating_option_average'] = $optionAverage;
        }
        return $this->_objectFactory
            ->addData($result);
    }
    function recursive_array_search($array) {
        $flag = false;
        foreach($array as $key=>$value) {
            if($value != ''){
                $flag = true;
                break;
            }
        }
        return $flag;
    }
}
