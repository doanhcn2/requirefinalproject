<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/13/18
 * Time: 3:24 PM
 */

namespace Magenest\VendorApi\Model\Hotel;


class HotelApi implements \Magenest\VendorApi\Api\HotelApiInterface
{
    protected $_objectFactory;
    protected $_hotelOrderCollectionFactory;

    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\Hotel\Model\ResourceModel\HotelOrder\CollectionFactory $hotelOrderCollectionFactory
    ) {
        $this->_objectFactory = $objectFactory;
        $this->_hotelOrderCollectionFactory = $hotelOrderCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getHotelBooking($customerId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\HotelOrder\Collection $hotelOrderCollection */
        $hotelOrderCollection = $this->_hotelOrderCollectionFactory->create();
        return $hotelOrderCollection->loadByCustomerId($customerId)->getItems();
//        return $this->_objectFactory->addData([
//            'test' => 'aah',
//            'baaa' => 'fef'
//        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getHotelProduct($store, $id)
    {
        // TODO: Implement getHotelProduct() method.
    }
}