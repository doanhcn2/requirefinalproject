<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/10/2017
 * Time: 19:49
 */

namespace Magenest\VendorApi\Model\Filter;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magenest\VendorApi\Api\Filter\FilterManagementInterface;
use Magenest\VendorApi\Model\Payload;
use Unirgy\SimpleLicense\Exception;

class FilterManagement  implements FilterManagementInterface
{
    protected $_storeManager;
    protected $_catalogSession;
    protected $_coreRegistry;
    protected $layerResolver;
    protected $categoryRepository;
    protected $_catalogConfig;
    protected $filterList;
    protected $_catalogLayer;
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Framework\Registry $coreRegistry,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\Config $catalogConfig,

        array $data = []
    ){
        $this->_storeManager = $storeManager;
        $this->_catalogSession = $catalogSession;
        $this->_coreRegistry = $coreRegistry;
        $this->layerResolver = $layerResolver;
        $this->categoryRepository = $categoryRepository;

        $this->_catalogConfig = $catalogConfig;

    }

    /**
     * {@inheritdoc}
     */
    public function getFilters($categoryId)
    {
        $outputItems = [];
        try{
            $category = $this->categoryRepository->get($categoryId, $this->_storeManager->getStore()->getId());
        }catch (\Magento\Framework\Exception\LocalizedException $e){

        }

        $this->_catalogLayer = $this->layerResolver->get();

        try{
            if (isset($category)) {
                $this->_catalogLayer->setCurrentCategory($category);
            }
        }catch (\Magento\Framework\Exception\LocalizedException $e){

        }

        $finalFilters = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $fill = $objectManager->create('Magento\Catalog\Model\Layer\Category\FilterableAttributeList');
        $this->filterList = new \Magento\Catalog\Model\Layer\FilterList($objectManager,$fill);
        /** @var \Magento\Catalog\Model\Layer\Filter\AbstractFilter $filter */
        $cnt = 0;
        $filterData = [];
        foreach ($this->filterList->getFilters($this->_catalogLayer) as $filter) {
            //todo
            //$filter->apply($this->getRequest());
            if ($filter->getItemsCount()) {
                $finalFilters[$cnt] = $filter;

                $payload = new Payload('filter');
                $filterName = '';
                try{
                    $filterName = $filter->getName();
                }catch (\Magento\Framework\Exception\LocalizedException $e){

                }

                $payload->setLabel($filterName);

                //list($catId,$filterId,$filterName)
                $payload->setValue($categoryId."_".$cnt."_". $filterName . " attribute");

                $outputItems[] = $payload;
                $cnt++;

                $itemCount = $filter->getItemsCount();
                $itemsData = [];
                if ($itemCount)  {
                    $items = $filter->getItems();
                    foreach ($items as $item) {
                        $text = $item->getLabel();
                        if(is_object($item->getLabel())){
                            $text = $item->getLabel()->getText();
                            $arguments = $item->getLabel()->getArguments();
                            foreach ($arguments as $key => $argument){
                                $text = str_replace('%'.($key+1), strip_tags($argument), $text);
                            }
                        }
                        $itemsData[] = [
                            'label' => $text,
                            'value' => $item->getValue(),
                            'count' => $item->getCount()
                        ];
                    }
                }
                $filterData [] = [
                    'label' => $filterName,
                    'value' => $itemsData,
                    'name' => $filter->getRequestVar()
                ];
            }
        }
        $this->_catalogLayer->apply();
        return $filterData;
    }
}