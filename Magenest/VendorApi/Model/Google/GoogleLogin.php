<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:24
 */
namespace Magenest\VendorApi\Model\Google;

use Magento\Checkout\Model\CartFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Integration\Model\Oauth\Token as Token;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Framework\Exception\AuthenticationException;
use Magenest\VendorApi\Plugin\AccessChangeQuoteControl\Disable;

class GoogleLogin implements \Magenest\VendorApi\Api\Google\GoogleLoginInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface|null
     */
    protected $_objectManager = null;
    protected $helper;
    protected $customerFactory;
    protected $tokenFactory;
    protected $tokenModelFactory;
    protected $tokenModelCollectionFactory;
    protected $_objectFactory;
    protected $cartRepository;
    protected $rewardPointManagement;
    protected $_balanceFactory;
    protected $_rewardFactory;
    protected $_storeManager;
    protected $registry;
    public function __construct(
        \Magenest\SocialLogin\Helper\SocialLogin $helper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenFactory,
        \Magento\Framework\DataObject $objectFactory,
        TokenModelFactory $tokenModelFactory,
        TokenCollectionFactory $tokenModelCollectionFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magenest\VendorApi\Api\RewardPoint\RewardPointManagementInterface $rewardPointManagement,
        \Magento\CustomerBalance\Model\BalanceFactory $balanceFactory,
        \Magento\Reward\Model\RewardFactory $rewardFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Registry $registry
    )
    {
        $this->helper = $helper;
        $this->customerFactory = $customerFactory;
        $this->tokenFactory = $tokenFactory;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->tokenModelCollectionFactory = $tokenModelCollectionFactory;
        $this->_objectFactory = $objectFactory;
        $this->cartRepository = $cartRepository;
        $this->rewardPointManagement = $rewardPointManagement;
        $this->_balanceFactory = $balanceFactory;
        $this->_rewardFactory = $rewardFactory;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
        $this->registry = $registry;
    }

    /**
     * @param string[] $userInfo
     * @return mixed
     */
    public function loginOrCreateNewCustomer($userInfo)
    {
        /** @var \Magento\Integration\Model\Oauth\Token $customerToken */
      //  $customerToken = $this->tokenFactory->create();
        if (is_array($userInfo)) {
            $username = $userInfo['name'];
            $userEmail = $userInfo['email'];
            $userFacebookId = $userInfo['id'];

            $customer = $this->helper->getCustomerByEmail($userEmail);

            if ($customer->getId()) {

            } else {

                $data = [
                    'sendemail' => 0,
                    'confirmation' => 0,
                    'magenest_sociallogin_id' => $userInfo['id'],
                    'magenest_sociallogin_type' => 'google'
                ];

                $nameExploded = explode(' ', $username);

                $data['email'] = $userEmail;
                $data['firstname'] = $nameExploded[0];
                $data['lastname'] = $nameExploded[1];

                $newCustomer = $this->customerFactory->create();
                $newCustomer->setData($data);
                $newCustomer->save();


                $customer = $newCustomer;

            }
            $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
            //$cart=$this->cartFactory->create()->load(27595);

            try {
                $myCart=$this->cartRepository->getForCustomer($customer->getId());
                $itemQty = $myCart->getItemsQty();
                $cartId=$myCart->getId();
            }catch (\Exception $e) {
                $this->registry->register(Disable::DISABLE_REGISTRY, true);
                $cartManagement = $this->_objectManager->create(\Magento\Quote\Api\CartManagementInterface::class);
                $this->registry->unregister(Disable::DISABLE_REGISTRY);
                $myCart = $cartManagement->createEmptyCartForCustomer($customer->getId());
                $itemQty = 0;
                $cartId=$myCart->getId();
            }
            /** $rewardPoint=$this->rewardPointManagement->getRewardPointForCustomer($customer->getId())->getResult();
            $balance=0;
            foreach($rewardPoint as $r) {
                $balance+=$r['balance'];
            }**/

            $customer = $this->helper->getCustomerByEmail($customer->getEmail());
            $this->_rewardInstance = $this->_rewardFactory->create()->setCustomer(
                $customer
            )->setWebsiteId(
                $this->_storeManager->getWebsite()->getId()
            )->loadByCustomer();
            $balance = $this->_rewardInstance->getPointsBalance();
            $collection = $this->_balanceFactory->create()->getCollection()->addFieldToFilter(
                'customer_id',
                $customer->getId()
            );

            $storeCreditTotal=$collection->getFirstItem();

            return $this->_objectFactory
                ->addData([
                    'access_token' => $token,
                    'cart_item_qty'=> $itemQty,
                    'reward_point_balance'=>$balance,
                    'store_credit_total'=> $storeCreditTotal->getAmount(),
                    'customer_info'=>$customer->getDataModel(),
                    'cart_id'=>$cartId
                ]);
        } else {
            return false;
        }
    }
}