<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:24
 */
namespace Magenest\VendorApi\Model\Social;

use Magenest\VendorApi\Api\Social\SocialLoginInterface;
use Magento\CustomerBalance\Model\BalanceFactory;

class SocialLogin implements SocialLoginInterface
{
    protected $helper;

    protected $customerFactory;

    protected $tokenFactory;

    /**
     * @var $_customerBalanceFactory BalanceFactory
     */
    protected $_customerBalanceFactory;

    /**
     * Reward history collection
     *
     * @var \Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory
     */
    protected $_historyCollectionFactory;

    /**
     * SocialLogin constructor.
     * @param \Magenest\SocialLogin\Helper\SocialLogin $helper
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Integration\Model\Oauth\TokenFactory $tokenFactory
     * @param BalanceFactory $balanceFactory
     * @param \Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magenest\SocialLogin\Helper\SocialLogin $helper,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenFactory,
        BalanceFactory $balanceFactory,
        \Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory $collectionFactory
    ) {
        $this->_customerBalanceFactory = $balanceFactory;
        $this->_historyCollectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->customerFactory = $customerFactory;
        $this->tokenFactory = $tokenFactory;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function loginOrCreateNewCustomerViaFacebook($userInfo)
    {
        /** @var \Magento\Integration\Model\Oauth\Token $customerToken */
        $customerToken = $this->tokenFactory->create();
        if (is_array($userInfo)) {
            $username = $userInfo['name'];
            $userEmail = $userInfo['email'];

            $customer = $this->helper->getCustomerByEmail($userEmail);

            if ($customer->getId()) {
                $tokenKey = $customerToken->createCustomerToken($customer->getId())->getToken();
                $customerData = $customer->getData();
                $customerData['access_token'] = $tokenKey;
                $collection =
                    $this->_historyCollectionFactory->create()
                        ->addFieldToSelect('*')
                        ->addCustomerFilter($customer->getId())
                        ->skipExpiredDuplicates()
                        ->setDefaultOrder();
                $pointBalance = 0;
                $lastChangeTime = 0;
                foreach ($collection as $item) {
                    if (strtotime($item->getCreatedAt()) > $lastChangeTime) {
                        $pointBalance = $item->getPointsBalance();
                        $lastChangeTime = strtotime($item->getCreatedAt());
                    }
                }
                $customerData['reward_point_balance'] = $pointBalance;

                $balance = $this->_customerBalanceFactory->create()->getCollection()->addFieldToFilter('customer_id', $customer->getId())->getFirstItem()->getAmount();
                $customerData['store_credit_balance'] = @$balance ? $balance : 0;

                return [$customerData];
            } else {

                $data = [
                    'sendemail' => 0,
                    'confirmation' => 0,
                    'magenest_sociallogin_id' => @$userInfo['id'],
                    'magenest_sociallogin_type' => 'facebook'
                ];

                $nameExploded = explode(' ', $username);

                $data['email'] = $userEmail;
                $data['firstname'] = $nameExploded[0];
                $data['lastname'] = $nameExploded[1];
                $data['reward_point_balance'] = 0;
                $data['store_credit_balance'] = 0;

                $newCustomer = $this->customerFactory->create();
                $newCustomer->setData($data);
                $newCustomer->save();

                $tokenKey = $customerToken->createCustomerToken($newCustomer->getId())->getToken();

                $customerData = $newCustomer->getData();
                $customerData['access_token'] = $tokenKey;

                return [$customerData];
            }
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function loginOrCreateNewCustomerViaGoogle($userInfo)
    {
        /** @var \Magento\Integration\Model\Oauth\Token $customerToken */
        $customerToken = $this->tokenFactory->create();
        if (is_array($userInfo)) {
            $username = $userInfo['name'];
            $userEmail = $userInfo['email'];

            $customer = $this->helper->getCustomerByEmail($userEmail);

            if ($customer->getId()) {
                $tokenKey = $customerToken->createCustomerToken($customer->getId())->getToken();
                $customerData = $customer->getData();
                $customerData['access_token'] = $tokenKey;
                $collection =
                    $this->_historyCollectionFactory->create()
                        ->addFieldToSelect('*')
                        ->addCustomerFilter($customer->getId())
                        ->skipExpiredDuplicates()
                        ->setDefaultOrder();
                $pointBalance = 0;
                $lastChangeTime = 0;
                foreach ($collection as $item) {
                    if (strtotime($item->getCreatedAt()) > $lastChangeTime) {
                        $pointBalance = $item->getPointsBalance();
                        $lastChangeTime = strtotime($item->getCreatedAt());
                    }
                }
                $customerData['reward_point_balance'] = $pointBalance;

                $balance = $this->_customerBalanceFactory->create()->getCollection()->addFieldToFilter('customer_id', $customer->getId())->getFirstItem()->getAmount();
                $customerData['store_credit_balance'] = @$balance ? $balance : 0;

                return [$customerData];
            } else {

                $data = [
                    'sendemail' => 0,
                    'confirmation' => 0,
                    'magenest_sociallogin_id' => @$userInfo['id'],
                    'magenest_sociallogin_type' => 'google'
                ];

                $nameExploded = explode(' ', $username);

                $data['email'] = $userEmail;
                $data['firstname'] = $nameExploded[0];
                $data['lastname'] = $nameExploded[1];
                $data['reward_point_balance'] = 0;
                $data['store_credit_balance'] = 0;

                $newCustomer = $this->customerFactory->create();
                $newCustomer->setData($data);
                $newCustomer->save();

                $tokenKey = $customerToken->createCustomerToken($newCustomer->getId())->getToken();

                $customerData = $newCustomer->getData();
                $customerData['access_token'] = $tokenKey;

                return [$customerData];
            }
        } else {
            return false;
        }
    }
}