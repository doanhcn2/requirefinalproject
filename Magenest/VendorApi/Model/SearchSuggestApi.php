<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model;

use Magenest\VendorApi\Api\SearchSuggestApiInterface;
use \MageWorx\SearchSuiteAutocomplete\Helper\Data as HelperData;
use Magento\Framework\ObjectManagerInterface as ObjectManager;
use Magento\Search\Model\Query as QueryModel;

class SearchSuggestApi implements SearchSuggestApiInterface{
    /**
     * @var \Magento\Search\Model\AutocompleteInterface
     */
    protected $_autocomplete;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_dataObject;
    protected $_searchModel;
    protected $queryFactory;
    protected $storeManager;
    protected $request;
    protected $helperData;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager = null;
    protected $queryModel;
    protected $queryCollectionFactory;
    public function __construct(
        \Magento\Search\Model\AutocompleteInterface $autocomplete,
        \MageWorx\SearchSuiteAutocomplete\Model\Search $searchModel,
        \Magento\Search\Model\QueryFactory $queryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\DataObject $dataObject,
        \Magento\Search\Model\ResourceModel\Query $queryModel,
        \Magento\Search\Model\ResourceModel\Query\CollectionFactory $queryCollectionFactory,
        HelperData $helperData,
        ObjectManager $objectManager
    ) {
        $this->_autocomplete = $autocomplete;
        $this->_dataObject = $dataObject;
        $this->_searchModel = $searchModel;
        $this->queryFactory = $queryFactory;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->helperData = $helperData;
        $this->objectManager = $objectManager;
        $this->queryModel = $queryModel;
        $this->queryCollectionFactory = $queryCollectionFactory;
    }
    public function getSearchSuggest($query_text){
        $result = [];
        $this->request->setPostValue('q',$query_text);
        $query = $this->queryFactory->get();
        $query->setStoreId($this->storeManager->getStore()->getId());
        if ($query->getQueryText() != '') {
            $query->setId(0)->setIsActive(1)->setIsProcessed(1);
            $result = $this->getData();
        }
        return $result;
        //get the fields show in popup
    }
    public function getData(){
        $data = [];
        $autocompleteFields = $this->helperData->getAutocompleteFieldsAsArray();
        foreach ($autocompleteFields as $field) {
            if($field == 'category'){
                $instance = $this->objectManager->create('MageWorx\SearchSuiteAutocomplete\Model\Search\Category');
            }elseif($field == 'product'){
                $instance = $this->objectManager->create('\Magenest\VendorApi\Model\Search\Product');
            }else{
                $instance = $this->objectManager->create('\MageWorx\SearchSuiteAutocomplete\Model\Search\Suggested');
            }
            $data[] = $instance->getResponseData();
        }
        return $data;
    }

    public function saveSearchSuggest($query_text){
        $this->request->setPostValue('q',$query_text);
        $query = $this->queryFactory->get();
        $query->setStoreId($this->storeManager->getStore()->getId());
        if ($query->getQueryText() != '') {
            $query->setId(0)->setIsActive(1)->setIsProcessed(1);
            $this->saveNumberResults($query);
            return true;
        }
        return false;
    }
    public function saveNumberResults($query){
        $adapter = $this->queryCollectionFactory->create()->getConnection();
        $table = $this->queryCollectionFactory->create()->getMainTable();
        $numResults = (int)$query->getNumResults();
        $saveData = [
            'store_id' => $query->getStoreId(),
            'query_text' => $query->getQueryText(),
            'num_results' => $numResults,
        ];
        $updateData = ['num_results' => $numResults];
        $adapter->insertOnDuplicate($table, $saveData, $updateData);
    }

    public function getSearchSuggestion($query_text){
        $this->request->setPostValue('q',$query_text);
        $autocompleteData = [];
        $items = $this->_autocomplete->getItems();
        if (sizeof($items) > 0) {
            $autocompleteNData = [];
            $autocompleteAData = [];
            foreach ($items as $item) {
                $title = $item->getTitle();
                if (ctype_alpha($title[0])) {
                    $autocompleteAData[] = [
                        'title' => $title,
                        'num_results' => $item->getNumResults()
                    ];
                } else {
                    $autocompleteNData[] = [
                        'title' => $title,
                        'num_results' => $item->getNumResults()
                    ];
                }
            }
            usort($autocompleteAData, function ($a, $b) {
                return strcmp($a['title'], $b['title']);
            });
            usort($autocompleteNData, function ($a, $b) {
                return strcmp($a['title'], $b['title']);
            });
            foreach ($autocompleteAData as $item) {
                $autocompleteData[] = $item;
            }
            foreach ($autocompleteNData as $item) {
                $autocompleteData[] = $item;
            }
            $result = [
                'search' => $autocompleteData
            ];
        }
        return $autocompleteData;
    }
}