<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\Vendor\Ticket;


/**
 * Class TicketApi
 * @package Magenest\VendorApi\Model
 */
class VendorTicketApi implements \Magenest\VendorApi\Api\Vendor\Ticket\VendorTicketApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\EventTicketHelper
     */
    protected $_eventHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * ListEventApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory
    ) {
        $this->_ticketFactory = $ticketFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_eventHelper = $eventTicketHelper;
    }



    /**
     * {@inheritdoc}
     */
    public function redeemTickets($vendorId, $listTickets)
    {
        try {
            $coupons = $this->_ticketFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('redemption_code', ['in' => $listTickets]);
            $redeemed = 0;
            foreach ($coupons as $coupon)
            {
                /**
                 * @var $coupon \Magenest\Ticket\Model\Ticket
                 */
                $redeem = $coupon->redeemATicket();
                if ($redeem){
                    $redeemed++;
                }
            }
            return $this->_objectFactory->addData([
                'redeem_status' => true,
                'redeem_message' => $redeemed . ' of ' . count($listTickets) . ' has been redeemed.',
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'redeem_message' => $e->getMessage(),
                    'api_status' => 0
                ]);
        }
    }
    /**
     * {@inheritdoc}
     */
    public function checkTicket($vendorId, $ticketCode)
    {
        try {
            $ticketData = $this->_eventHelper->getVendorCheckTicketByCode($vendorId, $ticketCode);
            return $this->_objectFactory->addData([
                'status' => $ticketData['status'],
                'groupon_data' => [$ticketData['ticket_data']],
                'api_status' => 1
            ]);

        } catch (\Exception $e) {
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

}
