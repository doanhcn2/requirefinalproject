<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Vendor;

use Magenest\VendorApi\Api\Vendor\VendorApiInterface;
use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Helper\Data as DataHelper;
use Unirgy\Rma\Model\RmaFactory;
use Unirgy\Dropship\Helper\Data as ForgotPasswordHelper;
use Magenest\VendorApi\Helper\DebugHelper;

class VendorRepository implements VendorApiInterface
{
    protected $_vendorFactory;
    protected $_ticketHelper;
    protected $_hotelHelper;
    protected $_urlBuilder;
    protected $dealActive;
    protected $_magenestVendorHelper;
    protected $_magenestQuestionHelper;
    protected $_magenestEventTicketHelper;
    protected $_magenestInventoryHelper;
    protected $_vendorApiProductHelper;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /** @var \Magento\Framework\DataObject */
    protected $_objectFactory;

    /** @var RmaFactory */
    protected $rmaFactory;

    protected $_forgotPasswordHelper;
    protected $statement;
    protected $payout;
    protected $_scopeConfig;
    /**
     * @var \Magento\Framework\ObjectManagerInterface|null
     */
    protected $_objectManager = null;
    protected $_debugHelper;
    /**
     * Constructor
     *
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Magenest\Ticket\Helper\Information $ticketHelper
     * @param \Magenest\Hotel\Helper\Dashboard $hotelHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\VendorComponent\QuestionHelper $magenestQuestionHelper
     * @param \Magenest\VendorApi\Helper\VendorComponent\VendorHepler $magenestVendorHelper
     * @param \Magenest\VendorApi\Helper\VendorComponent\EventTicketHelper $magenestEventTicketHelper
     * @param \Magenest\VendorApi\Helper\VendorComponent\InventoryHelper $magenestInventoryHelper
     * @param RmaFactory $rmaFactory
     * @param ForgotPasswordHelper $forgotPasswordHelper
     */
    public function __construct(
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Ticket\Helper\Information $ticketHelper,
        \Magenest\Hotel\Helper\Dashboard $hotelHelper,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\VendorComponent\QuestionHelper $magenestQuestionHelper,
        \Magenest\VendorApi\Helper\VendorComponent\VendorHepler $magenestVendorHelper,
        \Magenest\VendorApi\Helper\VendorComponent\EventTicketHelper $magenestEventTicketHelper,
        \Magenest\VendorApi\Helper\VendorComponent\InventoryHelper $magenestInventoryHelper,
        RmaFactory $rmaFactory,
        ForgotPasswordHelper $forgotPasswordHelper,
        \Magenest\VendorApi\Helper\ProductHelper $vendorApiProductHelper,
        \Unirgy\Dropship\Model\Vendor\StatementFactory $statement,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper
    )
    {
        $this->_vendorFactory = $vendorFactory;
        $this->_ticketHelper = $ticketHelper;
        $this->_hotelHelper = $hotelHelper;
        $this->_magenestVendorHelper = $magenestVendorHelper;
        $this->_urlBuilder = $urlBuilder;
        $this->_objectFactory = $objectFactory;
        $this->_magenestQuestionHelper = $magenestQuestionHelper;
        $this->_magenestEventTicketHelper = $magenestEventTicketHelper;
        $this->_magenestInventoryHelper = $magenestInventoryHelper;
        $this->rmaFactory = $rmaFactory;
        $this->_forgotPasswordHelper = $forgotPasswordHelper;
        $this->_vendorApiProductHelper = $vendorApiProductHelper;
        $this->statement = $statement;
        $this->payout = $payoutFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
        $this->_debugHelper = $debugHelper;
    }

    public function getVendorGrouponDashboard($vendorId)
    {
        $productType = $this->_magenestVendorHelper->getVendor($vendorId)->getPromoteProduct();

        if ($productType == 1) {
            $data['product_type'] = "coupons";
            $data['product_type_title'] = "Campaigns";
            $this->_magenestVendorHelper->setVendorAsLoggedIn($vendorId);
            //Today sale
            $collectionTodaySale = $this->_magenestVendorHelper->getGrouponTodayCollection($vendorId)->getColumnValues('price');
            $todaySale = 0;
            foreach ($collectionTodaySale as $value)
                $todaySale += (float)$value;
            $data['today_sale'] = $todaySale;
            //Sold today
            $data['sold_today'] = count($this->_magenestVendorHelper->getGrouponTodayCollection($vendorId));
            //To day redeem
            $collectionTodayRedeem = $this->_magenestVendorHelper->getGrouponTodayCollection($vendorId)->addFieldToFilter('status', \Magenest\Groupon\Model\Groupon::STATUS_USED);
            $data['today_redeem'] = count($collectionTodayRedeem->getData());

            $data['soldout_day'] = $this->_magenestVendorHelper->getSoldOutOption($vendorId);
            $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
            $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
            $data['currency_code'] = $currencyCode;

            //Active Campaign
            $data['active_campaign'] = count($this->_magenestVendorHelper->getActiveCampaignCollection($vendorId));


            //Pending Balance
            $data['pending_balance'] = $this->_magenestVendorHelper->getPendingBalance($vendorId);

            //Reviews Collection
            $vendorRating = $this->_vendorApiProductHelper->getVendorReviewsCollection($vendorId);
            $reviewVendorIds = [];
            foreach ($vendorRating as $rating) {
                $reviewVendorIds[] = $rating->getReviewId();
            }
            $rating = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\View\Rating');
            $avgRate = $rating->getAvgRate($reviewVendorIds);
            $data['review_collection'] = $avgRate ? $avgRate : 0;

            //Campaign
            $data['campaign'] = $campaign = [];
            $campaign['active_campaign'] = count($this->_magenestVendorHelper->getActiveCampaignCollection($vendorId));
            $activeCampaignIds = $this->_magenestVendorHelper->getActiveCampaignCollectionIds($vendorId);

            $redeemNumber = $this->_magenestVendorHelper->getRedeemed($activeCampaignIds);
            $campaign['redeem_number'] = $redeemNumber;
            //Remaining
            $remaining = $this->_magenestVendorHelper->getRemaining($activeCampaignIds);
            $campaign['available'] = $remaining;
            //Refunded
            $refuned = $this->_magenestVendorHelper->getRefunded($activeCampaignIds);
            $campaign['refunded'] = $refuned;
            //Sold
            $sold = $this->_magenestVendorHelper->getSold($activeCampaignIds);
            $campaign['sold'] = $sold;
            // Percen
            $percent = $sold == 0 ? 0 : number_format($redeemNumber / $sold, 2) * 100;
            $campaign['percent_redeem'] = $percent;
            //live Campaign url
            $liveCampaignUrl = ($data['product_type_title'] != "Events") ? $this->_urlBuilder->getUrl('groupon/vendor/campaign', ['filter' => '5']) : $this->_urlBuilder->getUrl('ticket/vendor/manage', ['filter' => '5']);
            $campaign['live_campaign_url'] = $liveCampaignUrl;
            $data['campaign'] = [$campaign];
            //Payment
            $data['payment'] = $this->getPaymentByVendorId($vendorId);

            //Payment chart
            $data['payment_chart'] = $paymentChart = [];
            $data['payment_chart'] = [
                $this->_magenestVendorHelper->getOption('all_time', $vendorId),
                $this->_magenestVendorHelper->getOption("this_month", $vendorId),
                $this->_magenestVendorHelper->getOption(7, $vendorId),
                $this->_magenestVendorHelper->getOption(30, $vendorId),
                $this->_magenestVendorHelper->getOption(90, $vendorId)
            ];

            //Feedback
            $data['feedback'] = $this->getFeedbackByVendorId($vendorId);
            //Question
            $data['question'] = $this->_magenestVendorHelper->getRecentQuestion();
            $data['all_question_url'] = $this->_urlBuilder->getUrl('questions/index/index');
            return $this->_objectFactory->addData($data);
        }
        return [];
    }

    //Vendor Ticket Dashboard
    public function getVendorTicketDashboard($vendorId)
    {
        $productType = $this->_magenestVendorHelper->getVendor($vendorId)->getPromoteProduct();
        if ($productType == 3) {
            $data['product_type'] = "tickets";
            $data['product_type_title'] = "Events";

            //Currency
            $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
            $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
            $data['currency_code'] = $currencyCode;
            $this->_magenestVendorHelper->setVendorAsLoggedIn($vendorId);
            //Today sale
            $data['today_sale'] = $this->_magenestVendorHelper->getTicketTodaySale($vendorId);

            //Today sold
            $data['sold_today'] = count($this->_magenestVendorHelper->getTicketCollectionSaleToday($vendorId));

            //To day redeem
            $data['today_redeem'] = $this->_magenestVendorHelper->getTicketTodayRedeemed($vendorId);
            //Sold-out
            $data['soldout_day'] = $this->_magenestVendorHelper->getTicketSoldOutOption($vendorId);
            //Active Event
            $data['active_event'] = count($this->_magenestVendorHelper->getActiveEventCollection($vendorId));
            //Pending Balance
            $data['pending_balance'] = $this->_magenestVendorHelper->getPendingBalance();
            //Reviews Collection
            $data['review_collection'] = $this->_magenestVendorHelper->getReviewsCollection($vendorId);

            //Event ticket
            $data['events'] = $campaign = [];
            $campaign['active_event'] = count($this->_magenestVendorHelper->getActiveEventCollection($vendorId));
            $activeCampaignIds = $this->_magenestVendorHelper->getActiveEventCollectionIds($vendorId);

            $redeemNumber = $this->_ticketHelper->getRedeemed($activeCampaignIds);
            $campaign['redeem_number'] = $redeemNumber;
            //Remaining
            $remaining = $this->_ticketHelper->getRemaining($activeCampaignIds);
            $campaign['available'] = $remaining;
            //Refunded
            $refuned = $this->_magenestVendorHelper->getRefunded($activeCampaignIds);
            $campaign['refunded'] = $refuned;
            //Sold
            $sold = $this->_ticketHelper->getSold($activeCampaignIds);
            $campaign['sold'] = $sold;
            // Percen
            $percent = $sold == 0 ? 0 : number_format($redeemNumber / $sold, 2) * 100;
            $campaign['percent_redeem'] = $percent;
            //live Event url
            $liveCampaignUrl = $this->_urlBuilder->getUrl('ticket/vendor/manage', ['filter' => '5']);
            $campaign['live_campaign_url'] = $liveCampaignUrl;
            $data['events'] = [$campaign];
            //End event ticket

            //Payment
            $data['payment'] = $this->getPaymentByVendorId($vendorId);

            //Payment chart
            $data['payment_chart'] = $paymentChart = [];
            $data['payment_chart'] = [
                $this->_magenestVendorHelper->getOption('all_time', $vendorId),
                $this->_magenestVendorHelper->getOption("this_month", $vendorId),
                $this->_magenestVendorHelper->getOption(7, $vendorId),
                $this->_magenestVendorHelper->getOption(30, $vendorId),
                $this->_magenestVendorHelper->getOption(90, $vendorId)
            ];
            //Feedback
            $data['feedback'] = $this->getFeedbackByVendorId($vendorId);
            //Question
            $data['question'] = $this->_magenestVendorHelper->getRecentQuestion();
            $data['all_question_url'] = $this->_urlBuilder->getUrl('questions/index/index');
            return $this->_objectFactory->addData($data);
        }
        return [];
//        $vendor = $this->_vendorFactory->create()->load($vendorId);
//        \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->setVendor($vendor);
    }

    //Vendor Product Dashboard
    public function getVendorProductDashboard($vendorId)
    {
        try{
            $productType = $this->_magenestVendorHelper->getVendor($vendorId)->getPromoteProduct();

            if ($productType == 4) {
                $data['product_type'] = "product";
                $data['product_type_title'] = "Inventory";
                //Currency
                $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
                $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
                $data['currency_code'] = $currencyCode;
                $this->_magenestVendorHelper->setVendorAsLoggedIn($vendorId);
                //Today sale
                $data['today_sale'] = $this->_magenestVendorHelper->getProductTodaySale();

                //Today sold
                $data['sold_today'] = $this->_magenestVendorHelper->getproductUnitsSaleToday();

                //Sold-out
                $data['soldout_day'] = $this->_magenestVendorHelper->getProductSoldOutOption();
                //Active Product
                $data['active_inventory'] = count($this->_magenestVendorHelper->getActiveProductCollection());
                //Pending Balance
                $data['pending_balance'] = $this->_magenestVendorHelper->getPendingBalance();
                //Reviews Collection
                $data['review_collection'] = $this->_magenestVendorHelper->getReviewsCollection($vendorId);

                //Product
                $data['inventory'] = $campaign = [];
                $campaign['active_inventory'] = count($this->_magenestVendorHelper->getActiveProductCollection());
                $activeCampaignIds = $this->_magenestVendorHelper->getActiveProductCollectionIds($vendorId);
                //Fulfilled
                $redeemNumber = $this->_magenestVendorHelper->getProductRedeemed();
                $campaign['fulfilled_number'] = $redeemNumber;
                //Pending Shipment
                $remaining = $this->_magenestVendorHelper->getProductRemaining($activeCampaignIds);
                $campaign['pending_shipment'] = $remaining;
                //Refunded
                $refuned = $this->_magenestVendorHelper->getRefunded($activeCampaignIds);
                $campaign['returns'] = $refuned;
                //Sold
                $sold = $this->_magenestVendorHelper->getProductSold($activeCampaignIds);
                $campaign['units_sold'] = $sold;
                // Percen
                $percent = $sold == 0 ? 0 : number_format($redeemNumber / $sold, 2) * 100;
                $campaign['percent_fulfilled'] = $percent;
                //live Event url
                $liveCampaignUrl = $this->_urlBuilder->getUrl('udsell/inventory/index', ['filter' => '5']);
                $campaign['live_campaign_url'] = $liveCampaignUrl;
                $data['inventory'] = [$campaign];
                //End product

                //Payment
                $data['payment'] = $this->getPaymentByVendorId($vendorId);

                //Payment chart
                $data['payment_chart'] = $paymentChart = [];
                $data['payment_chart'] = [
                    $this->_magenestVendorHelper->getOption('all_time', $vendorId),
                    $this->_magenestVendorHelper->getOption("this_month", $vendorId),
                    $this->_magenestVendorHelper->getOption(7, $vendorId),
                    $this->_magenestVendorHelper->getOption(30, $vendorId),
                    $this->_magenestVendorHelper->getOption(90, $vendorId)
                ];
                //Feedback
                $data['feedback'] = $this->getFeedbackByVendorId($vendorId);
                //Question
                $data['question'] = $this->_magenestVendorHelper->getRecentQuestion();
                $data['all_question_url'] = $this->_urlBuilder->getUrl('questions/index/index');
                return $this->_objectFactory->addData($data);
            }
            return [];
        }catch (\Exception $e){
            $this->_debugHelper->debugString('VendorProductDashboard', $e);
            return false;
        }
    }

    public function getPaymentByVendorId($vendorId)
    {
        $data = $payment = [];
        $paymentCollection = $this->_magenestVendorHelper->getPayoutCollection($vendorId);

        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        $currency = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Directory\Model\Currency');
        $currency->load($currencyCode);
        foreach ($paymentCollection as $item) {
            $payment['id'] = $item->getId();
            $payment['payout_status'] = $item->getPayoutStatus();
            $payment['total_payout'] = $item->getTotalPayout();
            $payment['total_paid'] = $item->getTotalPaid();
            $payment['total_due'] = $item->getTotalDue();
            $payment['total_payment'] = $item->getTotalPayment();
            $payment['total_invoice'] = $item->getTotalInvoice();
            $payment['total_refund'] = $item->getTotalRefund();
            $payment['total_reversed'] = $item->getTotalReversed();
            $payment['created_at'] = date("F j, Y", strtotime($item->getData('created_at')));
            $paymentUrl = $this->_urlBuilder->getUrl('payments');
            $payment['payment_url'] = $paymentUrl;
            $data[] = $payment;
        }
        return $data;
    }

    public function getFeedbackByVendorId($vendorId)
    {
        $data = $feedback = [];
        $rating = [];
        $feedbackCollection = $this->_magenestVendorHelper->getFeedbackCollection($vendorId, $cur_page = 1, $size = 2);
        foreach ($feedbackCollection as $collection) {
            foreach ($collection->getRatingVotes() as $_vote) {
                $rating[] = $_vote->getData();
            }

            $feedback['nickname'] = $collection->getNickname();
            $feedback['title'] = $collection->getTitle();
            $feedback['detail'] = $collection->getDetail();
            $feedback['created_at'] = $collection->getCreatedAt();
            $feedback['customer_id'] = $collection->getCustomerId();
            $feedback['rating'] = $rating;
            $data[] = $feedback;
        }
        return $data;
    }

    public function getVendorCampaignDashboard($vendorId, $requestParams)
    {
        $status = isset($requestParams['status']) ? $requestParams['status'] : 'all';
        $cur_page = isset($requestParams['cur_page']) ? $requestParams['cur_page'] : '';
        $size = isset($requestParams['size']) ? $requestParams['size'] : '';
        $sortField = isset($requestParams['sort_field']) ? $requestParams['sort_field'] : '';
        $sortOrder = isset($requestParams['sort_order']) ? $requestParams['sort_order'] : 'desc';
        $collections = $this->_magenestVendorHelper->getDeal($vendorId, $status, $cur_page, $size, $sortField, $sortOrder);
        $results = $resultData = [];
        foreach ($collections as $collection) {
            $productId = $collection->getProductId();
            $product = $this->_magenestVendorHelper->getItems($productId);
            $data = $this->_magenestVendorHelper->getDealDetails($collection);
            $resultData['campaign_status'] = $data['status'];
            $resultData['campaign_name'] = [
                'url' => $this->_urlBuilder->getUrl('groupon/vendor_campaign/detail', ['id' => $collection->getProductId()]),
                'image' => $this->_urlBuilder->getBaseUrl() . 'media/catalog/product' . $product->getData('image'),
                'product_name' => $product->getName(),
                'option_count' => $data['option_count']
            ];
            $resultData['product_id'] = $productId;
            $resultData['campaign_starts'] = $data['start_time'];
            $resultData['campaign_ends'] = $data['end_time'];
            $resultData['campaign_sold'] = $data['sold'];
            $resultData['campaign_revenue'] = $data['revenue'];
            $resultData['campaign_validFrom'] = $data['start_valid'];
            $resultData['campaign_validTo'] = $data['end_valid'];
            $resultData['campaign_views'] = $data['view_count'];
            $resultData['campaign_activity'] = $data['redeemed'];
            $results[] = $resultData;
        }
        return $results;
    }

    public function getVendorFeedbackDashboard($vendorId, $cur_page, $size)
    {
        $vendor = $this->_magenestVendorHelper->getVendor($vendorId);
        $vendor_name = $vendor->getVendorName();
        $results = [];
        $feedbackCollection = $this->_magenestVendorHelper->getFeedbackCollection($vendorId, $cur_page, $size);
        foreach ($feedbackCollection as $collection) {
            foreach ($collection->getRatingVotes() as $_vote) {
                $rating[] = $_vote->getData();
            }
            $feedback['nickname'] = $collection->getNickname();
            $feedback['title'] = $collection->getTitle();
            $feedback['detail'] = $collection->getDetail();
            $feedback['created_at'] = $collection->getCreatedAt();
            $feedback['customer_id'] = $collection->getCustomerId();
            $feedback['rating'] = $rating;
            $results[] = $feedback;
        }
        return $this->_objectFactory->addData(["vendor_name" => $vendor_name, "feedback" => $results]);
    }

    public function getVendorQuestionAnswerDashboard($vendorId, $status, $cur_page, $size)
    {
        $results = [];
        $numberStatusQuestion = $this->_magenestQuestionHelper->getNumStatusQuestion();
        $results['number_question_status'][] = $numberStatusQuestion;
        $results['question'] = $arrQuestion = [];
        $allQuestion = $this->_magenestQuestionHelper->getQuestions($vendorId, $status, $cur_page, $size);
        foreach ($allQuestion->getData() as $question) {

            $arrQuestion['question_id'] = $question['question_id'];
            $arrQuestion['customer_name'] = $question['customer_name'];
            $arrQuestion['question_status'] = $question['question_status'];
            $arrQuestion['answer_status'] = $question['answer_status'];

            $arrQuestion['answer_text'] = $question['answer_text'];
            $arrQuestion['answer_date'] = $question['answer_date'];
            $arrQuestion['visibility'] = $question['visibility'];
            $arrQuestion['question_date'] = $question['question_date'];

            if ($question['product_id'] != null) {
                $arrQuestion['product_id'] = $question['product_id'];
                $product = $this->_magenestVendorHelper->getProductInfoByID($question['product_id']);
                $arrQuestion['product_name'] = $product[0];
                $arrQuestion['product_url'] = $product[1];
            }
            $arrQuestion['question_text'] = $question['question_text'];

            $vendor = $this->_magenestVendorHelper->getVendor($question['vendor_id']);
            $arrQuestion['vendor_name'] = $vendor->getVendorName();


            $arrQuestion['is_customer_notified'] = $question['is_customer_notified'];

            $arrQuestion['button_set_text'] = "Reply";
            if ($question['is_customer_notified']) {
                $arrQuestion['button_set_text'] = "Reply And Email";
            }
            if ($question['question_status'] == 1 && $question['answer_status'] == 1 && ($question['answer_text'] !== null && $question['answer_text'] !== "")) {
                $arrQuestion['button_set_text'] = "Modify";
                if ($question['is_customer_notified']) {
                    $arrQuestion['button_set_text'] = "Modify And Email";
                }
            }
            $results['question'][] = $arrQuestion;
        }
        return $this->_objectFactory->addData($results);
    }

    public function getVendorManageEventDashboard($vendorId, $requestParams)
    {
        $status = isset($requestParams['status']) ? $requestParams['status'] : 'all';
        $cur_page = isset($requestParams['cur_page']) ? $requestParams['cur_page'] : '';
        $size = isset($requestParams['size']) ? $requestParams['size'] : '';
        $sortby = isset($requestParams['sort_field']) ? $requestParams['sort_field'] : '';
        $sorttype = isset($requestParams['sort_order']) ? $requestParams['sort_order'] : 'desc';
        $eventCollection = $this->_magenestEventTicketHelper->getEventProductCollection($vendorId, $status, $sortby, $sorttype, $cur_page, $size);
        $serial = $totalSold = $totalRevenue = $totalView = $totalRedeem = 0;
        $results = $data = [];
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        $currencySymbol = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Directory\Model\CurrencyFactory')->create()->load($currencyCode)->getCurrencySymbol();
        foreach ($eventCollection as $event) {
            $serial++;
            $sessionData = $this->_magenestEventTicketHelper->getSessionDataByEventId($event['entity_id']);
            $sessionData['count'] = $event['view'];
            $totalSold += intval($sessionData['sold']);
            $totalRevenue += $sessionData['revenue'];
            $totalRedeem += intval($sessionData['redeemed']['redeem']);

            $data['entity_id'] = $event['entity_id'];

            $status = $this->_magenestEventTicketHelper->getCampaignStatus($event['entity_id']);
            $data['status'] = $status;

            $data['event_name'] = $event['event_name'];

            $productImage = $this->_magenestEventTicketHelper->getProductImage($event['entity_id']);
            $data['product_image'] = $productImage;

            $data['buy_options'] = $sessionData['buy_options'];
            $data['ticket_options'] = $sessionData['ticket_options'];

            $data['start'] = $event['start'];
            $data['end'] = $event['end'];
            $data['sold'] = $event['sold'];
            $data['total_ticket'] = $sessionData['total_ticket'];
            $data['currency'] = $currencySymbol;
            $data['revenue'] = number_format($event['revenue'] ? $event['revenue'] : 0, 2);
            $data['view'] = $event['view'];
            $data['redeemed'] = $sessionData['redeemed'];
            $results[] = $data;
        }
        return $this->_objectFactory->addData([
            "event_list" => $results,
            "total_sold" => $totalSold,
            "total_revenue" => $totalRevenue,
            "total_review" => $totalView,
            "total_redeem" => $totalRedeem
        ]);
    }

    public function getVendorInventoryManage($vendorId, $requestParams)
    {
        $status = isset($requestParams['status']) ? $requestParams['status'] : '-2';
        $cur_page = isset($requestParams['cur_page']) ? $requestParams['cur_page'] : '';
        $size = isset($requestParams['size']) ? $requestParams['size'] : '';
        $sortby = isset($requestParams['sort_field']) ? $requestParams['sort_field'] : '';
        $sorttype = isset($requestParams['sort_order']) ? $requestParams['sort_order'] : 'desc';
        $search = isset($requestParams['search']) ? $requestParams['search'] : '';
        $inventoryCollections = $this->_magenestInventoryHelper->getVendorInventory($vendorId, $status, $sortby, $sorttype, $search, $cur_page, $size);
        return $inventoryCollections;
    }

    public function getVendorCampaignDetail($vendorId, $requestParams)
    {
        $productId = isset($requestParams['product_id']) ? $requestParams['product_id'] : '';
        $dealProductId = DataHelper::getChildIds($productId);


        $search_pol = isset($requestParams['search_pol']) ? $requestParams['search_pol'] : '';
        $cur_page_pol = isset($requestParams['cur_page_pol']) ? $requestParams['cur_page_pol'] : '';
        $size_pol = isset($requestParams['size_pol']) ? $requestParams['size_pol'] : '';
        $sortby_pol = isset($requestParams['sort_field_pol']) ? $requestParams['sort_field_pol'] : '';
        $sorttype_pol = isset($requestParams['sort_order_pol']) ? $requestParams['sort_order_pol'] : 'desc';

        $cur_page_ch = isset($requestParams['cur_page_ch']) ? $requestParams['cur_page_ch'] : '';
        $size_ch = isset($requestParams['size_ch']) ? $requestParams['size_ch'] : '';

        $results = [];
        $results['info'] = [
            "Selling " . $this->_magenestVendorHelper->getSellingFrom($productId) . " " . $this->_magenestVendorHelper->getSellingTo($productId) . "",
            "Coupons valid " . $this->_magenestVendorHelper->getValidFrom($productId) . " " . $this->_magenestVendorHelper->getValidTo($productId) . "",
            "This offer will be unpublished in " . $this->_magenestVendorHelper->getUnpublishedIn($productId) . " day(s). Customers have " . $this->_magenestVendorHelper->getInvalidIn($productId) . " day(s) left to redeem their coupons."
        ];
        $results['info']['links_edit'] = $this->_urlBuilder->getUrl('udprod/vendor/productEdit', ['id' => $productId]);

        $results['activity_chart'] = [];
        $qtyAvailable = $this->_magenestVendorHelper->getQtyAvailable($dealProductId);
        $qtyRedeemed = $this->_magenestVendorHelper->getQtyRedeemed($dealProductId);
        $qtyRefunded = $this->_magenestVendorHelper->getQtyRefunded($dealProductId);
        $qtySole = (int)($qtyAvailable + $qtyRedeemed);
        $totalCoupon = intval($qtyRedeemed) + intval($qtyAvailable) + intval($qtyRefunded);
        if ($totalCoupon == 0) {
            $availablePercent = $redeemedPercent = $refundedPercent = 0;
        } else {
            $availablePercent = number_format((intval($qtyAvailable) / $totalCoupon) * 100, 0, '.', '');
            $refundedPercent = number_format((intval($qtyRefunded) / $totalCoupon) * 100, 0, '.', '');
            $redeemedPercent = number_format(100 - $availablePercent - $refundedPercent, 0, '.', '');
        }
        $results['activity_chart'] = [
            [
                'name' => 'qty',
                'available' => $qtyAvailable,
                'redeem' => $qtyRedeemed,
                'refunded' => $qtyRefunded,
                'sold' => $qtySole
            ],
            [
                'name' => 'percent',
                'available' => $availablePercent,
                'redeem' => $redeemedPercent,
                'refunded' => $refundedPercent,
                'sold' => $qtySole
            ]
        ];
        $options["all_time"] = $this->_magenestVendorHelper->getOptions("all_time", $dealProductId,$productId);
        $options["this_month"] = $this->_magenestVendorHelper->getOptions("this_month", $dealProductId,$productId);
        $options[7] = $this->_magenestVendorHelper->getOptions(7, $dealProductId,$productId);
        $options[30] = $this->_magenestVendorHelper->getOptions(30, $dealProductId,$productId);
        $options[90] = $this->_magenestVendorHelper->getOptions(90, $dealProductId,$productId);

        $results['sales_chart'] = [
            [
                'name' => 'all_time',
                'option' => $options['all_time']['option'],
                'summary' => $options['all_time']['summary']
            ],
            [
                'name' => 'this_month',
                'option' => $options['this_month']['option'],
                'summary' => $options['this_month']['summary']
            ],
            [
                'name' => 'last_7_days',
                'option' => $options[7]['option'],
                'summary' => $options[7]['summary']
            ],
            [
                'name' => 'last_30_days',
                'option' => $options[30]['option'],
                'summary' => $options[30]['summary']
            ],
            [
                'name' => 'last_90_days',
                'option' => $options[90]['option'],
                'summary' => $options[90]['summary']
            ]
        ];

        //Campaig Buy Options
        $campaignBuyOptions = $this->_magenestVendorHelper->getCampaignBuyOption($productId);
        $results['campaign_buy_options'] = [];
        foreach ($campaignBuyOptions as $campaignBuyOption){
            if (@$campaignBuyOption['sold'] == null || @$campaignBuyOption['sold'] == '0'){
                $activity_percent =  null;
            }else{
                $activity_percent = 100 * (int)@$campaignBuyOption['redeemed'] / (int)@$campaignBuyOption['sold'];
            }
            $redeem = @$campaignBuyOption['redeemed'] ?: 0;
            $sold = @$campaignBuyOption['sold'] ?: 0;
            $activity = $redeem." of ". $sold;
            $data = [
                'option_id' => $campaignBuyOption['product_id'],
                'option_name' => $campaignBuyOption['name'],
                'sold' => $sold,
                'revenue' => @$campaignBuyOption['revenue'] ?: 0,
                'refunded' => @$campaignBuyOption['refunded'] ?: 0,
                'qty' => @$campaignBuyOption['qty'] ?: 0,
                'activity_percent' => number_format($activity_percent,2),
                'activity' => $activity
            ];
            $results['campaign_buy_options'][] = $data;
        }

        $purchasedCouponCollection = $this->_magenestVendorHelper->getGroupon($cur_page_pol, $size_pol, $search_pol, $sortby_pol, $sorttype_pol, $dealProductId);
        $results['purchased_coupon_list'] = $purchasedData = [];
        foreach ($purchasedCouponCollection as $groupon) {
            $purchasedData['campaign_code'] = $groupon->getGrouponCode();
            $purchasedData['customer_name'] = $groupon->getCustomerName();
            $purchasedData['product_name'] = $groupon->getProductName();
            $purchasedData['redemption_location'] = $groupon->getCustomerSpecifyLocation() . " Any " . $groupon->getCustomerSpecifyLocation();
            $purchasedData['campaign_status'] = Groupon::getStatusLabelByCode($groupon->getStatus());
            $purchasedData['purchase_date'] = $groupon->getCreatedAt();
            $purchasedData['redeem_date'] = $groupon->getRedeemedAt();
            $results['purchased_coupon_list'][] = $purchasedData;
        }
        $results['change_histories'] = $histories = [];
        $changHistoryCollection = $this->_magenestVendorHelper->getChangeHistory($cur_page_ch, $size_ch, $productId);
        if($changHistoryCollection->getSize()){
            foreach ($changHistoryCollection as $item) {
                $histories['campaign_start'] = date('D, M j, Y', strtotime($item->getCreatedAt()));
                $histories['campaign_status'] = $this->_magenestVendorHelper->getStatusLabel($item->getStatusCode());
                $histories['campaign_name'] = $item->getComment();
                $results['change_histories'][] = $histories;
            }
        }
        return $this->_objectFactory->addData($results);
    }

    public function getVendorEventTicketDetail($vendorId, $requestParams)
    {
        $results = [];
        if (isset($requestParams['product_id'])) {
            $productId = $requestParams['product_id'];
            $search_ptl = isset($requestParams['search_ptl']) ? $requestParams['search_ptl'] : '';
            $cur_page_ptl = isset($requestParams['cur_page_ptl']) ? $requestParams['cur_page_ptl'] : '1';
            $size_ptl = isset($requestParams['size_ptl']) ? $requestParams['size_ptl'] : '20';
            $sortby_ptl = isset($requestParams['sort_field_ptl']) ? $requestParams['sort_field_ptl'] : '';
            $sorttype_ptl = isset($requestParams['sort_order_ptl']) ? $requestParams['sort_order_ptl'] : 'desc';

            $cur_page_ch = isset($requestParams['cur_page_ch']) ? $requestParams['cur_page_ch'] : 1;
            $size_ch = isset($requestParams['size_ch']) ? $requestParams['size_ch'] : 1;

            $sortPTL = [];
            if ($sortby_ptl != '') {
                $sortPTL = [
                    'sort_by' => $sortby_ptl,
                    'sort_type' => $sorttype_ptl
                ];
            }
            $sortby_ss = isset($requestParams['sort_field_ss']) ? $requestParams['sort_field_ss'] : '';
            $sorttype_ss = isset($requestParams['sort_order_ss']) ? $requestParams['sort_order_ss'] : 'desc';

            $results['currency'] = $this->_magenestEventTicketHelper->getCurrencySymbol();
            $product = $this->_magenestEventTicketHelper->getProductById($productId);
            $results['product_id'] = $productId;
            $results['product_name'] = $product->getEventName();
            $results['status'] = $this->_magenestEventTicketHelper->getCampaignStatusCode($productId);
            $results['status_label'] = $this->_magenestEventTicketHelper->getCampaignStatus($productId);
            $results['info'] = [
                "There are " . $this->_magenestEventTicketHelper->getSessionCount($productId) . " different session(s) setup for this event",
                "You are selling tickets in " . $this->_magenestEventTicketHelper->getCategoriesCount($productId) . " different categories"
            ];
            $results['info']['links_edit'] = $this->_urlBuilder->getUrl('udprod/vendor/productEdit', ['id' => $productId]);
            $availableQty = $this->_magenestEventTicketHelper->getQtyAvailableByProductId($productId);
            $redeemQty = $this->_magenestEventTicketHelper->getQtyRedeemByProductId($productId);
            $refundQty = $this->_magenestEventTicketHelper->getQtyRefundedByProductId($productId);
            $soldQty = $this->_magenestEventTicketHelper->getQtyTicketSold($productId);;
            $totalTickets = intval($redeemQty) + intval($availableQty) + intval($refundQty);
            if ($totalTickets == 0) {
                $availablePercent = $redeemedPercent = $refundedPercent = 0;
            } else {
                $availablePercent = number_format((intval($availableQty) / $totalTickets) * 100, 2, '.', '');
                $refundedPercent = number_format((intval($refundQty) / $totalTickets) * 100, 2, '.', '');
                $redeemedPercent = number_format(100 - $availablePercent - $refundedPercent, 2, '.', '');
            }
            $activityChart = [
                'available' => $availableQty,
                'redeem' => $redeemQty,
                'refunded' => $refundQty,
                'sold' => $soldQty
            ];
            $results['activity_chart'][] = $activityChart;
            $options["all_time"] = $this->_magenestEventTicketHelper->getOption("all_time", $productId);
            $options["this_month"] = $this->_magenestEventTicketHelper->getOption("this_month", $productId);
            $options[7] = $this->_magenestEventTicketHelper->getOption(7, $productId);
            $options[30] = $this->_magenestEventTicketHelper->getOption(30, $productId);
            $options[90] = $this->_magenestEventTicketHelper->getOption(90, $productId);
            $results['sales_chart'] = [
                [
                    'name' => 'all_time',
                    'option' => $options['all_time']['option'],
                    'summary' => $options['all_time']['summary']
                ],
                [
                    'name' => 'this_month',
                    'option' => $options['this_month']['option'],
                    'summary' => $options['this_month']['summary']
                ],
                [
                    'name' => 'last_7_days',
                    'option' => $options[7]['option'],
                    'summary' => $options[7]['summary']
                ],
                [
                    'name' => 'last_30_days',
                    'option' => $options[30]['option'],
                    'summary' => $options[30]['summary']
                ],
                [
                    'name' => 'last_90_days',
                    'option' => $options[90]['option'],
                    'summary' => $options[90]['summary']
                ]
            ];
            $sort = [];
            if ($sortby_ss != '') {
                $sort = [
                    'sort_by' => $sortby_ss,
                    'sort_type' => $sorttype_ss
                ];
            }

            $results['sessions_table'] = $arrSession = [];
            $sessionCollection = $this->_magenestEventTicketHelper->getSessionsCollection($sessionId = null, $page = '', $pageSize = '', $productId, $sort);
            $serial = 0;
            foreach ($sessionCollection as $session) {
                $serial++;
                $sessionId = $session->getSessionId();
                $arrSession['session_id'] = $session->getSessionId();
                $arrSession['session_title'] = $this->_magenestEventTicketHelper->getSessionTitle($sessionId);
                $arrSession['session_status'] = $this->_magenestEventTicketHelper->getSessionStatus($sessionId);
                $arrSession['session_qty_redeem'] = $this->_magenestEventTicketHelper->getQtyRedeemed($sessionId, $productId);
                $arrSession['session_sold'] = $session->getQtyPurchased();
                $arrSession['session_revenue'] = $session->getRevenue();
                $arrSession['session_refunded'] = $session->getQtyRefunded() ?: "0";
                $arrSession['session_qty'] = $session->getQty();
                $arrSession['session_max_qty_each_person'] = $session->getMaxQty() ?: "_";
                $results['sessions_table'][] = $arrSession;
            }
            $results['purchased_ticket_list'] = $arrPurchasesTicketList = [];
            $purchasedTicketCollection = $this->_magenestEventTicketHelper->getPurchasedTicketList($vendorId, $sessionId = null, $search_ptl, $cur_page_ptl, $size_ptl, $sortPTL, $productId);
            foreach ($purchasedTicketCollection as $event) {
                $arrPurchasesTicketList['ticket_code'] = $event->getCode();
                $arrPurchasesTicketList['customer_name'] = $event->getCustomerName();
                $arrPurchasesTicketList['ticket_code'] = $event->getCode();
                $arrPurchasesTicketList['session_date'] = date('D, M j, Y', strtotime($event->getSessionFrom()));
                $arrPurchasesTicketList['session_time'] = date('H:i A', strtotime($event->getSessionFrom()));
                $arrPurchasesTicketList['ticket_type'] = $event->getTicketType();
                $arrPurchasesTicketList['ticket_status'] = $this->_magenestEventTicketHelper->getTicketStatus($event->getStatus());
                $arrPurchasesTicketList['purchased_date'] = date('D, M j, Y', strtotime($event->getCreatedAt()));
                $arrPurchasesTicketList['purchased_time'] = date('H:i A', strtotime($event->getCreatedAt()));
                if ($event->getRedeemedAt()) {
                    $arrPurchasesTicketList['redeem_date'] = date('H:i A', strtotime($event->getRedeemedAt()));
                    $arrPurchasesTicketList['redeem_time'] = date('H:i A', strtotime($event->getRedeemedAt()));
                }
                $results['purchased_ticket_list'][] = $arrPurchasesTicketList;
            }
            $results['change_history'] = $arrChangeHistory = [];
            $histories = $this->_magenestEventTicketHelper->getChangeHistory($productId, $cur_page_ch, $size_ch);
            foreach ($histories as $item) {
                $arrChangeHistory['start_date'] = date('D, M j, Y', strtotime($item->getCreatedAt()));
                $arrChangeHistory['start_time'] = date('H:i A', strtotime($item->getCreatedAt()));
                $arrChangeHistory['status'] = $this->_magenestEventTicketHelper->getStatusLabel($item->getStatusCode());
                $arrChangeHistory['commment'] = $item->getComment();
                $results['change_history'][] = $arrChangeHistory;
            }
        }
        return $this->_objectFactory->addData($results);
    }

    public function getVendorTicketSessionDetail($vendorId, $requestParams)
    {
        $results = [];
        if (isset($requestParams['product_id']) && isset($requestParams['session_id'])) {
            $productId = $requestParams['product_id'];
            $search_ptl = isset($requestParams['search_ptl']) ? $requestParams['search_ptl'] : '';
            $cur_page_ptl = isset($requestParams['cur_page_ptl']) ? $requestParams['cur_page_ptl'] : '1';
            $size_ptl = isset($requestParams['size_ptl']) ? $requestParams['size_ptl'] : '20';
            $sortby_ptl = isset($requestParams['sort_field_ptl']) ? $requestParams['sort_field_ptl'] : '';
            $sorttype_ptl = isset($requestParams['sort_order_ptl']) ? $requestParams['sort_order_ptl'] : 'desc';

            $sortPTL = [];
            if ($sortby_ptl != '') {
                $sortPTL = [
                    'sort_by' => $sortby_ptl,
                    'sort_type' => $sorttype_ptl
                ];
            }

            $cur_page_st = isset($requestParams['cur_page_st']) ? $requestParams['cur_page_st'] : 1;
            $size_st = isset($requestParams['size_st']) ? $requestParams['size_st'] : 10;
            $sortby_st = isset($requestParams['sort_field_st']) ? $requestParams['sort_field_st'] : '';
            $sorttype_st = isset($requestParams['sort_order_st']) ? $requestParams['sort_order_st'] : '';

            $sessionId = $requestParams['session_id'];
            $results['session_title'] = $this->_magenestEventTicketHelper->getSessionTitle($sessionId);
            $status = $this->_magenestEventTicketHelper->getCampaignStatusCode($productId);
            $results['status'] = $status;
            $availableQty = $this->_magenestEventTicketHelper->getTicketSessionQtyAvailable($sessionId, $productId);
            $redeemQty = $this->_magenestEventTicketHelper->getQtyRedeemed($sessionId, $productId);
            $refundQty = $this->_magenestEventTicketHelper->getTicketSessionQtyRefunded($sessionId, $productId);
            $soldQty = $this->_magenestEventTicketHelper->getTicketSessionQtySold($sessionId, $productId);
            $totalTickets = intval($redeemQty) + intval($availableQty) + intval($refundQty);
            if ($totalTickets == 0) {
                $availablePercent = $redeemedPercent = $refundedPercent = 0;
            } else {
                $availablePercent = number_format((intval($availableQty) / $totalTickets) * 100, 2, '.', '');
                $refundedPercent = number_format((intval($refundQty) / $totalTickets) * 100, 2, '.', '');
                $redeemedPercent = number_format(100 - $availablePercent - $refundedPercent, 2, '.', '');
            }
            $activityChart = [
                'available' => $availableQty,
                'redeem' => $redeemQty,
                'refunded' => $refundQty,
                'sold' => $soldQty
            ];
            $results['activity_chart'][] = $activityChart;
            $options["all_time"] = $this->_magenestEventTicketHelper->getTicketSessionOption("all_time", $productId, $sessionId);
            $options["this_month"] = $this->_magenestEventTicketHelper->getTicketSessionOption("this_month", $productId, $sessionId);
            $options[7] = $this->_magenestEventTicketHelper->getTicketSessionOption(7, $productId, $sessionId);
            $options[30] = $this->_magenestEventTicketHelper->getTicketSessionOption(30, $productId, $sessionId);
            $options[90] = $this->_magenestEventTicketHelper->getTicketSessionOption(90, $productId, $sessionId);
            $results['sales_chart'] = [
                [
                    'name' => 'all_time',
                    'option' => $options['all_time']['option'],
                    'summary' => $options['all_time']['summary']
                ],
                [
                    'name' => 'this_month',
                    'option' => $options['this_month']['option'],
                    'summary' => $options['this_month']['summary']
                ],
                [
                    'name' => 'last_7_days',
                    'option' => $options[7]['option'],
                    'summary' => $options[7]['summary']
                ],
                [
                    'name' => 'last_30_days',
                    'option' => $options[30]['option'],
                    'summary' => $options[30]['summary']
                ],
                [
                    'name' => 'last_90_days',
                    'option' => $options[90]['option'],
                    'summary' => $options[90]['summary']
                ]
            ];

            $results['session_types'] = $arrSessionType = [];
            $sortSt = [];
            if ($sortby_st != '' && $sorttype_st != '') {
                $sortSt = [
                    'sort_by' => $sortby_st,
                    'sort_type' => $sorttype_st
                ];
            }
            $sessionCollection = $this->_magenestEventTicketHelper->getTicketTypeBySessionCollection($sessionId, $productId, $cur_page_st, $size_st, $sortSt);
            foreach ($sessionCollection as $session) {
                $sessionId = $session->getSessionId();

                $session_status = $this->_magenestEventTicketHelper->getSessionStatus($sessionId);
                $status = $session_status['status'];
                $arrSessionType['status'] = $this->getSessionTypeStatusCode($status);

                $session_redeem = $this->_magenestEventTicketHelper->getQtyRedeemed($sessionId, $productId);
                $arrSessionType['session_redeem'] = $session_redeem;
                $arrSessionType['ticket_category'] = $session->getTicketCategory();
                $arrSessionType['ref_id'] = $session->getTicketRef();
                $arrSessionType['session_sold'] = $session->getQtyPurchased();
                $arrSessionType['session_revenue'] = number_format($session->getRevenue(), 2);
                $arrSessionType['session_refunds'] = $session->getQtyRefunded() ?: "0";
                $arrSessionType['session_qty'] = $session->getQty();
                $arrSessionType['session_max_qty_each_person'] = $session->getMaxQty() ?: "-";
                $results['session_types'][] = $arrSessionType;
            }
            $results['purchased_ticket_list'] = $arrPurchasesTicketList = [];
            $purchasedTicketCollection = $this->_magenestEventTicketHelper->getPurchasedTicketList($vendorId, $sessionId = null, $search_ptl, $cur_page_ptl, $size_ptl, $sortPTL, $productId);
            foreach ($purchasedTicketCollection as $event) {
                $arrPurchasesTicketList['ticket_code'] = $event->getCode();
                $arrPurchasesTicketList['customer_name'] = $event->getCustomerName();
                $arrPurchasesTicketList['ticket_code'] = $event->getCode();
                $arrPurchasesTicketList['session_date'] = date('D, M j, Y', strtotime($event->getSessionFrom()));
                $arrPurchasesTicketList['session_time'] = date('H:i A', strtotime($event->getSessionFrom()));
                $arrPurchasesTicketList['ticket_type'] = $event->getTicketType();
                $arrPurchasesTicketList['ticket_status'] = $this->_magenestEventTicketHelper->getTicketStatus($event->getStatus());
                $arrPurchasesTicketList['purchased_date'] = date('D, M j, Y', strtotime($event->getCreatedAt()));
                $arrPurchasesTicketList['purchased_time'] = date('H:i A', strtotime($event->getCreatedAt()));
                if ($event->getRedeemedAt()) {
                    $arrPurchasesTicketList['redeem_date'] = date('H:i A', strtotime($event->getRedeemedAt()));
                    $arrPurchasesTicketList['redeem_time'] = date('H:i A', strtotime($event->getRedeemedAt()));
                }
                $results['purchased_ticket_list'][] = $arrPurchasesTicketList;
            }
        }
        return $this->_objectFactory->addData($results);
    }
    public function getSessionTypeStatusCode($status){
        switch ($status){
            case "Live":
                $code = 0;
                break;
            case "Schedule":
                $code = 1;
                break;
            case "Expired":
                $code = 2;
                break;
            case "Sold Out":
                $code = 3;
                break;
            default:
                $code = 0;
                break;
        }
        return $code;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorReturns($vendorId)
    {
        $rmaCollection = $this->rmaFactory->create()->getCollection()
            ->addFieldToFilter('udropship_vendor', array('eq' => $vendorId))
            ->setOrder('created_at');
        $result = [];

        /** @var \Unirgy\Rma\Model\Rma $rma */
        foreach ($rmaCollection as $rma) {
            $rmaData = $rma->getData();

            $shippingAddress = $rma->getShippingAddress()->getData();
            $billingAddress = $rma->getBillingAddress()->getData();
            $allItems = $allComments = $allTracks = [];

            /** @var \Unirgy\Rma\Model\Rma\Item $item */
            foreach ($rma->getAllItems() as $item) {
                $itemData = $item->getData();

                array_push($allItems, [
                    'name' => $itemData['name'],
                    'sku' => $itemData['sku'],
                    'product_id' => $itemData['product_id'],
                    'order_item_id' => $itemData['order_item_id'],
                    'qty' => $itemData['qty'],
                    'price' => $itemData['price'],
                    'weight' => $itemData['weight']
                ]);
            }

            /** @var \Unirgy\Rma\Model\Rma\Comment $comment */
            foreach ($rma->getComments() as $comment) {
                array_push($allComments, $comment->getData());
            }

            /** @var \Unirgy\Rma\Model\Rma\Track $track */
            foreach ($rma->getAllTracks() as $track) {
                array_push($allTracks, $track->getData());
            }

            array_push($result, [
                'entity_id' => $rmaData['entity_id'],
                'order_id' => $rmaData['order_id'],
                'increment_id' => $rmaData['increment_id'],
                'customer_id' => $rmaData['customer_id'],
                'shipment_id' => $rmaData['shipment_id'],
                'status' => $rmaData['rma_status'],
                'vendor_id' => $rmaData['udropship_vendor'],
                'udropship_method' => [
                    'code' => $rmaData['udropship_method'],
                    'label' => $rmaData['udropship_method_description']
                ],
                'reason' => $rmaData['rma_reason'],
                'shipping_address' => [
                    'firstname' => $shippingAddress['firstname'],
                    'lastname' => $shippingAddress['lastname'],
                    'email' => $shippingAddress['email'],
                    'street' => $shippingAddress['street'],
                    'city' => $shippingAddress['city'],
                    'postcode' => $shippingAddress['postcode'],
                    'country_id' => $shippingAddress['country_id'],
                    'telephone' => $shippingAddress['telephone']

                ],
                'billing_address' => [
                    'firstname' => $billingAddress['firstname'],
                    'lastname' => $billingAddress['lastname'],
                    'email' => $billingAddress['email'],
                    'street' => $billingAddress['street'],
                    'city' => $billingAddress['city'],
                    'postcode' => $billingAddress['postcode'],
                    'country_id' => $billingAddress['country_id'],
                    'telephone' => $billingAddress['telephone']

                ],
                'items' => $allItems,
                'comments' => $allComments,
                'tracks' => $allTracks
            ]);
        }

        $response = [
            'result' => $result,
            'api_status' => 1
        ];

        return $this->_objectFactory->addData($response);
    }

    public function forgotPassword($email)
    {
        if ($this->_forgotPasswordHelper->apiSendPasswordResetEmail($email)) {
            $status = 1;
        } else {
            $status = 0;
        }
        return $this->_objectFactory
            ->addData([
                'status' => $status
            ]);
    }

    public function getVendorPayments($vendorId)
    {
        $this->_magenestVendorHelper->setVendorAsLoggedIn($vendorId);
        $offers = $this->_magenestVendorHelper->getVendorProductByVendor($vendorId);
        $summary = [
            'available_balance' => $this->_magenestVendorHelper->getEarning($vendorId),
            'pending_balance' => $this->_magenestVendorHelper->getPendingBalance(),
            'deductible_fees' => $this->_magenestVendorHelper->getPaid($vendorId)
        ];
        return $this->_objectFactory
            ->addData([
                'list_product' => $offers,
                'summary' => [$summary],
                'api_status' => 1,
            ]);
    }

    public function getVendorOfferDetails($offerId)
    {
        $offer = $this->_magenestVendorHelper->getVendorOfferDetails($offerId);
        $productId = $offer['product_id'];
        $vendorId = $offer['vendor_id'];
        $options["all_time"] = $this->_magenestVendorHelper->getOptionsOffer("all_time", $productId, $vendorId);
        $options["this_month"] = $this->_magenestVendorHelper->getOptionsOffer("this_month", $productId, $vendorId);
        $options[7] = $this->_magenestVendorHelper->getOptionsOffer(7, $productId, $vendorId);
        $options[30] = $this->_magenestVendorHelper->getOptionsOffer(30, $productId, $vendorId);
        $options[90] = $this->_magenestVendorHelper->getOptionsOffer(90, $productId, $vendorId);

        $salesChart = [
            [
                'name' => 'all_time',
                'option' => $options['all_time']['option'],
                'summary' => $options['all_time']['summary']
            ],
            [
                'name' => 'last_7_days',
                'option' => $options[7]['option'],
                'summary' => $options[7]['summary']
            ],
            [
                'name' => 'last_30_days',
                'option' => $options[30]['option'],
                'summary' => $options[30]['summary']
            ],
            [
                'name' => 'last_90_days',
                'option' => $options[90]['option'],
                'summary' => $options[90]['summary']
            ]
        ];

        return $this->_objectFactory
            ->addData([
                'offer_data' => [$offer],
                'sales_chart' => $salesChart,
                'api_status' => 1,
            ]);
    }

    public function getVendorInventoryDetail($vendorId,$vendorProductId){
        $results = [];
        if ($vendorProductId != 0 && $vendorProductId != null) {
            $results['vendor_product_id'] = $vendorProductId;

            $inventoryCollections = $this->_magenestInventoryHelper->getVendorInventory($vendorId, $status = '-2', $sortby = '', $sorttype = '', $search = '', $cur_page = '', $size = '');
            foreach ($inventoryCollections as $inventory){
                if($inventory['product_id'] == $vendorProductId){
                    $results['product_id'] = $inventory['product_id'];
                    $results['status'] = $inventory['status'];
                    $results['product_name'] = $inventory['name'];
                    $results['sku'] = $inventory['sku'];
                    $results['quantity'] = $inventory['qty'];
                    $results['selling_price'] = ($inventory['special_price'])*1;
                    $results['price'] = $inventory['price'];
                    $results['special_price'] = $inventory['special_price'];
                    $results['offer_note'] = $inventory['offer_note'];
                    $results['handling_time'] = $inventory['handling_time'];
                    $results['payment_chart'] = [
                        $this->_magenestVendorHelper->getInventoryPaymentChartOption(7, $vendorId,$inventory['product_id']),
                        $this->_magenestVendorHelper->getInventoryPaymentChartOption(30, $vendorId,$inventory['product_id']),
                        $this->_magenestVendorHelper->getInventoryPaymentChartOption(90, $vendorId,$inventory['product_id']),
                        $this->_magenestVendorHelper->getInventoryPaymentChartOption('all_time', $vendorId,$inventory['product_id']),
                        $this->_magenestVendorHelper->getInventoryPaymentChartOption("this_month",  $vendorId,$inventory['product_id']),
                    ];
                    $paymentAllTime = $this->_magenestVendorHelper->getInventoryPaymentChartOption('all_time', $vendorId,$inventory['product_id']);
                    $paymentAllTimeSummarry = $paymentAllTime['summarry'];
                    // Percen
                    $percentFullfill = $paymentAllTimeSummarry['units_sold'] == 0 ? 0 : number_format($paymentAllTimeSummarry['units_fulfilled'] / $paymentAllTimeSummarry['units_sold'], 2) * 100;
                    $percentRefund = $paymentAllTimeSummarry['units_sold'] == 0 ? 0 : number_format($paymentAllTimeSummarry['returns_quantity'] / $paymentAllTimeSummarry['units_sold'], 2) * 100;
                    if($paymentAllTimeSummarry['units_sold'] == 0){
                        $results['active_inventory'] = [
                            [
                                'name' => 'qty',
                                'fulfilled' => $paymentAllTimeSummarry['units_fulfilled'],
                                'units_sold' => $paymentAllTimeSummarry['units_sold'],
                                'refunded' => $paymentAllTimeSummarry['returns_quantity']
                            ],
                            [
                                'name' => 'percen',
                                'fulfilled' => 0,
                                'units_sold' => 0 ,
                                'refunded' => 0
                            ]
                        ];
                    }else{
                        $results['active_inventory'] = [
                            [
                                'name' => 'qty',
                                'fulfilled' => $paymentAllTimeSummarry['units_fulfilled'],
                                'units_sold' => $paymentAllTimeSummarry['units_sold'],
                                'refunded' => $paymentAllTimeSummarry['returns_value']
                            ],
                            [
                                'name' => 'percen',
                                'fulfilled' => $percentFullfill,
                                'units_sold' => 100 - $percentRefund - $percentRefund ,
                                'refunded' => $percentRefund
                            ]
                        ];
                    }

                }
            }
            return $this->_objectFactory->addData($results);
        }
        return $results;
    }

    public function getPaymentsForVendor($vendorId,$statementpage,$statementsize,$payoutpage,$payoutsize){
        $summary = [
            'available_balance' => $this->_magenestVendorHelper->getEarning($vendorId),
            'pending_balance' => $this->_magenestVendorHelper->getPendingBalance($vendorId),
            'deductible_fees' => $this->_magenestVendorHelper->getPaid($vendorId)
        ];
        $statementCollection = $this->getStatementCollection($vendorId,$statementpage,$statementsize);
        $payoutCollection = $this->getPayoutCollection($vendorId,$payoutpage,$payoutsize);
        $statementData = [];
        foreach ($statementCollection as $statement){
            $data = [
                'statement_id' => $statement->getData('statement_id'),
                'statement_period' => $statement->getData('statement_id'),
                'no_order' => number_format($statement->getData('total_orders'), 0),
                'total_payment' => number_format($statement->getData('total_payment'), 2),
                'total_paid' => number_format($statement->getData('total_paid'), 2),
                'total_due' => number_format($statement->getData('total_due'), 2),
                'email_sent' => 1,
                'created_at' => date('d/m/Y \a\t h:i A', strtotime($statement->getData('created_at')))
            ];
            $statementData[] = $data;
        }
        $payoutData = [];
        foreach ($payoutCollection as $payout){
            $data = [
                'payout_id' => $payout->getData('payout_id'),
                'statement_id' => $payout->getData('statement_id'),
                'transaction_id' => $payout->getData('transaction_id'),
                'total_orders' => $payout->getData('total_orders'),
                'total_payout' => number_format($payout->getData('total_payout'),2),
                'total_paid' => number_format($payout->getData('total_paid'),2),
                'total_refund' => number_format($payout->getData('total_refund'),2),
                'total_reversed' => number_format($payout->getData('total_reversed'),2),
                'created_at' => date('d/m/Y \a\t h:i A',strtotime($payout->getData('created_at')))
            ];
            $payoutData[] = $data;
        }
        return $this->_objectFactory
            ->addData([
                'summary' => [$summary],
                'statement_collection' => $statementData,
                'payout_collection' => $payoutData
            ]);
    }
    public function getStatementCollection($vendorId,$page,$size){
        if (is_numeric($page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        $statementCollection = $this->statement->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId)->setOrder('created_at', 'DES');
        if($hasDivided){
            $statementCollection->setPageSize($size)->setCurPage($page);
        }
        return $statementCollection;
    }
    public function getPayoutCollection($vendorId,$page,$size){
        if (is_numeric($page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        $payoutCollection = $this->payout->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId)->setOrder('created_at', 'DES');
        if($hasDivided){
            $payoutCollection->setPageSize($size)->setCurPage($page);
        }
        return $payoutCollection;
    }

    public function getVendorProfile($vendorId){
        $store = $this->_storeManager->getStore();
        $vendor = $this->_vendorFactory->create()->load($vendorId);
        $generalInfo = [
            ['label' => 'vendor_name', 'value' => $vendor->getVendorName()],
            ['label' => 'business_category', 'value' => $vendor->getBusinessCategory()],
            ['label' =>'website', 'value' => $vendor->getWebsite()],
            ['label' => 'business_des', 'value' => $vendor->getBusinessDes()]
        ];
        $locationArr = [];
        $locations = $this->_magenestVendorHelper->getVendorLocation($vendor->getId());
        foreach ($locations as $location){
            $locationArr[] = [
                'location_name' => $location->getData('location_name') ? $location->getData('location_name') : '',
                'street' => $location->getData('street') ? $location->getData('street') : '',
                'street_two' => $location->getData('street_two') ? $location->getData('location_name') : '',
                'city' => $location->getData('city') ? $location->getData('city') : '',
                'region' => $location->getData('region') ? $location->getData('region') : '',
                'postcode' => $location->getData('postcode') ? $location->getData('postcode') : '',
                'country' => $location->getData('country') ? $location->getData('country') : ''
            ];
        }
        $deliverieArr = [];
        $deliveries = $this->_magenestVendorHelper->getVendorDelivery($vendor->getId());
        foreach ($deliveries as $delivery){
            $deliverieArr[] = [
                'delivery_id' => $delivery->getData('delivery_id'),
                'address' => $delivery->getData('address'),
                'distance' => $delivery->getData('distance')
            ];
        }
        $pointCotact = [
            ['label' => 'firstname', 'value' => $vendor->getFirstname()],
            ['label' => 'lastname', 'value' => $vendor->getLastname()],
            ['label' => 'email', 'value' => $vendor->getEmail()],
            ['label' => 'telephone', 'value' => $vendor->getTelephone()],
            ['label' => 'business_role', 'value' => $vendor->getBusinessRole()]
        ];
        $countries = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Directory\Model\Config\Source\CountryFactory')->create()->toOptionArray();
        unset($countries[0]);
        $countryCode = $vendor->getCountryId();
        $countryLabel = '';
        foreach ($countries as $_country) {
            if ($_country['value'] == $countryCode)
                $countryLabel = $_country['label'];
        }
        $mailingAddress = [
            ['label' => 'street', 'value' => $vendor->getStreet()],
            ['label' => 'city', 'value' => $vendor->getCity()],
            ['label' => 'country', 'value' => $countryLabel],
            ['label' => 'region', 'value' => $vendor->getRegion()],
            ['label' => 'zip', 'value' => $vendor->getZip()],
            ['label' => 'telephone', 'value' => $vendor->getTelephone()]
        ];

        $paymentDetail = [
            ['label' => 'bank_name', 'value' => $vendor->getBankName()],
            ['label' => 'bank_swiftcode', 'value' => $vendor->getBankSwiftcode()],
            ['label' => 'bank_iban', 'value' =>  $vendor->getBankIban()],
            ['label' => 'bank_account_name', 'value' => $vendor->getBankAccountName()],
            ['label' => 'legal_business_name', 'value' => $vendor->getLegalBusinessName()],
            ['label' => 'tax_number', 'value' => $vendor->getTaxNumber()]
        ];
        $businessImage = $productImage = $uploadImage =  [];
        $campaignDetails = $this->_magenestVendorHelper->getDeal($vendor->getId(),$status = 'all',$cur_page = '',$size = '',$sortField = '',$sortOrder = '');
        $campaignImages = $this->_magenestVendorHelper->getCampaignImages($campaignDetails);
        foreach ($campaignImages as $image){
            $productImage[] = $image;
        }
        $campaignUploadImage = $this->_magenestVendorHelper->getUploadedImages($vendorId);
        $uploadImage = array_diff($campaignUploadImage, $productImage);
//        $businessImage['product_image'] = empty($productImage)?null:$productImage;
//        $businessImage['upload_image'] = empty($uploadImage)?null:$uploadImage;
        $businessImage = $this->_objectFactory->addData([
            'product_image' => empty($productImage)?null:$productImage,
            'upload_image' => empty($uploadImage)?null:$uploadImage
        ]);
        $documents = $this->_magenestVendorHelper->getVendorUploadedDocuments($vendor->getId());
        return $this->_objectFactory
            ->addData([
                'vendor_name' => $vendor->getVendorName(),
                'promote_product' => $vendor->getPromoteProduct(),
                'privacy_policy' => $this->_scopeConfig->getValue('vendorapi/static/privacy_policy', 'stores', $store),
                'license' => $this->_scopeConfig->getValue('vendorapi/static/license', 'stores', $store),
                'general_info' => $generalInfo,
                'location_info' => $locationArr,
                'deliverie_info' => $deliverieArr,
                'contact_info' => $pointCotact,
                'mailing_address' => $mailingAddress,
                'payment' => $paymentDetail,
                'business_image' => $businessImage,
                'document' => $documents
            ]);
    }

    public function getCampaignBuyOptionDetail($vendorId,$product_id,$option_id){
//        $productId = isset($requestParams['product_id']) ? $requestParams['product_id'] : '';
        $dealProductId = DataHelper::getChildIds($product_id);
        $results = [];
        $results['activity_chart'] = [];
        $qtyAvailable = $this->_magenestVendorHelper->getQtyAvailable($option_id);
        $qtyRedeemed = $this->_magenestVendorHelper->getQtyRedeemed($option_id);
        $qtyRefunded = $this->_magenestVendorHelper->getQtyRefunded($option_id);

        $totalCoupon = intval($qtyRedeemed) + intval($qtyAvailable) + intval($qtyRefunded);
        if ($totalCoupon == 0) {
            $availablePercent = $redeemedPercent = $refundedPercent = 0;
        } else {
            $availablePercent = number_format((intval($qtyAvailable) / $totalCoupon) * 100, 0, '.', '');
            $refundedPercent = number_format((intval($qtyRefunded) / $totalCoupon) * 100, 0, '.', '');
            $redeemedPercent = number_format(100 - $availablePercent - $refundedPercent, 0, '.', '');
        }
        $results['activity_chart'] = [
            [
                'name' => 'qty',
                'available' => $qtyAvailable,
                'redeem' => $qtyRedeemed,
                'refunded' => $qtyRefunded
            ],
            [
                'name' => 'percent',
                'available' => $availablePercent,
                'redeem' => $redeemedPercent,
                'refunded' => $refundedPercent
            ]
        ];
        $options["all_time"] = $this->_magenestVendorHelper->getOptions("all_time", $dealProductId);
        $options["this_month"] = $this->_magenestVendorHelper->getOptions("this_month", $dealProductId);
        $options[7] = $this->_magenestVendorHelper->getOptions(7, $dealProductId);
        $options[30] = $this->_magenestVendorHelper->getOptions(30, $dealProductId);
        $options[90] = $this->_magenestVendorHelper->getOptions(90, $dealProductId);

        $results['sales_chart'] = [
            [
                'name' => 'all_time',
                'option' => $options['all_time']['option'],
                'summary' => $options['all_time']['summary']
            ],
            [
                'name' => 'this_month',
                'option' => $options['this_month']['option'],
                'summary' => $options['this_month']['summary']
            ],
            [
                'name' => 'last_7_days',
                'option' => $options[7]['option'],
                'summary' => $options[7]['summary']
            ],
            [
                'name' => 'last_30_days',
                'option' => $options[30]['option'],
                'summary' => $options[30]['summary']
            ],
            [
                'name' => 'last_90_days',
                'option' => $options[90]['option'],
                'summary' => $options[90]['summary']
            ]
        ];

        $purchaseList = $this->_magenestVendorHelper->getPurchasedList($vendorId,$product_id,$option_id);
        $results['purchased_coupon_list'] = $purchasedData = [];
        foreach ($purchaseList as $groupon) {
            $purchasedData['campaign_code'] = $groupon->getGrouponCode();
            $purchasedData['customer_name'] = $groupon->getCustomerName();
            $purchasedData['product_name'] = $groupon->getProductName();
            $purchasedData['redemption_location'] = $groupon->getCustomerSpecifyLocation() . " Any " . $groupon->getCustomerSpecifyLocation();
            $purchasedData['campaign_status'] = Groupon::getStatusLabelByCode($groupon->getStatus());
            $purchasedData['purchase_date'] = $groupon->getCreatedAt();
            $purchasedData['redeem_date'] = $groupon->getRedeemedAt();
            $results['purchased_coupon_list'][] = $purchasedData;
        }
        return $this->_objectFactory->addData($results);
    }
}