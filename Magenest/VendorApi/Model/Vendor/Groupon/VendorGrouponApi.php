<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Vendor\Groupon;


/**
 * Class TicketApi
 * @package Magenest\VendorApi\Model
 */
class VendorGrouponApi implements \Magenest\VendorApi\Api\Vendor\Groupon\VendorGrouponApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\EventTicketHelper
     */
    protected $_grouponHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_couponFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * ListEventApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\GrouponHelper $grouponHelper,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory
    )
    {
        $this->_couponFactory = $grouponFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_grouponHelper = $grouponHelper;
    }


    /**
     * {@inheritdoc}
     */
    public function checkGroupon($vendorId, $grouponCode)
    {
        try {
            $grouponData  = $this->_grouponHelper->getVendorCouponDetailsByCode($vendorId, $grouponCode);
            return $this->_objectFactory->addData([
                'status' => 1,
                'groupon_data' => [$grouponData['coupon_data']],
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function redeemGroupons($vendorId, $listGroupons)
    {
        try {
            $coupons = $this->_couponFactory->create()
                ->getCollection()
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('redemption_code', ['in' => $listGroupons]);
            $redeemed = 0;
            foreach ($coupons as $coupon)
            {
                /**
                 * @var $coupon \Magenest\Groupon\Model\Groupon
                 */
                $redeem = $coupon->redeemCoupon();
                if ($redeem){
                    $redeemed++;
                }
            }
            return $this->_objectFactory->addData([
                'redeem_status' => true,
                'redeem_message' => $redeemed . ' of ' . count($listGroupons) . ' has been redeemed.',
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'redeem_message' => $e->getMessage(),
                    'api_status' => 0
                ]);
        }
    }


}
