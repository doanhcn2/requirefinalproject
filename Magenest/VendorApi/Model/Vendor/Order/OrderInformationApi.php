<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Vendor\Order;

use Magenest\VendorApi\Helper\OrderHelper;

/**
 * Class OrderInformationInterface
 * @package Magenest\VendorApi\Model\Groupon
 */
class OrderInformationApi implements \Magenest\VendorApi\Api\Vendor\Order\OrderApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\OrderHelper
     */
    protected $_orderHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var $_vendorHelper \Magenest\VendorApi\Helper\VendorComponent\VendorHepler
     */
    protected $_vendorHelper;

    /**
     * GrouponApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param OrderHelper $orderHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magenest\VendorApi\Helper\VendorComponent\VendorHepler $vendorHepler
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\OrderHelper $orderHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\VendorApi\Helper\VendorComponent\VendorHepler $vendorHepler
    ) {
        $this->_vendorHelper = $vendorHepler;
        $this->_orderHelper = $orderHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;

    }

    /**
     * {@inheritdoc}
     */
    public function getAllOrderByVendor($vendorId, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        try {
            if ($this->_vendorHelper->getVendorPromoteProduct($vendorId) !== 'simple') {
                throw new \Exception('Vendor does not have this resource.');
            }
            $filterGroup = $searchCriteria->getFilterGroups();
            foreach ($filterGroup as $filter) {
                $filterItems = $filter->getFilters();
                foreach ($filterItems as $filterItem) {
                    if ($filterItem->getConditionType() == 'like') {
                        $filterItem->setValue("%" . $filterItem->getValue() . "%");
                    }
                }
            }
            $order = $this->_orderHelper->getAllVendorOrder($vendorId, $searchCriteria);
            $order->load();
            return $this->_objectFactory->addData([
                'order_by_vendor_id' => $order->getData(),
                'total_count' => $order->getSize(),
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }

    }

    public function getVendorOrderByOrderId($order_id, $vendorId)
    {
        try {
            if ($this->_vendorHelper->getVendorPromoteProduct($vendorId) !== 'simple') {
                throw new \Exception('Vendor does not have this resource.');
            }
            $order = $this->_orderHelper->getPurchaseOrderByPOId($order_id);
            return $this->_objectFactory->addData([
                'order_by_id' => $order,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

}
