<?php
/**
 * Author: Eric Quach
 * Date: 4/20/18
 */

namespace Magenest\VendorApi\Model\Vendor\Product;

use Magenest\VendorApi\Api\Vendor\Product\VendorProductListInterface;
use Magenest\VendorApi\Helper\GrouponHelper;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\CollectionModifier;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Model\Vendor\ProductFactory as VendorProductFactory;
use \Magento\Review\Model\ReviewFactory;
use Magenest\Groupon\Helper\Data as GrouponDataHelper;


class VendorProductList implements VendorProductListInterface
{
    private   $collectionFactory;
    private   $collectionModifier;
    private   $collectionProcessor;
    private   $searchResults;
    protected $_reviewFactory;
    protected $vendorProductFactory;
    protected $eventLocationCollectionFactory;
    protected $grouponLocationCollectionFactory;
    protected $sessionCollectionFactory;
    protected $eventTicketHelper;
    /**
     * Catalog product type configurable
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;
    /**
     * Store manager
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_sellYourRatingHelper;
    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;
    protected $_reviewHelper;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        CollectionModifier $collectionModifier,
        ReviewFactory $reviewFactory,
        SearchResultsInterface $searchResults,
        VendorProductFactory $vendorProductFactory,
        \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $eventLocationCollectionFactory,
        \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory $grouponLocationCollectionFactory,
        \Magenest\Ticket\Model\ResourceModel\Session\CollectionFactory $sessionCollectionFactory,
        StoreManagerInterface $storeManager,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magenest\SellYours\Helper\RatingHelper $sellYourRatingHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $_reviewHelper
    ) {
        $this->_storeManager                    = $storeManager;
        $this->collectionFactory                = $collectionFactory;
        $this->collectionModifier               = $collectionModifier;
        $this->collectionProcessor              = $collectionProcessor;
        $this->searchResults                    = $searchResults;
        $this->_reviewFactory                   = $reviewFactory;
        $this->vendorProductFactory             = $vendorProductFactory;
        $this->eventLocationCollectionFactory   = $eventLocationCollectionFactory;
        $this->grouponLocationCollectionFactory = $grouponLocationCollectionFactory;
        $this->sessionCollectionFactory         = $sessionCollectionFactory;
        $this->eventTicketHelper                = $eventTicketHelper;
        $this->_catalogProductTypeConfigurable  = $configurable;
        $this->_sellYourRatingHelper            = $sellYourRatingHelper;
        $this->_vendorFactory                   = $vendorFactory;
        $this->_reviewHelper                    = $_reviewHelper;
    }

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria, $storeId)
    {
        $filterGroup = $searchCriteria->getFilterGroups();
        $wherePrices = [];

        foreach ($filterGroup as $filter) {
            $filterItems = $filter->getFilters();
            foreach ($filterItems as $filterItem) {
                if ($filterItem->getConditionType() == 'like') {
                    $filterItem->setValue("%" . $filterItem->getValue() . "%");
                }
                if ($filterItem->getField() === 'price') {
                    $v = $filterItem->getValue();
                    $c = $filterItem->getConditionType();
                    if ($c === 'lt') {
                        $wherePrices[] = "min_price <= $v";
                    } elseif ($c === 'gt') {
                        $wherePrices[] = "min_price >= $v";
                    }
                    $filterItem->setField('entity_id');
                    $filterItem->setConditionType('gt');
                    $filterItem->setValue('0');
                }
            }
        }

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->collectionFactory->create();
        $productCollection
            ->joinField(
                'vendor_id',
                $productCollection->getTable('udropship_vendor_product'),
                'vendor_id',
                'product_id = entity_id',
                null,
                'left'
            )
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addCategoryIds()
            ->setRowIdFieldName('vendor_product_id');
        $productCollection->getSelect()->columns('*', $productCollection::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');
        $productCollection->getSelect()->columns(['vendor_special_price' => 'special_price'], $productCollection::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');

        $this->collectionModifier->apply($productCollection);
        $this->collectionProcessor->process($searchCriteria, $productCollection);

        $items         = [];
        $itemIds       = [];
        $reviewModel   = $this->_reviewFactory->create();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource      = $objectManager->create('Magento\Config\Model\ResourceModel\Config\Data\Collection');
        $connection    = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

//        $productCollection->getSelect()->where('min_price < ?', 20.09);
        foreach ($wherePrices as $whereSql) {
            $productCollection->getSelect()->where($whereSql);
        }

        /** @var Product $item */
        foreach ($productCollection->getItems() as $item) {
            if ($item->getTypeId() == 'coupon') {
                $item = $this->getParentProduct($item);
            }
            if (in_array($item->getEntityId(), $itemIds)) continue;
            if ($item->getTypeId() == Configurable::TYPE_CODE) {
                $this->setConfigurableVendorPrice($item);
            }

            if ($item->getTypeId() === 'simple') {
                $attributeId = $item->getResource()->getAttribute('brand');
                if ($attributeId->usesSource()) {
                    $data = $attributeId->getSource()->getAttribute()->getData();
                    if ($item->getBrand()) {
                        $optionText = $attributeId->getSource()->getOptionText($item->getBrand());
                    } else {
                        $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
                    }
                } else {
                    $optionText = "";
                }
            } else {
                $optionText = false;
            }
            $reviewCollection = $this->_sellYourRatingHelper->getReviewCollection($item->getEntityId(), $isApi = false, $vendorId = null);
            $reviewIds        = [];
            foreach ($reviewCollection as $review) {
                $reviewIds[] = $review->getReviewId();
            }
            $rating        = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\View\Rating');
            $avgRate       = $rating->getAvgRate($reviewIds);
            $ratingSummary = intval($avgRate * 20);
            $ratingCount   = $reviewCollection->count();

            $price       = $item->getVendorPrice();
            $final_price = $item->getVendorSpecialPrice() != null ? $item->getVendorSpecialPrice() : $item->getVendorPrice();
            $item->setId($item->getEntityId());

            //LastChanceList Block
            $lastChanceList = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\LastChanceList');
            $is_new = $lastChanceList->getIsNew($item->getEntityId());
            $reviewModel->getEntitySummary($item, $storeId);
            $itemIds[]                  = $item->getEntityId();
            $vendorId                   = $item->getVendorId();
            $sql                        = "SELECT `vendor_name` FROM `udropship_vendor` WHERE `vendor_id` = " . $vendorId . ";";
            $result                     = $connection->fetchRow($sql);
            $vendor_name                = $result['vendor_name'];
            $itemData['id']             = $item->getEntityId();
            $itemData['image']          = $this->getProductImage($item->getImage());
            $itemData['name']           = $item->getName();
            $itemData['sku']            = $item->getSku();
            $itemData['brand']          = @$optionText ? $optionText : null;
            $itemData['free_shipping']  = $item->getFreeshipping();
            $itemData['spring_sale']    = $item->getSpringsale();
            $itemData['is_new']         = $is_new;
            $itemData['rating_summary'] = $ratingSummary;
            $itemData['rating_count']   = $ratingCount;
            $itemData['price']          = $price;
            $itemData['final_price']    = $final_price;
            $itemData['vendor_id']      = $vendorId;
            $itemData['vendor_name']    = $vendor_name;
            $itemData['vendor_sku']     = $item->getVendorSku();
            $itemData['stock_qty']      = $item->getStockQty();
            $itemData['type_id']        = $item->getTypeId();
            $items[]                    = $itemData;
        }
        $ticketIds  = $ticketItems = [];
        $grouponIds = $grouponItems = [];
        $simpleIds  = $simpleItems = [];
        foreach ($items as $item) {
            if ($item['type_id'] == 'ticket') {
                $ticketIds[]   = $item['id'];
                $ticketItems[] = $item;
            } elseif ($item['type_id'] == 'configurable' && GrouponDataHelper::isDeal($item['id'])) {
                $grouponIds[]  = $item['id'];
                $grouponItem[] = $item;
            } else {
                $simpleIds[]   = $item['id'];
                $simpleItems[] = $item;
            }
        }
        $results = [];
        if (!empty($ticketIds)) {
            foreach ($ticketItems as $item) {
                $data     = $item;
                $location = $this->eventTicketHelper->getTicketLocation($item['id']);
                if ($location != "") {
                    $data['location'] = $location['location_detail'];
                }
                $data['locations']   = $location;
                $sessionData         = \Magenest\Ticket\Helper\Event::getTicketInfo($item['id']);
                $data['event_start'] = $sessionData['first_session'];
                if (isset($sessionData['number_session']) && @$sessionData['number_session'] > 0) {
                    $data['session_qty'] = $sessionData['number_session'];
                } else {
                    $data['session_qty'] = null;
                }

                $data['week_day']    = isset($sessionData['week_day']) ? $sessionData['week_day'] : null;
                $data['day']         = $sessionData['day'];
                $data['month']       = $sessionData['month'];
                $ticketType          = $this->eventTicketHelper->getTicketType($item['id']);
                $data['ticket_type'] = $ticketType;
                /** @var  $ticketPrices \Magenest\Ticket\Controller\Product\Price */
                $ticketPrices = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Ticket\Controller\Product\Price');
                $ticketPrice = $ticketPrices->getTicketPrice($item['id']);
                $salePrice = $ticketPrice['sale_price'];
                $oriPrice = isset($ticketPrice['original_value']) ? $ticketPrice['original_value'] : 0;

                if ($ticketType == 'free') {
                    $data['price']       = number_format(0, 2);
                    $data['final_price'] = number_format(0, 2);
                } else {
                    $data['price'] = $oriPrice;
                    $data['final_price'] = $salePrice;
                }
                if (!empty($data)) {
                    $results[] = $data;
                }
            }
        }
        if (!empty($grouponIds)) {
            foreach ($grouponItem as $item) {
                $locationIds         = GrouponDataHelper::getDeal($item['id'])->getLocationIds();
                $data                = $item;
                $data['type_id']     = 'groupon';
                $childIds            = GrouponDataHelper::getChildIds($item['id']);
                $price               = $this->_reviewHelper->getPriceProductGroupon($childIds);
                $data['price']       = $price['vendor_price'];
                $data['final_price'] = $price['vendor_special_price'];
                $locationArr         = [];
                $locationIdsArr      = explode(',', $locationIds);
                foreach ($locationIdsArr as $locationId) {
                    $arr = $this->_reviewHelper->getGrouponLocationById($locationId);
                    if ($arr['location_detail'] != "" || $arr['lat_long'] != "") {
                        $locationArr[] = $arr;
                    }
                }
                if (!empty($locationIdsArr)) $data['locations'] = $locationArr;

                $data['location'] = GrouponDataHelper::getGrouponLocation($locationIds);
                if (!empty($data)) {
                    $results[] = $data;
                }
            }
        }
        if (!empty($simpleItems)) {
            foreach ($simpleItems as $item) {
                $results[] = $item;
            }
        }
        $searchResult = $this->searchResults;
//        $searchResult->setItems($results);
        $dataItems = $results;
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            if ($field == 'position') continue;
            $type      = ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC';
            $dataItems = $this->sortItems($field, $type, $results);
        }
        $searchResult->setItems($dataItems);
        $searchResult->setTotalCount($productCollection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);

        return $searchResult;
    }

    /**
     * @param Product $product
     */
    private function setConfigurableVendorPrice($product)
    {
        /** @var Collection $childProductCollection */
        $childProductCollection = $product->getTypeInstance()->getUsedProductCollection($product);
        $childProductCollection
            ->setRowIdFieldName('vendor_product_id')
            ->getSelect()
            ->joinLeft(
                ['udvp' => $childProductCollection->getTable('udropship_vendor_product')],
                'udvp.product_id = e.entity_id',
                ['*', 'vendor_special_price' => 'udvp.special_price']
            );
        $minPrice           = null;
        $lowestPriceProduct = null;
        /** @var Product $childProduct */
        foreach ($childProductCollection as $childProduct) {
            $price              = $childProduct->getVendorPrice();
            $minPrice           = $minPrice ?: $price;
            $lowestPriceProduct = $lowestPriceProduct ?: $childProduct;
            if ($price < $minPrice) {
                $lowestPriceProduct = $childProduct;
            }
        }
        if ($lowestPriceProduct->getEntityId()) {
            $product->setVendorPrice($lowestPriceProduct->getVendorPrice());
            $product->setVendorSpecialPrice($lowestPriceProduct->getVendorSpecialPrice());
        }
    }

    /**
     * @param Product $product
     *
     * @return Product $product
     */
    public function getParentProduct($product)
    {
        $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($product->getEntityId());
        $product  = $this->collectionFactory->create();
        $product->joinField(
            'vendor_id',
            $product->getTable('udropship_vendor_product'),
            'vendor_id',
            'product_id = entity_id',
            null,
            'left'
        )
            ->addAttributeToSelect('*')
            ->setStoreId(1)
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addCategoryIds()
            ->setRowIdFieldName('vendor_product_id')
            ->addFieldToFilter('entity_id', ['eq' => $parentId]);
        $product->getSelect()->columns('*', $product::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');
        $product->getSelect()->columns(['vendor_special_price' => 'special_price'], $product::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');
        foreach ($product->getItems() as $item) {
            return $item;
        }

    }

    /**
     * @return string
     */
    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl            = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    protected function getProductImage($image)
    {
        if (isset($image) && !empty($image) && $image !== "no_selection") {
            return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product/' . $image;
        } else {
            return $this->getDefaultPlaceholderImageUrl();
        }

    }

    public function sortItems($field, $type, $dataCollection)
    {
        $data = array();
        foreach ($dataCollection as $key => $row) {
            $data[$key] = $row[$field];
        }
        if ($type == 'DESC') {
            $sorttype = SORT_DESC;
        } else {
            $sorttype = SORT_ASC;
        }
        array_multisort($data, $sorttype, $dataCollection);

        return $dataCollection;
    }

}
