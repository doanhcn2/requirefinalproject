<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/10/2017
 * Time: 09:04
 */
namespace Magenest\VendorApi\Model;

/**
 *  * 12 cat
 * 12 subcat
 * 12 subcat size attr
 * 12 subcat size attr 5 value
 * 12 subcat price attr 46 to -1 value
 * 12 subcat mango brand price 12 to 46 value
 * Class Payload
 * @package Magenest\CommerceBot\Model
 */
class Payload
{
    const TYPE_FIRST_CAT = 1;

    const TYPE_SUB_CAT = 2;

    const TYPE_FILTER_ATTRIBUTE = 3;

    const TYPE_FILTER = 4;

    protected $type;

    private $label;

    private $value;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }
    public function  getLabel()
    {
        return $this->label;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setLabel($label)
    {
        $this->label =  $label;
    }
    public function setValue($value)
    {
        $this->value =  $value;
    }

}