<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\Review;

use Magenest\VendorApi\Api\CustomerReivewSortFilterRepositoryInterface;
use Magenest\VendorApi\Helper\VendorComponent\ReviewHelper;

class CustomerReivewSortFilterRepository implements CustomerReivewSortFilterRepositoryInterface{
    protected $_reviewHelper;
    public function __construct(
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $reviewHelper
    ){
        $this->_reviewHelper = $reviewHelper;
    }
    public function filterReviewByCustomerId($customerId,$requestParams){
        $review_entity = isset($requestParams['review_entity'])?$requestParams['review_entity']:10;
        $filterby = isset($requestParams['filter_by'])?$requestParams['filter_by']:'';
        $datafilter = isset($requestParams['data_filter'])?$requestParams['data_filter']:'';
        $sort = isset($requestParams['sort'])?$requestParams['sort']:[];
        $cur_page = isset($requestParams['cur_page'])?$requestParams['cur_page']:1;
        $size = isset($requestParams['size'])?$requestParams['size']:1;
        $result = $this->_reviewHelper->filterCustomerReviewByVendor($customerId,$filterby,$datafilter,$cur_page,$size,$sort,$review_entity);
        return $result;
    }
    public function getDetailReviewById($id){
        $result = $this->_reviewHelper->getDetailReviewByReviewId($id);
        return $result;
    }
}