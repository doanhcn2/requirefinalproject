<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\Review;

use Magenest\VendorApi\Api\CustomerCreateReviewVendorRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Json\EncoderInterface;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;
use Magenest\FixUnirgy\Helper\Review\Data as FUData;

class CustomerCreateReviewVendorRepository implements CustomerCreateReviewVendorRepositoryInterface {
    /**
     * @var ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var HelperData
     */
    protected $_rateHlp;
    /**
     * @var RatingFactory
     */
    protected $_ratingFactory;
    protected $_ratingOptionFactory;
    protected $_hlp;
    protected $_url;
    protected $_responseFactory;
    protected $_magenestReviewHelper;
    protected $_objectFactory;
    public function __construct(
        ReviewFactory $modelReviewFactory,
        StoreManagerInterface $modelStoreManagerInterface,
        RatingFactory $modelRatingFactory,
        Context $context,
        HelperData $helperData,
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Review\Model\Rating\OptionFactory $optionFactory,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $magenestReviewHelper
    ){
        $this->_reviewFactory = $modelReviewFactory;
        $this->_storeManager = $modelStoreManagerInterface;
        $this->_ratingFactory = $modelRatingFactory;
        $this->_hlp = $udropshipHelper;
        $this->_url = $url;
        $this->_responseFactory = $responseFactory;
        $this->_rateHlp = $helperData;
        $this->_ratingOptionFactory = $optionFactory;
        $this->_magenestReviewHelper = $magenestReviewHelper;
        $this->_objectFactory = $objectFactory;
    }

    public function createReviewVendor($customerId,$requestParams){
        $result = [];
        if(isset($requestParams)){
            $dataPost = [];
            $dataPost['rel_entity_pk_value'] = $requestParams['rel_id'];
            $dataPost['ratings'] = $requestParams['ratings'];
            $dataPost['nickname'] = $requestParams['nickname'];
            $dataPost['title'] = $requestParams['title'];
            $dataPost['detail'] = $requestParams['detail'];

            if(!empty($dataPost)){
                $review = $this->_reviewFactory->create()->setData($dataPost);
                $validate = $review->validate();
                if ($validate === true) {
                    if($requestParams['entity_id'] == 7){
                        $review->setEntityId($requestParams['entity_id'])
                            ->setEntityPkValue($requestParams['id'])
                            ->setRelEntityPkValue($requestParams['rel_id'])
                            ->setStatusId(Review::STATUS_PENDING)
                            ->setCustomerId($customerId)
                            ->setStoreId($this->_storeManager->getStore()->getId())
                            ->setStores([$this->_storeManager->getStore()->getId()])
                            ->save();
                        foreach($dataPost['ratings'] as $ratingId => $optionId) {
                            $this->_ratingFactory->create()
                                ->setRatingId($ratingId)
                                ->setReviewId($review->getId())
                                ->setCustomerId($customerId)
                                ->addOptionVote($optionId, $requestParams['id']);
                        }
                        $review->aggregate();
                        $result = true;
                    }else{
                        $product_id = $requestParams['id'];
                        $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE))
                            ->setEntityPkValue($product_id)
                            ->setStatusId(Review::STATUS_PENDING)
                            ->setCustomerId($customerId)
                            ->setStoreId($this->_storeManager->getStore()->getId())
                            ->setStores([$this->_storeManager->getStore()->getId()])
                            ->save();
                        foreach ($dataPost['ratings'] as $ratingId => $optionId) {
                            $this->_ratingFactory->create()
                                ->setRatingId($ratingId)
                                ->setReviewId($review->getId())
                                ->setCustomerId($customerId)
                                ->addOptionVote($optionId, $product_id)->save();
                        }
                        $review->aggregate();
                        $result = true;
                    }
                }else{
                    $result = false;
                }
            }else{
                $result = false;
            }
        }
        return $result;
    }
    protected function _validatePost($customerId,$requestParams){
        $id = $requestParams['id'];
        $relId = $requestParams['rel_id'];
        $shipment = $this->_hlp->createObj('Magento\Sales\Model\Order\Shipment')->load($relId);
        return !empty($id) && !empty($relId) && $shipment->getId()
            && $shipment->getUdropshipVendor()===$id && $shipment->getCustomerId()==$customerId;
    }

    public function getAllRatingByEntityId($entity_id){
        $ratingCollection = $this->_ratingFactory->create()->getCollection()->addFieldToFilter('entity_id',['eq' => $entity_id])->getItems();
        $result = [];
        foreach ($ratingCollection as $item){
            $data = [];
            $data['rating_id'] = $item->getRatingId();
            $data['rating_code'] = $item->getRatingCode();
            $data['is_active'] = $item->getIsActive();
            $data['option'] = $this->getRatingOption($item->getRatingId());
            $result[] = $data;
        }
        return $result;
    }
    public function getRatingOption($rating_id){
        $optionItem = $this->_ratingOptionFactory->create()->getCollection()->addFieldToFilter('rating_id',['eq' => $rating_id])->getItems();
        $data = [];
        foreach ($optionItem as $option){
            $data[] = $option->getData();
        }
        return $data;
    }
    public function getAllFormDataReviewVendorByCustomerId($customerId){
        $shipments = $this->_magenestReviewHelper->getReviewVendorPendingCollection($customerId)->getItems();
        $results['shipment'] = $results['virtual'] = $arrVirtual = $arrShipment = [];
        //_hlp
        $_msHlp = $this->_hlp->getObj('Unirgy\DropshipMicrosite\Helper\Data');
        $_rateHlp = $this->_hlp->getObj('Unirgy\DropshipVendorRatings\Helper\Data');
        foreach ($shipments as $shipment){
            $data = $shipment->getData();
            $arrShipment['entity_id'] = 10;
            $arrShipment['id'] = $shipment->getUdropshipVendor();
            $arrShipment['rel_id'] = $shipment->getId();
            $results['shipment'][] = $arrShipment;
        }
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $itemCollection = $_objectLoader->get('Magenest\FixUnirgy\Block\Customer\OrderLoader')->getRatingItemCollection();
        $virtualProducts = $itemCollection->getItems();
        foreach ($virtualProducts as $virtualProduct){
            $product = $virtualProduct->getProduct();
            $arrVirtual['entity_id'] = 7;
            $arrShipment['id'] = $product->getUdropshipVendor();
            $arrShipment['rel_id'] = $virtualProduct->getId();
            $results['virtual'][] = $arrShipment;
        }
        return $this->_objectFactory->addData($results);
    }
}
