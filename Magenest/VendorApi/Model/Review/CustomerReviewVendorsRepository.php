<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 09/04/2018
 * Time: 10:37
 */
namespace Magenest\VendorApi\Model\Review;
use Magenest\VendorApi\Api\CustomerReviewVendorsRepositoryInterface;
use Magenest\VendorApi\Helper\VendorComponent\ReviewHelper;

class CustomerReviewVendorsRepository implements CustomerReviewVendorsRepositoryInterface{
    /**
     * @var $_reviewHelper \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper
     */
    protected $_reviewHelper;
    public function __construct(
        ReviewHelper $reviewHelper
    ){
        $this->_reviewHelper = $reviewHelper;
    }

    public function getAllCustomerReviewVendors($customer_id,$cur_page,$size){
        try{
            $customerReviewVendor = $this->_reviewHelper->getAllCustomerReviewForVendors($customer_id,$cur_page,$size);
            return $customerReviewVendor;
        }catch (\Exception $exceptione){
            return $exceptione;
        }
    }
}