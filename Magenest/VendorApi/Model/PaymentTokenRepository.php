<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/7/18
 * Time: 3:27 PM
 */

namespace Magenest\VendorApi\Model;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class PaymentTokenRepository
 * @package Magenest\VendorApi\Model
 */
class PaymentTokenRepository implements \Magenest\VendorApi\Api\PaymentTokenRepositoryInterface
{
    /**
     * @var \Magento\Vault\Api\PaymentTokenManagementInterface
     */
    protected $vault;

    /**
     * @var \Magento\Vault\Api\PaymentTokenRepositoryInterface
     */
    protected $tokenRepo;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $objFactory;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assezRepo;

    /**
     * @var \Magento\Customer\Model\CustomerFactory $customerModelFactory
     */
    protected $customerModelFactory;

    /**
     * PaymentTokenRepository constructor.
     * @param \Magento\Vault\Api\PaymentTokenManagementInterface $paymentTokenManagement
     * @param \Magento\Vault\Api\PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param \Magento\Framework\DataObjectFactory $objectFactory
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Vault\Api\PaymentTokenManagementInterface $paymentTokenManagement,
        \Magento\Vault\Api\PaymentTokenRepositoryInterface $paymentTokenRepository,
        \Magento\Framework\DataObjectFactory $objectFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        CustomerFactory $customerFactory
    ) {
        $this->customerModelFactory = $customerFactory;
        $this->assezRepo = $assetRepo;
        $this->objFactory = $objectFactory;
        $this->vault = $paymentTokenManagement;
        $this->tokenRepo = $paymentTokenRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getListByCustomerId($customerId)
    {
        /** @var \Magento\Vault\Api\Data\PaymentTokenInterface[] $entities */
        $entities = $this->vault->getListByCustomerId($customerId);
        foreach ($entities as $entity) {
            $details = json_decode($entity->getTokenDetails(), true);
            $customer = $this->customerModelFactory->create()->load($entity->getCustomerId());
            $middleName = $customer->getMiddlename() ? $customer->getMiddlename() . ' ' : '';
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $customerName = $firstName . ' ' . $middleName . $lastName;
            if (isset($details['type'])) {
                $ccType = strtolower($details['type']);
                $details['image'] = $this->assezRepo->getUrlWithParams(
                    "Magento_Payment::images/cc/$ccType.png",
                    ['area' => 'frontend', 'theme' => 'Gosawa/gosawa']
                );
            }
            $entity->setTokenDetails($this->objFactory->create()->addData($details));
            $entity->setCustomerName($customerName);
        }
        return $entities;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($customerId, $tokenId)
    {
        $token = $this->tokenRepo->getById($tokenId);
        if ((int)$token->getCustomerId() !== (int)$customerId) {
            throw new LocalizedException(__("Can't access token"));
        }
        if (!$this->tokenRepo->delete($token)) {
            throw new LocalizedException(__("Can't remove token"));
        }
        return "OK";
    }
}
