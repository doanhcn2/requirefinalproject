<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:19
 */

namespace Magenest\VendorApi\Model;

use Magenest\VendorApi\Api\ProductApiInterface;
use Magenest\VendorApi\Helper\VendorComponent\ReviewHelper;
use Unirgy\SimpleLicense\Exception;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute;
use Magento\ConfigurableProduct\Helper\Data as CoreData;
use Unirgy\Dropship\Model\VendorFactory;
use Magento\Review\Model\RatingFactory;
use Unirgy\DropshipVendorRatings\Helper\Data;
use Magenest\VendorApi\Helper\ProductHelper;

/**
 * Class ProductApi
 * @package Magenest\VendorApi\Model
 */
class ProductApi implements ProductApiInterface
{
    const XML_PATH_VIRTUAL_GIFTCARD_PRODUCT = 'giftcard/default_product/physical';
    const XML_PATH_PHYSICAL_GIFTCARD_PRODUCT = 'giftcard/default_product/virtual';

    /**
     * @var \Magento\Backend\App\ConfigInterface
     */
    protected $_storeConfig;

    /*** @var \Magenest\VendorApi\Helper\ProductHelper */
    protected $_magenestProductHelper;

    /**
     * @var $_reviewHelper ReviewHelper
     */
    protected $_reviewHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    protected $_productResultFactory;

    /** @var ProductFactory */
    protected $productFactory;

    /** @var \Magento\ConfigurableProduct\Helper\Data $imageHelper */
    protected $helper;

    /** @var $_vendorFactory VendorFactory */
    protected $_vendorFactory;

    /** @var $_ratingFactory RatingFactory */
    protected $_ratingFactory;

    /** @var $_ratingHelper Data */
    protected $_ratingHelper;

    /**
     * Product Api Constructor.
     *
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\ProductHelper $magenestProductHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param ReviewHelper $reviewHelper
     * @param \Magento\Backend\App\ConfigInterface $config
     * @param ProductFactory $productFactory
     * @param CoreData $helper
     * @param VendorFactory $vendorFactory
     * @param RatingFactory $ratingFactory
     * @param Data $helperData
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        ProductHelper $magenestProductHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ReviewHelper $reviewHelper,
        \Magento\Backend\App\ConfigInterface $config,
        ProductFactory $productFactory,
        CoreData $helper,
        VendorFactory $vendorFactory,
        RatingFactory $ratingFactory,
        Data $helperData
    )
    {
        $this->_storeConfig = $config;
        $this->_reviewHelper = $reviewHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_magenestProductHelper = $magenestProductHelper;
        $this->productFactory = $productFactory;
        $this->helper = $helper;
        $this->_vendorFactory = $vendorFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_ratingHelper = $helperData;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct($store, $id)
    {
        try {
            $productInfo = $this->_magenestProductHelper->getProductFullDetail($store, $id);
            return $this->_objectFactory
                ->addData([
                    'result' => $productInfo,
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProductReviewById($store, $productId)
    {
        try {
            $productInfo = $this->_reviewHelper->getProductReviewById($store, $productId);
            return $this->_objectFactory
                ->addData([
                    'rating_average' => $productInfo['rating_average'],
                    'rating_count' => $productInfo['rating_count'],
                    'rating_details' => $productInfo['rating_details'],
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getGiftCardProductPhysical()
    {
        $productId = $this->_storeConfig->getValue(self::XML_PATH_PHYSICAL_GIFTCARD_PRODUCT);
        try {
            if ($productId === null) {
                throw new Exception('Could not found configuration.');
            }
            $productInfo = $this->_magenestProductHelper->getProductFullDetail(0, $productId);
            return $this->_objectFactory
                ->addData([
                    'result' => $productInfo,
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getGiftCardProductVirtual()
    {
        $productId = $this->_storeConfig->getValue(self::XML_PATH_VIRTUAL_GIFTCARD_PRODUCT);
        try {
            if ($productId === null) {
                throw new Exception('Could not found configuration.');
            }
            $productInfo = $this->_magenestProductHelper->getProductFullDetail(0, $productId);
            return $this->_objectFactory
                ->addData([
                    'result' => $productInfo,
                    'api_status' => 1
                ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    public function checkProductExitWishlish($customerId, $productId)
    {
        $result = $this->_magenestProductHelper->getMyWishlistByProductId($customerId, $productId);
        if (is_array($result) && !empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorListFromConfigurableProduct($productId, $storeId)
    {
        $product = $this->productFactory->create()
            ->setStoreId($storeId)
            ->load($productId);

        if ($product->getId()
            && $product->getTypeId() == Configurable::TYPE_CODE
        ) {
            $attributes = [];
            $options = $this->helper->getOptions($product, $this->_magenestProductHelper->getAllowProducts($product));

            foreach ($product->getTypeInstance()->getConfigurableAttributes($product) as $attribute) {
                $attributeOptionsData = $this->_magenestProductHelper->getAttributeOptionsData($attribute, $options);
                if ($attributeOptionsData) {
                    $productAttribute = $attribute->getProductAttribute();
                    $attributeCode = $productAttribute->getAttributeCode();
                    $attributes[$attributeCode] = [
                        'id' => $productAttribute->getId(),
                        'code' => $attributeCode,
                        'label' => $productAttribute->getStoreLabel($product->getStoreId()),
                        'options' => $attributeOptionsData
                    ];
                }
            }
            return $this->_objectFactory
                ->addData([
                    'result' => $attributes,
                    'api_status' => 1
                ]);
        }

        return $this->_objectFactory
            ->addData([
                'api_status' => 0
            ]);
    }
}
