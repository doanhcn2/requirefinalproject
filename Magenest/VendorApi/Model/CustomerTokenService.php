<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:58
 */

namespace Magenest\VendorApi\Model;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Checkout\Model\CartFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Integration\Model\CredentialsValidator;
use Magento\Integration\Model\Oauth\Token as Token;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory as TokenCollectionFactory;
use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Framework\Exception\AuthenticationException;
use Magenest\VendorApi\Plugin\AccessChangeQuoteControl\Disable;
use Magento\Framework\Oauth\Helper\Oauth as OauthHelper;

/**
 * Class HomeApi
 * @package Magenest\Eplus\Model
 */
class CustomerTokenService implements \Magenest\VendorApi\Api\CustomerTokenServiceInterface
{
    /**
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    /**
     * Customer Account Service
     *
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @var \Magento\Integration\Model\CredentialsValidator
     */
    private $validatorHelper;

    /**
     * Token Collection Factory
     *
     * @var TokenCollectionFactory
     */
    private $tokenModelCollectionFactory;

    /**
     * @var RequestThrottler
     */
    private $requestThrottler;

    protected $_rewardInstance;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var \Magenest\VendorApi\Api\RewardPoint\RewardPointManagementInterface
     */
    protected $rewardPointManagement;

    /**
     * @var \Magento\CustomerBalance\Model\BalanceFactory
     */
    protected $_balanceFactory;

    /**
     * @var \Magenest\SocialLogin\Helper\SocialLogin
     */
    protected $helper;

    /**
     * @var \Magento\Reward\Model\RewardFactory
     */
    protected $_rewardFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;
    protected $_oauthHelper;
    protected $_scopeConfig;
    protected $_magenestVendorHelpler;
    /**
     * CustomerTokenService constructor.
     * @param \Magenest\VendorApi\Api\RewardPoint\RewardPointManagementInterface $rewardPointManagement
     * @param \Magento\CustomerBalance\Model\BalanceFactory $balanceFactory
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagement
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param TokenCollectionFactory $tokenModelCollectionFactory
     * @param \Magento\Reward\Model\RewardFactory $rewardFactory
     * @param \Magenest\SocialLogin\Helper\SocialLogin $helper
     * @param AccountManagementInterface $accountManagement
     * @param \Magento\Framework\DataObject $objectFactory
     * @param CredentialsValidator $validatorHelper
     * @param TokenModelFactory $tokenModelFactory
     * @param Registry $registry
     */
    public function __construct(
        \Magenest\VendorApi\Api\RewardPoint\RewardPointManagementInterface $rewardPointManagement,
        \Magento\CustomerBalance\Model\BalanceFactory $balanceFactory,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        TokenCollectionFactory $tokenModelCollectionFactory,
        \Magento\Reward\Model\RewardFactory $rewardFactory,
        \Magenest\SocialLogin\Helper\SocialLogin $helper,
        AccountManagementInterface $accountManagement,
        \Magento\Framework\DataObject $objectFactory,
        CredentialsValidator $validatorHelper,
        TokenModelFactory $tokenModelFactory,
        Registry $registry,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        OauthHelper $oauthHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\VendorApi\Helper\VendorComponent\VendorHepler $magenestVendorHelpler
    ) {
        $this->tokenModelCollectionFactory = $tokenModelCollectionFactory;
        $this->rewardPointManagement = $rewardPointManagement;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->accountManagement = $accountManagement;
        $this->validatorHelper = $validatorHelper;
        $this->_balanceFactory = $balanceFactory;
        $this->cartManagement = $cartManagement;
        $this->cartRepository = $cartRepository;
        $this->_objectFactory = $objectFactory;
        $this->_rewardFactory = $rewardFactory;
        $this->_storeManager = $storeManager;
        $this->registry = $registry;
        $this->helper = $helper;
        $this->_vendorFactory = $vendorFactory;
        $this->_oauthHelper = $oauthHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_magenestVendorHelpler = $magenestVendorHelpler;
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerAccessToken($username, $password)
    {
        $this->validatorHelper->validate($username, $password);
        try {
            $customer = $this->accountManagement->authenticate($username, $password);
        } catch (\Exception $e) {
            $this->getRequestThrottler()->logAuthenticationFailure($username, RequestThrottler::USER_TYPE_CUSTOMER);
            throw new AuthenticationException(
                __('You did not sign in correctly or your account is temporarily disabled.')
            );
        }
        $customerId = $customer->getId();
        $this->getRequestThrottler()->resetAuthenticationFailuresCount($username, RequestThrottler::USER_TYPE_CUSTOMER);
        $token = $this->tokenModelFactory->create()->createCustomerToken($customerId)->getToken();
//        $cart = $this->cartFactory->create()->load(27595);
        try {
            $cart = $this->cartRepository->getForCustomer($customerId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $this->registry->register(Disable::DISABLE_REGISTRY, true);
            $cartId = $this->cartManagement->createEmptyCartForCustomer($customerId);
            $this->registry->unregister(Disable::DISABLE_REGISTRY);
            $cart = $this->cartRepository->get($cartId);
        }
        $itemQty = $cart->getItemsQty();
        $cartId = $cart->getId();
//        $rewardPoint = $this->rewardPointManagement->getRewardPointForCustomer($customerId)->getResult();
//        $balance = 0;
//        foreach ($rewardPoint as $r) {
//            $balance = $r['balance'];
//        }

        $customer = $this->helper->getCustomerByEmail($customer->getEmail());
        $this->_rewardInstance = $this->_rewardFactory->create()->setCustomer(
            $customer
        )->setWebsiteId(
            $this->_storeManager->getWebsite()->getId()
        )->loadByCustomer();
        $balance = $this->_rewardInstance->getPointsBalance();


        $collection = $this->_balanceFactory->create()->getCollection()->addFieldToFilter(
            'customer_id',
            $customerId
        );

        $storeCreditTotal = $collection->getFirstItem();


//        $data = $customer->getAddresses();
//        $addresses = [];
//        foreach ($data as $item) {
//            $addresses[] = [
//                "city" => $item->getCity(),
//                "street" => $item->getStreet(),
//                "regionid" => $item->getRegionId(),
//                "countryid" => $item->getCountryId(),
//                "telephone" => $item->getTelephone(),
//                "postcode" => $item->getPostcode()
//            ];
//
//        }
//        $customerData[] = [
//            'Email' => $customer->getEmail(),
//            'Gender' => $customer->getGender(),
//            'First Name' => $customer->getFirstname(),
//            'Last Name' => $customer->getLastname(),
//            'Address' => $addresses,
//        ];


        return $this->_objectFactory
            ->addData([
                'access_token' => $token,
                'cart_item_qty' => $itemQty,
                'reward_point_balance' => $balance,
                'store_credit_total' => $storeCreditTotal->getAmount(),
                'customer_info' => $customer->getDataModel(),
                'cart_id' => $cartId
            ]);
    }

    /**
     * Get request throttler instance
     *
     * @return RequestThrottler
     * @deprecated 100.0.4
     */
    private function getRequestThrottler()
    {
        if (!$this->requestThrottler instanceof RequestThrottler) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(RequestThrottler::class);
        }
        return $this->requestThrottler;
    }

    public function createVendorAccessToken($username, $password){
        $this->validatorHelper->validate($username, $password);
        $this->getRequestThrottler()->throttle($username, 3);
        try{
            /**
             * @var $vendor \Unirgy\Dropship\Model\Vendor
             */
            $vendor = $this->_vendorFactory->create();
            $vendorResult = $vendor->authenticate($username, $password);
            if (!$vendorResult) {
                throw new Exception();
            }
        }catch (\Exception $exception){
            $this->getRequestThrottler()->logAuthenticationFailure($username, 3);
            throw new AuthenticationException(
                __('You did not sign in correctly or your account is temporarily disabled.')
            );
        }
        $this->getRequestThrottler()->resetAuthenticationFailuresCount($username, 3);
        $token = $this->tokenModelFactory->create();
        $token->setVendorId($vendor->getId());
        $token->setUserType(5);
        $token->setType(Token::TYPE_ACCESS);
        $token->setToken($this->_oauthHelper->generateToken());
        $token->setSecret($this->_oauthHelper->generateTokenSecret());
        $token->save();
        $vendorToken = $token->getToken();
        $store = $this->_storeManager->getStore();
        $generalInfo = [
            ['label' => 'vendor_name', 'value' => $vendor->getVendorName()],
            ['label' => 'business_category', 'value' => $vendor->getBusinessCategory()],
            ['label' =>'website', 'value' => $vendor->getWebsite()],
            ['label' => 'business_des', 'value' => $vendor->getBusinessDes()]
        ];
        $locationArr = [];
        $locations = $this->_magenestVendorHelpler->getVendorLocation($vendor->getId());
        foreach ($locations as $location){
            $locationArr[] = [
                'location_name' => $location->getData('location_name') ? $location->getData('location_name') : '',
                'street' => $location->getData('street') ? $location->getData('street') : '',
                'street_two' => $location->getData('street_two') ? $location->getData('location_name') : '',
                'city' => $location->getData('city') ? $location->getData('city') : '',
                'region' => $location->getData('region') ? $location->getData('region') : '',
                'postcode' => $location->getData('postcode') ? $location->getData('postcode') : '',
                'country' => $location->getData('country') ? $location->getData('country') : ''
            ];
        }
        $deliverieArr = [];
        $deliveries = $this->_magenestVendorHelpler->getVendorDelivery($vendor->getId());
        foreach ($deliveries as $delivery){
            $deliverieArr[] = [
                'delivery_id' => $delivery->getData('delivery_id'),
                'address' => $delivery->getData('address'),
                'distance' => $delivery->getData('distance')
            ];
        }
        $pointCotact = [
            ['label' => 'firstname', 'value' => $vendor->getFirstname()],
            ['label' => 'lastname', 'value' => $vendor->getLastname()],
            ['label' => 'email', 'value' => $vendor->getEmail()],
            ['label' => 'telephone', 'value' => $vendor->getTelephone()],
            ['label' => 'business_role', 'value' => $vendor->getBusinessRole()]
        ];
        $countries = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Directory\Model\Config\Source\CountryFactory')->create()->toOptionArray();
        unset($countries[0]);
        $countryCode = $vendor->getCountryId();
        $countryLabel = '';
        foreach ($countries as $_country) {
            if ($_country['value'] == $countryCode)
                $countryLabel = $_country['label'];
        }
        $mailingAddress = [
            ['label' => 'street', 'value' => $vendor->getStreet()],
            ['label' => 'city', 'value' => $vendor->getCity()],
            ['label' => 'country', 'value' => $countryLabel],
            ['label' => 'region', 'value' => $vendor->getRegion()],
            ['label' => 'zip', 'value' => $vendor->getZip()],
            ['label' => 'telephone', 'value' => $vendor->getTelephone()]
        ];

        $paymentDetail = [
            ['label' => 'bank_name', 'value' => $vendor->getBankName()],
            ['label' => 'bank_swiftcode', 'value' => $vendor->getBankSwiftcode()],
            ['label' => 'bank_iban', 'value' =>  $vendor->getBankIban()],
            ['label' => 'bank_account_name', 'value' => $vendor->getBankAccountName()],
            ['label' => 'legal_business_name', 'value' => $vendor->getLegalBusinessName()],
            ['label' => 'tax_number', 'value' => $vendor->getTaxNumber()]
        ];
        $businessImage = [];
        $campaignDetails = $this->_magenestVendorHelpler->getDeal($vendor->getId(),$status = 'all',$cur_page = '',$size = '',$sortField = '',$sortOrder = '');
        $campaignImages = $this->_magenestVendorHelpler->getCampaignImages($campaignDetails);
        foreach ($campaignImages as $image){
            $businessImage[] = $image;
        }
        $documents = $this->_magenestVendorHelpler->getVendorUploadedDocuments($vendor->getId());


        return $this->_objectFactory
            ->addData([
                'access_token' => $vendorToken,
                'vendor_name' => $vendor->getVendorName(),
                'promote_product' => $vendor->getPromoteProduct(),
                'privacy_policy' => $this->_scopeConfig->getValue('vendorapi/static/privacy_policy', 'stores', $store),
                'license' => $this->_scopeConfig->getValue('vendorapi/static/license', 'stores', $store),
                'general_info' => $generalInfo,
                'location_info' => $locationArr,
                'deliverie_info' => $deliverieArr,
                'contact_info' => $pointCotact,
                'mailing_address' => $mailingAddress,
                'payment' => $paymentDetail,
                'business_image' => $businessImage,
                'document' => $documents
            ]);
    }

    public function createAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $password = null,
        $redirectUrl = ''
    ){
        $customers = $this->accountManagement->createAccount($customer,$password,$redirectUrl);
        return $this->createCustomerAccessToken($customers->getEmail(),$password);
    }
}
