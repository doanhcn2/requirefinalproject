<?php
/**
 * Contributor company: iPragmatech solution Pvt Ltd.
 * Contributor Author : Manish Kumar
 * Date: 23/5/16
 * Time: 11:55 AM
 */

namespace Magenest\VendorApi\Model\Category;

use Magenest\VendorApi\Api\Category\PageBuilderManagementInterface;
use Magento\Framework\DataObject;
use Ves\PageBuilder\Model\BlockFactory;
use Ves\PageBuilder\Model\ResourceModel\Block\CollectionFactory;

/**
 * Defines the implementaiton class of the PageBuilderManagementInterface
 */
class PageBuilderManagement implements PageBuilderManagementInterface
{
    protected $_pageBuilderCollectionFactory;

    protected $_pageBuilderFactory;

    protected $_objectFactory;

    protected $storeManagerInterFace;
    protected $newWidget;

    /**
     * PageBuilderManagement constructor.
     * @param DataObject $objectFactory
     * @param BlockFactory $pageBuilderFactory
     * @param CollectionFactory $pageBuilderCollectionFactory
     */
    public function __construct(
        DataObject $objectFactory,
        BlockFactory $pageBuilderFactory,
        CollectionFactory $pageBuilderCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterFace,
        \Magento\Catalog\Block\Product\Widget\NewWidget $newWidget
    )
    {
        $this->_objectFactory = $objectFactory;
        $this->_pageBuilderFactory = $pageBuilderFactory;
        $this->_pageBuilderCollectionFactory = $pageBuilderCollectionFactory;
        $this->storeManagerInterFace = $storeManagerInterFace;
        $this->newWidget = $newWidget;
    }

    /**
     * {@inheritdoc}
     */
    public function getPageBuilderByCategoryId($categoryId)
    {
//        $collection = $this->_pageBuilderFacage
        $category_header_model = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\CategoryHeader\Model\RuleFactory')->create();
        $blockHeader = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\CategoryHeader\Block\CategoryHeader');
        $_dataHeaders = $category_header_model->getCollection()
            ->addFieldToFilter('category', $categoryId)
            ->setOrder('priority','asc');
        $categoryHeaders = $this->getDataHeader($_dataHeaders, $blockHeader);
        $page = $this->_objectFactory->addData([
            'page' => $categoryHeaders
        ]);

        //Magento\Widget\Model\Widget\Instance
        $instanceFactory = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Widget\Model\Widget\InstanceFactory')->create();
        $joinConditions = 'main_table.instance_id = widget_instance_page.instance_id';
        /** @var \Magento\Widget\Model\ResourceModel\Widget\Instance\Collection $collection */
        $collection = $instanceFactory->getCollection();
        $collection->addFieldToSelect('*');
        $collection->getSelect()->join(
            ['widget_instance_page' => 'widget_instance_page'],
            $joinConditions,
            []//)->w
        )->columns("widget_instance_page.block_reference")
            ->where("widget_instance_page.entities IN (?)", 4);
//        ->group('main_table.instance_id');//,['eq'=>$categoryId]);

//            ->where("store_price.store_id=1");
        foreach ($collection as $co){
            $y = 1;
        }
        return $page;
    }
    public function getDataHeader($_dataHeaders, $blockHeader){
        $rawData = [];
        /** @var $blockHeader \Magenest\CategoryHeader\Block\CategoryHeader */
        $banners = [
            'type' => 'banners',
            'items_number' =>2,
            'items' => [
                '0' => [
                    'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/productcategory.jpg',
                    'url' => '#'
                ],
                '1' => [
                    'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/productcategory.jpg',
                    'url' => '#'
                ]
            ]
        ];
        $rawData[] = $banners;
        foreach ($_dataHeaders as $_header){
            $column = $_header->getNumber();
            $slider = $_header->getUseSlider();
            $items =  $blockHeader->collectionItem($_header->getId());
//            $info = $this->getItemInfo($items, $blockHeader);
            $data = [
                'type' => 'product_list',
                'title' => $_header->getTitle(),
                'products' => $this->getItemInfo($items, $blockHeader)
            ];
            $rawData[] = $data;
        }

        $images = [
            'type' => 'images',
            'columns_number' => 2,
            'rows_number' => 1,
            'columns' => [
                '0' => [
                    'column' => 1,
                    'items' => [
                        '0' => [
                            'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/linkimg1.jpg',
                            'url' => '#',
                            'h3' => 'Home & Kitchen',
                            'text' => 'The best of home decor and kitchenware'
                        ]
                    ]
                ],
                '1' => [
                    'column' => 2,
                    'items' => [
                        '0' => [
                            'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/linkimg1.jpg',
                            'url' => '#',
                            'h3' => null,
                            'text' => null

                        ],
                        '1' => [
                            'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/linkimg1.jpg',
                            'url' => '#',
                            'h3' => null,
                            'text' => null
                        ],
                        '2' => [
                            'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/linkimg1.jpg',
                            'url' => '#',
                            'h3' => null,
                            'text' => null
                        ],
                        '3' => [
                            'src' => 'https://dev1.hungnamecommerce.com/pub/media/wysiwyg/linkimg1.jpg',
                            'url' => '#',
                            'h3' => null,
                            'text' => null
                        ]
                    ]
                ]
            ]
        ];

        $rawData[] = $images;
        return $rawData;
    }
    public function getItemInfo($items, $blockHeader){
        $rawData = [];
        /** @var $blockHeader \Magenest\CategoryHeader\Block\CategoryHeader */
        foreach ($items as $prditem){
            /** @var $info \Magento\Catalog\Model\Product */
            $info = $blockHeader->getDataItem($prditem);
            $sale = null;
            if($info->getSpecialPrice()) {
                if ($info->getPrice() > $info->getFinalPrice()) {
                    $sale = ($info->getPrice() - $info->getFinalPrice()) * 100 / $info->getPrice();
                }
            }
            $checkShipping = $this->getFreeShipping($info);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $RatingOb = $objectManager->create('Magento\Review\Model\Rating')->getEntitySummary($info->getId());
            if($RatingOb->getCount()>0) {
                $ratings = $RatingOb->getSum() / $RatingOb->getCount();
                $ratingCount = $RatingOb->getCount();
            } else{
                $ratings = 0;
                $ratingCount = 0;
            }
            $result = [
                'id' => $info->getEntityId(),
                'name' => $info->getName(),
                'image' => $blockHeader->getPathImage().'catalog/product'.$info->getImage(),
                'sku' => $info->getSku(),
                'price' => $info->getPrice(),
                'final_price' => $info->getFinalPrice(),
                'free_shipping' => $checkShipping == 1 ? 'Free Shipping' : null,
                'rating' => $ratings,
                'rating_count' => $ratingCount,
                'sale_off' => isset($sale) ? round($sale).'% off' : null
            ];
            $rawData[] = $result;
        }
        return $rawData;
    }
    public function getFreeShipping($info){
        $vendor_id = $info->getUdropshipVendor();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('udropship_vendor_product'); //gives table name with prefix
        $sql = "Select freeshipping FROM " . $tableName ." Where product_id = '" .$info->getId(). "' and vendor_id = '".$vendor_id."''";
        $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
        $checkShipping = null;
        if($result[0]['freeshipping']){
            $checkShipping = $result[0]['freeshipping'];
        }
        return $checkShipping;
    }

    public function getCategoriesInfo($categories){
        $rawData = [];
        foreach ($categories->getItems() as $category){
            $data = $category->getData();
            $rawData[] = $data;
        }
        return $rawData;
    }
}
