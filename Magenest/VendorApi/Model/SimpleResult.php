<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:03
 */
namespace Magenest\VendorApi\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class SimpleResult
 * @package Magenest\VendorApi\Model
 */
class SimpleResult extends  AbstractModel
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_vendor_simple_result';

    /**
     * Event Object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_vendor_simple_result';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\VendorApi\Model\ResourceModel\SimpleResult');
    }
}
