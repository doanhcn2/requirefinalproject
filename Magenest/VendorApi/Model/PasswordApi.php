<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 09/04/2018
 * Time: 09:36
 */

namespace Magenest\VendorApi\Model;

use Magento\Framework\Exception\LocalizedException;
use Zend\Http\Request;

class PasswordApi implements \Magenest\VendorApi\Api\PasswordInterface
{
    protected $_objectFactory;
    protected $_encryptor;

    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor)
    {
        $this->_objectFactory = $objectFactory;
        $this->_encryptor = $encryptor;
    }

    /**
     * @api
     * @param string $password
     * @return \Magenest\VendorApi\Api\Data\PasswordInterface
     */
    public function create($password)
    {
        $hash = $this->_encryptor->getHash($password, true);
        return $this->_objectFactory
            ->addData([
                'hash' => $hash
            ]);
    }
}
