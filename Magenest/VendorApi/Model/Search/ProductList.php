<?php
namespace Magenest\VendorApi\Model\Search;

use Magenest\Groupon\Helper\Data as GrouponDataHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\UrlInterface;

class ProductList implements \Magenest\VendorApi\Api\Vendor\Product\ProductListInterface{
    /**
     * @var \Magento\Framework\ObjectManagerInterface|null
     */
    protected $_objectManager = null;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $metadataService;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    protected $_reviewFactory;
    protected $_storeManager;
    protected $eventTicketHelper;
    protected $resourceModel;
    protected $stockRegistryProvider;
    protected $_categoryFactory;

    /**
     * ProductList constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $resourceModel
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataServiceInterface
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product $resourceModel,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataServiceInterface,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\CatalogInventory\Model\StockRegistry $stock,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magenest\VendorApi\Helper\EventTicketHelper $eventTicketHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ){
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->metadataService = $metadataServiceInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->resourceModel = $resourceModel;
        $this->stockRegistryProvider = $stock;
        $this->_reviewFactory = $reviewFactory;
        $this->eventTicketHelper = $eventTicketHelper;
        $this->_categoryFactory = $categoryFactory;
    }
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria, $storeId){
        $filterGroup = $searchCriteria->getFilterGroups();
        foreach ($filterGroup as $filter) {
            $filterItems = $filter->getFilters();
            foreach ($filterItems as $filterItem) {
                if ($filterItem->getConditionType() == 'like') {
                    $filterItem->setValue("%" . $filterItem->getValue() . "%");
                }
            }
        }
        $productCollection = $this->collectionFactory->create();
        $productCollection
            ->joinField(
                'vendor_id',
                $productCollection->getTable('udropship_vendor_product'),
                'vendor_id',
                'product_id = entity_id',
                null,
                'left'
            )
            ->addAttributeToSelect('*')
            ->setStoreId($storeId)
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addCategoryIds()
            ->setRowIdFieldName('vendor_product_id');
        $productCollection->getSelect()->columns('*', $productCollection::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');
        $productCollection->getSelect()->columns(['vendor_special_price' => 'special_price'], $productCollection::ATTRIBUTE_TABLE_ALIAS_PREFIX . 'vendor_id');

        //Add filters from root filter group to the collection
        $filters = [];
        foreach ($searchCriteria->getFilterGroups() as $group){
            $filters[] = $this->addFilterGroupToCollection($group, $productCollection);
        }

//        $filters = $results[0];
        foreach ($filters as $filter){
            if(!is_array($filter) || empty($filter)) continue;
            $productCollection->addFieldToFilter($filter['attribute'],[$filter['type'] => $filter['condition']]);
        }
        $productCollection->setCurPage($searchCriteria->getCurrentPage());
        $productCollection->setPageSize($searchCriteria->getPageSize());
        $productCollection->load();
//        $filterCategory = $results[1];
//        $data = $this->getFilterByCategory($filterCategory,$productCollection);
//        $x = $productCollection->getSelect()->__toString();

        $items = [];
        $itemIds = [];
        $reviewModel = $this->_reviewFactory->create();
        foreach ($productCollection->getItems() as $item) {
            if(in_array($item->getEntityId(),$itemIds)) continue;
            if ($item->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $this->setConfigurableVendorPrice($item);
            }
            if($item->getTypeId() == 'coupon') continue;
            if ($item->getTypeId() === 'simple') {
                $attributeId = $item->getResource()->getAttribute('brand');
                if ($attributeId->usesSource()) {
                    $data = $attributeId->getSource()->getAttribute()->getData();
                    if ($item->getBrand()) {
                        $optionText = $attributeId->getSource()->getOptionText($item->getBrand());
                    } else {
                        $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
                    }
                } else {
                    $optionText = "";
                }
            } else {
                $optionText = false;
            }
            $price = $item->getVendorPrice();

            $final_price = $item->getVendorSpecialPrice() != null ? $item->getVendorSpecialPrice() : $item->getVendorPrice();
            $item->setId($item->getEntityId());
            $reviewModel->getEntitySummary($item, $storeId);
            $itemIds[] = $item->getEntityId();
            $itemData['id'] = $item->getEntityId();
            $itemData['image'] = $this->getProductImage($item->getImage());
            $itemData['name'] = $item->getName();
            $itemData['sku'] = $item->getSku();
            $itemData['brand'] = @$optionText ? $optionText : null;
            $itemData['free_shipping'] = $item->getFreeshipping();
            $itemData['spring_sale'] = $item->getSpringSale();
            $itemData['is_new'] = $item->getIsNew();
            $itemData['rating_summary'] = $item->getRatingSummary() ? $item->getRatingSummary()->getRatingSummary() : null;
            $itemData['rating_count'] = $item->getRatingSummary() ? $item->getRatingSummary()->getReviewsCount() : null;
            $itemData['price'] = $price;
            $itemData['final_price'] = $final_price;
            $itemData['vendor_id'] = $item->getVendorId();
            $itemData['vendor_sku'] = $item->getVendorSku();
            $itemData['stock_qty'] = $item->getStockQty();
            $itemData['type_id'] = $item->getTypeId();
            $items[] = $itemData;
        }
        $ticketIds = $ticketItems = [];
        $grouponIds = $grouponItems = [];
        $simpleIds = $simpleItems = [];
        foreach ($items as $item){
            if($item['type_id'] == 'ticket'){
                $ticketIds[] = $item['id'];
                $ticketItems[] = $item;
            }elseif($item['type_id'] == 'configurable' && GrouponDataHelper::isDeal($item['id'])){
                $grouponIds[] = $item['id'];
                $grouponItem[] = $item;
            }else{
                $simpleIds[] = $item['id'];
                $simpleItems[] = $item;
            }
        }
        $results = [];
        if (!empty($ticketIds)) {
            foreach($ticketItems as $item) {
                $data = $item;
                $data['location'] = ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')
                    ->getTicketLocation($item['id']);
                $sessionData = \Magenest\Ticket\Helper\Event::getTicketInfo($item['id']);
                $data['event_start'] = $sessionData['first_session'];
                $data['session_qty'] = $sessionData['number_session'];
                if($this->eventTicketHelper->isFreeTicketType($item['id'])){
                    $data['price'] = number_format(0, 2);
                    $data['final_price'] = number_format(0, 2);
                }else{
                    $price = $this->eventTicketHelper->getPriceTicket($item['id'],'price');
                    if(is_array($price)){
                        $data['price'] = $price[1];
                        $data['price_from_to'] = $price[0]." - ".$price[1];
                    }else{
                        $data['price'] = $price;
                    }
                    $finalPrice = $this->eventTicketHelper->isHasSalePricce($item['id']);
                    if($finalPrice){
                        if(is_array($finalPrice)){
                            $data['final_price'] = $finalPrice[0];
                            $data['final_price_from_to'] = $finalPrice[0]." - ".$finalPrice[1];
                        }else{
                            $data['final_price'] = $finalPrice;
                        }
                    }else{
                        if(is_array($price)){
                            $data['final_price'] = $price[0];
                        }else{
                            $data['final_price'] = $price;
                        }
                    }
                }
                if (!empty($data)) {
                    $results[] = $data;
                }
            }
        }
        if (!empty($grouponIds)) {
            foreach($grouponItem as $item) {
                $locationIds = GrouponDataHelper::getDeal($item['id'])->getLocationIds();
                $data = $item;
                $data['location'] = GrouponDataHelper::getGrouponLocation($locationIds);
                if (!empty($data)) {
                    $results[] = $data;
                }
            }
        }
        if(!empty($simpleItems)){
            foreach($simpleItems as $item) {
                $results[] = $item;
            }
        }
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $dataItems = $results;
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $type = ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC';
            $dataItems = $this->sortItems($field,$type,$results);
        }
        $searchResult->setItems($dataItems);
        $searchResult->setTotalCount($productCollection->getSize());
        return $searchResult;
    }

    /**
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return array
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ){
        $fields = [];
        $categoryFilter = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            if ($filter->getField() == 'category_id') {
                $categoryFilter[$conditionType][] = $filter->getValue();
                continue;
            }
            $fields[] = ['attribute' => $filter->getField(), 'condition' => $filter->getValue(), 'type' => $conditionType];
        }
        if ($categoryFilter) {
            $collection->addCategoriesFilter($categoryFilter);
        }
        return $fields;
    }
    public function getCategoryProducts($categoryId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->create('Magento\Config\Model\ResourceModel\Config\Data\Collection');
        $connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $sql = "SELECT `entity_id` FROM `catalog_category_product` WHERE `category_id` = $categoryId";
        $count = $connection->fetchAll($sql);
        $productId = [];
        foreach ($count as $id){
            $productId[] = $id['entity_id'];
        }
        return $productId;
    }
    public function getFilterByCategory($filter,$collection){
        /** @var \Magento\Framework\Api\Search\FilterGroup $filter */
        $categoryId =  $filter->getValue();
        $productIdCategory = $this->getCategoryProducts($categoryId);
        $results = [];
        foreach ($collection as $product){
            if(in_array($product->getEntityId(),$productIdCategory)){
                $results[] = $product;
            }
        }
        return $results;
    }
    /**
     * @param Product $product
     */
    public function setConfigurableVendorPrice($product)
    {
        /** @var Collection $childProductCollection */
        $childProductCollection = $product->getTypeInstance()->getUsedProductCollection($product);
        $childProductCollection
            ->setRowIdFieldName('vendor_product_id')
            ->getSelect()
            ->joinLeft(
                ['udvp' => $childProductCollection->getTable('udropship_vendor_product')],
                'udvp.product_id = e.entity_id',
                ['*', 'vendor_special_price' => 'udvp.special_price']
            );
        $minPrice = null;
        $lowestPriceProduct = null;
        /** @var Product $childProduct */
        foreach ($childProductCollection as $childProduct) {
            $price = $childProduct->getVendorPrice();
            $minPrice = $minPrice ?:$price;
            $lowestPriceProduct = $lowestPriceProduct ?:$childProduct;
            if ($price < $minPrice) {
                $lowestPriceProduct = $childProduct;
            }
        }
        if ($lowestPriceProduct->getEntityId()) {
            $product->setVendorPrice($lowestPriceProduct->getVendorPrice());
            $product->setVendorSpecialPrice($lowestPriceProduct->getVendorSpecialPrice());
        }
    }
    public function getProductImage($image){
        if (isset($image) && !empty($image) && $image !== "no_selection") {
            return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product/' . $image;
        } else {
            return $this->getDefaultPlaceholderImageUrl();
        }
    }
    /**
     * @return string
     */
    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }
    public function sortItems($field,$type,$dataCollection){
        $data = array();
        foreach ($dataCollection as $key => $row){
            $data[$key] = $row[$field];
        }
        if($type == 'DESC'){
            $sorttype = SORT_DESC;
        }else{
            $sorttype = SORT_ASC;
        }
        array_multisort($data,$sorttype,$dataCollection);
        return $dataCollection;
    }
}