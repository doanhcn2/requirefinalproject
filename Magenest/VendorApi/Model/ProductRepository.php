<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * Class ProductRepository
 * @package Magenest\Eplus\Model
 */
class ProductRepository implements \Magenest\VendorApi\Api\ProductRepositoryInterface
{
    protected $_dataObject;

    protected $_productHelper;

    protected $collectionFactory;

    protected $collectionProcessor;

    protected $_catalogProductTypeConfigurable;

    protected $_extensionAttributesJoinProcessor;

    protected $_serializer;

    protected $_reviewFactory;

    /**
     * @var Product[]
     */
    protected $instances = [];

    /**
     * @var Product[]
     */
    protected $instancesById = [];

    protected $cacheLimit = 1000;

    /**
     * ProductRepository constructor.
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magenest\Eplus\Helper\ProductHelper $productHelper
     * @param \Magento\Framework\DataObjectFactory $dataObject
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface|null $collectionProcessor
     */
    public function __construct(
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magenest\Eplus\Helper\ProductHelper $productHelper,
        \Magento\Framework\DataObjectFactory $dataObject,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->_reviewFactory = $reviewFactory;
        $this->_extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_productHelper = $productHelper;
        $this->_dataObject = $dataObject;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->_serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getList($store, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $result = $this->_dataObject->create();
        $collection = $this->collectionFactory->create();
        $this->_extensionAttributesJoinProcessor->process($collection);
        $collection->addAttributeToSelect('*');
        $collection->joinAttribute('image', 'catalog_product/image', 'entity_id', null, 'inner');
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        $collection->joinAttribute('meta_title', 'catalog_product/meta_title', 'entity_id', null, 'left');
        $collection->joinAttribute('meta_description', 'catalog_product/meta_description', 'entity_id', null, 'left');
        $collection->joinAttribute('brand', 'catalog_product/brand', 'entity_id', null, 'left');
        $collection->joinAttribute('freeshipping', 'catalog_product/freeshipping', 'entity_id', null, 'left');
        $collection->joinAttribute('springsale', 'catalog_product/springsale', 'entity_id', null, 'left');
        $this->collectionProcessor->process($searchCriteria, $collection);
        $collection->load();
        $collection->addCategoryIds();
        $result->setTotalCount($collection->getSize());
        $items = [];
        foreach ($collection->getItems() as $item) {
            $this->_reviewFactory->create()->getEntitySummary($item, $store);
            $itemData = [];
            $itemData['id'] = $item->getId();
            $itemData['image'] = $item->getImage();
            $itemData['name'] = $item->getName();
            $itemData['sku'] = $item->getSku();
            $itemData['brand'] = $item->getBrand();
            $itemData['free_shipping'] = $item->getFreeshipping();
            $itemData['spring_sale'] = $item->getSpringSale();
            $itemData['is_new'] = $item->getIsNew();
            $itemData['rating_summary'] = $item->getRatingSummary()->getRatingSummary();
            $itemData['rating_count'] = $item->getRatingSummary()->getReviewsCount();
            $itemData['price'] = $item->getPrice();
            $itemData['final_price'] = $item->getFinalPrice();
            $items[] = $itemData;
        }
        $result->setItems($items);
        return $result;
    }

    private function getCollectionProcessor()
    {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                'Magento\Catalog\Model\Api\SearchCriteria\ProductCollectionProcessor'
            );
        }
        return $this->collectionProcessor;
    }
}
