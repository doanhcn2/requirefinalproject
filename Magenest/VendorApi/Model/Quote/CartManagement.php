<?php
/**
 * Author: Eric Quach
 * Date: 3/14/18
 */
namespace Magenest\VendorApi\Model\Quote;

use Magenest\VendorApi\Api\CartManagementInterface;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\CartFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Framework\Event\ManagerInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;

class CartManagement implements CartManagementInterface
{
    protected $_customerSession;
    protected $_checkoutSession;
    protected $_cartFactory;
    protected $_cartRepository;
    protected $_productRepository;
    protected $eventManager;
    protected $userContext;
    protected $quoteIdMaskFactory;
    /**
     * Reward helper
     *
     * @var \Magento\Reward\Helper\Data
     */
    protected $rewardData;
    public function __construct(
        CartFactory $cartFactory,
        Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        UserContextInterface $userContext,
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        ManagerInterface $eventManager,
        \Magento\Reward\Helper\Data $rewardData
    ) {
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_cartFactory = $cartFactory;
        $this->_cartRepository = $cartRepository;
        $this->_productRepository = $productRepository;
        $this->eventManager = $eventManager;
        $this->userContext = $userContext;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->rewardData = $rewardData;
    }

    /**
     * @inheritdoc
     */
    public function addProduct($requestParams)
    {
        if (!isset($requestParams['product'])) {
            throw new \Exception('Missing Product ID');
        }

        try {
            $quote = $this->_cartRepository->get(@$requestParams['cartId']);
        } catch (\Exception $e) {
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load(@$requestParams['quoteId'], 'masked_id');
            $quote = $this->_cartRepository->get($quoteIdMask->getQuoteId());
        }

        $this->_initSession($quote);
        $product = $this->_initProduct($requestParams['product']);
//        if($product->getTypeId() == 'configurable'){
//            $product = $this->_initProduct($requestParams['selected_configurable_option']);
//        }
        $cart = $this->_cartFactory->create();
//        $cart = $this->_cartFactory->create()->getQuote()->load($quote->getId());
        $request = new \Magento\Framework\DataObject($requestParams);
        $cart->addProduct($product, $request);
        if (!empty($requestParams['related_product'])) {
            $cart->addProductsByIds(explode(',', $requestParams['related_product']));
        }
        $cart->save();
        $cartData = $cart->getData();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $cartData['quote'];

        $quoteId = $quote->getEntityId();
        /** @var $quoteIdMask QuoteIdMask */
//        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'quote_id');
        $results = $this->_cartRepository->get($quoteId);
        $items = $results->getItems();
        $count = count($items);
        $result = new DataObject([
            'success' => true,
            'cart_items_count' => $count
        ]);
        return $result;
    }

    /**
     * @param Quote $quote
     */
    protected function _initSession($quote)
    {
        if ($quote->getCustomer()->getId()) {
            $this->_checkoutSession->setCustomerData($quote->getCustomer());
        } else {
            $this->_customerSession->setCustomerId(null);
        }
        $this->_checkoutSession->setQuoteId($quote->getId());
    }

    /**
     * @param int $productId
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    protected function _initProduct($productId)
    {
        return $this->_productRepository->getById($productId);
    }

    public function removeApplyRewardPoints($customerId,$cartId){
        if(!$this->rewardData->isEnabledOnFront() || !$this->rewardData->getHasRates()){
            return false;
        }
        try {
            $quote = $this->_cartRepository->get($cartId);
        } catch (\Exception $e) {
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $quote = $this->_cartRepository->get($quoteIdMask->getQuoteId());
        }
        $this->_initSession($quote);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->get(\Magento\Checkout\Model\Session::class)->getQuote();
        $a = $quote->getUseRewardPoints();
        if($quote->getUseRewardPoints()){
            $quote->setUseRewardPoints(false)->collectTotals()->save();
            return true;
        }else{
            return false;
        }
    }
    public function removeStoreCredit($customerId,$cartId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if(!$objectManager->get(\Magento\CustomerBalance\Helper\Data::class)->isEnabled()){
            return false;
        }
        try {
            $quote = $this->_cartRepository->get($cartId);
        } catch (\Exception $e) {
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $quote = $this->_cartRepository->get($quoteIdMask->getQuoteId());
        }
        $this->_initSession($quote);
        $quote = $objectManager->get(\Magento\Checkout\Model\Session::class)->getQuote();
        if ($quote->getUseCustomerBalance()) {
            $quote->setUseCustomerBalance(false)->collectTotals()->save();
            return true;
        }else{
            return false;
        }
    }
}