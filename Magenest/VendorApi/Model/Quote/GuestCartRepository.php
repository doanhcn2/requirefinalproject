<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 06/04/2018
 * Time: 15:28
 */
namespace Magenest\VendorApi\Model\Quote;

use Magenest\VendorApi\Api\Cart\GuestCartRepositoryInterface;
use \Magento\Quote\Model\QuoteIdMask;
use \Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Quote\Model\QuoteIdMaskFactory;

class GuestCartRepository implements GuestCartRepositoryInterface{
    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    protected $objectFactory;

    protected $vendorFactory;

    protected $typeConfigurable;

    protected $productRepositoryFactory;

    protected $storeManager;
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;
    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $session;
    /**
     * Initialize dependencies.
     *
     * @param CartRepositoryInterface $quoteRepository
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Framework\DataObject $objectFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
        $this->objectFactory = $objectFactory;
        $this->vendorFactory = $vendorFactory;
        $this->typeConfigurable = $catalogProductTypeConfigurable;
        $this->productRepositoryFactory = $productRepositoryFactory;
        $this->storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->tickets = $ticketsFactory;
        $this->session = $sessionFactory;
    }

    public function get($cartId){
        /** @var $quoteIdMask QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        $results = $this->quoteRepository->get($quoteIdMask->getQuoteId());
        $items = $results->getItems();
        $address = $results->getShippingAddress();
        $shipping_amount = 0;
        if($address){
            $shipping_amount = $address->getShippingAmount();
        }
        $discountAmount = 0;
        foreach ($items as $key => $item){
            $discountAmount += $item->getDiscountAmount();
            $product = $item->getProduct();
            $options = $product->getTypeInstance(true)->getOrderOptions($product);
            $vendor_id = $item->getUdropshipVendor();
            if(isset($vendor_id)){
                $_v = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Helper\Data')->getVendor($vendor_id);
                /** @var \Unirgy\Dropship\Model\Vendor\Product $_vpm */
                $_vpm = \Magento\Framework\App\ObjectManager::getInstance()->create(\Unirgy\Dropship\Model\Vendor\Product::class);
                $_vpc = $_vpm->getCollection()
                    ->addFieldToSelect('*')->addFieldToFilter('product_id', $product->getId())
                    ->addFieldToFilter('vendor_id', ['eq'=> $vendor_id])
                    ->getFirstItem();
                $seller_note = $_vpc->getStateDescr();
                $vendor_name = $_v->getVendorName();
            }
            $small_image = $product->getSmallImage() ? $product->getSmallImage() : '';
            $product->addData([
                'image'   =>  $small_image
            ]);
            $additionalOptions = [];
            if(isset($options['info_buyRequest']['additional_options'])){
                $data = $options['info_buyRequest'];
                if(isset($data['additional_options']['is_gifted'])){
                    if (isset($data['additional_options']['To'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Name',
                            'value' => $data['additional_options']['To'],
                        ];
                    }
                    if (isset($data['additional_options']['Email'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Email',
                            'value' => $data['additional_options']['Email'],
                        ];
                    }
                    if (isset($data['additional_options']['Message'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Message',
                            'value' => $data['additional_options']['Message'],
                        ];
                    }
                }
                if (isset($data['additional_options']['coupon_selected_children'])) {
                    $additionalOptions[] = [
                        'label' => 'Deal Option',
                        'value' => $this->_productRepository->getById((int)$data['additional_options']['coupon_selected_children'])->getName(),
                    ];
                }
                if(isset($data['additional_options']['ticket']) && (!empty($data['additional_options']['ticket']))){
                    $ticketModel = $this->tickets->create()->load($data['additional_options']['ticket']);
                    $additionalOptions[] = array(
                        'label' => 'Option',
                        'value' => $ticketModel->getTitle()
                    );
                }
                if (isset($data['additional_options']['ticket_session']) && (!empty($data['additional_options']['ticket_session']))) {
                    $ticketModel = $this->session->create()->load($data['additional_options']['ticket_session']);
                    $additionalOptions[] = array(
                        'label' => 'Session',
                        'value' => $this->convertTime($ticketModel->getSessionFrom()) . ' - ' . $this->convertTime($ticketModel->getSessionTo())
                    );
                }


            }
            $item->addData([
                'item_options'   => isset($options['attributes_info']) ? $options['attributes_info'] : [],
                'custom_options' => isset($options['options']) ? $options['options'] : [],
                'additional_options' => $additionalOptions,
                'vendor_id'   => isset($vendor_id) ? $vendor_id : '',
                'vendor_name'   => isset($vendor_name) ? $vendor_name : 'Unknown Vendor',
                'seller_note'   => isset($seller_note) ? $seller_note : '',
                'shipping' => '0.0000',
                'tax' => $item->getTaxAmount()
            ]);
        }
        $results->addData([
            'discount_total' => $discountAmount,
            'shipping_amount' => $shipping_amount
        ]);
        return $results;
    }
    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time){
        $strTime = strtotime($time);

        return date('d-m-Y H:i:s', $strTime);
    }
}