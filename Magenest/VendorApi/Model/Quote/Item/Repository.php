<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/02/2018
 * Time: 08:37
 */

namespace Magenest\VendorApi\Model\Quote\Item;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;

/**
 * Class Repository
 * @package Magenest\VendorApi\Model\Quote\Item
 */
class Repository implements \Magenest\VendorApi\Api\CartItemRepositoryInterface
{
    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * Product repository.
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Quote\Api\Data\CartItemInterfaceFactory
     */
    protected $itemDataFactory;

    /**
     * @var CartItemProcessorInterface[]
     */
    protected $cartItemProcessors;

    protected $_unirgyProductPriceHelper;

    /**
     * @param \Unirgy\DropshipMultiPrice\Helper\Data $unirgyProductPriceHelper
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Quote\Api\Data\CartItemInterfaceFactory $itemDataFactory
     * @param CartItemProcessorInterface[] $cartItemProcessors
     */
    public function __construct(
        \Unirgy\DropshipMultiPrice\Helper\Data $unirgyProductPriceHelper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Quote\Api\Data\CartItemInterfaceFactory $itemDataFactory,
        array $cartItemProcessors = []
    ) {
        $this->_unirgyProductPriceHelper = $unirgyProductPriceHelper;
        $this->quoteRepository = $quoteRepository;
        $this->productRepository = $productRepository;
        $this->itemDataFactory = $itemDataFactory;
        $this->cartItemProcessors = $cartItemProcessors;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return \Magento\Quote\Api\Data\CartItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function save(\Magento\Quote\Api\Data\CartItemInterface $cartItem)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        /** @var \Magento\Quote\Model\Quote\Item $cartItem */
        $cartId = $cartItem->getQuoteId();
        $addedProduct = $this->productRepository->get($cartItem->getSku());
        if ($addedProduct->getTypeId() == 'configurable') {
            $this->_unirgyProductPriceHelper->addVendorOption($cartItem, $cartItem->getExtensionAttributes()->getUdropshipVendor());
        }
        $quote = $this->quoteRepository->getActive($cartId);
        $quoteItems = $quote->getItems();
        $quoteItems[] = $cartItem;
        $quote->setItems($quoteItems);
        $this->quoteRepository->save($quote);
        if ($cartItem->getPrice()) {
            $lastItem = $quote->getLastAddedItem();
            $lastItem->setPrice($cartItem->getPrice());
            $lastItem->setCustomPrice($cartItem->getPrice());
            $lastItem->setOriginalCustomPrice($cartItem->getPrice());
            $lastItem->getProduct()->setIsSuperMode(true);
            $lastItem->save();
        }
        $quote->collectTotals();
        return $quote->getLastAddedItem();
    }
}
