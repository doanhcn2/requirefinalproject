<?php
/**
 * Contributor company: iPragmatech solution Pvt Ltd.
 * Contributor Author : Manish Kumar
 * Date: 23/5/16
 * Time: 11:55 AM
 */

namespace Magenest\VendorApi\Model\RewardPoint;

use Magenest\VendorApi\Api\RewardPoint\RewardPointManagementInterface;
use Magenest\VendorApi\Helper\DebugHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CustomerBalance\Model\Balance\HistoryFactory;
use Magento\CustomerBalance\Model\BalanceFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\InputException;
use Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory;

/**
 * Defines the implementaiton class of the RewardPointManagementInterface
 */
class RewardPointManagement implements RewardPointManagementInterface
{
    /**
     * @var $_customerBalanceFactory BalanceFactory
     */
    protected $_customerBalanceFactory;

    /**
     * @var $_customerBalanceHistoryFactory HistoryFactory
     */
    protected $_customerBalanceHistoryFactory;

    /**
     * @var CollectionFactory
     */
    protected $_rewardHistoryCollectionFactory;

    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * @var DataObject
     */
    protected $_objectFactory;

    /**
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * WishlistManagement constructor.
     * @param DebugHelper $debugHelper
     * @param DataObject $objectFactory
     * @param CollectionFactory $rewardHistoryCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param HistoryFactory $historyFactory
     * @param BalanceFactory $balanceFactory
     */
    public function __construct(
        DebugHelper $debugHelper,
        DataObject $objectFactory,
        CollectionFactory $rewardHistoryCollectionFactory,
        ProductRepositoryInterface $productRepository,
        HistoryFactory $historyFactory,
        BalanceFactory $balanceFactory
    ) {
        $this->_debugHelper = $debugHelper;
        $this->_customerBalanceFactory = $balanceFactory;
        $this->_customerBalanceHistoryFactory = $historyFactory;
        $this->_objectFactory = $objectFactory;
        $this->_rewardHistoryCollectionFactory = $rewardHistoryCollectionFactory;
        $this->_productRepository = $productRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getRewardPointForCustomer($customerId)
    {

        if (empty($customerId) || !isset($customerId) || $customerId == "") {
            throw new InputException(__('Id required'));
        } else {
            $collection =
                $this->_rewardHistoryCollectionFactory->create()
                    ->addFieldToSelect('*')
                    ->addCustomerFilter($customerId)
                    ->skipExpiredDuplicates()
                    ->setDefaultOrder();
            
            $rewardData = [];
            /** @var \Magento\Reward\Model\Reward\History $item */
            foreach ($collection as $item) {
                if($item->getMessage()->getText()){
                    $reason = $this->getReasonRewardPoint($item->getMessage());
                }else{
                    $reason = '';
                }
                $data = [
                    "balance" => $item->getPointsBalance(),
                    "amount"      => $item->getCurrencyAmount(),
                    "points"       => $item->getPointsDelta(),
                    "create_at"         => $item->getCreatedAt(),
                    "expired_at"         => $item->getExpiresAt(),
                    "comment"      => $item->getComment(),
                    "reason"      => $reason,

                ];
                $rewardData[] = $data;
            }
            $x =  $this->_objectFactory->addData([
                'result' => $rewardData
            ]);
            return $x;
        }
    }
    public function getReasonRewardPoint($message){
        /** @var $message \Magento\Framework\Phrase */
        $text = $message->getText();
        $arguments = $message->getArguments();
        $arr = explode('#',$text);
        if(!empty($arguments)){
            $order_id = $arguments[0];
            $string = $arr[0]. ' #'.$order_id;
        }else{
            $string = $arr[0];
        }



        return $string;
    }
    /**
     * {@inheritdoc}
     */
    public function getStoreCreditForCustomer($customerId)
    {
        // TODO: Implement getStoreCreditForCustomer() method.
        try {
            $balanceCollection = $this->_customerBalanceFactory->create()->getCollection()->addFieldToFilter('customer_id', $customerId)->getFirstItem();
            if (!empty($balanceCollection->getData())) {
                $storeCreditBalance = $balanceCollection->getAmount();
            }

            if (@$storeCreditBalance) {
                $histories = $this->_customerBalanceHistoryFactory->create()->getCollection()->addFieldToFilter('main_table.balance_id', $balanceCollection->getBalanceId())->setOrder('updated_at','DESC');

                $storeCredit = [];
                /** @var \Magento\CustomerBalance\Model\Balance\History $history */
                foreach ($histories as $history) {
                    $data = [
                        "balance_amount" => $history->getBalanceAmount(),
                        "balance_delta"      => $history->getBalanceDelta(),
                        "updated_at"         => $history->getUpdatedAt(),
                        "reason"      => $history->getAdditionalInfo()
                    ];
                    array_push($storeCredit, $data);
                }
            }

            return $this->_objectFactory->addData([
                'store_credit_balance' => @$storeCreditBalance,
                'store_credit_history' => @$storeCredit,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
}
