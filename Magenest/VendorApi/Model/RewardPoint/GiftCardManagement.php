<?php
/**
 * Contributor company: iPragmatech solution Pvt Ltd.
 * Contributor Author : Manish Kumar
 * Date: 23/5/16
 * Time: 11:55 AM
 */

namespace Magenest\VendorApi\Model\RewardPoint;

use Magenest\VendorApi\Api\RewardPoint\GiftCardInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\GiftCardAccount\Model\GiftcardaccountFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Defines the implementaiton class of the RewardPointManagementInterface
 */
class GiftCardManagement implements GiftCardInterface
{
    /**
     * @var GiftcardaccountFactory
     */
    protected $giftCardAccountFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * GiftCardManagement constructor.
     * @param GiftcardaccountFactory $giftcardaccountFactory
     * @param StoreManagerInterface $storeManagement
     */
    public function __construct(GiftcardaccountFactory $giftcardaccountFactory, StoreManagerInterface $storeManagement)
    {
        $this->giftCardAccountFactory = $giftcardaccountFactory;
        $this->storeManager = $storeManagement;
    }

    /**
     * @param int $customerId
     * @param string $giftCode
     * @return mixed
     * @throws \Exception
     */
    public function redeemGiftCard($customerId, $giftCode)
    {
        $giftCard = $this->giftCardAccountFactory->create();
        $giftCard->loadByCode($giftCode);
        try {
            $giftCard->isValid(true, true, true, false);
            $amount = $giftCard->getBalance();
            $giftCard->redeem($customerId);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result[] = ['api_status' => 0, 'result' => false, 'message' => "Invalid gift card"];
            return false;
        }
        /** @var \Magento\Directory\Model\Currency $currency */
        $currency = $this->storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
        $result[] = ['api_status' => 1, "result" => true , 'gift_card_redeem_amount' => @$amount, 'gift_card_balance' => $giftCard->getBalance(), 'currency' => $currency];
        return true;
    }

    public function checkGiftCard($giftCode)
    {
        $giftCard = $this->giftCardAccountFactory->create();
        $giftCard->loadByCode($giftCode);
        try {
            $giftCard->isValid(true, true, true, false);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result[] = ['api_status' => 0, 'result' => false, 'message' => "Invalid gift card"];
            return $result;
        }
        /** @var \Magento\Directory\Model\Currency $currency */
        $currency = $this->storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
        $result[] = ['api_status' => 1, "result" => true , 'gift_card_balance' => $giftCard->getBalance(), 'currency' => $currency];
        return $result;
    }
}
