<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:07
 */
namespace Magenest\VendorApi\Model\ResourceModel\SimpleResult;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\VendorApi\Model\ResourceModel\SimpleResult
 */
class Collection extends AbstractCollection
{

    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_vendor_simple_result';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'magenest_vendor_simple_result';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\VendorApi\Model\SimpleResult', 'Magenest\VendorApi\Model\ResourceModel\SimpleResult');
    }
}
