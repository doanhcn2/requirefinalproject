<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\ResourceModel;

use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\Customer\NotificationStorage;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ImageProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\CustomerBalance\Model\BalanceFactory;
use Magento\Reward\Model\ResourceModel\Reward\History\CollectionFactory;

/**
 * Customer repository.
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomerRepository implements \Magenest\VendorApi\Api\CustomerRepositoryInterface
{
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Model\Data\CustomerSecureFactory
     */
    protected $customerSecureFactory;

    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var \Magento\Customer\Model\ResourceModel\AddressRepository
     */
    protected $addressRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $customerResourceModel;

    /**
     * @var \Magento\Customer\Api\CustomerMetadataInterface
     */
    protected $customerMetadata;

    /**
     * @var \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var ImageProcessorInterface
     */
    protected $imageProcessor;

    /**
     * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var NotificationStorage
     */
    private $notificationStorage;

    /**
     * @var $_customerBalanceFactory BalanceFactory
     */
    protected $_customerBalanceFactory;

    /**
     * Reward history collection
     *
     * @var CollectionFactory
     */
    protected $_historyCollectionFactory;

    /**
     * @var $_dataObject \Magento\Framework\DataObject
     */
    protected $_dataObject;

    /**
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Data\CustomerSecureFactory $customerSecureFactory
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Customer\Model\ResourceModel\AddressRepository $addressRepository
     * @param \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel
     * @param \Magento\Customer\Api\CustomerMetadataInterface $customerMetadata
     * @param \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param DataObjectHelper $dataObjectHelper
     * @param ImageProcessorInterface $imageProcessor
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param CollectionProcessorInterface $collectionProcessor
     * @param NotificationStorage $notificationStorage
     * @param BalanceFactory $balanceFactory
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\DataObject $objectFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Data\CustomerSecureFactory $customerSecureFactory,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Customer\Model\ResourceModel\AddressRepository $addressRepository,
        \Magento\Customer\Model\ResourceModel\Customer $customerResourceModel,
        \Magento\Customer\Api\CustomerMetadataInterface $customerMetadata,
        \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        DataObjectHelper $dataObjectHelper,
        ImageProcessorInterface $imageProcessor,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor,
        CollectionProcessorInterface $collectionProcessor,
        NotificationStorage $notificationStorage,
        BalanceFactory $balanceFactory, CollectionFactory $collectionFactory,
        \Magento\Framework\DataObject $objectFactory
    )
    {
        $this->_dataObject = $objectFactory;
        $this->customerFactory = $customerFactory;
        $this->customerSecureFactory = $customerSecureFactory;
        $this->customerRegistry = $customerRegistry;
        $this->addressRepository = $addressRepository;
        $this->customerResourceModel = $customerResourceModel;
        $this->customerMetadata = $customerMetadata;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->eventManager = $eventManager;
        $this->storeManager = $storeManager;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->imageProcessor = $imageProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->collectionProcessor = $collectionProcessor;
        $this->notificationStorage = $notificationStorage;
        $this->_historyCollectionFactory = $collectionFactory;
        $this->_customerBalanceFactory = $balanceFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($customerId)
    {
        $customerModel = $this->customerRegistry->retrieve($customerId);
        $result = $customerModel->getDataModel();
        $collection =
            $this->_historyCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addCustomerFilter($customerId)
                ->skipExpiredDuplicates()
                ->setDefaultOrder();
        $pointBalance = 0;
        $lastChangeTime = 0;
        foreach ($collection as $item) {
            if (strtotime($item->getCreatedAt()) > $lastChangeTime) {
                $pointBalance = $item->getPointsBalance();
                $lastChangeTime = strtotime($item->getCreatedAt());
            }
        }
        $result->setData('reward_point_balance', $pointBalance);

        $balance = $this->_customerBalanceFactory->create()->getCollection()->addFieldToFilter('customer_id', $customerId)->getFirstItem()->getAmount();
        $customerBalance = @$balance ? $balance : 0;
        $result->setData('store_credit_balance', strval($customerBalance));

        $data = ['reward_point_balance' => $pointBalance,
            'store_credit_balance' => strval($customerBalance),
            'website_id' => $result->getWebsiteId(),
            'email' => $result->getEmail(),
            'group_id' => $result->getGroupId(),
            'store_id' => $result->getStoreId(),
            'created_at' => $result->getCreatedAt(),
            'updated_at' => $result->getUpdatedAt(),
            'created_in' => $result->getCreatedIn(),
            'confirmation' => $result->getConfirmation(),
            'id' => $result->getId(),
            'dob' => $result->getDob(),
            'firstname' => $result->getFirstname(),
            'gender' => $result->getGender(),
            'lastname' => $result->getLastname(),
            'middlename' => $result->getMiddlename(),
            'prefix' => $result->getPrefix(),
            'suffix' => $result->getSuffix(),
            'taxvat' => $result->getTaxvat(),
            'default_billing' => $result->getDefaultBilling(),
            'default_shipping' => $result->getDefaultShipping(),
            'addresses' => $result->getAddresses(),
            'disable_auto_group_change' => $result->getDisableAutoGroupChange(),
            'extension_attributes' => $result->getExtensionAttributes()];

        return $this->_dataObject->addData($data);
    }
}
