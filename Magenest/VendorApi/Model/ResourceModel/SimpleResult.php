<?php
/**
 * Created by PhpStorm.
 * User: hoang
 * Date: 23/02/2017
 * Time: 23:06
 */
namespace Magenest\VendorApi\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SimpleResult
 *
 * @package Magenest\VendorApi\Model\ResourceModel
 */
class SimpleResult extends  AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_vendor_simple_result', 'id');
    }
}
