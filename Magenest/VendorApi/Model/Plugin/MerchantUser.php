<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\Plugin;

use Magenest\Eplus\Model\CustomerTokenService;

/**
 * Plugin to delete customer tokens when customer becomes inactive
 */
class MerchantUser
{
    /**
     * @var CustomerTokenService
     */
    private $merchantTokenService;

    /**
     * @param CustomerTokenService $customerTokenService
     */
    public function __construct(
        CustomerTokenService $customerTokenService
    ) {
        $this->merchantTokenService = $customerTokenService;
    }

    /**
     * Check if customer is inactive - if so, invalidate their tokens
     *
     * @param \Unirgy\Dropship\Model\Vendor $subject
     * @param \Magento\Framework\DataObject $object
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterSave(
        \Unirgy\Dropship\Model\Vendor $subject,
        \Magento\Framework\DataObject $object
    ) {
        $isActive = $object->getStatus();
        $isNew = $object->getNewReg();
        if (isset($isActive) && $isActive === 'I' && $isNew != "1") {
            $this->merchantTokenService->revokeVendorAccessToken($object->getId());
        }
        return $subject;
    }
}
