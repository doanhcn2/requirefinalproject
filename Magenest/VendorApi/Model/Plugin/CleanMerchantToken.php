<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Plugin;

use Magento\Authorization\Model\UserContextInterface;
use Magento\Integration\Api\AuthorizationServiceInterface as AuthorizationService;
use Magento\Integration\Model\ResourceModel\Oauth\Token as TokenResourceModel;

/**
 * Class CleanMerchantToken
 * @package Magenest\VendorApi\Model\Plugin
 */
class CleanMerchantToken
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var TokenResourceModel
     */
    private $tokenResourceModel;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param TokenResourceModel $tokenResourceModel
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, TokenResourceModel $tokenResourceModel)
    {
        $this->tokenResourceModel = $tokenResourceModel;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $this->tokenResourceModel->deleteExpiredTokens(
            $this->getMerchantTokenLifetime(),
            [5]
        );
    }

    private function getMerchantTokenLifetime()
    {
        $hours = (int)$this->_scopeConfig->getValue('oauth/access_token_lifetime/merchant');
        return $hours > 0 ? $hours : 0;
    }
}
