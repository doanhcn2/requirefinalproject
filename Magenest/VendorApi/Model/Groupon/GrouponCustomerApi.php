<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 10/04/2018
 * Time: 13:11
 */
namespace Magenest\VendorApi\Model\Groupon;

use Magenest\VendorApi\Api\GrouponCustomerApiInterface;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\DealFactory;
use Unirgy\Dropship\Model\VendorFactory;

class GrouponCustomerApi implements GrouponCustomerApiInterface {
    /**
     * @var GrouponFactory
     */
    protected $_grouponFactory;
    /**
     * @var \Magenest\VendorApi\Helper\GrouponHelper
     */
    protected $_grouponHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    protected $storeManager;
    /**
     * @var DealFactory
     */
    protected $_dealFactory;
    /**
     * @var $_vendorFactory VendorFactory
     */
    protected $_vendorFactory;
    /**
     * Catalog product type configurable
     *
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    protected $_vendorHelper;
    public function __construct(
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\GrouponHelper $grouponHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        VendorFactory $vendorFactory,
        \Unirgy\Dropship\Helper\Data $vendorHelper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->_grouponFactory = $grouponFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_grouponHelper = $grouponHelper;
        $this->storeManager = $storeManager;
        $this->_dealFactory = $dealFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_productFactory = $productFactory;
        $this->_catalogProductTypeConfigurable = $configurable;
        $this->_vendorHelper = $vendorHelper;
    }//->getData()
//    public function getAllGrouponByCustomerId($customerId,$cur_page,$size,$status){
//        $a = $this->getAll($customerId,$cur_page,$size,$status)
//    }
    public function getAllGrouponByCustomerId($customerId,$cur_page,$size,$status){
        $status = $this->getStatus($status);
        if (!$status || $status == 0) {
            $collection = $this->_grouponFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('main_table.status', ['neq' => \Magenest\Groupon\Model\Groupon::STATUS_CANCELED]);
        } elseif ($status == 4) {
            $daystart = new \DateTime();
            $today = $daystart->format('Y-m-d');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d');
            $collection = $this->_grouponFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('main_table.status', 1)->addFieldToFilter('main_table.status',
                    ['neq' => \Magenest\Groupon\Model\Groupon::STATUS_CANCELED]);
            $customerDealIds = $collection->getColumnValues('product_id');
            $customerDealIds = array_unique($customerDealIds);
            $customerDealIds = implode(',', $customerDealIds);
            $ids = $this->_dealFactory->create()->getCollection()
                ->addFieldToFilter('product_id', ['in' => $customerDealIds])
                ->addFieldToFilter('groupon_expire', ['gteq' => $today])
                ->addFieldToFilter('groupon_expire', ['lteq' => $dateText])
                ->getColumnValues('product_id');
            $ids = array_unique($ids);
            $collection->clear()->addFieldToFilter('main_table.product_id', ['in' => $ids]);
        } elseif ($status != 4) {
            if ($status == 5) {
                $collection = $this->_grouponFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('main_table.status', ['neq' => \Magenest\Groupon\Model\Groupon::STATUS_CANCELED])
                    ->addFieldToFilter('customer_id', $customerId)
                    ->addFieldToFilter('recipient', ['notnull' => true]);
            } elseif($status == 2){
                $collection = $this->_grouponFactory->create()
                    ->getCollection()
                    ->addFieldToFilter(['main_table.status', 'main_table.customer_status'], [$status, \Magenest\Groupon\Model\Groupon::CUSTOMER_STATUS_USED])
                    ->addFieldToFilter('customer_id', $customerId);
            }else {
                $collection = $this->_grouponFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('customer_id', $customerId)
                    ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => \Magenest\Groupon\Model\Groupon::CUSTOMER_STATUS_USED], ['null' => true]])
                    ->addFieldToFilter('main_table.status', $status);
            }
        }
        if (!isset($collection)) {
            return [];
        }
        if ($cur_page && $size) {
            $collection->setPageSize($size)->setCurPage($cur_page);
        }

        $groupons = $collection->setOrder('created_at','DESC');
        $customerGroupon = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Order\CustomerGroupon');
        $grouponByProduct = $customerGroupon->getDataByProduct($groupons);
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
        $data = array();
        foreach ($grouponByProduct as $parentId => $groupon){
            $dataTmp = [];
            /** @var \Magento\Catalog\Model\Product $parentProduct */
            $parentProduct =  $this->_productFactory->create()->load($parentId);
            $parentDealData = $this->_dealFactory->create()->load($parentProduct->getEntityId(), 'product_id');
            $vendor = $this->_vendorHelper->getVendor($parentProduct);

            $dataTmp['product_id'] = $parentId;
            $dataTmp['product_name'] = $parentProduct->getName();
            $dataTmp['sku'] = $parentProduct->getSku();
            $dataTmp['product_image'] = $mediaUrl.'catalog/product'.$parentProduct->getData('image');
            $dataTmp['vendor_name'] = $vendor->getData('vendor_name');
            if($parentDealData->getData('redeemable_after')!=''){
                $dataTmp['redeemable_after'] = 'from '.date('D, M j, Y ',strtotime($parentDealData->getData('redeemable_after')));
            }
            $dataTmp['groupon_start'] = $parentDealData->getRedeemableAfter();
            $dataTmp['groupon_expire'] = $parentDealData->getData('groupon_expire');
            $dataTmp['count_days_left'] = $this->getRedeemable($parentId);
            $dataTmp['groupon_term'] = $this->_grouponHelper->getCouponTerms($parentId);

            foreach ($groupon as $coupon){
                $dataTmp['groupon_product'][] = $coupon->getData();
            }
            $data[] = $dataTmp;
        }
        return $data;
    }
    /**
     * @return int|mixed
     */
    public function getStatus($status){
        if ($status == 'available') {
            $status = 1;
        } elseif ($status == 'used') {
            $status = 2;
        } elseif ($status == 'expired') {
            $status = 3;
        } elseif ($status == 'all') {
            $status = 0;
        } elseif ($status == 'expired_soon') {
            $status = 4;
        } elseif ($status == 'gifted') {
            $status = 5;
        }
        return $status;
    }
    /**
     * @param $productId
     * @return mixed
     */
    public function getDealData($productId,$data){
        $model = $this->_dealFactory->create()->load($productId, 'product_id');
        if($data == 'expire_date'){
            return $model->getGrouponExpire();
        }elseif ($data == 'term'){
            return $model->getTerm();
        }
    }
    public function getRedeemFromDate($productId){
        $model = $this->_dealFactory->create()->load($productId, 'product_id');
        return $model->getRedeemableAfter();
    }
    /**
     * @param $productId
     * @return mixed
     */
    public function getRedeemable($productId){
        $model = $this->_dealFactory->create()->load($productId, 'product_id');
        $redeemDate = date_create($model->getData("groupon_expire"));
        $today = date_create();
        if ($today <= $redeemDate) {
            if ($redeemDate) {
                $diff = date_diff($redeemDate, $today);
                $dateDiff = $diff->days;
                if ($diff->h > 0 || $diff->i > 0 || $diff->s > 0) {
                    $dateDiff = $dateDiff + 1;
                }
                return $dateDiff;
            }
        }
        return false;
    }
    //redeemable_after
}