<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\Groupon;

/**
 * Class GrouponApi
 * @package Magenest\VendorApi\Model\Groupon
 */
class GrouponApi implements \Magenest\VendorApi\Api\GrouponApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\GrouponHelper
     */
    protected $_grouponHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_couponFactory \Magenest\Groupon\Model\GrouponFactory
     */
    protected $_couponFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * GrouponApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\VendorApi\Helper\GrouponHelper $grouponHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\GrouponHelper $grouponHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory
    ) {
        $this->_couponFactory = $grouponFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_grouponHelper = $grouponHelper;
    }

    /**
     * collection data of each Groupon
     * {@inheritdoc}
     */
    public function getGrouponProduct($store, $id)
    {
        try {
            $productInfo = $this->_grouponHelper->getGrouponDetail($store, $id);
            return $this->_objectFactory->addData([
                'groupon_product' => $productInfo,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * collection data of each Groupon
     * {@inheritdoc}
     */
    public function getListGrouponProduct($store, $size)
    {
        try {
            $productInfo = $this->_grouponHelper->getListGrouponDetail($store, $size);
            return $this->_objectFactory->addData([
                'list_groupon_product' => $productInfo,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * collection data of each Coupon
     * {@inheritdoc}
     */
    public function getCouponProduct($store, $id)
    {
        try {
            $productInfo = $this->_grouponHelper->getCouponDetail($store, $id);
            return $this->_objectFactory->addData([
                'coupon_product' => $productInfo,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicket($id, $customer_id)
    {

        try {
            $ticket = $this->_grouponHelper->getTicketDetail($id, $customer_id);
            return $this->_objectFactory->addData([
                'ticket' => $ticket,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketByCustomerId($customer_id, $size)
    {
        try {
            $ticket = $this->_grouponHelper->getTicketByCustomerIdDetail($customer_id, $size);
            return $this->_objectFactory->addData([
                'ticket_by_customer_id' => $ticket,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function redeemCoupons($customerId, $listCoupons)
    {
        try {
            $coupons = $this->_couponFactory->create()->getCollection()->addFieldToFilter('customer_id', $customerId)->addFieldToFilter('redemption_code', ['in' => $listCoupons]);
            $redeemed = 0;
            foreach ($coupons as $coupon)
            {
                /**
                 * @var $coupon \Magenest\Groupon\Model\Groupon
                 */
                $redeem = $coupon->redeemCoupon();
                if ($redeem){
                    $redeemed++;
                }
            }
            return $this->_objectFactory->addData([
                'redeem_status' => true,
                'redeem_message' => $redeemed . ' of ' . count($listCoupons) . ' has been redeemed.',
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'redeem_message' => $e->getMessage(),
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCouponDetail($customerId, $couponId)
    {

            $coupon = $this->_grouponHelper->getCouponDetailsById($customerId, $couponId);
            return $this->_objectFactory->addData([
                'coupon_data' => [$coupon['coupon_data']],
                'coupon_location' => [$coupon['coupon_location']],
                'coupon_term' => [$coupon['coupon_term']],
                'rating_option' => $coupon['rating_option'],
                'api_status' => 1
            ]);
    }
    public function customerMarkAvailableCoupon($customerId,$listCouponRedeemCode){
        if($listCouponRedeemCode){
            $groupons = $this->_couponFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('redemption_code', ['in' => $listCouponRedeemCode]);
            $flag = false;
            foreach ($groupons as $groupon){
                /**
                 * @var $coupon \Magenest\Groupon\Model\Groupon
                 */
                $status = (int)$groupon->getStatus();
                if(($status != \Magenest\Groupon\Model\Groupon::STATUS_CANCELED)){
                    try{
                        $groupon->setStatus(\Magenest\Groupon\Model\Groupon::STATUS_AVAILABLE);
                        $groupon->setUpdatedAt(date("Y-m-d H:i:s"));
                        $groupon->save();
                        $flag = true;
                    }catch (\Exception $exception){
                        $this->_debugHelper->debugString($exception->getCode(),$exception->getMessage());
                        $flag = false;
                    }

                }
            }
            return $flag;
        }else{
            $this->_debugHelper->debugString('couponId','couponId empty');
            return false;
        }
    }
    private function isCustomerpermitted($customerId,$customerIdInCoupon){
        return (int)$customerId === (int)$customerIdInCoupon;
    }
}
