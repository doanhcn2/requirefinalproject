<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\Order;

use Magenest\VendorApi\Helper\OrderHelper;

/**
 * Class OrderInformationInterface
 * @package Magenest\VendorApi\Model\Groupon
 */
class OrderInformationApi implements \Magenest\VendorApi\Api\OrderApiInterface
{
    /**
     * @var \Magenest\VendorApi\Helper\OrderHelper
     */
    protected $_orderHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    /**
     * GrouponApi constructor.
     * @param \Magenest\VendorApi\Helper\DebugHelper $debugHelper
     * @param \Magento\Framework\DataObject $objectFactory
     * @param OrderHelper $orderHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\OrderHelper $orderHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_orderHelper = $orderHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOrderByCustomer($customer_id, $cur_page, $size)
    {
        try {
            $order = $this->_orderHelper->getAllOrderByCustomerId($customer_id, $cur_page, $size);
            return $this->_objectFactory->addData([
                'order_by_customer_id' => $order,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
    public function getCustomerOrderByOrderId($order_id){
        try{
            $order = $this->_orderHelper->getCustomerOrderByOrderId($order_id);
            return $this->_objectFactory->addData([
                'order_by_id' => $order,
                'api_status' => 1
            ]);
        }catch (\Exception $e){
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }
    public function filterCustomerOrder($customerId,$requestParams){
        if(isset($requestParams)){
            $filterby = isset($requestParams['filter_by'])?$requestParams['filter_by']:'';
            $datafilter = isset($requestParams['data_filter'])?$requestParams['data_filter']:'';
            $sort = isset($requestParams['sort'])?$requestParams['sort']:'';
            $search = isset($requestParams['search'])?$requestParams['search']:'';
            $cur_page = isset($requestParams['cur_page'])?$requestParams['cur_page']:'';
            $size = isset($requestParams['size'])?$requestParams['size']:'';
            $result = $this->_orderHelper->getAllCustomerOrder($customerId,$filterby,$datafilter,$search,$cur_page,$size,$sort);
            return $result;
        }
    }
}
