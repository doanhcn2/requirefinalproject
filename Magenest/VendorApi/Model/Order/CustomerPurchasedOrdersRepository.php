<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 11/04/2018
 * Time: 08:41
 */
namespace Magenest\VendorApi\Model\Order;
use Magenest\VendorApi\Api\CustomerPurchasedOrdersApiInterface;
use Magento\Framework\Api\ObjectFactory;
use Magento\Framework\App\ObjectManager;

class CustomerPurchasedOrdersRepository implements CustomerPurchasedOrdersApiInterface{
    /**
     * @var \Magenest\VendorApi\Helper\OrderHelper
     */
    protected $orderHelper;
    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $debugHelper;
    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    protected $storeManager;
    protected $_hlp;
    protected $_magenestReviewHelper;
    protected $helper;
    protected $_unirgyRatingHelper;
    protected $_dealFactory;
    protected $_reviewFactory;
    protected $_orderFactory;
    protected $_unirgyRmaHelper;
    public function __construct(
        \Magenest\VendorApi\Helper\OrderHelper $orderHelper,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\Dropship\Helper\Data $udropshipHelper,
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $magenestReviewHelper,
        \Magenest\FixUnirgy\Helper\Review\Data $datahelper,
        \Unirgy\DropshipVendorRatings\Helper\Data $unirgyRatingHelper,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Review\Model\ReviewFactory $modelReviewFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Unirgy\Rma\Helper\Data $unirgyRmaHelper
    ){
        $this->orderHelper = $orderHelper;
        $this->debugHelper = $debugHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_objectFactory = $objectFactory;
        $this->storeManager = $storeManager;
        $this->_magenestReviewHelper = $magenestReviewHelper;
        $this->_hlp = $udropshipHelper;
        $this->helper = $datahelper;
        $this->_unirgyRatingHelper = $unirgyRatingHelper;
        $this->_dealFactory = $dealFactory;
        $this->_reviewFactory = $modelReviewFactory;
        $this->_orderFactory = $orderFactory;
        $this->_unirgyRmaHelper = $unirgyRmaHelper;
    }
    public function getAllItemCustomerCanReview($customerId){
        $shipments = $this->_magenestReviewHelper->getReviewVendorPendingCollection($customerId)->getItems();
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $aggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getAggregateRatings();
        $noneAggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getNonAggregateRatings();

        $results = [];
        foreach ($shipments as $shipment){
            $data = $shipment->getData();
            $allItems = $shipment->getAllItems();
            $dataItem = [];
            foreach ($allItems as $item){
                $dataItem = $item->getData();
            }

            $ratings = [];
            foreach ($aggregateRatings as $aggregateRating){
                $checkCategories = $this->orderHelper->checkCategoryShipment($aggregateRating,$shipment);
                if($checkCategories){
                    $options = $aggregateRating->getOptions();
                    $dataRating = [];
                    $dataRating['code'] = $aggregateRating->getRatingCode();
                    foreach ($options as $option){
                        $dataRating['option'][] = $option->getData();
                    }
                    $ratings[] = $dataRating;
                }
            }
            foreach ($noneAggregateRatings as $noneAggregateRating){
                $checkCategories = $this->orderHelper->checkCategoryShipment($noneAggregateRating,$shipment);
                if($checkCategories){
                    $options = $noneAggregateRating->getOptions();
                    $dataRating = [];
                    $dataRating['code'] = $noneAggregateRating->getRatingCode();
                    foreach ($options as $option){
                        $dataRating['option'][] = $option->getData();
                    }
                    $ratings[] = $dataRating;
                }
            }
            if(!empty($dataItem)){
                $results[] = [
                    'entity_id' => 10,
                    'id' => $data['udropship_vendor'],
                    'rel_id' => $data['entity_id'],
                    'order_id' => $data['order_id'],
                    'store_id' => $data['store_id'],
                    'increment_id' => $data['increment_id'],
                    'product_id' => $dataItem['product_id'],
                    'product_name' => $dataItem['name'],
                    'created_at' => $data['created_at'],
                    'updated_at' => $data['updated_at'],
                    'vendor_id' => $data['udropship_vendor'],
                    'is_virtual' => 0,
                    'rating_option' => $ratings
                ];
            }else{
                $results[] = [
                    'entity_id' => 10,
                    'id' => $data['udropship_vendor'],
                    'rel_id' => $data['entity_id'],
                    'order_id' => $data['order_id'],
                    'store_id' => $data['store_id'],
                    'increment_id' => $data['increment_id'],
                    'created_at' => $data['created_at'],
                    'updated_at' => $data['updated_at'],
                    'vendor_id' => $data['udropship_vendor'],
                    'is_virtual' => 0,
                    'rating_option' => $ratings
                ];
            }
        }
        $itemCollection = $this->getRatingItemCollection($customerId);//getRatingItemCollection
        $virtualProducts = $itemCollection->getItems();
        foreach ($virtualProducts as $virtualProduct){
            $ratings = [];
            foreach ($aggregateRatings as $aggregateRating){
                $checkCategories = $this->orderHelper->checkCategoryVirtualProduct($aggregateRating,$virtualProduct);

                if($checkCategories){
                    $options = $aggregateRating->getOptions();
                    $dataRating = [];
                    $dataRating['code'] = $aggregateRating->getRatingCode();
                    foreach ($options as $option){
                        $dataRating['option'][] = $option->getData();
                    }
                    $ratings[] = $dataRating;
                }
            }
            foreach ($noneAggregateRatings as $noneAggregateRating){
                $checkCategories = $this->orderHelper->checkCategoryVirtualProduct($noneAggregateRating,$virtualProduct);
                if($checkCategories){
                    $options = $noneAggregateRating->getOptions();
                    $dataRating = [];
                    $dataRating['code'] = $noneAggregateRating->getRatingCode();
                    foreach ($options as $option){
                        $dataRating['option'][] = $option->getData();
                    }
                    $ratings[] = $dataRating;
                }
            }
            $results[] = [
                'entity_id' => 7,
                'id' => $virtualProduct->getUdropshipVendor(),
                'rel_id' => $virtualProduct->getItemId(),
                'item_id' => $virtualProduct->getItemId(),
                'order_id' => $virtualProduct->getOrderId(),
                'store_id' => $virtualProduct->getStoreId(),
                'created_at' => $virtualProduct->getCreatedAt(),
                'updated_at' => $virtualProduct->getUpdatedAt(),
                'product_id' => $virtualProduct->getProductId(),
                'product_type' => $virtualProduct->getProductType(),
                'sku' => $virtualProduct->getSku(),
                'product_name' => $virtualProduct->getName(),
                'vendor_id' => $virtualProduct->getUdropshipVendor(),
                'is_virtual' => 1,
                'rating_option' => $ratings
            ];
        }
        return $results;
    }
    public function getItemsPendingReviewVendor(){
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $unirgyListCustomerReviewPending = $_objectLoader->get('Unirgy\DropshipVendorRatings\Block\Customer\ListCustomer\Pending');
        $_shipments = $unirgyListCustomerReviewPending->getReviewsCollection()->setPageSize(false)->setCurPage(false);
        $_shipments = $_shipments->getItems();
        $itemCollection = $_objectLoader->get('Magenest\FixUnirgy\Block\Customer\OrderLoader')->getRatingItemCollection();
        $items = $itemCollection->getItems();
        $pendingReviews = array_merge($_shipments, $items);
        return $pendingReviews;
    }
    public function getItemsPendingReviewProduct(){
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $fixUnirgyListCustomerReviewPending = $_objectLoader->get('Magenest\FixUnirgy\Block\Customer\ListCustomer\Pending');
        $_shipments = $fixUnirgyListCustomerReviewPending->getReviewsCollection()->setPageSize(false)->setCurPage(false);
        $_shipments = $_shipments->getItems();
        return $_shipments;
    }
    public function getAggregateRatings(){
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $aggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getAggregateRatings();
        $noneAggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getNonAggregateRatings();
        $results = [];
        foreach ($aggregateRatings as $aggregateRating){
            $checkCategories = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->checkCategory($aggregateRating);
            if($checkCategories){
                $options = $aggregateRating->getOptions();
                $data = [];
                $data['code'] = $aggregateRating->getRatingCode();
                foreach ($options as $option){
                    $data['option'][] = $option->getData();
                }
                $results[] = $data;
            }
        }
        foreach ($noneAggregateRatings as $noneAggregateRating){
            $options = $noneAggregateRating->getOptions();
            $data = [];
            $data['code'] = $noneAggregateRating->getRatingCode();
            foreach ($options as $option){
                $data['option'][] = $option->getData();
            }
            $results[] = $data;
        }
        return $results;
    }
    public function getItemInOrderByVendorId($vendorId,$reviewCollection){
        $result = [];
        foreach ($reviewCollection as $review){
            if($review['vendor_id'] == $vendorId){
                $result[] = $review;
            }
        }
        return $result;
    }
    public function getAllVendorsFromCustomerPurchasedOrders($customer_id){
        $entityId = isset($_GET['entity_id'])?$_GET['entity_id']:7;
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        //set customer session id
        $_objectLoader->create('Magento\Customer\Model\Session')->setCustomerId($customer_id);

        $vendors = $this->getVendorInformations($customer_id);
        $resultsForVendor = $resultsForProduct = [];
        $pendingReview = [];
        if($entityId == 1){
            $pendingReview = $this->getItemsPendingReviewProduct();
            foreach ($vendors as $vendor){
                $productItems = [];
                $vendorId = $vendor['vendor_id'];
                foreach ($pendingReview as $review){
                    $udropship_vendor = $review->getUdropshipVendor();
                    if($udropship_vendor == $vendorId){
                        $shipmentItemHasNoReview = $_objectLoader->get('Magenest\FixUnirgy\Block\Customer\ListCustomer\Pending');
                        foreach ($review->getAllItems() as $_item){
                            if ($_item->getOrderItem()->getParentItem() || !in_array($_item->getProductId(), $shipmentItemHasNoReview->getShipmentItemHasNoReview($review))){
                                continue;
                            }
                            $productReview = $_objectLoader->create('Magenest\FixUnirgy\Block\Product\ProductReview');
                            //set product review Id
                            $productReview->setProductId($_item->getProductId());
                            //get product review name
                            $product = $productReview->getProductInfo();
                            //get ratings
                            $ratings = $productReview->getRatings();
                            $rating = [];
                            foreach ($ratings as $_rating){
                                $checkCategories = $productReview->checkCategory($_rating);
                                if($checkCategories){
                                    $rating_option = [];
                                    foreach ($_rating->getOptions() as $option){
                                        $rating_option [] = $option->getData();
                                    }
                                    $rating[] = [
                                        'rating_id' => $_rating->getId(),
                                        'rating_code' => $_rating->getRatingCode(),
                                        'rating_option' => $rating_option
                                    ];
                                }
                            }
                            $productItems [] = [
                                'entity_id' => '1',
                                'id' => $_item->getProductId(),
                                'shipment_id' => $review->getEntityId(),
                                'vendor_id' => $vendorId,
                                'vendor_name' => $vendor['vendor_name'],
                                'rel_id' => $review->getId(),
                                'item_id' => null,
                                'order_id' => $review->getOrderId(),
                                'store_id' => $review->getStoreId(),
                                'shipment_increment_id' => $review->getIncrementId(),
                                'created_at' => $review->getCreatedAt(),
                                'updated_at' => $review->getUpdatedAt(),
                                'product_id' => $_item->getProductId(),
                                'sku' => $_item->getSku(),
                                'price' => $_item->getPrice(),
                                'qty_shipped' => (int)$_item->getQtyShipped(),
                                'product_name' => $product->getName(),
                                'product_type' => $product->getType(),
                                'rating_option' => $rating
                            ];
                        }
                    }
                }
                if(!empty($productItems)){
                    $data = $vendor;
                    $data['item'] = $productItems;
                    $resultsForProduct[] = $data;
                }
            }
        }elseif ($entityId == 7){
            $pendingReview = $this->getItemsPendingReviewVendor();
            foreach ($vendors as $vendor){
                $vendorItems = [];
                $vendorId = $vendor['vendor_id'];
                /** @var $review \Magento\Sales\Model\Order\Item | \Magento\Sales\Model\Order\Shipment */
                foreach ($pendingReview as $review){
                    $items = [];
                    $udropship_vendor = $review->getUdropshipVendor();
                    if($udropship_vendor == $vendorId){
                        if ($review instanceof \Magento\Sales\Model\Order\Item){
                            $product = $review->getProduct();
                            $rating = null;
                            $reviewForm = $_objectLoader->create('Magenest\FixUnirgy\Block\ReviewForm');
                            $reviewForm->setEntityId(\Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID);
                            $reviewForm->setEntityPkValue($product->getUdropshipVendor());
                            $reviewForm->setRelEntityPkValue($review->getId());
                            $reviewForm->setOrderItem($review);
                            $aggregateRatings = $reviewForm->getAggregateRatings();
                            $nonAggregateRatings = $reviewForm->getNonAggregateRatings();
                            if($aggregateRatings && $aggregateRatings->getSize()){
                                foreach ($aggregateRatings as $_rating){
                                    $checkCategories = $reviewForm->checkCategory($_rating);
                                    if($checkCategories){
                                        $rating_option = null;
                                        foreach ($_rating->getOptions() as $option){
                                            $rating_option [] = $option->getData();
                                        }
                                        $rating[] = [
                                            'rating_id' => $_rating->getId(),
                                            'rating_code' => $_rating->getRatingCode(),
                                            'rating_option' => $rating_option
                                        ];
                                    }
                                }
                            }
                            if($nonAggregateRatings && $nonAggregateRatings->getSize()){
                                foreach ($nonAggregateRatings as $_rating){
                                    $checkCategories = $reviewForm->checkCategory($_rating);
                                    if($checkCategories){
                                        $rating_option = null;
                                        foreach ($_rating->getOptions() as $option){
                                            $rating_option [] = $option->getData();
                                        }
                                        $rating[] = [
                                            'rating_id' => $_rating->getId(),
                                            'rating_code' => $_rating->getRatingCode(),
                                            'rating_option' => $rating_option
                                        ];
                                    }
                                }
                            }
                            $items[] = [
                                'entity_id' => '7',
                                'id' => $vendorId,
                                'shipment_id' => $review->getId(),
                                'vendor_id' => $vendorId,
                                'vendor_name' => $vendor['vendor_name'],
                                'rel_id' => $review->getId(),
                                'item_id' => $review->getId(),
                                'order_id' => $review->getOrderId(),
                                'store_id' => $review->getStoreId(),
                                'shipment_increment_id' => null,
                                'created_at' => $review->getCreatedAt(),
                                'updated_at' => $review->getUpdatedAt(),
                                'product_id' => $product->getProductId(),
                                'sku' => $product->getSku(),
                                'price' => $product->getPrice(),
                                'product_name' => $product->getName(),
                                'product_type' => $product->getType()
                            ];
                            $dataTmp = [];
                            $dataTmp['product'] = $items;
                            $dataTmp['rating_option'] = $rating;
                            $vendorItems[] = $dataTmp;
                        }
                        if($review instanceof \Magento\Sales\Model\Order\Shipment){
                            foreach ($review->getAllItems() as $_item){
                                if ($_item->getOrderItem()->getParentItem()) continue;
                                $items[] = [
                                    'entity_id' => '10',
                                    'id' => $vendorId,
                                    'shipment_id' => $review->getId(),
                                    'vendor_id' => $vendorId,
                                    'vendor_name' => $vendor['vendor_name'],
                                    'rel_id' => $review->getId(),
                                    'item_id' => null,
                                    'order_id' => $review->getOrderId(),
                                    'store_id' => $review->getStoreId(),
                                    'shipment_increment_id' => $review->getIncrementId(),
                                    'created_at' => $review->getCreatedAt(),
                                    'updated_at' => $review->getUpdatedAt(),
                                    'product_id' => $_item->getProductId(),
                                    'sku' => $_item->getSku(),
                                    'price' => $_item->getPrice(),
                                    'qty_shipped' => (int)$_item->getQtyShipped(),
                                    'product_name' => $_item->getName(),
                                    'product_type' => $_item->getType()
                                ];
                            }

                            $rating = null;
                            $reviewForm = $_objectLoader->create('Magenest\FixUnirgy\Block\ReviewForm');
                            $reviewForm->setEntityPkValue($review->getUdropshipVendor());
                            $reviewForm->setRelEntityPkValue($review->getId());
                            $reviewForm->setShipment($review);
                            $aggregateRatings = $reviewForm->getAggregateRatings();
                            $nonAggregateRatings = $reviewForm->getNonAggregateRatings();
                            if($aggregateRatings && $aggregateRatings->getSize()){
                                foreach ($aggregateRatings as $_rating){
                                    $checkCategories = $reviewForm->checkCategoryForShipment($_rating,$review);
                                    if($checkCategories){
                                        $rating_option = null;
                                        foreach ($_rating->getOptions() as $option){
                                            $rating_option [] = $option->getData();
                                        }
                                        $rating[] = [
                                            'rating_id' => $_rating->getId(),
                                            'rating_code' => $_rating->getRatingCode(),
                                            'rating_option' => $rating_option
                                        ];
                                    }
                                }
                            }
                            if($nonAggregateRatings && $nonAggregateRatings->getSize()){
                                foreach ($nonAggregateRatings as $_rating){
                                    $checkCategories = $reviewForm->checkCategoryForShipment($_rating,$review);
                                    if($checkCategories){
                                        $rating_option = null;
                                        foreach ($_rating->getOptions() as $option){
                                            $rating_option [] = $option->getData();
                                        }
                                        $rating[] = [
                                            'rating_id' => $_rating->getId(),
                                            'rating_code' => $_rating->getRatingCode(),
                                            'rating_option' => $rating_option
                                        ];
                                    }
                                }
                            }
                            $dataTmp = [];
                            $dataTmp['product'] = $items;
                            $dataTmp['rating_option'] = $rating;
                            $vendorItems[] = $dataTmp;
                        }
                    }
                }
                if(!empty($vendorItems)){
                    $data = $vendor;
                    $data['item'] = $vendorItems;
                    $resultsForVendor[] = $data;
                }

            }
        }
        $results = array_merge($resultsForProduct,$resultsForVendor);
        if(empty($results)){
            return $this->_objectFactory->addData([
                'vendor_informations' => null
            ]);
        }
        return $this->_objectFactory->addData([
            'vendor_informations' => $results
        ]);
    }
    public function getVendorInformations($customerId){
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $vendorTbl = $connection->getTableName('udropship_vendor');
        $salesOrderTbl = $connection->getTableName('sales_order');
        $salesOrderItemTbl = $connection->getTableName('sales_order_item');

        $sql = "SELECT `vendor_id`, `vendor_name`, `email` FROM $vendorTbl 
                INNER JOIN $salesOrderItemTbl ON $salesOrderItemTbl.`udropship_vendor` = $vendorTbl.`vendor_id`
                INNER JOIN $salesOrderTbl ON $salesOrderTbl.`entity_id` = $salesOrderItemTbl.`order_id` AND $salesOrderTbl.`customer_id` = $customerId
                WHERE $vendorTbl.`allow_udratings` = 1
                GROUP BY $vendorTbl.`vendor_id`;";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getAllCustomerOrderItemByVendorId($vendorId,$customerId){
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $salesOrderTbl = $connection->getTableName('sales_order');
        $salesOrderItemTbl = $connection->getTableName('sales_order_item');
        $sql = "SELECT $salesOrderItemTbl.* FROM $salesOrderItemTbl 
                INNER JOIN $salesOrderTbl ON $salesOrderTbl.`entity_id` = $salesOrderItemTbl.`order_id` AND $salesOrderTbl.`customer_id` = $customerId
                WHERE $salesOrderItemTbl.`udropship_vendor` = $vendorId";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getRatingItemCollection($customerId){
        $itemCollection = $this->helper->getPendingVirtualItemReview();
        $itemCollection->addFieldToFilter('so.customer_id', $customerId);
        $productHasReview = $this->getListProductHasReview($customerId);
        if(count($productHasReview) > 0){
            $itemCollection->addFieldToFilter('main_table.product_id', ['nin' => $productHasReview]);
        }
        $itemCollection->addFieldToFilter('product_type', ['IN' => ['virtual', 'coupon', 'ticket']]);
        $itemCollection->getSelect()->group('main_table.product_id');
        $itemCollection->load();
        return $itemCollection;
    }
    protected function getListProductHasReview($customerId){
        $result = [];
        $reviews = $this->getCustomerReviewsCollection($customerId);
        foreach($reviews->getItems() as $review) {
            if ($review->getProductId()) {
                $productType = $review->getProductType();
                $productId = $review->getProductId();
                if (\Magenest\Groupon\Helper\Data::isDeal($productId)) {
                    array_push($result, $productId);
                    $dealCollection = $this->_dealFactory->create()
                        ->getCollection()
                        ->addFieldToFilter('parent_id', $productId);
                    foreach($dealCollection->getItems() as $item) {
                        array_push($result, $item->getProductId());
                    }
                }elseif($productType === 'coupon'){
                    $deal = $this->_dealFactory->create()->load($productId, 'product_id');
                    $parentId = $deal->getParentId();
                    array_push($result, $parentId);
                    $dealCollection = $this->_dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $parentId);
                    foreach($dealCollection->getItems() as $item) {
                        array_push($result, $item->getProductId());
                    }
                }else{
                    array_push($result, $productId);
                }
            }
        }
        return $result;
    }
    public function getCustomerReviewsCollection($customerId){
        $collection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($collection);
        $this->orderItems($collection);
        $this->orders($collection);
        $collection->addStoreFilter($this->storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [10,7]])
            ->addCustomerFilter($customerId)
            ->setDateOrder()
            ->addRateVotes();
        return $collection;
    }
    public function shipments($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['sm' => 'sales_shipment'],
                "main_table.rel_entity_pk_value = sm.entity_id",
                ['review_entity_id' => 'main_table.entity_id','shipment_order_id' => 'sm.order_id', '*']
            );
        return $collection;
    }

    public function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'sales_order_item'],
                "main_table.rel_entity_pk_value = soi.item_id"
            );
        return $collection;
    }

    public function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => 'sales_order'],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );
        return $collection;
    }

    public function createRma($customerId,$requestParams){
        if($requestParams){
            try{
                $order_id = $requestParams['order_id'];
                $dataParams = $requestParams;
                $rmas = $this->initRma($order_id,$dataParams);
                $dataParams['send_email'] = true;
                $comment = '';
                if (empty($rmas)) {
                    throw new \Exception('Return could not be created');
                }
                foreach ($rmas as $rma) {
                    $order = $rma->getOrder();
                    $rma->register();
                }
                if (!empty($dataParams['comment_text'])) {
                    foreach ($rmas as $rma) {
                        $rma->addComment($dataParams['comment_text'], true, true);
                    }
                    $comment = $dataParams['comment_text'];
                }

                if (!empty($dataParams['send_email'])) {
                    foreach ($rmas as $rma) {
                        $rma->setEmailSent(true);
                    }
                }
                $rma->setRmaReason(@$dataParams['rma_reason']);
                $order->setCustomerNoteNotify(!empty($dataParams['send_email']));
                $order->setIsInProcess(true);
                $trans = $this->_hlp->transactionFactory()->create();
                foreach ($rmas as $rma) {
                    $rma->setIsCutomer(true);
                    $trans->addObject($rma);
                }
                $trans->addObject($rma->getOrder())->save();
                foreach ($rmas as $rma) {
                    $this->_unirgyRmaHelper->sendNewRmaNotificationEmail($rma, $comment);
                }
                return true;
            }catch (\Exception $exception){
                $this->debugHelper->debugString('errorCreateRma',$exception->getMessage());
                return false;
            }
        }
    }
    public function initRma($order_id,$dataParams){
        try{
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_orderFactory->create()->load($order_id);
            if (!$order->getId()) {
                throw new \Exception(__('The order no longer exists.'));
            }
            if(isset($dataParams['items'])){
                $qtys = $dataParams['items'];
            }else{
                $qtys = [];
            }
            if(isset($dataParams['items_condition'])){
                $conditions = $dataParams['items_condition'];
            }else{
                $conditions = [];
            }
            $rma = $this->_hlp->createObj('Unirgy\Rma\Model\ServiceOrder', ['order'=>$order])->prepareRmaForSave($qtys, $conditions);
            return $rma;
        }catch (\Exception $e){
            $this->debugHelper->debugString('errorInitRma',$e->getMessage());
            return false;
        }
    }

}