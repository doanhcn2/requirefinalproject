<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model;

use Magenest\OneSignal\Model\ReadStatus;
use Magenest\VendorApi\Api\NotificationRepositoryInterface;

class NotificationRepository implements NotificationRepositoryInterface{
    /**
     * @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\CollectionFactory
     */
    protected $statusCollectionFactory;
    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;
    /**
     * @var \Magenest\OneSignal\Model\DeviceFactory
     */
    protected $_deviceFactory;
    /**
     * @var \Magenest\OneSignal\Model\ReadStatusFactory
     */
    protected $_readStatusFactory;
    /**
     * @var \Magenest\OneSignal\Model\NotificationFactory
     */
    protected $_notificationFactory;
    /**
     * NotificationRepository constructor.
     * @param \Magenest\OneSignal\Model\ResourceModel\ReadStatus\CollectionFactory $statusCollectionFactory
     */
    public function __construct(
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\OneSignal\Model\ResourceModel\ReadStatus\CollectionFactory $statusCollectionFactory,
        \Magenest\OneSignal\Model\ReadStatusFactory $readStatusFactory,
        \Magenest\OneSignal\Model\NotificationFactory $notificationFactory,
        \Magenest\OneSignal\Model\DeviceFactory $deviceFactory
    ){
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->_deviceFactory = $deviceFactory;
        $this->_readStatusFactory = $readStatusFactory;
        $this->_notificationFactory = $notificationFactory;
    }

    /**
     * @param string $customerId
     * @return array
     */
    public function getListByCustomer($customerId,$size){
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $results = $statusCollection->getTop50NotificationByCustomerId($customerId,$size);
        $dataResult = array();
        foreach ($results as $result){
            $dataResult[] = $result->getData();
        }
        return $dataResult;
    }

    public function getUnreadListByCustomer($customerId){
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $dataResult = array();

        $unread = $statusCollection->loadUnreadByCustomerId($customerId)->getSize();
        $dataResult['count'] = $unread;
        return $dataResult;
    }

    public function createDeviceIdForCustomer($customerId,$deviceId){
        $device = $this->_deviceFactory->create();
        $deviceCollection = $device->load($deviceId,'device_id');
        if($deviceCollection->getId()&&$deviceCollection->getCustomerId()==$customerId){
            return false;
        }else{
            $result = $device->setData(
                'customer_id',$customerId
            )->setData(
                'device_id',$deviceId
            )->save();
            if($result){
                return true;
            }else{
                return false;
            }
        }
    }
    public function deleteDeviceIdForCustomer($customerId,$deviceId){
        $device = $this->_deviceFactory->create();
        $deviceCollection = $device->load($deviceId,'device_id');
        if($deviceCollection->getId()&&$deviceCollection->getCustomerId()==$customerId){
            if($deviceCollection->delete()){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function changeStatusNotificationForCustomer($customerId, $notifId, $isLocalId){
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $item = $statusCollection->getStatusNotificationForCustomer($customerId, $notifId, $isLocalId);
        if($item->getId()){
            $readStatus = $this->_readStatusFactory->create()->load($item->getId());
            $result = $readStatus->setData('status',1)->save();
            if($result){
                return true;
            }else{
                return false;
            }
        }else{
            return "Data doesn't exist";
        }
    }

    public function deleteNotificationByCustomer($customerId, $notifId, $isLocalId){
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $item = $statusCollection->getStatusNotificationForCustomer($customerId, $notifId, $isLocalId);
        if($item->getId()){
            $readStatus = $this->_readStatusFactory->create()->load($item->getId())->delete();
            $filter = $isLocalId ? 'id' : 'notif_id';
            $status = $this->_notificationFactory->create()->load($notifId,$filter)->delete();
            if($readStatus&&$status){
                return true;
            }else{
                return false;
            }
        }else{
            return "Data doesn't exist";
        }
    }
}