<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/13/18
 * Time: 7:07 PM
 */

namespace Magenest\VendorApi\Model\Udvendor;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipMicrosite\Model\Source as ModelSource;
use Unirgy\Dropship\Model\Source;

class UdvendorApi implements \Magenest\VendorApi\Api\UdvendorApiInterface
{
    protected $_hlp;
    protected $_msHlp;
    protected $_scope;
    protected $_scopeConfig;
    protected $_objectFactory;
    protected $_isVendorMembershipEnabled;

    public function __construct(
        \Unirgy\Dropship\Helper\Data $helper,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Framework\Config\ScopeInterface $scope,
        \Magento\Framework\Module\Manager $moduleManager,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_hlp = $helper;
        $this->_scope = $scope;
        $this->_msHlp = $micrositeHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_objectFactory = $objectFactory;
        $this->_isVendorMembershipEnabled = $moduleManager->isEnabled('Unirgy_DropshipVendorMembership');
    }

    /**
     * {@inheritdoc}
     */
    public function signup($request)
    {
        try {
            $this->_scope->setCurrentScope('frontend');
            /** @var \Unirgy\DropshipMicrosite\Model\Registration $reg */
            $reg = $this->_hlp->createObj('\Unirgy\DropshipMicrosite\Model\Registration')
                ->setData($request);

            if ($this->_isVendorMembershipEnabled) {
                $membership = ObjectManager::getInstance()->create('Unirgy\DropshipVendorMembership\Model\Membership')
                    ->load(@$request['udmember']['membership']);

                if (!$membership->getId()) {
                    throw new \Exception(__('Unknown membership'));
                }

                $reg->setData('udmember_limit_products', $membership->getLimitProducts());
                $reg->setData('udmember_membership_code', $membership->getMembershipCode());
                $reg->setData('udmember_membership_title', $membership->getMembershipTitle());
                $reg->setData('udmember_allow_microsite', $membership->getAllowMicrosite());
                $reg->setData('udmember_billing_type', $membership->getBillingType());
                if (!in_array($membership->getBillingType(), ['paypal'])) {
                    $reg->setData('udmember_profile_sync_off', 1);
                }
            }

            $reg->validate()
                ->save();
            if (!$this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE)) {
                $this->_msHlp->sendVendorSignupEmail($reg);
            }
            $this->_msHlp->sendVendorRegistration($reg);

            if ($this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE)) {
                /** @var \Unirgy\Dropship\Model\Vendor $vendor */
                $vendor = $reg->toVendor();
                $vendor->setStatus(Source::VENDOR_STATUS_INACTIVE);
                if ($this->_scopeConfig->getValue('udropship/microsite/auto_approve', ScopeInterface::SCOPE_STORE) == ModelSource::AUTO_APPROVE_YES_ACTIVE
                ) {
                    $vendor->setStatus(Source::VENDOR_STATUS_ACTIVE);
                }
                if (!$this->_scopeConfig->isSetFlag('udropship/microsite/skip_confirmation', ScopeInterface::SCOPE_STORE)) {
                    $vendor->setSendConfirmationEmail(1);
                    $vendor->save();
                    return $this->_objectFactory
                        ->addData([
                            'result' => 'Waiting to confirm',
                            'api_status' => 1
                        ]);
                } else {
                    $vendor->save();
                    $vendorInfo = $vendor->getData();
                    return $this->_objectFactory
                        ->addData([
                            'result' => 'Done',
                            'vendor_info' => $this->_objectFactory->addData($vendorInfo),
                            'api_status' => 1
                        ]);
//                    ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->loginById($vendor->getId());
//                    if (!$this->_getVendorSession()->getBeforeAuthUrl()) {
//                        $promoteProduct = $vendor->getData('promote_product');
//                        switch ($promoteProduct) {
//                            case '1':
//                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('register/vendor/location'));
//                                break;
//                            case '2':
//                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-HotelBooking', 'type_of_product' => 'HotelBooking')));
//
//                                break;
//                            case '3':
//                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udprod/vendor/productNew', array('set_id' => '4-Event', 'type_of_product' => 'Event')));
//                                break;
//                            default :
//                                $this->_getVendorSession()->setBeforeAuthUrl($this->_url->getUrl('udropship'));
//                        }
//                    }
                }
            } else {
                return $this->_objectFactory
                    ->addData([
                        'result' => 'Waiting to be approved',
                        'api_status' => 1
                    ]);
            }
        } catch (\Exception $e) {
            return $this->_objectFactory
                ->addData([
                    'result' => $e->getMessage(),
                    'api_status' => 0
                ]);;
        }
    }
}
