<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Model\VendorComponent;


use Magenest\VendorApi\Api\Data\VendorComponent\QuestionParamsInterface;
use Magenest\VendorApi\Api\VendorComponentApiInterface;
use Magenest\VendorApi\Helper\VendorComponent\QuestionHelper;
use Magenest\VendorApi\Helper\DebugHelper;
use Magenest\VendorApi\Helper\VendorComponent\ReviewHelper;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Unirgy\DropshipMultiPrice\Exception;

/**
 * Class OrderInformationInterface
 * @package Magenest\VendorApi\Model\Groupon
 */
class QuestionApi implements VendorComponentApiInterface
{
    /**
     * @var $_reviewHelper \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper
     */
    protected $_reviewHelper;

    /**
     * @var QuestionHelper
     */
    protected $_questionHelper;

    /**
     * @var DataObject
     */
    protected $_objectFactory;

    /**
     * @var DebugHelper
     */
    protected $_debugHelper;
    /**
     * Catalog product model
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * Review model
     *
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $reviewFactory;
    /**
     * Core model store manager interface
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Rating model
     *
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $ratingFactory;

    /**
     * @var \Magenest\VendorApi\Helper\OrderHelper
     */
    protected $orderHelper;
    /**
     * @var \Magenest\FixUnirgy\Block\Customer\QAForm
     */
    protected $qaForm;
    /**
     * QuestionApi constructor.
     * @param DebugHelper $debugHelper
     * @param DataObject $objectFactory
     * @param QuestionHelper $conditionHelper
     * @param StoreManagerInterface $storeManager
     * @param ReviewHelper $reviewHelper
     */
    public function __construct(
        DebugHelper $debugHelper,
        DataObject $objectFactory,
        QuestionHelper $conditionHelper,
        StoreManagerInterface $storeManager,
        ReviewHelper $reviewHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magenest\VendorApi\Helper\OrderHelper $orderHelper,
        \Magenest\FixUnirgy\Block\Customer\QAForm $qaForm
    ){
        $this->_reviewHelper = $reviewHelper;
        $this->_questionHelper = $conditionHelper;
        $this->_debugHelper = $debugHelper;
        $this->_objectFactory = $objectFactory;
        $this->productRepository = $productRepository;
        $this->reviewFactory = $reviewFactory;
        $this->storeManager = $storeManager;
        $this->ratingFactory = $ratingFactory;
        $this->orderHelper = $orderHelper;
        $this->qaForm = $qaForm;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllVendorQuestionsByProduct($product_id, $cur_page = null, $size = null)
    {
        // TODO: Implement getAllVendorQuestionsByProduct() method.
        try {
            $productQuestion = $this->_questionHelper->getAllApprovedQuestionByProduct($product_id, $cur_page, $size);
            return $this->_objectFactory->addData([
                'question_by_product' => $productQuestion,
                'question_by_customer' => null,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllVendorQuestionsByCustomer($customerId, $cur_page = null, $size = null)
    {
        // TODO: Implement getAllVendorQuestionsByCustomer() method.
        try {
            $customerQuestion = $this->_questionHelper->getAllApprovedQuestionByCustomer($customerId, $cur_page, $size);
            return $this->_objectFactory->addData([
                'question_by_product' => null,
                'question_by_customer' => $customerQuestion,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllVendorQuestions($vendor_id, $cur_page = null, $size = null)
    {
        // TODO: Implement getAllVendorQuestions() method.
        try {
            $customerQuestion = $this->_questionHelper->getAllApprovedQuestion($vendor_id, $cur_page, $size);
            return $this->_objectFactory->addData([
                'question_by_vendor' => $customerQuestion,
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function askQuestionsForVendor($customerId, $requestParams)
    {
        // TODO: Implement askQuestionsForVendor() method.
        $result = false;
        try {
            $askQuestions = $this->_questionHelper->askVendorQuestions($customerId, $requestParams);
            return $this->_objectFactory->addData([
                'result' => (boolean)$askQuestions
            ]);
        } catch (\Exception $e) {
            return $this->_objectFactory->addData([
                'result' => (boolean)$result
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMyProductReview($customerId, $cur_page = null, $size = null)
    {
        // TODO: Implement getMyProductReview() method.
        try {
            $customerReviews = $this->_reviewHelper->getAllCustomerReviewForProduct($customerId, $cur_page, $size);
            return $this->_objectFactory->addData([
                'general' => $customerReviews[1],
                'review_by_product' => $customerReviews[0],
                'api_status' => 1
            ]);
        } catch (\Exception $e) {
            $this->_debugHelper->debugString('product', $e->getMessage());
            return $this->_objectFactory
                ->addData([
                    'api_status' => 0
                ]);
        }
    }

    public function createProductReview($customerId,$productId,$requestParams){
        $result = false;
        $product = $this->_reviewHelper->loadProduct($productId);
        if($product && !empty($requestParams)){
            $data = $requestParams;
            $rating = $data['ratings'];
            /** @var \Magento\Review\Model\Review $review */
            $review = $this->reviewFactory->create()->setData($data);
            $review->unsetData('review_id');
            $validate = $review->validate();
            if($validate === true){
                $review->setEntityId($review->getEntityIdByCode('product'))
                    ->setEntityPkValue($product->getId())
                    ->setStatusId(2)
                    ->setCustomerId($customerId)
                    ->setStoreId($this->storeManager->getStore()->getId())
                    ->setStores([$this->storeManager->getStore()->getId()])
                    ->save();
                foreach ($rating as $ratingId => $optionId) {
                    $this->ratingFactory->create()
                        ->setRatingId($ratingId)
                        ->setReviewId($review->getId())
                        ->setCustomerId($customerId)
                        ->addOptionVote($optionId, $product->getId())->save();
                }
                $review->aggregate();
                $result = true;
            }else{
                $result = false;
            }
        }else{
            $result = false;
        }
        return [$result];
    }

    public function filterQuestionByCustomerId($customerId, $requestParams)
    {
        if (isset($requestParams)) {
            $filterby = isset($requestParams['filter_by']) ? $requestParams['filter_by'] : '';
            $datafilter = isset($requestParams['data_filter']) ? $requestParams['data_filter'] : '';
            $sort = isset($requestParams['sort']) ? $requestParams['sort'] : [];
            $cur_page = isset($requestParams['cur_page']) ? $requestParams['cur_page'] : 1;
            $size = isset($requestParams['size']) ? $requestParams['size'] : 100;
            $result = $this->_questionHelper->filterCustomerQuestion($customerId, $filterby, $datafilter, $cur_page, $size, $sort);
            $this->_debugHelper->debugString('resultFilterQuestion',json_encode($result));
            $objectData = $this->_objectFactory;
            $objectData->unsetData();
            return $objectData->addData([
                'result' => $result
            ]);
        } else {
            return false;
        }
    }

    public function filterQuestByCustomerId($customerId, $requestParams)
    {
        if (isset($requestParams)) {
            $filterBy = isset($requestParams['filter_by']) ? $requestParams['filter_by'] : '';
            $dataFilter = isset($requestParams['data_filter']) ? $requestParams['data_filter'] : '';
            if (isset($dataFilter) && isset($filterBy)) {
                $filterParams = json_encode(['filter_by' => $filterBy, 'data_filter' => $dataFilter]);
            } else {
                $filterParams = '';
            }
            $sort = isset($requestParams['sort']) ? $requestParams['sort'] : [];
            if (isset($sort)) {
                $sortParams = json_encode(['sort_by' => $sort['sortby'], 'sort_type' => $sort['sorttype']]);
            } else {
                $sortParams = '';
            }
            $cur_page = isset($requestParams['cur_page']) ? $requestParams['cur_page'] : 1;
            $size = isset($requestParams['size']) ? $requestParams['size'] : 100;
            $result = $this->_questionHelper->filterQuestByCusId($customerId, $cur_page, $size, $sortParams, $filterParams);
            return $this->_objectFactory->addData([
                'result' => $result
            ]);
        } else {
            return false;
        }
    }

    public function filterQuesByCusId($customer)
    {
        try {
            $cur_page = (isset($_GET['cur_page']) && is_numeric($_GET['cur_page']))? (int)$_GET['cur_page'] : 1;
            $size = (isset($_GET['size']) && is_numeric($_GET['size']))? (int)$_GET['size'] : 100;
            $sort_by = isset($_GET['sort_by']) ? $_GET['sort_by'] : '';
            $sort_type = isset($_GET['sort_type']) ? $_GET['sort_type'] : '';
            $filter_by = isset($_GET['filter_by']) ? $_GET['filter_by'] : '';
            $data_filter = isset($_GET['data_filter']) ? $_GET['data_filter'] : '';
            $result = $this->_questionHelper->filterQuestByCusId($customer, $cur_page, $size, $sort_by, $sort_type, $filter_by, $data_filter);
            return $this->_objectFactory->addData(['result' => $result]);
        } catch (\Exception $e) {
            return $this->_objectFactory->addData(['result' => [$e->getMessage()]]);
        }
    }
    public function getFormAskQuestion($customerId){
        $result = [];
        $orderCollection = $this->orderHelper->getOrderCollectionByCustomerId($customerId);
        $orderVendor = $this->qaForm->getOrderVendors($orderCollection);
        $vendor = $this->qaForm->getVendors();
        foreach ($orderCollection as $order){
            $order_id = $order->getId();
            $increment_id = $order->getIncrementId();
            $vendor_in_order = [];
            foreach ($orderVendor as $key => $_vendor){
                if (count(@$_vendor['items'] >= 1) && $key == $order_id){
                    $data_vendor = [];
                    foreach ($_vendor['items'] as $_v){
                        if (!empty(@$vendor[$_v])){
                            $data_vendor[] = [
                                'vendor_id' => $_v,
                                'vendor_name' => $vendor[$_v]
                            ];
                        }
                    }
                    $vendor_in_order = $data_vendor;
                }
            }
            $data = [
                'order_id' => $order_id,
                'increment_id' => $increment_id,
                'vendor' => $vendor_in_order
            ];
            $result['order'][] = $data;
        }
        foreach ($vendor as $vId => $vName){
            $data = [
                'vendor_id' => $vId,
                'vendor_name' => $vName
            ];
            $result['vendor'][] = $data;
        }
        return $this->_objectFactory->addData([
            'ask_order' => $result['order'],
            'ask_vendor' => $result['vendor']
        ]);
    }
}
