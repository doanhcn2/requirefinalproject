<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Model\VendorComponent;

use Magenest\VendorApi\Api\Data\VendorComponent\QuestionParamsInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class QuestionParams extends AbstractSimpleObject implements QuestionParamsInterface
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * @return int
     */
    public function getVendorId()
    {
        // TODO: Implement getVendorId() method.
        return $this->_get(self::VENDOR_ID);
    }

    /**
     * @param int $vendorId
     * @return $this
     */
    public function setVendorId($vendorId)
    {
        // TODO: Implement setVendorId() method.
        return $this->setData(self::VENDOR_ID, $vendorId);
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        // TODO: Implement getQuestion() method.
        return $this->_get(self::QUESTION);
    }

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question)
    {
        // TODO: Implement setQuestion() method.
        return $this->setData(self::QUESTION, $question);
    }
}