<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:22
 */

namespace Magenest\VendorApi\Helper;
use Magento\Framework\App\ObjectManager;

/**
 * Class StoreHelper
 * @package Magenest\VendorApi\Helper
 */
class StoreHelper
{
    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_categoryHelper;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var ImageHelper
     */
    protected $_imageHelper;

    /**
     * @var DebugHelper
     */
    protected $_debugHelper;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $_currencySymbolFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\MediaStorage\Helper\File\Storage\Database
     */
    protected $_fileStorageHelper;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\Catalog\Api\CategoryManagementInterface
     */
    protected $_categoryManagement;

    protected $_cmsPageFactory;
    protected $categoryRepository;

    /**
     * StoreHelper constructor.
     * @param \Magento\Catalog\Api\CategoryManagementInterface $categoryManagement
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Locale\CurrencyInterface $currencySymbolFactory
     * @param DebugHelper $debugHelper
     * @param ImageHelper $imageHelper
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Catalog\Api\CategoryManagementInterface $categoryManagement,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Helper\File\Storage\Database $fileStorageHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\CurrencyInterface $currencySymbolFactory,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magenest\VendorApi\Helper\ImageHelper $imageHelper,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository
    ) {
        $this->_categoryManagement = $categoryManagement;
        $this->_filesystem = $filesystem;
        $this->_fileStorageHelper = $fileStorageHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_currencySymbolFactory = $currencySymbolFactory;
        $this->_debugHelper = $debugHelper;
        $this->_imageHelper = $imageHelper;
        $this->_storeFactory = $storeFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->_cmsPageFactory = $pageFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return array|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllStoreData()
    {
        $storeData = [];
        $result = null;
        /** @var \Magento\Store\Model\Store $store */
        $store = $this->_storeFactory->create()
            ->getCollection()->addFieldToFilter('store_id', 1)->getFirstItem();
        /** @var \Ves\Megamenu\Model\MenuFactory $vesMenuModel */
        $vesMenuModel = ObjectManager::getInstance()->get(\Ves\Megamenu\Model\MenuFactory::class);
        /** @var \Ves\Megamenu\Block\Menu $vesMenuBlock */
        $vesMenuBlock = ObjectManager::getInstance()->get(\Ves\Megamenu\Block\Menu::class);

        $menuCollection = $vesMenuModel->create()->getCollection()->addFieldToSelect('menu_id')
            ->addFieldToFilter('alias', 'top-menu')
            ->addFieldToFilter('status', '1')
            ->addFieldToFilter('desktop_template', 'horizontal')
            ->getFirstItem();
        $menuData = false;
        if(isset($menuCollection))
            $menuData = $vesMenuBlock->getMenuProfile($menuCollection->getData('menu_id'));
        $baseCurrencyCode = $store->getBaseCurrencyCode();
        $vesBlockModel = \Magento\Framework\App\ObjectManager::getInstance()->get(\Ves\PageBuilder\Model\Block::class);

        $currencyData = [];
        $currencies = $store->getAvailableCurrencyCodes();
        foreach ($currencies as $currency) {
            $symbolModel = $this->_currencySymbolFactory->getCurrency($currency);
            $currentSymbol = $symbolModel->getSymbol();
            if (!$currentSymbol) {
                $currentSymbol = $currency;
            }
            $currencyData[] = [
                'code' => $currency,
                'symbol' => $currentSymbol,
                'name' => $symbolModel->getName(),
                'rate' => $store->getBaseCurrency()->getRate($currency)
            ];
        }

        $mediaPath = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $language = $this->_scopeConfig->getValue('general/locale/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getEntityId());
        $google_ua = $this->_scopeConfig->getValue('google/analytics/account', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store->getEntityId());
        $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
        $storeLogoPath = $this->_scopeConfig->getValue(
            'design/header/logo_src',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $path = $folderName . '/' . $storeLogoPath;
        $logoUrl = $store
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;
        $rootCategory = $this->_categoryFactory->create()
            ->load($store->getRootCategoryId());

        $categoryItems = $this->_categoryManagement->getTree($rootCategory->getEntityId(), $this->_categoryManagement->getCount());

        if($menuData) {
            $categoriesData = $this->getCategoryByMenu($menuData, $vesBlockModel, $store, $categoryItems);
        }
        else {
            $rootCategory = $this->_categoryFactory->create()
                ->load($store->getRootCategoryId());
            $categoriesData = [];
            $categoryItems = $this->_categoryManagement->getTree($rootCategory->getEntityId(), $this->_categoryManagement->getCount());
            foreach($categoryItems->getChildrenData() as $item) {
                $categoryInfo = $this->getCategoryType($item->getEntityId());
                $categoryType = $categoryInfo['category_type'];
                if ($item->getIsActive()) {
                    $currentCategory = $this->_categoryFactory->create()
                        ->load($item->getEntityId());
                    $categoryPage = $vesBlockModel->getCollection()->addFieldToSelect('*')
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('category_id', $item->getId())->getFirstItem();
                    $catagoryPageResult = [];
                    if(count($categoryPage->getData()) > 0){
                        $catagoryPageResult = $this->getPageResult($categoryPage->getData('block_id'), $store->getId());
                    }
                    $children_data_temp = $this->getChildrenData($item, 'in ' . $item->getName());
                    if (sizeof($children_data_temp) > 0) {
                        $categoriesData[] = [
                            'name' => $item->getName(),
                            'id' => $item->getEntityId(),
                            'children' => $children_data_temp,
                            'address' => 'in ' . $item->getName(),
                            'categoryPage' => $catagoryPageResult,
                            'category_type' => $categoryType
                        ];
                    } else {
                        $categoriesData[] = [
                            'name' => $item->getName(),
                            'id' => $item->getEntityId(),
                            'address' => 'in ' . $item->getName(),
                            'categoryPage' => $catagoryPageResult,
                            'category_type' => $categoryType
                        ];
                    }
                }
            }
        }
        $storeData[] = [
            'id' => $store->getId(),
            'name' => $store->getName(),
            'base_currency' => $baseCurrencyCode,
            'category' => $categoriesData,
            'currency' => $currencyData,
            'language' => $language,
            'logo' => $logoUrl,
            'flag_icon' => $logoUrl,
            'google_ua' => $google_ua,
            'media_url' => $mediaPath,
        ];
        if (sizeof($storeData) > 0) {
            $result = [
                'api_status' => 1,
                'store' => $storeData
            ];
        } else {
            $result =  [
                'api_status' => 0
            ];
        }
        return $result;
    }

    /**
     * @param $menuData \Ves\Megamenu\Model\Menu
     * @param $vesBlockModel \Ves\PageBuilder\Model\Block
     * @return []
     */
    public function getCategoryByMenu($menuData, $vesBlockModel, $store, $categoryItems)
    {
        $flag = true;
        $categoriesData = [];
        $menuItems = $menuData->getData('menuItems');
        /** @var \Ves\Megamenu\Model\Item $menuItem */
        foreach($menuItems as $menuItem) {
            $categoryInformation = $this->getCategoryType($menuItem['category']);
            $categoryType = $categoryInformation['category_type'];
            $categoryPath = $categoryInformation['category_path'];
            if ($menuItem['link_type'] == 'category_link') {
//                $categoryPage = $vesBlockModel->getCollection()->addFieldToSelect('*')
//                    ->addFieldToFilter('status', 1)
//                    ->addFieldToFilter('category_id', $menuItem['category'])->getFirstItem();
//                $catagoryPageResult = [];
                $catagoryPageResult = $this->getPageResult(1, 1);
//                if (count($categoryPage->getData()) > 0) {
//                    $catagoryPageResult = $this->getPageResult($categoryPage->getData('block_id'), $store->getId());
//                }
                /** @var \Magento\Catalog\Model\Category $cats */
                $cats = $this->_categoryFactory->create()->load($menuItem['category']);
                $children = $cats->getChildren(true);
                if ($children) {
                    $children_data_temp = $this->getChildrenData($children, 'in ' . $menuItem['name']);
                    $categoriesData[] = [
                        'name' => $menuItem['name'],
                        'id' => $menuItem['category'],
                        'address' => "in " . $categoryPath,
                        'categoryPage' => count($catagoryPageResult) > 0 ? $catagoryPageResult : null,
                        'children' => $children_data_temp,
                        'category_type' => $categoryType
                    ];
                } else {
                    $categoriesData[] = [
                        'name' => $menuItem['name'],
                        'id' => $menuItem['category'],
                        'address' => "in " . $categoryPath,
                        'categoryPage' => count($catagoryPageResult) > 0 ? $catagoryPageResult : null,
                        'category_type' => $categoryType
                    ];
                }
            } elseif ($menuItem['link_type'] == 'custom_link' && $flag) {
                $flag = false;
                $categoryPage = $vesBlockModel->getCollection()->addFieldToSelect('*')
                    ->addFieldToFilter('status', 1)
                    ->addFieldToFilter('alias', 'homepage')->getFirstItem();
//                $catagoryPageResult = [];
                if (count($categoryPage->getData()) > 0) {
                    $catagoryPageResult = $this->getPageResult($categoryPage->getData('block_id'), $store->getId());

                    $categoriesData[] = [
                        'name' => 'homepage',
                        'id' => -1,
                        'categoryPage' => count($catagoryPageResult) > 0 ? $catagoryPageResult : null,
                        'category_type' => -1
                    ];
                }
            }
        }
        return $categoriesData;
    }

    public function getPageResult($pageId, $store){
        $pageResult = ObjectManager::getInstance()->get(\Magenest\VendorApi\Model\PageBuilder\PageBuilderManagement::class)
            ->getPageBuilderById($pageId, $store)->getData();
        return count($pageResult) > 0 ? $pageResult : null;
    }
    public function getChildrenData($children, $address,$xxx = array())
    {
        $result = [];
        $childrenArrays = explode(",",$children);
        $id = $this->getChildrenId($children);
        foreach ($childrenArrays as $childrenArray){
            $categoryInfo = $this->getCategoryType($childrenArray);
            $categoryType = $categoryInfo['category_type'];
            if(in_array($childrenArray,$id)){
                continue;
            }
            $childrenData = $this->_categoryFactory->create()->load($childrenArray);
            $children = $childrenData->getChildren(true);
            if($children){
                $yyy = explode(",",$children);
                $xxx = array_merge($yyy,$xxx);
                $children_data_temp = $this->getChildrenData($children, 'in ' . $childrenData->getName(),$xxx);
                $result[] = [
                    'name' => $childrenData->getName(),
                    'id' => $childrenData->getEntityId(),
                    'children' => $children_data_temp,
                    'address' => $address . ' -> ' . $childrenData->getName(),
                    'category_type' => $categoryType
                ];
            }
            else{
                $result[] = [
                    'name' => $childrenData->getName(),
                    'id' => $childrenData->getEntityId(),
                    'address' => $address . ' -> ' . $childrenData->getName(),
                    'category_type' => $categoryType
                ];
            }

        }
        return $result;
    }
    public function getChildrenId($children,$arr = array()){
        $childrenArrays = explode(",",$children);
        foreach ($childrenArrays as $childrenArray){
            $childrenData = $this->_categoryFactory->create()->load($childrenArray);
            $children = $childrenData->getChildren(true);
            if($children){
                $yyy = explode(",",$children);
                $arr = array_merge($arr,$yyy);
                $this->getChildrenId($children,$arr);
            }
        }
        return $arr;
    }
    protected function _isFile($filename)
    {
        if ($this->_fileStorageHelper->checkDbUsage() && !$this->getMediaDirectory()->isFile($filename)) {
            $this->_fileStorageHelper->saveFileToFilesystem($filename);
        }

        return $this->getMediaDirectory()->isFile($filename);
    }

    protected function getMediaDirectory()
    {
        return $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
    }

    public function getCategoryType($categoryId)
    {
        $category = $this->_categoryFactory->create()->getCollection()
            ->addAttributeToSelect('category_type')
            ->addFieldToFilter('entity_id', (int)$categoryId)->getFirstItem();
        $categoryType = $category->getCategoryType();
        $path = $category->getPath();
        $categoryPath = $this->getCategoryBreadcrumb($path);
        if ($categoryType || $categoryPath) {
            return ['category_type' => (int)$categoryType, 'category_path' => $categoryPath];
        }

        return 1;
    }

    protected function getCategoryBreadcrumb($path)
    {
        $pathCut = substr($path, 4);
        $pathExplode = explode('/', $pathCut);
        $result = "";
        foreach ($pathExplode as $item) {
            $categoryName = $this->_categoryFactory->create()->load($item)->getName();
            if ($result !== "") {
                $result .= " -> " . $categoryName;
            } else {
                $result .= $categoryName;
            }
        }

        return $result;
    }
}