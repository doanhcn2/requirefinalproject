<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper\VendorComponent;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\App\Filesystem\DirectoryList;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Helper\Data as DataHelper;
use Magenest\Groupon\Model\StatusFactory;
use Magenest\Ticket\Model\TicketFactory;
use Unirgy\DropshipPo\Model\Source as ShipmentStatus;

class VendorHepler
{
    const VENDOR_GROUPON = 1;
    const VENDOR_HOTEL = 2;
    const VENDOR_EVENT = 3;
    const VENDOR_PRODUCT = 4;

    protected $_vendorFactory;
    protected $grouponFactory;
    protected $dealActive;
    protected $dealFactory;
    protected $parentIds = [];
    protected $ticketFactory;
    protected $sessionFactory;
    /**
     * @var $_question QuestionFactory
     */
    protected $_rateHlp;

    /**
     * @var \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection
     */
    protected $_reviewCollection;
    protected $grouponTodayCollection = null;
    protected $activeCampaignCollectionIds;
    /** @var \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\CollectionFactory */
    protected $creditmemoItemCollectionFactory;

    protected $payout;
    /**
     * @var $_question QuestionFactory
     */
    protected $_question;
    protected $productFactory;
    protected $_magentoEventFactory;
    protected $_dealInfoFactory;
    protected $_magenestReviewHelper;
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;
    private $productLoad = [];
    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    protected $grouponCollectionFactory;
    /**
     * @var \Unirgy\DropshipPo\Model\ResourceModel\Po\CollectionFactory
     */
    protected $_poCollectionFactory;
    /**
     * @var $_orderItemFactory \Magento\Sales\Model\Order\ItemFactory
     */
    protected $_orderItemFactory;
    /**
     * @var $_urmaFactory \Unirgy\Rma\Model\RmaFactory
     */
    protected $_urmaFactory;
    protected $locationFactory;
    protected $deliveryFactory;
    protected $productRepository;
    protected $fileSystem;
    protected $vendorProductCollectionFactory;
    /**
     * @var \Magenest\VendorApi\Helper\DebugHelper
     */
    protected $_debugHelper;

    protected $imagesLoader;
    protected $_productDashboardHelper;
    /**
     * @var \Magenest\Payments\Block\Vendor\Dashboard
     */
    protected $_paymentDashboard;
    protected $_magenestEventTicketHelper;

    public function __construct(
        StatusFactory $statusFactory,
        HelperData $helperData,
        QuestionFactory $questionFactory,
        \Magento\Catalog\Model\Product $product,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Reports\Model\EventFactory $eventFactory,
        \Magenest\Groupon\Model\DealInfoFactory $dealInfoFactory,
        \Magenest\VendorApi\Helper\VendorComponent\ReviewHelper $magenestReviewHelper,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\CollectionFactory $creditmemoItemCollectionFactory,
        \Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory $grouponCollectionFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Unirgy\DropshipPo\Model\ResourceModel\Po\CollectionFactory $poCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\DropshipPo\Model\ResourceModel\Po\Item\CollectionFactory $poItemCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $collectionFactory,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Unirgy\Rma\Model\RmaFactory $rmaFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Filesystem $fileSystem,
        \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\CollectionFactory $collectionVendorProductFactory,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
        \Magenest\Profile\Model\ImageUploadFactory $imagesLoader,
        \Magenest\SellYours\Helper\Dashboard $dashboardSimple,
        \Magenest\Payments\Block\Vendor\Dashboard $paymentDashboard,
        \Magenest\VendorApi\Helper\VendorComponent\EventTicketHelper $magenestEventTicketHelper
    )
    {
        $this->_vendorFactory = $vendorFactory;
        $this->grouponFactory = $grouponFactory;
        $this->dealFactory = $dealFactory;
        $this->_rateHlp = $helperData;
        $this->payout = $payoutFactory;
        $this->creditmemoItemCollectionFactory = $creditmemoItemCollectionFactory;
        $this->_question = $questionFactory;
        $this->productFactory = $productFactory;
        $this->_magentoEventFactory = $eventFactory;
        $this->_dealInfoFactory = $dealInfoFactory;
        $this->_magenestReviewHelper = $magenestReviewHelper;
        $this->grouponCollectionFactory = $grouponCollectionFactory;
        $this->_statusHistory = $statusFactory;
        $this->product = $product;
        $this->ticketFactory = $ticketFactory;
        $this->sessionFactory = $sessionFactory;
        $this->_poCollectionFactory = $poCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_poItemCollectionFactory = $poItemCollectionFactory;
        $this->_saleShipmentCollectionFactory = $collectionFactory;
        $this->_unirgyProductFactory = $unirgyProductFactory;
        $this->_orderItemFactory = $itemFactory;
        $this->_urmaFactory = $rmaFactory;
        $this->locationFactory = $locationFactory;
        $this->deliveryFactory = $deliveryFactory;
        $this->productRepository = $productRepository;
        $this->fileSystem = $fileSystem;
        $this->vendorProductCollectionFactory = $collectionVendorProductFactory;
        $this->_debugHelper = $debugHelper;
        $this->_eavAttribute = $eavAttribute;
        $this->imagesLoader = $imagesLoader;
        $this->_productDashboardHelper = $dashboardSimple;
        $this->_paymentDashboard = $paymentDashboard;
        $this->_magenestEventTicketHelper = $magenestEventTicketHelper;
    }

    public function getVendor($vendorId)
    {
        $vendor = $this->_vendorFactory->create()->load($vendorId);
        $session = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $session->setVendor($vendor);
        return $session->getVendor();
    }

    public function getGrouponTodayCollection($vendorId)
    {
        if (!$this->grouponTodayCollection) {
            $date = new \DateTime();
            $today = $date->format('Y-m-d');
            /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
            $this->grouponTodayCollection = $this->grouponFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('created_at', array('like' => "%$today%"));
        }
        return $this->grouponTodayCollection;
    }

    public function getTicketCollectionSaleToday($vendorId)
    {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        return $this->ticketFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('created_at', array('like' => "%$today%"));
    }

    public function getEventProductCollection($vendorId)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productFactory->create()->getCollection();
        $collection->addFieldToFilter('udropship_vendor', $vendorId)
            ->addFieldToFilter('type_id', 'ticket')->addAttributeToSelect(['campaign_status', 'image']);
        $eventTable = $collection->getTable('magenest_ticket_event');
        $joinStatement = $collection->getSelect()->join(
            $eventTable,
            "entity_id=$eventTable.product_id",
            ["$eventTable.event_id", "$eventTable.event_name", "$eventTable.is_reserved_seating", "$eventTable.venue_map_id"]
        );
        $collection->getConnection()->fetchAll($joinStatement);
        return $collection;
    }

    public function getTicketTodaySale($vendorId)
    {
        $tickets = $this->getTicketCollectionSaleToday($vendorId);
        $total = 0;
        foreach ($tickets as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->sessionFactory->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }
        return $total;
    }

    public function getTicketTodayRedeemed($vendorId)
    {
        $tickets = $this->getTicketCollectionSaleToday($vendorId)->addFieldToFilter('status', 2);
        return count($tickets);
    }

    public function getTicketSoldOutOption($vendorId)
    {
        $productIds = $this->getEventProductCollection($vendorId)->getAllIds();
        if (count($productIds) == 0) return 0;
        $ticketsFactory = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Ticket\Model\Tickets');
        $tickets = $ticketsFactory->getCollection()->addFieldToFilter('product_id', $productIds);
        $sessions = $this->sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', $tickets->getAllIds())
            ->addFieldToFilter("qty_available", 0);
        return count($sessions);
    }

    public function getActiveEventCollection($vendorId)
    {
        return $this->getEventProductCollection($vendorId)->addFieldToFilter("campaign_status", 5);
    }

    public function getActiveEventCollectionIds($vendorId)
    {
        return $this->getActiveEventCollection($vendorId)->getAllIds();
    }
    public function getSoldOutOption($vendorId){
        $ids = $this->getActiveCampaignCollection($vendorId)->getColumnValues('product_id');
        $collection = $this->dealFactory->create()->getCollection()
            ->addFieldToFilter('parent_id', ['in' => $ids])
            ->addFieldToFilter('available_qty', ['lteq' => 1]);
        return count($collection->getData());
    }
    public function getActiveCampaignCollection($vendorId)
    {
        if ($this->dealActive == null) {
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $this->productFactory->create()->getCollection();
            $collection->addFieldToFilter('udropship_vendor', $vendorId)
                ->addFieldToFilter('type_id', 'configurable')->addAttributeToSelect(['campaign_status'])->addFieldToFilter('campaign_status', 5);
            $dealTable = $collection->getTable('magenest_groupon_deal');
            $collection->getSelect()->joinLeft(
                $dealTable,
                "entity_id = $dealTable.product_id",
                "*"
            );
            $this->dealActive = $collection;
        }
        return $this->dealActive;
    }

    public function getActiveCampaignCollectionIds($vendorId)
    {
        if (!$this->activeCampaignCollectionIds) {
            /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $collection */
            $collection = $this->dealFactory->create()->getCollection();
            $this->activeCampaignCollectionIds = $collection
                ->addFieldToFilter('product_type', 'coupon')
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('groupon_expire', array('gteq' => date('Y-m-d')))
                ->addFieldToFilter('enable', '1')->getColumnValues('product_id');
        }
        return $this->activeCampaignCollectionIds;
    }

    public function getPendingBalance()
    {
        return $this->_paymentDashboard->getOwed();
    }

    public function getEarning($vendorId)
    {
        $collection = $this->payout->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId);
        $amount = 0;
        foreach ($collection as $payout) {
            $amount += $payout->getData('total_payout');
        }
        return (float)$amount;
    }

    public function getPaid($vendorId)
    {
        $collection = $this->payout->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('payout_status', 'paid');
        $amount = 0;
        foreach ($collection as $payout) {
            $amount += $payout->getData('total_paid');
        }
        return (float)$amount;
    }

    public function getOwed($vendorId)
    {
        return (float)($this->getEarning($vendorId) - $this->getPaid($vendorId));
    }

    public function getReviewsCollection($vendorId)
    {
        if (null === $this->_reviewCollection) {
            $this->_reviewCollection = $this->_rateHlp->getVendorReviewsCollection($vendorId);
        }
        $total = 0;
        $i = 0;
        foreach ($this->_reviewCollection as $review) {
            $_votes = [];
            foreach ($review->getRatingVotes() as $_vote) {
                if ($_vote->getIsAggregate()) {
                    $_votes[] = $_vote;
                }
            }
            foreach ($_votes as $vote) {
                $total += $vote->getPercent();
                $i++;
            }
        }

        return $i == 0 ? 0 : number_format(($total * 5) / (100 * $i), 1);
    }

    public function getSold($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('refunded_at', array('null' => true));
        /** @var \Magenest\Groupon\Model\Groupon $item */
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count;
    }

    public function getRedeemed($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('status', '2');
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count;
    }

    public function getRemaining($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('status', '1');
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count - $this->getRefunded($ids);
    }

    public function getRefunded($ids)
    {
        $result = $this->_productDashboardHelper->getRefunded($ids);
        return $result;
    }

    public function getPayoutCollection($vendorId)
    {
        $collection = $this->payout->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->setPageSize(3)
            ->setCurPage(1)
            ->setOrder('created_at', 'DES');
        return $collection;
    }
    public function getDayForTicketDashboard($daynum,$vendorId){
        $date = new \DateTime();
        if ($daynum === "all_time") {
            $startTime = $this->getVendor($vendorId)->getCreatedAt();
            if ($startTime === null) {
                $startTime = '2018-01-01 00:00:00';
            }
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);

            return $startTime->format('Y-m-d');
        } elseif ($daynum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));

            return $startTime->format('Y-m-d');
        } else {
            $date->sub(new \DateInterval('P' . $daynum . 'D'));
        }

        return $date->format('Y-m-d');
    }
    public function getOption($daynum, $vendorId)
    {
        $arr = [];
        $summary = [];
        if (is_integer($daynum)) {
            $dayCount = (int)$daynum;
        } else {
            $dayCount = floor((time() - strtotime($this->getDayForTicketDashboard($daynum,$vendorId))) / 86400);
            $dayCount = (int)$dayCount;
        }

        $daystart = new \DateTime();
        $date = $daystart->sub(new \DateInterval('P' . $dayCount . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $productType = $this->getVendor($vendorId)->getPromoteProduct();
        if ($productType == 3) {
            $soldInDays = $this->getTicketSoldInDay($this->getTicketSoldFromDay($dayCount, $vendorId));
            $soldInDaysValue = $this->getTicketSoldInDayValue($this->getTicketSoldFromDay($dayCount, $vendorId));
            $refundInDays = $this->getTicketRefundedInDay($this->getTicketRefundedFromDay($dayCount, $vendorId));
            $refundInDaysValue = $this->getTicketRefundedInDayValue($this->getTicketRefundedFromDay($dayCount, $vendorId));
            $summary = [
                'sales_value' => $this->getEventTicketSaleValue($dayCount, $vendorId),
                'tickets_sold' => count($this->getTicketSoldFromDay($dayCount, $vendorId)),
                'tickets_redeemed' => count($this->getTicketRedeemedFromDay($dayCount, $vendorId)),
                'refunds_value' => $this->getTicketRefundedValueFromDay($dayCount, $vendorId),
                'refunds_quantity' => count($this->getTicketRefundedFromDay($dayCount, $vendorId))
            ];
        } elseif ($productType == 1) {
            $soldInDays = $this->getGrouponSoldInDay($this->getGrouponSoldFromDay($dayCount, $vendorId));
            $soldInDaysValue = $this->getGrouponSoldInDayValue($this->getGrouponSoldFromDay($dayCount, $vendorId));
            $refundInDays = $this->getGrouponRefundedInDay($this->getGrouponRefundedFromDay($dayCount, $vendorId));
            $refundInDaysValue = $this->getGrouponRefundedInDayValue($this->getGrouponRefundedFromDay($dayCount, $vendorId));
            $summary = [
                'sold_vouchers' => count($this->getGrouponSoldFromDay($dayCount, $vendorId)),
                'redeem_vouchers' => count($this->getRedeemedFromDay($dayCount, $vendorId)),
                'campaign_page_view' => $this->getProductGrouponCount($dayCount, $vendorId),
                'customer_feedback_review' => $this->getFeedbackGrouponCount($dayCount, $vendorId),
            ];
        } elseif ($productType == 4) {
            $soldInDays = $this->getProductSoldInDay($this->getProductSoldFromDay($dayCount, $vendorId));
            $soldInDaysValue = $this->getProductSoldInDayValue($this->getProductSoldFromDay($dayCount, $vendorId));
            $refundInDays = $this->getProductRefundedInDay($this->getProductRefundedFromDay($dayCount, $vendorId));
            $refundInDaysValue = $this->getProductRefundedInDayValue($this->getProductRefundedFromDay($dayCount, $vendorId));
            $summary = [
                'sales_value' => $this->getProductSaleValue($dayCount, $vendorId),
                'units_sold' => $this->getProductUnitsSoldFromDay($dayCount, $vendorId),
                'units_fulfilled' => $this->getFulfilledFromDay($dayCount, $vendorId),
                'returns_value' => $this->getProductRefundedValueFromDay($dayCount, $vendorId),
                'returns_quantity' => $this->getProductRefundedFromDayQty($dayCount, $vendorId)
            ];
        }

        for ($i = 0; $i < $dayCount; $i++) {
            if($i == 0){
                $date->add(new \DateInterval('P0D'));
            }else{
                $date->add(new \DateInterval('P1D'));
            }

            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [
                'date_text' => $dateText,
                'sales' => $sales,
                'qty_sale' => $qtySale,
                'refunded' => $refunds,
                'qty_refunded' => $qtyRefund,
                'style' => 'color: #017cb9; stroke-color: #017cb9'
            ];
        }

        return [
            'label' => $daynum,
            'day_number' => $dayCount,
            'start_date' => $startDateText,
            'end_date' => $dateText,
            'result' => $arr,
            'summarry' => $summary
        ];
    }

    // Begin Inventory Detail
    public function getInventoryPaymentChartOption($daynum, $vendorId,$productId){
        $number = $daynum;
        if($daynum == 'all_time'){
            $daynum = $this->getAllTimeCreateProduct($productId);
        }
        if($daynum == 'this_month'){
            $daynum = $this->getThisMonthCount($productId);
            $dayCount = floor((time() - strtotime($daynum)) / 86400);
            $daynum = (int)$dayCount;
        }
        $arr = [];
        $summary = [];
        $daystart = new \DateTime();
        $date = $daystart->sub(new \DateInterval('P' . $daynum . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $soldInDays = $this->getProductSoldInDay($this->getInventorySoldFromDay($daynum, $vendorId,$productId));
        $soldInDaysValue = $this->getProductSoldInDayValue($this->getInventorySoldFromDay($daynum, $vendorId,$productId));
        $refundInDays = $this->getProductRefundedInDay($this->getInventoryRefundedFromDay($daynum, $vendorId,$productId));
        $refundInDaysValue = $this->getProductRefundedInDayValue($this->getInventoryRefundedFromDay($daynum, $vendorId,$productId));
        for ($i = 0; $i < $daynum; $i++) {
            $date->add(new \DateInterval('P1D'));
            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [
                'date_text' => $dateText,
                'sales' => $sales,
                'qty_sale' => $qtySale,
                'refunded' => $refunds,
                'qty_refunded' => $qtyRefund,
                'style' => 'color: #017cb9; stroke-color: #017cb9'
            ];
            $summary = [
                'sales_value' => $this->getInventorySaleValue($daynum, $vendorId,$productId),
                'units_sold' => $this->getInventoryUnitsSoldFromDay($daynum, $vendorId,$productId),
                'units_fulfilled' => $this->getInventoryFulfilledFromDay($daynum, $vendorId,$productId),
                'returns_value' => $this->getInventoryRefundedValueFromDay($daynum, $vendorId,$productId),
                'returns_quantity' => $this->getInventoryRefundedFromDayQty($daynum, $vendorId,$productId)
            ];
        }

        return [
            'day_number' => $number,
            'start_date' => $startDateText,
            'end_date' => $dateText,
            'result' => $arr,
            'summarry' => $summary
        ];
    }
    public function getInventorySoldFromDay($daynum, $vendorId,$productId){
        $day = $this->getDay($daynum);
        $products = $this->_orderItemFactory->create()->getCollection()
            ->addFieldToFilter("udropship_vendor", $vendorId)
            ->addFieldToFilter('product_id',$productId)
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $products;
    }
    public function getInventoryRefundedFromDay($daynum, $vendorId,$productId){
        $rmaItemTable = $this->creditmemoItemCollectionFactory->create()->getTable('urma_rma_item');
        $collection = $this->_urmaFactory->create()->getCollection()->addFieldToFilter('udropship_vendor', $vendorId)
            ->addFieldToFilter('rma_status', 'approved');
        $collection->getSelect()->joinLeft(
            $rmaItemTable,
            "main_table.entity_id = $rmaItemTable.parent_id",
            ['qty', 'price']
        );
        if ($daynum) {
            $day = $this->getDay($daynum);
            $collection->addFieldToFilter('product_id',$productId)
                ->addFieldToFilter('created_at', array('gt' => $day));
        }

        return $collection;
    }
    public function getInventorySaleValue($daynum, $vendorId,$productId){
        $tickets = $this->getInventorySoldFromDay($daynum, $vendorId,$productId);
        $total = 0;
        foreach ($tickets as $ticket) {
            $total += $ticket->getBaseRowTotal();
        }
        return $total;
    }
    public function getInventoryUnitsSoldFromDay($daynum, $vendorId,$productId){
        $orderItems = $this->getInventorySoldFromDay($daynum, $vendorId,$productId);
        $result = 0;

        foreach ($orderItems as $orderItem) {
            $result += $orderItem->getQtyOrdered();
        }

        return $result;
    }
    public function getInventoryFulfilledFromDay($key, $vendorId,$productId){
        $day = $this->getDay($key);
        $result = 0;
        $shipments = $this->_saleShipmentCollectionFactory->create()
            ->addFieldToFilter('udropship_vendor', $vendorId)
            ->addFieldToFilter('product_id', $productId);
        $shipmentItemsTable = $shipments->getTable('sales_shipment_item');
        $shipments->addFieldToFilter('created_at', array('gt' => $day));
        $shipments->getSelect()->joinLeft(
            $shipmentItemsTable,
            "main_table.entity_id = $shipmentItemsTable.parent_id",
            ["$shipmentItemsTable.qty_shipped"]
        );
        foreach ($shipments as $shipment) {
            if (@$shipment->getQtyShipped() && intval($shipment->getUdropshipStatus()) === ShipmentStatus::UDPO_STATUS_DELIVERED) {
                $result += $shipment->getQtyShipped();
            }
        }

        return $result;
    }
    public function getInventoryRefundedValueFromDay($daynum, $vendorId,$productId){
        $ticketsRefunded = $this->getInventoryRefundedFromDay($daynum, $vendorId,$productId);
        $total = 0;
        foreach ($ticketsRefunded as $ticket) {
            $total += $ticket->getPrice() * $ticket->getQty();
        }
        return $total;
    }
    public function getInventoryRefundedFromDayQty($daynum, $vendorId,$productId){
        $collection = $this->getInventoryRefundedFromDay($daynum, $vendorId,$productId);
        $refunded = 0;

        foreach ($collection->getItems() as $item) {
            $refunded += $item->getQty();
        }
        return $refunded;
    }
    public function getAllTimeCreateProduct($productId){
        $now = new \DateTime('now');
        $list = array();
        $startTime = $this->productFactory->create()->load($productId)->getCreatedAt();
        $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);
        $period = new \DatePeriod(
            $startTime,
            new \DateInterval('P1D'),
            $now
        );
        $count = 0;
        foreach ($period as $item) {
            $count++;
            $list[] = $item->format("Y-m-d");
        }
        return $count;
    }
    public function getThisMonthCount($productId){
        $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));

        return $startTime->format('Y-m-d');
    }
    //End Inventory Detail

    public function getTicketRedeemedFromDay(int $daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->ticketFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('redeemed_at', array('gt' => $day));
        return $collection;
    }

    public function getRedeemedFromDay(int $daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('redeemed_at', array('gt' => $day));
        return $collection;
    }

    public function getProductGrouponCount($daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->vendorProductCollectionFactory->create()
            ->addFieldToFilter('vendor_id', $vendorId);
        $ids = $collection->getColumnValues('product_id');
        /** @var  $eventCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $eventCollection = $this->_magentoEventFactory->create()->getCollection();
        $eventCollection->addFieldToFilter('event_type_id', \Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW)
            ->addFieldToFilter('object_id', ['in' => $ids])
            ->addFieldToFilter('logged_at', array('gt' => $day));
        return count($eventCollection);
    }

    public function getFeedbackGrouponCount($daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $reviewCollection = $this->_rateHlp->getVendorReviewsCollection($vendorId);
        $reviewCollection->addFieldToFilter('created_at', array('gt' => $day));
        return count($reviewCollection);
    }

    public function getTicketRefundedValueFromDay($daynum, $vendorId)
    {
        $ticketsRefunded = $this->getTicketRefundedFromDay($daynum, $vendorId);
        $total = 0;
        foreach ($ticketsRefunded as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->sessionFactory->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }
        return $total;
    }

    public function getProductRefundedFromDayQty($daynum, $vendorId)
    {
        $collection = $this->getProductRefundedFromDay($daynum, $vendorId);
        $refunded = 0;

        foreach ($collection->getItems() as $item) {
            $refunded += $item->getQty();
        }
        return $refunded;
    }

    public function getProductRefundedValueFromDay($daynum, $vendorId)
    {
        $ticketsRefunded = $this->getProductRefundedFromDay($daynum, $vendorId);
        $total = 0;
        foreach ($ticketsRefunded as $ticket) {
            $total += $ticket->getPrice() * $ticket->getQty();
        }
        return $total;
    }

    public function getFulfilledFromDay($key, $vendorId)
    {
        $day = $this->getDay($key);
        $result = 0;
        $shipments = $this->_saleShipmentCollectionFactory->create()->addFieldToFilter('udropship_vendor', $vendorId);
        $shipmentItemsTable = $shipments->getTable('sales_shipment_item');
        $shipments->addFieldToFilter('created_at', array('gt' => $day));
        $shipments->getSelect()->joinLeft(
            $shipmentItemsTable,
            "main_table.entity_id = $shipmentItemsTable.parent_id",
            ["SUM($shipmentItemsTable.qty_shipped) AS total_qty_shipped"]
        )->group(['main_table.entity_id']);
        foreach ($shipments as $shipment) {
            if (@$shipment->getTotalQtyShipped() && intval($shipment->getUdropshipStatus()) === ShipmentStatus::UDPO_STATUS_DELIVERED) {
                $result += $shipment->getTotalQtyShipped();
            }
        }

        return $result;
    }


    public function getEventTicketSaleValue($key, $vendorId)
    {
        $tickets = $this->getTicketSoldFromDay($key, $vendorId);
        $total = 0;
        foreach ($tickets as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->sessionFactory->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }
        return $total;
    }

    public function getProductSaleValue($key, $vendorId)
    {
        $tickets = $this->getProductSoldFromDay($key, $vendorId);
        $total = 0;
        foreach ($tickets as $ticket) {
            $total += $ticket->getBaseRowTotal();
        }
        return $total;
    }

    public function getProductUnitsSoldFromDay($key, $vendorId)
    {
        $orderItems = $this->getProductSoldFromDay($key, $vendorId);
        $result = 0;

        foreach ($orderItems as $orderItem) {
            $result += $orderItem->getQtyOrdered();
        }

        return $result;
    }


    public function getGrouponSoldInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection->getColumnValues('created_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getTicketSoldInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Ticket\Model\ResourceModel\Ticket\Collection $collection */
        foreach ($collection->getColumnValues('created_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getProductSoldInDay($collection)
    {
        $arr = [];
        foreach ($collection as $item) {
            $value = $item->getCreatedAt();
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = floatval($item->getQtyOrdered());
            else
                $arr[$key] += floatval($item->getQtyOrdered());
        }
        return $arr;
    }

    public function getGrouponSoldFromDay(int $daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $collection;
    }

    public function getTicketSoldFromDay(int $daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->ticketFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $collection;
    }

    public function getProductSoldFromDay($key, $vendorId)
    {
        $day = $this->getDay($key);
        $products = $this->_orderItemFactory->create()->getCollection()
            ->addFieldToFilter("udropship_vendor", $vendorId)
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $products;
    }

    public function getDay(int $daynum)
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval('P' . $daynum . 'D'));
        return $date->format('Y-m-d');
    }

    public function getGrouponSoldInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('created_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('price') * $item->getData('qty');
            else
                $arr[$key] += $item->getData('price') * $item->getData('qty');
        }
        return $arr;
    }

    public function getTicketSoldInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Ticket\Model\ResourceModel\Ticket\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('created_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('ticket_price');
            else
                $arr[$key] += $item->getData('ticket_price');
        }
        return $arr;
    }

    public function getProductSoldInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $product) {
            $key = explode(' ', $product->getCreatedAt())[0];
            $sold = $product->getBaseRowTotal();
            if (!isset($arr[$key]))
                $arr[$key] = $sold;
            else
                $arr[$key] += $sold;
        }
        return $arr;
    }

    public function getGrouponRefundedFromDay($daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('refunded_at', array('gt' => $day));
        return $collection;
    }

    public function getTicketRefundedFromDay($daynum, $vendorId)
    {
        $day = $this->getDay($daynum);
        $collection = $this->ticketFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('refunded_at', array('gt' => $day));
        return $collection;
    }

    public function getProductRefundedFromDay($daynum = null, $vendorId)
    {
        $rmaItemTable = $this->creditmemoItemCollectionFactory->create()->getTable('urma_rma_item');
        $collection = $this->_urmaFactory->create()->getCollection()->addFieldToFilter('udropship_vendor', $vendorId)
            ->addFieldToFilter('rma_status', 'approved');
        $collection->getSelect()->joinLeft(
            $rmaItemTable,
            "main_table.entity_id = $rmaItemTable.parent_id",
            ['qty', 'price']
        );
        if ($daynum) {
            $day = $this->getDay($daynum);
            $collection->addFieldToFilter('created_at', array('gt' => $day));
        }

        return $collection;
    }

    public function getGrouponRefundedInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection->getColumnValues('refunded_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getTicketRefundedInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Ticket\Model\ResourceModel\Ticket\Collection $collection */
        foreach ($collection->getColumnValues('refunded_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getProductRefundedInDay($collection)
    {
        $arr = [];
        foreach ($collection as $item) {
            $value = $item->getCreatedAt();
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = floatval($item->getQty());
            else
                $arr[$key] += floatval($item->getQty());
        }
        return $arr;
    }


    public function getGrouponRefundedInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('refunded_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('price') * $item->getData('qty');
            else
                $arr[$key] += $item->getData('price') * $item->getData('qty');
        }
        return $arr;
    }

    public function getTicketRefundedInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Ticket\Model\ResourceModel\Ticket\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('refunded_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('ticket_price');
            else
                $arr[$key] += $item->getData('ticket_price');
        }
        return $arr;
    }

    public function getProductRefundedInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $product) {
            $key = explode(' ', $product->getCreatedAt())[0];
            $refund = $product->getPrice() * $product->getQty();
            if (!isset($arr[$key])) {
                $arr[$key] = $refund;
            } else {
                $arr[$key] += $refund;
            }
        }
        return $arr;
    }

    public function getFeedbackCollection($vendorId, $cur_page, $size)
    {
        $_reviewCollection = $this->_magenestReviewHelper->getVendorReviewCollection($vendorId);
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        if ($hasDivided) {
            $_reviewCollection->setPageSize($size)->setCurPage($cur_page);
        }
        $_reviewCollection->setOrder('created_at', 'DESC');
        return $_reviewCollection;
    }

    public function getRecentQuestion()
    {
        /**
         * @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection $questions
         */
        $questions = $this->_question->create()->getCollection();
        $allData = $questions->addFieldToFilter('question_status', '1')->addFieldToFilter('answer_text', ['null' => true])
            ->addOrder('question_date', 'DESC')->getData();
        $count = 0;
        $result = [];
        foreach ($allData as $item) {
            if ($count < 3) {
                array_push($result, $item);
            } else break;
            $count++;
        }
        return $result;
    }

    public function getFilter($status)
    {
        if ($status == 'live') {
            $status = CampaignStatus::STATUS_LIVE;
        } elseif ($status == 'active') {
            $status = CampaignStatus::STATUS_ACTIVE;
        } elseif ($status == 'scheduled') {
            $status = CampaignStatus::STATUS_SCHEDULED;
        } elseif ($status == 'pending') {
            $status = CampaignStatus::STATUS_PENDING_REVIEW;
        } elseif ($status == 'sold_out') {
            $status = CampaignStatus::STATUS_SOLD_OUT;
        } elseif ($status == 'expired') {
            $status = CampaignStatus::STATUS_EXPIRED;
        } elseif ($status == 'paused') {
            $status = CampaignStatus::STATUS_PAUSED;
        } elseif ($status == 'rejected') {
            $status = CampaignStatus::STATUS_REJECTED;
        } else $status = 'all';

        return $status;
    }

    public function getDeal($vendorId, $status, $cur_page, $size, $sortField, $sortOrder)
    {
        $status = $this->getFilter($status);

        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        /** @var  $collection \Magenest\Groupon\Model\ResourceModel\Deal\Collection */
        $collection = $this->dealFactory->create()->getCollection();
        if ($status === 'all' || $status === null) {
            $collection->addFieldToFilter('product_type', 'configurable')
                ->addFieldToFilter('vendor_id', $vendorId);
        } else {
            $collection->addFieldToFilter('product_type', 'configurable')
                ->addFieldToFilter('status', $status)
                ->addFieldToFilter('vendor_id', $vendorId);
        }
        $collection->getSelect()->joinLeft(['info' => $collection->getTable('magenest_groupon_deal_info')], 'main_table.deal_id=info.deal_id', ['sold', 'revenue', 'view_count', 'info_id']);
        if ($sortField != '' && $sortOrder != '') {
            if (($sortOrder == 'asc' || $sortOrder == 'desc') && (true == in_array($sortField, ['start_time', 'end_time', 'sold', 'revenue', 'start_valid', 'end_valid', 'view_count']))) {
                if ($sortField == 'start_valid') {
                    $sortField = 'redeemable_after';
                }
                if ($sortField == 'end_valid') {
                    $sortField = 'groupon_expire';
                }
                if ($sortField == 'view_count') {
                    $sortField = 'info.view_count';
                }
                $collection->setOrder($sortField, $sortOrder);
            }
        }
        if ($hasDivided) {
            $collection->setPageSize($size)->setCurPage($cur_page);
        }

        $this->parentIds = $collection->getColumnValues('product_id');
        return $collection;
    }

    /**
     * Get Product
     *
     * @param $productId
     * @return \Magento\Catalog\Model\Product
     */
    public function getItems($productId)
    {
        /** @var  $productCollection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $productCollection = $this->productFactory->create()->getCollection();
        $product = $productCollection
            ->addFieldToSelect('entity_id')
            ->addFieldToSelect('sku')
            ->addAttributeToSelect(['name', 'image', 'campaign_status'])
            ->addFieldToFilter('entity_id', $productId)->getFirstItem();
        $this->productLoad[$productId] = $product;
        return $product;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getProductCount($id)
    {
        /** @var  $eventCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $eventCollection = $this->_magentoEventFactory->create()->getCollection();
        $eventCollection->addFieldToFilter('event_type_id', \Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW)
            ->addFieldToFilter('object_id', $id);
        return $eventCollection->getSize();
    }

    /**
     * @param \Magenest\Groupon\Model\Deal $deal
     * @return mixed
     */
    public function getDealDetails($deal)
    {
        $dateFormat = 'd-m-Y';
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $children */
        $children = $this->dealFactory->create()->getCollection()
            ->addFieldToFilter('parent_id', $deal->getProductId());
        $data['option_count'] = $children->count();
        $data['start_time'] = $deal->getStartTime() ? \DateTime::createFromFormat("Y-m-d H:i:s", $deal->getStartTime())->format($dateFormat) : '';
        $data['end_time'] = $deal->getEndTime() ? \DateTime::createFromFormat("Y-m-d H:i:s", $deal->getEndTime())->format($dateFormat) : '';
        $data['start_valid'] = $deal->getRedeemableAfter() ? \DateTime::createFromFormat("Y-m-d H:i:s", $deal->getRedeemableAfter())->format($dateFormat) : '';
        $data['end_valid'] = $deal->getGrouponExpire() ? \DateTime::createFromFormat("Y-m-d H:i:s", $deal->getGrouponExpire())->format($dateFormat) : '';
        $data['view_count'] = $this->getProductCount($deal->getProductId());
        $data['product_id'] = $deal->getProductId();
        //Check info model
        //sold
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $grouponCollection */
        $grouponCollection = $this->grouponFactory->create()->getCollection();
        $grouponCollection->addFieldToFilter('product_id', ['in' => $children->getColumnValues('product_id')]);
        $data['sold'] = 0;
        /** @var Groupon $item */
        foreach ($grouponCollection->load()->getColumnValues('qty') as $item)
            $data['sold'] += $item;

        //revenue
        $data['revenue'] = 0;
        foreach ($grouponCollection->load()->getColumnValues('price') as $item)
            $data['revenue'] += $item;

        //redeemed
        $data['redeemed'] = 0;
        $grouponCollection->addFieldToFilter('status', Groupon::STATUS_USED);
        $grouponCollection->resetData();
        foreach ($grouponCollection->getData() as $item) {
            $data['redeemed'] += $item['qty'];
        }
        $isInfoChange = false;
        if ($deal->getData('info_id') != null && $deal->getData('info_id') != '') {
            if ($data['sold'] != $deal->getData('sold') || $data['revenue'] != $deal->getData('revenue') || $data['view_count'] != $deal->getData('view_count')) {
                $infoModel = $this->_dealInfoFactory->create()->load($deal->getData('info_id'));
                $isInfoChange = true;
            }
        } elseif ($isInfoChange || $deal->getData('info_id') == '' || $deal->getData('info_id') == null) {
            if (!$isInfoChange) {
                $infoModel = $this->_dealInfoFactory->create();
            }
            $infoData = [
                'deal_id' => $deal->getId(),
                'product_id' => $deal->getProductId(),
                'sold' => $data['sold'],
                'revenue' => $data['revenue'],
                'view_count' => $data['view_count'],
            ];
            $infoModel->addData($infoData);
            $infoModel->save();
        }
        if (!isset($this->productLoad[$deal->getProductId()])) {
            $productCollection = $this->productFactory->create()
                ->getCollection();
            $productModel = $productCollection->addAttributeToSelect(['name', 'campaign_status', 'image'])
                ->addFieldToFilter('entity_id', $deal->getProductId())
                ->getFirstItem();
            $this->productLoad[$deal->getProductId()] = $productModel;
        }
        $productModel = $this->productLoad[$deal->getProductId()];
        $data['status'] = CampaignStatus::getStatusLabelByCode($productModel->getCampaignStatus());
        return $data;
    }

    public function getProductInfoByID($product_id)
    {
        $product = $this->productFactory->create()->load($product_id);
        return [$product->getName(), $product->getProductUrl()];
    }

    public function getSellingFrom($dealProductId)
    {
        $date = @DataHelper::getDealSaleDate($dealProductId)['from'];
        if (empty($date))
            return '';
        $date = \DateTime::createFromFormat("Y-m-d H:i:s", $date);
        return "from " . $date->format("M j, Y");
    }

    public function getSellingTo($dealProductId)
    {
        $date = @DataHelper::getDealSaleDate($dealProductId)['to'];
        if (empty($date))
            return '';
        $date = \DateTime::createFromFormat("Y-m-d H:i:s", $date);
        return "until " . $date->format("M j, Y");
    }

    public function getValidFrom($dealProductId)
    {
        $date = @DataHelper::getDealDate($dealProductId)['from'];
        if (empty($date))
            return '';
        $date = \DateTime::createFromFormat("Y-m-d H:i:s", $date);
        return "from " . $date->format("M j, Y");
    }

    public function getValidTo($dealProductId)
    {
        $date = @DataHelper::getDealDate($dealProductId)['to'];
        if (empty($date))
            return '';
        $date = \DateTime::createFromFormat("Y-m-d H:i:s", $date);
        return "until " . $date->format("M j, Y");
    }

    public function getUnpublishedIn($dealProductId)
    {
        $today = $date = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));

        $sellingTo = @DataHelper::getDealSaleDate($dealProductId)['to'];
        if (empty($sellingTo))
            return 9999;
        $sellingTo = \DateTime::createFromFormat("Y-m-d H:i:s", $sellingTo);
        return $sellingTo->diff($today)->days;
    }

    public function getInvalidIn($dealProductId)
    {
        $today = $date = \DateTime::createFromFormat("Y-m-d", date("Y-m-d"));

        $validTo = @DataHelper::getDealSaleDate($dealProductId)['to'];
        if (empty($validTo))
            return 9999;
        $validTo = \DateTime::createFromFormat("Y-m-d H:i:s", $validTo);
        return $today->diff($validTo)->days;
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getAvailableCollection($dealProductId)
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $dealProductId])
            ->addFieldToFilter("status", Groupon::STATUS_AVAILABLE);
    }

    /**
     * @return int
     */
    public function getQtyAvailable($dealProductId)
    {
        return $this->getAvailableCollection($dealProductId)->count();
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getRedeemedCollection($dealProductId)
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $dealProductId])
            ->addFieldToFilter("status", Groupon::STATUS_USED);
    }

    /**
     * @return int
     */
    public function getQtyRedeemed($dealProductId)
    {
        return $this->getRedeemedCollection($dealProductId)->count();
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getRefundedCollection($dealProductId)
    {
        return $this->grouponCollectionFactory->create()
            ->addFieldToFilter("product_id", ['in' => $dealProductId])
            ->addFieldToFilter(["status", "refunded_at"], [Groupon::STATUS_CANCELED, "null" => false]);
    }

    /**
     * @return int
     */
    public function getQtyRefunded($dealProductId)
    {
        return $this->getRefundedCollection($dealProductId)->count();
    }

    public function getDays($dayNum, $dealProductId)
    {
        $now = new \DateTime('now');
        $list = array();
        if ($dayNum === "all_time") {
            $startTime = $this->productFactory->create()->load($dealProductId)->getCreatedAt();
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);

            $period = new \DatePeriod(
                $startTime,
                new \DateInterval('P1D'),
                $now
            );
            foreach ($period as $item) {
                $list[] = $item->format("Y-m-d");
            }
        } elseif ($dayNum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));
            $period = new \DatePeriod(
                $startTime,
                new \DateInterval('P1D'),
                $now
            );
            foreach ($period as $item) {
                $list[] = $item->format("Y-m-d");
            }
        } else {
            for ($i = $dayNum - 1; $i >= 0; $i--) {
                $date = new \DateTime();
                $date->sub(new \DateInterval('P' . $i . 'D'));
                $list[] = $date->format('Y-m-d');
            }
        }
        return $list;
    }

    public function getOptionData($daysList, $dealProductId)
    {
        $collection = $this->grouponCollectionFactory->create()
            ->addFieldToFilter('product_id', ['in' => $dealProductId])
            ->addFieldToFilter('created_at', array('gt' => array_keys($daysList)[0]));
        /** @var \Magenest\Groupon\Model\Groupon $item */

        $summary = [
            'sales' => 0,
            'qtySold' => 0,
            'refunds' => 0,
            'qtyRefund' => 0,
            'redeemed' => 0
        ];

        foreach ($collection as $item) {
            if (!empty($item->getRefundedAt())) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $item->getRefundedAt())->format("Y-m-d");
                $daysList[$date]['qtyRefund'] += $item->getQty();
                $daysList[$date]['refunds'] += $item->getPrice();
                $summary['qtyRefund'] += $item->getQty();
                $summary['refunds'] += $item->getPrice();
                continue;
            }
            if (!empty($item->getCreatedAt())) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $item->getCreatedAt())->format("Y-m-d");
                $daysList[$date]['qtySold'] += $item->getQty();
                $daysList[$date]['sales'] += $item->getPrice();
                $summary['qtySold'] += $item->getQty();
                $summary['sales'] += $item->getPrice();
            }

            if ($item->getStatus() == Groupon::STATUS_USED)
                $summary['redeemed'] += $item->getQty();
        }

        return ['option' => $daysList, 'summary' => $summary];
    }

    public function getOptions($dayOption, $dealProductId,$productId)
    {
        $daysList = $this->getDays($dayOption, $productId);
        foreach ($daysList as $key => $day) {
            $daysList [$day] = [
                'sales' => 0,
                'qtySold' => 0,
                'refunds' => 0,
                'qtyRefund' => 0
            ];
            unset($daysList[$key]);
        }
        return $this->getOptionData($daysList, $dealProductId);
    }

    /**
     * @return \Magenest\Groupon\Model\ResourceModel\Groupon\Collection
     */
    public function getGroupon($page, $pageSize, $search, $sortby, $sorttype, $productId)
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        $collection = $this->grouponCollectionFactory->create();

        if ($search != '') {
            $pattern = '%' . $search . '%';
            $collection->addFieldToFilter(['groupon_code', 'customer_name'], [['like' => $pattern], ['like' => $pattern]]);
        }

        if ($sortby != '' && $sorttype != '') {
            $collection->setOrder($sortby, strtoupper($sorttype));
        }

        $collection->addFieldToFilter('product_id', $productId);
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }

    public function getChangeHistory($page, $pageSize, $productId)
    {
        if(is_numeric($page)&&is_numeric($pageSize)){
            $flag = true;
        }else{
            $flag = false;
        }
        $collection = $this->_statusHistory->create()->getCollection()->addFieldToFilter('product_id', $productId)->setOrder('created_at', 'DESC');
        if ($flag) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }

    public function getStatusLabel($id)
    {
        return CampaignStatus::getStatusLabelByCode($id);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductTodaySale()
    {
        $result = $this->_productDashboardHelper->getTodaySale();
        return $result;
    }

    public function getproductUnitsSaleToday()
    {
        $result = $this->_productDashboardHelper->getUnitsSaleToday();
        return $result;
    }

    /**
     * Get number of units fulfilled for each vendor
     */
    public function getProductRedeemed(){
        $result = $this->_productDashboardHelper->getRedeemed($ids = []);
        return $result;
    }
    public function setVendorAsLoggedIn($vendorId){
        $vendor = $this->_vendorFactory->create()->load($vendorId);
        $session = \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $session->setVendorAsLoggedIn($vendor);
    }

    /**
     * @throws LocalizedException
     */
    public function getProductSoldOutOption()
    {
        $result = $this->_productDashboardHelper->getSoldOutOption();
        return $result;
    }

    /**
     * Get active product of simple product vendor
     */
    public function getActiveProductCollection()
    {
        $result = $this->_productDashboardHelper->getActiveCampaignCollection();
        return $result;
    }

    public function getActiveProductCollectionIds($vendorId)
    {
        $activeProducts = $this->getActiveProductCollection();
        $result = [];
        foreach ($activeProducts as $product) {
            array_push($result, $product->getId());
        }
        return $result;
    }

    public function getProductRemaining($ids)
    {
        $result = $this->_productDashboardHelper->getRemaining($ids);
        return $result;
    }

    public function getProductSold($ids)
    {
        $result = $this->_productDashboardHelper->getSold($ids);
        return $result;
    }

    public function getVendorLocation($vendorId)
    {
        $collection = $this->locationFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->setOrder('sort_order', 'ASC');
        $firstItem = $collection->getFirstItem();
        $i = 1;
        if ($firstItem->getData('sort_order') === null)
            foreach ($collection as $item) {
                $item->setData('sort_order', $i++);
                $item->save();
            }
        return $collection;
    }

    public function getVendorDelivery($vendorId)
    {
        $collection = $this->deliveryFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->setOrder('sort_order', 'ASC');
        $firstItem = $collection->getFirstItem();
        $i = 1;
        if ($firstItem->getData('sort_order') === null)
            foreach ($collection as $item) {
                $item->setData('sort_order', $i++);
                $item->save();
            }
        return $collection;
    }

    public function getCampaignImages($dealCollection)
    {
        $imagehelper = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Catalog\Helper\Image');
        $images = [];
        foreach ($dealCollection as $deal) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productRepository->getById($deal->getProductId());
            $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $imageItems = $product->getMediaGallery()['images'];
            foreach ($imageItems as $item) {
                $fileName = $item['file'];
                $url = $baseMediaUrl . 'catalog/product' . $fileName;
                $images[] = $url;
            }
        }
        return $images;
    }
    public function getUploadedImages($vendorId){
        $images = $this->imagesLoader->create()->getCollection()
            ->AddFieldToFilter('vendor_id', $vendorId);
        $imageURLs = [];
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        foreach($images as $image) {
            $imageURLs[] = $baseMediaUrl . 'vendor/' . $image->getImagePath();
        }
        return $imageURLs;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getVendorUploadedDocuments($vendorId)
    {
        $baseMediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $documentsUrl = $baseMediaUrl . 'upload/vendor_' . $vendorId . '/documents/';
        $directory = $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath('upload/vendor_' . $vendorId . '/documents/');
        $document = [];
        try {
            $files = array_diff(scandir($directory), array('..', '.'));
            foreach ($files as $file) {
                $fileName = pathinfo($file, PATHINFO_FILENAME);
                $document[$fileName] = $documentsUrl . $file;
            }
        } catch (\Exception $e) {

        }
        return $document;
    }


    public function getVendorProductByVendor($vendorId)
    {
        $eventCollection = $this->_magenestEventTicketHelper->getEventProductCollection($vendorId, 'all', '', '', '', '');
        $productData = [];
        foreach ($eventCollection as $event) {
            $productId = $event['entity_id'];
            $sessionData = $this->_magenestEventTicketHelper->getSessionDataByEventId($productId);

            $product = $this->productFactory->create()->load($productId);
            $vendorProduct = $this->vendorProductCollectionFactory->create()
                ->addFieldToFilter('vendor_id', $vendorId)
                ->addFieldToFilter('product_id', $productId)
                ->getFirstItem();

            $data['offer_id'] = $vendorProduct->getVendorProductId();
            $data['EAN'] = $this->getProductEAN($productId);
            $productImage = $this->_magenestEventTicketHelper->getProductImage($productId);
            $data['image'] = $productImage;
            $data['product_name'] = $event['event_name'];
            $data['price'] = $event['revenue'] ? $event['revenue'] : 0;
            $data['sku'] = $product->getData('sku');
            $data['qty_sold'] = $event['sold'];
            $data['total_qty'] = $sessionData['total_ticket'];
            $data['campaign_status'] = $product->getData('campaign_status');
            $productData[] = $data;
        }

        return $productData;
    }

    public function getVendorOfferDetails($offerId)
    {
        $offerData = [];
        $vendorProduct = $this->vendorProductCollectionFactory->create()
            ->addFieldToFilter('vendor_product_id', $offerId)->getFirstItem();
        $productId = $vendorProduct->getProductId();
        if ($productId) {
            $product = $this->productFactory->create()->load($productId);
            $offerData = [
                'offer_id' => $vendorProduct->getVendorProductId(),
                'vendor_id' => $vendorProduct->getVendorId(),
                'product_id' => $product->getData('entity_id'),
                'EAN' => $this->getProductEAN($productId),
                'product_name' => $product->getData('name'),
                'price' => $vendorProduct->getVendorPrice(),
                'special_price' => $this->getVendorSpecialPrice($vendorProduct->getData()),
                'sku' => $product->getData('sku'),
                'total_qty' => $vendorProduct->getStockQty(),
                'campaign_status' => $product->getData('campaign_status'),
                'offer_note' => $vendorProduct->getOfferNote(),
            ];
        }
        return $offerData;

    }

    /**
     * @return string
     */
    public function getProductEAN($productId)
    {
        return null;
    }

    public function getOptionsOffer($dayOption, $productId, $vendorId)
    {
        $daysList = $this->getDays($dayOption, $productId);
        foreach ($daysList as $key => $day) {
            $daysList [$day] = [
                'date_text' => $day,
                'sales' => 0,
                'qtySold' => 0,
                'refunds' => 0,
                'qtyRefund' => 0,
                'qtyShipped' => 0
            ];
            unset($daysList[$key]);
        }
        return $this->getOptionsDataOffer($daysList, $productId, $vendorId);
    }

    public function getOptionsDataOffer($daysList, $productId, $vendorId)
    {

        if(sizeof($daysList)>0) {
            $items = $this->_orderItemFactory->create()->getCollection()->addFieldToFilter('udropship_vendor', $vendorId)
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('created_at', array('gt' => array_keys($daysList)[0]));
            $summary = [
                'sales' => 0,
                'qtySold' => 0,
                'refunds' => 0,
                'qtyRefund' => 0,
                'qtyShipped' => 0
            ];

            foreach ($items as $item) {
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $item->getCreatedAt())->format("Y-m-d");
                $daysList[$date]['qtySold'] += $item->getQtyOrdered();
                $daysList[$date]['qtyShipped'] += $item->getQtyShipped();
                $daysList[$date]['qtyRefund'] += $item->getQtyRefunded();
                $daysList[$date]['sales'] += $item->getPrice();
                if ($item->getQtyRefunded() > 0) {
                    $daysList[$date]['refunds'] += $item->getPrice();
                }
                $summary['qtySold'] += $item->getQtyOrdered();
                $summary['qtyShipped'] += $item->getQtyShipped();
                $summary['qtyRefund'] += $item->getQtyRefunded();
                $summary['sales'] += $item->getPrice();
                if ($item->getQtyRefunded() > 0) {
                    $summary['refunds'] += $item->getPrice();
                }
            }
            $daysList=array_values($daysList);

            return ['option' => $daysList, 'summary' => $summary];
        } else
            return ['option' => $daysList, 'summary' => []];
    }

    protected function getVendorSpecialPrice($vendor)
    {
        try {
            if ($vendor['special_from_date'] != null && $vendor['special_to_date'] != null) {
                $today = date("Ymd");
                $from = str_replace('-', '', $vendor['special_from_date']);
                $to = str_replace('-', '', $vendor['special_to_date']);
                if (strcmp($today, $from) >= 0 && strcmp($today, $to) <= 0) {
                    return $vendor['special_price'];
                }
            } else {
                return $vendor['special_price'];
            }
        } catch (\Exception $e) {
            return 0;
        }
        return 0;
    }

    public function getVendorPromoteProduct($vendorId)
    {
        $vendor = $this->_vendorFactory->create()->load($vendorId);

        return $this->getPromoteProductLabel($vendor->getPromoteProduct());
    }

    public static function getPromoteProductLabel($code) {
        switch (intval($code)) {
            case self::VENDOR_GROUPON:
                {
                    return 'groupon';
                    break;
                }
            case self::VENDOR_HOTEL:
                {
                    return 'hotel';
                    break;
                }
            case self::VENDOR_EVENT:
                {
                    return 'event';
                    break;
                }
            case self::VENDOR_PRODUCT:
                {
                    return 'simple';
                    break;
                }
            default :
                {
                    return 'undefined';
                    break;
                }
        }
    }

    public function getCampaignBuyOption($productId){
        $childIds = \Magenest\Groupon\Helper\Data::getChildIds($productId);
        $attributeId = $this->_eavAttribute->getIdByCode('catalog_product', 'name');
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $groupon */
        $groupon = $this->grouponCollectionFactory->create();
        $groupon->getSelect()
            ->columns(['revenue' => 'sum(main_table.price)'])
            ->columns(['sold' => 'sum(main_table.qty)'])
            ->columns(['redeemed' => 'count(main_table.redeemed_at)'])
            ->columns(['refunded' => 'count(main_table.refunded_at)']);
        $groupon->getSelect()->joinRight(['product' => $groupon->getTable('udropship_vendor_product')], "main_table.product_id = product.product_id", ['product.product_id', 'qty' => 'product.stock_qty']);
        $groupon->getSelect()->joinRight(['name' => $groupon->getTable('catalog_product_entity_varchar')], "product.product_id = name.row_id and name.attribute_id ={$attributeId}", ['name' => 'name.value']);
        $groupon->getSelect()->group(['product.product_id', 'name' => 'name.value']);
        $groupon->addFieldToFilter('product.product_id', ['in' => $childIds]);
        $data = $groupon->getData();
        return $data;
    }

    public function getPurchasedList($vendorId,$product_id,$option_id){
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        $collection = $this->grouponCollectionFactory->create()
            ->addFieldToFilter('product_id', $option_id);
        return $collection;
    }
}
