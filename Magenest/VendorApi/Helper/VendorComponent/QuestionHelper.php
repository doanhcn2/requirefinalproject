<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper\VendorComponent;

use Magenest\VendorApi\Api\Data\VendorComponent\QuestionParamsInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Collection;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\Dropship\Model\VendorFactory;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;

class QuestionHelper
{
    /**
     * @var QuestionFactory
     */
    protected $_questionFactory;

    /**
     * @var $_vendorFactory VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_shipmentCollection Collection
     */
    protected $_shipmentCollection;

    /**
     * @var DataObject
     */
    protected $_objectFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezoneInterface;
    protected $_debugHelper;
    /**
     * QuestionHelper constructor.
     * @param DataObject $dataObject
     * @param QuestionFactory $questionFactory
     * @param VendorFactory $vendorFactory
     * @param ProductFactory $productFactory
     * @param Collection $collection
     */
    public function __construct(
        DataObject $dataObject,
        QuestionFactory $questionFactory,
        VendorFactory $vendorFactory,
        ProductFactory $productFactory,
        Collection $collection,
        \Magenest\VendorApi\Helper\DebugHelper $debugHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
    ){
        $this->_productFactory = $productFactory;
        $this->_shipmentCollection = $collection;
        $this->_vendorFactory = $vendorFactory;
        $this->_questionFactory = $questionFactory;
        $this->_objectFactory = $dataObject;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_debugHelper = $debugHelper;
    }


    public function getAllApprovedQuestion($vendor_id, $cur_page, $size)
    {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        /**
         * @var $questionCollection \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection
         */
        $questionCollection = $this->_questionFactory->create()->getCollection();
        $questions = $questionCollection->addFieldToFilter('vendor_id', $vendor_id)->addFieldToFilter('question_status', 1);
        if ($hasDivided) {
            $questions->setCurPage($cur_page)->setPageSize($size);
        }

        $question = [];
        foreach ($questions as $item) {
            array_push($question, $item->getData());
        }

        return $question;
    }


    public function getAllApprovedQuestionByProduct($product_id, $cur_page, $size)
    {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        /**
         * @var $questionCollection \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection
         */
        $questionCollection = $this->_questionFactory->create()->getCollection();
        $questions = $questionCollection->addFieldToFilter('question_status', 1)->addFieldToFilter("main_table.product_id", $product_id);
        if ($hasDivided) {
            $questions->setCurPage($cur_page)->setPageSize($size);
        }

        $question = [];
        foreach ($questions as $item) {
            array_push($question, $item->getData());
        }

        return $question;
    }

    public function getAllApprovedQuestionByCustomer($customer_id, $cur_page, $size)
    {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        /**
         * @var $questionCollection \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection
         */
        $questionCollection = $this->_questionFactory->create()->getCollection();
        $questions = $questionCollection->addFieldToFilter('customer_id', $customer_id)->addFieldToFilter('question_status', 1);
        if ($hasDivided) {
            $questions->setCurPage($cur_page)->setPageSize($size);
        }

        $questionCollection->setOrder('question_date', 'DESC');
        $question = [];
        foreach ($questions as $item) {
            $data = $item->getData();
            $vendorId = $item->getVendorId();
            $data['vendor_name'] = $this->_vendorFactory->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId)->getFirstItem()->getVendorName();
            $shipmentId = $item->getShipmentId();
            if ($shipmentId !== null) {
                $shipments = $this->_shipmentCollection->load($shipmentId)->getData();
                foreach ($shipments as $ship) {
                    if ($ship['entity_id'] === $shipmentId) {
                        $data['order_id'] = $ship['order_id'];
                    }
                }
            } else {
                $data['order_id'] = null;
            }
            if ($item->getProductId()) {
                $name = $this->_productFactory->create()->load($item->getProductId())->getName();
                $data['product_name'] = $name;
            } else {
                $data['product_name'] = null;
            }

            array_push($question, $data);
        }

        return $question;
    }


    public function askVendorQuestions($customerId, $requestParams)
    {
        if ($requestParams['vendor_id'] === null || empty($requestParams['question'])) {
            return false;
        }
        /**
         * $data = ['question' => string, 'customer_id' $customer_id]
         */
        $questionData = [
            'vendor_id' => $requestParams['vendor_id'],
            'question_text' => $requestParams['question']
        ];
        if(isset($requestParams['magento_order_id'])){
            $questionData['magento_order_id'] = $requestParams['magento_order_id'];
        }
        $cSess = ObjectManager::getInstance()->create('Magento\Customer\Model\Customer');
        $customer = $cSess->load($customerId);

        $qModel = $this->_questionFactory->create()
            ->setData($questionData)
            ->setQuestionDate(date('Y-m-d H:i:s'));
        $qModel
            ->setCustomerEmail($customer->getEmail())
            ->setCustomerName($customer->getFirstname() . ' ' . $customer->getLastname())
            ->setCustomerId($customer->getId());
        $validate = $qModel->validate();
        if ($validate === true) {
            try {
                $qModel->save();
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public function filterCustomerQuestion($customerId,$filterby,$datafilter,$cur_page,$size,$sort){
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        /**
         * @var $questionCollection \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection
         */
        $questionCollection = $this->_questionFactory->create()->getCollection();
        $questionCollection->addCustomerFilter($customerId);
        if($hasDivided){
            $questionCollection->setCurPage($cur_page)->setPageSize($size);
        }
        if(!empty($sort)&&is_array($sort)&&($sort['sortby'] != 'vendor_name')){
            $sortby = $sort['sortby'];
            $sorttype = $sort['sorttype'];
            $questionCollection->setOrder("main_table.$sortby",$sorttype);
        }elseif (!empty($sort)&&($sort['sortby'] != 'vendor_name')){
            $questionCollection->setOrder("main_table.$sort",'DESC');
        }

        if(!empty($filterby)&&!empty($datafilter)){
            $questionCollection->joinShipments();
            $questionCollection->joinProducts();
            $questionCollection->joinVendors();
            $arrProduct = ['product_id', 'product_sku','product_name'];
            $arrShipments = ['order_increment_id','order_id','shipment_id'];
            $arrVendor = ['vendor_name', 'vendor_email', 'vendor_id'];
            $arrQuestion = ['question_id','question_status','answer_status'];
            $arrQuestionDate = ['question_date','answer_date'];
            if(in_array($filterby,$arrProduct)){
                if(is_array($datafilter)){
                    $from = isset($datafilter['from'])?$datafilter['from']:'';
                    $to = isset($datafilter['to'])?$datafilter['to']:'';
                    if($from != ''){
                        $questionCollection->addFieldToFilter("product.$filterby", ['gteq'=>$from]);
                    }
                    if($to != ''){
                        $questionCollection->addFieldToFilter("product.$filterby", ['lteq'=>$to]);
                    }
                }else{
                    $filter = ['like'=>'%'.$datafilter.'%'];
                    $questionCollection->addFieldToFilter("product.$filterby", $filter);
                }
            }elseif(in_array($filterby,$arrShipments)){
                if(is_array($datafilter)){
                    $from = isset($datafilter['from'])?$datafilter['from']:'';
                    $to = isset($datafilter['to'])?$datafilter['to']:'';
                    if($from != ''){
                        $questionCollection->addFieldToFilter("shipment_grid.$filterby", ['gteq'=>$from]);
                    }
                    if($to != ''){
                        $questionCollection->addFieldToFilter("shipment_grid.$filterby", ['lteq'=>$to]);
                    }
                }else{
                    $filter = ['like'=>'%'.$datafilter.'%'];
                    $questionCollection->addFieldToFilter("shipment_grid.$filterby", $filter);
                }
            }elseif(in_array($filterby,$arrVendor)){
                if(is_array($datafilter)){
                    $from = isset($datafilter['from'])?$datafilter['from']:'';
                    $to = isset($datafilter['to'])?$datafilter['to']:'';
                    if($from != ''){
                        $questionCollection->addFieldToFilter("vendor.$filterby", ['gteq'=>$from]);
                    }
                    if($to != ''){
                        $questionCollection->addFieldToFilter("vendor.$filterby", ['lteq'=>$to]);
                    }
                }else{
                    $filter = ['like'=>'%'.$datafilter.'%'];
                    $questionCollection->addFieldToFilter("vendor.$filterby", $filter);
                }
            }elseif(in_array($filterby,$arrQuestion)){
                if(is_array($datafilter)){
                    $from = isset($datafilter['from'])?$datafilter['from']:'';
                    $to = isset($datafilter['to'])?$datafilter['to']:'';

                    if($from != '' && $to != ''){
                        $questionCollection->addFieldToFilter("main_table.$filterby", array('from'=>$from, 'to'=>$to));
                    }
                }else{
                    $filter = ['like'=>'%'.$datafilter.'%'];
                    $questionCollection->addFieldToFilter("main_table.$filterby", $filter);
                }
            }elseif(in_array($filterby,$arrQuestionDate)){
                if(is_array($datafilter)){
                    $from = isset($datafilter['from'])?$datafilter['from']:'';
                    $to = isset($datafilter['to'])?$datafilter['to']:'';
                    if($from != ''){
                        $from = $this->_timezoneInterface->date(new \DateTime($from))->format('Y-m-d H:i:s');
                        $questionCollection->addFieldToFilter("main_table.$filterby", ['gteq'=>$from]);
                    }
                    if($to != ''){
                        $to = $this->_timezoneInterface->date(new \DateTime($to))->format('Y-m-d H:i:s');
                        $questionCollection->addFieldToFilter("main_table.$filterby", ['lteq'=>$to]);
                    }
                }
            }else{
                return false;
            }
        }


        $question = [];
        foreach ($questionCollection as $item) {
            $data = $item->getData();
            $vendorId = $item->getVendorId();
            $data['vendor_name'] = $this->_vendorFactory->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId)->getFirstItem()->getVendorName();
            $shipmentId = $item->getShipmentId();
            if ($shipmentId !== null) {
                $shipments = $this->_shipmentCollection->load($shipmentId)->getData();
                foreach ($shipments as $ship) {
                    if ($ship['entity_id'] === $shipmentId) {
                        $data['order_id'] = $ship['order_id'];
                    }
                }
            } else {
                $data['order_id'] = null;
            }
            if ($item->getProductId()) {
                $name = $this->_productFactory->create()->load($item->getProductId())->getName();
                $data['product_name'] = $name;
            } else {
                $data['product_name'] = null;
            }
//            $this->_debugHelper->debugString('dataDebugQuestionFilter',json_encode($data));
            array_push($question, $data);
        }

        if(!empty($sort)&&is_array($sort)&&($sort['sortby'] == 'vendor_name')){
            if($sort['sorttype'] == 'DESC'){
                $sorttype = SORT_DESC;
            }else{
                $sorttype = SORT_ASC;
            }
            $results = $this->sortData($question,$sort['sortby'],$sorttype);
            $this->_debugHelper->debugString('resultDebugQuestionFilter',json_encode($results));
            return $results;
        }
        return $question;
    }
    public function sortData($results, $sortby, $sort){
        $data = array();
        foreach ($results as $key => $row){
            $data[$key] = $row[$sortby];
        }
        array_multisort($data,$sort,$results);
        return $results;
    }

    public function getNumStatusQuestion(){
        $allData = $this->_questionFactory->create()->getCollection()->getData();
        $numAnswer = $numUnanswer = 0;
        $numPrivate = $numPublic = 0;
        foreach ($allData as $data) {
            if ($data['question_status'] == 1 && $data['is_answered'] == 0){
                $numUnanswer++;
            }
            if ($data['question_status'] == 1 && $data['is_answered'] == 1){
                $numAnswer++;
            }
            if ($data['question_status'] == 1 && $data['visibility'] == 0){
                $numPrivate++;
            }
            if ($data['question_status'] == 1 && $data['visibility'] == 1){
                $numPublic++;
            }
        }
        return ['answer_qty' => $numAnswer, 'unanswer_qty' => $numUnanswer, 'private_qty' => $numPrivate, 'public_qty' => $numPublic];
    }
    public function getQuestions($vendorId,$status,$cur_page,$size) {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        if ($status !== null && $status !== 'all') {
//            $type = $this->getRequest()->getParam('type');
//            if ($status !== false) {
//                if (!isset($type)) $type = $status;
//            }
            /** @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection $resultCollections */
            $resultCollections = $this->_questionFactory->create()->getCollection();
            if ($status === 'answered') {
                $resultCollections->addFieldToFilter('question_status', '1')
                    ->addFieldToFilter('answer_status', '1')
                    ->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('is_answered', '1');
            } elseif ($status == 'unanswered') {
                $resultCollections->addFieldToFilter('question_status', '1')
                    ->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('is_answered', '0');
            } elseif ($status == 'private') {
                $resultCollections->addFieldToFilter('question_status', '1')
                    ->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('visibility', '0');
            } elseif ($status == 'public') {
                $resultCollections->addFieldToFilter('question_status', '1')
                    ->addOrder('question_date', 'DESC')
                    ->addFieldToFilter('visibility', '1');
            }
            if($hasDivided){
                $resultCollections->setPageSize($size)->setCurPage($cur_page);
            }
            return $resultCollections;
        } else {
            $resultCollections = $this->_questionFactory->create()->getCollection();
            $resultCollections ->addFieldToFilter('question_status', '1')
                ->addOrder('question_date', 'DESC');
            if($hasDivided){
                $resultCollections->setPageSize($size)->setCurPage($cur_page);
            }
            return $resultCollections;
        }
    }

    public function filterQuestByCusId($customerId, $cur_page, $size, $sort_by, $sort_type, $filter_by, $data_filter)
    {
        /**
         * @var $collection \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection
         */
        $collection = $this->_questionFactory->create()->getCollection();
        $collection->addFieldToFilter('main_table.customer_id', $customerId);
        $collection->getSelect()->joinLeft(
            ['cpe' => $collection->getTable('catalog_product_entity')],
            "main_table.product_id = cpe.entity_id",
            ["product_sku" => "sku", "cpe.row_id"]
        );
        $productNameAttributeId = $this->getAttributeIdByCode('name');
        $collection->getSelect()->joinLeft(
            ["cpev" => $collection->getTable('catalog_product_entity_varchar')],
            "cpe.row_id = cpev.row_id AND cpev.attribute_id = $productNameAttributeId",
            ["product_name" => "cpev.value"]
        );
        $collection->getSelect()->joinLeft(
            ['uv' => $collection->getTable('udropship_vendor')],
            "main_table.vendor_id = uv.vendor_id",
            ["uv.vendor_name", "vendor_email" => "uv.email"]
        );
        $collection->getSelect()->joinLeft(
            ['ss' => $collection->getTable('sales_shipment')],
            "main_table.shipment_id = ss.entity_id",
            ["ss.order_id"]
        );
        $collection->getSelect()->joinLeft(
            ['so' => $collection->getTable('sales_order')],
            "ss.order_id = so.entity_id",
            ["order_increment_id" => "so.increment_id"]
        );
        $collection->setPageSize($size)->setCurPage($cur_page);
        if ($sort_by != "") {
            $sort_type = ($sort_type != "")?$sort_type:'DESC';
            if ($this->validateSortParams($sort_by,$sort_type)) {
                $collection->setOrder($sort_by, $sort_type);
            }
        }
        if ($filter_by != "" && $data_filter != "") {
            if ($this->validateFilterParams($filter_by,$data_filter)) {
                $collection->addFieldToFilter($filter_by, $data_filter);
            }
        }

        return $collection->getData();
    }

    private function validateSortParams($sort_by,$sort_type)
    {
        $allowedFields = ['vendor_name','question_date','answer_date','question_id'];
        if (in_array($sort_by, $allowedFields) && in_array(strtoupper($sort_type), ['ASC', 'DESC'])) {
            return true;
        }
        return false;
    }

    private function validateFilterParams($filter_by,$data_filter)
    {
        $allowedFields = ['question_status','answer_status', 'question_date','answer_date','product_id', 'product_sku', 'product_name', 'order_increment_id', 'order_id','shipment_id', 'vendor_name', 'vendor_email', 'vendor_id', 'question_id','question_status','answer_status'];
        if ($filter_by && $data_filter) {
            if (in_array($filter_by, $allowedFields)) {
                $filterText = trim($data_filter);
                if (isset($filterText)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getAttributeIdByCode($code)
    {
        $eventHelper = ObjectManager::getInstance()->create('Magenest\VendorApi\Helper\VendorComponent\EventTicketHelper');

        return $eventHelper->getAttributeIdByEntityCode($code);
    }
}
