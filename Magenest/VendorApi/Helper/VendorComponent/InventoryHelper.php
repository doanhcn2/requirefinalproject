<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Helper\VendorComponent;
use Magento\Catalog\Model\Product;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus as Status;
use Magento\Catalog\Model\Product\Media\ConfigInterface;
class InventoryHelper{
    protected $_attributeRepository;
    protected $_productFactory;
    protected $_coreProductFactory;
    protected $_imageBuilder;
    protected $storeManager;
    /**
     * @var ConfigInterface
     */
    private $mediaConfig;
    public function __construct(
        ConfigInterface $mediaConfig,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductFactory $coreProductFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository
    ) {
        $this->_attributeRepository = $attributeRepository;
        $this->_productFactory = $productFactory;
        $this->_coreProductFactory = $coreProductFactory;
        $this->_imageBuilder = $imageBuilder;
        $this->mediaConfig = $mediaConfig;
        $this->storeManager = $storeManager;
    }

    public function getVendorInventory($vendorId,$status,$sortby,$sorttype,$keyWord,$cur_page,$size){
        $results = [];
        /** @var  $product \Magento\Catalog\Model\Product*/
        $campaignStatusAtt = $this->_attributeRepository->get(Product::ENTITY,'campaign_status');
        if($campaignStatusAtt != null){
            $campaignStatusAttId = $campaignStatusAtt->getAttributeId();
        }else{
            $campaignStatusAttId = 0;
        }
        $productNameAtt = $this->_attributeRepository->get(Product::ENTITY,'name');
        if($productNameAtt != null){
            $productNameAttId = $productNameAtt->getAttributeId();
        }else{
            $productNameAttId = 0;
        }
        $subQuery = new \Zend_Db_Expr('(SELECT  sum(qty_ordered) as sold, product_id from sales_order_item GROUP BY product_id)');
        $collection = $this->_productFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            [
                'view_table' => $collection->getTable('report_event')
            ],'main_table.product_id = view_table.object_id AND view_table.event_type_id = '.\Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW
            ,[]
        )->joinLeft(
            [
                'product_table' => $collection->getTable('catalog_product_entity')
            ],'main_table.product_id = product_table.entity_id'
            ,[]
        )->joinLeft(
            [
                'campain_status_table' => $collection->getTable('catalog_product_entity_varchar')
            ],"product_table.row_id= campain_status_table.row_id AND campain_status_table.attribute_id='".$campaignStatusAttId."'"
            ,[]
        )->joinLeft(
            [
                'product_name_table' => $collection->getTable('catalog_product_entity_varchar')
            ],"product_table.row_id= product_name_table.row_id AND product_name_table.attribute_id='".$productNameAttId."'"
            ,[]
        )->joinLeft(
            [
                'sold_table' => $subQuery
            ],'sold_table.product_id = main_table.product_id'
            ,[]
        );
        $collection->addExpressionFieldToSelect('revenue','({{vendor_price}} * {{sold}})',array('vendor_price'=>'main_table.vendor_price','sold' => 'sold_table.sold'));
        $collection->getSelect()->columns(['count_view' => 'count(view_table.event_id)']);
        $collection->getSelect()->columns(['campaign_status' => 'campain_status_table.value']);
        $collection->getSelect()->columns( ['product_updated_at' => 'product_table.updated_at']);
        $collection->getSelect()->columns(['sold' => 'sold_table.sold']);
        $collection->getSelect()->columns(['product_name' => 'product_name_table.value']);
        $collection->addFieldToFilter('main_table.vendor_id',$vendorId);
        $collection->getSelect()
            ->group([
                'main_table.vendor_product_id',
                'campain_status_table.value',
                'product_name_table.value',
                'product_table.updated_at',
                'sold_table.sold',
            ]);
        $this->applyStatusFilter($collection,$status);
        $this->applySortOrder($collection,$sortby,$sorttype);
        $this->applyKeyWord($collection,$keyWord);
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        if($hasDivided){
            $collection->setPageSize($size);
            $collection->setCurPage($cur_page);
        }

        $productIds = $collection->getColumnValues('product_id');
        $products = $this->_coreProductFactory->create()->getCollection()
            ->addAttributeToSelect(['name','small_image'])
            ->addFieldToFilter('entity_id', ['in' => $productIds])->getItems();
        foreach ($collection->getItems() as $item){
            if(isset($products[$item->getProductId()])) {
                $product = $products[$item->getProductId()];
                $_product = $this->_coreProductFactory->create()->load($item->getProductId());
                /** @var  $product  \Magento\Catalog\Model\Product */
                $data     = [
                    'product_id'   => $item->getProductId(),
                    'name'         => $product->getName(),
                    'sku'          => $product->getSku(),
                    'image'        => $this->getImageUrl($_product),
                    'status'       => $item->getCampaignStatus() != null ? $item->getCampaignStatus() : 0,
                    'status_label' => $this->getCampaignStatus($item->getCampaignStatus() != null ? $item->getCampaignStatus() : 0),
                    'stock_status' => $product->isInStock() ? 'In Stock' : 'Sold Out',
                    'sold'         => $item->getSold() != null ? intval($item->getSold()) : 0,
                    'qty'          => $this->getStockQty($item->getStockQty(), $item->getSold()),
                    'price'        => ($item->getVendorPrice())*1,
                    'special_price'      => ($item->getSpecialPrice())*1,
                    'special_price_from' => $item->getSpecialFromDate(),
                    'special_price_to'   => $item->getSpecialToDate(),
                    'offer_note' => $item->getOfferNote(),
                    'handling_time' => $this->getOptionHandlingTime($item->getHandlingTime())
                ];
                $results[] = $data;
            }
        }
        $firstIndex = ($collection->getCurPage() - 1) * 20 + 1;
        $lastIndex = $collection->getCurPage() * 20;
        if ($lastIndex > $collection->getSize()) {
            $lastIndex = $collection->getSize();
        }
        $returnData = [
            'items' => $results,
            'total' => $collection->getSize(),
            'lastPage' => $collection->getLastPageNumber(),
            'firstIndex' => $firstIndex,
            'lastIndex' => $lastIndex
        ];
        return $results;
    }

    public function getOptionHandlingTime($id){
        $result = null;
        switch ($id){
            case 1:
                $result = '1 day';
                break;
            case 2:
                $result = '2 day';
                break;
            case 3:
                $result = '3 day';
                break;
            case 4:
                $result = '4 day';
                break;
            case 5:
                $result = '5 day';
                break;
            case 6:
                $result = '6 day';
                break;
            case 7:
                $result = '7 day';
                break;
            case 10:
                $result = '10 day';
                break;
        }
        return $result;
    }
    public static function getDateString($time){
        return date('d/m/Y \a\t h:i A',strtotime($time));
    }

    public static function getCampaignStatus($value){
        $statusArray = [
            Status::STATUS_REJECTED => __('Rejected'),
            Status::STATUS_PENDING_REVIEW => __('Pending Review'),
            Status::STATUS_APPROVED => __('Approved'),
            Status::STATUS_SOLD_OUT => __('Sold Out'),
            Status::STATUS_PAUSED => __('Paused'),
            Status::STATUS_DRAFT => __('Draft'),
            Status::STATUS_SCHEDULED => __('Scheduled'),
            Status::STATUS_LIVE => __('Live'),
            Status::STATUS_ACTIVE => __('Active'),
            Status::STATUS_EXPIRED => __('Expired')
        ];
        if(isset($statusArray[$value])) {
            return $statusArray[$value];
        }
        return __('Pending Review');
    }

    public function getImageUrl ($product){
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA );
        $image =  $product->getImage();
        if($image == null){
            $placeholderImageUrl = $this->storeManager->getStore()->getConfig('catalog/placeholder/small_image_placeholder');
            $image = $placeholderImageUrl;
            if($image != null){
                $imageUrl = $mediaUrl.'catalog/product'.$image;
            }else{
                $imageUrl = '';
            }
        }else{
            $imageUrl = $mediaUrl.'catalog/product'.$image;
        }

        return $imageUrl;
    }

    public function applyStatusFilter($collection,$status){
        try {
            if ((int)$status>=-1&& $status<=8) $filterStatus = $status;
            else $filterStatus = -2;
        }catch (\Exception $e){
            $filterStatus = -2;
        }
        if($filterStatus != -2){
            if($filterStatus!=2) {
                $collection->addFieldToFilter('campain_status_table.value', $filterStatus);
            }else{
                $collection->addFieldToFilter(['campain_status_table.value','campain_status_table.value'], [$filterStatus,['null' => true]]);
            }
        }
        return;
    }

    public function applySortOrder($collection,$sortby,$sorttype){
        if($sorttype == 'desc'|| $sorttype =='asc' || $sorttype =='Asc' || $sorttype =='Desc'){
            $sortOrder = strtoupper($sorttype);
        }elseif($sorttype =='DESC' ||  $sorttype =='ASC'){
            $sortOrder = $sorttype;
        }else{
            $sortOrder = 'DESC';
        }
        if($sortby =='last_updated' || $sortby =='quantity' || $sortby =='view' || $sortby =='revenue' || $sortby =='price' || $sortby =='sold'){
            $fieldMap = $sortby;
        }else{
            $fieldMap = 'last_updated';
        }

        $sortField = self::sortFieldMap($fieldMap);
        $collection->setOrder( $sortField, $sortOrder);
        $collection->addOrder('main_table.vendor_product_id', 'DESC');
        return;
    }

    public static function sortFieldMap($key){
        $data = [
            'last_updated' => 'product_updated_at',
            'quantity'     => 'main_table.stock_qty',
            'view'         => 'count_view',
            'revenue'      => 'revenue',
            'price'        => 'vendor_price',
            'sold'         => 'sold',
        ];
        return $data[$key];
    }

    public function applyKeyWord($collection,$keyWord){
        if($keyWord!=''){
            $keyWord = str_replace('%',"\%",$keyWord);
            $keyWord = str_replace('*', "\*", $keyWord);
            $paramKeyWord = '%'.$keyWord.'%';
            $collection->addFieldToFilter(['main_table.vendor_sku','product_table.sku', 'product_name_table.value'],
                [['like' => $paramKeyWord],['like' => $paramKeyWord], ['like' => $paramKeyWord]]);
        }
        return;
    }

    private function getStockQty($getStockQty, $getSold)
    {
        if ($getSold) {
            if ($getStockQty) {
                return $getStockQty > $getSold ? intval($getStockQty) : intval($getSold);
            } else {
                return intval($getSold);
            }
        }
        if ($getStockQty) {
            return intval($getStockQty);
        } else {
            return 0;
        }
    }
}