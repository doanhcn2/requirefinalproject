<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Helper\VendorComponent;

use Magento\Framework\UrlInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Framework\Exception\NoSuchEntityException;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use \Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\Ticket;

class EventTicketHelper{
    /**
     * @var $_attributeInterface \Magento\Eav\Api\AttributeRepositoryInterface
     */
    protected $_attributeInterface;
    /**
     * @var $_ticketsFactory \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketsFactory;
    /**
     * @var $_sessionFactory \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;
    /**
     * @var $_orderedTicketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_orderedTicketFactory;
    /**
     * @var $productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;
    /** @var UrlInterface */
    protected $_urlBuilder;
    protected $_eventFactory;
    protected $_sessionHelper;
    /**
     * @var $_statusHistory StatusFactory
     */
    protected $_statusHistory;
    public function __construct(
        SessionHelper $sessionHelper,
        UrlInterface $urlBuilder,
        \Magenest\Groupon\Model\StatusFactory $statusFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
        \Magenest\Ticket\Model\EventFactory $eventFactory
    ){
        $this->_urlBuilder = $urlBuilder;
        $this->productFactory = $product;
        $this->_attributeInterface = $attributeRepository;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_sessionFactory = $sessionFactory;
        $this->_orderedTicketFactory = $ticketFactory;
        $this->_eventFactory = $eventFactory;
        $this->_sessionHelper = $sessionHelper;
        $this->_statusHistory = $statusFactory;
    }

    public function getEventProductCollection($vendorId,$status,$sortby,$sorttype,$cur_page,$size){
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $reportTable = $resource->getTableName('report_event');
        $eventTable = $resource->getTableName('magenest_ticket_event');
        $ticketsTable = $resource->getTableName('magenest_ticket_tickets');
        $ticketTable = $resource->getTableName('magenest_ticket_ticket');
        $sessionTable = $resource->getTableName('magenest_ticket_sessions');
        $productTable = $resource->getTableName('catalog_product_entity');
        $productIntTable = $resource->getTableName('catalog_product_entity_int');
        $productVarcharTable = $resource->getTableName('catalog_product_entity_varchar');

        $sql = "SELECT
  view.*,
  count(" . $reportTable . ".event_id) AS view
FROM
  (SELECT
     old.*,
     count(ticket.event_id)   AS sold,
     sum(ticket.ticket_price) AS revenue
   FROM
     (SELECT
        `e`.entity_id,
        `at_udropship_vendor`.`value`              AS `udropship_vendor`,
        `at_campaign_status`.`value`  AS `campaign_status`,
        `" . $eventTable . "`.`event_id`,
        `" . $eventTable . "`.`event_name`,
        MIN(" . $sessionTable . ".session_from) AS `start`,
        MAX(" . $sessionTable . ".session_to)   AS `end`
      FROM `" . $productTable . "` AS `e`
        INNER JOIN `" . $productIntTable . "` AS `at_udropship_vendor`
          ON (`at_udropship_vendor`.`row_id` = `e`.`row_id`) AND (`at_udropship_vendor`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('udropship_vendor') . "') AND
             (`at_udropship_vendor`.`store_id` = 0)
        INNER JOIN `" . $productVarcharTable . "` AS `at_campaign_status`
          ON (`at_campaign_status`.`row_id` = `e`.`row_id`) AND (`at_campaign_status`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('campaign_status') . "') AND
             (`at_campaign_status`.`store_id` = 0)
        INNER JOIN `" . $eventTable . "` ON entity_id = " . $eventTable . ".product_id
        INNER JOIN `" . $ticketsTable . "` ON " . $eventTable . ".product_id = " . $ticketsTable . ".product_id
        LEFT JOIN `" . $sessionTable . "`
          ON " . $sessionTable . ".tickets_id = " . $ticketsTable . ".tickets_id
      WHERE
        (at_udropship_vendor.value = '" . $vendorId . "') AND (`e`.`type_id` = 'ticket') AND (e.created_in <= 1) AND
        (e.updated_in > 1)";

        if($status != 'all'){
            $sql .= " AND (at_campaign_status.value = '" . $status . "') ";
        }
        $sql .= "GROUP BY `entity_id`) AS old 
                LEFT JOIN `" . $ticketTable . "` AS ticket
                    ON ticket.product_id = old.entity_id
                GROUP BY entity_id) AS view
                LEFT JOIN `" . $reportTable . "` ON " . $reportTable . ".object_id = entity_id AND " . $reportTable . ".event_type_id = 1
                GROUP BY entity_id";
        if($sortby != '' && $sorttype != ''){
            if ($sortby === 'view') {
                $field = '`view`';
            } else {
                $field = '`view`.`' . $sortby . '`';
            }
            $sql .= " ORDER BY " . $field . " " . strtoupper($sorttype);
        }
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        if($hasDivided){
            if ($cur_page === 1) {
                $offset = 0;
            } else {
                $offset = (intval($cur_page) - 1) * intval($size);
            }
            $sql .= " LIMIT $size OFFSET $offset";
        }
        $result = $connection->fetchAll($sql);

        return $result;
    }
    public function getAttributeIdByEntityCode($code){
        try {
            $result = $this->_attributeInterface->get(ProductModel::ENTITY, $code);
            return $result->getAttributeId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }
    public function getFilter($status){
        if ($status === '0') {
            return $status;
        } else {
            return $status ? $status : 'all';
        }
    }
    public function getSessionDataByEventId($productId){
        $result = [];
        $ticketList = $this->_ticketsFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getAllIds();
        $result['ticket_options'] = count($ticketList);
        $sessions = $this->_sessionFactory->create()->getCollection()
            ->addFieldToFilter('tickets_id', array('in' => $ticketList));
        $result['buy_options'] = $sessions->getSize();
        $total_sale = 0;
        foreach ($sessions as $session){
            $total_sale += $session->getQty();
        }
        $result['redeemed'] = $this->getRedeemedAmount($productId);
        $result['sold'] = $result['redeemed']['total'];
        $result['total_ticket'] = $total_sale;
        $result['revenue'] = $result['redeemed']['revenue'] ? $result['redeemed']['revenue'] : 0;
        $result['product_id'] = $productId;

        return $result;
    }
    protected function getRedeemedAmount($productId){
        $ticketSold = $this->_orderedTicketFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $result['total'] = intval($ticketSold->getSize());
        $count = 0;
        $revenue = 0;
        foreach ($ticketSold as $item) {
            $revenue += floatval($item->getTicketPrice());
            if ($item->getStatus() === '2') {
                $count++;
            }
        }
        $result['redeem'] = $count;
        $result['revenue'] = $revenue;
        if ($result['total'] !== 0) {
            $result['percent'] = number_format($count / $result['total'] * 100, 2);
        } else {
            $result['percent'] = 0;
        }
        return $result;
    }
    public function getProductImage($productId){
        $image = $this->productFactory->create()->load($productId)->getImage();
        if ($image) {
            return $this->_urlBuilder->getBaseUrl() . 'media/catalog/product' . $image;
        } else {
            return $this->_urlBuilder->getBaseUrl() . 'media/catalog/product/placeholder/default/E3D-placeholder.jpg';
        }
    }

    public function getCampaignStatusCode($productId) {
        return $this->productFactory->create()->load($productId)->getCampaignStatus();
    }

    public function getCampaignStatus($productId){
        $campaignStatus = $this->productFactory->create()->load($productId)->getCampaignStatus();
        return $this->getCampaignStatusTitle($campaignStatus);
    }
    public function getCampaignStatusTitle($campaignId)
    {
        return CampaignStatus::getStatusLabelByCode($campaignId);
    }

    public function getProductById($productId){
        $product = $this->_eventFactory->create()->load($productId, 'product_id');
        return $product;
    }

    public function getSessionCount($productId){
        $tickets = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getAllIds();
        $sessions = $this->_sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $tickets])->getSize();
        return $sessions;
    }
    public function getCategoriesCount($productId){
        $product = $this->productFactory->create()->load($productId);
        return count($product->getCategoryIds());
    }

    public function getRedeemCollectionByProductId($productId){
        return $this->_orderedTicketFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 2);
    }
    public function getQtyRedeemByProductId($productId){
        return $this->getRedeemCollectionByProductId($productId)->count();
    }
    public function getActiveTicketsCollection($productId){
        return $this->_orderedTicketFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 1);
    }
    public function getQtyAvailableByProductId($productId){
        return $this->getActiveTicketsCollection($productId)->count();
    }
    public function getRefundedTicketsCollection($productId){
        return $this->_orderedTicketFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 3);
    }
    public function getQtyRefundedByProductId($productId){
        return $this->getRefundedTicketsCollection($productId)->count();
    }

    public function getEvent($productId){
        return $this->_eventFactory->create()->getCollection()->addFieldToFilter("product_id", $productId)->getFirstItem();
    }

    public function getEventType($productId){
        return $this->getEvent($productId)->getTicketType();
    }
    public function getRelatedSession($sessionId,$productId){
        $tickets = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getAllIds();
        $result = [];

        $currentSession = $this->_sessionFactory->create()->load($sessionId);
        $neededSessionStart = $currentSession->getSessionFrom();
        $neededSessionEnd = $currentSession->getSessionTo();

        $allSession = $this->_sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $tickets]);
        foreach ($allSession as $item) {
            if (abs(strtotime($neededSessionStart) - strtotime($item->getSessionFrom())) <= 60 && abs(strtotime($neededSessionEnd) - strtotime($item->getSessionTo())) <= 60) {
                $result[] = $item->getSessionId();
            }
        }
        return $result;
    }
    public function getSessionsCollection($sessionId = null,$page = '', $pageSize = '',$productId,$sort = []){

        /** @var \Magenest\Ticket\Model\ResourceModel\Session\Collection $collection */
        $collection = $this->_sessionFactory->create()->getCollection();
        if ($sessionId) {
            $collection->addFieldToFilter('main_table.session_id', ['in' => $this->getRelatedSession($sessionId,$productId)]);
        }
        if($this->getEventType($productId)=='free')
            $revenue = "`main_table`.`qty_purchased`*0";
        else
            $revenue ="`main_table`.`sale_price`*`main_table`.`qty_purchased`";
        $collection->join(
            $collection->getTable('magenest_ticket_sessions'),
            "`main_table`.`session_id`=`magenest_ticket_sessions`.`session_id`
            AND `main_table`.`product_id` = ".$productId,
            "(".$revenue.") AS revenueOld"
        );
        if(is_numeric($page)&&is_numeric($pageSize)){
            $flag = true;
        }else{
            $flag = false;
        }
        if(!empty($sort)){
            $sortby = $sort['sort_by'];
            $sorttype = $sort['sort_type'];
            $collection->setOrder($sortby, strtoupper($sorttype));
        }
        if($flag){
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }
    public function getQtyTicketSold($productId){
        $sessions = $this->getSessionsCollection($sessionId = null,$page = '', $pageSize = '',$productId,$sort = []);
        $qtyPurchased = 0;
        foreach ($sessions as $session) {
            $qtyPurchased += $session->getQtyPurchased();
        }
        return $qtyPurchased;
    }
    public function getDays($dayNum,$productId,$sessionId = null){
        $now = new \DateTime('now');
        $list = array();
        if ($dayNum === "all_time") {
            $saleStart = null;
            $saleEnd = null;
            $sessions = $this->getSessionsCollection($sessionId,$page = '', $pageSize = '',$productId,$sort = []);
            foreach ($sessions as $session) {
                $sellFrom = date('Y-m-d', strtotime($session->getSellFrom()));
                $sellTo = date('Y-m-d', strtotime($session->getSellTo()));
                if ($saleStart == null || $sellFrom < $saleStart)
                    $saleStart = $sellFrom;
                if ($saleEnd == null || $sellTo > $saleStart)
                    $saleEnd = $sellTo;
            }
            $start = strtotime($saleStart);
            $end = strtotime($saleEnd);
            $diffInSeconds = $end - $start;
            $datediff = $diffInSeconds / 86400;
            for ($d = 0; $d <= $datediff; $d++) {
                $list[] = $saleStart;
                $saleStart = date('Y-m-d', strtotime('+1 day', strtotime($saleStart)));
            }
        } elseif ($dayNum === "this_month") {
            $month = $now->format('m');
            $year = $now->format('Y');

            for ($d = 1; $d <= 31; $d++) {
                $time = mktime(12, 0, 0, $month, $d, $year);
                if (date('m', $time) == $month)
                    $list[] = date('Y-m-d', $time);
            }
        } else {
            for ($i = $dayNum - 1; $i >= 0; $i--) {
                $date = new \DateTime();
                $date->sub(new \DateInterval('P' . $i . 'D'));
                $list[] = $date->format('Y-m-d');
            }
        }
        return $list;
    }
    public function getTicketsSoldFrom($daysList,$productId){
        $return = [];
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_orderedTicketFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', ['nin' => [3]])
                ->addFieldToFilter('created_at', array('like' => "%$day%"));
            $return [$day] = [
                'sales' => 0,
                'qtySold' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['sales'] += $sold;
                $return [$day]['qtySold']++;
            }
        }
        return $return;
    }
    public function getTicketsRefundedFrom($daysList,$productId){
        $return = [];
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_orderedTicketFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', ['in' => [3]])
                ->addFieldToFilter('refunded_at', array('like' => "%$day%"));
            $return [$day] = [
                'refunds' => 0,
                'qtyRefund' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['refunds'] += $sold;
                $return [$day]['qtyRefund']++;
            }
        }
        return $return;
    }
    public function getOption($dayOption,$productId){
        $daysList = $this->getDays($dayOption,$productId);
        $return = [];
        $ticketsSoldFrom = $this->getTicketsSoldFrom($daysList,$productId);
        $summarySoldFrom = $this->getValueTicketSoldFrom($ticketsSoldFrom);
        $ticketsRefundedFrom = $this->getTicketsRefundedFrom($daysList,$productId);
        $summaryRefundedFrom = $this->getValueTicketRefundedFrom($ticketsRefundedFrom);
        $summaryRedeemFrom = $this->getQtyRedeemedFrom($daysList,$productId);
        $arrSummary = [
            'sale_value' => $summarySoldFrom['sales'],
            'ticket_sold' => $summarySoldFrom['qty'],
            'ticket_redeem' => $summaryRedeemFrom,
            'refunded_value' => $summaryRefundedFrom['refunds'],
            'refunded_qty' => $summaryRefundedFrom['qtyRefund']
        ];
        foreach ($daysList as $day) {
            $return [$day] = [
                'sales' => $ticketsSoldFrom[$day]['sales'],
                'qtySold' => $ticketsSoldFrom[$day]['qtySold'],
                'refunds' => $ticketsRefundedFrom[$day]['refunds'],
                'qtyRefund' => $ticketsRefundedFrom[$day]['qtyRefund']
            ];
        }
        return ['option' => $return, 'summary' => $arrSummary];
    }
    public function getValueTicketSoldFrom($ticketsSold){
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['sales'];
            $qty += $day['qtySold'];
        }
        return [
            'sales' => $sale,
            'qty' => $qty
        ];
    }
    public function getValueTicketRefundedFrom($ticketsSold){
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['refunds'];
            $qty += $day['qtyRefund'];
        }
        return [
            'refunds' => $sale,
            'qtyRefund' => $qty
        ];
    }
    public function getQtyRedeemedFrom($daysList,$productId){
        $qtyRedeemed = 0;
        foreach ($daysList as $day) {
            $ticketsPurchased = $this->_orderedTicketFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', 2)
                ->addFieldToFilter('redeemed_at', array('like' => "%$day%"));
            $qtyRedeemed += $ticketsPurchased->count();
        }
        return $qtyRedeemed;
    }

    public function getCurrencySymbol(){
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Directory\Model\CurrencyFactory')->create()
            ->load($currencyCode)->getCurrencySymbol();
    }
    public function getSessionStatus($sessionId){
        return $this->_sessionHelper->getSessionStatus($sessionId);
    }
    public function getQtyRedeemed($sessionId,$productId){
        return $this->_sessionHelper->getQtyRedeemed($sessionId, $productId);

    }
    public function getSessionTitle($sessionId){
        $session = $this->_sessionFactory->create()->load($sessionId);
        $sessionStart = strtotime($session->getSessionFrom());
        return __(date("D, M d, Y %1 h:m A", $sessionStart), "at");
    }
    private function getEventId($productId){
        return $this->_eventFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)
            ->getFirstItem()->getId();
    }
    public function getPurchasedTicketList($vendorId,$sessionId = null,$search = '',$page = '',$pageSize = '',$sort = [],$productId){
        if(is_numeric($page) && is_numeric($pageSize)){
            $flag = true;
        }else{
            $flag = false;
        }
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_orderedTicketFactory->create()->getCollection();
        $collection->addFieldToFilter('event_id', $this->getEventId($productId))->addFieldToFilter('vendor_id', $vendorId);
//        $params = $this->getRequest()->getParams();
//        if ($sessionId) {
//            $collection->addFieldToFilter('main_table.session_id', ['in' => $this->getRelatedSession($sessionId,$vendorId)]);
//        }
        if ($search != '') {
            $pattern = '%' . $search . '%';
            $collection->addFieldToFilter(['code','customer_name'], [['like' => $pattern], ['like' => $pattern]]);
        }
        if ($flag) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        $eventTable = $collection->getTable('magenest_ticket_sessions');
        $ticketsTable = $collection->getTable('magenest_ticket_tickets');
        if(!empty($sort)){
            $sortby = $sort['sort_by'];
            $sorttype = $sort['sort_type'];
            $joinStatement = $collection->getSelect()->joinLeft(
                $eventTable,
                "main_table.session_id=$eventTable.session_id",
                ["$eventTable.session_from"]
            )->joinLeft(
                $ticketsTable,
                "main_table.tickets_id=$ticketsTable.tickets_id",
                ["$ticketsTable.title as ticket_type"]
            )->order($sortby. ' '.strtoupper($sorttype))->__toString();
        }else{
            $joinStatement = $collection->getSelect()->joinLeft(
                $eventTable,
                "main_table.session_id=$eventTable.session_id",
                ["$eventTable.session_from"]
            )->joinLeft(
                $ticketsTable,
                "main_table.tickets_id=$ticketsTable.tickets_id",
                ["$ticketsTable.title as ticket_type"]
            )->__toString();
        }


        $collection->getConnection()->fetchAll($joinStatement);
        return $collection;
    }
    public function getTicketStatus($status){
        $result = null;
        switch ($status) {
            case Ticket::STATUS_AVAILABLE:
            {
                $result = 'Available';
                break;
            }
            case Ticket::STATUS_USED:
            {
                $result = 'Used';
                break;
            }
            case Ticket::STATUS_REFUNDED:
            {
                $result = 'Refunded';
                break;
            }
            case Ticket::STATUS_CANCELED:
            {
                $result = 'Canceled';
                break;
            }
            default:
            {
                $result = null;
                break;
            }
        }
        return $result;
    }

    public function getChangeHistory($productId,$page,$pageSize){
        $collection = $this->_statusHistory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->setOrder('created_at', 'DESC');
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }

    public function getStatusLabel($id){
        return CampaignStatus::getStatusLabelByCode($id);
    }

    public function getActiveTicketSessionCollection($sessionId,$productId){
        return $this->_orderedTicketFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 1)
            ->addFieldToFilter("session_id", ['in' => $this->getRelatedSession($sessionId,$productId)]);
    }

    public function getTicketSessionQtyAvailable($sessionId,$productId){
        return $this->getActiveTicketSessionCollection($sessionId,$productId)->count();
    }
    public function getRefundedTicketSessionCollection($sessionId,$productId){
        return $this->_orderedTicketFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 3)
            ->addFieldToFilter("session_id", ['in' => $this->getRelatedSession($sessionId,$productId)]);
    }
    public function getTicketSessionQtyRefunded($sessionId,$productId){
        return $this->getRefundedTicketSessionCollection($sessionId,$productId)->count();
    }
    public function getTicketSessionQtySold($sessionId, $productId){
        $sessions = $this->getSessionsCollection($sessionId,$page = '', $pageSize = '',$productId,$sort = []);
        $qtyPurchased = 0;
        foreach ($sessions as $session) {
            $qtyPurchased += $session->getQtyPurchased();
        }
        return $qtyPurchased;

    }
    public function getTicketSessionOption($dayOption,$productId,$sessionId){
        $daysList = $this->getDays($dayOption,$productId,$sessionId);
        $return = [];
        $ticketsSoldFrom = $this->getTicketSessionSoldFrom($daysList,$productId,$sessionId);
        $ticketsRefundedFrom = $this->getTicketSessionRefundedFrom($daysList,$productId,$sessionId);
        foreach ($daysList as $day) {
            $return [] = [
                'date_text' => $day,
                'sales' => $ticketsSoldFrom[$day]['sales'],
                'qtySold' => $ticketsSoldFrom[$day]['qtySold'],
                'refunds' => $ticketsRefundedFrom[$day]['refunds'],
                'qtyRefund' => $ticketsRefundedFrom[$day]['qtyRefund']
            ];
        }
        $ticketSessionSoldValue = $this->getValueTicketSessionSoldFrom($daysList,$productId,$sessionId);
        $ticketSessionRefundedValue = $this->getValueTicketSessionRefundedFrom($daysList,$productId,$sessionId);
        $summary = [
            'sales_value' => $ticketSessionSoldValue['sales'],
            'tickets_sold' => $ticketSessionSoldValue['qty'],
            'tickets_redeem' => $this->getQtyRedeemedFrom($daysList,$productId),
            'refunds_value' => $ticketSessionRefundedValue['refunds'],
            'refunds_quantity' => $ticketSessionRefundedValue['qtyRefund']
        ];
        return ['option' => $return, 'summary' => $summary];
    }
    public function getTicketSessionSoldFrom($daysList,$productId,$sessionId){
        $return = [];
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_orderedTicketFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('session_id', ['in' => $this->getRelatedSession($sessionId,$productId)])
                ->addFieldToFilter('status', ['nin' => [3]])
                ->addFieldToFilter('created_at', array('like' => "%$day%"));
            $return [$day] = [
                'sales' => 0,
                'qtySold' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['sales'] += $sold;
                $return [$day]['qtySold']++;
            }
        }
        return $return;
    }
    public function getTicketSessionRefundedFrom($daysList,$productId,$sessionId){
        $return = [];
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_orderedTicketFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('session_id', ['in' => $this->getRelatedSession($sessionId,$productId)])
                ->addFieldToFilter('status', ['in' => [3]])
                ->addFieldToFilter('refunded_at', array('like' => "%$day%"));
            $return [$day] = [
                'refunds' => 0,
                'qtyRefund' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['refunds'] += $sold;
                $return [$day]['qtyRefund']++;
            }
        }
        return $return;
    }
    public function getValueTicketSessionSoldFrom($daysList,$productId,$sessionId){
        $ticketsSold = $this->getTicketSessionSoldFrom($daysList,$productId,$sessionId);
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['sales'];
            $qty += $day['qtySold'];
        }
        return [
            'sales' => $sale,
            'qty' => $qty
        ];
    }
    public function getValueTicketSessionRefundedFrom($daysList,$productId,$sessionId){
        $ticketsSold = $this->getTicketSessionRefundedFrom($daysList,$productId,$sessionId);
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['refunds'];
            $qty += $day['qtyRefund'];
        }
        return [
            'refunds' => $sale,
            'qtyRefund' => $qty
        ];
    }

    public function getTicketTypeBySessionCollection($sessionId,$productId,$cur_page,$size,$sort){
        /** @var \Magenest\Ticket\Model\ResourceModel\Session\Collection $collection */
        $collection = $this->_sessionFactory->create()->getCollection();
        $sessions = $this->getRelatedSession($sessionId,$productId);
        if ($sessionId) {
            $collection->addFieldToFilter('main_table.session_id', ['in' => $sessions]);
        }
        if($this->getEventType($productId)=='free')
            $revenue = "`main_table`.`qty_purchased`*0";
        else
            $revenue ="`main_table`.`sale_price`*`main_table`.`qty_purchased`";
        $collection->join(
            $collection->getTable('magenest_ticket_sessions'),
            "`main_table`.`session_id`=`magenest_ticket_sessions`.`session_id`
            AND `main_table`.`product_id` = ".$productId,
            "(".$revenue.") AS revenueOld"
        )->join(
            $collection->getTable('magenest_ticket_tickets'),
            "`main_table`.`tickets_id` = `magenest_ticket_tickets`.`tickets_id`",
            ["magenest_ticket_tickets.title AS ticket_category", "magenest_ticket_tickets.tickets_id as ticket_ref"]);

        if(is_numeric($cur_page)&&is_numeric($size)){
            $flag = true;
        }else{
            $flag = false;
        }
        if($flag){
            $collection->setPageSize($size)->setCurPage($cur_page);
        }
        if(!empty($sort)){
            $sortby = $sort['sort_by'];
            $sorttype = $sort['sort_type'];
            $collection->setOrder($sortby, strtoupper($sorttype));
        }

        return $collection;
    }

}