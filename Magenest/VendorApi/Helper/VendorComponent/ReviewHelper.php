<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper\VendorComponent;

use Magento\Framework\DataObject;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Review\Model\Rating\Option\VoteFactory;
use Magento\Review\Model\ReviewFactory;
use Magento\Review\Model\Review\SummaryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use \Magenest\FixUnirgy\Helper\Data;
use Magenest\FixUnirgy\Helper\Review\Data as FUData;
use Unirgy\Dropship\Model\VendorFactory;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class ReviewHelper
{
    /**
     * @var $_reviewSummary SummaryFactory
     */
    protected $_reviewSummary;

    /**
     * @var $_productFactory ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_magentoRatingOptionVoteFactory VoteFactory
     */
    protected $_magentoRatingOptionVoteFactory;

    /**
     * @var ReviewFactory
     */
    protected $_magentoReviewFactory;

    protected $_magentoReviewCollectionFactory;
    /**
     * @var DataObject
     */
    protected $_objectFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magenest\FixUnirgy\Helper\Data
     */
    protected $_helperVendorRatingsData;
    /**
     * @var $_vendorFactory VendorFactory
     */
    protected $_vendorFactory;
    protected $_hlp;
    protected $_customerRepositoryInterface;
    /**
     * @var HelperData
     */
    protected $_rateHlp;
    protected $_inventoryHelper;
    private $productCollectionFactory;

    /**
     * QuestionHelper constructor.
     * @param DataObject $dataObject
     * @param VoteFactory $voteFactory
     * @param ReviewFactory $reviewFactory
     * @param ProductFactory $productFactory
     * @param SummaryFactory $summaryFactory
     * @param StoreManagerInterface $modelStoreManagerInterface
     * @param Data $helperVendorRatingsData
     * @param VendorFactory $vendorFactory
     * @param \Unirgy\Dropship\Helper\Data $udropshipHelper
     * @param CollectionFactory $reviewCollectionFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param HelperData $helperData
     */
    public function __construct(DataObject $dataObject,
                                VoteFactory $voteFactory,
                                ReviewFactory $reviewFactory,
                                ProductFactory $productFactory,
                                SummaryFactory $summaryFactory,
                                StoreManagerInterface $modelStoreManagerInterface,
                                \Magenest\FixUnirgy\Helper\Data $helperVendorRatingsData,
                                VendorFactory $vendorFactory,
                                \Unirgy\Dropship\Helper\Data $udropshipHelper,
                                \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
                                HelperData $helperData,
                                \Magenest\VendorApi\Helper\VendorComponent\InventoryHelper $_inventoryHelper,
                                \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_reviewSummary = $summaryFactory;
        $this->_productFactory = $productFactory;
        $this->_magentoRatingOptionVoteFactory = $voteFactory;
        $this->_magentoReviewFactory = $reviewFactory;
        $this->_objectFactory = $dataObject;
        $this->_storeManager = $modelStoreManagerInterface;
        $this->_helperVendorRatingsData = $helperVendorRatingsData;
        $this->_vendorFactory = $vendorFactory;
        $this->_hlp = $udropshipHelper;
        $this->_magentoReviewCollectionFactory = $reviewCollectionFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_rateHlp = $helperData;
        $this->_inventoryHelper = $_inventoryHelper;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    public function getAllCustomerReviewForProduct($customer_id, $cur_page, $size)
    {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        $reviewCollection = $this->_magentoReviewFactory->create()->getCollection();
        $reviews = $reviewCollection->addFieldToFilter('customer_id', $customer_id)->addFieldToFilter('entity_id', 1);
        if ($hasDivided) {
            $reviews->setCurPage($cur_page)->setPageSize($size);
        }

        $reviewResult = [];
        $totalReview = 0;
        $totalPoint = 0;
        foreach ($reviews as $item) {
            $products = $this->_productFactory->create();
            $product = $products->load($item->getEntityPkValue());
            $review = $this->_reviewSummary->create()->load($item->getEntityPkValue());
            $totalPoint += floatval($review->getRatingSummary());
            $totalReview++;
            $productImage = $this->_inventoryHelper->getImageUrl($product);

            $data = [
                'product_name' => $product->getName(),
                'product_image' => $productImage,
                'created_at' => $item->getCreatedAt(),
                'rating_summary' => $review->getRatingSummary(),
                'nickname' => $item->getNickname(),
                'title' => $item->getTitle(),
                'detail' => $item->getDetail(),
                'status' => $item->getStatusId(),
                'stores' => $review->getStores()
            ];
            array_push($reviewResult, $data);
        }

        $result[] = $reviewResult;

        $general[] = [
            'average_rating_summary' => number_format($totalPoint / $totalReview, 2),
            'total_review' => $totalReview
        ];
        $result[] = $general;

        return $result;
    }

    public function getAllCustomerReviewForVendors($customer_id,$cur_page, $size){
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        /**
         * @param \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection $reviewCollection
         */
        $reviewCollection = $this->_magentoReviewFactory->create()->getCollection();
        if ($hasDivided) {
            $reviewCollection->setCurPage($cur_page)->setPageSize($size);
        }
        $this->shipments($reviewCollection);
        $this->orderItems($reviewCollection);
        $this->orders($reviewCollection);
        $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [10, FUData::ENTITY_TYPE_ID]])
            ->addCustomerFilter($customer_id)
            ->setDateOrder()
            ->addRateVotes();
        $results = array();
        foreach ($reviewCollection->getItems() as $items) {
            $dataTmp = $items->getData();
            $vendor = $this->_vendorFactory->create()->load($dataTmp['entity_pk_value']);
            $vendorName = $vendor->getVendorName();
            $data['review_id'] = $dataTmp['review_id'];
            $data['created_at'] = $dataTmp['created_at'];
            $data['vendor_id'] = $dataTmp['entity_pk_value'];
            $data['vendor_name'] = $vendorName;
            $data['shipment_order_id'] = $dataTmp['shipment_order_id'];
            $data['store_id'] = $dataTmp['store_id'];
            $data['status_id'] = $dataTmp['status_id'];
            $data['rel_entity_pk_value'] = $dataTmp['rel_entity_pk_value'];
            $data['helpfulness_yes'] = $dataTmp['helpfulness_yes'];
            $data['helpfulness_no'] = $dataTmp['helpfulness_no'];
            $data['helpfulness_pcnt'] = $dataTmp['helpfulness_pcnt'];
            $data['title'] = $dataTmp['title'];
            $data['detail'] = $dataTmp['detail'];
            $data['customer_nickname'] = $dataTmp['nickname'];
            $data['customer_id'] = $dataTmp['customer_id'];
            $data['review_entity_id'] = $dataTmp['review_entity_id'];
            $data['order_id'] = $dataTmp['order_id'];
            $data['increment_id'] = $dataTmp['increment_id'];
            $data['updated_at'] = $dataTmp['updated_at'];
            $data['product_name'] = $dataTmp['name'];
            $data['review_increment_id'] = $dataTmp['review_increment_id'];

            $ratingVotes = $items->getRatingVotes();
            $_votes = [];
            foreach ($ratingVotes as $vote) {
                $_votes[] = $vote->getData();
            }
            $data['rating_votes'] = $_votes;
            $results[] = $data;
        }
        return $results;
    }
    public function shipments($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['sm' => 'sales_shipment'],
                "main_table.rel_entity_pk_value = sm.entity_id",
                ['review_entity_id' => 'main_table.entity_id','shipment_order_id' => 'sm.order_id', '*']
            );
        return $collection;
    }
    public function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'sales_order_item'],
                "main_table.rel_entity_pk_value = soi.item_id"
            );
        return $collection;
    }

    public function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => 'sales_order'],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );
        return $collection;
    }
    public function getProductReviewById($store, $productId)
    {
        $reviews = ObjectManager::getInstance()->create('Magenest\SellYours\Helper\RatingHelper')->getReviewCollection($productId, true);
        if ($store !== '0') {
            $reviews->addFieldToFilter('store_id', $store);
        }

        $reviewResult = [];
        $totalReview = 0;
        $totalPoint = 0;
        foreach ($reviews as $item) {
            $review = $this->_magentoRatingOptionVoteFactory->create()->load($item->getReviewId(), 'review_id');
            $totalPoint += floatval($review->getPercent());
            $totalReview++;

            $data = [
                'review_nickname' => $item->getNickname(),
                'created_at' => $item->getCreatedAt(),
                'rating_summary' => $review->getPercent(),
                'review_title' => $item->getTitle(),
                'review_detail' => $item->getDetail()
            ];
            array_push($reviewResult, $data);
        }

        $result['rating_details'] = $reviewResult;
        if($totalReview != 0){
            $result['rating_average'] = number_format($totalPoint / $totalReview, 2);
        }else{
            $result['rating_average'] = '';
        }
        $result['rating_count'] = $totalReview;

        return $result;
    }

    public function loadProduct($productId){
        if (!$productId) {
            return false;
        }
        try {
            $product = $this->_productFactory->create()->load($productId);
            if (!$product->isVisibleInCatalog() || !$product->isVisibleInSiteVisibility()) {
                throw new NoSuchEntityException();
            }
            return $product;
        } catch (NoSuchEntityException $noEntityException) {
            return false;
        }
    }

    public function sortData($results, $sortby, $sort){
        $data = array();
        foreach ($results as $key => $row){
            $data[$key] = $row[$sortby];
        }
        array_multisort($data,$sort,$results);
        return $results;
    }

    public function filterCustomerReviewByVendor($customer_id,$filterby,$datafilter,$cur_page,$size,$sort,$review_entity){
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        $reviewCollection = $this->_magentoReviewCollectionFactory->create();
        if($hasDivided){
            $reviewCollection->setCurPage($cur_page)->setPageSize($size);
        }
        if(!empty($sort)&&is_array($sort)){
            $sortby = $sort['sortby'];
            $reviewCollection->setOrder("main_table.$sortby",$sort['sorttype']);
        }elseif (!empty($sort)){
            $reviewCollection->setOrder("main_table.$sort",'DESC');
        }
        if(!empty($filterby)&&!empty($datafilter)){
            if(is_array($datafilter)){
                $this->addDateFilter($reviewCollection,$filterby,$datafilter);
            }else{
                $this->addFilterString($reviewCollection,$filterby,$datafilter);
            }
        }
        $this->shipments($reviewCollection);
        $this->orderItems($reviewCollection);
        $this->orders($reviewCollection);
        $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId())
        ->addFieldToFilter('main_table.entity_id', ['eq' => $review_entity])
            ->addCustomerFilter($customer_id)
            ->addRateVotes();
        $results = array();
        foreach ($reviewCollection->getItems() as $items) {
            $products = $this->_productFactory->create();
            $product = $products->load($items['entity_pk_value']);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $productImage = $product->getImage();

            $dataTmp = $items->getData();
            $vendor = $this->_vendorFactory->create()->load($dataTmp['entity_pk_value']);
            $vendorName = $vendor->getVendorName();
            $data['review_id'] = $dataTmp['review_id'];
            $data['created_at'] = $dataTmp['created_at'];
            $data['vendor_id'] = $dataTmp['entity_pk_value'];
            $data['vendor_name'] = $vendorName;
            $data['shipment_order_id'] = $dataTmp['shipment_order_id'];
            $data['store_id'] = $dataTmp['store_id'];
            $data['status_id'] = $dataTmp['status_id'];
            $data['rel_entity_pk_value'] = $dataTmp['rel_entity_pk_value'];
            $data['helpfulness_yes'] = $dataTmp['helpfulness_yes'];
            $data['helpfulness_no'] = $dataTmp['helpfulness_no'];
            $data['helpfulness_pcnt'] = $dataTmp['helpfulness_pcnt'];
            $data['title'] = $dataTmp['title'];
            $data['detail'] = $dataTmp['detail'];
            $data['customer_nickname'] = $dataTmp['nickname'];
            $data['customer_id'] = $dataTmp['customer_id'];
            $data['review_entity_id'] = $dataTmp['review_entity_id'];
            $data['order_id'] = $dataTmp['order_id'];
            $data['increment_id'] = $dataTmp['increment_id'];
            $data['updated_at'] = $dataTmp['updated_at'];
            $data['product_name'] = $dataTmp['name'];
            $data['product_image'] = $productImage;//$product_image;
            $data['review_increment_id'] = $dataTmp['review_increment_id'];

            $ratingVotes = $items->getRatingVotes();
            $_votes = [];
            foreach ($ratingVotes as $vote) {
                $_votes[] = $vote->getData();
            }
            $data['rating_votes'] = $_votes;
            $results[] = $data;
        }
        return $results;
    }
    public function filterCustomerReviewByProduct($customer_id,$filterby,$datafilter,$cur_page,$size,$sort){
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }

        $reviewCollection = $this->_magentoReviewCollectionFactory->create();
        if($hasDivided){
            $reviewCollection->setCurPage($cur_page)->setPageSize($size);
        }
        $arr = ['detail_id', 'title', 'detail', 'nickname', 'customer_id','review_id', 'created_at', 'entity_id', 'entity_pk_value', 'status_id', 'rel_entity_pk_value', 'helpfulness_yes', 'helpfulness_no', 'helpfulness_pcnt'];
        if(!empty($sort)&&is_array($sort)&&in_array($sort['sortby'],$arr)){
            $sortby = $sort['sortby'];
            $reviewCollection->setOrder("main_table.$sortby",$sort['sorttype']);
        }elseif (!empty($sort)&&in_array($sort,$arr)){
            $reviewCollection->setOrder("main_table.$sort",'DESC');
        }
        $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToFilter('main_table.entity_id', ['eq' => 1])
            ->addCustomerFilter($customer_id)
            ->addRateVotes();
        if(!empty($filterby)&&$datafilter != ''){
            if(is_array($datafilter)){
                $this->addDateFilter($reviewCollection,$filterby,$datafilter);
            }else{
                $this->addFilterString($reviewCollection,$filterby,$datafilter);
            }
        }

        $reviewResult = [];

//        $items = $reviewCollection->getItems();
        foreach ($reviewCollection->getData() as $item){
            $products = $this->_productFactory->create();
            $product = $products->load($item['entity_pk_value']);
            $review = $this->_reviewSummary->create()->load($item['entity_pk_value']);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $productImage = $product->getImage();

            $data = [
                'review_id' => $item['review_id'],
                'product_id' => $product->getId(),
                'product_name' => $product->getName(),
                'product_type' => $product->getTypeId(),
                'product_image' => $productImage,
                'created_at' => $item['created_at'],
                'rating_summary' => $review->getRatingSummary(),
                'nickname' => $item['nickname'],
                'title' => $item['title'],
                'detail' => $item['detail_id'],
                'stores' => $review->getStores()
            ];
            array_push($reviewResult, $data);
        }
        if(!empty($sort)&&is_array($sort)&&($sort['sortby'] == 'rating_summary')){
            if($sort['sorttype'] == 'DESC'){
                $sorttype = SORT_DESC;
            }else{
                $sorttype = SORT_ASC;
            }
            $results = $this->sortData($reviewResult,$sort['sortby'],$sorttype);
            return $results;
        }
        return $reviewResult;
    }
    public function addDateFilter($collection,$filterby,$datafilter){
        $arrDC = ['detail_id', 'title', 'detail', 'nickname', 'customer_id'];
        $fromDate = $datafilter['from'] < $datafilter['to'] ? $datafilter['from'] : $datafilter['to'];
        $toDate = $datafilter['to'] > $datafilter['from'] ? $datafilter['to'] : $datafilter['from'];
        if(in_array($filterby,$arrDC)){
            $collection->getSelect()->where("detail.$filterby >= '$fromDate' and detail.$filterby <= '$toDate'");
        }else{
            $collection->getSelect()->where("main_table.$filterby >= '$fromDate' and main_table.$filterby <= '$toDate'");
        }
        return $collection;
    }
    public function addFilterString($collection,$filterby,$datafilter){
        $arrDC = ['detail_id', 'title', 'detail', 'nickname', 'customer_id'];
        if(in_array($filterby,$arrDC)){
             $collection->getSelect()->where("detail.$filterby LIKE '%$datafilter%'");
        }else{
            $x = $collection->getSelect()->where("main_table.$filterby LIKE '%$datafilter%'")->__toString();
        }
        return $collection;
    }

    public function getDetailReviewByReviewId($reviewId){
        $reviewCollection = $this->_magentoReviewFactory->create()->getCollection();
        $reviewCollection->addFieldToFilter('main_table.review_id', $reviewId)->addRateVotes();
        $results = [];
        foreach ($reviewCollection->getItems() as $item){
            $data = $item->getData();
            $ratingVotes = $item->getRatingVotes();
            $_votes = [];
            foreach ($ratingVotes as $vote) {
                $_votes[] = $vote->getData();
            }
            $data['rating_votes'] = $_votes;
            $entity_pk_value = $item->getEntityPkValue();
            $entityId = $item->getEntityId();
            if($entityId == 10){
                $vendor = $this->_vendorFactory->create()->load($entity_pk_value);
                $data['vendor_name'] = $vendor->getVendorName();
            }
            if($entityId == 1){
                $products = $this->_productFactory->create();
                $product = $products->load($item->getEntityPkValue());
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                if($product->getImage() != '' || $product->getImage() != null){
                    $productImage = $storeManager->getStore()->getBaseUrl() . 'pub/media/catalog/product' . $product->getImage();
                }else{
                    $productImage = '';
                }
                $data['product_image'] = $productImage;
                $data['product_name'] = $product->getName();
            }
            if(!empty($item->getCustomerId())){
                $customer = $this->_customerRepositoryInterface->getById($item->getCustomerId());
                $data['customer_name'] = $customer->getFirstName().' '.$customer->getLastName();
                $data['customer_email'] = $customer->getEmail();
            }
            $results[] = $data;
        }
        return $results;
    }

    public function getVendorReviewCollection($vendorId){
        return $this->_magentoReviewFactory->create()->getCollection()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [10, FUData::ENTITY_TYPE_ID]])
            ->addFieldToFilter('main_table.entity_pk_value', ['eq' => $vendorId])
            ->setFlag('AddRateVotes', true)
            ->setFlag('AddAddressData', true);
    }

    public function getReviewVendorPendingCollection($customerId){
        $reviewCollection = $this->_rateHlp->udHlp()->createObj('\Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Shipment\Collection')
            ->addCustomerFilter($customerId)
            ->addPendingFilter();
        return $reviewCollection;
    }


    public function getGrouponLocationById($locationId){
        $locationData = \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Groupon\Model\Location')->load($locationId);
        $locationDetails = "";
        if ($locationData->getRegion()) {
            $locationDetails .= $locationData->getRegion() . ", ";
        } elseif ($locationData->getCity()) {
            $locationDetails .= $locationData->getCity() . ", ";
        }
        if ($locationData->getCountry()) {
            $locationDetails .= \Magento\Framework\App\ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($locationData->getCountry());
        }
        $map_info = $locationData->getMapInfo();

        return ['location_detail' => $locationDetails,'lat_long' => $map_info];
    }
    public function getPriceProductGroupon($childIds){
        $vendorPrice = 0;
        $vendorSpecial = 0;
        foreach ($childIds as $childId){
            $product = $this->productCollectionFactory->create()->addAttributeToSelect('*')->addFieldToFilter('entity_id', $childId);
            $priceTmp = $this->setConfigurableVendorPrice($product);
            if(!empty($priceTmp)&&$priceTmp['vendor_price']!=null){
                $vendorPrice = $priceTmp['vendor_price'] > $vendorPrice ? $priceTmp['vendor_price'] : $vendorPrice;
                if($priceTmp['vendor_special_price'] < $vendorSpecial || $vendorSpecial == 0){
                    $vendorSpecial = $priceTmp['vendor_special_price'];
                }
            }
        }
        return [
            'vendor_price' => $vendorPrice,
            'vendor_special_price' => $vendorSpecial
        ];
    }
    private function setConfigurableVendorPrice($product)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $childProductCollection */
        $childProductCollection = $product;
        $childProductCollection
            ->setRowIdFieldName('vendor_product_id')
            ->getSelect()
            ->joinLeft(
                ['udvp' => $childProductCollection->getTable('udropship_vendor_product')],
                'udvp.product_id = e.entity_id',
                ['*', 'vendor_special_price' => 'udvp.special_price']
            );
        $minPrice = null;
        $lowestPriceProduct = null;
        /** @var Product $childProduct */
        foreach ($childProductCollection as $childProduct) {
            $price = $childProduct->getVendorPrice();
            $minPrice = $minPrice ?:$price;
            $lowestPriceProduct = $lowestPriceProduct ?:$childProduct;
            if ($price < $minPrice) {
                $lowestPriceProduct = $childProduct;
            }
        }
        if ($lowestPriceProduct->getEntityId()&&$lowestPriceProduct->getStockQty()>0) {
            if($lowestPriceProduct->getVendorPrice() == null || $lowestPriceProduct->getVendorPrice() == ""){
                $price = [
                    'vendor_price' => $lowestPriceProduct->getPrice(),
                    'vendor_special_price' => $lowestPriceProduct->getFinalPrice()
                ];
            }else{
                $price = [
                    'vendor_price' => $lowestPriceProduct->getVendorPrice(),
                    'vendor_special_price' => $lowestPriceProduct->getVendorSpecialPrice()
                ];
            }
        }else{
            $price = [];
        }
        return $price;
    }
}
