<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper;

use Braintree\Exception;
use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\UrlInterface;

/**
 * Class EventTicketHelper
 * @package Magenest\VendorApi\Helper
 */
class EventTicketHelper
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    protected $_reportFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Unirgy\DropshipVendorRatings\Helper\Data
     */
    protected $_vendorReviewHelper;

    /**
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var \Magento\Review\Model\Rating\Option\VoteFactory
     */
    protected $_voteFactory;

    /**
     * @var \Magenest\Ticket\Model\TicketFactory
     */
    protected $ticket;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $event;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $eventLocation;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $ticketsType;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magenest\VendorTicket\Model\VenueMapFactory
     */
    protected $_venueMap;

    /**
     * @var \Magenest\VendorTicket\Model\VenueMapDataFactory
     */
    protected $_venueMapData;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;
    protected $ratingOptionHelper;
    protected $_magentoReviewFactory;
    protected $modelStoreManagerInterface;
    /**
     * @var $_countryFactory \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;
    /**
     * EventTicketHelper constructor.
     * @param \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory
     * @param \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreFactory $storeManager
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param ImageFactory $imageFactory
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsTypeFactory
     * @param \Magenest\VendorTicket\Model\VenueMapFactory $venueMapFactory
     * @param \Magenest\VendorTicket\Model\VenueMapDataFactory $venueMapDataFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\DataObject $objectFactory
     */
    public function __construct(
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreFactory $storeManager,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsTypeFactory,
        \Magenest\VendorTicket\Model\VenueMapFactory $venueMapFactory,
        \Magenest\VendorTicket\Model\VenueMapDataFactory $venueMapDataFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\VendorApi\Helper\RatingOptionHelper $ratingOptionHelper,
        \Magento\Store\Model\StoreManagerInterface $modelStoreManagerInterface,
        \Magento\Directory\Model\CountryFactory $countryFactory
    )
    {
        $this->_voteFactory = $voteFactory;
        $this->_vendorReviewHelper = $vendorReviewHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_eavConfig = $eavConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productAttributeFactory = $productAttributeFactory;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_reportFactory = $reportFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_swatchHelper = $swatchHelper;
        $this->ticket = $ticketFactory;
        $this->event = $eventFactory;
        $this->eventLocation = $eventLocationFactory;
        $this->session = $sessionFactory;
        $this->ticketsType = $ticketsTypeFactory;
        $this->_venueMap = $venueMapFactory;
        $this->_venueMapData = $venueMapDataFactory;
        $this->logger = $logger;
        $this->_objectFactory = $objectFactory;
        $this->ratingOptionHelper = $ratingOptionHelper;
        $this->modelStoreManagerInterface = $modelStoreManagerInterface;
        $this->_countryFactory = $countryFactory;
    }

    /**
     * @param $storeId
     * @param $productIds
     * @return array|null
     */
    public function getEventDetailByIds($storeId, $productIds)
    {
        $result = null;
        if ($storeId != null && sizeof($productIds) > 0) {
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            foreach ($productIds as $productId) {
                $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
                if (sizeof($parents) > 0) {
                    $currentProduct = $this->_productFactory->create()
                        ->load($parents[0]);
                } else {
                    $currentProduct = $this->_productFactory->create()
                        ->load($productId);
                }
                if ($currentProduct->getId()) {
                    $productImage = null;
                    if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                        if (substr($currentProduct->getImage(), 0, 1) == '/') {
                            $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                        } else {
                            $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                        }
                    }

                    $this->_reviewFactory
                        ->create()
                        ->getEntitySummary($currentProduct, $storeId);
                    $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
                    $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();

                    $result[] = [
                        'id' => $currentProduct->getEntityId(),
                        'name' => $currentProduct->getName(),
                        'image' => $productImage,
                        'sku' => $currentProduct->getSku(),
                        'rating' => $ratingSummary,
                        'reviews_count' => $ratingCount,
                        'price' => $currentProduct->getFinalPrice()
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * collection data of each event
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getEventDetail($storeId, $productId)
    {
        $result = null;
        $currentProduct = $this->_productFactory->create()->setStoreId($storeId)->load($productId);

        if ($currentProduct->getId() && $currentProduct->getTypeId() == 'ticket') {
            $price = $currentProduct->getPrice();
            $finalPrice = $currentProduct->getFinalPrice();
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory->create()->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();
            $vendorData = key($currentProduct->getMultiVendorData());
            $result[] = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
                'vendor_id' => $vendorData,
                'event' => $this->getEventData($currentProduct->getEntityId())
            ];
        }

        return $result;
    }

    /**
     * get event data
     * @param $id
     * @return mixed|null
     */
    public function getEventData($id)
    {
        $result = null;
        $modelEvent = $this->event->create()->loadByProductId($id)->getData();
        $modelLocation = $this->eventLocation->create()->load($id, 'product_id');

        $result = $modelEvent;
        if ($modelEvent['is_online'] && $modelEvent['is_online'] == 1) {
            $result['location'][] = $modelLocation->getData();
        }
        if ($modelEvent['ticket_type'] && $modelEvent['ticket_type'] != null) {
            $modelType = $this->ticketsType->create()->getCollection()
                ->addFieldToFilter('product_id', $id);
            $resultType = [];
            foreach ($modelType as $type) {
                $typeArr = $type->getData();
                $typeId = $type->getId();
                $resultSession = [];
                $modelSession = $this->session->create()->getCollection()
                    ->addFieldToFilter('tickets_id', $typeId);
                if (count($modelSession) > 0) {
                    foreach ($modelSession as $session) {
                        $sessionArr = $session->getData();
                        $sessionArr['sold_out'] = $this->checkSoldOut($session);
//                        $useMap = $modelEvent['is_reserved_seating'];
//                        if ($useMap == 1) {
//                            $modelMap = $this->_venueMapData->create()->getCollection()
//                                ->addFieldToFilter('product_id', $modelEvent['product_id'])
//                                ->addFieldToFilter('session_map_id', $session->getSessionMapId())
//                                ->getFirstItem();
//                            if ($modelMap->getId()) {
//                                $sessionArr['venue_map_data'] = $this->getVenueMapDataDetail($modelMap->getSessionMapId(), $session->getProductId());
//                            }
//                        }
                        array_push($resultSession, $sessionArr);
                    }
                }
                if (!empty($resultSession)) {
                    $typeArr['session'] = $resultSession;
                }
                array_push($resultType, $typeArr);
            }
            if (!empty($resultType)) {
                $result['type'] = $resultType;
            }
        }

        return $result;
    }

    /**
     * @param $type
     * @return int
     */
    protected function checkSoldOut($type)
    {
        $sellTo = strtotime($type->getSellTo());
        $qtyAvailable = $type->getQtyAvailable();
        $now = strtotime("now");
        if ($sellTo < $now || $qtyAvailable <= 0) {
            return 0;
        }
        return 1;
    }

    /**
     * @param $storeId
     * @param $size
     * @return array|null
     */
    public function getListEventDetail($storeId, $size)
    {
        $result = null;
        $currentProducts = $this->_productFactory->create()->setStoreId($storeId)->getCollection()
            ->addFieldToFilter('type_id', 'ticket')
            ->addFieldToFilter('status', 1)
            ->setPageSize(20)
            ->setCurPage($size);

        if (count($currentProducts) > 0) {
            foreach ($currentProducts as $currentProduct) {
                $price = $currentProduct->getPrice();
                $finalPrice = $currentProduct->getFinalPrice();
                $path = $this->_storeManager->create()
                    ->load($storeId)
                    ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                $productImage = null;
                if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                    if (substr($currentProduct->getImage(), 0, 1) == '/') {
                        $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                    } else {
                        $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                    }
                }

                $this->_reviewFactory->create()->getEntitySummary($currentProduct, $storeId);
                $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
                $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();
                $vendorData = key($currentProduct->getMultiVendorData());
                $result[] = [
                    'id' => $currentProduct->getEntityId(),
                    'name' => $currentProduct->getName(),
                    'image' => $productImage,
                    'sku' => $currentProduct->getSku(),
                    'rating' => $ratingSummary,
                    'reviews_count' => $ratingCount,
                    'price' => $price,
                    'final_price' => $finalPrice,
                    'vendor_id' => $vendorData
                ];
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @param $customer_id
     * @return array|null
     */
    public function getTicketDetail($id, $customer_id)
    {
        $result = null;
        $model = $this->ticket->create()->getCollection()
            ->addFieldToFilter('ticket_id', $id)
            ->addFieldToFilter('customer_id', $customer_id)
            ->getFirstItem();
        if ($model->getId()) {
            $result[] = $model->getData();
        }

        return $result;
    }

    /**
     * get list ticket by email customer
     * @param $email
     * @param $size
     * @return array|null
     */
    public function getTicketByEmailDetail($email, $size)
    {
        $result = null;
        $model = $this->ticket->create()->getCollection()
            ->addFieldToFilter('customer_email', $email)
            ->setPageSize(20)
            ->setCurPage($size);
        if (count($model) > 0) {
            foreach ($model as $ticket) {
                $result[] = $ticket->getData();
            };
        }

        return $result;
    }

    public function getTicketByCustomerId($customerId, $cur_page, $size, $status)
    {
        $result = null;
        if ($status == 0) {
            $model = $this->ticket->create()->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->setPageSize($size)
                ->setCurPage($cur_page);
        } else {
            $model = $this->ticket->create()->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('status', $status)
                ->setPageSize($size)
                ->setCurPage($cur_page);
        }


        if (count($model) > 0) {
            foreach ($model as $ticket) {
                $result[] = $this->getTicketInfo($ticket);
            };
        }

        return $result;
    }

    /**
     * @param \Magenest\Ticket\Model\Ticket $ticketObj
     * @return mixed
     */
    public function getTicketInfo($ticketObj)
    {

        $data = $ticketObj->getData();
        $infoTicketData = unserialize($ticketObj->getInformation());
        $event = $ticketObj->getEvent();
        if ($event->getShowUp() == 1) {
            $data['term'][] = '* Must Show up before session start ' . $event->getShowUpTime() . ' minutes before start of session';
        }
        if ($event->getCanPurchase() == 1) {
            $data['term'][] = '* Additional Tickets can be purchased at venue (full price and upon availability)';
        }
        if ($event->getMustPrint() == 1) {
            $data['term'][] = '* Tickets must be printed';
        }
        if ($event->getAge()) {
            $data['term'][] = '* Age Suitability: ' . $event->getAge();
        }
        $data['ticket_infors'] = $this->ticketsType->create()->load($infoTicketData['tickets_id'])->getData();
        $product_id = $data['product_id'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
        $storeManager = $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        if ($product->getId()) {
            $product_name = $product->getName();
            $product_image = $product->getData('image');
            if ($product_image) {
                $mediaUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $data['product_image'] = $mediaUrl . 'catalog/product' . $product_image;
            } else {
                $data['product_image'] = '';
            }
            $data['vendor_id'] = $product->getUdropshipVendor();
            $vendor = $this->_vendorFactory->create()->load($data['vendor_id']);
            $data['vendor_name'] = $vendor->getVendorName();
            $data['product_name'] = $product_name;
        }
        $ticketCollections = $this->ticket->create()->load($data['ticket_id']);
        $ticketInfo = unserialize($ticketCollections->getInformation());
        $sessionCollection = $this->session->create()->load($ticketInfo['session_id']);
        $data['ticket_infors'] = array_merge($data['ticket_infors'], $sessionCollection->getData());
        if ($ticketCollections) {
            $location = $this->eventLocation->create()->getCollection()->addFieldToFilter('event_id', $ticketCollections->getEventId());
            if ($location->count() > 0) {
                $location = $location->getFirstItem();
                $data['location'] = $location->getVenueTitle() . ", " . $location->getAddress();
                $data['longitude'] = $location->getLongitude();
                $data['latitude'] = $location->getLatitude();
            } else {
                $data['location'] = "online";
            }

            return $data;
        }

        return null;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);
        return date('d/m/Y H:i:s', $strTime);
    }

    /**
     * get list ticket by product id
     * @param $id
     * @param $size
     * @return array|null
     */
    public function getTicketByProductDetail($id, $size)
    {
        $resutl = null;
        $model = $this->ticket->create()->getCollection()
            ->addFieldToFilter('product_id', $id)
            ->setPageSize(20)
            ->setCurPage($size);
        if (count($model) > 0) {
            foreach ($model as $ticket) {
                $resutl[] = $ticket->getData();
            };
        }

        return $resutl;
    }

    public function getTicketDetailsById($customerId, $ticketId)
    {
        $result = [];
        $ticketDetails = $this->ticket->create()->getCollection()->addFieldToFilter('redemption_code', $ticketId)->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        $product = $this->_productFactory->create()->load($ticketDetails->getProductId());
        $ticketData = $ticketDetails->getData();
        $ticketData['product_name'] = $product->getName();
        $ticketData['product_url'] = $product->getProductUrl();
        $result['ticket_data'] = $ticketData;
        $ticketInformation = unserialize($ticketDetails->getInformation());
        $result['session_data'] = $this->getSessionDataByTicketId($ticketInformation);
        $result['ticket_location'] = $this->getTicketLocationDetail($ticketDetails->getProductId());
        $result['ticket_term'] = $this->getTicketTerms($ticketDetails->getProductId());
        $vendorId = $ticketData['vendor_id'];
        $productId = $product->getEntityId();
        $productIds = $this->checkCustomerReview($customerId);
        $rating_option = null;
        if(!in_array($productId,$productIds)){
            if($this->ratingOptionHelper->checkVendorAllowRating($vendorId)){
                $rating_option = $this->ratingOptionHelper->getRatingOption($product);
            }
        }
        $result['rating_option'] = $rating_option;
        return $result;
    }
    public function checkCustomerReview($customerId){
        $reviewCollection = $this->_reviewFactory->create()->getCollection();
        $reviewCollection->addStoreFilter($this->modelStoreManagerInterface->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['eq' => 7])
            ->addCustomerFilter($customerId)
            ->setDateOrder()
            ->addRateVotes();
        $dataTmp = [];
        foreach ($reviewCollection->getItems() as $items) {
            $dataTmp[] = $items->getEntityPkValue();
        }
        return $dataTmp;
    }
    public function getVendorCheckTicketByCode($vendor_id, $ticketCode)
    {
        $result = [];
        $ticketDetails = $this->ticket->create()->getCollection()->addFieldToFilter('redemption_code', $ticketCode)->addFieldToFilter('vendor_id', $vendor_id)->getFirstItem();
        $product = $this->_productFactory->create()->load($ticketDetails->getProductId());
        $ticketData = $ticketDetails->getData();
        $ticketData['product_name'] = $product->getName();
        $ticketData['product_url'] = $product->getProductUrl();
        $ticketInformation = unserialize($ticketDetails->getInformation());
        $type = $this->ticketsType->create()->load($ticketInformation['tickets_id']);
        $ticketData['ticket_type'] = $type->getTitle();
        $ticketData['ticket_description'] = $type->getDescription();
        $session = $this->session->create()->load($ticketInformation['session_id']);
        $ticketData['ticket_sessionfrom'] = $session->getSessionFrom();
        $ticketData['ticket_sessionto'] = $session->getSessionTo();
        $result['ticket_location'] = $this->getTicketLocationDetail($ticketDetails->getProductId());
        $result['ticket_term'] = $this->getTicketTerms($ticketDetails->getProductId());
        $result['ticket_data'] = $ticketData;
        $result['status'] = $ticketDetails->getTicketId() ? 1 : 0;
        return $result;
    }

    /**
     * @param $vendor
     * @return int
     */
    protected function getVendorSpecialPrice($vendor)
    {
        try {
            if ($vendor['special_from_date'] != null && $vendor['special_to_date'] != null) {
                $today = date("Ymd");
                $from = str_replace('-', '', $vendor['special_from_date']);
                $to = str_replace('-', '', $vendor['special_to_date']);
                if (strcmp($today, $from) >= 0 && strcmp($today, $to) <= 0) {
                    return $vendor['special_price'];
                }
            } else {
                return $vendor['special_price'];
            }
        } catch (Exception $e) {
            return 0;
        }
        return 0;
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getVenueMapDetail($id)
    {
        $modelMap = $this->_venueMap->create()->load($id);

        if ($modelMap->getId()) {
            $data = $modelMap->getData();
            return $this->_objectFactory->addData([
                'id' => $data['venue_map_id'],
                'venue_map_name' => $data['venue_map_name'],
                'venue_map_data' => [json_decode(json_decode($data['venue_map_data']), true)],
                'created' => $data['created'],
                'vendor_id' => $data['vendor_id'],
            ]);
        }

        return null;
    }

    /**
     * @param $id
     * @param $product_id
     * @return mixed|null
     */
    public function getVenueMapDataDetail($id, $product_id)
    {
        $modelMapData = $this->_venueMapData->create()->getCollection()
            ->addFieldToFilter('session_map_id', $id)
            ->addFieldToFilter('product_id', $product_id)
            ->getFirstItem();
        if ($modelMapData->getId()) {
            $data = $modelMapData->getData();
            $ticketData = $this->prepareTicketData($data['session_ticket_data']);
            return $this->_objectFactory->addData([
                'id' => $data['map_data_id'],
                'product_id' => $data['product_id'],
                'venue_map_id' => $data['venue_map_id'],
                'session_map_id' => $data['session_map_id'],
                'session_map_name' => $data['session_map_name'],
                'session_map_data' => [json_decode(json_decode($data['session_map_data']), true)],
                'session_ticket_data' => $ticketData,
                'vendor_id' => $data['vendor_id'],
            ]);
        }

        return null;
    }

    private function prepareTicketData($data)
    {
        $phaseOne = json_decode($data, true);
        $phaseTwo = [];
        foreach ($phaseOne as $item) {
            $chairsData = $item['chairs'];
            unset($item['chairs']);
            $newData = $item;
            $chairs = [];
            foreach ($chairsData as $chairsDatum) {
                array_push($chairs, json_decode($chairsDatum, true));
            }
            $newData['chairs'] = $chairs;
            array_push($phaseTwo, $newData);
        }

        return $phaseTwo;
    }

    protected function getTicketLocationDetail($getProductId){
        $isOnline = $this->event->create()->getCollection()->addFieldToFilter('product_id', $getProductId)->getFirstItem()->getIsOnline();
        if ($isOnline !== '1') {
            return null;
        } else {
            $location = $this->eventLocation->create()->getCollection()->addFieldToFilter('product_id', $getProductId)->getFirstItem()->getData();
            return [$location];
        }
    }
    public function getTicketLocation($productId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->create('Magento\Config\Model\ResourceModel\Config\Data\Collection');
        $connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $sql = "SELECT * FROM `magenest_ticket_event` WHERE `product_id` = ".$productId.";";
        $event = $connection->fetchRow($sql);
        $is_online = $event['is_online'];
//        $isOnline = $this->event->create()->getCollection()->addFieldToFilter('product_id', $getProductId)->getFirstItem()->getIsOnline();
        if ($is_online !== '1') {
            return ['location_detail' => "Online Event",'map_info' => ""];
        } else {
            $sql = "SELECT * FROM `magenest_ticket_location` WHERE `product_id` = ".$productId.";";
            $location = $connection->fetchRow($sql);
            $result = "";
            if ($location['city']) {
                $result .= $location['city'];
            } else {
                $result .= $location['address'];
            }
            $lat_long = $location['latitude'].','.$location['longitude'];
            $result .= ", ";
            $country = $this->_countryFactory->create()->loadByCode($location['country']);
            $result .= $country->getName();
            if ($country->getName()) {
                return ['location_detail' => $result,'map_info' => $lat_long];
            } else {
                return "";
            }
        }
    }
    protected function getTicketTerms($getProductId)
    {
        $event = $this->event->create()->getCollection()->addFieldToFilter('product_id', $getProductId)->getFirstItem();
        $terms = [];
        if ($event->getMustPrint() === '1') {
            array_push($terms, 'Tickets must be printed.');
        }
        if ($event->getCanPurchase() === '1') {
            array_push($terms, 'Additional Tickets can be purchased at venue (full price and upon availability).');
        }
        if ($event->getShowUp() === '1') {
            array_push($terms, 'Must Show up before session start ' . $event->getShowUpTime() . ' minute(s).');
        }

        return $terms;
    }

    private function getSessionDataByTicketId($ticketInformation)
    {
        $result = [];
        $type = $this->ticketsType->create()->load($ticketInformation['tickets_id']);
        $result['ticket_type'] = $type->getTitle();
        $result['ticket_description'] = $type->getDescription();

        $session = $this->session->create()->load($ticketInformation['session_id']);

        $result['ticket_sessionfrom'] = $session->getSessionFrom();
        $result['ticket_sessionto'] = $session->getSessionTo();

        return $result;
    }

    public function getPriceTicket($productId,$type){
        $sessionTickets = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $result = [];
        foreach ($sessionTickets as $sessionTicket){
            $result[] = $sessionTicket->getOriginalValue();
//            if($sessionTicket->getSalePrice()){
//                $result[] = $sessionTicket->getSalePrice();
//                $result[] = $sessionTicket->getOriginalValue();
//            }else{
//                $result[] = $sessionTicket->getOriginalValue();
//            }
        }
        if(count($result) === 0){
            return number_format(0, 2);
        }
        $minPrice = min($result);
        $maxPrice = max($result);
        if ($minPrice == $maxPrice) {
            return number_format($minPrice, 2);
        } else {
            return [number_format($minPrice, 2), number_format($maxPrice, 2)];
        }
    }
    public function getEvent($productId){
        $model = $this->event->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem();
        return $model;
    }
    public function isFreeTicketType($productId){
        if ($this->getEvent($productId)->getTicketType() === 'free') {
            return true;
        } else {
            return false;
        }
    }
    public function getTicketType($productId){
        $ticketType = $this->getEvent($productId)->getTicketType();
        return $ticketType;
    }
    public function isHasSalePricce($productId){
        $ticketType = $this->getEvent($productId)->getTicketType();
        if ($ticketType === 'paid') {
            $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
            $result = [];
            $hasSalePrice = false;
            foreach ($sessionTicket as $session) {
                if ($session->getSalePrice()) {
                    $hasSalePrice = true;
                    array_push($result, $session->getSalePrice());
                }
            }
            if ($hasSalePrice) {
                if (is_array($result)) {
                    if(min($result) == max($result)){
                        return min($result);
                    }else{
                        return [min($result), max($result)];
                    }
                } else {
                    return 0;
                }
            } else {
                return false;
            }
        }
    }
    public function getAllBoughtTicket($productId){
        $ticketIds = $this->ticketsType->create()->getCollection()
            ->addFieldToFilter('product_id',$productId)
            ->getAllIds();
        $sessions = $this->session->create()->getCollection()->addFieldToFilter('tickets_id',['in',$ticketIds]);
        $bought = 0;
        /** @var \Magenest\Ticket\Model\Session $session */
        foreach ($sessions as $session){
            if($session->getQtyPurchased()) $bought += $session->getQtyPurchased();
        }
        return $bought;
    }
}
