<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 11:22
 */

namespace Magenest\VendorApi\Helper;

use Braintree\Exception;
use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Eav\Model\AttributeFactory;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute;
use Unirgy\DropshipVendorRatings\Helper\Data as RatingData;

/**
 * Class ProductHelper
 * @package Magenest\VendorApi\Helper
 */
class ProductHelper
{
    /**
     * @var $_reviewSummary SummaryFactory
     */
    protected $_reviewSummary;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    protected $_reportFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Unirgy\DropshipVendorRatings\Helper\Data
     */
    protected $_vendorReviewHelper;

    /**
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var \Magento\Review\Model\Rating\Option\VoteFactory
     */
    protected $_voteFactory;

    /**
     * @var EventTicketHelper
     */
    protected $_eventTicket;

    /**
     * @var HotelHelper
     */
    protected $_hotel;

    /**
     * @var GrouponHelper
     */
    protected $_groupon;

    protected $_objectFactory;
    protected $_wishlistRepository;
    protected $_wishlistCollectionFactory;

    /** @var $_ratingHelper RatingData */
    protected $_ratingHelper;

    /**
     * @var \Magenest\Groupon\Helper\Data|GrouponHelper
     */
    protected $_grouponDataHelper;
    protected $shipconfig;
    protected $scopeConfig;
    protected $_storeManagerInterface;
    protected $_sellYourRatingHelper;
    protected $_ratingInfoFactory;

    /**
     * ProductHelper constructor.
     * @param \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory
     * @param \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreFactory $storeManager
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param ImageFactory $imageFactory
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param EventTicketHelper $eventTicketHelper
     * @param HotelHelper $hotelHelper
     * @param GrouponHelper $grouponHelper
     */
    public function __construct(
        \Magenest\Groupon\Helper\Data $grouponDataHelper,
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreFactory $storeManager,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Swatches\Helper\Data $swatchHelper,
        EventTicketHelper $eventTicketHelper,
        HotelHelper $hotelHelper,
        GrouponHelper $grouponHelper,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
        \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $wishlistCollectionFactory,
        RatingData $ratingHelper,
        \Magento\Review\Model\Review\SummaryFactory $summaryFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config $shipconfig,
        \Magento\Store\Model\StoreManagerInterface $modelStoreManagerInterface,
        \Magenest\SellYours\Helper\RatingHelper $sellYourRatingHelper,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory
    ) {
        $this->_grouponDataHelper = $grouponDataHelper;
        $this->_objectFactory = $objectFactory;
        $this->_voteFactory = $voteFactory;
        $this->_vendorReviewHelper = $vendorReviewHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_eavConfig = $eavConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productAttributeFactory = $productAttributeFactory;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_reportFactory = $reportFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_swatchHelper = $swatchHelper;
        $this->_eventTicket = $eventTicketHelper;
        $this->_hotel = $hotelHelper;
        $this->_groupon = $grouponHelper;
        $this->_wishlistRepository = $wishlistFactory;
        $this->_wishlistCollectionFactory = $wishlistCollectionFactory;
        $this->_ratingHelper = $ratingHelper;
        $this->_reviewSummary = $summaryFactory;
        $this->shipconfig = $shipconfig;
        $this->scopeConfig = $scopeConfig;
        $this->_storeManagerInterface = $modelStoreManagerInterface;
        $this->_sellYourRatingHelper = $sellYourRatingHelper;
        $this->_ratingInfoFactory = $ratingInfoFactory;
    }

    /**
     * @param $storeId
     * @return array|null
     */
    public function getHomeMostViewedProducts($storeId)
    {
        $result = null;
        $done = false;
        $count = 0;
        $mostViewedProducts = null;
        while (!$done) {
            switch ($count) {
                case 0:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('month')
                        ->setDateRange(
                            date('Y-m-d', strtotime("first day of last month")),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 1:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('year')
                        ->setDateRange(
                            date('Y-m-d', strtotime('-4 month')),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 2:
                    $mostViewedProducts = $this->_reportFactory->create('\Magento\Reports\Model\ResourceModel\Report\Product\Viewed\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPageSize(20)
                        ->load();
                    break;
                default:
                    $done = true;
                    break;
            }
            $count++;
            if ($mostViewedProducts != null && $mostViewedProducts->getSize() > 0) {
                $result = [];
                foreach ($mostViewedProducts as $mostViewedProduct) {
                    $resultTemp = $this->getProductFewDetail($storeId, $mostViewedProduct->getData('product_id'));
                    if ($resultTemp) {
                        $resultTemp['views_num'] = $mostViewedProduct->getData('views_num');
                        $resultTemp['period'] = $mostViewedProduct->getData('period');
                        $result[] = $resultTemp;
                    }
                }
                if (sizeof($result) > 10) {
                    $done = true;
                }
            }
        }
        return $result;
    }

    /**
     * @param $storeId
     * @return array|null
     */
    public function getHomeBestSellingProducts($storeId)
    {
        $result = null;
        $done = false;
        $count = 0;
        $bestsellingProducts = null;

        while (!$done) {
            switch ($count) {
                case 0:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('month')
                        ->setDateRange(
                            date('Y-m-d', strtotime("first day of last month")),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 1:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPeriod('year')
                        ->setDateRange(
                            date('Y-m-d', strtotime('-4 month')),
                            date('Y-m-d')
                        )
                        ->setPageSize(20)
                        ->load();
                    break;
                case 2:
                    $bestsellingProducts = $this->_reportFactory->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection')
                        ->addStoreRestrictions($storeId)
                        ->setPageSize(20)
                        ->load();
                    break;
                default:
                    $done = true;
                    break;
            }
            $count++;
            if ($bestsellingProducts->getSize() > 0) {
                $result = [];
                foreach ($bestsellingProducts as $bestsellingProduct) {
                    $resultTemp = $this->getProductFewDetail($storeId, $bestsellingProduct->getData('product_id'));
                    if ($resultTemp) {
                        $resultTemp['qty_ordered'] = $bestsellingProduct->getData('qty_ordered');
                        $resultTemp['period'] = $bestsellingProduct->getData('period');
                        $result[] = $resultTemp;
                    }
                }
                if (sizeof($result) > 10) {
                    $done = true;
                }
            }
        }
        return $result;
    }

    /**
     * @param $storeId
     * @param $productIds
     * @return array|null
     */
    public function getProductFewDetailByIds($storeId, $productIds)
    {
        $result = null;
        if ($storeId != null && sizeof($productIds) > 0) {
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            foreach ($productIds as $productId) {
                $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
                if (sizeof($parents) > 0) {
                    $currentProduct = $this->_productFactory->create()
                        ->load($parents[0]);
                } else {
                    $currentProduct = $this->_productFactory->create()
                        ->load($productId);
                }
                if ($currentProduct->getId()) {
//            $productImage = $this->_imageFactory->create()
//                ->init($currentProduct, 'category_page_grid')
//                ->constrainOnly(FALSE)
//                ->keepAspectRatio(TRUE)
//                ->keepFrame(FALSE)
//                ->resize(400)
//                ->getUrl();
                    $productImage = null;
                    if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                        if (substr($currentProduct->getImage(), 0, 1) == '/') {
                            $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                        } else {
                            $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                        }
                    }

                    $this->_reviewFactory
                        ->create()
                        ->getEntitySummary($currentProduct, $storeId);
                    $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
                    $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();

                    $result[] = [
                        'id' => $currentProduct->getEntityId(),
                        'name' => $currentProduct->getName(),
                        'image' => $productImage,
                        'sku' => $currentProduct->getSku(),
                        'rating' => $ratingSummary,
                        'reviews_count' => $ratingCount,
                        'price' => $currentProduct->getFinalPrice()
                    ];
                }
            }
        }
        return $result;
    }

    public function getProductFewDetail($storeId, $productId)
    {
        $result = null;
        $parents = $this->_catalogProductTypeConfigurable->getParentIdsByChild($productId);
        if (sizeof($parents) > 0) {
            $currentProduct = $this->_productFactory->create()->setStoreId($storeId)
                ->load($parents[0]);
        } else {
            $currentProduct = $this->_productFactory->create()->setStoreId($storeId)
                ->load($productId);
        }
        if ($currentProduct->getId()) {
            $price = 0;
            $finalPrice = 0;
            if ($currentProduct->getTypeId() == 'grouped') {
                $childrenIds = $currentProduct->getTypeInstance()->getChildrenIds($currentProduct->getId());
                foreach ($childrenIds as $key => $value) {
                    foreach ($value as $valueProductId) {
                        $firstChild = $this->_productFactory->create()->setStoreId($storeId)
                            ->load($valueProductId);
                        $price = $firstChild->getFinalPrice();
                        $finalPrice = $firstChild->getFinalPrice();
                        break;
                    }
                    break;
                }

            } else {
                $price = $currentProduct->getPrice();
                $finalPrice = $currentProduct->getFinalPrice();
            }

//            $productImage = $this->_imageFactory->create()
//                ->init($currentProduct, 'category_page_grid')
//                ->constrainOnly(FALSE)
//                ->keepAspectRatio(TRUE)
//                ->keepFrame(FALSE)
//                ->resize(400)
//                ->getUrl();
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory
                ->create()
                ->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();

            $result = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
            ];
        }
        return $result;
    }

    /**
     * @param $store
     * @param $id
     * @return $this
     */
    public function getProductFullDetail($store, $id)
    {
        $finalProductData = $this->_productFactory->create()->load($id);
        if ($finalProductData->getId()) {
            $reviewCollection = $this->_sellYourRatingHelper->getReviewCollection($finalProductData->getId(),$isApi = false, $vendorId = null);
            $reviewIds = [];
            foreach ($reviewCollection as $review){
                $reviewIds[] = $review->getReviewId();
            }
            $this->_reviewFactory->create()->getEntitySummary($finalProductData, $store);
            $rating = \Magento\Framework\App\ObjectManager::getInstance()->get('Magenest\Groupon\Block\Product\View\Rating');
            $avgRate = $rating->getAvgRate($reviewIds);
            $ratingSummary = $avgRate*20;
            $ratingCount = $reviewCollection->count();
            $finalProductData->addData([
                'rating_summary' => number_format($ratingSummary,2),
                'rating_summary_percent' => number_format(((float)$avgRate / 5) * 100, 2),
                'reviews_count' => $ratingCount,
                'quantity_and_stock_status' => $finalProductData->getData('quantity_and_stock_status')['is_in_stock'],
                'qty' => $finalProductData->getData('quantity_and_stock_status')['qty'],
                'best_vendor' => $finalProductData->getData('udmulti_best_vendor') ? $finalProductData->getData('udmulti_best_vendor') : null
            ]);
            $reviewCollection = $this->getReviewCollection($finalProductData->getId());
            $topReviewArray = $this->getTopReviewByProductId($finalProductData->getId(),$reviewCollection);
            $reviewItems = $reviewCollection->getItems();
            $review = [];
            foreach ($reviewItems as $_review){
                $reviewItem = $_votes = $_naVotes = [];
                foreach ($_review->getRatingVotes() as $_vote) {
                    if ($_vote->getIsAggregate()) {
                        $_votes[] = $_vote;
                    } else {
                        $_naVotes[] = $_vote;
                    }
                }
                if (count($_votes)){
                    $percent = 0;
                    $ratingCode = '';
                    foreach ($_votes as $_vote){
                        $ratingCode = $_vote->getRatingCode();
                        $percent = $percent + $_vote->getPercent();
                    }
                    $percent = $percent / count($_votes);
                    $reviewItem['reviews'][] = [
                        'is_aggregatable' => true,
                        'rating_code' => $ratingCode,
                        'rating_value' => $percent,
                    ];
                }
                if(count($_naVotes)){
                    foreach ($_naVotes as $_vote){
                        $ratingCode = $_vote->getRatingCode();
                        $ratingValue = $_vote->getValue() ? 'Yes' : 'No';
                        $reviewItem['reviews'][] = [
                            'is_aggregatable' => false,
                            'rating_code' => $ratingCode,
                            'rating_value' => $ratingValue
                        ];
                    }
                }
                $customerId = $_review->getCustomerId();
                $nickName = $_review->getNickname();
                $reviewDetail = $_review->getDetail();
                $reviewQty = $rating->getTotalReview($customerId);
                $reviewEntityPkValue = $_review->getRelEntityPkValue();
                $redeemAt = $rating->getRedeemAt($reviewEntityPkValue);
                $reviewItem['nick_name'] = $nickName;
                $reviewItem['review_detail'] = $reviewDetail;
                $reviewItem['review_qty'] = $reviewQty;
                if($redeemAt) $reviewItem['review_redeem'] = $redeemAt;
                $review[] = $reviewItem;
            }

            $vendors = $finalProductData->getMultiVendorData();
            foreach ($vendors as &$vendor) {
                $currentVendor = $this->_vendorFactory->create()->load($vendor['vendor_id']);
                //getVendorInformation
                $info_rating_vendor = $this->getVendorInformation($currentVendor);
                //getVendorReviewsCollection
                $reviewVendors = $this->getVendorReviewsCollection($currentVendor->getId());
                $reviewId = [];
                foreach ($reviewVendors as $reviewVendor){
                    $reviewId[] = $reviewVendor->getReviewId();
                }
                $avgRate = $rating->getAvgRate($reviewId);
                $ratingVendorSummary = $avgRate*20;
                $vendor_rating_count = $reviewVendors->count();
                $rating_general = $info_rating_vendor['rating_general'];
                $vendor_rating_percent =  number_format(((float)$avgRate / 5) * 100, 2);
                $vendor['vendor_name'] = $currentVendor->getVendorName();
                $vendor['special_price'] = $this->getVendorSpecialPrice($vendor);
                $vendor['vendor_rating'] = number_format( $ratingVendorSummary, 2);
                $vendor['vendor_rating_count'] = $vendor_rating_count;
                $vendor['vendor_rating_percent'] = $vendor_rating_percent;
                $vendor['vendor_description'] = $currentVendor->getBusinessDes();
                $carrierCode = $currentVendor->getCarrierCode();
                if($carrierCode != ''){
                    $vendor['carrier_code'] = $this->scopeConfig->getValue('carriers/'.$carrierCode.'/title');
                }
            }
            $finalProductData->addData([
                'vendor_list' => array_values($vendors)
            ]);

            $finalProductData->addData([
                'top_review' => $topReviewArray,
                'review_items' => array_values($review)
            ]);
            $finalProductData->addData([
                'media_url' => $this->_storeManager->create()
                    ->load($store)
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA)
            ]);
            $all_custom_attributes = $this->getCustomAttributesValue();
            $custom_attributes = $finalProductData->getCustomAttributes();
            $custom_attributes_result = [];
            foreach ($custom_attributes as $custom_attributes_item) {
                $fixed = false;
                foreach ($all_custom_attributes as $all_custom_attributes_item) {
                    if ($all_custom_attributes_item['code'] == $custom_attributes_item->getAttributeCode() && $fixed == false) {
                        if (array_key_exists('value', $all_custom_attributes_item)) {
                            foreach ($all_custom_attributes_item['value'] as $all_custom_attributes_item_value) {
                                if (intval($all_custom_attributes_item_value['value']) == intval($custom_attributes_item->getValue())
                                    && $fixed == false) {
                                    $custom_attributes_result[] = [
                                        'attribute_code' => $custom_attributes_item->getAttributeCode(),
                                        'value' => $all_custom_attributes_item_value['label'],
                                        'visible' => $all_custom_attributes_item['visible'],
                                        'label' => $all_custom_attributes_item['label']
                                    ];
                                    $fixed = true;
                                    break;
                                }
                            }
                        } else {
                            $custom_attributes_result[] = [
                                'attribute_code' => $custom_attributes_item->getAttributeCode(),
                                'value' => $custom_attributes_item->getValue(),
                                'visible' => $all_custom_attributes_item['visible'],
                                'label' => $all_custom_attributes_item['label']
                            ];
                        }
                    }
                }
            }

            $finalProductData->addData([
                'custom_attribute_value' => $custom_attributes_result
            ]);
            $productLinks = $finalProductData->getProductLinks();
            if (sizeof($productLinks) > 0) {
                $productLinksResult = [];
                foreach ($productLinks as $productLink) {
                    $productLinkData = [];
                    $productLinkData['link_type'] = $productLink->getLinkType();
                    $productLinkData['position'] = $productLink->getPosition();
                    $currentProduct = $this->_productRepository->get($productLink->getLinkedProductSku());
                    $currentProduct = $this->_productFactory->create()->load($currentProduct->getId());
                    $productLinkData['image'] = $currentProduct->getImage();
                    $productLinkData['id'] = $currentProduct->getId();
                    $productLinkData['price'] = $currentProduct->getPrice();
                    $productLinkData['final_price'] = $currentProduct->getFinalPrice();
                    if ($currentProduct->getTypeId() == "configurable") {
                        //vendors
                        $childs = $currentProduct->getTypeInstance()->getUsedProducts($currentProduct);
                        foreach ($childs as $child) {
                            $vendors = $child->getMultiVendorData();
                            if ($vendors != null && sizeof($vendors) > 0) {
                                foreach ($vendors as $item) {
                                    if ($item['vendor_price'] != null && $item['vendor_price'] != "") {
                                        $special = $this->getVendorSpecialPrice($item);
                                        if ($special > 0) {
                                            $productLinkData['price'] = $item['vendor_price'];
                                            $productLinkData['final_price'] = $special;
                                        } else {
                                            $productLinkData['price'] = 0;
                                            $productLinkData['final_price'] = $item['vendor_price'];
                                        }
                                        $productLinkData['freeshipping'] = $item['freeshipping'];
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    } else {
                        $vendors = $currentProduct->getMultiVendorData();
                        if ($vendors != null && sizeof($vendors) > 0) {
                            foreach ($vendors as $item) {
                                if ($item['vendor_price'] != null && $item['vendor_price'] != "") {
                                    $special = $this->getVendorSpecialPrice($item);
                                    if ($special > 0) {
                                        $productLinkData['price'] = $item['vendor_price'];
                                        $productLinkData['final_price'] = $special;
                                    } else {
                                        $productLinkData['price'] = 0;
                                        $productLinkData['final_price'] = $item['vendor_price'];
                                    }
                                    $productLinkData['freeshipping'] = $item['freeshipping'];
                                }
                                break;
                            }
                        }
                    }

                    $productLinkData['sku'] = $currentProduct->getSku();
                    $productLinkData['name'] = $currentProduct->getName();
                    $this->_reviewFactory
                        ->create()
                        ->getEntitySummary($currentProduct, $store);
                    $productLinkRatingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
                    $productLinkRatingCount = $currentProduct->getRatingSummary()->getReviewsCount();
                    $productLinkData['rating'] = $productLinkRatingSummary;
                    $productLinkData['reviews_count'] = $productLinkRatingCount;
                    $productLinksResult[] = $productLinkData;
                }
                $finalProductData->setProductLinks([]);
                $finalProductData->addData([
                    'product_link' => $productLinksResult
                ]);
            }
            $finalProductData->addData([
                'final_price' => $finalProductData->getFinalPrice()
            ]);
            if ($finalProductData->getTypeId() == "configurable") {
                $configurable_product_links = $finalProductData->getExtensionAttributes()->getConfigurableProductLinks();
                if ($configurable_product_links != null && sizeof($configurable_product_links) > 0) {
                    $configurable_product_links_info = [];
                    foreach ($configurable_product_links as $configurable_product_links_item) {
                        $configurable_product_options_product = $this->_productFactory->create()
                            ->load($configurable_product_links_item);
                        $imageList = [];
                        $media_gallery_images = $configurable_product_options_product->getMediaGalleryImages();
                        foreach ($media_gallery_images as $media_gallery_image) {
                            $imageList[] = $media_gallery_image->getFile();
                        }
                        $vendors = $configurable_product_options_product->getMultiVendorData();
                        foreach ($vendors as &$vendor) {
                            $info_rating_vendor = $this->getRatingByVendorId($vendor['vendor_id']);
                            $currentVendor = $this->_vendorFactory->create()->load($vendor['vendor_id']);
                            $vendor['vendor_name'] = $currentVendor->getVendorName();
                            $vendor['special_price'] = $this->getVendorSpecialPrice($vendor);
                            $vendor['vendor_rating'] = $info_rating_vendor['average_rating_summary'];
                            $vendor['vendor_rating_count'] = $info_rating_vendor['total_review'];
                            $vendor['vendor_description'] = $currentVendor->getBusinessDes();
//                            $vendorReviews = $this->_vendorReviewHelper->getVendorReviewsCollection($currentVendor);
                            //todo
                        }
                        $vendorResult = [];
                        foreach ($vendors as $vendor) {
                            $vendorResult[] = $vendor;
                        }
                        $new_configurable_product_links_info = [
                            'id' => $configurable_product_options_product->getEntityId(),
                            'name' => $configurable_product_options_product->getName(),
                            'quantity_and_stock_status' => $configurable_product_options_product->getData('quantity_and_stock_status')['is_in_stock'],
                            'status' => $configurable_product_options_product->getStatus(),
                            'image' => $imageList,
                            'qty' => $configurable_product_options_product->getData('quantity_and_stock_status')['qty'],
                            'price' => $configurable_product_options_product->getPrice(),
                            'final_price' => $configurable_product_options_product->getFinalPrice(),
                            'vendor' => $vendorResult
                        ];

                        $configurable_product_links_info[] = $new_configurable_product_links_info;
                    }
                    if (sizeof($configurable_product_links_info) > 0) {
                        $finalProductData->addData([
                            'configurable_product_links_info' => $configurable_product_links_info
                        ]);
                    }
                    $configurable_product_options = $finalProductData->getExtensionAttributes()->getConfigurableProductOptions();
                    $configurable_product_options_value = [];
                    foreach ($configurable_product_options as $configurable_product_options_item) {
                        foreach ($configurable_product_options_item->getOptions() as $option) {
                            $newConfigurable_product_options_value = [
                                'value_index' => $option['value_index'],
                                'label' => $option['store_label']
                            ];
                            $realValue = $this->_swatchHelper->getSwatchesByOptionsId([$option['value_index']]);
                            if ($realValue != null && array_key_exists($option['value_index'], $realValue)) {
                                $newConfigurable_product_options_value['value'] = $realValue[$option['value_index']]['value'];
                            } else {
                                $newConfigurable_product_options_value['value'] = $option['store_label'];
                            }
                            $configurable_product_options_value[] = $newConfigurable_product_options_value;
                        }
                    }
                    $finalProductData->addData([
                        'configurable_product_option_value' => $configurable_product_options_value
                    ]);
                }
                $configurable_product_option_type = [];
                foreach ($finalProductData->getExtensionAttributes()
                             ->getConfigurableProductOptions() as $extensionAttribute) {
                    $additionalData = $extensionAttribute->getProductAttribute()->getAdditionalData();
                    $additionalData = json_decode($additionalData, true);
                    $swatch_input_type = 'text';
                    if ($additionalData != null && array_key_exists('swatch_input_type', $additionalData)) {
                        $swatch_input_type = $additionalData['swatch_input_type'];
                    }
                    $configurable_product_option_type[] = [
                        'id' => $extensionAttribute->getAttributeId(),
                        'swatch_input_type' => $swatch_input_type
                    ];
                }
                $finalProductData->addData([
                    'configurable_product_option_type' => $configurable_product_option_type
                ]);
            }
            if ($finalProductData->getTypeId() == "simple") {
                $vendors = $finalProductData->getMultiVendorData();
                foreach ($vendors as &$vendor) {
                    $currentVendor = $this->_vendorFactory->create()->load($vendor['vendor_id']);
                    $vendor['vendor_name'] = $currentVendor->getVendorName();
                    $vendor['special_price'] = $this->getVendorSpecialPrice($vendor);
//                            $vendorReviews = $this->_vendorReviewHelper->getVendorReviewsCollection($currentVendor);
                    //todo
                }
                $vendorResult = [];
                foreach ($vendors as $vendor) {
                    $vendorResult[] = $vendor;
                }
                $finalProductData->addData([
                    'vendor' => $vendorResult,
                ]);
            }
            $finalProductData->addData([
                'url' => $finalProductData->getProductUrl()
            ]);

            $typeId = $finalProductData->getTypeId();
            if ($typeId == 'ticket') {
                $productId = $finalProductData->getEntityId();
                $boughtTicket = $this->_eventTicket->getAllBoughtTicket($productId);
                $eventInfo = $this->_eventTicket->getEventData($productId);
                if ($eventInfo) {
                    $finalProductData->addData([
                        'bought' => $boughtTicket,
                        'event_info' => $this->_objectFactory->addData($eventInfo)
                    ]);
                }
            }

            if ($this->_grouponDataHelper->isDeal($id)) {
                $productId = $finalProductData->getEntityId();
                $childProductIds = $this->_grouponDataHelper->getChildIds($productId);
                $boughtGroupon = $this->_groupon->getAllBoughtGroupon($childProductIds);
                $couponInfo = $this->_groupon->getGrouponInfo($store, $productId);
                if ($couponInfo) {
                    $finalProductData->addData([
                        'bought' => $boughtGroupon,
                        'coupon_info' => $this->_objectFactory->addData($couponInfo),
                        'type_id' => "groupon"
                    ]);
                }
            }

            if ($typeId == 'hotel') {
                $hotelInfo = $this->_hotel->getHotelData($store, $finalProductData->getEntityId());
                if ($hotelInfo) {
                    $finalProductData->addData([
                        'hotel_info' => $this->_objectFactory->addData($hotelInfo)
                    ]);
                }
            }

            return $finalProductData;
        } else {
            return $finalProductData;
        }
    }
    public function getTopReviewByProductId($productId,$reviewCollection){
        $allowedRatingAttr = $this->getAllowedRatingAttribute($productId);
        $results = [];
        foreach ($allowedRatingAttr as $attrId){
            $_review = $this->getNewestReview($attrId,$reviewCollection->getColumnValues('review_id'));
            if($_review->getIsAggregate()){
                $ratingCode = $_review->getRatingCode();
                $reviewPercent = $_review->getPercent();
                $reviewDetail = $_review->getDetail();
                $results[] = [
                    'rating_code' => $ratingCode,
                    'review_percent' => $reviewPercent,
                    'review_detail' => $reviewDetail
                ];
            }
        }
        return $results;
    }

    public function getReviewCollection($productId){
        return $this->_sellYourRatingHelper->getReviewCollection($productId);
    }
    public function getNewestReview($ratingAttrId, $reviewIds){
        /** @var  $voteCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $voteCollection = $this->_voteFactory->create()->getCollection()
            ->addFieldToFilter('main_table.review_id', ['in' => $reviewIds])
            ->addFieldToFilter('main_table.rating_id', $ratingAttrId);
        $voteCollection->setStoreFilter(
            $this->_storeManagerInterface->getStore()->getId()
        );
        $voteCollection->addRatingInfo(
            $this->_storeManagerInterface->getStore()->getId()
        );
        $voteCollection->getSelect()->joinLeft(
            ['review_detail' => $voteCollection->getTable('review_detail')],
            'main_table.review_id = review_detail.review_id',
            ['title', 'detail', 'nickname']
        );
        return $voteCollection->getLastItem();
    }
    public function getSmallestCategory($productId){
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->_productFactory->create()->load($productId);
        $categories = $product->getCategoryCollection()->addFieldToSelect('name');
        $categoryResults = null;
        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categories as $category){
            if($categoryResults == null || $category->getLevel() < $categoryResults->getLevel()){
                $categoryResults = $category;
            }
        }
        return $categoryResults;
    }
    public function getAllowedRatingAttribute($productId){
        /** @var \Magento\Catalog\Model\Category $category */
        $category = $this->getSmallestCategory($productId);
        if($category == null) return [];
        $categoryId = $category->getId();
        $ratingCollection = $this->_ratingInfoFactory->create()
            ->getCollection()
            ->addFieldToFilter(
                'category_ids',
                [
                    ['like' => '%,' . $categoryId],
                    ['like' => '%,' . $categoryId . ',%'],
                    ['like' => $categoryId . ',%'],
                    $categoryId
                ]
            );
        return $ratingCollection->getColumnValues('rating_id');
    }
    private function getCustomAttributesValue()
    {
        $result = null;
        $searchCriteria = $this->_searchCriteriaBuilder->create();
        $attributes = $this->_productAttributeFactory
            ->getList('catalog_product', $searchCriteria)
            ->getItems();
        foreach ($attributes as $attribute) {

//            if (
//                $attribute->getAttributeCode() != 'name'
//                && $attribute->getAttributeCode() != 'description'
//                && $attribute->getAttributeCode() != 'short_description'
//                && $attribute->getAttributeCode() != 'sku'
//                && $attribute->getAttributeCode() != 'price'
//                && $attribute->getAttributeCode() != 'special_price'
//                && $attribute->getAttributeCode() != 'special_from_date'
//                && $attribute->getAttributeCode() != 'special_to_date'
//                && $attribute->getAttributeCode() != 'cost'
//                && $attribute->getAttributeCode() != 'weight'
//                && $attribute->getAttributeCode() != 'meta_title'
//                && $attribute->getAttributeCode() != 'meta_keyword'
//                && $attribute->getAttributeCode() != 'meta_description'
//                && $attribute->getAttributeCode() != 'image'
//                && $attribute->getAttributeCode() != 'small_image'
//                && $attribute->getAttributeCode() != 'thumbnail'
//                && $attribute->getAttributeCode() != 'media_gallery'
//                && $attribute->getAttributeCode() != 'old_id'
//                && $attribute->getAttributeCode() != 'tier_price'
//                && $attribute->getAttributeCode() != 'has_options'
//                && $attribute->getAttributeCode() != 'custom_design_from'
//                && $attribute->getAttributeCode() != 'custom_design_to'
//                && $attribute->getAttributeCode() != 'gallery'
//                && $attribute->getAttributeCode() != 'status'
//                && $attribute->getAttributeCode() != 'url_key'
//                && $attribute->getAttributeCode() != 'url_path'
//                && $attribute->getAttributeCode() != 'minimal_price'
//                && $attribute->getAttributeCode() != 'custom_layout_update'
//                && $attribute->getAttributeCode() != 'page_layout'
//                && $attribute->getAttributeCode() != 'category_ids'
//                && $attribute->getAttributeCode() != 'required_options'
//                && $attribute->getAttributeCode() != 'image_label'
//                && $attribute->getAttributeCode() != 'small_image_label'
//                && $attribute->getAttributeCode() != 'thumbnail_label'
//                && $attribute->getAttributeCode() != 'created_at'
//                && $attribute->getAttributeCode() != 'updated_at'
//                && $attribute->getAttributeCode() != 'country_of_manufacture'
//                && $attribute->getAttributeCode() != 'msrp_display_actual_price_type'
//                && $attribute->getAttributeCode() != 'msrp'
//                && $attribute->getAttributeCode() != 'gift_message_available'
//                && $attribute->getAttributeCode() != 'price_type'
//                && $attribute->getAttributeCode() != 'sku_type'
//                && $attribute->getAttributeCode() != 'weight_type'
//                && $attribute->getAttributeCode() != 'price_view'
//                && $attribute->getAttributeCode() != 'shipment_type'
//                && $attribute->getAttributeCode() != 'links_purchased_separately'
//                && $attribute->getAttributeCode() != 'samples_title'
//                && $attribute->getAttributeCode() != 'links_title'
//                && $attribute->getAttributeCode() != 'links_exist'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//                && $attribute->getAttributeCode() != 'custom_layout'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//                && $attribute->getAttributeCode() != 'quantity_and_stock_status'
//            ) {
//                $result[] = $attribute->getData();
//            }
            if ($attribute->getFrontendInput() == 'select') {
                $allAttributeOptions = $this->_eavConfig->getAttribute('catalog_product', $attribute->getAttributeCode())
                    ->getSource()->getAllOptions();
                if (sizeof($allAttributeOptions) > 0) {
                    $allAttributeOptionsArray = [];
                    foreach ($allAttributeOptions as $allAttributeOption) {
                        if ($allAttributeOption['value']
                            && strlen($allAttributeOption['value']) > 0
                            && strlen($allAttributeOption['label']) > 0) {
                            $allAttributeOptionsArray[] = [
                                'label' => is_string($allAttributeOption['label']) ? $allAttributeOption['label'] : $allAttributeOption['label']->getText(),
                                'value' => $allAttributeOption['value']
                            ];
                        }
                    }
                    if (sizeof($allAttributeOptionsArray) > 0) {
                        $result[] = [
                            'code' => $attribute->getAttributeCode(),
                            'value' => $allAttributeOptionsArray,
                            'visible' => $attribute->getIsVisibleOnFront(),
                            'label' => $attribute->getFrontendLabel()
                        ];
                    }
                }
            } else if ($attribute->getFrontendInput() == 'boolean') {
                $result[] = [
                    'code' => $attribute->getAttributeCode(),
                    'value' => [
                        [
                            'value' => 0,
                            'label' => 'false'
                        ],
                        [
                            'value' => 1,
                            'label' => 'true'
                        ]
                    ],
                    'visible' => $attribute->getIsVisibleOnFront(),
                    'label' => $attribute->getFrontendLabel()
                ];
            } else {
                $result[] = [
                    'code' => $attribute->getAttributeCode(),
                    'visible' => $attribute->getIsVisibleOnFront(),
                    'label' => $attribute->getFrontendLabel()
                ];
            }
        }
        return $result;
    }

    protected function getVendorSpecialPrice($vendor)
    {
        try {
            if ($vendor['special_from_date'] != null && $vendor['special_to_date'] != null) {
                $today = date("Ymd");
                $from = str_replace('-', '', $vendor['special_from_date']);
                $to = str_replace('-', '', $vendor['special_to_date']);
                if (strcmp($today, $from) >= 0 && strcmp($today, $to) <= 0) {
                    return $vendor['special_price'];
                }
            } else {
                return $vendor['special_price'];
            }
        } catch (Exception $e) {
            return 0;
        }
        return 0;
    }

    public function getGiftCardPhysical($productId)
    {
        $product = $this->_productFactory->create()->load($productId);
        $result = $product->getData();
        $result['api_status'] = '1';

        return $result;
    }

    public function getMyWishlistByProductId($customerId,$productId){
        if (empty($customerId) || !isset($customerId) || $customerId == "") {
            throw new InputException(__('Id required'));
        } else {
            $collection = $this->_wishlistCollectionFactory->create()->addCustomerIdFilter($customerId)->addFieldToFilter('product_id',$productId);
            $wishlistData = [];
            foreach ($collection as $item) {
                $productInfo = $item->getProduct()->toArray();
                $data = [
                    "wishlist_item_id" => $item->getWishlistItemId(),
                    "wishlist_id"      => $item->getWishlistId(),
                    "product_id"       => $item->getProductId(),
                    "store_id"         => $item->getStoreId(),
                    "added_at"         => $item->getAddedAt(),
                    "description"      => $item->getDescription(),
                    "qty"              => round($item->getQty()),
                    "product"          => $productInfo
                ];
                $wishlistData[] = $data;
            }
            return $wishlistData;
        }
    }


    /**
     * Get Allowed Products
     *
     * @param Product $product
     * @return \Magento\Catalog\Model\Product[]
     */
    public function getAllowProducts($product)
    {
        $products = [];
        $allProducts = $product->getTypeInstance()->getUsedProducts($product, null);
        foreach ($allProducts as $product) {
            if ($product->isSaleable()) {
                $products[] = $product;
            }
        }

        return $products;
    }

    /**
     * @param Attribute $attribute
     * @param array $config
     * @return array
     */
    public function getAttributeOptionsData($attribute, $config)
    {
        $attributeOptionsData = [];
        foreach ($attribute->getOptions() as $attributeOption) {
            $vendors = [];
            $optionId = $attributeOption['value_index'];
            $products = isset($config[$attribute->getAttributeId()][$optionId])
                ? $config[$attribute->getAttributeId()][$optionId]
                : [];

            $vendorCollection = $this->_vendorFactory->create()->getCollection();
            $vendorCollection->getSelect()->join(
                ['vendor_product' => $vendorCollection->getTable('udropship_vendor_product')],
                'main_table.vendor_id = vendor_product.vendor_id',
                ['vendor_product.product_id AS product_id']
            );
            $vendorCollection->addFieldToFilter('product_id', array('in' => $products));
            $vendorCollection->getSelect()->group('vendor_id');

            foreach ($vendorCollection as $vendor) {
                array_push($vendors, $this->getVendorInformation($vendor));
            }

            $attributeOptionsData[] = [
                'id' => $optionId,
                'label' => $attributeOption['label'],
                'products' => $products,
                'vendors' => $vendors
            ];
        }
        return $attributeOptionsData;
    }

    /**
     * Get vendor information
     *
     * @param \Unirgy\Dropship\Model\Vendor $vendor
     * @return array
     */
    public function getVendorInformation($vendor)
    {
        $ratingDetails = [];
        $vendorReview = $this->getVendorReviewsCollection($vendor->getId());
        $ratingOptionsVote = [];

        foreach ($vendorReview as $review) {
            $reviewData = $review->getData();
            $ratingVotes = $review->getRatingVotes();
            $reviewVote = [];

            foreach ($ratingVotes as $vote) {
                $voteItem = [
                    'rating_code' => $vote->getRatingCode(),
                    'value' => $vote->getValue(),
                    'rating_summary' => $vote->getPercent(),
                    'store_id' => $vote->getStoreId(),
                    'is_aggregate' => $vote->getIsAggregate()
                ];

                array_push($reviewVote, $voteItem);
            }
            $ratingOptionsVote[] = $reviewVote;

            unset($reviewData['rating_votes']);
            unset($reviewData['shipping_address']);
            $data = $reviewData;
            $data['rating_options'] = $reviewVote;
            array_push($ratingDetails, $data);
        }

        $ratingGeneral = $this->countingRatingOptions($ratingOptionsVote);

        return [
            'vendor_name' => $vendor->getVendorName(),
            'vendor_description' => $vendor->getBusinessDes(),
            'vendor_email' => $vendor->getEmail(),
            'vendor_phone' => $vendor->getPhone(),
            'vendor_street' => $vendor->getStreet(),
            'vendor_zip' => $vendor->getZip(),
            'vendor_country' => $vendor->getCountryId(),
            'rating_general' => $ratingGeneral,
            'rating_details' => $ratingDetails,
            'xxx' => $vendorReview->count()
        ];
    }
    public function getRatingByVendorId($vendorId){
        $vendorReview = $this->_ratingHelper->getVendorReviewsCollection($vendorId)->getItems();

        $totalReview = 0;
        $totalPoint = 0;
        foreach ($vendorReview as $item) {
//            $review = $this->_reviewSummary->create()->load($item->getEntityPkValue());
            $review = $this->_voteFactory->create()->load($item->getReviewId(), 'review_id');
            $totalPoint += floatval($review->getPercent());
            $totalReview++;
        }
        if($totalReview > 0){
            $general = [
                'average_rating_summary' => number_format($totalPoint / $totalReview, 2),
                'total_review' => $totalReview
            ];
        }else{
            $general = [
                'average_rating_summary' => 0.0,
                'total_review' => $totalReview
            ];
        }

        return $general;
    }
    private function countingRatingOptions($voteData)
    {
        $summing = [];
        $naming = [];
        $allCount = 0;
        foreach ($voteData as $voteDatum) {
            foreach ($voteDatum as $vote) {
                if ($vote['is_aggregate'] === '0') continue;
                if (!isset($summing[$vote['rating_code']])) {
                    $summing[$vote['rating_code']] = intval($vote['value']);
                    $naming[$vote['rating_code']] = 1;
                    $allCount++;
                } else {
                    $summing[$vote['rating_code']] += intval($vote['value']);
                    $naming[$vote['rating_code']]++;
                    $allCount++;
                }
            }
        }
        $voteCount = count($voteData);
        $countTotal = 0;
        $optionAverage = [];

        foreach ($summing as $key => $item) {
            $countTotal += $item;
            if (intval($naming[$key]) !== 0) {
                $count = intval($naming[$key]);
                $optionAverage[$key] = number_format($item / intval($count), 2);
            }
        }

        $result = [];
        if (intval($allCount) !== 0) {
            $result['vote_average'] = number_format(intval($countTotal) / intval($allCount), 2);
        }
        $result['vote_count_total'] = $allCount;
        $result['rating_option_average'] = [$optionAverage];

        return $result;
    }

    public function getVendorReviewsCollection($vendorId){
        return $this->_reviewFactory->create()->getCollection()
            ->addStoreFilter($this->_storeManagerInterface->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [10, 7]])
            ->addFieldToFilter('main_table.entity_pk_value', ['eq' => $vendorId])
            ->setFlag('AddRateVotes', true)
            ->setFlag('AddAddressData', true);
    }
}
