<?php
namespace Magenest\VendorApi\Helper;

class RatingOptionHelper {
    /**
     * @var \Magenest\VendorApi\Helper\OrderHelper
     */
    protected $orderHelper;
    protected $_ratingInfoFactory;
    public function __construct(
        \Magenest\VendorApi\Helper\OrderHelper $orderHelper,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory
    ){
        $this->orderHelper = $orderHelper;
        $this->_ratingInfoFactory = $ratingInfoFactory;
    }

    public function getRatingOption($product){
        $_objectLoader = \Magento\Framework\App\ObjectManager::getInstance();
        $aggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getAggregateRatings();
        $noneAggregateRatings = $_objectLoader->get('Magenest\FixUnirgy\Block\ReviewForm')->getNonAggregateRatings();
        $ratings = [];
        foreach ($aggregateRatings as $aggregateRating){
            $checkCategories = $this->checkCategoryVirtualProduct($aggregateRating,$product);

            if($checkCategories){
                $options = $aggregateRating->getOptions();
                $dataRating = [];
                $dataRating['code'] = $aggregateRating->getRatingCode();
                foreach ($options as $option){
                    $dataRating['option'][] = $option->getData();
                }
                $ratings[] = $dataRating;
            }
        }
        foreach ($noneAggregateRatings as $noneAggregateRating){
            $checkCategories = $this->checkCategoryVirtualProduct($noneAggregateRating,$product);
            if($checkCategories){
                $options = $noneAggregateRating->getOptions();
                $dataRating = [];
                $dataRating['code'] = $noneAggregateRating->getRatingCode();
                foreach ($options as $option){
                    $dataRating['option'][] = $option->getData();
                }
                $ratings[] = $dataRating;
            }
        }
        return $ratings;
    }
    public function checkCategoryVirtualProduct($rateModel, $product)
    {
        if ($product != null) {
            $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
                ->addFieldToFilter('rating_id', $rateModel->getId());

            $categoriesId = $product->getCategoryIds();
            if ($ratingInfo->getSize() > 0) {
                $rating = $ratingInfo->getFirstItem();
                $allowedCategory = $rating->getData('category_ids');
                $allowedCategorIdsArray = explode(',', $allowedCategory);
                if (count(array_intersect($allowedCategorIdsArray, $categoriesId)) == 0) return false;
                return true;
            }
            return false;
        }
        return false;
    }
    public function checkVendorAllowRating($vendorId){
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $vendorTbl = $connection->getTableName('udropship_vendor');
        $sql = "SELECT `vendor_id`, `vendor_name`, `email` FROM $vendorTbl 
                WHERE `allow_udratings` = 1
                AND `vendor_id` = $vendorId;
                ";
        $result = $connection->fetchRow($sql);
        if(empty($result)){
            return false;
        }else{
            return true;
        }
    }
}
