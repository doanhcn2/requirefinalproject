<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper;

use Magenest\Ticket\Controller\Order\Date;
use Unirgy\DropshipPo\Model\PoFactory;
use Unirgy\Dropship\Helper\Data;

class OrderHelper
{
    /** @var \Magento\Sales\Model\OrderFactory */
    protected $_orderFactory;
    /**
     * @var PoFactory
     */
    protected $_poFactory;
    /**
     * @var Data
     */
    protected $_hlp;
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezoneInterface;

    private $collectionProcessor;

    protected $collectionFactory;
    protected $_magenestReviewDataHelper;
    protected $_ratingInfoFactory;
    protected $_productRepository;
    /**
     * Serializer interface instance.
     *
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    private $serializer;

    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\DataObject $dataObject,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\DropshipPo\Model\PoFactory $modelPoFactory,
        \Unirgy\Dropship\Helper\Data $helperData,
        \Magenest\FixUnirgy\Helper\Review\Data $datahelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor = null,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    )
    {
        $this->_timezoneInterface = $timezoneInterface;
        $this->_hlp = $helperData;
        $this->_poFactory = $modelPoFactory;
        $this->_objectFactory = $dataObject;
        $this->_orderFactory = $orderFactory;
        $this->storeManager = $storeManager;
        $this->_magenestReviewDataHelper = $datahelper;
        $this->collectionFactory = $collectionFactory;
        $this->_ratingInfoFactory = $ratingInfoFactory;
        $this->_productRepository = $productRepository;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Serialize\Serializer\Json::class);
    }

    public function getOrderCollectionByCustomerId($customer_id){
        if($customer_id){
            $orderColllection = $this->_orderFactory->create()->getCollection()->addFieldToFilter('customer_id',$customer_id);
            return $orderColllection;
        }
    }

    private function getCollectionProcessor()
    {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface::class
            );
        }
        return $this->collectionProcessor;
    }

    public function getAllOrderByCustomerId($customerId, $cur_page, $size)
    {
        $order = [];
//        $curPage = isset($cur_page) && is_numeric($cur_page) ? $cur_page : 1;
//        $curSize = isset($size) && is_numeric($size) ? $size : 10;
        $to = date("Y-m-d h:i:s"); // current date
        $from = strtotime('-90 day', strtotime($to));
        $from = date('Y-m-d h:i:s', $from); // 90 days before
        /**
         * @var $orderCollection \Magento\Sales\Model\ResourceModel\Order\Collection
         */
        $orderCollection = $this->_orderFactory->create()->getCollection();
        $orders = $orderCollection->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('created_at', array('from' => $from, 'to' => $to));//->setCurPage($curPage)->setPageSize($curSize);
        foreach ($orders as $orderItem) {
            /**
             * @var $orderItem \Magento\Sales\Model\Order
             */
            $data = $orderItem->getData();
            $orderItems = $this->getAllItemsOrder($orderItem);
            $orderData = [
                'order_information' => [
                    'order_id' => $data['entity_id'],
                    'state' => $data['state'],
                    'status' => $data['status'],
                    'updated_at' => $data['updated_at']
                ],
                'status_information' => $this->getOrderHistory($orderItem),
                'item_collection' => $orderItems
            ];
            $order[] = [
                'general_information' => $orderData,
                'detail_information' => $this->getOrderDetailsInformation($orderItem)
            ];
        }
        return $order;
    }

    public function getCustomerOrderByOrderId($order_id, $po = false)
    {
        $orderCollection = $this->_orderFactory->create()->load($order_id);
        $data = $orderCollection->getData();

        $orderItems = $this->getOrderDetailsInformation($orderCollection);
        if($po){
            $comment = $this->getVendorOrderDetailComment($po);
            $orderItems['comment'] = $comment;
            $poItems = $this->getQtyPOByPOID($po);
            $items = $orderItems['items'];
            $dataItem = [];
            foreach ($items as $item){
                $dataTmp = [];
                $item_id = $item['item_id'];
                $key = $this->searchForId($item_id,$poItems);
                if($key != null){
                    $i = $key-1;
                    $dataTmp = $item;
                    unset($poItems[$i]['order_item_id']);
                    $dataTmp['qty'] = $poItems[$i];
                }
                if(!empty($dataTmp)) $dataItem[] = $dataTmp;
            }
            if(!empty($dataItem)) $orderItems['items'] = $dataItem;
        }
        $orderData = [
            'order_information' => [
                'order_id' => $data['entity_id'],
                'state' => $data['state'],
                'status' => $data['status'],
                'updated_at' => $data['updated_at']
            ],
            'status_information' => $this->getOrderHistory($orderCollection),
            'item_collection' => $this->getAllItemsOrder($orderCollection)
        ];
        $order[] = [
            'general_information' => $orderData,
            'detail_information' => $orderItems
        ];
        return $order;
    }

    public function getAllItemsOrder($orderItem){
        $allItems = $orderItem->getAllItems();
        $orderIncrementId = $orderItem->getIncrementId();
        $orderItems = [];
        foreach ($allItems as $items) {
            $product_type = $items->getProductType();
            if($product_type == 'coupon') continue;
            $itemDatas = $items->getData();
            foreach ($itemDatas['product_options']['additional_options'] as $option) {
                if ($option['label'] === 'Vendor') {
                    $vendor = $option['value'];
                }
            }
            $productId = $itemDatas['product_id'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);

            $productImage = $optionText = '';
            if ($product->getId()) {
                $product_image = $product->getData('image');
                if ($product_image) {
                    $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                    $productImage = $mediaUrl . 'catalog/product' . $product_image;
                }
                $brandId = $product->getData('brand');
                $attributeId = $product->getResource()->getAttribute('brand');
                if ($attributeId) {
                    if ($attributeId->usesSource()) {
                        $data = $attributeId->getSource()->getAttribute()->getData();
                        if ($brandId) {
                            $optionText = $attributeId->getSource()->getOptionText($brandId);
                        } else {
                            $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
                        }
                    }
                }
            }

            $orderItems[] = [
                'item_id' => $itemDatas['item_id'],
                'order_id' => $itemDatas['order_id'],
                'order_increment_id' => $orderIncrementId,
                'vendor' => @$vendor === null ? $itemDatas['udropship_vendor'] : $vendor,
                'vendor_id' => $itemDatas['udropship_vendor'],
                'item_name' => $itemDatas['name'],
                'item_sku' => $itemDatas['sku'],
                'price' => $itemDatas['price'],
                'base_price' => $itemDatas['base_price'],
                'row_total' => $itemDatas['row_total'],
                'qty' => $itemDatas['product_options']['info_buyRequest']['qty'],
                'product_image' => $productImage,
                'product_id' => $productId,
                'brand' => $optionText
            ];
        }

        return $orderItems;
    }

    /**
     * @param $orderItem
     * @return array|null
     */
    private function getOrderHistory($orderItem)
    {
        /**
         * @var $orderItem \Magento\Sales\Model\Order
         */
        $history = [
            'ordered' => $orderItem->getUpdatedAt()
        ];
        $invoice = $orderItem->getInvoiceCollection();
        $invoiceTime = null;
        foreach ($invoice as $item) {
            $invoiceTime = $item->getCreatedAt();
        }
        $history['processing'] = @$invoiceTime;

        $invoice = $orderItem->getShipmentsCollection();
        $shipmentTime = null;
        foreach ($invoice as $item) {
            $shipmentTime = $item->getCreatedAt();
        }
        $history['shipped'] = @$shipmentTime;

        $history['delivered'] = null;
        if ($orderItem->getStatus() === 'complete') {
            if ($orderItem->getIsVirtual()) {
                foreach ($orderItem->getInvoiceCollection() as $item) {
                    $deliveredTime = $item->getUpdatedAt();
                }
                $history['delivered'] = @$deliveredTime;
            } else {
                foreach ($orderItem->getShipmentsCollection() as $item) {
                    $deliveredTime = $item->getUpdatedAt();
                }
                $history['delivered'] = @$deliveredTime;
            }
        }

        return $history;
    }

    private function getOrderDetailsInformation($orderItem)
    {
        /**
         * $result = ['is_virtual' => $isVirtual, 'order_summary' => [], 'items' => [], 'shipment' => [], 'payment_method' => []]
         *
         * @var $orderItem \Magento\Sales\Model\Order
         */
        $orderSummary = [
            'currency_code' => $orderItem->getOrderCurrencyCode(),
            'subtotal' => $orderItem->getSubtotal(),
            'discount' => $orderItem->getDiscountAmount(),
            'shipping' => $orderItem->getShippingAmount(),
            'tax' => $orderItem->getTaxAmount(),
            'order_total' => $orderItem->getGrandTotal(),
            'balance_discount' => $orderItem->getCustomerBalanceAmount(),
            'reward_points_balance' => $orderItem->getRewardPointsBalance()
        ];
        $paymentsOrder = $orderItem->getPayment()->getAdditionalInformation();
        $bill_adress = $orderItem->getBillingAddress()->getData();
        $shippings = $orderItem->getUdropshipShippingDetails();
        $shiping_method = [];
        if($shippings != null && $shippings != "[]"){
            $shippingData = $this->serializer->unserialize($shippings);
            $shippingMethods = $shippingData['methods'];
            foreach ($shippingMethods as $key => $value){
                $data = $value;
                $data['vendor_id'] = $key;
                $shiping_method[] = $data;
            }
        }
        $shipments = $orderItem->getShipmentsCollection()->getFirstItem();
        $ship_address = $orderItem->getShippingAddress();
        if($ship_address){
            $shipAdressData = $ship_address->getData();
        }else{
            $shipAdressData = null;
        }
        $shipment = [
            'shipment_id' => $shipments->getId(),
            'created' => $shipments->getCreatedAt(),
            'updated' => $shipments->getUpdatedAt(),
            'shipment_status' => $shipments->getShipmentStatus(),
            'address' => $shipAdressData,
            'description' => $orderItem->getShippingDescription(),
            'shipping method' => $orderItem->getShippingMethod()
        ];
        $result = [
            'order_id' => $orderItem->getId(),
            'created_at' => $orderItem->getCreatedAt(),
            'status' => $orderItem->getStatus(),
            'order_total' => $orderItem->getGrandTotal(),
            'bill_to_name' => $bill_adress['firstname'] . ' ' . $bill_adress['lastname'],
            'ship_to_name' => $ship_address['firstname'] . ' ' . $ship_address['lastname'],
            'is_virtual' => $orderItem->getIsVirtual(),
            'items' => $this->getAllItemsOrder($orderItem),
            'order_summary' => $orderSummary,
            'payment_method' => $paymentsOrder['method_title'],
            'shipping_method' => empty($shiping_method)?null:$shiping_method,
            'shipments' => $shipment
        ];
        return $result;
    }

    public function getAllCustomerOrder($customerId, $filterby, $datafilter, $search, $cur_page, $size, $sort)
    {
        if (is_numeric($cur_page) && is_numeric($size)) {
            $hasDivided = true;
        } else {
            $hasDivided = false;
        }
        /**
         * @var $orderCollection \Magento\Sales\Model\ResourceModel\Order\Collection
         */
        $orderCollection = $this->_orderFactory->create()->getCollection();
        $orders = $orderCollection->addFieldToFilter('customer_id', $customerId);
        if ($hasDivided) {
            $orders->setCurPage($cur_page)->setPageSize($size);
        }
        if (!empty($sort) && is_array($sort)) {
            $sortby = $sort['sortby'];
            $sorttype = $sort['sorttype'];
            $orders->setOrder("main_table.$sortby", $sorttype);
        } elseif (!empty($sort)) {
            $orders->setOrder("main_table.$sort", 'DESC');
        }

        if (!empty($filterby) && $datafilter != '') {
            if ($filterby == 'filter_status' && !is_array($datafilter)) {
                $status = $this->getStatus($datafilter);
                if($status == 99){
                    $orders->addAttributeToFilter('main_table.status',$datafilter);
                }else{
                    $orders->addAttributeToFilter('main_table.udropship_status',$status);
                }
            }
            if ($filterby == 'filter_order_date') {
                if (is_array($datafilter)) {
                    $from = isset($datafilter['from']) ? $datafilter['from'] : '';
                    if ($from != '') {
                        $from = $this->_timezoneInterface->date(new \DateTime($from))->format('Y-m-d H:i:s');
                        $orders->addAttributeToFilter("main_table.created_at", ['gteq' => $from]);
                    }

                    $to = isset($datafilter['to']) ? $datafilter['to'] : '';
                    if ($to != '') {
                        $to = $this->_timezoneInterface->date(new \DateTime($to))->format('Y-m-d H:i:s');
                        $orders->addAttributeToFilter("main_table.created_at", ['lteq' => $to]);
                    }
                } else {
                    $to = date("Y-m-d h:i:s"); // current date
                    $from = strtotime("-$datafilter month", strtotime($to));
                    $from = date('Y-m-d h:i:s', $from);
                    $orders->addAttributeToFilter("main_table.created_at", array('from' => $from, 'to' => $to));
                }
            }
        }

        $order = [];
        if ($search != '' && empty($filterby)) {
            $results = [];
            $dataSearchItem = $this->searchItemNameInOrder($orders, $search);
            foreach ($orders as $orderItem) {
                $items = $orderItem->getItems();
                $flag = false;
                foreach ($items as $item) {
                    if (in_array($item->getProductId(), $dataSearchItem)) {
                        $flag = true;
                    }
                }
                if ($flag) {
                    /**
                     * @var $orderItem \Magento\Sales\Model\Order
                     */
                    $data = $orderItem->getData();
                    $orderItems = $this->getAllItemsOrder($orderItem);
                    $orderHistory = $this->getOrderHistory($orderItem);
                    $detail_info = $this->getOrderDetailsInformation($orderItem);
                    $orderData = [
                        'order_information' => [
                            'order_id' => $data['entity_id'],
                            'state' => $data['state'],
                            'status' => $data['status'],
                            'created_at' => $data['created_at'],
                            'updated_at' => $data['updated_at']
                        ],
                        'status_information' => $orderHistory,
                        'item_collection' => $orderItems
                    ];
                    $results[] = [
                        'general_information' => $orderData,
                        'detail_information' => $detail_info
                    ];
                }
                $flag = false;
            }
            return $results;
        }
        foreach ($orders as $orderItem) {
            /**
             * @var $orderItem \Magento\Sales\Model\Order
             */
            $data = $orderItem->getData();
            $orderItems = $this->getAllItemsOrder($orderItem);
            $orderHistory = $this->getOrderHistory($orderItem);
            $detail_info = $this->getOrderDetailsInformation($orderItem);
            $orderData = [
                'order_information' => [
                    'order_id' => $data['entity_id'],
                    'state' => $data['state'],
                    'status' => $data['status'],
                    'created_at' => $data['created_at'],
                    'updated_at' => $data['updated_at']
                ],
                'status_information' => $orderHistory,
                'item_collection' => $orderItems
            ];
            $order[] = [
                'general_information' => $orderData,
                'detail_information' => $detail_info
            ];
        }
        return $order;
    }

    public function getAllProductInOrder($order)
    {
        $productId = [];
        foreach ($order as $orderItem) {
            $allItems = $orderItem->getAllItems();
            foreach ($allItems as $items) {
                $productId[] = $items->getProductId();
            }
        }
        return $productId;
    }

    public function searchItemNameInOrder($order, $search)
    {
        $arrProductId = $this->getAllProductInOrder($order);
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToFilter('entity_id', ['in' => $arrProductId]);
        $collection->addAttributeToFilter(
            'name', ['like' => '%' . $search . '%']
        );
        $dataCollections = $collection->getData();
        foreach ($dataCollections as $collection) {
            $data[] = $collection['entity_id'];
        }
        return $data;
    }

    public function getStatus($status)
    {
        $st = 0;
        switch ($status) {
            case 'exported':
                $st = 10;
                break;
            case 'returned':
                $st = 11;
                break;
            case 'ack':
                $st = 9;
                break;
            case 'backorded':
                $st = 5;
                break;
            case 'ready':
                $st = 3;
                break;
            case 'partial':
                $st = 2;
                break;
            case 'shipped':
                $st = 1;
                break;
            case 'canceled':
                $st = 6;
                break;
            case 'delivered':
                $st = 7;
                break;
            default:
                $st = 99;
                break;
        }
        return $st;
    }

    public function getRatingItemCollection($customerId)
    {
        $itemCollection = $this->_magenestReviewDataHelper->getPendingVirtualItemReview();
        $itemCollection->addFieldToFilter('so.customer_id', $customerId);
        $itemCollection->addFieldToFilter('product_type', ['IN' => ['virtual', 'coupon', 'ticket']]);
        $itemCollection->load();

        return $itemCollection;
    }

    public function getRatingItemsByProductId($productIds, $customerId)
    {
        $itemCollection = $this->_magenestReviewDataHelper->getPendingVirtualItemReview();
        $itemCollection->addFieldToFilter('so.customer_id', $customerId);
        $itemCollection->addFieldToFilter('main_table.product_id', ['in' => $productIds]);
        $itemCollection->getSelect()->group('main_table.product_id');

        return $itemCollection->getFirstItem();
    }

    public function checkCategoryVirtualProduct($rateModel, $orderItem)
    {
        if ($orderItem != null) {
            $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
                ->addFieldToFilter('rating_id', $rateModel->getId());
            $categoriesId = $orderItem->getProduct()->getCategoryIds();
            if ($ratingInfo->getSize() > 0) {
                $rating = $ratingInfo->getFirstItem();
                $allowedCategory = $rating->getData('category_ids');
                $allowedCategorIdsArray = explode(',', $allowedCategory);
                if (count(array_intersect($allowedCategorIdsArray, $categoriesId)) == 0) return false;
                return true;
            }
            return false;
        }
        return false;
    }

    public function checkCategoryShipment($rateModel, $shipment)
    {
        $shipmentItems = $shipment->getItems();
        $categoriesId = [];
        /** @var  $item \Magento\Sales\Model\Order\Shipment\Item */
        foreach ($shipmentItems as $item) {
            $productId = $item->getProductId();
            /** @var  $product \Magento\Catalog\Model\Product */
            $product = $this->_productRepository->getById($productId);
            $tempIds = $product->getCategoryIds();
            foreach ($tempIds as $tempId) {
                if (!in_array($tempId, $categoriesId)) {
                    $categoriesId[] = $tempId;
                }
            }
        }
        $ratingInfo = $this->_ratingInfoFactory->create()->getCollection()
            ->addFieldToFilter('rating_id', $rateModel->getId());
        if ($ratingInfo->getSize() > 0) {
            $rating = $ratingInfo->getFirstItem();
            $allowedCategory = $rating->getData('category_ids');
            $allowedCategorIdsArray = explode(',', $allowedCategory);
            if (count(array_intersect($allowedCategorIdsArray, $categoriesId)) == 0) return false;
            return true;
        }
        if ($ratingInfo->getSize() == 0) return false;
        if (count($categoriesId) == 0) return false;
        return true;
    }

    public function getAllVendorOrder($vendorId, $searchCriteria)
    {

        /**
         * @var $orderCollection \Magento\Sales\Model\ResourceModel\Order\Collection
         */
        $collection = $this->_poFactory->create()->getCollection();

        $collection->join('sales_order', "sales_order.entity_id=main_table.order_id", [
            'order_increment_id' => 'increment_id',
            'order_created_at' => 'created_at',
            'shipping_method',
        ]);

        $collection->addAttributeToFilter('main_table.udropship_vendor', $vendorId);

        $this->collectionProcessor->process($searchCriteria, $collection);


        return $collection;
    }

    public function getPurchaseOrderByPOId($PoId) {
        $_po = $this->_poFactory->create()->load($PoId);
        $orderId = $_po->getOrderId();
        $data = $this->getCustomerOrderByOrderId($orderId, $PoId);
        return $data;
    }
    public function getQtyPOByPOID($PoId){
        $_po = $this->_poFactory->create()->load($PoId);
        $itemsInPo = $_po->getAllItems();
        $items = [];
        foreach ($itemsInPo as $item){
            if ($item->getOrderItem()->getParentItem()) continue;
            $items[] = [
                'order_item_id' => $item->getOrderItemId(),
                'qty' => $item->getQty(),
                'qty_shipped' => $item->getQtyShipped(),
                'qty_invoiced' => $item->getQtyInvoiced(),
                'qty_canceled' => $item->getQtyCanceled()
            ];
        }
        return $items;
    }
    public function searchForId($value, $array) {
        foreach ($array as $key => $val) {
            if ($val['order_item_id'] === $value) {
                return $key+1;
            }
        }
        return null;
    }
    public function getVendorOrderDetailComment($PoId){
        $_po = $this->_poFactory->create()->load($PoId);
        $_comments = $_po->getComments();
        $commentData = [];
        foreach ($_comments as $comment){
            $commentData[] = $comment->getData();
        }
        return $commentData;
    }
}