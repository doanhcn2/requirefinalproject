<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper;

use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\UrlInterface;

/**
 * Class GrouponHelper
 * @package Magenest\VendorApi\Helper
 */
class GrouponHelper
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    protected $_reportFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Unirgy\DropshipVendorRatings\Helper\Data
     */
    protected $_vendorReviewHelper;

    /**
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var \Magento\Review\Model\Rating\Option\VoteFactory
     */
    protected $_voteFactory;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $_deal;

    /**
     * @var \Magenest\Groupon\Model\DeliveryFactory
     */
    protected $_delivery;

    /**
     * @var \Magenest\Groupon\Model\FeedbackFactory
     */
    protected $_feedback;

    /**
     * @var \Magenest\Groupon\Model\GrouponFactory
     */
    protected $_groupon;

    /**
     * @var \Magenest\Groupon\Model\LocationFactory
     */
    protected $_location;

    /**
     * @var \Magenest\Groupon\Model\TemplateFactory
     */
    protected $_template;

    /**
     * @var \Magenest\Groupon\Model\TermFactory
     */
    protected $_term;

    /**
     * @var \Magenest\Groupon\Model\TicketFactory
     */
    protected $_ticket;

    protected $_objectFactory;
    protected $ratingOptionHelper;
    protected $modelStoreManagerInterface;

    /**
     * GrouponHelper constructor.
     * @param \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory
     * @param \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreFactory $storeManager
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param ImageFactory $imageFactory
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory
     * @param \Magenest\Groupon\Model\FeedbackFactory $feedbackFactory
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magenest\Groupon\Model\LocationFactory $locationFactory
     * @param \Magenest\Groupon\Model\TemplateFactory $templateFactory
     * @param \Magenest\Groupon\Model\TermFactory $termFactory
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Magento\Framework\DataObject $objectFactory,
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreFactory $storeManager,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\DeliveryFactory $deliveryFactory,
        \Magenest\Groupon\Model\FeedbackFactory $feedbackFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Groupon\Model\TemplateFactory $templateFactory,
        \Magenest\Groupon\Model\TermFactory $termFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magenest\VendorApi\Helper\RatingOptionHelper $ratingOptionHelper,
        \Magento\Store\Model\StoreManagerInterface $modelStoreManagerInterface
    )
    {
        $this->_objectFactory = $objectFactory;
        $this->_voteFactory = $voteFactory;
        $this->_vendorReviewHelper = $vendorReviewHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_eavConfig = $eavConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productAttributeFactory = $productAttributeFactory;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_reportFactory = $reportFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_swatchHelper = $swatchHelper;
        $this->_deal = $dealFactory;
        $this->_delivery = $deliveryFactory;
        $this->_feedback = $feedbackFactory;
        $this->_groupon = $grouponFactory;
        $this->_location = $locationFactory;
        $this->_template = $templateFactory;
        $this->_term = $termFactory;
        $this->_ticket = $ticketFactory;
        $this->ratingOptionHelper = $ratingOptionHelper;
        $this->modelStoreManagerInterface = $modelStoreManagerInterface;
    }

    /**
     * @param $storeId
     * @param $size
     * @return array|null
     */
    public function getListGrouponDetail($storeId, $size)
    {
        $result = null;
        $listGroupons = $this->_deal->create()->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('enable', 1)
            ->setPageSize(10)
            ->setCurPage($size);

        if (count($listGroupons) > 0) {
            foreach ($listGroupons as $listGroupon) {
                $result[] = $this->getGrouponDetail($storeId, $listGroupon->getProductId());
            }
        }

        return $result;
    }

    /**
     * collection data of each groupon
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getGrouponDetail($storeId, $productId)
    {
        $result = null;
        $currentProduct = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
        $modelDeal = $this->_deal->create()->load($productId);

        if ($modelDeal->getId()) {
            $price = $currentProduct->getPrice();
            $finalPrice = $currentProduct->getFinalPrice();
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory->create()->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();
            $vendorId = key($currentProduct->getMultiVendorData());
            $result[] = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
                'vendor_id' => $vendorId,
                'vendor_data' => $this->getVendorInfo($currentProduct, $vendorId),
                'coupon_child' => $this->getCouponChild($storeId, $productId)
            ];
        }

        return $result;
    }

    /**
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getCouponChild($storeId, $productId)
    {
        $result = null;
        $modelDeal = $this->_deal->create()->getCollection()
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('parent_id', $productId);

        if (count($modelDeal) > 0) {
            /** @var \Magenest\Groupon\Model\Deal $deal */
            foreach ($modelDeal as $deal) {
                $modelProduct = $this->_productFactory->create()->load($deal->getProductId());
                $vendorId = key($modelProduct->getMultiVendorData());
                $result[] = [
                    'deal_id' => $deal->getId(),
                    'product_id' => $deal->getProductId(),
                    'store_id' => $deal->getStoreId(),
                    'name' => $modelProduct->getName(),
                    'sku' => $modelProduct->getSku(),
                    'price' => $modelProduct->getPrice(),
                    'final_price' => $modelProduct->getFinalPrice(),
                    'vendor_id' => $vendorId,
                    'vendor_data' => $this->getVendorInfo($modelProduct, $vendorId),
                ];
            }
        }

        return $result;
    }

    /**
     * collection data of each coupon
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getCouponDetail($storeId, $productId)
    {
        $result = null;
        $currentProduct = $this->_productFactory->create()->setStoreId($storeId)->load($productId);

        if ($currentProduct->getId() && $currentProduct->getTypeId() == 'coupon') {
            $price = $currentProduct->getPrice();
            $finalPrice = $currentProduct->getFinalPrice();
            $path = $this->_storeManager->create()
                ->load($storeId)
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory->create()->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();
            $vendorId = key($currentProduct->getMultiVendorData());
            $result[] = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
                'vendor_id' => $vendorId,
                'vendor_data' => $this->getVendorInfo($currentProduct, $vendorId),
                'coupon' => $this->getCouponInfo($storeId, $productId)
            ];
        }

        return $result;
    }

    /**
     * @param $product
     * @param $vendorId
     * @return null
     */
    public function getVendorInfo($product, $vendorId)
    {
        $result = null;
        if ($product->getMultiVendorData() && $product->getMultiVendorData() != null) {
            $data = $product->getMultiVendorData();
            $result = $data[$vendorId];
        }

        return $result;
    }

    /**
     * @param $storeId
     * @param $productId
     * @return null
     */
    public function getCouponInfo($storeId, $productId)
    {
        $result = null;
        /** @var \Magenest\Groupon\Model\Deal $modeDeal */
        $modeDeal = $this->_deal->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('store_id', $storeId)->getFirstItem();
        $result = [];
        if ($modeDeal->getId()) {
            $parentId = $modeDeal->getParentId();
            if ($parentId) {
                $modelDealParent = $this->_deal->create()->getCollection()
                    ->addFieldToFilter('product_id', $parentId)->getFirstItem()->getData();
                $result['child'] = $this->_objectFactory->addData([
                    'minimum_buyers_limit' => $modeDeal->getMinimumBuyersLimit(),
                    'maximum_buyers_limit' => $modeDeal->getMaximumBuyersLimit(),
                ]);
//                $resultTerm = null;
//                $a = $modelDealParent['term'];
//                $term = unserialize($modelDealParent['term']);
//                if ($term) {
//                    $resultTerm['addition_terms'] = $term['addition_terms'];
//                    $resultTerm['dynamic_term'] = null;
//                    if ($term['terms'] && !empty($term['terms'])) {
//                        foreach ($term['terms'] as $termId) {
//                            $modelTerm = $this->_term->create()->load($termId);
//                            if ($modelTerm->getId()) {
//                                $resultTerm['dynamic_term'][] = $modelTerm->getData();
//                            }
//                        }
//                    }
//                }
//                unset($modelDealParent['term']);
//                $result['parent'] = $modelDealParent;\
                $data = [];
                if ($modelDealParent['is_online_coupon'] == 0) {
                    $resultLocation = null;
                    $modelLocation = $this->_location->create()->getCollection()
                        ->addFieldToFilter('product_id', $modelDealParent['product_id']);
                    $data['address'] = [];
                    foreach ($modelLocation as $location) {
                        $resultLocation[] = $location->getData();
                    }
                    $data['address']['location'] = $resultLocation;

                    $resultDelivery = null;
                    $modelDelivery = $this->_delivery->create()->getCollection()
                        ->addFieldToFilter('product_id', $modelDealParent['product_id']);
                    foreach ($modelDelivery as $delivery) {
                        $resultDelivery[] = $delivery->getData();
                    }
                    $data['address']['delivery'] = $resultDelivery;
                }
                $modelDealParent = array_merge($modelDealParent, $data);
                $result['parent'] = $this->_objectFactory->addData($modelDealParent);

//                $result['parent']['term'] = $resultTerm;
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @param $customer_id
     * @return array|null
     */
    public function getTicketDetail($id, $customer_id)
    {
        $result = null;
        $model = $this->_groupon->create()->getCollection()
            ->addFieldToFilter('groupon_id', $id)
            ->addFieldToFilter('customer_id', $customer_id)
            ->getFirstItem();
        if ($model->getId()) {
            $result[] = $model->getData();
        }

        return $result;
    }

    /**
     * get list ticket by email customer
     * @param $customer_id
     * @param $size
     * @return array|null
     */
    public function getTicketByCustomerIdDetail($customer_id, $size)
    {
        $result = null;
        $model = $this->_groupon->create()->getCollection()
            ->addFieldToFilter('customer_id', $customer_id)
            ->setPageSize(20)
            ->setCurPage($size);
        if (count($model) > 0) {
            foreach ($model as $ticket) {
                $result[] = $ticket->getData();
            };
        }

        return $result;
    }

    /**
     * @param $id
     * @param $customer_id
     * @return array|null
     */
    public function getCouponDetailsById($customerId, $couponId)
    {
        $result = [];
        $couponDetails = $this->_groupon->create()->getCollection()
            ->addFieldToFilter('groupon_id', $couponId)
            ->addFieldToFilter('customer_id', $customerId)
            ->getFirstItem();
        $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($couponDetails->getProductId());
        if (isset($parentId[0])) {
            $product = $this->_productFactory->create()->load($parentId[0]);
            $productName = $product->getName();
            $produrUrl = $product->getProductUrl();
            $couponData = $couponDetails->getData();
            $couponData['product_id'] = $parentId[0];
            $couponData['product_name'] = $productName;
            $couponData['product_url'] = $produrUrl;
            $result['coupon_data'] = $couponData;
            $result['coupon_location'] = $this->getCouponLocationDetail($parentId[0]);
            $result['coupon_term'] = $this->getCouponTerms($parentId[0]);
            $vendorId = $couponData['vendor_id'];
            $productId = $product->getEntityId();
            $productIds = $this->checkCustomerReview($customerId);
            $rating_option = null;
            if(!in_array($productId,$productIds)){
                if($this->ratingOptionHelper->checkVendorAllowRating($vendorId)){
                    $rating_option = $this->ratingOptionHelper->getRatingOption($product);
                }
            }
            $result['rating_option'] = $rating_option;

        }
        return $result;
    }
    public function checkCustomerReview($customerId){
        $reviewCollection = $this->_reviewFactory->create()->getCollection();
        $reviewCollection->addStoreFilter($this->modelStoreManagerInterface->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['eq' => 7])
            ->addCustomerFilter($customerId)
            ->setDateOrder()
            ->addRateVotes();
        $dataTmp = [];
        foreach ($reviewCollection->getItems() as $items) {
            $dataTmp[] = $items->getEntityPkValue();
        }
        return $dataTmp;
    }

    public function getVendorCouponDetailsByCode($vendorId, $grouponCode)
    {
        $result = [];
        $couponDetails = $this->_groupon->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('redemption_code', $grouponCode)
            ->getFirstItem();
        $parentId = $this->_catalogProductTypeConfigurable->getParentIdsByChild($couponDetails->getProductId());
        if (isset($parentId[0])) {
            $product = $this->_productFactory->create()->load($parentId[0]);
            $couponData = $couponDetails->getData();
            $couponData['product_name'] = $product->getName();
            $modelDealParent = $this->_deal->create()->getCollection()
                ->addFieldToFilter('product_id', $parentId[0])->getFirstItem()->getData();
            $expired = $modelDealParent['groupon_expire'];
            $redeem = $couponDetails['redeemed_at'];
            $couponData['groupon_expire'] = $expired;
            if ($expired) {
                $couponData['message'] = 'Expired:  ' . "\n " . date('D, M j, Y', strtotime($expired));
            } elseif ($redeem) {
                $couponData['message'] = 'Redeemed: ' . "\n " . $this->formatDate($redeem);
            }
            $result['coupon_data'] = $couponData;
        }
        return $result;
    }

    public function getCouponLocationDetail($productId)
    {
        if ($productId > 0) {
            $modelDealParent = $this->_deal->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData();
            $data = [];
            if ($modelDealParent['is_online_coupon'] == 0) {
                $locationIds = explode(",", $modelDealParent['location_ids']);
                $resultLocation = null;
                $modelLocation = $this->_location->create()->getCollection()
                    ->addFieldToFilter('location_id', ['in' => $locationIds]);

                $data['address'] = [];
                $number = 0;
                foreach ($modelLocation as $location) {
                    $resultLocation[$number] = $location->getData();
                    $businessHour = json_decode($location->getData('business_hour'));
                    $businessHourData = [];
                    $timeData = [];
                    $k = 0;
                    if($businessHour != null){
                        foreach ($businessHour as $key => $item) {
                            $businessHourData[$k]['day'] = $key;
                            $k2 = 0;

                            foreach ($item as $timeSlots) {
                                $fromTo = [];
                                foreach ($timeSlots as $keyTimeSlot => $timeSlot){

                                    $fromTo[$keyTimeSlot] = $timeSlot;

                                }
                                $timeData[$k2] = $fromTo;
                                $k2++;
                            }

                            $businessHourData[$k]['timeSlot'] = $timeData;
                            $k++;
                        }
                    }
                    $resultLocation[$number]['business_hour'] = $businessHourData;
                    $number++;

                }
                $data['address']['location'] = $resultLocation;
                $resultDelivery = null;
                if ($modelDealParent['is_delivery_address'] == 1) {
                    $dealIds = explode(",", $modelDealParent['delivery_ids']);
                    $modelDelivery = $this->_delivery->create()->getCollection()
                        ->addFieldToFilter('delivery_id', ['in' => $dealIds]);
                    foreach ($modelDelivery as $delivery) {
                        $resultDelivery[] = $delivery->getData();
                    }
                }
                $data['address']['delivery'] = $resultDelivery;
            }
            return $data;
        }
    }

    public function getCouponTerms($productId)
    {
        $modelDealParent = $this->_deal->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData();

        $customerSpecifyLocation = ($modelDealParent['is_online_coupon'] == 0) ? $modelDealParent['customer_specify_location'] : 0;

        $term = json_decode(($modelDealParent['term']), true);
        $term = (array)$term;
        if ($term) {
            if (@$term['reservation']['enable'] === '1' || @$term['reservation']['enable'] === 1) {
                if (isset($data)) {
                    $data .= "\n - Reservation required " . @$term['reservation']['days'] . " days in advance.";
                } else {
                    $data = "Reservation required " . @$term['reservation']['days'] . " days in advance.";
                }
            }
            if (@$term['combine'] === '1' || @$term['combine'] == 1) {
                if (isset($data)) {
                    $data .= "\n - May combine coupons for greater value.";
                } else {
                    $data = "May combine coupons for greater value.";
                }
            }
            if (@$term['age_limit']['enable'] == 1 || @$term['age_limit']['enable'] === '1') {
                if (isset($data)) {
                    $data .= "\n - No coupon required for kids below " . @$term['age_limit']['age_number'] . " years of age.";
                } else {
                    $data = "No coupon required for kids below " . @$term['age_limit']['age_number'] . " years of age.";
                }
            }
            if (@$term['sharing'] == 1 || @$term['sharing'] === '1') {
                if (isset($data)) {
                    $data .= "\n - Coupons cannot be share.";
                } else {
                    $data = "Coupons cannot be share.";
                }
            }
            if (@$term['vendor_specify'] && trim($term['vendor_specify']) !== "") {
                if (isset($data)) {
                    $data .= "\n - " . $term['vendor_specify'];
                } else {
                    $data = $term['vendor_specify'];
                }
            }
        }

        if ($customerSpecifyLocation) {
            if (isset($data)) {
                $data .= "\n - " . 'Customer need to specify a redemption location on purchase';
            } else {
                $data = "\n - " . 'Customer need to specify a redemption location on purchase';
            }
        }
        return @$data;
    }

    /**
     * @return string
     */
    public function formatDate($date)
    {
        $date1 = date('D, M j, Y', strtotime($date));
        $time = date('g:i A', strtotime($date));
        return $date1 . ' at ' . $time;

    }

    public function getGrouponInfo($storeId, $productId){
        $result = null;
        /** @var \Magenest\Groupon\Model\Deal $modeDeal */
        $modeDeal = $this->_deal->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem();
        $result = [];
        if($modeDeal->getDealId()){
            $dealId = $modeDeal->getDealId();
            $modelDealParent = $this->_deal->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData();
            $result['child'] = $this->_objectFactory->addData([
                'minimum_buyers_limit' => $modeDeal->getMinimumBuyersLimit(),
                'maximum_buyers_limit' => $modeDeal->getMaximumBuyersLimit(),
            ]);
            $resultTerm = null;
            $a = $modelDealParent['term'];
            $term = $this->getCouponTerms($productId);

            $modelDealParent['term'] = $term;
            $modelDealParent['groupon_start'] = $modelDealParent['redeemable_after'];
            $modelDealParent['groupon_expired'] = $modelDealParent['groupon_expire'];
            $result['parent'] = $modelDealParent;
            $data = [];
            if ($modelDealParent['is_online_coupon'] == 0) {
                $locationIds = $modelDealParent['location_ids'];
                $resultLocation = null;
                if($locationIds != null){
                    $location_id = explode(',', $locationIds);
//                    array_shift($location_id);
                    $modelLocation = $this->_location->create()->getCollection()
                        ->addFieldToFilter('location_id', ['IN' => $location_id]);
                    foreach ($modelLocation as $location){
                        $locationData = $location->getData();
                        $businessHour = json_decode($locationData['business_hour']);
                        $k = 0;
                        if($businessHour != null){
                            $businessHourData = [];
                            foreach ($businessHour as $key => $item) {
                                $businessHourData[$k]['day'] = $key;
                                $k2 = 0;
                                $timeData = [];
                                foreach ($item as $timeSlots) {
                                    $fromTo = [];
                                    foreach ($timeSlots as $keyTimeSlot => $timeSlot){
                                        $fromTo[$keyTimeSlot] = $timeSlot;
                                    }
                                    $timeData[$k2] = $fromTo;
                                    $k2++;
                                }
                                $businessHourData[$k]['timeSlot'] = $timeData;
                                $k++;
                            }
                            $locationData['business_hour'] = $businessHourData;
                        }
                        $resultLocation[] = $locationData;
                    }
                }
                $data['location_address'] = $resultLocation;
                $deliveryIds = $modelDealParent['delivery_ids'];
                $resultDelivery = null;
                if($deliveryIds != null){
                    $delivery_id = explode(',', $locationIds);
                    $modelDelivery = $this->_delivery->create()->getCollection()
                        ->addFieldToFilter('delivery_id', $delivery_id);
                    foreach ($modelDelivery as $delivery) {
                        $resultDelivery[] = $delivery->getData();
                    }
                }
                $data['delivery_address'] = $resultDelivery;
            }
            $data['customer_specify_location'] = $modelDealParent['customer_specify_location'];
            $modelDealParent = array_merge($modelDealParent, $data);
            $result['parent'] = $this->_objectFactory->addData($modelDealParent);
        }
        return $result;
    }
    public function getAllBoughtGroupon($grouponIds){
        $groupons = $this->_groupon->create()->getCollection()->addFieldToFilter('product_id',['in',$grouponIds]);
        $bought = 0;
        /** @var \Magenest\Groupon\Model\Groupon $groupon */
        foreach ($groupons as $groupon){
            if($groupon->getQty()) $bought += $groupon->getQty();
        }
        return $bought;
    }
}

