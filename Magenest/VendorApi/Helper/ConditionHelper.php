<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper;

use Magenest\Groupon\Model\TermFactory;
use Magento\CheckoutAgreements\Model\AgreementFactory;
use Magento\Framework\DataObject;
use Magento\Paypal\Model\Billing\AgreementFactory as AgreementBillingFactory;
use Ves\All\Model\LicenseFactory;
use Unirgy\SimpleLicense\Model\LicenseFactory as UnirgyLicense;

class ConditionHelper
{
    /**
     * @var $_paypalFactory AgreementBillingFactory
     */
    protected $_paypalFactory;

    /**
     * @var $_checkoutFactory AgreementFactory
     */
    protected $_checkoutFactory;

    /**
     * @var $_grouponFactory TermFactory
     */
    protected $_grouponFactory;

    /**
     * @var $_unirgyLicenseFactory UnirgyLicense
     */
    protected $_unirgyLicenseFactory;

    /**
     * @var $_vesLicense LicenseFactory
     */
    protected $_vesLicense;

    /**
     * @var DataObject
     */
    protected $_objectFactory;

    /**
     * ConditionHelper constructor.
     * @param AgreementBillingFactory $agreementFactory
     * @param AgreementFactory $agreementCheckoutFactory
     * @param TermFactory $termFactory
     * @param DataObject $dataObject
     * @param UnirgyLicense $licenseFactory
     * @param LicenseFactory $vesLicenseFactory
     */
    public function __construct(
        AgreementBillingFactory $agreementFactory,
        AgreementFactory $agreementCheckoutFactory,
        TermFactory $termFactory,
        DataObject $dataObject,
        UnirgyLicense $licenseFactory,
        LicenseFactory $vesLicenseFactory)
    {
        $this->_vesLicense = $vesLicenseFactory;
        $this->_unirgyLicenseFactory = $licenseFactory;
        $this->_grouponFactory = $termFactory;
        $this->_checkoutFactory = $agreementCheckoutFactory;
        $this->_paypalFactory = $agreementFactory;
        $this->_objectFactory = $dataObject;
    }

    public function getAllTermAndConditions()
    {
        $billingAgreementCollection = $this->_paypalFactory->create()->getCollection();
        $billings = [];
        foreach ($billingAgreementCollection as $billing) {
            array_push($billings, $billing->getData());
        }

        $checkoutCollection = $this->_checkoutFactory->create()->getCollection();
        $checkouts = [];
        foreach ($checkoutCollection as $checkout) {
            array_push($checkouts, $checkout->getData());
        }

        $grouponCollection = $this->_grouponFactory->create()->getCollection();
        $groupons = [];
        foreach ($grouponCollection as $groupon) {
            array_push($groupons, $groupon->getData());
        }

        return [
            'groupon' => @$groupons,
            'checkout' => @$checkouts,
            'paypal' => @$billings
        ];
    }

    public function getAllLicenses()
    {
        $unirgyCollections = $this->_unirgyLicenseFactory->create()->getCollection();
        $unirgyLicense = [];
        foreach ($unirgyCollections as $unirgy) {
            array_push($unirgyLicense, $unirgy->getData());
        }

        $vesCollections = $this->_vesLicense->create()->getCollection();
        $vesLicense = [];
        foreach ($vesCollections as $ves) {
            array_push($vesLicense, $ves->getData());
        }

        return [
            'ves' => $vesLicense,
            'unirgy' => $unirgyLicense
        ];
    }

}