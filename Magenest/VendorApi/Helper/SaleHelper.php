<?php

namespace Magenest\VendorApi\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;

class SaleHelper
{
    /** @var ProductFactory */
    protected $productFactory;

    /** @var  StoreManagerInterface */
    protected $storeManager;

    function __construct(
        ProductFactory $productFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
    }


    /**
     * Get sale history
     *
     * @param \Magento\Sales\Model\Order\Shipment $entity
     * @return array
     */
    function getShipmentHistory($entity)
    {

        $result = [];

        /** @var \Magento\Sales\Model\Order\Shipment\Comment $comment */
        foreach ($entity->getCommentsCollection() as $comment) {
            $data = $comment->getData();
            array_push($result, [
                'comment' => $data['comment'],
                'created_at' => $data['created_at'],
                'is_customer_notified' => $data['is_customer_notified'],
                'is_visible_to_vendor' => $data['is_visible_to_vendor'],
                'status' => $data['udropship_status'],
                'username' => $data['username']
            ]);
        }

        return $result;
    }

    /**
     * Get shipment item
     *
     * @param \Magento\Sales\Model\Order\Shipment $entity
     * @return array
     */
    function getShipmentItems($entity)
    {
        $allItems = $entity->getAllItems();
        $orderIncrementId = $entity->getIncrementId();
        $result = [];

        /** @var \Magento\Sales\Model\Order\Shipment\Item $item */
        foreach ($allItems as $item) {
            $itemData = $item->getData();
            $orderItemData = $item->getOrderItem()->getData();

            foreach ($orderItemData['product_options']['additional_options'] as $option) {
                if ($option['label'] === 'Vendor') {
                    $vendor = $option['value'];
                }
            }

            $product = $this->productFactory->create()->load($itemData['product_id']);
            $productImage = $optionText = '';

            if ($product->getId()) {
                $product_image = $product->getData('image');

                if ($product_image) {
                    $productImage = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product_image;
                }

                $brandId = $product->getData('brand');
                $attributeId = $product->getResource()->getAttribute('brand');
                if ($attributeId->usesSource()) {
                    $data = $attributeId->getSource()->getAttribute()->getData();
                    if ($brandId) {
                        $optionText = $attributeId->getSource()->getOptionText($brandId);
                    } else {
                        $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
                    }
                }
            }

            $result[] = [
                'item_id' => $itemData['entity_id'],
                'order_item_id' => $itemData['order_item_id'],
                'order_increment_id' => $orderIncrementId,
                'vendor' => @$vendor === null ? $orderItemData['udropship_vendor'] : $vendor,
                'vendor_id' => $orderItemData['udropship_vendor'],
                'item_name' => $itemData['name'],
                'item_sku' => $itemData['sku'],
                'price' => $itemData['price'],
                'row_total' => $itemData['row_total'],
                'qty' => $item->getQty(),
                'product_image' => $productImage,
                'product_id' => $itemData['product_id'],
                'brand' => $optionText
            ];
        }

        return $result;
    }

    /**
     * Get shipment information
     *
     * @param \Magento\Sales\Model\Order\Shipment $entity
     * @return array
     */
    public function getShipmentDetailsInformation($entity)
    {
        $order = $entity->getOrder();
        $paymentsOrder = $order->getPayment()->getAdditionalInformation();

        return [
            'billing_address' => $order->getBillingAddress()->getData(),
            'shipping_address' => $order->getShippingAddress()->getData(),
            'payment_method' => $paymentsOrder['method_title'],
            'shiping_method' => $order->getShippingMethod()
        ];
    }
}