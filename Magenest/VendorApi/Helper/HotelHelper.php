<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Helper;

use Magento\Catalog\Helper\ImageFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\UrlInterface;

/**
 * Class HotelHelper
 * @package Magenest\VendorApi\Helper
 */
class HotelHelper
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ImageFactory
     */
    protected $_imageFactory;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Reports\Model\ResourceModel\Report\Collection\Factory
     */
    protected $_reportFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ResourceModel\Review\CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $_searchCriteriaBuilder;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * @var \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var \Unirgy\DropshipVendorRatings\Helper\Data
     */
    protected $_vendorReviewHelper;

    /**
     * @var \Magento\Review\Model\RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var \Magento\Review\Model\Rating\Option\VoteFactory
     */
    protected $_voteFactory;

    /**
     * @var \Magenest\Hotel\Model\BedTypeFactory
     */
    protected $_bedType;

    /**
     * @var \Magenest\Hotel\Model\HotelOrderFactory
     */
    protected $_hotelOrder;

    /**
     * @var \Magenest\Hotel\Model\HotelPropertyFactory
     */
    protected $_hotelProperty;

    /**
     * @var \Magenest\Hotel\Model\PackageAndServiceFactory
     */
    protected $_packageService;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packageDetail;

    /**
     * @var \Magenest\Hotel\Model\PackageDetailsFactory
     */
    protected $_packagePrice;

    /**
     * @var \Magenest\Hotel\Model\RoomAmenitiesAdminFactory
     */
    protected $_roomAmenitiesAmin;

    /**
     * @var \Magenest\Hotel\Model\RoomAmenitiesVendorFactory
     */
    protected $_roomAmenitiesVendor;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeAdminFactory
     */
    protected $_roomTypeAdmin;

    /**
     * @var \Magenest\Hotel\Model\RoomTypeVendorFactory
     */
    protected $_roomTypeVendor;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\CollectionFactory
     */
    protected $_serviceCollectionFactory;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_objectFactory;

    /**
     * @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory
     */
    protected $_facilCollectionFactory;

    /**
     * HotelHelper constructor.
     * @param \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory
     * @param \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory
     * @param \Magento\Store\Model\StoreFactory $storeManager
     * @param \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param ImageFactory $imageFactory
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory
     * @param \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory
     * @param \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory
     * @param \Magenest\Hotel\Model\PackageAndServiceFactory $packageAndServiceFactory
     * @param \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory
     * @param \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory
     * @param \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $roomAmenitiesAdminFactory
     * @param \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenitiesVendorFactory
     * @param \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory
     * @param \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory
     * @param \Magento\Framework\DataObject $objectFactory
     * @param \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $facilCollectionFactory
     */
    public function __construct(
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $vendorReviewHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Eav\Api\AttributeRepositoryInterface $productAttributeFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewCollectionFactory,
        \Magento\Store\Model\StoreFactory $storeManager,
        \Magento\Reports\Model\ResourceModel\Report\Collection\Factory $reportFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Catalog\Helper\ImageFactory $imageFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magenest\Hotel\Model\BedTypeFactory $bedTypeFactory,
        \Magenest\Hotel\Model\HotelOrderFactory $hotelOrderFactory,
        \Magenest\Hotel\Model\HotelPropertyFactory $hotelPropertyFactory,
        \Magenest\Hotel\Model\PackageAndServiceFactory $packageAndServiceFactory,
        \Magenest\Hotel\Model\PackageDetailsFactory $packageDetailsFactory,
        \Magenest\Hotel\Model\PackagePriceFactory $packagePriceFactory,
        \Magenest\Hotel\Model\RoomAmenitiesAdminFactory $roomAmenitiesAdminFactory,
        \Magenest\Hotel\Model\RoomAmenitiesVendorFactory $roomAmenitiesVendorFactory,
        \Magenest\Hotel\Model\RoomTypeAdminFactory $roomTypeAdminFactory,
        \Magenest\Hotel\Model\RoomTypeVendorFactory $roomTypeVendorFactory,
        \Magento\Framework\DataObject $objectFactory,
        \Magenest\Hotel\Model\ResourceModel\PackageAndService\CollectionFactory $serviceCollectionFactory,
        \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\CollectionFactory $facilCollectionFactory
    ) {
        $this->_voteFactory = $voteFactory;
        $this->_vendorReviewHelper = $vendorReviewHelper;
        $this->_vendorFactory = $vendorFactory;
        $this->_catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->_eavConfig = $eavConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productAttributeFactory = $productAttributeFactory;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_reportFactory = $reportFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_imageFactory = $imageFactory;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_bedType = $bedTypeFactory;
        $this->_hotelOrder = $hotelOrderFactory;
        $this->_hotelProperty = $hotelPropertyFactory;
        $this->_packageService = $packageAndServiceFactory;
        $this->_packageDetail = $packageDetailsFactory;
        $this->_packagePrice = $packagePriceFactory;
        $this->_roomAmenitiesAmin = $roomAmenitiesAdminFactory;
        $this->_roomAmenitiesVendor = $roomAmenitiesVendorFactory;
        $this->_roomTypeAdmin = $roomTypeAdminFactory;
        $this->_roomTypeVendor = $roomTypeVendorFactory;
        $this->_objectFactory = $objectFactory;
        $this->_serviceCollectionFactory = $serviceCollectionFactory;
        $this->_facilCollectionFactory = $facilCollectionFactory;
    }

    /**
     * @param $storeId
     * @param $size
     * @return array|null
     */
    public function getListHotelDetail($storeId, $size)
    {
        $result = null;
        $listHotels = $this->_productFactory->create()->setStoreId($storeId)->getCollection()
            ->addFieldToFilter('type_id', 'hotel')
            ->addFieldToFilter('status', 1)
            ->setPageSize(20)
            ->setCurPage($size);
        if (count($listHotels) > 0) {
            foreach ($listHotels as $listHotel) {
                $result[] = $this->getHotelDetail($storeId, $listHotel->getEntityId());
            }
        }

        return $result;
    }

    /**
     * collection data of each groupon
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getHotelDetail($storeId, $productId)
    {
        $result = null;
        $currentProduct = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
        if ($currentProduct->getTypeId() == 'hotel') {
            $price = $currentProduct->getPrice();
            $finalPrice = $currentProduct->getFinalPrice();
            $path = $this->_storeManager->create()->load($storeId)->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $productImage = null;
            if ($currentProduct->getImage() != null && strlen($currentProduct->getImage()) > 0) {
                if (substr($currentProduct->getImage(), 0, 1) == '/') {
                    $productImage = $path . 'catalog/product' . $currentProduct->getImage();
                } else {
                    $productImage = $path . 'catalog/product/' . $currentProduct->getImage();
                }
            }

            $this->_reviewFactory->create()->getEntitySummary($currentProduct, $storeId);
            $ratingSummary = $currentProduct->getRatingSummary()->getRatingSummary();
            $ratingCount = $currentProduct->getRatingSummary()->getReviewsCount();
            $vendorId = key($currentProduct->getMultiVendorData());
            $result[] = [
                'id' => $currentProduct->getEntityId(),
                'name' => $currentProduct->getName(),
                'image' => $productImage,
                'sku' => $currentProduct->getSku(),
                'rating' => $ratingSummary,
                'reviews_count' => $ratingCount,
                'price' => $price,
                'final_price' => $finalPrice,
                'vendor_id' => $vendorId,
                'vendor_data' => $this->getVendorInfo($currentProduct, $vendorId),
                'hotel_data' => $this->getHotelData($storeId, $productId)
            ];
        }

        return $result;
    }

    /**
     * @param $storeId
     * @param $productId
     * @return array|null
     */
    public function getHotelData($storeId, $productId)
    {
        $result = null;
        $model = $this->_packageDetail->create()->load($productId, 'product_id')->getData();

        if (!empty($model)) {
            $modelRoomAdmin = $this->_roomTypeAdmin->create()->load(@$model['room_type']);
            $result['room_type_name'] = $modelRoomAdmin->getRoomType();
            $result['hotel_property'] = $this->_objectFactory->addData($this->getHotelProperty($storeId, @$model['vendor_id']));
            $result['hotel_facilities'] = $this->getHotelFacil(@$model['vendor_id']);
            $result['room_types'] = $this->getRoomTypes(@$model['vendor_id']);;
            $result['room_amenities'] = $this->getRoomAmenities(@$model['room_type']);;
            $result['offer_services'] = $this->getPropertyServices(@$model['id']);
            $result['package_offers'] = $this->getPackageOffers(@$model['vendor_id'], $productId);
        }

        return $result;
    }

    public function getHotelFacil($vendorId)
    {
        /** @var \Magenest\Hotel\Model\ResourceModel\PropertyFacilityVendor\Collection $collection */
        $collection = $this->_facilCollectionFactory->create();
        $collection->addVendorToFilter($vendorId)->addFacilDetails();
        return $collection->getData();
    }

    /**
     * @param $storeId
     * @param $vendorId
     * @return mixed|null
     */
    public function getHotelProperty($storeId, $vendorId)
    {
        $path = $this->_storeManager->create()->load($storeId)->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        $modePrDetail = $this->_hotelProperty->create()->load($vendorId, 'vendor_id');
        if ($modePrDetail->getId()) {
            $photos = json_decode($modePrDetail->getPhotos());
            $image = null;
            if (count($photos) > 0) {
                foreach ($photos as $photo) {
                    $image[] = $path . 'hotel/property/images' . $photo;
                }
            }
            $result = $modePrDetail->getData();
            unset($result['photos']);
            $result['photos'] = $image;
            return $result;
        }
        return null;
    }

    /**
     * @param $vendorId
     * @return mixed
     */
    public function getRoomTypes($vendorId)
    {
        $model = $this->_roomTypeVendor->create()->load($vendorId, 'vendor_id');

        return $model->getData();
    }

    /**
     * @param $roomTypeId
     * @return array
     */
    public function getRoomAmenities($roomTypeId)
    {
        $data = $this->_roomAmenitiesVendor->create()->getCollection()
            ->getAmenitiesData($roomTypeId);
        $mappedData = array_map(function ($e) {
            return @$e['name'];
        }, $data);
        return $mappedData;
    }

    public function getPropertyServices($packageId)
    {
        $result = null;
        /** @var \Magenest\Hotel\Model\ResourceModel\PackageAndService\Collection $collection */
        $collection = $this->_serviceCollectionFactory->create();
        $collection->addPackageIdToFilter($packageId)
            ->moreDetails();
        return $collection->getData();
    }

    /**
     * @param $vendorId
     * @return array|null
     */
    public function getPackageOffers($vendorId, $productId)
    {
        $result = null;
        $modelPackage = $this->_packageDetail->create()->getCollection()
            ->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData();
//        foreach ($modelPackage as $pakage) {
//            $result[] = $pakage->getData();
//        }
        if ($modelPackage) {
            $modelPrice = $this->_packagePrice->create()->getCollection()
                ->addFieldToFilter('package_id', $modelPackage['id']);
            $resultPrice = [];
            foreach ($modelPrice as $price) {
                $resultPrice[] = $price->getData();
            }

            $modelPackage['price_option'] = $resultPrice;
        }

        $result[] = $modelPackage;

        return $result;
    }

    /**
     * @param $product
     * @param $vendorId
     * @return null
     */
    public function getVendorInfo($product, $vendorId)
    {
        $result = null;
        if ($product->getMultiVendorData() && $product->getMultiVendorData() != null) {
            $data = $product->getMultiVendorData();
            $result = $data[$vendorId];
        }

        return $result;
    }
}
