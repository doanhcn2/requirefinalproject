<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Helper;

/**
 * Class QuoteItem
 * @package Magenest\QuickBooksOnline\Helper
 */
class QuoteItem
{

    public function __construct(
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\DataObject $dataObject
    ) {
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->product = $product;
        $this->quoteRepository = $quoteRepository;
        $this->dataObject = $dataObject;
    }

    /**
     * add item to cart
     */
    public function createItem()
    {
//        $productId =27578;
        $params = [
            'sku'=> '27578',
            'qty'=> 1,
            'quote_id'=> 13,
            'is_deleted' => null
        ];
        $data = $this->dataObject->addData($params);
//        //Load the product based on productID
//        $_product = $this->product->load($productId);
//        $this->cart->addProduct($_product, $params);
//        $this->cart->save();

        /** @var \Magento\Quote\Model\Quote $quote */
        $cartId = 13;
        $quote = $this->quoteRepository->get($cartId);
        $item = $this->product->load(27578);
//        $quoteItems = $quote->getItems();
//        $quoteItems[] = $data;
//        $quote->setItems($quoteItems);
//        $this->quoteRepository->save($quote);
        $quote->addProduct($item, 2);
        $quote->save();
        $quote->collectTotals();
        return 13213213;
    }
}
