<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\VendorApi\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Psr\Log\LoggerInterface;

/**
 * Class Index
 * @package Magenest\VendorApi\Controller\Index
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $loggerInterface
    ) {
        $this->logger = $loggerInterface;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * View my ticket
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $tempOrder=[
            'currency_id'  => 'USD',
            'email'        => 'gialam.pham1993@gmail.com', //buyer email id
            'shipping_address' =>[
                'firstname'    => 'jhon', //address Details
                'lastname'     => 'Deo',
                'street' => 'xxxxx',
                'city' => 'xxxxx',
                'country_id' => 'IN',
                'region' => 'xxx',
                'postcode' => '43244',
                'telephone' => '52332',
                'fax' => '32423',
                'save_in_address_book' => 1
            ],
            'shipping_amount' => 10,
            'tax' => 5,
            'items'=> [ //array of product which order you want to create
                ['product_id'=>'27578','qty'=>2]
            ]
        ];
        $params = [
            'sku' => '27578',
            'qty' => 2,
            'quote_id' => 13
        ];
        $quoteOrder = $this->_objectManager->create('Magenest\VendorApi\Helper\QuoteItem')->createItem();
        echo 111111;
    }
}
