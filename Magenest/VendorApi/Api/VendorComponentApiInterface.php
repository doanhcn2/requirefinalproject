<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api;


use Magenest\VendorApi\Api\Data\VendorComponent\QuestionParamsInterface;


interface VendorComponentApiInterface
{
    /**
     * @api
     * @param string $product_id
     * @param string|null $cur_page
     * @param string|null $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\CustomerQuestionInterface
     */
    public function getAllVendorQuestionsByProduct($product_id, $cur_page = null, $size = null);

    /**
     * @api
     * @param string $customerId
     * @param string|null $cur_page
     * @param string|null $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\CustomerQuestionInterface
     */
    public function getAllVendorQuestionsByCustomer($customerId, $cur_page = null, $size = null);


    /**
     * @api
     * @param string $vendor_id
     * @param string|null $cur_page
     * @param string|null $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\CustomerQuestionInterface
     */
    public function getAllVendorQuestions($vendor_id, $cur_page = null, $size = null);

    /**
     * @param string $customerId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\QuestionStatusInterface
     */
    public function askQuestionsForVendor($customerId, $requestParams);

    /**
     * @api
     * @param string|int $customerId
     * @param string|null $cur_page
     * @param string|null $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\CustomerReviewInterface
     */
    public function getMyProductReview($customerId, $cur_page = null, $size = null);

    /**
     * @param string $customerId
     * @param string $productId
     * @param mixed $requestParams
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createProductReview($customerId,$productId,$requestParams);

    /**
     * @param string $customerId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\QuestionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function filterQuestionByCustomerId($customerId,$requestParams);

    /**
     * @param string $customerId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\QuestionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function filterQuestByCustomerId($customerId,$requestParams);

    /**
     * @param string $customerId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\QuestionInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function filterQuesByCusId($customerId);

    /**
     * @param string|int $customerId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\CustomerAskFormInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getFormAskQuestion($customerId);
}