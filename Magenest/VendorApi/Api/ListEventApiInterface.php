<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface ListEventApiInterface
 * @package Magenest\VendorApi\Api
 */
interface ListEventApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\EventTicket\EventApiInterface
     */
    public function getEvent($store, $id);

    /**
     * @api
     * @param string $store
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\EventTicket\EventApiInterface
     */
    public function getListEvent($store, $size);

    /**
     * @api
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\EventTicket\VenueMap\VenueMapApiInterface
     */
    public function getVenueMap($id);

    /**
     * @api
     * @param string $id
     * @param string $product_id
     * @return \Magenest\VendorApi\Api\Data\EventTicket\VenueMap\VenueMapDataApiInterface
     */
    public function getVenueMapData($id, $product_id);

}