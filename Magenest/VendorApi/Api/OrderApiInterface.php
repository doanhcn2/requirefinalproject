<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

interface OrderApiInterface
{
    /**
     * @api
     * @param string $customer_id
     * @param string $cur_page
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\Order\OrderInformationInterface
     */
    public function getAllOrderByCustomer($customer_id, $cur_page, $size);

    /**
     * @api
     * @param string $orderId
     * @return \Magenest\VendorApi\Api\Data\Order\OrderInformationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerOrderByOrderId($orderId);

    /**
     * @param string $customerId
     * @param mixed $requestParams
     * @return mixed
     */
    public function filterCustomerOrder($customerId,$requestParams);
}
