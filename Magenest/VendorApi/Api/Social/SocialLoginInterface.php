<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:19
 */
namespace Magenest\VendorApi\Api\Social;

interface SocialLoginInterface
{
    /**
     * login or create new customer via facebook login
     *
     * @param string[] $userInfo
     * @return mixed
     */
    public function loginOrCreateNewCustomerViaFacebook($userInfo);

    /**
     * login or create new customer via google login
     *
     * @param string[] $userInfo
     * @return mixed
     */
    public function loginOrCreateNewCustomerViaGoogle($userInfo);
}