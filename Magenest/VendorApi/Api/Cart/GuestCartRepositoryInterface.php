<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 06/04/2018
 * Time: 13:43
 */
namespace Magenest\VendorApi\Api\Cart;

interface GuestCartRepositoryInterface{
    /**
     * Enable a guest user to return information for a specified cart.
     *
     * @param string $cartId
     * @return \Magenest\VendorApi\Api\Data\Cart\CartInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($cartId);
}