<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 17/01/2018
 * Time: 17:05
 */
namespace Magenest\VendorApi\Api\Cart;

/**
 * Interface CartManagementInterface
 * @package Magenest\Eplus\Api
 */
interface CartManagementInterface
{

    /**
     * Returns information for the cart for a specified customer.
     *
     * @param int $customerId The customer ID.
     * @return \Magenest\VendorApi\Api\Data\Cart\CartInterface Cart object.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified customer does not exist.
     */
    public function getCartForCustomer($customerId);

}