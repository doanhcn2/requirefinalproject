<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface GrouponApiInterface
 * @package Magenest\VendorApi\Api
 */
interface GrouponApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponApiInterface
     */
    public function getGrouponProduct($store, $id);

    /**
     * @api
     * @param string $store
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponApiInterface
     */
    public function getListGrouponProduct($store, $size);

    /**
     * @api
     * @param string $id
     * @param string $customer_id
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponApiInterface
     */
    public function getTicket($id, $customer_id);

    /**
     * @api
     * @param string $customer_id
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponApiInterface
     */
    public function getTicketByCustomerId($customer_id, $size);

    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponApiInterface
     */
    public function getCouponProduct($store, $id);

    /**
     * @param string $customerId
     * @param mixed $listCoupons
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\RedeemInterface
     */
    public function redeemCoupons($customerId, $listCoupons);
    /**
     * @param string $customerId
     * @param string $couponId
     * @return \Magenest\VendorApi\Api\Data\Groupon\CouponDetailsInterface
     */
    public function getCouponDetail($customerId, $couponId);

    /**
     * @param string $customerId
     * @param mixed $listCouponRedeemCode
     * @return mixed
     */
    public function customerMarkAvailableCoupon($customerId,$listCouponRedeemCode);
}