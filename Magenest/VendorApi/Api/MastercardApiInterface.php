<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/6/18
 * Time: 11:04 AM
 */

namespace Magenest\VendorApi\Api;


interface MastercardApiInterface
{
    /**
     * @param string $cardNumber
     * @param string $expiryMonth
     * @param string $expiryYear
     * @param string $securityCode
     * @return string
     */
    public function sessionize($cardNumber, $expiryMonth, $expiryYear, $securityCode = null);
}
