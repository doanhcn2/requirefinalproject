<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Vendor;

interface VendorApiInterface{
    /**
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorGrouponDashboard
     */
    public function getVendorGrouponDashboard($vendorId);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return mixed
     */
    public function getVendorCampaignDashboard($vendorId,$requestParams);

    /**
     * @param string $vendorId
     * @param string $cur_page
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorFeedbackDashboard
     */
    public function getVendorFeedbackDashboard($vendorId,$cur_page,$size);

    /**
     * @param string $vendorId
     * @param string $status
     * @param string $cur_page
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorQuestionDashboard
     */
    public function getVendorQuestionAnswerDashboard($vendorId,$status,$cur_page,$size);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorManageEventDashboard
     */
    public function getVendorManageEventDashboard($vendorId,$requestParams);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorCampaignDetail
     */
    public function getVendorCampaignDetail($vendorId,$requestParams);

    /**
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorTicketDashboard
     */
    public function getVendorTicketDashboard($vendorId);

    /**
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorDashboard
     */
    public function getVendorProductDashboard($vendorId);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return mixed
     */
    public function getVendorInventoryManage($vendorId,$requestParams);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorEventTicketDetail
     */
    public function getVendorEventTicketDetail($vendorId,$requestParams);

    /**
     * @param string $vendorId
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorTicketSessionDetail
     */
    public function getVendorTicketSessionDetail($vendorId,$requestParams);
    /**
     * @param string $email
     * @return \Magenest\VendorApi\Api\Data\ForgotPasswordInterface
     */
    public function forgotPassword($email);

    /**
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\DefaultInterface
     */
    public function getVendorReturns($vendorId);

    /**
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorPaymentsInterface
     */
    public function getVendorPayments($vendorId);

    /**
     * @param string $offerId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorOfferDetailsInterface
     */
    public function getVendorOfferDetails($offerId);

    /**
     * @param string $vendorId
     * @param string $productId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorInventoryDetailsInterface
     */
    public function getVendorInventoryDetail($vendorId,$productId);

    /**
     * @param string|int $vendorId
     * @param string|int $statementpage
     * @param string|int $statementsize
     * @param string|int $payoutpage
     * @param string|int $payoutsize
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorPaymentInterface
     */
    public function getPaymentsForVendor($vendorId,$statementpage,$statementsize,$payoutpage,$payoutsize);

    /**
     * @param string|int $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorProfileInterface
     */
    public function getVendorProfile($vendorId);

    /**
     * @param string|int $vendorId
     * @param string|int $product_id
     * @param string|int $option_id
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorCampaignBuyOptionDetailInterface
     */
    public function getCampaignBuyOptionDetail($vendorId,$product_id,$option_id);
}