<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Vendor\Ticket;

/**
 * Interface ListEventApiInterface
 * @package Magenest\VendorApi\Api
 */
interface VendorTicketApiInterface
{

    /**
     * @param string $vendorId
     * @param mixed $listTickets
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\RedeemInterface
     */
    public function redeemTickets($vendorId, $listTickets);
    /**
     * @param string $vendorId
     * @param string $ticketCode
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponDetailsInterface
     */
    public function checkTicket($vendorId, $ticketCode);

}