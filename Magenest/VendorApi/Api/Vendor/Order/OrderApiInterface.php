<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Vendor\Order;

interface OrderApiInterface
{
    /**
     * @api
     * @param string $vendorId
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\OrderInformationInterface
     */
    public function getAllOrderByVendor($vendorId, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @api
     * @param string $orderId
     * @param string $vendorId
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\OrderInformationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getVendorOrderByOrderId($orderId, $vendorId);

}
