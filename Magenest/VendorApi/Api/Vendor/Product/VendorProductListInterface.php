<?php
namespace Magenest\VendorApi\Api\Vendor\Product;

interface VendorProductListInterface
{
    /**
     * Collect and retrieve the list of product by vendor
     * This info contains raw prices and formatted prices, product name, stock status, store_id, etc
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @param int $storeId
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria, $storeId);
}