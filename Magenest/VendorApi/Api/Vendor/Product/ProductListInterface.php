<?php
namespace Magenest\VendorApi\Api\Vendor\Product;

interface ProductListInterface{
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @param string|int $storeId
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria, $storeId);
}