<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Vendor\Groupon;

/**
 * Interface ListEventApiInterface
 * @package Magenest\VendorApi\Api
 */
interface VendorGrouponApiInterface
{

    /**
     * @param string $vendorId
     * @param string $grouponCode
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponDetailsInterface
     */
    public function checkGroupon($vendorId, $grouponCode);

    /**
     * @param string $vendorId
     * @param mixed $listGroupon
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\RedeemInterface
     */
    public function redeemGroupons($vendorId, $listGroupons);

}