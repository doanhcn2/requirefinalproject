<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface ListEventApiInterface
 * @package Magenest\VendorApi\Api
 */
interface TicketApiInterface
{
    /**
     * @api
     * @param string $id
     * @param string $customer_id
     * @return \Magenest\VendorApi\Api\Data\EventTicket\TicketApiInterface
     */
    public function getTicket($id, $customer_id);

    /**
     * @api
     * @param string $email
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\EventTicket\TicketApiInterface
     */
    public function getTicketByEmail($email, $size);

    /**
     * @api
     * @param string $id
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\EventTicket\TicketApiInterface
     */
    public function getTicketByProduct($id, $size);

    /**
     * @param string $customerId
     * @param string $cur_page
     * @param string $size
     * @param string $status
     * @return \Magenest\VendorApi\Api\Data\EventTicket\TicketApiInterface
     */
    public function getAllTicketByCustomerId($customerId,$cur_page,$size,$status);

    /**
     * @param string $customerId
     * @param mixed $listTickets
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\RedeemInterface
     */
    public function redeemTickets($customerId, $listTickets);

    /**
     * @param string $customerId
     * @param string $ticketId
     * @return \Magenest\VendorApi\Api\Data\EventTicket\TicketDetailsInterface
     */
    public function getTicketDetail($customerId, $ticketId);

    /**
     * @param string $customerId
     * @param mixed $listTicketRedeemCode
     * @return mixed
     */
    public function customerMarkAvailableTicket($customerId,$listTicketRedeemCode);
}