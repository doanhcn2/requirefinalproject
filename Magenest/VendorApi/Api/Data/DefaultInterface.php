<?php
namespace Magenest\VendorApi\Api\Data;

interface DefaultInterface
{
    const CORE_PRODUCT = 'result';

    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getResult();

    /**
     * @return int
     */
    public function getApiStatus();
}
