<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 09/04/2018
 * Time: 10:39
 */
namespace Magenest\VendorApi\Api\Data\Review;

interface CustomerReviewVendorsInterface extends \Magento\Framework\Api\ExtensibleDataInterface{
    /**
     * @return int review_id
     */
    public function getReviewId();

    /**
     * @param $review_id
     * @return $this
     */
    public function setReviewId($review_id);
    /**
     * @return string created_at
     */
    public function getCreatedAt();

    /**
     * @param $created_at
     * @return $this
     */
    public function setCreatedAt($created_at);
    /**
     * @return int entity_id
     */
    public function getEntityId();

    /**
     * @param $entity_id
     * @return $this
     */
    public function setEntityId($entity_id);
    /**
     * @return int entity_pk_value
     */
    public function getEntityPkValue();

    /**
     * @param $entity_pk_value
     * @return $this
     */
    public function setEntityPkValue($entity_pk_value);

    /**
     * @return int status_id
     */
    public function getStatusId();

    /**
     * @param $status_id
     * @return $this
     */
    public function setStatusId($status_id);

    /**
     * @return int rel_entity_pk_value
     */
    public function getRelEntityPkValue();

    /**
     * @param $rel_entity_pk_value
     * @return $this
     */
    public function setRelEntityPkValue($rel_entity_pk_value);

    /**
     * @return int helpfulness_yes
     */
    public function getHelpfulnessYes();

    /**
     * @param $helpfulness_yes
     * @return $this
     */
    public function setHelpfulnessYes($helpfulness_yes);

    /**
     * @return int $helpfulness_no
     */
    public function getHelpfulnessNo();

    /**
     * @param $helpfulness_no
     * @return $this
     */
    public function setHelpfulnessNo($helpfulness_no);

    /**
     * @return string helpfulness_pcnt
     */
    public function getHelpfulnessPcnt();

    /**
     * @param $helpfulness_pcnt
     * @return $this
     */
    public function setHelpfulnessPcnt($helpfulness_pcnt);
    /**
     * @return int detail_id
     */
    public function getDetailId();

    /**
     * @param $detail_id
     * @return $this
     */
    public function setDetailId($detail_id);
    /**
     * @return string title
     */
    public function getTitle();

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string detail
     */
    public function getDetail();

    /**
     * @param $detail
     * @return $this
     */
    public function setDetail($detail);

    /**
     * @return string nickname
     */
    public function getNickname();

    /**
     * @param $nickname
     * @return $this
     */
    public function setNickname($nickname);

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param $customer_id
     * @return $this
     */
    public function setCustomerId($customer_id);

    /**
     * Lists rating votes
     *
     * @return string []
     */
    public function getRatingVotes();
    /**
     * Sets items in the cart.
     *
     * @param string [] $rating_votes
     * @return $this
     */
    public function setRatingVotes($rating_votes);
}