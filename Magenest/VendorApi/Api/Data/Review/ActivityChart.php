<?php
namespace Magenest\VendorApi\Api\Data\Review;

interface ActivityChart{
    /**
     * @return mixed
     */
    public function getAvailable();

    /**
     * @return mixed
     */
    public function getRedeem();

    /**
     * @return mixed
     */
    public function getRefunded();
}