<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Review;

interface FormDataCustomerInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getShipment();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVirtual();
}