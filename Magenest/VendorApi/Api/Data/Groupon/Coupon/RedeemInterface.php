<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon\Coupon;

interface RedeemInterface
{
    const API_STATUS = 'api_status';
    const REDEEM_STATUS = 'redeem_status';
    const REDEEM_MESSAGE = 'redeem_message';

    /**
     * @return string
     */
    public function getApiStatus();

    /**
     * @return string
     */
    public function getRedeemStatus();

    /**
     * @return string
     */
    public function getRedeemMessage();
}