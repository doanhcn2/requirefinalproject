<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon\Coupon\Parent;

/**
 * Interface AddressInterface
 * @package Magenest\VendorApi\Api\Data\Groupon\Coupon\Parent
 */
interface AddressInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const LOCATION = 'location';

    const DELIVERY = 'delivery';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getLocation();

    /**
     * @param array $location
     * @return $this
     */
    public function setLocation($location);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDelivery();

    /**
     * @param array $delivery
     * @return $this
     */
    public function setDelivery($delivery);
}
