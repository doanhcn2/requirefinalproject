<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon\Coupon;

/**
 * Interface ChildInterface
 * @package Magenest\VendorApi\Api\Data\Groupon\Coupon
 */
interface ChildInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const MINIMUM_BUYERS_LIMIT = 'minimum_buyers_limit';

    const MAXIMUM_BUYERS_LIMIT = 'maximum_buyers_limit';

    /**#@-*/

    /**
     * minimum
     *
     * @return int|null
     */
    public function getMinimumBuyersLimit();

    /**
     * Set minimum
     *
     * @param int $minimum
     * @return $this
     */
    public function setMinimumBuyersLimit($minimum);

    /**
     * minimum
     *
     * @return int|null
     */
    public function getMaximumBuyersLimit();

    /**
     * Set minimum
     *
     * @param int $maximum
     * @return $this
     */
    public function setMaximumBuyersLimit($maximum);
}
