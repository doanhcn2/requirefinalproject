<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon\Coupon;

/**
 * Interface ParentInterface
 * @package Magenest\VendorApi\Api\Data\Groupon\Coupon
 */
interface ParentInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const DEAL_ID = 'deal_id';
    const PRODUCT_ID = 'product_id';
    const STORE_ID = 'store_id';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const GROUPON_EXPIRED = 'groupon_expired';
    const GROUPON_START = 'groupon_start';
    const ENABLE = 'enable';
    const VENDOR_ID = 'vendor_id';
    const ALLOW_CREDIT_PAY = 'allow_credit_pay';
    const VIEW_COUNT = 'view_count';
    const PURCHASED_COUNT = 'purchased_count';
    const STATUS = 'status';
    const PRIORITY = 'priority';
    const USER_LIMIT = 'user_limit';
    const QTY = 'qty';
    const AVAILABLE_QTY = 'available_qty';
    const PURCHASED_QTY = 'purchased_qty';
    const IS_BUSINESS_ADDRESS = 'is_business_address';
    const IS_DELIVERY_ADDRESS = 'is_delivery_address';
    const IS_ONLINE_COUPON = 'is_online_coupon';
    const TEMPLATE_ID = 'template_id';
    const TERM = 'term';
    const ORIG_DATA = 'orig_data';
    const ADDRESS = 'address';

    /**
     * deal id
     *
     * @return int|null
     */
    public function getDealId();

    /**
     * Set deal id
     *
     * @param int $id
     * @return $this
     */
    public function setDealId($id);

    /**
     * deal id
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Set deal id
     *
     * @param int $id
     * @return $this
     */
    public function setProductId($id);

    /**
     * store id
     *
     * @return int|null
     */
    public function getStoreId();

    /**
     * Set store id
     *
     * @param int $id
     * @return $this
     */
    public function setStoreId($id);

    /**
     * start time
     *
     * @return string
     */
    public function getStartTime();

    /**
     * Set store id
     *
     * @param string $start
     * @return $this
     */
    public function setStartTime($start);

    /**
     * end time
     *
     * @return string
     */
    public function getEndTime();

    /**
     * Set end time
     *
     * @param string $end
     * @return $this
     */
    public function setEndTime($end);

    /**
     * expired
     *
     * @return string
     */
    public function getGrouponExpired();

    /**
     * Set expired
     *
     * @param string $expired
     * @return $this
     */
    public function setGrouponExpired($expired);


    /**
     * expired
     *
     * @return string
     */
    public function getGrouponStart();

    /**
     * Set expired
     *
     * @param string $expired
     * @return $this
     */
    public function setGrouponStart($expired);

    /**
     * expired
     *
     * @return int
     */
    public function getEnable();

    /**
     * Set expired
     *
     * @param int $enable
     * @return $this
     */
    public function setEnable($enable);

    /**
     * expired
     *
     * @return int
     */
    public function getVendorId();

    /**
     * Set expired
     *
     * @param int $enable
     * @return $this
     */
    public function setVendorId($enable);

    /**
     * expired
     *
     * @return int
     */
    public function getAllowCreditPay();

    /**
     * Set expired
     *
     * @param int $creditPay
     * @return $this
     */
    public function setAllowCreditPay($creditPay);

    /**
     * view count
     *
     * @return int
     */
    public function getViewCount();

    /**
     * Set view count
     *
     * @param int $count
     * @return $this
     */
    public function setViewCount($count);

    /**
     * purchased count
     *
     * @return int
     */
    public function getPurchasedCount();

    /**
     * Set purchased count
     *
     * @param int $purchasedCount
     * @return $this
     */
    public function setPurchasedCount($purchasedCount);

    /**
     * status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set purchased count
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * priority
     *
     * @return int
     */
    public function getPriority();

    /**
     * Set priority
     *
     * @param int $priority
     * @return $this
     */
    public function setPriority($priority);

    /**
     * user limit
     *
     * @return int
     */
    public function getUserLimit();

    /**
     * Set user limit
     *
     * @param int $userLimit
     * @return $this
     */
    public function setUserLimit($userLimit);

    /**
     * user qty
     *
     * @return int
     */
    public function getQty();

    /**
     * Set qty
     *
     * @param int $qty
     * @return $this
     */
    public function setQty($qty);

    /**
     * user available qty
     *
     * @return int
     */
    public function getAvailableQty();

    /**
     * Set available qty
     *
     * @param int $qty
     * @return $this
     */
    public function setAvailableQty($qty);

    /**
     * user purchased qty
     *
     * @return int
     */
    public function getPurchasedQty();

    /**
     * Set purchased qty
     *
     * @param int $qty
     * @return $this
     */
    public function setPurchasedQty($qty);

    /**
     * user is business address
     *
     * @return int
     */
    public function getIsBusinessAddress();

    /**
     * Set is business address
     *
     * @param int $isBusiness
     * @return $this
     */
    public function setIsBusinessAddress($isBusiness);

    /**
     * user is delivery address
     *
     * @return int
     */
    public function getIsDeliveryAddress();

    /**
     * Set is delivery address
     *
     * @param int $isDelivery
     * @return $this
     */
    public function setIsDeliveryAddress($isDelivery);

    /**
     * user is online
     *
     * @return int
     */
    public function getIsOnlineCoupon();

    /**
     * Set is online
     *
     * @param int $isOnline
     * @return $this
     */
    public function setIsOnlineCoupon($isOnline);

    /**
     * template
     *
     * @return int
     */
    public function getTemplateId();

    /**
     * Set template
     *
     * @param int $temp
     * @return $this
     */
    public function setTemplateId($temp);

    /**
     * term
     *
     * @return string
     */
    public function getTerm();

    /**
     * Set term
     *
     * @param string $term
     * @return $this
     */
    public function setTerm($term);

    /**
     * orig
     *
     * @return string
     */
    public function getOrigData();

    /**
     * Set orig
     *
     * @param string $orig
     * @return $this
     */
    public function setOrigData($orig);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getLocationAddress();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDeliveryAddress();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCustomerSpecifyLocation();
}
