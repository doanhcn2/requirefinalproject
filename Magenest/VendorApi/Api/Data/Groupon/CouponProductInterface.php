<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon;

/**
 * Interface CouponProductInterface
 * @package Magenest\VendorApi\Api\Data\Groupon
 */
interface CouponProductInterface
{
    /**
     * define array value
     */
    const CHILD = 'child';

    const PARENT = 'parent';

    /**
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\ChildInterface
     */
    public function getChild();

    /**
     * @param array $child
     * @return $this
     */
    public function setChild($child);

    /**
     * @return \Magenest\VendorApi\Api\Data\Groupon\Coupon\ParentInterface
     */
    public function getParent();

    /**
     * @param array $parent
     * @return $this
     */
    public function setParent($parent);
}
