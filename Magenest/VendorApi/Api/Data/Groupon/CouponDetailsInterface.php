<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon;
/**
 * Interface CouponDetailsInterface
 * @package Magenest\VendorApi\Api\Data\Groupon\Coupon
 */
interface CouponDetailsInterface
{
    /**
     * @return string
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCouponData();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCouponLocation();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCouponTerm();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRatingOption();
}