<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Groupon;

/**
 * Interface GrouponApiInterface
 * @package Magenest\VendorApi\Api\Data\Groupon
 */
interface GrouponApiInterface
{
    const API_STATUS = 'api_status';
    const GROUPON_PRODUCT = 'groupon_product';
    const LIST_GROUPON_PRODUCT = 'list_groupon_product';
    const COUPON_PRODUCT = 'coupon_product';
    const TICKET = 'ticket';
    const TICKET_BY_CUSTOMER_ID = 'ticket_by_customer_id';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGrouponProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getListGrouponProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCouponProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicket();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketByCustomerId();

    /**
     * @return string
     */
    public function getApiStatus();
}
