<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 10/04/2018
 * Time: 14:20
 */
namespace Magenest\VendorApi\Api\Data\Groupon;

interface GrouponCustomerInformationInterface
{
    /**
     * @return string $groupon_id
     */
    public function getGrouponId();

    /**
     * @param $groupon_id
     * @return $this
     */
    public function setGrouponId($groupon_id);

    /**
     * @return string $product_id
     */
    public function getProductId();

    /**
     * @param $product_id
     * @return $this
     */
    public function setProductId($product_id);

    /**
     * @return string $customer_id
     */
    public function getCustomerId();

    /**
     * @param $customer_id
     * @return $this
     */
    public function setCustomerId($customer_id);

    /**
     * @return string $groupon_code
     */
    public function getGrouponCode();

    /**
     * @param $groupon_code
     * @return $this
     */
    public function setGrouponCode($groupon_code);

    /**
     * @return string $redemption_code
     */
    public function getRedemptionCode();

    /**
     * @param $redemption_code
     * @return $this
     */
    public function setRedemptionCode($redemption_code);

    /**
     * @return string $order_id
     */
    public function getOrderId();

    /**
     * @param $order_id
     * @return $this
     */
    public function setOrderId($order_id);

    /**
     * @return string $status
     */
    public function getStatus();

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string $recipient
     */
    public function getRecipient();

    /**
     * @param $recipient
     * @return $this
     */
    public function setRecipient($recipient);

    /**
     * @return string $created_at
     */
    public function getCreatedAt();

    /**
     * @param $created_at
     * @return $this
     */
    public function setCreatedAt($created_at);

    /**
     * @return string $updated_at
     */
    public function getUpdatedAt();

    /**
     * @param $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at);

    /**
     * @return string $price
     */
    public function getPrice();

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * @return string $qty
     */
    public function getQty();

    /**
     * @param $qty
     * @return mixed
     */
    public function setQty($qty);

    /**
     * @return string $option_id
     */
    public function getOptionId();

    /**
     * @param $option_id
     * @return $this
     */
    public function setOptionId($option_id);

    /**
     * @return string $location
     */
    public function getLocation();

    /**
     * @param $location
     * @return $this
     */
    public function setLocation($location);

    /**
     * @return string $customer_email
     */
    public function getCustomerEmail();

    /**
     * @param $customer_email
     * @return $this
     */
    public function setCustomerEmail($customer_email);

    /**
     * @return string $customer_name
     */
    public function getCustomerName();

    /**
     * @param $customer_name
     * @return $this
     */
    public function setCustomerName($customer_name);

    /**
     * @return string $is_sent
     */
    public function getIsSent();

    /**
     * @param $is_sent
     * @return $this
     */
    public function setIsSent($is_sent);

    /**
     * @return string $redeemed_at
     */
    public function getRedeemedAt();

    /**
     * @param $redeemed_at
     * @return $this
     */
    public function setRedeemedAt($redeemed_at);

    /**
     * @return string $vendor_id
     */
    public function getVendorId();

    /**
     * @param $vendor_id
     * @return $this
     */
    public function setVendorId($vendor_id);

    /**
     * @return string $redemption_location_id
     */
    public function getRedemptionLocationId();

    /**
     * @param $redemption_location_id
     * @return $this
     */
    public function setRedemptionLocationId($redemption_location_id);

    /**
     * @return string $product_image
     */
    public function getProductImage();

    /**
     * @param $product_image
     * @return $this
     */
    public function setProductImage($product_image);

    /**
     * @return string $expire_date
     */
    public function getExpireDate();

    /**
     * @param $expire_date
     * @return $this
     */
    public function setExpireDate($expire_date);

    /**
     * @return string $redeem_date
     */
    public function getRedeemDate();

    /**
     * @param $redeem_date
     * @return $this
     */
    public function setRedeemDate($redeem_date);
}