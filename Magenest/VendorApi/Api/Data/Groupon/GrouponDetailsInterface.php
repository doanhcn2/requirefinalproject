<?php
/**
 * Created by PhpStorm.
 * User: thien
 * Date: 14/05/2018
 * Time: 07:54
 */
namespace Magenest\VendorApi\Api\Data\Groupon;
/**
 * Interface CouponDetailsInterface
 * @package Magenest\VendorApi\Api\Data\Groupon
 */
interface GrouponDetailsInterface
{


    /**
     * @return string
     */
    public function getApiStatus();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGrouponData();


}