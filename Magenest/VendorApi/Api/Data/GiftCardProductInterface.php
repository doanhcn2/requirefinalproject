<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data;

interface GiftCardProductInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const SKU = 'sku';

    const URL = 'url';

    const NAME = 'name';

    const PRICE = 'price';

    const WEIGHT = 'weight';

    const BEST_VENDOR = 'best_vendor';

    const STATUS = 'status';

    const VISIBILITY = 'visibility';

    const ATTRIBUTE_SET_ID = 'attribute_set_id';

    const TYPE_ID = 'type_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    const MEDIA_URL = 'media';

    const RATING_SUMMARY = 'rating_summary';

    const REVIEWS_COUNT = 'reviews_count';

    const CUSTOM_ATTRIBUTE_VALUE = 'custom_attribute_value';

    const QUANTITY_AND_STOCK_STATUS = 'quantity_and_stock_status';

    const QTY = 'qty';

    const PRODUCT_LINK = 'product_link';

    const FINAL_PRICE = 'final_price';

    const CONFIGURABLE_PRODUCT_LINKS_INFO = 'configurable_product_links_info';

    const CONFIGURABLE_PRODUCT_OPTION_VALUE = 'configurable_product_option_value';

    const CONFIGURABLE_PRODUCT_OPTION_TYPE = 'configurable_product_option_type';

    const VENDOR = 'vendor';

    const CAMPAIGN_STATUS = "campaign_status";

    const GIFTCARD_AMOUNTS = 'giftcard_amounts';

    const GIFT_WRAPPING_AVAILABLE = 'gift_wrapping_available';

    const GIFT_MESSAGE_AVAILABLE = 'gift_message_available';

    const ALLOW_MESSAGE = 'allow_message';

    const IS_REDEEMABLE = 'is_redeemable';

    const GIFTCARD_TYPE = 'giftcard_type';

    const ALLOW_OPEN_AMOUNT = 'allow_open_amount';

    const OPEN_AMOUNT_MAX = 'open_amount_max';

    const OPEN_AMOUNT_MIN = 'open_amount_min';

    const API_STATUS = 'api_status';

    /**#@-*/

    /**
     * Product id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set product id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Product sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Product url
     *
     * @return string
     */
    public function getUrl();

    /**
     * Set product sku
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Product name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set product name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param string $campaignStatus
     * @return $this
     */
    public function setCampaignStatus($campaignStatus);

    /**
     * @return string
     */
    public function getCampaignStatus();

    /**
     * best vendor
     *
     * @return string|null
     */
    public function getBestVendor();

    /**
     * Product attribute set id
     *
     * @return int|null
     */
    public function getAttributeSetId();

    /**
     * Product price
     *
     * @return float|null
     */
    public function getPrice();

    /**
     * Product status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Product visibility
     *
     * @return int|null
     */
    public function getVisibility();

    /**
     * Product type id
     *
     * @return string|null
     */
    public function getTypeId();

    /**
     * Product created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Product updated date
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Product weight
     *
     * @return float|null
     */
    public function getWeight();

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Magento\Catalog\Api\Data\ProductExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Get product links info
     *
     * @return \Magento\Catalog\Api\Data\ProductLinkInterface[]|null
     */
    public function getProductLinks();

    /**
     * Get list of product options
     *
     * @return \Magento\Catalog\Api\Data\ProductCustomOptionInterface[]|null
     */
    public function getOptions();

    /**
     * Get media gallery entries
     *
     * @return \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[]|null
     */
    public function getMediaGalleryEntries();

    /**
     * Gets list of product tier prices
     *
     * @return \Magento\Catalog\Api\Data\ProductTierPriceInterface[]|null
     */
    public function getTierPrices();

    /**
     * @return string
     */
    public function getMediaUrl();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCustomAttributeValue();

    /**
     * @return string
     */
    public function getRatingSummary();

    /**
     * @return string
     */
    public function getReviewsCount();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getQuantityAndStockStatus();

    /**
     * @return int
     */
    public function getQty();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getProductLink();

    /**
     * @return string
     */
    public function getFinalPrice();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductLinksInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductOptionValue();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductOptionType();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVendor();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGiftcardAmounts();

    /**
     * @return string
     */
    public function getGiftWrappingAvailable();

    /**
     * @return string
     */
    public function getGiftMessageAvailable();

    /**
     * @return string
     */
    public function getAllowMessage();

    /**
     * @return string
     */
    public function getIsRedeemable();

    /**
     * @return string
     */
    public function getGiftcardType();

    /**
     * @return string
     */
    public function getAllowOpenAmount();

    /**
     * @return string
     */
    public function getOpenAmountMax();

    /**
     * @return string
     */
    public function getOpenAmountMin();

    /**
     * @return string
     */
    public function getApiStatus();
}
