<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface CoreProductInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const SKU = 'sku';

    const URL = 'url';

    const NAME = 'name';

    const PRICE = 'price';

    const WEIGHT = 'weight';

    const BEST_VENDOR = 'best_vendor';

    const STATUS = 'status';

    const VISIBILITY = 'visibility';

    const ATTRIBUTE_SET_ID = 'attribute_set_id';

    const TYPE_ID = 'type_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    const MEDIA_URL = 'media';

    const RATING_SUMMARY = 'rating_summary';

    const REVIEWS_COUNT = 'reviews_count';

    const CUSTOM_ATTRIBUTE_VALUE = 'custom_attribute_value';

    const QUANTITY_AND_STOCK_STATUS = 'quantity_and_stock_status';

    const QTY = 'qty';

    const PRODUCT_LINK = 'product_link';

    const FINAL_PRICE = 'final_price';

    const CONFIGURABLE_PRODUCT_LINKS_INFO = 'configurable_product_links_info';

    const CONFIGURABLE_PRODUCT_OPTION_VALUE = 'configurable_product_option_value';

    const CONFIGURABLE_PRODUCT_OPTION_TYPE = 'configurable_product_option_type';

    const VENDOR = 'vendor';

    const VENDOR_LIST = 'vendor_list';

    const TOP_REVIEW = 'top_vendor';

    const EVENT_INFO = 'event_info';

    const COUPON_INFO = 'coupon_info';

    const HOTEL_INFO = 'hotel_info';

    const CAMPAIGN_STATUS = "campaign_status";

    /**#@-*/

    /**
     * Product id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set product id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Product sku
     *
     * @return string
     */
    public function getSku();

    /**
     * Product url
     *
     * @return string
     */
    public function getUrl();

    /**
     * Set product sku
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Product name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set product name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param string $campaignStatus
     * @return $this
     */
    public function setCampaignStatus($campaignStatus);

    /**
     * @return string
     */
    public function getCampaignStatus();

    /**
     * best vendor
     *
     * @return string|null
     */
    public function getBestVendor();

    /**
     * Set best vendor
     *
     * @param string $vendor
     * @return $this
     */
    public function setBestVendor($vendor);

    /**
     * Product attribute set id
     *
     * @return int|null
     */
    public function getAttributeSetId();

    /**
     * Set product attribute set id
     *
     * @param int $attributeSetId
     * @return $this
     */
    public function setAttributeSetId($attributeSetId);

    /**
     * Product price
     *
     * @return float|null
     */
    public function getPrice();

    /**
     * Set product price
     *
     * @param float $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * Product status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set product status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Product visibility
     *
     * @return int|null
     */
    public function getVisibility();

    /**
     * Set product visibility
     *
     * @param int $visibility
     * @return $this
     */
    public function setVisibility($visibility);

    /**
     * Product type id
     *
     * @return string|null
     */
    public function getTypeId();

    /**
     * Set product type id
     *
     * @param string $typeId
     * @return $this
     */
    public function setTypeId($typeId);

    /**
     * Product created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set product created date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Product updated date
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set product updated date
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Product weight
     *
     * @return float|null
     */
    public function getWeight();

    /**
     * Set product weight
     *
     * @param float $weight
     * @return $this
     */
    public function setWeight($weight);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Magento\Catalog\Api\Data\ProductExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Magento\Catalog\Api\Data\ProductExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(\Magento\Catalog\Api\Data\ProductExtensionInterface $extensionAttributes);

    /**
     * Get product links info
     *
     * @return \Magento\Catalog\Api\Data\ProductLinkInterface[]|null
     */
    public function getProductLinks();

    /**
     * Set product links info
     *
     * @param \Magento\Catalog\Api\Data\ProductLinkInterface[] $links
     * @return $this
     */
    public function setProductLinks(array $links = null);

    /**
     * Get list of product options
     *
     * @return \Magento\Catalog\Api\Data\ProductCustomOptionInterface[]|null
     */
    public function getOptions();

    /**
     * Set list of product options
     *
     * @param \Magento\Catalog\Api\Data\ProductCustomOptionInterface[] $options
     * @return $this
     */
    public function setOptions(array $options = null);

    /**
     * Get media gallery entries
     *
     * @return \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[]|null
     */
    public function getMediaGalleryEntries();

    /**
     * Set media gallery entries
     *
     * @param \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[] $mediaGalleryEntries
     * @return $this
     */
    public function setMediaGalleryEntries(array $mediaGalleryEntries = null);

    /**
     * Gets list of product tier prices
     *
     * @return \Magento\Catalog\Api\Data\ProductTierPriceInterface[]|null
     */
    public function getTierPrices();

    /**
     * Sets list of product tier prices
     *
     * @param \Magento\Catalog\Api\Data\ProductTierPriceInterface[] $tierPrices
     * @return $this
     */
    public function setTierPrices(array $tierPrices = null);

    /**
     * @return string
     */
    public function getMediaUrl();

    /**
     * @param string $path
     * @return $this
     */
    public function setMediaUrl($path);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCustomAttributeValue();

    /**
     * @param array $data
     * @return $this
     */
    public function setCustomAttributeValue($data);

    /**
     * @return string
     */
    public function getRatingSummary();

    /**
     * @param string $data
     * @return $this
     */
    public function setRatingSummary($data);

    /**
     * @return string
     */
    public function getReviewsCount();

    /**
     * @param string $data
     * @return $this
     */
    public function setReviewsCount($data);

    /**
     * @return bool
     */
    public function getQuantityAndStockStatus();

    /**
     * @return int
     */
    public function getQty();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getProductLink();

    /**
     * @return string
     */
    public function getFinalPrice();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductLinksInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductOptionValue();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getConfigurableProductOptionType();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVendor();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVendorList();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTopReview();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getReviewItems();

    /**
     * @return \Magenest\VendorApi\Api\Data\EventTicket\EventProductInterface
     */
    public function getEventInfo();

    /**
     * @param array $data
     * @return $this
     */
    public function setEventInfo($data);

    /**
     * @return \Magenest\VendorApi\Api\Data\Groupon\CouponProductInterface
     */
    public function getCouponInfo();

    /**
     * @param array $data
     * @return $this
     */
    public function setCouponInfo($data);

    /**
     * @return \Magenest\VendorApi\Api\Data\Hotel\ItemInterface
     */
    public function getHotelInfo();

    /**
     * @param array $data
     * @return $this
     */
    public function setHotelInfo($data);

    /**
     * @return string|int $bought
     */
    public function getBought();
}
