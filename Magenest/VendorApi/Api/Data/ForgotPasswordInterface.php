<?php
namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface ForgotPasswordInterface
{
    const STATUS = 'status';

    /**
     * @return string
     */
    public function getStatus();

}
