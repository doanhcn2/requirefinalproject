<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorManageEventDashboard{
    /**
     * @return string []
     */
    public function getTotalSold();
    /**
     * @return string []
     */
    public function getTotalRevenue();
    /**
     * @return string []
     */
    public function getTotalReview();
    /**
     * @return string []
     */
    public function getTotalRedeem();
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getEventList();
}