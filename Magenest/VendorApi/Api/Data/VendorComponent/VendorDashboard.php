<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorDashboard{
    /**
     * @return string
     */
    public function getProductType();

    /**
     * @return string
     */
    public function getProductTypeTitle();

    /**
     * @return string
     */
    public function getCurrencyCode();

    /**
     * @return string
     */
    public function getTodaySale();

    /**
     * @return string
     */
    public function getSoldToday();
    /**
     * @return string
     */
    public function getSoldoutDay();

    /**
     * @return string
     */
    public function getActiveInventory();

    /**
     * @return string
     */
    public function getPendingBalance();

    /**
     * @return string
     */
    public function getReviewCollection();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getInventory();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPayment();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPaymentChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getFeedback();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getQuestion();

    /**
     * @return string
     */
    public function getAllQuestionUrl();
}