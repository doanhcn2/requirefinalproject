<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorCampaignDetail{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getActivityChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSalesChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCampaignBuyOptions();
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPurchasedCouponList();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getChangeHistories();
}