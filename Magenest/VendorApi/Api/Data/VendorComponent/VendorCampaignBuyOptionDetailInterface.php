<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorCampaignBuyOptionDetailInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getActivityChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSalesChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPurchasedCouponList();
}