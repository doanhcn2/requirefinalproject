<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/15/18
 * Time: 12:18 PM
 */

namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * Interface ResultInterface
 * @package Magenest\VendorApi\Api\Data\VendorComponent
 */
interface ResultInterface
{
    const CORE_PRODUCT = 'result';

    const API_STATUS = 'api_status';

    /**
     * @return string
     */
    public function getResult();

    /**
     * @return int
     */
    public function getApiStatus();

    /**
     * @return \Magenest\VendorApi\Api\Data\VendorApiInterface
     */
    public function getVendorInfo();
}
