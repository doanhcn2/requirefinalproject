<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface RatingGeneralInterface
{
    const VOTE_AVERAGE = 'vote_average';
    const VOTE_COUNT_TOTAL = 'vote_count_total';
    const RATING_OPTION_AVERAGE = 'rating_option_average';

    /**
     * @return string
     */
    public function getVoteAverage();

    /**
     * @return string
     */
    public function getVoteCountTotal();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRatingOptionAverage();
}