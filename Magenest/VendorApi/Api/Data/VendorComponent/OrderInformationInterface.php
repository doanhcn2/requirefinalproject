<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;
/**
 * Interface OrderInformationInterface
 * @package Magenest\VendorApi\Api\Data\VendorComponent
 */
interface OrderInformationInterface
{

    /**
     * @return string|null
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOrderByVendorId();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOrderById();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTotalCount();
}
