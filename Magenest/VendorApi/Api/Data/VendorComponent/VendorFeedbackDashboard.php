<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorFeedbackDashboard{
    /**
     * @return string
     */
    public function getVendorName();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getFeedback();

}