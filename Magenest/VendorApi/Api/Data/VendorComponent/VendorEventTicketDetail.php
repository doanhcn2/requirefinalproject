<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorEventTicketDetail{
    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @return string
     */
    public function getProductId();

    /**
     * @return string
     */
    public function getProductName();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return mixed
     */
    public function getInfo();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getActivityChart();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getSalesChart();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getSessionsTable();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getPurchasedTicketList();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getChangeHistory();
}