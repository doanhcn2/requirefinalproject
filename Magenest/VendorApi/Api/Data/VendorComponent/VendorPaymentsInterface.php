<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * @api
 */
interface VendorPaymentsInterface
{

    const API_STATUS = 'api_status';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSummary();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getListProduct();

    /**
     * @return int
     */
    public function getApiStatus();

}
