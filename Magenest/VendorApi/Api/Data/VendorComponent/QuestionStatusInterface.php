<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * Interface QuestionStatusInterface
 * @package Magenest\VendorApi\Api\Data\VendorComponent
 */
interface QuestionStatusInterface
{
    const RESULT = 'result';

    /**
     * @return bool
     */
    public function getResult();

}