<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorProfileInterface{
    /**
     * @return string
     */
    public function getVendorName();

    /**
     * @return string|int
     */
    public function getPromoteProduct();

    /**
     * @return string
     */
    public function getPrivacyPolicy();

    /**
     * @return string
     */
    public function getLicense();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGeneralInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getLocationInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDeliverieInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getContactInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getMailingAddress();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPayment();
    /**
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\VendorProfileBusinessImageInterface
     */
    public function getBusinessImage();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDocument();
}