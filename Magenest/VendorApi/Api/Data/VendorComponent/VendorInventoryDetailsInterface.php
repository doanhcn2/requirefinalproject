<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorInventoryDetailsInterface{
    /**
     * @return string
     */
    public function getVendorProductId();
    /**
     * @return string
     */
    public function getProductId();

    /**
     * @return string
     */
    public function getProductName();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getSku();

    /**
     * @return string|int
     */
    public function getQuantity();

    /**
     * @return int|float
     */
    public function getSellingPrice();

    /**
     * @return int|float
     */
    public function getPrice();

    /**
     * @return int|float
     */
    public function getSpecialPrice();

    /**
     * @return string
     */
    public function getOfferNote();
    /**
     * @return string
     */
    public function getHandlingTime();
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPaymentChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getActiveInventory();

}