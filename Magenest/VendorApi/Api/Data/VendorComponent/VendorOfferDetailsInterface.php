<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * @api
 */
interface VendorOfferDetailsInterface
{

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOfferData();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSalesChart();

    /**
     * @return int
     */
    public function getApiStatus();

}
