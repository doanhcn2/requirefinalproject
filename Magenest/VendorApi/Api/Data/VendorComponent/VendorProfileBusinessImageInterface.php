<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;
interface VendorProfileBusinessImageInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getProductImage();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getUploadImage();
}