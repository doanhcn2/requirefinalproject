<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * Interface CustomerReviewInterface
 * @package Magenest\VendorApi\Api\Data\VendorComponent
 */
interface CustomerReviewInterface
{
    const API_STATUS = 'api_status';
    const QUESTION_BY_PRODUCT = 'review_by_product';
    const QUESTION_BY_VENDOR = 'review_by_vendor';
    const GENERAL = 'general';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getReviewByProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getReviewByVendor();

    /**
     * @return string
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGeneral();
}
