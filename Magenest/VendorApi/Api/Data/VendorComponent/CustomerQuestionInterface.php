<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

/**
 * Interface CustomerQuestionInterface
 * @package Magenest\VendorApi\Api\Data\VendorComponent
 */
interface CustomerQuestionInterface
{
    const API_STATUS = 'api_status';
    const QUESTION_BY_PRODUCT = 'question_by_product';
    const QUESTION_BY_CUSTOMER = 'question_by_customer';

    const QUESTION_BY_VENDOR = 'question_by_vendor';


    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getQuestionByProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */

    public function getQuestionByVendor();

    /**
     * @return \Magento\Framework\DataObject[]
     */

    public function getQuestionByCustomer();

    /**
     * @return string
     */
    public function getApiStatus();
}
