<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorQuestionDashboard{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getNumberQuestionStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getQuestion();
}