<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface CustomerAskFormInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getAskOrder();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getAskVendor();
}