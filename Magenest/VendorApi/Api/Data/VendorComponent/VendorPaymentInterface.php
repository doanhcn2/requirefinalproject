<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorPaymentInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSummary();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getStatementCollection();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPayoutCollection();
}