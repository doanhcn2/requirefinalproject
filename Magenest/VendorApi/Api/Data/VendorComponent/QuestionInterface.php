<?php
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface QuestionInterface{

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getResult();
}