<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface QuestionParamsInterface
{
    const VENDOR_ID = 'vendor_id';
    const QUESTION = 'question';

    /**
     * @return int
     */
    public function getVendorId();

    /**
     * @param int $vendorId
     * @return $this
     */
    public function setVendorId($vendorId);

    /**
     * @return string
     */
    public function getQuestion();

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question);
}