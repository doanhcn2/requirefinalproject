<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\VendorComponent;

interface VendorTicketSessionDetail{
    /**
     * @return string
     */
    public function getSessionTitle();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return \Magento\Framework\Data\ObjectFactory[]
     */
    public function getActivityChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSalesChart();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSessionTypes();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPurchasedTicketList();
}