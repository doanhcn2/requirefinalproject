<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/7/18
 * Time: 3:40 PM
 */

namespace Magenest\VendorApi\Api\Data\PaymentToken;

/**
 * Interface DetailsInterface
 * @package Magenest\VendorApi\Api\Data\PaymentToken
 */
interface DetailsInterface
{
    /**
     * @return string
     */
    public function getRepositoryId();

    /**
     * @return string
     */
    public function getMerchantId();

    /**
     * @return string
     */
    public function getVerificationStrategy();

    /**
     * @return string
     */
    public function getCcNumber();

    /**
     * @return string
     */
    public function getCcExprMonth();

    /**
     * @return string
     */
    public function getCcExprYear();

    /**
     * @return string
     */
    public function getType();

    /**
     * @return string
     */
    public function getImage();
}
