<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface CategoryProductInterface
{
    /**
     * @return string
     */
    public function getTotalCount();

    /**
     * @return \Magenest\VendorApi\Api\Data\BaseProductInterface[]
     */
    public function getItems();
}
