<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */

namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface StaticInterface
{
    const PREFERENCES = 'preferences';
    const HELP = 'help';
    const TERM_OF_USE = 'term_of_use';
    const PRIVACY_POLICY = 'privacy_policy';
    const LICENSE = 'license';

    /**
     * @return string
     */
    public function getPreferences();

    /**
     * @param string $data
     * @return $this
     */
    public function setPreferences($data);

    /**
     * @return string
     */
    public function getHelp();

    /**
     * @param string $data
     * @return $this
     */
    public function setHelp($data);

    /**
     * @return string
     */
    public function getTermOfUse();

    /**
     * @param string $data
     * @return $this
     */
    public function setTermOfUse($data);

    /**
     * @return string
     */
    public function getPrivacyPolicy();

    /**
     * @param string $data
     * @return $this
     */
    public function setPrivacyPolicy($data);

    /**
     * @return string
     */
    public function getLicense();

    /**
     * @param string $data
     * @return $this
     */
    public function setLicense($data);

    /**
     * @return string
     */
    public function getRewardProgram();

    /**
     * @param string $data
     * @return $this
     */
    public function setRewardProgram($data);
}
