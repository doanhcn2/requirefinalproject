<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/10/2017
 * Time: 00:59
 */
namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface PasswordInterface
{
    const HASH = 'hash';

    /**
     * @return string
     */
    public function getHash();

    /**
     * @param string $hash
     * @return $this
     */
    public function setHash($hash);
}
