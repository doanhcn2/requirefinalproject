<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Hotel;

/**
 * Interface ItemInterface
 * @package Magenest\VendorApi\Api\Data\Hotel
 */
interface ItemInterface
{
    const HOTEL_PROPERTY = 'hotel_property';
    const ROOM_TYPES = 'room_types';
    const ROOM_AMENITIES = 'room_amenities';
    const PROPERTY_SERVICES = 'property_services';
    const PACKAGE_OFFERS = 'package_offers';

    /**
     * @return \Magenest\VendorApi\Api\Data\Hotel\HotelProperty\PropertyInterface
     */
    public function getHotelProperty();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getHotel();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getHotelFacilities();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRoomAmenities();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOfferServices();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPackageOffers();
}
