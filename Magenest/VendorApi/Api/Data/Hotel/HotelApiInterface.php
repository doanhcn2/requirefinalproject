<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Hotel;

/**
 * Interface HotelApiInterface
 * @package Magenest\VendorApi\Api\Data\Hotel
 */
interface HotelApiInterface
{
    const API_STATUS = 'api_status';
    const HOTEL_PRODUCT = 'hotel_product';
    const LIST_HOTEL_PRODUCT = 'list_hotel_product';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getHotelProduct();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getListHotelProduct();

    /**
     * @return string
     */
    public function getApiStatus();
}
