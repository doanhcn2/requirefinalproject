<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/13/18
 * Time: 3:41 PM
 */

namespace Magenest\VendorApi\Api\Data\Hotel;

/**
 * Interface HotelBookingInterface
 * @package Magenest\VendorApi\Api\Data\Hotel
 */
interface HotelBookingInterface
{
    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getQty();

    /**
     * @return string
     */
    public function getPackageId();

    /**
     * @return string
     */
    public function getServices();

    /**
     * @return string
     */
    public function getOrderId();

    /**
     * @return string
     */
    public function getCheckInDate();

    /**
     * @return string
     */
    public function getCheckOutDate();
}