<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api\Data\Hotel\HotelProperty;

/**
 * Interface PropertyInterface
 * @package Magenest\VendorApi\Api\Data\Hotel\HotelProperty
 */
interface PropertyInterface
{
    /**#@+
     * Constants defined for keys of  data array
     */
    const ID = 'id';
    const VENDOR_ID = 'vendor_id';
    const CHECK_IN_FROM = 'check_in_from';
    const CHECK_IN_TO = 'check_in_to';
    const CHECK_OUT_FROM = 'check_out_from';
    const CHECK_OUT_TO = 'check_out_to';
    const IS_INTERNET = 'is_internet';
    const IS_PARKING = 'is_parking';
    const IS_BREAKFAST = 'is_breakfast';
    const IS_CHILDREN_ALLOWED = 'is_children_allowed';
    const IS_PET_ALLOWED = 'is_pet_allowed';
    const LANGUAGES = 'languages';
    const GYM = 'gym';
    const LAUNDRY = 'laundry';
    const SMOKING_ROOMS = 'smoking_rooms';
    const POOL = 'pool';
    const SHUTTLE_SERVICE = 'shuttle_service';
    const SERVICE_DESK_24 = 'service_desk_24';
    const RESTAURANT = 'restaurant';
    const BAR = 'bar';
    const NON_SMOKING_ROOMS = 'non_smoking_rooms';
    const BBQ_FACILITY = 'bbq_facility';
    const FRONT_DESK = 'front_desk';
    const CONCIERGE = 'concierge';
    const TERM = 'terms';
    const IS_TAX = 'is_tax';
    const TAX_AMOUNT = 'tax_amount';
    const TAX_TYPE = 'tax_type';
    const PHOTOS = 'photos';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * Set deal id
     *
     * @param int $data
     * @return $this
     */
    public function setId($data);

    /**
     * @return int|null
     */
    public function getVendorId();

    /**
     * @param int $data
     * @return $this
     */
    public function setVendorId($data);

    /**
     * @return string
     */
    public function getCheckInFrom();

    /**
     * @param string $data
     * @return $this
     */
    public function setCheckInFrom($data);

    /**
     * @return string
     */
    public function getCheckInTo();

    /**
     * @param string
     * @return $this
     */
    public function setCheckInTo($data);

    /**
     * @return string
     */
    public function getCheckOutFrom();

    /**
     * @param string $data
     * @return $this
     */
    public function setCheckOutFrom($data);

    /**
     * @return string
     */
    public function getCheckOutTo();

    /**
     * @param string $data
     * @return $this
     */
    public function setCheckOutTo($data);

    /**
     * @param int $data
     * @return $this
     */
    public function setIsInternet($data);

    /**
     * @return int|null
     */
    public function getIsInternet();

    /**
     * @param int $data
     * @return $this
     */
    public function setIsParking($data);

    /**
     * @return int|null
     */
    public function getIsParking();

    /**
     * @param int $data
     * @return $this
     */
    public function setIsBreakfast($data);

    /**
     * @return int|null
     */
    public function getIsBreakfast();

    /**
     * @param int $data
     * @return $this
     */
    public function setIsChildrenAllowed($data);

    /**
     * @return int|null
     */
    public function getIsChildrenAllowed();

    /**
     * @param int $data
     * @return $this
     */
    public function setPetAllowed($data);

    /**
     * @return int|null
     */
    public function getPetAllowed();

    /**
     * @return int|null
     */
    public function getLanguages();

    /**
     * @param int $data
     * @return $this
     */
    public function setLanguages($data);

    /**
     * @return int|null
     */
    public function getGym();

    /**
     * @param int $data
     * @return $this
     */
    public function setGym($data);

    /**
     * @return int|null
     */
    public function getLaundry();

    /**
     * @param int $data
     * @return $this
     */
    public function setLaundry($data);

    /**
     * @return int|null
     */
    public function getSmokingRooms();

    /**
     * @param int $data
     * @return $this
     */
    public function setSmokingRooms($data);

    /**
     * @return int|null
     */
    public function getPool();

    /**
     * @param int $data
     * @return $this
     */
    public function setPool($data);

    /**
     * @return int|null
     */
    public function getShuttleService();

    /**
     * @param int $data
     * @return $this
     */
    public function setShuttleService($data);

    /**
     * @return int|null
     */
    public function getServiceDesk24();

    /**
     * @param int $data
     * @return $this
     */
    public function setServiceDesk24($data);

    /**
     * @return int|null
     */
    public function getRestaurant();

    /**
     * @param int $data
     * @return $this
     */
    public function setRestaurant($data);

    /**
     * @return int|null
     */
    public function getBar();

    /**
     * @param int $data
     * @return $this
     */
    public function setBar($data);

    /**
     * @return int|null
     */
    public function getNonSmokingRooms();

    /**
     * @param int $data
     * @return $this
     */
    public function setNonSmokingRooms($data);

    /**
     * @return int|null
     */
    public function getBbqFacility();

    /**
     * @param int $data
     * @return $this
     */
    public function setBbqFacility($data);

    /**
     * @return int|null
     */
    public function getFrontDesk();

    /**
     * @param int $data
     * @return $this
     */
    public function setFrontDesk($data);

    /**
     * @return int|null
     */
    public function getConcierge();

    /**
     * @param int $data
     * @return $this
     */
    public function setConcierge($data);

    /**
     * @return int|null
     */
    public function getTerm();

    /**
     * @param int $data
     * @return $this
     */
    public function setTerm($data);

    /**
     * @return int|null
     */
    public function getIsTax();

    /**
     * @param int $data
     * @return $this
     */
    public function setIsTax($data);

    /**
     * @return int|null
     */
    public function getTaxAmount();

    /**
     * @param int $data
     * @return $this
     */
    public function setTaxAmount($data);

    /**
     * @return int|null
     */
    public function getTaxType();

    /**
     * @param int $data
     * @return $this
     */
    public function setTaxType($data);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPhotos();

    /**
     * @param array $data
     * @return $this
     */
    public function setPhotos($data);
}
