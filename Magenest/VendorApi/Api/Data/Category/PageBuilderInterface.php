<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\VendorApi\Api\Data\Category;

/**
 * @api
 */
interface PageBuilderInterface
{
    const Page = 'page';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPage();
}