<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data;

interface NotificationInterface{
    /**
     * @return string[]
     */
    public function getTop50Notifications();

    /**
     * @return int
     */
    public function getApiSatatus();
}