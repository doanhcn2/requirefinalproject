<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Condition;

/**
 * Interface TermAndConditionsInterface
 * @package Magenest\VendorApi\Api\Data\Condition
 */
interface TermAndConditionsInterface
{
    const API_STATUS = 'api_status';
    const GROUPON_TERM = 'groupon_term';
    const CHECKOUT_TERM = 'checkout_term';
    const PAYPAL_TERM = 'paypal_term';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGrouponTerm();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getCheckoutTerm();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPaypalTerm();

    /**
     * @return string
     */
    public function getApiStatus();
}
