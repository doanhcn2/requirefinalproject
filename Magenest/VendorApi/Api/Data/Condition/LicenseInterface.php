<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Condition;

/**
 * Interface LicenseInterface
 * @package Magenest\VendorApi\Api\Data\Condition
 */
interface LicenseInterface
{
    const API_STATUS = 'api_status';
    const UNIRGY_LICENSE = 'unirgy_license';
    const VES_LICENSE = 'ves_license';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getUnirgyLicense();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVesLicense();

    /**
     * @return string
     */
    public function getApiStatus();
}
