<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\VendorApi\Api\Data;

/**
 * @api
 */
interface CustomerInfoInterface
{
    const ACCESS_TOKEN = 'access_token';

    /**
     * @return string
     */
    public function getAccessToken();
    /**
     *  @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomerInfo();

    /**
     * @return string
     */
    public function getStoreCreditTotal();
    /**
     * @return string
     */
    public function getCartId();
    /**
     * @return array
     */
    public function getRewardPointBalance();
    /**
     * @return string
     */
    public function getCartItemQty();

}