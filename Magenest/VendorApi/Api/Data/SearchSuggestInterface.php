<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data;

interface SearchSuggestInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSearch();
}