<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket;

interface TicketDetailsInterface
{
    /**
     * @return string
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketData();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSessionData();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketLocation();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketTerm();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRatingOption();
}