<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket;

/**
 * Interface EventApiInterface
 * @package Magenest\VendorApi\Api\Data\EventTicket
 */
interface EventApiInterface
{
    const API_STATUS = 'api_status';
    const EVENT_PRODUCT = 'event';
    const LIST_EVENT_PRODUCT = 'list_event';
    const VENUE_MAP = 'venue_map';
    const VENUE_MAP_DATA = 'venue_map_data';


    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getEvent();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getListEvent();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVenueMap();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVenueMapData();

    /**
     * @return string
     */
    public function getApiStatus();
}
