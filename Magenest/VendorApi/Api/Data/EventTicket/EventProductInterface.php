<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket;

/**
 * Interface EventProductInterface
 * @package Magenest\VendorApi\Api\Data\EventTicket
 */
interface EventProductInterface
{
    /**
     * define array value
     */
    const EVENT_ID = 'event_id';
    const EVENT_NAME = 'event_name';
    const PRODUCT_ID = 'product_id';
    const TICKET_TYPE = 'ticket_type';
    const IS_ONLINE = 'is_online';

    const EVENT_TYPE = 'event_type';
    const AGE = 'age';
    const SHOW_UP = 'show_up';
    const SHOW_UP_TIME = 'show_up_time';
    const CAN_PURCHASE = 'can_purchase';

    const MUST_PRINT = 'must_print';
    const REFUND_POLICY = 'refund_policy';
    const REFUND_POLICY_DAY = 'refund_policy_day';
    const PUBLIC_ORGANIZER = 'public_organizer';
    const ORGANIZER_NAME = 'organizer_name';

    const ORGANIZER_DESCRIPTION = 'organizer_description';
    const USE_MAP = 'use_map';
    const EMAIL_CONFIG = 'email_config';
    const ENABLE_DATE_TIME = 'enable_date_time';
    const LOCATION = 'location';

    const TYPE = 'type';


    /**#@-*/

    /**
     * @return int
     */
    public function getEventId();

    /**
     * Set event id
     *
     * @param int $id
     * @return $this
     */
    public function setEventId($id);

    /**
     * @return string
     */
    public function getEventName();

    /**
     * Set event name
     *
     * @param string $name
     * @return $this
     */
    public function setEventName($name);

    /**
     * Product Id
     *
     * @return string|null
     */
    public function getProductId();

    /**
     * Set product id
     *
     * @param int $product_id
     * @return $this
     */
    public function setProductId($product_id);

    /**
     * ticket type
     *
     * @return string|null
     */
    public function getTicketType();

    /**
     * Set ticket type
     *
     * @param string $ticketType
     * @return $this
     */
    public function setTicketType($ticketType);

    /**
     * is online
     *
     * @return int|null
     */
    public function getIsOnline();

    /**
     * Set is online
     *
     * @param string $isOnline
     * @return $this
     */
    public function setIsOnline($isOnline);

    /**
     * event type
     *
     * @return int|null
     */
    public function getEventType();

    /**
     * Set event type
     *
     * @param string $eventType
     * @return $this
     */
    public function setEventType($eventType);

    /**
     * age
     *
     * @return int|null
     */
    public function getAge();

    /**
     * Set age
     *
     * @param int $age
     * @return $this
     */
    public function setAge($age);

    /**
     * show up
     *
     * @return int|null
     */
    public function getShowUp();

    /**
     * Set age
     *
     * @param int $showUp
     * @return $this
     */
    public function setShowUp($showUp);

    /**
     * show up
     *
     * @return string|null
     */
    public function getShowUpTime();

    /**
     * Set age
     *
     * @param string $showUpTime
     * @return $this
     */
    public function setShowUpTime($showUpTime);

    /**
     * can purchase
     *
     * @return int|null
     */
    public function getCanPurchase();

    /**
     * Set age
     *
     * @param int $canPurchase
     * @return $this
     */
    public function setCanPurchase($canPurchase);

    /**
     * must print
     *
     * @return int|null
     */
    public function getMustPrint();

    /**
     * Set must print
     *
     * @param int $mustPrint
     * @return $this
     */
    public function setMustPrint($mustPrint);

    /**
     * refund policy
     *
     * @return string|null
     */
    public function getRefundPolicy();

    /**
     * Set refund policy
     *
     * @param string $refundPolicy
     * @return $this
     */
    public function setRefundPolicy($refundPolicy);

    /**
     * refund policy day
     *
     * @return int|null
     */
    public function getRefundPolicyDay();

    /**
     * Set refund policy day
     *
     * @param int $refundPolicyDay
     * @return $this
     */
    public function setRefundPolicyDay($refundPolicyDay);

    /**
     * public organizer
     *
     * @return int|null
     */
    public function getPublicOrganizer();

    /**
     * Set public organizer
     *
     * @param int $publicOrganizer
     * @return $this
     */
    public function setPublicOrganizer($publicOrganizer);

    /**
     * organizer name
     *
     * @return string|null
     */
    public function getOrganizerName();

    /**
     * Set  organizer name
     *
     * @param string $organizerName
     * @return $this
     */
    public function setOrganizerName($organizerName);

    /**
     * organizer description
     *
     * @return string|null
     */
    public function getOrganizerDescription();

    /**
     * Set  organizer description
     *
     * @param string $organizerDescription
     * @return $this
     */
    public function setOrganizerDescription($organizerDescription);

    /**
     * use map
     *
     * @return int|null
     */
    public function getUseMap();

    /**
     * Set  use map
     *
     * @param int $useMap
     * @return $this
     */
    public function setUseMap($useMap);

    /**
     * email config
     *
     * @return string|null
     */
    public function getEmailConfig();

    /**
     * Set email config
     *
     * @param string $emailConfig
     * @return $this
     */
    public function setEmailConfig($emailConfig);

    /**
     * enable date time
     *
     * @return string|null
     */
    public function getEnableDateTime();

    /**
     * Set enable date time
     *
     * @param string $enableDateTime
     * @return $this
     */
    public function setEnableDateTime($enableDateTime);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getLocation();

    /**
     * @param array $location
     * @return $this
     */
    public function setLocation($location);

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getType();

    /**
     * @param array $type
     * @return $this
     */
    public function setType($type);
}
