<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket;

/**
 * Interface TicketApiInterface
 * @package Magenest\VendorApi\Api\Data\EventTicket
 */
interface TicketApiInterface
{
    const API_STATUS = 'api_status';

    const TICKET = 'ticket';

    const TICKET_BY_EMAIL = 'ticket_by_email';

    const TICKET_BY_PRODUCT = 'ticket_by_product';


    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicket();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketByEmail();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getAllTicketByCustomerId();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getTicketByProduct();

    /**
     * @return string
     */
    public function getApiStatus();
}
