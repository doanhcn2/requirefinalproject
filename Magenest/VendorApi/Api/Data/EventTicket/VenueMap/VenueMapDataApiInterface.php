<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket\VenueMap;

/**
 * Interface VenueMapApiInterface
 * @package Magenest\VendorApi\Api\Data\EventTicket\VenueMap
 */
interface VenueMapDataApiInterface
{
    const API_STATUS = 'api_status';
    const ID = 'map_data_id';
    const PRODUCT_ID = 'product_id';
    const VENUE_MAP_ID = 'venue_map_id';
    const SESSION_MAP_ID = 'session_map_id';
    const SESSION_MAP_NAME = 'session_map_name';
    const SESSION_MAP_DATA = 'session_map_data';
    const SESSION_TICKET_DATA = 'session_ticket_data';
    const VENDOR_ID = 'vendor_id';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string|null
     */
    public function getProductId();

    /**
     * @return int|null
     */
    public function getVenueMapId();

    /**
     * @return int|null
     */
    public function getSessionMapId();

    /**
     * @return string|null
     */
    public function getSessionMapName();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSessionMapData();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getSessionTicketData();

    /**
     * @return int|null
     */
    public function getVendorId();

    /**
     * @return string
     */
    public function getApiStatus();
}
