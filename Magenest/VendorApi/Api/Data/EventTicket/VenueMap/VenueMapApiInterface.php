<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\EventTicket\VenueMap;

/**
 * Interface VenueMapApiInterface
 * @package Magenest\VendorApi\Api\Data\EventTicket\VenueMap
 */
interface VenueMapApiInterface
{
    const API_STATUS = 'api_status';
    const ID = 'venue_map_id';
    const VENUE_MAP_NAME = 'venue_map_name';
    const VENUE_MAP_DATA = 'venue_map_data';
    const CREATED = 'created';
    const VENDOR_ID = 'vendor_id';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string|null
     */
    public function getVenueMapName();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVenueMapData();

    /**
     * @return string|null
     */
    public function getCreated();

    /**
     * @return int|null
     */
    public function getVendorId();

    /**
     * @return string
     */
    public function getApiStatus();
}
