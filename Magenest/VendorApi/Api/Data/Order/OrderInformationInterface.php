<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data\Order;

/**
 * Interface OrderInformationInterface
 * @package Magenest\VendorApi\Api\Data\Order
 */
interface OrderInformationInterface
{
    const API_STATUS = 'api_status';
    const ORDER_BY_CUSTOMER_ID = 'order_by_customer_id';

    /**
     * @return string|null
     */
    public function getApiStatus();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOrderByCustomerId();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getOrderById();
}
