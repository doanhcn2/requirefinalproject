<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 11/04/2018
 * Time: 09:28
 */
namespace Magenest\VendorApi\Api\Data\Order;
interface CustomerPurchasedOrdersInformationInterface{
    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getVendorInformations();
}