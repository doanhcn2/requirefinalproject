<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\VendorApi\Api\Data\RewardPoint;

/**
 * @api
 */
interface RewardPointInterface
{
    const RESULT = 'result';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getResult();
}