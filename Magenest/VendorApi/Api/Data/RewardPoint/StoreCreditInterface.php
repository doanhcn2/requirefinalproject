<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:55
 */
namespace Magenest\VendorApi\Api\Data\RewardPoint;

/**
 * Interface StoreCreditInterface
 * @package Magenest\VendorApi\Api\Data\RewardPoint
 */
interface StoreCreditInterface
{
    const API_STATUS = 'api_status';
    const STORE_CREDIT_HISTORY = 'store_credit_history';
    const STORE_CREDIT_BALANCE = 'store_credit_balance';

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getStoreCreditHistory();

    /**
     * @return float
     */
    public function getStoreCreditBalance();

    /**
     * @return int
     */
    public function getApiStatus();
}