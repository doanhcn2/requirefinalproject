<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Data;

interface VendorInfoInterface{
    /**
     * @return string
     */
    public function getAccessToken();

    /**
     * @return string
     */
    public function getVendorName();

    /**
     * @return string
     */
    public function getPromoteProduct();

    /**
     * @return string
     */
    public function getPrivacyPolicy();

    /**
     * @return string
     */
    public function getLicense();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getGeneralInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getLocationInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDeliverieInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getContactInfo();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getMailingAddress();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getPayment();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getBusinessImage();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getDocument();
}