<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api\Data;

interface ReviewInterface
{
    const RATING_AVERAGE = 'rating_average';
    const RATING_COUNT = 'rating_count';
    const RATING_DETAILS = 'rating_details';
    const API_STATUS = 'api_status';

    /**
     * @return string
     */
    public function getRatingAverage();

    /**
     * @return string
     */
    public function getRatingCount();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRatingDetails();

    /**
     * @return string
     */
    public function getApiStatus();
}
