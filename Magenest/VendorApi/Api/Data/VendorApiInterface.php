<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api\Data;
use Magento\TestFramework\Event\Magento;

/**
 * Interface VendorApiInterface
 * @package Magenest\VendorApi\Api\Data
 */
interface VendorApiInterface
{
    const VENDOR_NAME = 'vendor_name';
    const VENDOR_DESCRIPTION = 'vendor_description';
    const VENDOR_EMAIL = 'vendor_email';
    const VENDOR_PHONE = 'vendor_phone';
    const VENDOR_STREET = 'vendor_street';
    const VENDOR_ZIP = 'vendor_zip';
    const VENDOR_COUNTRY = 'vendor_country';
    const RATING_GENERAL = 'rating_general';
    const RATING_DETAILS = 'rating_details';
    const API_STATUS = 'api_status';

    /**
     * @return string
     */
    public function getVendorName();

    /**
     * @return string
     */
    public function getVendorDescription();

    /**
     * @return string
     */
    public function getVendorEmail();

    /**
     * @return string
     */
    public function getVendorPhone();

    /**
     * @return mixed
     */
    public function getVendorStreet();

    /**
     * @return string
     */
    public function getVendorZip();

    /**
     * @return string
     */
    public function getVendorCountry();

    /**
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\RatingGeneralInterface
     */
    public function getRatingGeneral();

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getRatingDetails();

    /**
     * @return int
     */
    public function getApiStatus();
}
