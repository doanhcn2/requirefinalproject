<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/7/18
 * Time: 3:36 PM
 */

namespace Magenest\VendorApi\Api\Data;

/**
 * Interface PaymentTokenInterface
 * @package Magenest\VendorApi\Api\Data
 */
interface PaymentTokenInterface
{
    /**
     * Gets the entity ID.
     *
     * @return int|null Entity ID.
     */
    public function getEntityId();

    /**
     * Gets the customer ID.
     *
     * @return int|null Customer ID.
     */
    public function getCustomerId();

    /**
     * @return string
     */
    public function getCustomerName();

    /**
     * Get public hash
     *
     * @return string
     */
    public function getPublicHash();

    /**
     * Get payment method code
     *
     * @return string
     */
    public function getPaymentMethodCode();

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Get token creation timestamp
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get token expiration timestamp
     *
     * @return string|null
     */
    public function getExpiresAt();

    /**
     * Get gateway token ID
     *
     * @return string
     */
    public function getGatewayToken();

    /**
     * Get token details
     *
     * @return \Magenest\VendorApi\Api\Data\PaymentToken\DetailsInterface
     */
    public function getTokenDetails();

    /**
     * Gets is vault payment record active.
     *
     * @return bool Is active.
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsActive();

    /**
     * Gets is vault payment record visible.
     *
     * @return bool Is visible.
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsVisible();
}
