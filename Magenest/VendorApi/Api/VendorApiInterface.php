<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface ProductApiInterface
 * @package Magenest\VendorApi\Api
 */
interface VendorApiInterface
{
    /**
     * @api
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\VendorApiInterface
     */
    public function getVendor( $id);
}