<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Filter;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface FilterManagementInterface
{
    /**
     * Retrieve page.
     *
     * @param int $categoryId
     * @return string[]
     */
    public function getFilters($categoryId);
}
