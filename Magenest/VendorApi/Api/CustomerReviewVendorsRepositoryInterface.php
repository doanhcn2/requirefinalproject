<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 09/04/2018
 * Time: 10:34
 */
namespace Magenest\VendorApi\Api;

interface CustomerReviewVendorsRepositoryInterface{
    /**
     * Enable a guest user to return information for a specified cart.
     *
     * @param string $customerId
     * @param string $cur_page
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\Review\CustomerReviewVendorsInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllCustomerReviewVendors($customerId,$cur_page,$size);
}