<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;
interface CustomerReivewSortFilterRepositoryInterface{
    /**
     * @api
     * @param string|int $customerId
     * @param mixed $requestParams
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function filterReviewByCustomerId($customerId,$requestParams);

    /**
     * @api
     * @param string $id
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getDetailReviewById($id);
}