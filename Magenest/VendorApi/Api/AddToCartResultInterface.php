<?php
/**
 * Author: Eric Quach
 * Date: 3/14/18
 */
namespace Magenest\VendorApi\Api;

interface AddToCartResultInterface
{
    /**
     * @return bool
     */
    public function getSuccess();

    /**
     * @return int
     */
    public function getCartItemsCount();
}