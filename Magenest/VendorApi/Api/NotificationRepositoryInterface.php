<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

interface NotificationRepositoryInterface{
    /**
     * @param string $customerId
     * @param string $size
     * @return \Magenest\VendorApi\Api\Data\NotificationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getListByCustomer($customerId,$size);

    /**
     * @param string $customerId
     * @return \Magenest\VendorApi\Api\Data\NotificationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getUnreadListByCustomer($customerId);

    /**
     * @param string $customerId
     * @param string $deviceId
     * @return string[]
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createDeviceIdForCustomer($customerId,$deviceId);

    /**
     * @param string $customerId
     * @param string $notifId
     * @param string $isLocalId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function changeStatusNotificationForCustomer($customerId, $notifId, $isLocalId);

    /**
     * @param string $customerId
     * @param string $notifId
     * @param string $isLocalId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteNotificationByCustomer($customerId, $notifId, $isLocalId);

    /**
     * @param string $customerId
     * @param string $deviceId
     * @return string[]
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteDeviceIdForCustomer($customerId,$deviceId);
}