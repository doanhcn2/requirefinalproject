<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api;

interface StaticInterface
{
    /**
     * @api
     * @param int $storeId
     * @return \Magenest\VendorApi\Api\Data\StaticInterface
     */
    public function getForCustomer($storeId);

    /**
     * @api
     * @param int $storeId
     * @return \Magenest\VendorApi\Api\Data\StaticInterface
     */
    public function getForVendor($storeId);

    /**
     * @param string|int $storeId
     * @return \Magenest\VendorApi\Api\Data\StaticInterface
     */
    public function getMobileConfigurableForCustomer($storeId);

    /**
     * @param string|int $storeId
     * @return \Magenest\VendorApi\Api\Data\StaticInterface
     */
    public function getMobileConfigurableForVendor($storeId);
}