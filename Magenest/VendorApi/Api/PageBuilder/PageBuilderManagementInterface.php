<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\PageBuilder;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface PageBuilderManagementInterface
{
    /**
     * Retrieve page.
     *
     * @param int $pageId
     * @return \Magenest\VendorApi\Api\Data\PageBuilder\PageBuilderInterface
     */
    public function getPageBuilderById($pageId);
}
