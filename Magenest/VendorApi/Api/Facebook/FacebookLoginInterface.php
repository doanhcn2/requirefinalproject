<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:19
 */
namespace Magenest\VendorApi\Api\Facebook;

interface FacebookLoginInterface
{
    /**
     * login or create new customer via facebook login
     *
     * @param string[] $userInfo
     * @return \Magenest\VendorApi\Api\Data\CustomerInfoInterface
     */
    public function loginOrCreateNewCustomer($userInfo);
}