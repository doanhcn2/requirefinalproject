<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 29/09/2017
 * Time: 10:41
 */

namespace Magenest\VendorApi\Api;

/**
 * Interface StoreApiInterface
 * @package Magenest\VendorApi\Api
 */
interface StoreApiInterface
{
    /**
     * @api
     * @return \Magenest\VendorApi\Api\Data\StoreInterface
     */
    public function getStore();
}