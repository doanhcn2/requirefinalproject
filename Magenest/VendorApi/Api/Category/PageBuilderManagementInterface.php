<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api\Category;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface PageBuilderManagementInterface
{
    /**
     * Retrieve page.
     *
     * @param int $categoryId
     * @return \Magenest\VendorApi\Api\Data\Category\PageBuilderInterface
     */
    public function getPageBuilderByCategoryId($categoryId);
}
