<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 11/04/2018
 * Time: 08:38
 */
namespace Magenest\VendorApi\Api;
interface CustomerPurchasedOrdersApiInterface{
    /**
     * @api
     * @param string $customerId
     * @return \Magenest\VendorApi\Api\Data\Order\CustomerPurchasedOrdersInformationInterface
     */
    public function getAllVendorsFromCustomerPurchasedOrders($customerId);

    /**
     * @param string|int $customerId
     * @param mixed $requestParams
     * @return mixed
     */
    public function createRma($customerId,$requestParams);
}