<?php

namespace Magenest\VendorApi\Api\RewardPoint;

/**
 * Interface RewardPointManagementInterface
 * @package Magenest\VendorApi\Api\RewardPoint
 */
interface RewardPointManagementInterface
{

    /**
     *
     * @param int $customerId
     * @return \Magenest\VendorApi\Api\Data\RewardPoint\RewardPointInterface
     */
    public function getRewardPointForCustomer($customerId);

    /**
     *
     * @param int $customerId
     * @return \Magenest\VendorApi\Api\Data\RewardPoint\StoreCreditInterface
     */
    public function getStoreCreditForCustomer($customerId);
}

