<?php

namespace Magenest\VendorApi\Api\RewardPoint;

/**
 * Interface RewardPointManagementInterface
 * @package Magenest\VendorApi\Api\RewardPoint
 */
interface GiftCardInterface
{
    /**
     * @param int $customerId
     * @param string $giftCode
     * @return mixed
     */
    public function redeemGiftCard($customerId, $giftCode);

    /**
     * @param string $giftCode
     * @return mixed
     */
    public function checkGiftCard($giftCode);
}

