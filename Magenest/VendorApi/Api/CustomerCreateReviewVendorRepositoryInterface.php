<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;
interface CustomerCreateReviewVendorRepositoryInterface{
    /**
     * @api
     * @param mixed $requestParams
     * @param mixed $customerId
     * @return \Magenest\VendorApi\Api\Data\Order\CustomerPurchasedOrdersInformationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createReviewVendor($customerId,$requestParams);

    /**
     * @api
     * @param string $entity_id
     * @return mixed
     */
    public function getAllRatingByEntityId($entity_id);

    /**
     * @param string $customerId
     * @return \Magenest\VendorApi\Api\Data\Review\FormDataCustomerInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllFormDataReviewVendorByCustomerId($customerId);
}