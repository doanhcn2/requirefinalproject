<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 26/12/2017
 * Time: 10:05
 */
namespace Magenest\VendorApi\Api;

interface ProductRepositoryInterface {

    /**
     * Get product list
     *
     * @param int $store
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magenest\VendorApi\Api\Data\CategoryProductInterface
     */
    public function getList($store, \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}