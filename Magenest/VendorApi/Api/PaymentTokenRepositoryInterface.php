<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api;

use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;

/**
 * Gateway vault payment token repository interface.
 *
 * @api
 */
interface PaymentTokenRepositoryInterface
{
    /**
     * Lists payment tokens that match specified search criteria.
     *
     * @param int $customerId Customer ID.
     * @return \Magenest\VendorApi\Api\Data\PaymentTokenInterface[] Payment token search result interface.
     */
    public function getListByCustomerId($customerId);

    /**
     * @param int $customerId
     * @param int $tokenId
     * @return string
     */
    public function delete($customerId, $tokenId);
}
