<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface ProductApiInterface
 * @package Magenest\VendorApi\Api
 */
interface ProductApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\VendorApiProductInterface
     */
    public function getProduct($store, $id);

    /**
     * Get product review by product page for product page
     *
     * @param string $store
     * @param string $productId
     * @return \Magenest\VendorApi\Api\Data\ReviewInterface
     */
    public function getProductReviewById($store, $productId);

    /**
     * @return \Magenest\VendorApi\Api\Data\VendorApiProductInterface
     */
    public function getGiftCardProductPhysical();

    /**
     * @return \Magenest\VendorApi\Api\Data\VendorApiProductInterface
     */
    public function getGiftCardProductVirtual();

    /**
     * @api
     * @param string $customerId
     * @param string $productId
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function checkProductExitWishlish($customerId,$productId);

    /**
     * @api
     * @param int $productId
     * @param int $storeId
     * @return \Magenest\VendorApi\Api\Data\DefaultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getVendorListFromConfigurableProduct($productId, $storeId);
}