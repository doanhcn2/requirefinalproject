<?php
/**
 * Created by PhpStorm.
 * User: keysnt
 * Date: 10/04/2018
 * Time: 13:04
 */
namespace Magenest\VendorApi\Api;
interface GrouponCustomerApiInterface{
    /**
     * Get customer by Customer ID.
     *
     * @param string $customerId
     * @param string $cur_page
     * @param string $size
     * @param string $status
     * @return \Magenest\VendorApi\Api\Data\Groupon\GrouponCustomerInformationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
     public function getAllGrouponByCustomerId($customerId,$cur_page,$size,$status);
}