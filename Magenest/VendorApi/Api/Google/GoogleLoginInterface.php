<?php
/**
 * Created by PhpStorm.
 * User: joel
 * Date: 30/01/2018
 * Time: 13:19
 */
namespace Magenest\VendorApi\Api\Google;

interface GoogleLoginInterface
{
    /**
     * login or create new customer via google login
     *
     * @param string[] $userInfo
     * @return \Magenest\VendorApi\Api\Data\CustomerInfoInterface
     */
    public function loginOrCreateNewCustomer($userInfo);
}