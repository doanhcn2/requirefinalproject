<?php
/**
 * Author: Eric Quach
 * Date: 3/14/18
 */
namespace Magenest\VendorApi\Api;

interface CartManagementInterface
{
    /**
     * Add requested product to cart
     *
     * @param mixed $requestParams
     * @return \Magenest\VendorApi\Api\AddToCartResultInterface
     */
    public function addProduct($requestParams);

    /**
     * @param string $customerId
     * @param string $cartId
     * @return \Magenest\VendorApi\Api\Data\Cart\CartInterface
     */
    public function removeApplyRewardPoints($customerId,$cartId);

    /**
     * @param string $customerId
     * @param string $cartId
     * @return \Magenest\VendorApi\Api\Data\Cart\CartInterface
     */
    public function removeStoreCredit($customerId,$cartId);
}