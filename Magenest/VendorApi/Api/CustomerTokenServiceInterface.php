<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 11/01/2018
 * Time: 14:53
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface providing token generation for Customers
 *
 * @api
 * @since 100.0.2
 */
interface CustomerTokenServiceInterface
{
    /**
     * Create access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $password
     * @return \Magenest\VendorApi\Api\Data\CustomerInfoInterface
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createCustomerAccessToken($username, $password);

    /**
     * @param string $username
     * @param string $password
     * @return \Magenest\VendorApi\Api\Data\VendorInfoInterface
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createVendorAccessToken($username, $password);

    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $password
     * @param string $redirectUrl
     * @return \Magenest\VendorApi\Api\Data\CustomerInfoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $password = null,
        $redirectUrl = ''
    );
}