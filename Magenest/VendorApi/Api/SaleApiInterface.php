<?php

namespace Magenest\VendorApi\Api;

interface SaleApiInterface
{
    /**
     * @api
     * @param int $customerId
     * @return \Magenest\VendorApi\Api\Data\DefaultInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerShipmentList($customerId);
}