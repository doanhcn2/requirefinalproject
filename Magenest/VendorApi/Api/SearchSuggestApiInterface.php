<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

interface SearchSuggestApiInterface{
    /**
     * @api
     * @param string $query_text
     * @return \Magenest\VendorApi\Api\Data\SearchSuggestInterface
     */
    public function getSearchSuggest($query_text);

    /**
     * @param string $query_text
     * @return \Magenest\VendorApi\Api\Data\SearchSuggestInterface
     */
    public function saveSearchSuggest($query_text);

    /**
     * @param string $query_text
     * @return mixed
     */
    public function getSearchSuggestion($query_text);
}