<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/13/18
 * Time: 7:03 PM
 */

namespace Magenest\VendorApi\Api;

/**
 * Interface UdvendorApiInterface
 * @package Magenest\VendorApi\Api
 */
interface UdvendorApiInterface
{
    /**
     * @param mixed $request
     * @return \Magenest\VendorApi\Api\Data\VendorComponent\ResultInterface
     */
    public function signup($request);
}
