<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\VendorApi\Api;

/**
 * Interface HotelApiInterface
 * @package Magenest\VendorApi\Api
 */
interface HotelApiInterface
{
    /**
     * @api
     * @param string $store
     * @param string $id
     * @return \Magenest\VendorApi\Api\Data\Hotel\HotelApiInterface
     */
    public function getHotelProduct($store, $id);

    /**
     * @param string $customerId
     * @return \Magenest\VendorApi\Api\Data\Hotel\HotelBookingInterface[]
     */
    public function getHotelBooking($customerId);
}