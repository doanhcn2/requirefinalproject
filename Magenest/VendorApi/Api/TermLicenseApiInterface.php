<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api;

interface TermLicenseApiInterface
{
    /**
     * @api
     * @return \Magenest\VendorApi\Api\Data\Condition\TermAndConditionsInterface
     */
    public function getAllTermCondition();

    /**
     * @api
     * @return \Magenest\VendorApi\Api\Data\Condition\LicenseInterface
     */
    public function getAllLicense();
}