<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\VendorApi\Api;

interface PasswordInterface
{
    /**
     * @api
     * @param string $password
     * @return \Magenest\VendorApi\Api\Data\PasswordInterface
     */
    public function create($password);
}