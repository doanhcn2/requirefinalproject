<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/6/18
 * Time: 4:12 PM
 */

namespace Magenest\VendorApi\Plugin\AccessChangeQuoteControl;


use Magento\Framework\Registry;

class Disable
{
    const DISABLE_REGISTRY = 'registrykeytoskipchecking';
    protected $registry;

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    public function aroundBeforeSave(
        \Magento\Quote\Model\QuoteRepository\Plugin\AccessChangeQuoteControl $subject,
        callable $proceed,
        ...$args
    ) {
        if ($this->registry->registry(self::DISABLE_REGISTRY) !== true) {
            $proceed(...$args);
        }
    }
}
