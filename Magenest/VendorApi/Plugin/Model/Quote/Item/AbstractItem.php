<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/02/2018
 * Time: 16:48
 */
namespace Magenest\VendorApi\Plugin\Model\Quote\Item;

use Magento\Framework\App\ObjectManager;

class AbstractItem {
    protected $_productRepository;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->_productRepository = $productRepository;
    }

    public function aroundGetProduct(
        \Magento\Quote\Model\Quote\Item\AbstractItem $subject,
        \Closure $proceed
    ) {
        if (!$subject->getProductId()) {
            return ObjectManager::getInstance()->create("\Magento\Catalog\Model\Product");
        } else {
            return $proceed();
        }
    }
}