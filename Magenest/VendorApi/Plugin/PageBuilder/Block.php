<?php
/**
 * Created by PhpStorm.
 * User: logan
 * Date: 01/02/2018
 * Time: 16:48
 */
namespace Magenest\VendorApi\Plugin\PageBuilder;

use Magento\Framework\App\ObjectManager;

class Block {
    protected $simpleResultFactory;
    protected $_storeHelper;

    public function __construct(
        \Magenest\VendorApi\Model\SimpleResultFactory $simpleResultFactory,
        \Magenest\VendorApi\Helper\StoreHelper $storeHelper
    ) {
        $this->simpleResultFactory = $simpleResultFactory;
        $this->_storeHelper = $storeHelper;
    }

    public function afterSave(
        \Magento\Framework\Model\AbstractModel $object
    ) {
        try {
            $storeData = $this->_storeHelper->getAllStoreData();
            $simpleResultFactory = $this->simpleResultFactory->create();
            $storeDataOld = $simpleResultFactory
                ->getCollection()
                ->addFieldToSelect('id')
                ->addFieldToFilter('result_key', 'vendorapi/store')->getFirstItem();
            if(count($storeDataOld->getData()) > 0){
                $simpleResultFactory->load($storeDataOld->getId());
                $simpleResultFactory->addData([
                    'result_value' => isset($storeData) ? json_encode($storeData) : ''
                ]);
                $simpleResultFactory->save();
            }
            else {
                $simpleResultFactory->setData([
                    'result_key' => 'vendorapi/store',
                    'result_value' => isset($storeData) ? json_encode($storeData) : ''
                ])->save();
            }
        }catch (\Exception $e){

        }
    }
}