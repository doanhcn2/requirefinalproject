<?php
/**
 * Venustheme
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Venustheme
 * @package    Ves_PageBuilder
 * @copyright  Copyright (c) 2016 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */
namespace Magenest\VendorApi\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\DB\Ddl\Table;


class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->createEplusSimpleResultTable($installer);
        }

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $table = $setup->getTable('oauth_token');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'vendor_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Vendor Id',
                    'unsigned'=>true, 'nullable' => true
                ]);
        }

        $installer->endSetup();
    }
    private function createEplusSimpleResultTable(SchemaSetupInterface $installer)
    {
        $tableName = 'magenest_vendor_simple_result';
        $table = $installer->getConnection()
            ->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'result_key',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Key'
            )
            ->addColumn(
                'result_value',
                Table::TYPE_TEXT,
                "1M",
                [ 'nullable' => true],
                'Value'
            )
            ->addIndex(
                $installer->getIdxName($tableName, ['result_key']),
                ['result_key'],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX]
            )
            ->setComment('Result Cache');

        $installer->getConnection()->createTable($table);
    }
}
