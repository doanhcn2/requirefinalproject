<?php
/**
 * Author: Eric Quach
 * Date: 3/10/18
 */

namespace Magenest\VendorApi\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use function Symfony\Component\DependencyInjection\Tests\Fixtures\factoryFunction;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->updateProductAttribute($setup);
        }
        $setup->endSetup();
    }

    private function updateProductAttribute($setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'brand',
            [
                'type' => 'varchar',
                'sort_order' => 10,
                'label' => 'Brand',
                'input' => 'select',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'required' => false,
                'user_defined' => true,
                'system' => true,
                'apply_to'=>'simple,virtual,downloadable,configurable'
            ]
        );
        $eavSetup->addAttribute(
            Product::ENTITY,
            'freeshipping',
            [
                'group' => 'General',
                'type' => 'int',
                'visible' => true,
                'required' => false,
                'input' => 'boolean',
                'user_defined' => true,
                'system' => true,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Free Shipping',
                'apply_to'=>'simple,virtual,downloadable,configurable'
            ]
        );

        $eavSetup->addAttribute(
            Product::ENTITY,
            'springsale',
            [
                'group' => 'General',
                'type' => 'int',
                'visible' => true,
                'required' => false,
                'input' => 'boolean',
                'user_defined' => true,
                'system' => true,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'Spring Sale',
                'apply_to'=>'simple,virtual,downloadable,configurable'
            ]
        );
    }
}