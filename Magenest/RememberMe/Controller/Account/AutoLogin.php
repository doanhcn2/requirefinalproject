<?php

namespace Magenest\RememberMe\Controller\Account;

use \Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\RememberMe\Helper\Data;
use Magenest\RememberMe\Model\TokenFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;


class AutoLogin extends Action
{
    /** @var Data  */
    private $_cookieManager;

    /** @var TokenFactory  */
    private $_tokenFactory;

    /** @var CustomerRepositoryInterface  */
    private $_customerRepository;

    /** @var Session  */
    private $_session;

    /**
     * @var PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * AutoLogin constructor.
     * @param Context $context
     * @param Data $data
     * @param TokenFactory $tokenFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $session
     */
    public function __construct
    (
        Context $context,
        Data $data,
        TokenFactory $tokenFactory,
        CustomerRepositoryInterface $customerRepository,
        Session $session
    )
    {
        $this->_tokenFactory = $tokenFactory;
        $this->_cookieManager = $data;
        $this->_customerRepository = $customerRepository;
        $this->_session = $session;
        parent::__construct($context);
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function execute()
    {
        $cookie = $this->_cookieManager->get(Data::COOKIE_NAME);
        $result = [
            'success' => false
        ];
        if ($cookie) {
            $cookie = json_decode($cookie, true);
            $token = $this->_tokenFactory->create()->getCollection();
            $token->addFieldToFilter('email', $cookie['username']);
            $data = $token->getFirstItem();
            if ($data->getToken() == $cookie['token']) {
                try {
                    $customer = $this->_customerRepository->get($data->getEmail());
                    $this->_session->setCustomerDataAsLoggedIn($customer);
                    $this->_session->regenerateId();
                    if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                        $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                        $metadata->setPath('/');
                        $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                    }
                    $result = [
                        'success' => true
                    ];
                }
                catch (NoSuchEntityException $e) {
                    $data->delete();
                    $this->_cookieManager->delete(Data::COOKIE_NAME);
                }
            }
        }
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        $response->setData($result);
        return $response;
    }
}