<?php

namespace Magenest\RememberMe\Observer;

use Magenest\RememberMe\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\RememberMe\Model\TokenFactory;

class CustomerLogout implements ObserverInterface
{
    /**
     * @var Data
     */
    private $_cookieManager;

    /** @var TokenFactory  */
    private $_tokenFactory;

    public function __construct(
        Data $data,
        TokenFactory $tokenFactory
    )
    {
        $this->_cookieManager = $data;
        $this->_tokenFactory = $tokenFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getCustomer();
        $token = $this->_tokenFactory->create();
        $collection = $token->getCollection()->addFieldToFilter('email', $customer->getEmail());
        foreach ($collection as $item) {
            $item->delete();
        }
        $this->_cookieManager->delete('r');
    }
}