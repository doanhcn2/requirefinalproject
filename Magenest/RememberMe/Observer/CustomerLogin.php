<?php

namespace Magenest\RememberMe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magenest\RememberMe\Model\TokenFactory;
use Magenest\RememberMe\Helper\Data as CustomCookie;


class CustomerLogin implements ObserverInterface
{
    /** @var RequestInterface  */
    private $_request;

    /** @var TokenFactory  */
    private $_tokenFactory;

    /** @var CustomCookie  */
    private $_cookieManager;

    public function __construct(
        RequestInterface $request,
        TokenFactory $tokenFactory,
        CustomCookie $data
    )
    {
        $this->_request = $request;
        $this->_tokenFactory = $tokenFactory;
        $this->_cookieManager = $data;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $post = $this->_request->getPost();
        if (isset($post['login'])) {
            $login = $post['login'];
            if (isset($login['remember']) && $login['remember'] == "on") {
                $token = $this->_tokenFactory->create();
                $collection = $token->getCollection()->addFieldToFilter('email', $login['username']);
                foreach ($collection as $item) {
                    $item->delete();
                }
                $currentTime = time();
                $shaToken = sha1($login['username'].$currentTime.$login['password']);
                $token->setData([
                    'token' => $shaToken,
                    'email' => $login['username']
                ]);
                $token->save();
                $loginDetails = array('token' => $shaToken, 'remchkbox' => 1, 'username' => $login['username']);
                $loginDetails = json_encode($loginDetails);
                $this->_cookieManager->set($loginDetails);
            }
        }
    }
}