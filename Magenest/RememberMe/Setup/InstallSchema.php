<?php

namespace Magenest\RememberMe\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->startSetup();
        $installer = $setup;
        $tableName = 'magenest_remember_me';
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'user email'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                80,
                [
                    'nullable' => false,
                ],
                'user email'
            )->addColumn(
                'token',
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                'token'
            );
        $installer->getConnection()->createTable($table);
        $setup->endSetup();
    }
}