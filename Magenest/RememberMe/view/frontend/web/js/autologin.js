define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function ($, customerData) {
    'use strict';
    var baseLoginUrl = "";
    function autoLogin (customer) {
        var cookie = JSON.parse($.cookie('r'));
        if ($.isEmptyObject(customer) === false && !customer["fullname"] && cookie && cookie['token']) {
            $.ajax({
                url: baseLoginUrl,
                dataType: 'json',
                type: 'POST',
                data: [],
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                success: function (r) {
                    if (r['success'] === true) {
                        $.when(customerData.reload([], false)).always( function () {
                            $('body').trigger('processStop');
                        });
                    } else {
                        $('body').trigger('processStop');
                    }
                },
                error: function (r) {
                    console.log(r);
                    $('body').trigger('processStop');
                }
            });
        }
    }

    return function (config) {
        baseLoginUrl = config.baseLoginUrl;
        autoLogin(customerData.get('customer')());
        customerData.get('customer').subscribe(autoLogin);
    }
});