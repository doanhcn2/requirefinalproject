<?php
/**
 * Created by PhpStorm.
 * User: congtt
 * Date: 27/03/2018
 * Time: 07:53
 */

namespace Magenest\RememberMe\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Token extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_remember_me', 'id');
    }
}