<?php
namespace Magenest\RememberMe\Model\ResourceModel\Token;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected $_idFieldName = "id";

    protected function _construct()
    {
        $this->_init(
            'Magenest\RememberMe\Model\Token',
            'Magenest\RememberMe\Model\ResourceModel\Token'
        );
    }
}
