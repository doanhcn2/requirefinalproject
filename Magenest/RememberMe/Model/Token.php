<?php
/**
 * Created by PhpStorm.
 * User: congtt
 * Date: 27/03/2018
 * Time: 07:53
 */

namespace Magenest\RememberMe\Model;

use Magento\Framework\Model\AbstractModel;

class Token extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\RememberMe\Model\ResourceModel\Token');
    }
}