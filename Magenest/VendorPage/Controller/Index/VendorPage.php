<?php

namespace Magenest\VendorPage\Controller\Index;

use Magento\Framework\Exception\LocalizedException;

class VendorPage extends \Magento\Framework\App\Action\Action
{
    const ID_PARAM_KEY = 'id';
    const VENDOR_MODEL_REGISTRY_KEY = 'vendor_model';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_pageFactory;

    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $udHlp;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Action\Context $context,
        \Unirgy\Dropship\Helper\Data $udHlp,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        $this->registry     = $registry;
        $this->udHlp        = $udHlp;

        return parent::__construct($context);
    }

    public function execute()
    {
        $vendorId   = $this->getRequest()->getParam(self::ID_PARAM_KEY);
        $resultPage = $this->_pageFactory->create();

        try {
            if ($vendorId) {
                $vendorModel = $this->udHlp->getVendor($vendorId);
            } else {
                /** @var \Unirgy\DropshipMicrosite\Helper\Data $_msHlp */
                $_msHlp      = $this->_objectManager->get(\Unirgy\DropshipMicrosite\Helper\Data::class);
                $vendorModel = $_msHlp->getCurrentVendor();
            }
            $this->registry->register(self::VENDOR_MODEL_REGISTRY_KEY, $vendorModel);

            $resultPage->getConfig()->getTitle()->set($vendorModel->getVendorName());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Vendor not found"));
        }

        return $resultPage;
    }
}