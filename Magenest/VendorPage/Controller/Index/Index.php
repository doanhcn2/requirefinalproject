<?php

namespace Magenest\VendorPage\Controller\Index;

class Index extends \Unirgy\DropshipMicrositePro\Controller\Index\Index
{
    public function execute()
    {
        $vendor = $this->_helperData->getCurrentVendor();
        if ($vendor) {
            $path = trim($this->getRequest()->getPathInfo(), '/');
            if ($path === 'listing') {
                $this->_forward('landingPage', 'index', 'umicrosite');
            } else {
                $this->_forward('vendorPage');
            }

            return;
        }
        $this->_forward('index', 'index', 'cms');
    }
}
