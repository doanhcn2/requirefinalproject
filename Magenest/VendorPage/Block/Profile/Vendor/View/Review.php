<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 9/25/18
 * Time: 11:03 AM
 */

namespace Magenest\VendorPage\Block\Profile\Vendor\View;


/**
 * Class Review
 * @package Magenest\VendorPage\Block\Profile\Vendor\View
 */
class Review extends \Magenest\VendorPage\Block\Profile\Vendor\View //\Magento\Review\Block\Product\Review
{
    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $_reviewFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magenest\SellYours\Helper\RatingHelper $ratingHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Unirgy\DropshipMicrosite\Helper\Data $msHlp,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        array $data = []
    ) {

        $this->_reviewFactory   = $reviewFactory;
        $this->_orderRepository = $orderRepository;
        parent::__construct($context, $ratingHelper, $customerSession, $msHlp, $registry, $data);
    }

    /**
     * @param $orderId
     * @return int|null
     */
    public function orderId2customerId($orderId)
    {
        return $this->_orderRepository->get($orderId)->getCustomerId();
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public function getCustomerReviewsCollection($customerId)
    {
        $collection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($collection);
        $this->orderItems($collection);
        $this->orders($collection);
        $collection->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', ['IN' => [
                \Unirgy\DropshipVendorRatings\Helper\Data::ENTITY_TYPE_ID,
                \Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID
            ]])
            ->addCustomerFilter($customerId)
            ->setDateOrder()
            ->addRateVotes();

        return $collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    private function shipments($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['sm' => 'sales_shipment'],
                "main_table.rel_entity_pk_value = sm.entity_id",
                ['review_entity_id' => 'main_table.entity_id', 'shipment_order_id' => 'sm.order_id', '*']
            );

        return $collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    private function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'sales_order_item'],
                "main_table.rel_entity_pk_value = soi.item_id"
            );

        return $collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    private function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => 'sales_order'],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );

        return $collection;
    }

}