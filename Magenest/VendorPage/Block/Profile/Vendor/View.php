<?php
/**
 * Created by PhpStorm.
 * User: chung
 * Date: 9/25/18
 * Time: 9:13 AM
 */

namespace Magenest\VendorPage\Block\Profile\Vendor;

use Magenest\VendorPage\Controller\Index\VendorPage as ViewController;
use Magento\Framework\Exception\LocalizedException;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\SellYours\Helper\RatingHelper
     */
    protected $_ratingHelper;

    /**
     * @var \Unirgy\Dropship\Model\Vendor
     */
    protected $vendorModel;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $_msHlp;

    /**
     * View constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magenest\SellYours\Helper\RatingHelper $ratingHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Unirgy\DropshipMicrosite\Helper\Data $msHlp
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magenest\SellYours\Helper\RatingHelper $ratingHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Unirgy\DropshipMicrosite\Helper\Data $msHlp,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_msHlp           = $msHlp;
        $this->_coreRegistry    = $registry;
        $this->_ratingHelper    = $ratingHelper;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return \Unirgy\Dropship\Model\Vendor
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendor()
    {
        if (!$this->vendorModel) {
            if ($this->vendorModel = $this->_coreRegistry->registry(ViewController::VENDOR_MODEL_REGISTRY_KEY)) {
                return $this->vendorModel;
            } else {
                $this->vendorModel = $this->_msHlp->getCurrentVendor();
            }
            if (!$this->vendorModel->getId()) {
                throw new LocalizedException(__("Vendor not found."));
            }
        }

        return $this->vendorModel;
    }

    /**
     * @param int $maxPoint
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendorAvgRate($maxPoint = 5)
    {
        return round($this->_ratingHelper->getVendorAvgRate($this->getVendor(), $maxPoint), 1);
    }

    /**
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendorReviewCollection()
    {
        return $this->_ratingHelper->getVendorReviewsCollection($this->getVendor());
    }

    /**
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAvgRateCollection()
    {
        return $this->_ratingHelper->getAvgRateCollectionByVendor($this->getVendor(), true);
    }

    public function getPositiveReviewPercent()
    {
        $rate = $this->_ratingHelper->getRateCollectionByVendor($this->getVendor())->count();
        if ($rate === 0) {
            return 0;
        }

        $positiveRate = $this->_ratingHelper->getPositiveRateCollectionByVendor($this->getVendor())->count();

        return round($positiveRate / $rate * 100, 0);
    }

    /**
     * @param \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection $ratings
     * @return float
     */
    public function getSummaryRate($ratings)
    {
        if (!$count = count($ratings)) {
            return 0;
        }
        $sum = 0;
        foreach ($ratings as $rating) {
            $sum += $rating->getValue();
        }
        $avg = $sum / $count * 20;

        return round($avg, 2);
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('udqa/customer/post');
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedId()
    {
        return $this->_customerSession->isLoggedIn();
    }
}