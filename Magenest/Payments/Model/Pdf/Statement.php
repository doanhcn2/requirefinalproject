<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/05/2018
 * Time: 20:22
 */
namespace Magenest\Payments\Model\Pdf;
class Statement extends \Unirgy\Dropship\Model\Pdf\Statement{

    public function addStatement($statement)
    {
        $hlp = $this->_hlp;
        $this->setStatement($statement);

        $ordersData = \Zend_Json::decode($statement->getOrdersData());

        // first front page header
        if(!isset($this->_curPageNum)) {
            $this->_curPageNum = 0;
        }
        $this->addPage()->insertPageHeader(array('first'=>true, 'data'=>$ordersData));

        if (!empty($ordersData['orders'])) {
            // iterate through orders
            foreach ($ordersData['orders'] as $order) {
                $this->insertOrder($order);
            }
        } else {
            $this->text(__('No orders found for this period.'), 'down')
              ->moveRel(0, .5);
        }

        $this->insertTotals($ordersData['totals']);

        $this->insertAdjustmentsPage(/*array('first'=>true, 'data'=>$ordersData)*/);

        if ($this->_hlp->isStatementRefundsEnabled()) {
            $this->insertRefundsPage(/*array('first'=>true, 'data'=>$ordersData)*/);
        }

        if ($hlp->isUdpayoutActive()) {
            $this->insertPayoutsPage(/*array('first'=>true, 'data'=>$ordersData)*/);
        }

        $this->setAlign('left')->font('normal', 10);
        foreach ($this->_pageFooter as $k=>&$p) {
            if (!empty($p['done'])) {
                continue;
            }
            $p['done'] = true;
            $str = __('%1 for %2 - Page %3 of %4',
              $statement->getVendor()->getVendorName(),
              $statement->getStatementPeriod(),
              $p['page_num'],
              $this->_curPageNum
            );
            $this->setPage($this->getPdf()->pages[$k])->move(.5, 10.6)->text($str);
        }
        unset($p);
        #$this->font('normal', 10)->setAlign('right')->addPageNumbers(8.25, .25);

        if (($vId = $statement->getVendor()->getId())) {
            $totals = $ordersData['totals'];
            $totals['vendor_name'] = $statement->getVendor()->getVendorName();
            $this->_globalTotals[$vId] = $totals;
            $this->_globalTotalsAmount[$vId] = isset($ordersData['totals_amount']) ? $ordersData['totals_amount'] : $ordersData['totals'];
        }

        return $this;
    }
}