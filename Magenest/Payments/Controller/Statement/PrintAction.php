<?php

namespace Magenest\Payments\Controller\Statement;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintAction extends Action
{
    protected $checkoutSession;
    protected $_fileFactory;
    protected $customerSession;
    protected $statement;
    protected $pdfFactory;
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Unirgy\Dropship\Model\Vendor\StatementFactory $statement,
        \Unirgy\Dropship\Model\Pdf\StatementFactory $pdfFactory
    )
    {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->_fileFactory = $fileFactory;
        $this->pdfFactory = $pdfFactory;
        $this->statement = $statement;
    }

    public function execute()
    {
        try {
            /** @var \Unirgy\Dropship\Model\Pdf\Statement $generator */
            $generator = $this->pdfFactory->create();
            $generator->before();
            $id = $this->getRequest()->getParam('statement_id');
            /** @var \Unirgy\Dropship\Model\Vendor\Statement $statement */
            $statement = $this->statement->create();
            $statement->load($id);
            if (!$statement->getId()) {
                throw new \Exception(__("No statements found to print"));
            }
            $generator->addStatement($statement);

            $pdf = $generator->getPdf();
            if (empty($pdf->pages)) {
                throw new \Exception(__('No statements found to print'));
            }
            $generator->insertTotalsPage()->after();
            return $this->_fileFactory->create(
                'statement.pdf',
                $pdf->render(),
                DirectoryList::TMP,
                'application/pdf'
            );
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('There was an error while download vendor statement(s): %1', $e->getMessage()));
        }
        return;
    }
}