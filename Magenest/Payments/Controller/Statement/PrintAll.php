<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/05/2018
 * Time: 19:01
 */
namespace Magenest\Payments\Controller\Statement;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
class PrintAll extends Action{

    protected $_statementFactory;

    protected $_statementPdfFactory;

    protected $_fileFactory;
    public function __construct(
      \Unirgy\DropshipPo\Model\StatementFactory $statementFactory,
      \Magenest\Payments\Model\Pdf\StatementFactory $statementPdf,
      \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
      \Magento\Framework\App\Action\Context $context)
    {
        $this->_fileFactory = $fileFactory;
        $this->_statementPdfFactory = $statementPdf;
        $this->_statementFactory = $statementFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $statementIds = $this->getRequest()->getParam('statement_id');
        if($statementIds!=''){
            /** @var  $generator \Magenest\Payments\Model\Pdf\Statement*/
            $generator = $this->_statementPdfFactory->create();
            $generator->before();
            $idsArray = explode(',', $statementIds);
            $statementsCollection = $this->_statementFactory->create()->getCollection()
              ->addFieldToFilter('vendor_statement_id', ['in' => $idsArray]);
            foreach ($statementsCollection as $statement){
                $generator->addStatement($statement);
            }
            $pdf = $generator->getPdf();
            if (empty($pdf->pages)) {
                throw new \Exception(__('No statements found to print'));
            }
            $generator->insertTotalsPage()->after();
            return $this->_fileFactory->create(
              'statement.pdf',
              $pdf->render(),
              DirectoryList::TMP,
              'application/pdf'
            );
        }
        return;
    }
}