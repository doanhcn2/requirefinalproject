<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 02/05/2018
 * Time: 13:01
 */
namespace Magenest\Payments\Controller\Statement;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Page extends Action{

    protected $_statementsBlock;
    public function __construct(
      \Magenest\Payments\Block\Vendor\Statement\Grid $statementBlock,
      \Magento\Framework\App\Action\Context $context)
    {
        $this->_statementsBlock = $statementBlock;
        $this->_statementsBlock->setTemplate('Magenest_Payments::unirgy/payments/vendor/statement/page.phtml');
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var  $result \Magento\Framework\Controller\Result\Raw*/
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $result->setContents($this->_statementsBlock->toHtml());
        return $result;
        // TODO: Implement execute() method.
    }
}