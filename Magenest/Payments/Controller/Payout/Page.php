<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 03/05/2018
 * Time: 12:55
 */
namespace Magenest\Payments\Controller\Payout;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
class Page extends Action{

    protected $_payoutblock;

    public function __construct(
      \Magenest\Payments\Block\Vendor\Payout\Grid $gridBlock,
      \Magento\Framework\App\Action\Context $context)
    {
        $this->_payoutblock = $gridBlock;
        $this->_payoutblock->setTemplate('Magenest_Payments::unirgy/payments/vendor/payout/page.phtml');
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $result->setContents($this->_payoutblock->toHtml());
        return $result;
        // TODO: Implement execute() method.
    }
}