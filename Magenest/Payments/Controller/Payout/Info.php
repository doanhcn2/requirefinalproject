<?php

namespace Magenest\Payments\Controller\Payout;

class Info extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Payments\Block\Vendor\Payout\Info $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('info');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}