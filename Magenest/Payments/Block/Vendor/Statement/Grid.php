<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\Payments\Block\Vendor\Statement;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Grid extends Template
{
    protected $statement;

    public function __construct(
        Context $context,
        \Unirgy\Dropship\Model\Vendor\StatementFactory $statement,
        array $data = [])
    {
        $this->statement = $statement;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magenest\Payments\Block\Vendor\Statement\Pager $toolbar */
        if ($toolbar = $this->getLayout()->getBlock('statement.grid.toolbar')) {
            $toolbar->setCollection($this->getStatementCollection());
            $this->setChild('toolbar', $toolbar);
        }
        return $this;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getStatementCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 20;
        return $this->statement->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize($pageSize)->setCurPage($page)->setOrder('created_at', 'DES');
    }

    public function getPageLoadUrl(){
        return $this->getUrl('payments/statement/page');
    }
    public function getIsEven($number)
    {
        return $number % 2 == 0;
    }

    public function getVendorId(){
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getCurrentPage(){
        return $this->getRequest()->getParam('p')!=''?$this->getRequest()->getParam('p'):1;
    }
}