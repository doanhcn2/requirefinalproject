<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\Payments\Block\Vendor\Payout;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Items\AbstractItems;
use Unirgy\DropshipPo\Model\PoFactory;

class Info extends AbstractItems
{
    protected $payout;
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        Registry $frameworkRegistry,
        array $data = [])
    {
        $this->payout = $payoutFactory;
        $this->_coreRegistry = $frameworkRegistry;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getPayout()
    {
        if (!$this->hasData('payout')) {
            $id = (int)$this->getRequest()->getParam('payout_id');
            $po = $this->payout->create()->load($id);
            $this->setData('payout', $po);
        }
        return $this->getData('payout');
    }
}
