<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipPo
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

namespace Magenest\Payments\Block\Vendor\Payout;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Grid extends Template
{
    protected $payout;

    protected $_paymentSource;

    public function __construct(
        Context $context,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        \Unirgy\DropshipPayout\Model\Source $source,
        array $data = [])
    {
        $this->payout = $payoutFactory;
        $this->_paymentSource = $source;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magenest\Payments\Block\Vendor\Payout\Pager $toolbar */
        if ($toolbar = $this->getLayout()->getBlock('payout.grid.toolbar')) {
            $toolbar->setCollection($this->getPayoutCollection());
            $this->setChild('toolbar', $toolbar);
        }
        return $this;
    }

    public function getPayoutCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 20;
        return $this->payout->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize($pageSize)->setCurPage($page)->setOrder('created_at', $this->getCreateAtSortOrder());
    }

    public function getCreateAtSortOrder()
    {
        if ($this->getRequest()->getParam('field') == 'payout_date' &&
            ($this->getRequest()->getParam('sort_order') == 'asc' ||
                $this->getRequest()->getParam('sort_order') == 'desc')) {
            return $this->getRequest()->getParam('sort_order');
        }
        return 'desc';
    }

    public function getIsEven($number)
    {
        return $number % 2 == 0;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getPayoutStatusSource()
    {
        return $this->_paymentSource->setPath('payout_status')->toOptionHash();
    }

    public function getSortUrl($field, $sortOrder)
    {
        $params = $this->getRequest()->getParams();
        $params['field'] = $field;
        $params['sort_order'] = $sortOrder;
        return $this->getUrl('payments') . '?' . http_build_query($params);
    }

    public function getLoadPageUrl()
    {
        return $this->getUrl('payments/payout/page');
    }

    public function getCurrentPage()
    {
        return ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
    }

    public function getIsFilter()
    {
        if ($this->getRequest()->getParam('sort_order')) {
            return true;
        }
        return false;
    }
}
