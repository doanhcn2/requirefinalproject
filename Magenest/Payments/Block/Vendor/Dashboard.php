<?php

namespace Magenest\Payments\Block\Vendor;

use \Magento\Framework\View\Element\Template;

class Dashboard extends Template
{

    protected $statementFactory;

    protected $payoutFactory;

    public function __construct(
        Template\Context $context,
        \Unirgy\Dropship\Model\Vendor\StatementFactory $statementFactory,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        array $data = []
    )
    {
        $this->statementFactory = $statementFactory;
        $this->payoutFactory = $payoutFactory;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getEarning()
    {
        $collection = $this->payoutFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
        $amount = 0;
        foreach ($collection as $payout) {
            $amount += $payout->getData('total_payout');
        }
        return (float)$amount;
    }

    public function getPaid()
    {
        $collection = $this->payoutFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->addFieldToFilter('payout_status', 'paid');
        $amount = 0;
        foreach ($collection as $payout) {
            $amount += $payout->getData('total_paid');
        }
        return (float)$amount;
    }

    public function getOwed(){
        return (float)($this->getEarning() - $this->getPaid());
    }
}