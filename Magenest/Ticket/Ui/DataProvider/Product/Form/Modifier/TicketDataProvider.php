<?php
/**
 * Created by Magenest.
 */

namespace Magenest\Ticket\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magenest\Ticket\Model\EventFactory as EventCollection;
use Magenest\Ticket\Model\EventLocationFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketsFactory;

/**
 * Class EventBooking
 *
 * @package Magenest\Ticket\Ui\DataProvider\Product\Form\Modifier
 */
class TicketDataProvider extends AbstractModifier
{

    const PRODUCT_TYPE = 'ticket';
    const CONTROLLER_ACTION_EDIT_PRODUCT = 'catalog_product_edit';
    const CONTROLLER_ACTION_NEW_PRODUCT = 'catalog_product_new';
    const EVENT_TICKET_TAB = 'event';


    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var EventCollection
     */
    protected $event;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var EventLocationFactory
     */
    protected $location;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var TicketsFactory
     */
    protected $tickets;

    /**
     * TicketDataProvider constructor.
     * @param RequestInterface $request
     * @param LocatorInterface $locator
     * @param EventCollection $eventCollection
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param EventLocationFactory $eventLocationFactory
     * @param SessionFactory $eventSessionFactory
     * @param TicketsFactory $ticketsFactory
     */
    public function __construct(
        RequestInterface $request,
        LocatorInterface $locator,
        EventCollection $eventCollection,
        \Psr\Log\LoggerInterface $loggerInterface,
        EventLocationFactory $eventLocationFactory,
        SessionFactory $eventSessionFactory,
        TicketsFactory $ticketsFactory
    )
    {
        $this->logger = $loggerInterface;
        $this->event = $eventCollection;
        $this->request = $request;
        $this->locator = $locator;
        $this->location = $eventLocationFactory;
        $this->session = $eventSessionFactory;
        $this->tickets = $ticketsFactory;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        if ($this->isEventTicket()) {
            if ($productId == null)
                $data['']['product']['price'] = 0;
            /** @var \Magenest\Ticket\Model\Event $eventModel */
            $eventModel = $this->event->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->getFirstItem();
            if ($eventModel) {

                //data email
                if (!empty($eventModel->getEmailConfig())) {
                    $email = $eventModel->getEmailConfig();
                } else {
                    $email = 'emailtemplate_config';
                }
                $data[strval($productId)]['event']['emailtemplate']['emailtemplate_config'] = $email;
                //data on address
                if ($eventModel->getIsOnline() == 1) {
                    /** @var \Magenest\Ticket\Model\EventLocation $model */
                    $model = $this->location->create()->load($productId, 'product_id');
                    $data[strval($productId)]['event']['ticket_address'] = [
                        'venue_title' => $model->getVenueTitle(),
                        'address' => $model->getAddress(),
                        'address2' => $model->getData('address2'),
                        'city' => $model->getCity(),
                        'state' => $model->getState(),
                        'postcode' => $model->getPostcode(),
                        'country' => $model->getCountry(),
                        'use_map' => $eventModel->getUseMap(),
                        'latitude' => $model->getLatitude(),
                        'longitude' => $model->getLongitude(),
                    ];;
                }
                $data[strval($productId)]['event'] = [
                    'is_online' => $eventModel->getIsOnline(),
                    'ticket_address' => $eventModel->getIsOnline() == 1 ? $this->getAddress($productId) : 0,
                    'event_type' => $eventModel->getEventType(),
                    'age' => $eventModel->getAge(),
                    'show_up' => $eventModel->getShowUp(),
                    'show_up_time' => $eventModel->getShowUpTime(),
                    'can_purchase' => $eventModel->getCanPurchase(),
                    'must_print' => $eventModel->getMustPrint(),
                    'refund_policy' => $eventModel->getRefundPolicy() == 'allow_refund' ? '1' : '0',
                    'refund_policy_day' => $eventModel->getRefundPolicyDay(),
                    'public_organizer' => $eventModel->getPublicOrganizer(),
                    'organizer_name' => $eventModel->getOrganizerName(),
                    'organizer_description' => $eventModel->getOrganizerDescription(),
                    'use_map' => $eventModel->getUseMap(),
                    'email_config' => $eventModel->getEmailConfig()
//                    'is_reserved_seating' => $eventModel->getIsReservedSeating()
                ];

//                if ($eventModel->getIsReservedSeating() === "1") {
//                    $data[strval($productId)]['event']['venue_map']['venue_map_choose'] = $eventModel->getVenueMapId();
//                }
                $data[strval($productId)]['event']['event_add_paid_ticket'] = $this->getTicket('paid', $productId);
                $data[strval($productId)]['event']['event_add_free_ticket'] = $this->getTicket('free', $productId);
                $data[strval($productId)]['event']['event_add_donation_ticket'] = $this->getTicket('donation', $productId);
            }
        }

        return $data;
    }


    public function getAddress($id)
    {
        /** @var \Magenest\Ticket\Model\EventLocation $model */
        $model = $this->location->create()->load($id, 'product_id');
        $addrArray = [
            'venue_title' => $model->getVenueTitle(),
            'address' => $model->getAddress(),
            'address2' => $model->getData('address2'),
            'city' => $model->getCity(),
            'state' => $model->getState(),
            'postcode' => $model->getPostcode(),
            'country' => $model->getCountry(),
            'use_map' => $model->getUseMap(),
            'latitude' => $model->getLatitude(),
            'longitude' => $model->getLongitude(),
        ];

        return $addrArray;
    }

    public function getTicket($type, $productId)
    {
        $modelTicket = $this->tickets->create()->getCollection()
            ->addFieldToFilter('type', $type)
            ->addFieldToFilter('product_id', $productId);
        $arrayTicket = [];
        foreach ($modelTicket as $ticket) {
            $array = [
                'tickets_id' => $ticket->getId(),
                'title' => $ticket->getTitle(),
                'description' => $ticket->getDescription(),
                'row_session' => $this->getSessionData($ticket->getId())
            ];
            $arrayTicket[] = $array;
        }

        return $arrayTicket;
    }

    /**
     * @param $id
     * @return array
     */
    public function getSessionData($id)
    {
        $modelSession = $this->session->create()->getCollection()
            ->addFieldToFilter('tickets_id', $id);
        $arraySession = [];
        foreach ($modelSession as $session) {
//            $sessionArray = [
//                'id_session' => $session->getSessionId(),
//                'start_time' =>  $session->getStartTime(),
//                'end_time' => $session->getEndTime(),
////                'max_qty' => $session->getMaxQty(),
//            ];
            $array = $session->getData();
            $array['original_value'] = $array['original_value'] ? $array['original_value'] + 0 : $array['original_value'];
            $array['sale_price'] = $array['sale_price'] ? $array['sale_price'] + 0 : $array['sale_price'];
            $arraySession [] = $array;
        }
        return $arraySession;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        if (!$this->isEventTicket()) {
            $meta['event']['arguments']['data']['config'] = [
                'disabled' => true,
                'visible' => false
            ];
        } else {
            unset($meta['product-details']['children']['container_price']);
            unset($meta['product-details']['children']['quantity_and_stock_status_qty']);
        }

        return $meta;
    }


    /**
     * @return bool
     */
    protected function isEventTicket()
    {
        $actionName = $this->request->getFullActionName();
        $isEventTicket = false;
        if ($actionName == self::CONTROLLER_ACTION_EDIT_PRODUCT) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->locator->getProduct();
            if ($product->getTypeId() == self::PRODUCT_TYPE) {
                $isEventTicket = true;
            }
        } elseif ($actionName == self::CONTROLLER_ACTION_NEW_PRODUCT) {
            if (self::PRODUCT_TYPE == $this->request->getParam('type')) {
                $isEventTicket = true;
            }
        }

        return $isEventTicket;
    }
}
