<?php
/**
 * Created by PhpStorm.
 * User: namnt
 * Date: 04/01/2018
 * Time: 12:56
 */
namespace Magenest\Ticket\Ui\DataProvider;

class Ticket extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $_ticketsFactory ;
    protected $_sessionFactory;
    public function __construct(
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,

        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $ticketFactory->create()->getCollection();
        $this->_ticketsFactory = $ticketsFactory;
        $this->_sessionFactory = $sessionFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $data =  $this->getCollection()->toArray();

        foreach ($data['items'] as $key=>$value) {
            if(array_key_exists("information",$data['items'][$key])){

                $ticketInfo =  unserialize($data['items'][$key]['information']);
                $ticketTitle = $this->_ticketsFactory->create()->load($ticketInfo['tickets_id'])->getTitle();
                $session = $this->_sessionFactory->create()->load($ticketInfo['session_id']);
                $sessionDate = "\nSession : ".$session->getSessionFrom()." to ".$session->getSessionTo();
                $data['items'][$key]['information'] = $ticketTitle.$sessionDate;
            }
        }

        return $data;
    }
}