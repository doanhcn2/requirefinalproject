<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Ui\Component\Config;

/**
 * Class Country
 * @package Magenest\Ticket\Ui\Component
 */
class EventType implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Class'),
                'value' => 'Class'
            ],
            [
                'label' => __('Networking'),
                'value' => 'Networking'
            ],
            [
                'label' => __('Seminar'),
                'value' => 'Seminar'
            ],
            [
                'label' => __('Tour'),
                'value' => 'Tour'
            ],
            [
                'label' => __('Party'),
                'value' => 'Party'
            ],
            [
                'label' => __('Performance'),
                'value' => 'Performance'
            ],
            [
                'label' => __('Conference'),
                'value' => 'Conference'
            ],
            [
                'label' => __('Race'),
                'value' => 'Race'
            ],
            [
                'label' => __('Festival'),
                'value' => 'Festival'
            ],
            [
                'label' => __('Attraction'),
                'value' => 'Attraction'
            ],
            [
                'label' => __('Gala'),
                'value' => 'Gala'
            ],
            [
                'label' => __('Screening'),
                'value' => 'Screening'
            ],
            [
                'label' => __('Game'),
                'value' => 'Game'
            ],
            [
                'label' => __('Retreat'),
                'value' => 'Retreat'
            ],
            [
                'label' => __('Appearance'),
                'value' => 'Appearance'
            ],
            [
                'label' => __('Expo'),
                'value' => 'Expo'
            ],
            [
                'label' => __('Tournament'),
                'value' => 'Tournament'
            ],
            [
                'label' => __('Convention'),
                'value' => 'Convention'
            ],
            [
                'label' => __('Rally'),
                'value' => 'Rally'
            ],
            [
                'label' => __('Other'),
                'value' => 'Other'
            ]
        ];
    }
}
