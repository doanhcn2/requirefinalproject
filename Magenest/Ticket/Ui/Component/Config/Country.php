<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Ui\Component\Config;

/**
 * Class Country
 * @package Magenest\Ticket\Ui\Component
 */
class Country implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $country;

    /**
     * Country constructor.
     * @param \Magento\Directory\Model\Config\Source\Country $country
     */
    public function __construct(
        \Magento\Directory\Model\Config\Source\Country $country
    ) {
        $this->country = $country;
    }

    public function toOptionArray()
    {
        return $this->country->toOptionArray();
    }
}
