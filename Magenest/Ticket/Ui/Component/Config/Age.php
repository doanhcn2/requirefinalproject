<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Ui\Component\Config;

/**
 * Class Age
 * @package Magenest\Ticket\Ui\Component
 */
class Age implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('All Ages'),
                'value' => 'All Ages'
            ],
            [
                'label' => __('Up to 13 years Old'),
                'value' => 'Up to 13 years Old'
            ],
            [
                'label' => __('13 to 18 years Old'),
                'value' => '13 to 18 years Old'
            ],
            [
                'label' => __('Adults 18+'),
                'value' => 'Adults 18+'
            ],
            [
                'label' => __('Adults 21+'),
                'value' => 'Adults 21+'
            ],
            [
                'label' => __('Adults 30+'),
                'value' => 'Adults 30+'
            ],
            [
                'label' => __('Seniors'),
                'value' => 'Seniors'
            ]
        ];
    }
}
