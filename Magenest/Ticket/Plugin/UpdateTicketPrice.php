<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Plugin;

use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Action\Context;

class UpdateTicketPrice
{
    /**
     * @var $_eventFactory EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var $session SessionFactory
     */
    protected $session;
    /**
     * @var $context Context
     */
    protected $context;

    public function __construct(EventFactory $eventFactory,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                SessionFactory $sessionFactory,
                                Context $context)
    {
        $this->session = $sessionFactory;
        $this->context = $context;
        $this->messageManager = $messageManager;
        $this->_eventFactory = $eventFactory;
    }

    public function afterGetFinalPrice(Product $product, $result)
    {
        $data = $product->getData();
        if (@$data['type_id'] === 'ticket') {
            $productId = @$data['entity_id'];
            if ($this->isEventTicketProduct($productId)) {
                $finalPrice = $this->getProductTicketPrice($productId);
                if(!$finalPrice)
                    $product->setFinalPrice($finalPrice);
                return $result;
            }
        }
        return $result;
    }

    private function isEventTicketProduct($productId)
    {
        return $this->_eventFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData() !== null ? true : false;
    }

    private function getProductTicketPrice($productId) {
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        if($sessionTicket->count()==0)
            return null;
        $result = [];

        foreach ($sessionTicket as $session) {
            if ($session->getSalePrice()) {
                array_push($result, $session->getSalePrice());
            } else {
                array_push($result, $session->getOriginalValue());
            }
        }

        return min($result);
    }
}
