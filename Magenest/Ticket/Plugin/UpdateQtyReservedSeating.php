<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Plugin;

use Magenest\Ticket\Model\EventFactory;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Context;

class UpdateQtyReservedSeating
{
    /**
     * @var $_eventFactory EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var $context Context
     */
    protected $context;

    public function __construct(EventFactory $eventFactory,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                Context $context)
    {
        $this->context = $context;
        $this->messageManager = $messageManager;
        $this->_eventFactory = $eventFactory;
    }

    public function beforeUpdateItems(Cart $cart, $data)
    {
        $cartData = $cart->getData();
        $quote = $cartData['quote'];
        $quoteItems = $quote->getItems();
        $newData = [];
        foreach ($data as $key => $datum) {
            foreach ($quoteItems as $quoteItem) {
                $quoteItemId = intval($quoteItem->getDataByKey('item_id'));
                if ($quoteItemId === $key) {
                    $productId = intval($quoteItem->getDataByKey('product_id'));
                    if ($this->isReservedSeatingProduct($productId) && $datum['qty'] !== 1) {
                        $newData[$key] = ['qty' => 1, 'before_suggest_qty' => 1];
                        $this->messageManager->addWarningMessage('You can\'t edit the quantity of reserved seating event product.');
                    } else {
                        $newData[$key] = $datum;
                    }
                }
            }
        }
        return [$newData];
    }

    private function isReservedSeatingProduct($productId)
    {
        return $this->_eventFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem()->getIsReservedSeating() === "1" ? true : false;
    }
}
