<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Model;

use Magenest\Redeem\Model\Redemption;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\App\Area;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Ticket\Model\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Groupon\Helper\Pdf;
use Magenest\Ticket\Helper\Information;
use Unirgy\Dropship\Helper\Data;

/**
 * Class Ticket
 * @package Magenest\Ticket\Model
 *
 * @method setCode(string $code)
 * @method string getCode()
 * @method string getCustomerName()
 * @method int getCustomerId()
 * @method string getCustomerEmail()
 * @method string getTitle()
 * @method string getOrderIncrementId()
 * @method int getEventId()
 * @method Ticket setStatus(int $status)
 * @method string getNote()
 * @method string getInformation()
 * @method int getQty()
 * @method int getProductId()
 */
class Ticket extends AbstractModel
{
    const STATUS_AVAILABLE = 1;
    const STATUS_USED = 2;
    const STATUS_REFUNDED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_EXPIRED = 5;
    const CUSTOMER_STATUS_USED = 2;

    const STATUS_REDEEMED_TEXT = 'redeemed';
    const STATUS_USED_TEXT = 'used';
    const STATUS_NOTFOUND_TEXT = 'not found';
    const STATUS_EXPIRED_TEXT = 'expired';
    const STATUS_CANCELED_TEXT = 'canceled';

    /**
     * Const Email
     */
    const XML_PATH_EMAIL_SENDER = 'trans_email/ident_general/email';

    /**
     * Const Name
     */
    const XML_PATH_NAME_SENDER = 'trans_email/ident_general/name';

    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_ticket_ticket';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_ticket_ticket';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_ticket_ticket';

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Pdf
     */
    protected $_pdf;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var SessionFactory
     */
    protected $session;


    protected $_eventLocationFactory;

    protected $_ticketsFactory;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Ticket\Model\ResourceModel\Ticket');
    }

    /**
     * Ticket constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param SessionFactory $eventSessionFactory
     * @param EventFactory $eventFactory
     * @param Pdf $pdf
     * @param Information $information
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        SessionFactory $eventSessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        EventFactory $eventFactory,
        Pdf $pdf,
        Information $information,
        array $data = []
    ) {
        parent::__construct($context, $registry);
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_storeManager = $storeManager;
        $this->_eventFactory = $eventFactory;
        $this->_pdf = $pdf;
        $this->_eventLocationFactory = $eventLocationFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->session = $eventSessionFactory;
        $this->information = $information;
    }

    /**
     * @param $ticketId
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Zend_Pdf_Exception
     * @throws \Exception
     */
    public function sendMail($ticketId)
    {
//        if (!$this->getId()) {
//            return;
//        }
        $model = $this->load($ticketId);
        $info = unserialize($model->getInformation());
        $session = $this->session->create()->load($info['session_id']);
        $ticketInfo = $this->_ticketsFactory->create()->load($info['tickets_id']);
        $location = $this->_eventLocationFactory->create()->getCollection()->addFieldToFilter('event_id', $this->getEventId());
        if ($location->count() > 0)
            $location = $location->getFirstItem();
        else
            $location = null;
        $pdf = $this->_pdf->getPdf(null, $this, 'ticket');
        $file = $pdf->render();
        $this->inlineTranslation->suspend();
        $emailTemplate = "ticket_mail_template";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('\Magento\Sales\Model\Order')
            ->load($model->getOrderId());
        $orderItem = $order->getItemById($model->getOrderItemId());

        $transport = $this->_transportBuilder->setTemplateIdentifier($emailTemplate)->setTemplateOptions(
            [
                'area' => Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            [
                'store' => $this->_storeManager->getStore(),
                'store URL' => $this->_storeManager->getStore()->getBaseUrl(),
                'ticket_code' => $this->getCode(),
                'customer_name' => $this->getCustomerName(),
                'title' => $this->getTitle(),
                'location_title' => ($location) ? $location->getVenueTitle() : "Online Event",
                'location_detail' => ($location) ? $location->getAddress() . ", " . $location->getCity() : "none",
                'from' => $session->getSessionFrom(),
                'to' => $session->getSessionTo(),
                'qty' => $orderItem->getQtyOrdered() + 0,
                'option_type' => $ticketInfo->getTitle()
            ]
        )->setFrom(
            [
                'email' => $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER),
                'name' => $this->_scopeConfig->getValue(self::XML_PATH_NAME_SENDER)
            ]
        )->addTo(
            $this->getCustomerEmail(),
            $this->getCustomerName()
        )->createAttachment($file)->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        $model = $this->_eventFactory->create();
        return $model->load($this->getEventId());
    }

    /**
     * @param string $code
     * @param null $vendorId
     * @return string
     * @throws \Exception
     */
    public function redeem($code = '', $vendorId = null)
    {
        if ($this->getData('ticket_id') && @$this->getData('status')) {
            if ($this->getData('status') == self::STATUS_AVAILABLE) {
                //available
                $redeem = $this->redeemATicket();
                if ($redeem) {
                    return $this->saveLog($this->getRedemptionCode(), self::STATUS_REDEEMED_TEXT, __("Ticket is valid for ") . $this->getData('title') . '.');
                } else {
                    return $this->saveLog($this->getRedemptionCode(), self::STATUS_EXPIRED_TEXT, __("Ticket was expired."));
                }
            } elseif ($this->getData('status') == self::STATUS_USED) {
                //used
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_USED_TEXT, __("Ticket was previously used on ") . @$this->getData('redeemed_at') . '.');
            } elseif ($this->getData('status') == self::STATUS_REFUNDED) {
                //refunded
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_EXPIRED_TEXT, __("Ticket was expired."));
            } else {
                //error
                return $this->saveLog($this->getRedemptionCode(), self::STATUS_NOTFOUND_TEXT, __("Ticket is not valid."));
            }
        }
        return $this->saveLog($code, self::STATUS_NOTFOUND_TEXT, __("Ticket is not valid."));
    }

    /**
     * @throws \Exception
     */
    public function redeemATicket()
    {
        if ($this->getData('ticket_id') && @$this->getData('status')) {
            if ($this->getData('status') == self::STATUS_AVAILABLE) {
                //available
                $sessionEnd = $this->session->create()->load($this->getSessionId())->getSessionTo();
                if (time() <= strtotime($sessionEnd) + 86400) {
                    $this->setData('status', self::STATUS_USED);
                    $this->setData('redeemed_at', date("Y-m-d H:i:s"));
                    $this->setData('updated_at', date("Y-m-d H:i:s"));
                    $this->save();

                    return true;
                } else {
                    $this->setData('status', self::STATUS_EXPIRED);
                    $this->setData('redeemed_at', date("Y-m-d H:i:s"));
                    $this->setData('updated_at', date("Y-m-d H:i:s"));
                    $this->save();

                    return false;
                }
            }
        }
        return false;
    }

    public function markAsUsed()
    {
        if ($this->getId()) {
            $this->setData('customer_status', self::CUSTOMER_STATUS_USED);
            $this->setData('updated_at', date("Y-m-d H:i:s"));
            try {
                $this->save();
            } catch (\Exception $e) {
                return false;
            }
        }

        return true;
    }

    public function loadByCode($code, $vendorId)
    {
        $this->unsetData();
        $this->load($code, 'redemption_code');
        if ($this->getVendorId() !== $vendorId) {
            $this->unsetData();
        }
        return $this;
    }

    protected function saveLog($code, $status, $mess = '')
    {
        $log = ObjectManager::getInstance()->create(Redemption::class);
        $log->setData('code', @$code);
        $log->setData('vendor_id', $this->getUnirgySession()->getVendorId());
        $log->setData('status', $status);
        $log->setData('message', $mess);
        $log->setData('redeemed_at', date('Y-m-d H:i:s', time()));
        $log->save();
        return json_encode($log->getData());
    }

    public function needLocationConfirmation()
    {
        return false;
    }

    private function getUnirgySession()
    {
        if (ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')) {
            return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        } else {
            return ObjectManager::getInstance()->create('Unirgy\Dropship\Model\Session');
        }
    }
}
