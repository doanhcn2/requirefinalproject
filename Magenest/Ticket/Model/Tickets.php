<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Model;

use Magento\Framework\Model\AbstractModel;


/**
 * Class Tickets
 * @package Magenest\Ticket\Model
 *
 * @method int getTicketsId()
 * @method string getTitle()
 * @method string getDescription()
 * @method string getSessions()
 * @method string getType()
 * @method string getQtyAvailable()
 * @method int getMaxQty()
 * @method string getCommission()
 * @method string getProductId()
 */
class Tickets extends AbstractModel
{

    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_ticket_tickets';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_ticket_tickets';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_ticket_tickets';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Ticket\Model\ResourceModel\Tickets');
    }
}
