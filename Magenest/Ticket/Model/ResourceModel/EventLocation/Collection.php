<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Model\ResourceModel\EventLocation;

use Magenest\Ticket\Model\EventLocation;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magenest\Ticket\Model\ResourceModel\EventLocation
 */
class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'location_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_ticket_location_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'event_location_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Ticket\Model\EventLocation', 'Magenest\Ticket\Model\ResourceModel\EventLocation');
    }

    public function getLocationFilterData()
    {
        $locationValues = [];
        $locationKeys   = [];
        /** @var EventLocation $location */
        foreach ($this as $location) {
            $locationKey = $location->getTicketLocationKey();
            if (!in_array($locationKey, $locationKeys)) {
                $locationValue = $location->getTicketLocation();
                array_push($locationKeys, $locationKey);
                array_push($locationValues, $locationValue);
            }
        }

        return array_combine($locationKeys, $locationValues);
    }
}
