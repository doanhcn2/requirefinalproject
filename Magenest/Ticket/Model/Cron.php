<?php

namespace Magenest\Ticket\Model;

/**
 * Class Cron
 * @package Magenest\Ticket\Model
 */
class Cron {

    /** @var \Psr\Log\LoggerInterface $logger */
    protected $logger;

    /** @var \Magento\Framework\ObjectManagerInterface */
    protected $objectManager;

    /**
     * Cron constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\ObjectManagerInterface
        $objectManager
    ) {
        $this->logger = $logger;
        $this->objectManager = $objectManager;
    }

    /**
     *
     */
    public function checkStatus() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->get('Magenest\Ticket\Helper\Status')->changeStatus();
    }
}