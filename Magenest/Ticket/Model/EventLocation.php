<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Model;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;

/**
 * Class EventLocation
 *
 * @package Magenest\Ticket\Model
 *
 * @method int getLocationId()
 * @method int getEventId()
 * @method int getProductId()
 * @method string getVenueTitle()
 * @method string getAddress()
 * @method string getAddress2()
 * @method string getCity()
 * @method string getState()
 * @method string getPostcode()
 * @method string getCountry()
 * @method string getLongitude()
 * @method string getLatitude()
 * @method int getUseMap()
 */
class EventLocation extends AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_ticket_location';

    /**
     * Product Type
     */
    const PRODUCT_TYPE = 'ticket';
    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_ticket_location';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_ticket_location';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Ticket\Model\ResourceModel\EventLocation');
    }

    public function getTicketLocationKey()
    {
        $locationDetails = "";
        if ($this->getRegion()) {
            $locationDetails .= $this->getRegion() . "||";
        } elseif ($this->getCity()) {
            $locationDetails .= $this->getCity() . "||";
        }
        if ($this->getCountry()) {
            $locationDetails .= $this->getCountry();
        }

        return $locationDetails;
    }

    public function getTicketLocation()
    {
        $locationDetails = "";
        if ($this->getRegion()) {
            $locationDetails .= $this->getRegion() . ", ";
        } elseif ($this->getCity()) {
            $locationDetails .= $this->getCity() . ", ";
        }
        if ($this->getCountry()) {
            $locationDetails .= ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($this->getCountry());
        }

        return $locationDetails;
    }
}
