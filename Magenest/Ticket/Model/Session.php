<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Session
 * @package Magenest\Ticket\Model
 *
 * @method int getSessionId()
 * @method int getProductId()
 * @method int getSessionFrom()
 * @method string getSessionTo()
 * @method string getSellFrom()
 * @method string getSellTo()
 * @method int getSalePrice()
 * @method int getOriginalValue()
 * @method int getQty()
 * @method int getQtyAvailable()
 * @method int getQtyPurchased()
 * @method int getMaxQty()
 * @method int getHideSoldOut()
 * @method int getMinDonation()
 * @method int getTicketsId()
 *
 */
class Session extends AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_ticket_sessions';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_ticket_sessions';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_ticket_sessions';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Ticket\Model\ResourceModel\Session');
    }
}
