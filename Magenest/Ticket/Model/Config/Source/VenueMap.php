<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magenest\VendorTicket\Model\VenueMapFactory;

class VenueMap implements ArrayInterface
{
    /**
     * @var $_venueMapFactory VenueMapFactory
     */
    protected $_venueMapFactory;

    public function __construct(VenueMapFactory $venueMapFactory)
    {
        $this->_venueMapFactory = $venueMapFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $allVenueMapOptions = $this->_venueMapFactory->create()->getCollection();
        $result = [];
        foreach ($allVenueMapOptions as $options) {
            $item = ['label' => $options->getVenueMapName(), 'value' => $options->getVenueMapId()];
            array_push($result, $item);
        }
        return $result;
    }
}
