<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Model\Quote\Item;

use Magento\Framework\Event\Observer;
use Magento\CatalogInventory\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * Class QuantityValidator
 * @package Magenest\Ticket\Model\Quote\Item
 */
class QuantityValidator
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    protected $_sessionFactory;

    /**
     * QuantityValidator constructor.
     * @param \Magento\Framework\Message\ManagerInterface $managerInterface
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        LoggerInterface $loggerInterface
    )
    {
        $this->_sessionFactory = $sessionFactory;
        $this->messageManager = $managerInterface;
        $this->_logger = $loggerInterface;
    }

    /**
     * Check product option event data when quote item quantity declaring
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function validate(Observer $observer)
    {
        /* @var $quoteItem \Magento\Quote\Model\Quote\Item */
        $quoteItem = $observer->getEvent()->getItem();
        $buyInfo = $quoteItem->getBuyRequest();
        $options = $buyInfo->getAdditionalOptions();
        if (!$quoteItem ||
            !$quoteItem->getProductId() ||
            !$quoteItem->getQuote() ||
            $quoteItem->getQuote()->getIsSuperMode()
        ) {
            return;
        }
        $qty = $quoteItem->getQty();
        $quote = $quoteItem->getQuote();
        if (!$quoteItem->getItemId()) {
            $allItems = $quote->getAllItems();
            $productId = $quoteItem->getProductId();
            foreach ($allItems as $itemOld) {
                /* @var $item \Magento\Quote\Model\Quote\Item */
                if ($itemOld->getProduct()->getId() == $productId && $itemOld->getItemId()) {
                    $sessionIdNew = $options['ticket_session'];
                    $sessionIdOld = $itemOld->getBuyRequest()
                        ->getAdditionalOptions()['ticket_session'];
                    if ($sessionIdOld == $sessionIdNew) {
                        $qty += $itemOld->getQty();
                    }
                }
            }

        }
        if (!empty($options)) {
            $session = $this->_sessionFactory->create()->load($options['ticket_session']);
            if ($session)
                if ($qty > $session->getMaxQty() && $session->getMaxQty() > 0 || $qty > $session->getQtyAvailable())
                    $this->getErrorMessage($session, $quoteItem);
        }
    }

    /**
     * @param $session
     * @param $quoteItem
     */
    public function getErrorMessage($session, $quoteItem)
    {
        $availableQty = $session->getQtyAvailable();
        $maxQty = $session->getMaxQty();
        $max = min($availableQty, $maxQty);
        $quoteItem->addErrorInfo(
            'error_info',
            Data::ERROR_QTY,
            __('You just can buy max ' . $max . ' ticket(s) for this session.')
        );
        return;
    }
}
