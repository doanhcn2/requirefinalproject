<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Magenest\Ticket\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /**
             * Add more column to magenest_ticket_event table for new reserved seating booking product type
             */
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'is_reserved_seating',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => null,
                    'comment' => 'Is reserved seating event'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_event'),
                'venue_map_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 15,
                    'default' => null,
                    'comment' => 'Venue map id for reserved seating'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_sessions'),
                'session_map_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 15,
                    'default' => null,
                    'comment' => 'Session map id for reserved seating'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_ticket_ticket'),
                'session_map_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 15,
                    'default' => null,
                    'comment' => 'Session map id for reserved seating event'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'redeemed_at',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'comment' => 'Redeemed At'
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'refunded_at',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'comment' => 'Refunded At'
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'redemption_code',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Redeemption Code'
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'session_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Session Id'
                ]);
            $connection->addColumn(
                $table,
                'tickets_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Tickets Id'
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $table = $setup->getTable('magenest_ticket_sessions');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'qty_refunded',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Qty Refunded'
                ]);
        }
        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $table = $setup->getTable('magenest_ticket_sessions');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'revenue',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Revenue'
                ]);

            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'ticket_price',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'comment' => 'Ticket Price'
                ]);
        }
        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'customer_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => null,
                    'comment' => 'Customer Status of Ticket'
                ]);
        }
        if (version_compare($context->getVersion(), '1.1.1') < 0) {
            $table = $setup->getTable('magenest_ticket_ticket');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $table,
                'recipient',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'default' => null,
                    'comment' => 'Recipient'
                ]);
        }
        $setup->endSetup();
    }
}
