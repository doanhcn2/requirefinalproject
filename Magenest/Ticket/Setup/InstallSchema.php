<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Magenest\Ticket\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('magenest_ticket_ticket')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_ticket_ticket')
            )
                ->addColumn(
                    'ticket_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Ticket ID'
                )
                ->addColumn(
                    'event_id',
                    Table::TYPE_INTEGER,
                    11,
                    ['nullable' => false],
                    'Event Id'
                )->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    11,
                    ['nullable' => false],
                    'Product Id'
                )->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => true],
                    'Ticket Type'
                )
                ->addColumn(
                    'code',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Ticket Code'
                )
                ->addColumn(
                    'information',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Information'
                )
                ->addColumn(
                    'customer_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Customer Name'
                )
                ->addColumn(
                    'customer_email',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Customer Email'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    11,
                    ['nullable' => true],
                    'Customer Id'
                )
                ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false],
                    'Ticket Order  Id'
                )
                ->addColumn(
                    'order_increment_id',
                    Table::TYPE_TEXT,
                    32,
                    ['nullable' => true],
                    'Ticket Order Increment Id'
                )
                ->addColumn(
                    'order_item_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Ticket Order Item Id'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true, 'default' => 0],
                    'Ticket Order Item Id'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Ticket Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Ticket Updated At'
                )
                ->addColumn(
                    'note',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Note'
                )
                ->addColumn(
                    'vendor_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Vendor Id'
                )
                ->setComment('Ticket Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('magenest_ticket_ticket'),
                $setup->getIdxName(
                    $installer->getTable('magenest_ticket_ticket'),
                    ['customer_name','customer_email'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['customer_name','customer_email'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('magenest_ticket_event')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_ticket_event')
            )->addColumn(
                'event_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Event ID'
            )->addColumn(
                'product_id',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Product Id'
            )->addColumn(
                'event_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Event Name'
            )->addColumn(
                'ticket_type',
                Table::TYPE_TEXT,
                15,
                ['nullable' => false],
                'Ticket Type'
            )->addColumn(
                'is_online',
                Table::TYPE_INTEGER,
                2,
                [],
                'Is Online'
            )->addColumn(
                'event_type',
                Table::TYPE_TEXT,
                null,
                [],
                'Event Type'
            )->addColumn(
                'age',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Age'
            )->addColumn(
                'show_up',
                Table::TYPE_INTEGER,
                2,
                ['nullable' => true],
                'Show Up'
            )->addColumn(
                'show_up_time',
                Table::TYPE_INTEGER,
                10,
                ['nullable' => true],
                'Show Up Time'
            )->addColumn(
                'can_purchase',
                Table::TYPE_INTEGER,
                2,
                ['nullable' => true],
                'Can Purchase'
            )->addColumn(
                'must_print',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Must Print'
            )->addColumn(
                'refund_policy',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Refund Policy'
            )->addColumn(
                'refund_policy_day',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Refund Policy Day'
            )->addColumn(
                'public_organizer',
                Table::TYPE_INTEGER,
                2,
                ['nullable' => true],
                'Public Organizer'
            )->addColumn(
                'organizer_name',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Organizer Name'
            )->addColumn(
                'organizer_description',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Organizer Description'
            )->addColumn(
                'use_map',
                Table::TYPE_INTEGER,
                2,
                ['nullable' => true],
                'Use Map'
            )->addColumn(
                'email_config',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Email Config'
            )
            ->setComment('Event Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('magenest_ticket_event'),
                $setup->getIdxName(
                    $installer->getTable('magenest_ticket_event'),
                    ['product_id','event_name'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['product_id','event_name'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

        if (!$installer->tableExists('magenest_ticket_location')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_ticket_location')
            )->addColumn(
                'location_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Location Id'
            )->addColumn(
                'event_id',
                Table::TYPE_INTEGER,
                11,
                ['nullable' => true],
                'Event Id'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                11,
                ['nullable' => false],
                'Product Id'
            )->addColumn(
                'venue_title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Venue Title'
            )->addColumn(
                'address',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Address'
            )->addColumn(
                'address2',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Address 2'
            )->addColumn(
                'city',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'City'
            )->addColumn(
                'state',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'State'
            )->addColumn(
                'postcode',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Postcode'
            )->addColumn(
                'country',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Country'
            )->addColumn(
                'longitude',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Longitude'
            )->addColumn(
                'latitude',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Latitude'
            )->addColumn(
                'use_map',
                Table::TYPE_INTEGER,
                2,
                ['nullable' => true],
                'Use Map'
            )
            ->setComment('Event Location');
            $installer->getConnection()->createTable($table);
            $installer->getConnection()->addIndex(
                $installer->getTable('magenest_ticket_location'),
                $setup->getIdxName(
                    $installer->getTable('magenest_ticket_location'),
                    ['product_id','event_id'],
                    AdapterInterface::INDEX_TYPE_INDEX
                ),
                ['product_id','event_id'],
                AdapterInterface::INDEX_TYPE_INDEX
            );
        }

        if (!$installer->tableExists('magenest_ticket_tickets')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_ticket_tickets')
            )->addColumn(
                'tickets_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Ticket ID'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Product Id'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Title'
            )->addColumn(
                'description',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Description'
            )->addColumn(
                'sessions',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Ticket Sessions'
            )->addColumn(
                'type',
                Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Ticket Type'
            )->addColumn(
                'qty_available',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Quantity Available'
            )->addColumn(
                'max_qty',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Max Quantity'
            )->addColumn(
                'commission',
                Table::TYPE_DECIMAL,
                [10,2],
                ['nullable' => true],
                'Comission'
            )
                ->setComment('Vendor Tickets Table');
            $installer->getConnection()->createTable($table);
        }
        if (!$installer->tableExists('magenest_ticket_sessions')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_ticket_sessions')
            )->addColumn(
                'session_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Session ID'
            )->addColumn(
                'tickets_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Ticket Id'
            )->addColumn(
                'session_from',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'Session From'
            )->addColumn(
                'session_to',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => true],
                'Session To'
            )->addColumn(
                'sell_from',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'Sell From'
            )->addColumn(
                'sell_to',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'Sell To'
            )->addColumn(
                'sale_price',
                Table::TYPE_DECIMAL,
                [10,4],
                ['nullable' => true],
                'Price of Paid Ticket'
            )->addColumn(
                'original_value',
                Table::TYPE_DECIMAL,
                [10,4],
                ['nullable' => true],
                'Original Value'
            )->addColumn(
                'qty',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Quantity'
            )->addColumn(
                'qty_available',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Quantity Available'
            )->addColumn(
                'qty_purchased',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Quantity Purchased'
            )->addColumn(
                'max_qty',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Max Quantity'
            )->addColumn(
                'hide_sold_out',
                Table::TYPE_INTEGER,
                1,
                ['nullable' => true],
                'Hide When Sold Out'
            )->addColumn(
                'min_donation',
                Table::TYPE_DECIMAL,
                [10,4],
                ['nullable' => true],
                'Minimum Donation'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Product Id'
            )
                ->setComment('Vendor Sessios Ticket Table');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
