<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Controller\ReservedSeating;

use Magenest\Ticket\Model\Ticket;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\VendorTicket\Model\VenueMapFactory;

/**
 * Class Session
 * @package Magenest\Ticket\Controller\Order
 */
class RefreshVenueMap extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * RefreshVenueMap constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $loggerInterface
     * @param VenueMapFactory $venueMapFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $loggerInterface,
        VenueMapFactory $venueMapFactory
    ) {
        $this->_venueMapFactory = $venueMapFactory;
        $this->logger = $loggerInterface;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Validate ticket if it can be purchase or not
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $vendorId = $this->getRequest()->getParam('vendor_id');
        $venueMapData = $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId);
        $venueMapOption = [];
        foreach ($venueMapData as $map) {
            array_push($venueMapOption, ['label' => $map->getVenueMapName(), 'value' => $map->getVenueMapId()]);
        }
        $resultArray = json_encode($venueMapOption);
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($resultArray);

        return $resultJson;
    }
}
