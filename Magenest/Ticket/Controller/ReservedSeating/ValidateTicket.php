<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\ReservedSeating;

use Magenest\Ticket\Model\Ticket;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\Ticket\Model\TicketsFactory;

/**
 * Class Session
 * @package Magenest\Ticket\Controller\Order
 */
class ValidateTicket extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var VenueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var TicketFactory $_ticketOrderFactory
     */
    protected $_ticketOrderFactory;

    /**
     * @var $_ticketsFactory TicketsFactory
     */
    protected $_ticketsFactory;

    /**
     * ValidateTicket constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $loggerInterface
     * @param SessionFactory $eventSessionFactory
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param TicketFactory $ticketFactory
     * @param TicketsFactory $ticketsFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $loggerInterface,
        SessionFactory $eventSessionFactory,
        VenueMapDataFactory $venueMapDataFactory,
        TicketFactory $ticketFactory,
        TicketsFactory $ticketsFactory
    ) {
        $this->_ticketsFactory = $ticketsFactory;
        $this->_ticketOrderFactory = $ticketFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        $this->session = $eventSessionFactory;
        $this->logger = $loggerInterface;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Validate ticket if it can be purchase or not
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        /**
         * @var $sessionId Ticket session Id for validate
         */
        $sessionId = $params['session_ticket_id'];
        /**
         * @var $sessionCollection \Magenest\Ticket\Model\ResourceModel\Session\Collection
         */
        $sessionCollection = $this->session->create()->getCollection();
        $sessionData = $sessionCollection->addFieldToFilter('session_id', $sessionId)->getFirstItem();
        $arraySession = [];
        if (!empty($sessionData->getData())) {
            $canBeSale = $this->isSellTime($sessionData->getSellFrom(), $sessionData->getSellTo());
            if (!$canBeSale) {
                $arraySession['success'] = false;
                $arraySession['message'] = "This ticket is sell from " . $sessionData->getSellFrom() . " to " . $sessionData->getSellTo() . ". Please choose another.";
                $resultArray = json_encode($arraySession);
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($resultArray);

                return $resultJson;
            }
            $availableQty = $sessionData->getQtyAvailable();
            $ticketId = $sessionData->getTicketsId();
            $productId = $sessionData->getProductId();
            $label = $params['ticket_label'];
            $isSoldOut = $this->isSoldOut($ticketId, $productId, $sessionId, $availableQty, $label);

            if ($isSoldOut) {
                $arraySession['success'] = false;
                $arraySession['message'] = 'This seat has been sold. Please try another.';
                $resultArray = json_encode($arraySession);
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($resultArray);

                return $resultJson;
            }

            $ticketLabel = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('tickets_id', $sessionData->getTicketsId())->getFirstItem()->getTitle();
            if ($sessionData->getSalePrice() === null) {
                $sessionPrice = $sessionData->getOriginalValue();
            } else {
                $sessionPrice = $sessionData->getSalePrice();
            }
            $arraySession = [
                'success' => true,
                'seat_label' => $label,
                'ticket_label' => $ticketLabel,
                'ticket_id' => $sessionData->getTicketsId(),
                'session_id' => $sessionData->getSessionId(),
                'sale_price' => $sessionPrice,
                'message' => null
            ];
            $resultArray = json_encode($arraySession);
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($resultArray);

            return $resultJson;
        } else {
            $arraySession['success'] = false;
            $arraySession['message'] = 'This seat has not ticket for sell yet. Please come back later.';
            $resultArray = json_encode($arraySession);
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($resultArray);

            return $resultJson;
        }
    }

    protected function isSoldOut($ticketId, $productId, $sessionId, $availableQty, $label)
    {
        $orderTicket = $this->_ticketOrderFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        if ($orderTicket === null) {
            return false;
        }
        foreach ($orderTicket as $item) {
            $informationData = $item->getInformation();
            if (!isset($informationData) || empty($informationData)) {
                return false;
            }
            $infoData = unserialize($informationData);
            if ($ticketId === @$infoData['tickets_id']) {
                if (@$infoData['session_id'] === $sessionId) {
                    if (@$infoData['ticket_seat_label'] === $label) {
                        return true;
                    }
                }
            }
        }
        if ($availableQty < 1) {
            return true;
        }
        return false;
    }

    private function isSellTime($getSellFrom, $getSellTo)
    {
        $now = time();
        if ($now >= strtotime($getSellFrom) && $now <= strtotime($getSellTo)) {
            return true;
        }
        return false;
    }

}
