<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Sales\Model\OrderFactory;
use Psr\Log\LoggerInterface;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Helper\Information;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Helper\Event as HelperEvent;
use Magento\Sales\Model\Order\Item as OrderItem;

/**
 * Class Order
 * @package Magenest\Ticket\Controller\Adminhtml\Order
 */
class Order extends \Magento\Backend\App\Action
{
    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var $_sessionFactory \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * email config
     */
    const XML_PATH_EMAIL = 'event_ticket/email_config/email';

    /**
     * qty config
     */
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';

    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var HelperEvent
     */
    protected $_helperEvent;

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var Information
     */
    protected $information;

    /**
     * Order constructor.
     * @param Action\Context $context
     * @param OrderFactory $orderFactory
     * @param LoggerInterface $logger
     * @param TicketFactory $ticketFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param EventFactory $eventFactory
     * @param HelperEvent $helperEvent
     * @param Information $information
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     */
    public function __construct(
        Action\Context $context,
        OrderFactory $orderFactory,
        LoggerInterface $logger,
        TicketFactory $ticketFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        EventFactory $eventFactory,
        HelperEvent $helperEvent,
        Information $information,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory
    ) {
        parent::__construct($context);
        $this->_sessionFactory = $sessionFactory;
        $this->orderFactory = $orderFactory;
        $this->logger = $logger;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->_ticketFactory = $ticketFactory;
        $this->_eventFactory = $eventFactory;
        $this->_helperEvent = $helperEvent;
        $this->information = $information;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Exception
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $model = $this->orderFactory->create()->load($orderId);
        $ticket = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('order_id', $orderId);

        /** @var \Magenest\Ticket\Model\Ticket $ticketRules */
        foreach ($ticket as $ticketRules) {
            $ticketRules->delete();
        }
        foreach ($model->getAllItems() as $orderItem) {
            /** @var \Magento\Catalog\Model\Product $product */
            $productType = $orderItem->getProductType();
            $buyInfo = $orderItem->getBuyRequest();
            $options = $buyInfo->getAdditionalOptions();
            $prices = intval($buyInfo->getQty()) * floatval($options['ticket_price']);
            $event = $this->_eventFactory->create()->loadByProductId($orderItem->getProductId());
            if ($event->getId() && $productType == Event::PRODUCT_TYPE && $orderItem->getStatusId() == OrderItem::STATUS_INVOICED) {
                /** @var \Magento\Sales\Model\Order $order */
                $order = $orderItem->getOrder();
                $qty = $orderItem->getQtyOrdered();
                $email = $order->getCustomerEmail();
                $firstname = $order->getCustomerFirstname();
                $lastname = $order->getCustomerLastname();
                $customerId = $order->getCustomerId();
                $customerName = $firstname . " " . $lastname;

                if (!$customerId) {
                    $customerName = 'Guest';
                }

                $configEmail = $this->_scopeConfig->getValue(self::XML_PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if ($configEmail == 'send_multi_email') {
                    $putQty = 1;
                    $number = $qty;
                } else {
                    $putQty = $qty;
                    $number = 1;
                }
                $arrayInformation = $this->information->getAll($options);

                $ticketData = [
                    'title' => $orderItem->getName(),
                    'event_id' => $event->getId(),
                    'product_id' => $orderItem->getProductId(),
                    'customer_name' => $customerName,
                    'customer_email' => $email,
                    'customer_id' => $customerId,
                    'order_item_id' => $orderItem->getId(),
                    'order_id' => $order->getId(),
                    'order_increment_id' => $order->getIncrementId(),
                    'information' => serialize($arrayInformation),
                    'qty' => $putQty,
                    'status' => 1,
                    'vendor_id' => $orderItem->getUdropshipVendor(),
                    'session_id' => $arrayInformation['session_id'],
                    'tickets_id'=>$arrayInformation['tickets_id'],
                    'ticket_price' => floatval($options['ticket_price'])
                ];
                $sessionRow = $this->_sessionFactory->create()->load($arrayInformation['session_id']);
                $oldRevenue = floatval($sessionRow->getRevenue());
                $oldRevenue += $prices;
                $sessionRow->setRevenue($oldRevenue)->save();
                for ($i = 0; $i < $number; $i++) {
                    /** @var array $ticketData */
                    $ticketData['code'] = $this->_helperEvent->generateCode();
                    $ticketData['redemption_code'] = $this->_helperEvent->generateRedemptionCode();
                    $model = $this->_ticketFactory->create();
                    $model->setData($ticketData)->save();
                }
//                    try {
//                        $modelTicket = $this->_ticketFactory->create()->getCollection()
//                        ->addFieldToFilter('event_id', $event->getId())
//                        ->addFieldToFilter('product_id', $orderItem->getProductId())
//                        ->addFieldToFilter('order_increment_id', $order->getIncrementId());
//                        foreach ($modelTicket as $ticketMail) {
//                            $this->_ticketFactory->create()->sendMail($ticketMail->getTicketId());
//                        }
//                    } catch (\Exception $exception) {
//                        $this->messageManager->addErrorMessage($exception->getMessage());
//                    }
            }
        }

        $resultPage = $this->resultRedirectFactory->create();
        return $resultPage->setPath('sales/order/view', ['order_id' => $orderId]);
    }


    /* @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
