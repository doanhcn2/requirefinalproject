<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magenest\Ticket\Model\SessionFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Session
 * @package Magenest\Ticket\Controller\Order
 */
class Session extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var $tickets \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * @var $eventCollection \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory
     */
    protected $eventCollection;

    /**
     * Session constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $loggerInterface
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory $collectionFactory
     * @param SessionFactory $eventSessionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $loggerInterface,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory $collectionFactory,
        SessionFactory $eventSessionFactory
    )
    {
        $this->eventCollection = $collectionFactory;
        $this->tickets = $ticketsFactory;
        $this->session = $eventSessionFactory;
        $this->logger = $loggerInterface;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * View my ticket
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $dateTime = $params['date_time'];
        $productId = $params['product_id'];
        $ticketIdArray = $this->tickets->create()->getCollection()->addFieldToFilter('product_id', $productId)->getColumnValues('tickets_id');
        $modelSession = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketIdArray);
        $arraySession = [];
        if (!empty($modelSession->getData())) {
            /**
             * @var \Magenest\Ticket\Model\Session $session
             */
            foreach ($modelSession as $session) {
                $sessionFrom = $session->getSessionFrom();
                $sessionFromFormat = date('Y-m-d H:i', strtotime($sessionFrom));
                if ($dateTime == $sessionFromFormat) {

                    if (!$session->getQtyAvailable() > 0 && $session->getHideSoldOut() == 1)
                        continue;
                    $current = date('Y-m-d H:i:s');
                    $expired = 0;
                    if (strtotime($current) > strtotime($session->getSellTo())) {
                        $expired = 1;
                    }
                    $array = [
                        'tickets_id' => $session->getTicketsId(),
                        'session_id' => $session->getId(),
                        'sale_price' => @$session->getSalePrice(),
                        'original_value' => $this->getOriginalValue($session),
                        'available' => $session->getQtyAvailable(),
                        'max' => $session->getMaxQty(),
                        'session_from' => $this->convertTime($session->getSessionFrom()),
                        'session_to' => $this->convertTime($session->getSessionTo()),
                        'isExpired' => $expired,
                        'bought' => $session->getQtyPurchased(),
                        'time_session' => $this->convertTime($session->getSessionFrom()) . ' - ' . $this->convertTime($session->getSessionTo()),
                        'title' => $this->getTicketTitle($session)
                    ];
                    $arraySession [] = $array;
                }
            }
        }
        $resultArray = json_encode($arraySession);
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($resultArray);

        return $resultJson;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);
        return date('d-m-Y H:i', $strTime);
    }

    /**
     * @param $session
     * @return string
     */
    public function getTicketTitle($session)
    {
        /**
         * @var \Magenest\Ticket\Model\Tickets $ticketModel
         */
        $ticketModel = $this->tickets->create()->load($session->getTicketsId());
        return $ticketModel->getTitle();
    }

    protected function getOriginalValue($session)
    {
        $productId = $this->tickets->create()->load($session->getTicketsId())->getProductId();
        $eventType = $this->eventCollection->create()->addFieldToFilter('product_id', $productId)->getFirstItem()->getTicketType();
        if ($eventType === 'donation') {
            return $session->getSalePrice();
        }
        return $session->getOriginalValue();
    }
}
