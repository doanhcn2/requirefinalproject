<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Ticket\Controller\Order;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Ticket\Helper\Pdf;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Helper\Pdf as PdfHelper;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Pdfticket extends Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var Pdf
     */
    protected $pdfTicket;

    /**
     * @var \Magenest\Ticket\Model\TicketFactory
     */
    protected $ticketFactory;

    /**
     * @var PdfHelper
     */
    protected $_pdfHelper;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var $_pdfCoupon \Magenest\Groupon\Helper\Pdf
     */
    protected $_pdfCoupon;

    /**
     * @param Context $context
     * @param DateTime $dateTime
     * @param PdfHelper $pdfHelper
     * @param FileFactory $fileFactory
     * @param Pdf $pdfTicket
     * @param CustomerSession $customerSession
     * @param TicketFactory $ticketFactory
     * @param PageFactory $resultPageFactory
     * @param \Magenest\Groupon\Helper\Pdf $pdf
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        PdfHelper $pdfHelper,
        FileFactory $fileFactory,
        Pdf $pdfTicket,
        CustomerSession $customerSession,
        TicketFactory $ticketFactory,
        PageFactory $resultPageFactory,
        \Magenest\Groupon\Helper\Pdf $pdf
    ) {
        $this->_pdfCoupon = $pdf;
        $this->_pdfHelper = $pdfHelper;
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTime;
        $this->pdfTicket = $pdfTicket;
        $this->_customerSession = $customerSession;
        $this->ticketFactory = $ticketFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
       $resultPage = $this->resultPageFactory->create();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return $resultRedirect->setPath('/');
        }

        $ticketId = (int)$this->getRequest()->getParam('ticket_id');
        if ($ticketId) {
            $ticket = $this->ticketFactory->create()->load($ticketId);
            if ($ticket->getId() && $customerId == $ticket->getCustomerId()) {
                try {
                    $ticketName = 'Ticket_' . $ticketId . '.pdf';
                    return $this->fileFactory->create(
                        $ticketName,
                        $this->_pdfCoupon->getPdf(null, $ticket, 'ticket')->render(),
                        DirectoryList::TMP,
                        'application/pdf',
                        null,
                        true
                    );
                } catch (\Exception $e) {
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    $this->messageManager->addError($e->getMessage());
                    return $resultRedirect;
                }
            } else {
                return $resultRedirect->setPath($this->_redirect->getRefererUrl());
            }
        }
        return $resultPage;
    }
}
