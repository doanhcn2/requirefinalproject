<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magenest\Ticket\Model\SessionFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class OptionTime
 * @package Magenest\Ticket\Controller\Order
 */
class OptionTime extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var $tickets \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * @var $eventCollection \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory
     */
    protected $eventCollection;

    /**
     * Session constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LoggerInterface $loggerInterface
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory $collectionFactory
     * @param SessionFactory $eventSessionFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        LoggerInterface $loggerInterface,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\ResourceModel\Event\CollectionFactory $collectionFactory,
        SessionFactory $eventSessionFactory
    )
    {
        $this->eventCollection = $collectionFactory;
        $this->tickets = $ticketsFactory;
        $this->session = $eventSessionFactory;
        $this->logger = $loggerInterface;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * View my ticket
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $date = $params['date'];
        $productId = $params['product_id'];
        $ticketIdArray = $this->tickets->create()->getCollection()->addFieldToFilter('product_id', $productId)->getColumnValues('tickets_id');
        $modelSession = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketIdArray);
        $arraySession = [];
        if (!empty($modelSession->getData())) {
            $arrayTimeFromUnique = [];
            /**
             * @var \Magenest\Ticket\Model\Session $session
             */
            foreach ($modelSession as $session) {
                $sessionDate = date('Y-m-d', strtotime($session->getSessionFrom()));
                if ($sessionDate == $date) {
                    $array = [
//                        'tickets_id' => $session->getTicketsId(),
                        'product_id' => $productId,
                        'datetime' => date('Y-m-d H:i', strtotime($session->getSessionFrom())),
                        'time_from' => date('H:i A', strtotime($session->getSessionFrom())),
//                        'session_id' => $session->getSessionId()
                    ];
                    if (!in_array(date('H:i A', strtotime($session->getSessionFrom())), $arrayTimeFromUnique)) {
                        $arraySession [] = $array;
                        $arrayTimeFromUnique[] = date('H:i A', strtotime($session->getSessionFrom()));
                    }
                }
            }
            foreach ($arraySession as $key => $value) {
                $sortArrayTime[$key] = strtotime($value['datetime']);
            }
            asort($sortArrayTime);
            foreach ($sortArrayTime as $key => $value) {
                $arraySessionOffical[] = [
                    'product_id' => $arraySession[$key]['product_id'],
                    'datetime' => $arraySession[$key]['datetime'],
                    'time_from' =>$arraySession[$key]['time_from']
                ];
            }
        }
        $resultArray = json_encode($arraySessionOffical);
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($resultArray);

        return $resultJson;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);
        return date('d-m-Y H:i', $strTime);
    }

    protected function getOriginalValue($session)
    {
        $productId = $this->tickets->create()->load($session->getTicketsId())->getProductId();
        $eventType = $this->eventCollection->create()->addFieldToFilter('product_id', $productId)->getFirstItem()->getTicketType();
        if ($eventType === 'donation') {
            return $session->getSalePrice();
        }
        return $session->getOriginalValue();
    }
}
