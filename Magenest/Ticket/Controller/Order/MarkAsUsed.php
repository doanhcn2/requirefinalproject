<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Controller\Order;

use Magenest\Ticket\Model\Ticket;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class MarkAsUsed extends Action
{
    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var $_customerSession \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var $_jsonFactory JsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var $_unirgyRatingHelper \Unirgy\DropshipVendorRatings\Helper\Data
     */
    private $_unirgyRatingHelper;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $session,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $ratingHelper,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        JsonFactory $jsonFactory
    ) {
        $this->_vendorFactory = $vendorFactory;
        $this->_unirgyRatingHelper = $ratingHelper;
        $this->_jsonFactory = $jsonFactory;
        $this->_customerSession = $session;
        $this->_ticketFactory = $ticketFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();
        if ($data = $this->getRequest()->getParams()) {
            $ticket = $this->_ticketFactory->create()->load($data['ticket_id']);
            if ((int)$ticket->getStatus() === Ticket::STATUS_AVAILABLE && $this->isCustomerPermitted($ticket->getCustomerId())) {
                try {
                    $redeem = $ticket->markAsUsed();
                    if ($redeem) {
                        $this->messageManager->addSuccessMessage('Your ticket has been marked as Used.');
                    } else {
                        $this->messageManager->addSuccessMessage('Something went wrong, please try again later.');
                    }
                    if ($redeem && $this->vendorAllowRating($ticket->getVendorId()) && $this->couponHasReview($ticket->getProductId(), $ticket->getCustomerId())) {
                        $modalHtml = $this->getReviewModalBlock($ticket->getProductId());
                        return $result->setData(['success' => true, 'review_modal' => $modalHtml, 'has_review' => true]);
                    } else {
                        return $result->setData(['success' => true, 'has_review' => false]);
                    }
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage('Something went wrong, please try again later.');
                    return $result->setData(['success' => false]);
                }
            }
        }
        $this->messageManager->addErrorMessage('You has no permission to do this.');
        return $result->setData(['success' => false]);
    }

    private function isCustomerPermitted($customerId)
    {
        $customerSessionId = $this->_customerSession->getCustomerId();

        return (int)$customerSessionId === (int)$customerId;
    }

    public function couponHasReview($productId, $customerId)
    {
        if (!$this->isProductHasReviewYet($productId)) {
            $items = ObjectManager::getInstance()->create('Magenest\VendorApi\Helper\OrderHelper')->getRatingItemsByProductId([$productId], $customerId);
            if (count($items->getData()) !== 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    protected function getReviewModalBlock($productId)
    {
        $view = ObjectManager::getInstance()->create('\Magento\Framework\App\ViewInterface');
        $view->addActionLayoutHandles();
        $reviewBlock = $view->getLayout()->createBlock('Magenest\Ticket\Block\Product\VirtualReview')
            ->setProductId($productId);
        $view->getLayout()->initMessages();
        return $reviewBlock->toHtml();
    }

    protected function isProductHasReviewYet($productId)
    {
        $result = [];
        $reviews = $this->_unirgyRatingHelper->getCustomerReviewsCollection();

        foreach($reviews->getItems() as $review) {
            if ($review->getProductId()) {
                array_push($result, $review->getProductId());
            }
        }

        return in_array($productId, $result);
    }

    protected function vendorAllowRating($vendorId)
    {
        $isAllowed = $this->_vendorFactory->create()->load($vendorId)->getAllowUdratings();

        return $isAllowed === 1 || $isAllowed === '1';
    }
}
