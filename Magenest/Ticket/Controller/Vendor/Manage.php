<?php

namespace Magenest\Ticket\Controller\Vendor;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

/**
 * Class Manage
 * @package Magenest\Ticket\Controller\Vendor
 */
class Manage extends AbstractVendor
{

    public function __construct(Context $context, ScopeConfigInterface $scopeConfig, DesignInterface $viewDesignInterface, StoreManagerInterface $storeManager, LayoutFactory $viewLayoutFactory, Registry $registry, ForwardFactory $resultForwardFactory, HelperData $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Framework\HTTP\Header $httpHeader)
    {
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
    }

    public function execute()
    {
        if (!$this->isVendorAllow()) {
            parent::_forward('index', 'vendor', 'udropship');
        }
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $session->setUdprodLastGridUrl($this->_url->getUrl('*/*/*', ['__vp'=>true,'_current'=>true]));
        $this->_renderPage(null, 'magenest_ticket_manage');
    }

    protected function isVendorAllow()
    {
        $vendorId = $this->_hlp->session()->getVendorId();
        $vendor = $this->_hlp->getVendor($vendorId);
        $promoteProduct = $vendor->getPromoteProduct();
        if ($promoteProduct === '3') {
            return true;
        } else {
            return false;
        }
    }
}