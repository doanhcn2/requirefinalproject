<?php

namespace Magenest\Ticket\Controller\Vendor;

use Magenest\Groupon\Model\ResourceModel\Groupon\CollectionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\LocationFactory;
use Magenest\Groupon\Model\DealFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\Filesystem\Directory\WriteInterface;


class Download extends Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    /**
     * @var FileFactory $fileFactory
     */
    protected $fileFactory;

    /**
     * @var CollectionFactory $_grouponCollection
     */
    protected $_grouponCollection;

    /**
     * @var $_dealFactory DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_locationFactory LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_campaignStatus CampaignStatus
     */
    protected $_campaignStatus;

    /**
     * @var $ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $ticketFactory;

    /**
     * Download constructor.
     * @param CollectionFactory $collectionFactory
     * @param CampaignStatus $campaignStatus
     * @param DealFactory $dealFactory
     * @param LocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param Filesystem $filesystem
     * @param FileFactory $fileFactory
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        CampaignStatus $campaignStatus,
        DealFactory $dealFactory,
        LocationFactory $locationFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->ticketFactory = $ticketFactory;
        $this->_dealFactory = $dealFactory;
        $this->_locationFactory = $locationFactory;
        $this->_campaignStatus = $campaignStatus;
        $this->_grouponCollection = $collectionFactory;
        parent::__construct($context);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $parentProductId = $this->getRequest()->getParam('product_id');
            if ($parentProductId) {
                $parentProductId = explode(',', $parentProductId);
                $collection = $this->ticketFactory->create()->getCollection()
                    ->addFieldToFilter('vendor_id', $this->getVendorId())
                    ->addFieldToFilter('product_id', ['in' => $parentProductId]);
            } else {
                $ticketsId = $this->getRequest()->getParam('ticket_id');
                $collection = $this->ticketFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->addFieldToFilter('ticket_id', ['in' => $ticketsId]);
            }

            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($collection || @$ticketsId) {
                $heading = [
                    ('ticket_id'),
                    ('product_name'),
                    ('ticket_code'),
                    ('order_id'),
                    ('status'),
                    ('created_at'),
                    ('customer_name'),
                    ('redeemed_at'),
                    ('note')
                ];

                $items = [];
                foreach($collection as $log) {
                    $items[] = [
                        $log->getData('ticket_id'),
                        $log->getData('title'),
                        $log->getData('code'),
                        $log->getData('order_id'),
                        $log->getData('status'),
                        $log->getData('created_at'),
                        $log->getData('customer_name'),
                        $log->getData('redeemed_at'),
                        $log->getData('note')
                    ];
                }
                $file = 'var/tmp/' . "Ticket_Log_" . date('Ymd_His') . ".csv";
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach($items as $item) {
                    $stream->writeCsv($item);
                }

                return $this->fileFactory->create("Ticket_Log_" . date('Ymd_His') . ".csv", [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('*/*/*');
    }
}
