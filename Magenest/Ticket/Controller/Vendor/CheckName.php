<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 23/03/2018
 * Time: 23:10
 */

namespace Magenest\Ticket\Controller\Vendor;


class CheckName extends \Magento\Framework\App\Action\Action
{

    protected $_productFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        parent::__construct($context);
        $this->_productFactory = $productFactory;
    }

    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->_request->getParams();

            if (array_key_exists("name", $data)) {
                $name = $data["name"];
                $productCollection = $this->_productFactory->create()->getCollection()->addFieldToFilter("name", $name)->getFirstItem();
                if (!$productCollection->getId() || $productCollection->getId() == $data['product_id'])
                    $result->setData([
                        'success' => true
                    ]);
                else
                    $result->setData([
                        'success' => false
                    ]);
            }
        } catch (\Exception $e) {
            $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        return $result;
    }
}