<?php

namespace Magenest\Ticket\Controller\Vendor\Campaign;

class Purchased extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Ticket\Block\Vendor\Campaign\PurchasedTicketsPage $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('campaign-purchased-ticket-page');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}