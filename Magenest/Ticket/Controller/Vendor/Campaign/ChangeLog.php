<?php

namespace Magenest\Ticket\Controller\Vendor\Campaign;

class ChangeLog extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Ticket\Block\Vendor\Campaign\ChangeHistoryPage $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('campaign-changelog-page');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}