<?php

namespace Magenest\Ticket\Controller\Vendor\Campaign\Session;

class Page extends \Unirgy\DropshipPo\Controller\Vendor\AbstractVendor
{

    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Ticket\Block\Vendor\Campaign\Session\SessionType $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('session-page-view');
        $view->getLayout()->initMessages();
        return $this->_resultRawFactory->create()->setContents($infoBlock->toHtml());
    }
}