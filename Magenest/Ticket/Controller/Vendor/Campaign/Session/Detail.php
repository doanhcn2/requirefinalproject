<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Vendor\Campaign\Session;

use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;


class Detail extends AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'magenest_ticket_manage');
    }
}