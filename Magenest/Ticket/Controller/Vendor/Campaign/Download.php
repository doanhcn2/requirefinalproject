<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 14/04/2018
 * Time: 16:09
 */

namespace Magenest\Ticket\Controller\Vendor\Campaign;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Download
 * @package Magenest\Ticket\Controller\Vendor\Campaign
 */
class Download extends \Magento\Framework\App\Action\Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    protected $fileFactory;

    protected $_sessionFactory;

    protected $_sessionHelper;

    public function __construct(
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Helper\Session $sessionHelper,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    )
    {
        $this->_sessionHelper = $sessionHelper;
        $this->_sessionFactory = $sessionFactory;
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            /** @var \Unirgy\Dropship\Model\Session $vendorSession */
            $vendorSession = $this->_objectManager->get('Unirgy\Dropship\Model\Session');
            if (!$vendorSession->isLoggedIn()) {
                return;
            }
            $data = $this->getRequest()->getParams();
            $productId = $data['product_id'];
            $resultRedirect = $this->resultRedirectFactory->create();

            /** @var \Magenest\Hotel\Model\HotelProperty $hotelProperty */
            if ($data) {
                $heading = [
                    __('Ref #'),
                    __('Status'),
                    __('Session'),
                    __('Sold'),
                    __('Revenue'),
                    __('Refunded'),
                    __('Max Qty'),
                    __('Redeemed')
                ];

                $collection = $this->_sessionFactory->create()->getCollection();
                $sessionsId = explode(",", $data['sessions']);
                $collection = $collection->addFieldToFilter("session_id", array('in' => $sessionsId));

                $items = [];
                /** @var  \Magenest\Ticket\Model\SessionFactory $model */
                foreach ($collection as $model) {
                    $items[] = [
                        $model->getSessionId(),
                        $this->_sessionHelper->getSessionStatus($model->getSessionId())['status'],
                        __(date("D, M d, Y %1 h:m A", strtotime($model->getSessionFrom())), "at"),
                        $model->getQtyPurchased(),
                        $this->_sessionHelper->getSessionRevenue($model->getSessionId(), $productId),
                        $model->getQtyRefunded(),
                        $model->getMaxQty(),
                        $this->_sessionHelper->getQtyRedeemed($model->getSessionId(), $productId),
                    ];
                }
                $file = 'export/ListSessions' . date('Ymd_His') . '.csv';
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                foreach ($items as $item) {
                    $stream->writeCsv($item);
                }
                return $this->fileFactory->create('ListSessions' . date('Ymd_His') . '.csv', [
                    'type' => 'filename',
                    'value' => $file,
                    'rm' => true  // can delete file after use
                ], 'var');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }
        return $resultRedirect->setPath('ticket/vendor_campaign/detail/', ["id" => $productId]);
    }


}
