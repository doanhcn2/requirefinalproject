<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:10
 */

namespace Magenest\Ticket\Controller\Vendor\Campaign;

use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;


class Detail extends AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'magenest_ticket_manage');
    }
}