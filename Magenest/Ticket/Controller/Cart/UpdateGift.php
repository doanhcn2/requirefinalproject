<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 */

namespace Magenest\Ticket\Controller\Cart;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Unirgy\SimpleLicense\Exception;

class UpdateGift extends \Magento\Framework\App\Action\Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var $_checkoutSession \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    protected $logger;

    /**
     * @var $_cart \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * Option constructor.
     * @param Context $context
     * @param ResultJsonFactory $resultJsonFactory
     * @param ProductFactory $productFactory
     * @param StockStateInterface $stockStateInterface
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        ProductFactory $productFactory,
        StockStateInterface $stockStateInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $cart,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_cart = $cart;
        $this->logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_productFactory = $productFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->stockStateInterface = $stockStateInterface;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $params = $this->getRequest()->getParams();
        $quoteItemId = @$params['quote_item_id'];
        if (!$quoteItemId) {
            $resultPage->setData(['success' => false]);
            return $resultPage;
        }
        try {
            $quote = $this->_checkoutSession->getQuote();
            $quoteItem = $quote->getItemById($quoteItemId);
            $buyOptions = $quoteItem->getBuyRequest();
            $options = $buyOptions->getData();
            $options['additional_options']['To'] = $params['recipient_name'];
            $options['additional_options']['Email'] = $params['recipient_mail'];
            $options['additional_options']['Message'] = $params['recipient_msg'];
            $options['additional_options']['Type'] = "Mail";
            $buyOptions->unsetData();
            $buyOptions->addData($options);
            $this->_cart->updateItem($quoteItemId, $buyOptions);
            $this->_cart->save();
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage('Something went wrong, please try again later.');
            $this->logger->critical($exception->getMessage());
            $resultPage->setData(['success' => false]);
            return $resultPage;
        }

        $resultPage->setData(['success' => true]);
        return $resultPage;
    }


}
