<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Controller\Product;

use Magenest\Ticket\Model\SessionFactory;
use Magento\Framework\App\Action\Context;

/**
 * Class Ticket
 * @package Magenest\Ticket\Block\Product
 */
class Price extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $eventFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_currency;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * Price constructor.
     * @param Context $context
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param SessionFactory $sessionFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magento\Directory\Model\Currency $currency
     */
    public function __construct(Context $context,
                                \Magenest\Ticket\Model\EventFactory $eventFactory,
                                SessionFactory $sessionFactory,
                                \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
                                \Magento\Directory\Model\Currency $currency)
    {
        $this->_currency = $currency;
        $this->eventFactory = $eventFactory;
        $this->tickets = $ticketsFactory;
        $this->session = $sessionFactory;
        parent::__construct($context);
    }

    public function getTicketPrice($productId)
    {
        $ticketType = $this->eventFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getFirstItem()->getTicketType();
        if ($ticketType === 'paid') {
            return $this->getTicketPaidPrice($productId);
        } elseif ($ticketType === 'free') {
            return ['is_sale' => false, 'sale_price' => 0];
        } elseif ($ticketType === 'donation') {
            return $this->getTicketDonationPrice($productId);
        }
    }

    protected function getTicketPaidPrice($productId)
    {
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('sell_to', ['gt' => date('Y-m-d H:i:s', time())])->addFieldToFilter('qty_available', ['gt' => 0]);
        $result = [];
        $minPrice = PHP_INT_MAX;
        $minOriginal = 0;
        foreach ($sessionTicket as $session) {
            if ($session->getSalePrice()) {
                if ($session->getSalePrice() < $minPrice) {
                    $minPrice = $session->getSalePrice();
                    $minOriginal = $session->getOriginalValue();
                }
            } else {
                if ($session->getOriginalValue() < $minPrice) {
                    $minPrice = $session->getOriginalValue();
                }
            }
        }

        $result['is_sale'] = $minOriginal == 0 ? false : true;
        $result['sale_price'] = $minPrice == PHP_INT_MAX ? 0 : $minPrice;
        $result['original_value'] = $minOriginal;

        return $result;
    }

    protected function getTicketDonationPrice($productId)
    {
        $result['is_sale'] = false;
        $donationPrice = $this->getTicketDonation($productId);
        if (count($donationPrice) > 0)
            $result['sale_price'] = min($donationPrice);
        else
            $result['sale_price'] = 0;

        return $result;
    }

    private function getTicketDonation($productId)
    {
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('sell_to', ['gt' => date('Y-m-d H:i:s', time())])->addFieldToFilter('qty_available', ['gt' => 0]);
        $result = [];
        foreach ($sessionTicket as $session) {
            array_push($result, $session->getSalePrice());
        }

        return array_unique($result);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return bool
     */
    public function execute()
    {
        return false;
    }

    private function _preparePrice($originalPrice)
    {
        if (is_array($originalPrice)) {
            if (min($originalPrice) === max($originalPrice)) {
                return $originalPrice;
            }
            return [min($originalPrice), max($originalPrice)];
        } else {
            return 0;
        }
    }

    private function getTicketOriginalPrice($productId)
    {
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $result = [];
        foreach ($sessionTicket as $session) {
            array_push($result, $session->getOriginalValue());
        }
        return array_unique($result);
    }

    private function getTicketSalePrice($productId)
    {
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $result = [];
        foreach ($sessionTicket as $session) {
            if ($session->getSalePrice()) {
                array_push($result, $session->getSalePrice());
            }
        }
        return array_unique($result);
    }

    private function checkCategoryPrice($result)
    {
        if (isset($result['sale_price'])) {
            if ($result['sale_price'][0] === $result['original_value'][0]) {
                $result['is_sale'] = false;
            }
        }

        return $result;
    }
}
