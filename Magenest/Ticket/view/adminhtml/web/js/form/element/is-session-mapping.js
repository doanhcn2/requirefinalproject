define([
    'jQuery',
    'Magento_Ui/js/form/element/abstract',
    'ko'
],function ($, Abstract, ko){

    return Abstract.extend({
        is_event_reserved: ko.observable(false),
        initialize: function() {
            var self = this;
            this._super();
            this.is_event_reserved.subscribe(function(isReservedSeating){
                if (isReservedSeating) {
                    self.visible(true);
                    // self.prop('readonly', 'readonly');
                } else {
                    self.visible(false);
                    // self.prop('readonly', null);
                }
            });
            return this;
        }
    });
});