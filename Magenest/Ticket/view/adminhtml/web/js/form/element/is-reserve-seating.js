define([
    'Magento_Ui/js/form/element/single-checkbox',
    'ko'
],function (SingleCheckbox, ko){

    return SingleCheckbox.extend({
        is_event_online: ko.observable(false),
        initialize: function() {
            var self = this;
            this._super();
            this.is_event_online.subscribe(function(isOnline){
                if (isOnline) {
                    self.visible(true);
                } else {
                    self.visible(false);
                    self.checked(false);
                }
            });
            return this;
        }
    });
});