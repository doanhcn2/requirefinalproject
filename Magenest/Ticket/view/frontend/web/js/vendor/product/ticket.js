define(
    [
        'jquery',
        'ko',
        'mage/calendar',
        'underscore'
    ],
    function ($, ko, calendar, _) {

        ko.bindingHandlers.mmCalendar = {
            init: function (element) {
                $(element).calendar({
                    dateFormat: "dd-mm-yyyy",
                    showsTime: true,
                    todayHighlight: true,
                    timeFormat: "HH:mm",
                    sideBySide: true,
                    closeText: "Done",
                    selectOtherMonths: true,
                    onClose: function (selectedDate) {
                        if (selectedDate !== '') {
                            var eleName = $(element).attr("name");
                            var ticketRow = eleName.substring(eleName.indexOf("info") + 6, eleName.indexOf("sessions") - 2);
                            var sessionRow = 0;
                            if (eleName.indexOf("start") > 0) {
                                sessionRow = 0;
                                if (eleName.indexOf("session_start") > 0) {
                                    //SESSION START

                                    sessionRow = eleName.substring(eleName.indexOf("sessions") + 10, eleName.indexOf("][session_start]"));
                                    if (compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][session_end]']").val())) {
                                        alert("Session Start must come before Session Ends");
                                        $(element).val("");
                                    } else {
                                        $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][session_end]']").val("");
                                        $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][session_end]']").datepicker("option", {
                                            minDate: selectedDate
                                        });
                                        $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_end]']").val(selectedDate);
                                        $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_end]']").change();
                                    }
                                }
                                else {
                                    //SALES START
                                    sessionRow = eleName.substring(eleName.indexOf("sessions") + 10, eleName.indexOf("][sale_start]"));
                                    if (compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_end]']").val())) {
                                        alert("Session Start must come before Sales End");
                                        $(element).val("");
                                    }
                                }
                            }
                            else {
                                //end date change, compare with start date
                                if (eleName.indexOf("session_end") > 0) {
                                    //SESSION END
                                    sessionRow = eleName.substring(eleName.indexOf("sessions") + 10, eleName.indexOf("][session_end]"));
                                    if (!compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][session_start]']").val())) {
                                        alert("Session Ends must come after Session Start");
                                        $(element).val("");
                                    }
                                    if (!compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_end]']").val())) {
                                        alert("Session Ends must come after Sales End");
                                        $(element).val("");
                                    }
                                }
                                else {
                                    //SALES END
                                    sessionRow = eleName.substring(eleName.indexOf("sessions") + 10, eleName.indexOf("][sale_end]"));
                                    if ($("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_start]']").val() !== "" &&
                                        !compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][sale_start]']").val())) {
                                        alert("Sales Start must come before Sales End");
                                        $(element).val("");
                                        return;
                                    }
                                    if (compareTime(selectedDate, $("input[name='event[ticket_info][" + ticketRow + "][sessions][" + sessionRow + "][session_end]']").val())) {
                                        $(element).val("");
                                        alert("Sales Ends must come before Session Ends");
                                    }

                                }
                            }
                        }
                    }
                });
            },
            update: function (element, value) {
            }
        };

        function toDefaultFormatDate(time) {
            var date = time.substring(0, time.indexOf(" "));
            var hour = time.substr(time.indexOf(" "));
            date = date.split("-");
            hour = hour.split(":");
            return new Date(date[2], date[1] - 1, date[0], hour[0], hour[1]);
        }

        function compareTime(time1, time2) {
            if (time1 !== undefined && time2 !== undefined)
                if (toDefaultFormatDate(time1) > toDefaultFormatDate(time2)) {
                    return true;
                }
            return false;
        }

        function SessionModel(id, type, origValue, salePriceValue, qty, maxQty, hideValue, sessionStart, sessionEnd, saleStart, saleEnd, sessionMapId) {
            var self = this;
            self.type = type;
            self.origClass = ko.observable("required-entry");
            if (type === "Add Donation Ticket") {
                self.salePriceLable = "Minimum Donation";
                self.disabledAttr = true;
            }
            else if (type === "Add Free Ticket") {
                self.salePriceLable = "Sale Price";
                self.disabledAttr = false;
                self.origClass("");
            } else {
                self.salePriceLable = "Sale Price";
                self.disabledAttr = true;
            }
            if (id !== undefined)
                self.sessionId = ko.observable(id);
            else
                self.sessionId = ko.observable();

            self.origValue = ko.observable(origValue === 0 ? "" : origValue);
            self.qty = ko.observable(qty);
            if (hideValue === 1)
                self.hideValue = ko.observable(true);
            else
                self.hideValue = ko.observable(false);
            self.salePriceValue = ko.observable(salePriceValue);
            self.maxQty = ko.observable(maxQty);
            self.sessionStart = ko.observable(sessionStart);
            self.sessionEnd = ko.observable(sessionEnd);
            self.saleStart = ko.observable(saleStart);
            self.saleEnd = ko.observable(saleEnd);
            self.sessionMapId = ko.observable(sessionMapId);
            self.showSessionMapId = ko.observable(window.isReservedSeatingEventType);
        }

        function changeTypeToId(text) {
            if (text === "Add Paid Ticket")
                return "paid";
            else if (text === "Add Free Ticket")
                return "free";
            return "donation";
        }

        function TicketModel(id, type, title, description, sessionArray) {
            var self = this;
            self.ticketType = ko.observable(type);
            if (id)
                self.ticketId = id;
            else
                self.ticketId = null;
            self.type = ko.observable(
                changeTypeToId(type)
            );
            self.ticketTitle = ko.observable(title);
            self.ticketDescription = ko.observable(description);
            self.sessions = ko.observableArray(
                [new SessionModel(null, type)]
            );
            if (sessionArray !== undefined) {
                self.sessions = ko.observableArray();
                for (var i = 0; i < sessionArray.length; i++) {
                    if (sessionArray[i]['id']) {
                        self.sessions.push(new SessionModel(
                            sessionArray[i]['id'],
                            type,
                            sessionArray[i]['orig_value'],
                            sessionArray[i]['salePrice'],
                            sessionArray[i]['qty'],
                            sessionArray[i]['max_qty'],
                            sessionArray[i]['is_hide'],
                            sessionArray[i]['session_start'],
                            sessionArray[i]['session_end'],
                            sessionArray[i]['sale_start'],
                            sessionArray[i]['sale_end'],
                            sessionArray[i]['session_map_id']
                            )
                        );
                    }


                    else {
                        self.sessions.push(new SessionModel(
                            null,
                            type,
                            sessionArray[i].origValue(),
                            sessionArray[i].salePriceValue(),
                            sessionArray[i].qty(),
                            sessionArray[i].maxQty(),
                            sessionArray[i].hideValue(),
                            sessionArray[i].sessionStart(),
                            sessionArray[i].sessionEnd(),
                            sessionArray[i].saleStart(),
                            sessionArray[i].saleEnd(),
                            sessionArray[i].sessionMapId()
                            )
                        );
                    }

                }
            }
            self.addSession = function () {
                self.sessions.push(
                    new SessionModel(null, type, "", "", "", "", false, "", "", "", "", "")
                );
            };
            self.duplicateSession = function (session) {
                self.sessions.push(
                    new SessionModel(
                        null,
                        type,
                        session.origValue(),
                        session.salePriceValue(),
                        session.qty(),
                        session.maxQty(),
                        session.hideValue(),
                        session.sessionStart(),
                        session.sessionEnd(),
                        session.saleStart(),
                        session.saleEnd(),
                        session.sessionMapId()
                    )
                );
            };
            self.removeSession = function (session) {
                self.sessions.remove(session);
            };
        }

        return function (config) {
            var self = this;
            var type = null;
            self.tickets = ko.observableArray();
            if (window.ticketData.length > 0) {
                var array = window.ticketData;
                for (var i = 0; i < array.length; i++) {
                    self.tickets.push(
                        new TicketModel(
                            array[i].id,
                            array[i].type,
                            array[i].title,
                            array[i].description,
                            array[i].sessions
                        )
                    );
                    type = array[i].type;
                }

                $("#add-"+changeTypeToId(type)+"-ticket").attr("disabled","true");
            } else {
                $(".form-button").attr("disabled", true);
            }
            self.addTicket = function (ticket) {

                self.tickets.push(new TicketModel(null, ticket.ticketType()));
            };
            self.duplicateTicket = function (ticket) {
                self.tickets.push(new TicketModel(null, ticket.ticketType(), ticket.ticketTitle(), ticket.ticketDescription(), ticket.sessions()));
            };
            self.removeTicket = function (ticket) {

                self.tickets.remove(ticket);
                if (self.tickets().length === 0) {
                    $(".form-button").attr("disabled", true);
                    $(".add-ticket").removeAttr("disabled");
                }
            };

            function deleteTicketConfirm() {
                if (self.tickets().length > 0) {
                    if (window.confirm("Change another Type of Ticket will lost saved Tickets.\n" +
                            "Do you really want to change?")) {
                        return true;
                    }
                    return false;
                }
                return true;
            }

            self.addPaidTicket = function () {
                if (deleteTicketConfirm() || self.tickets().length === 0) {
                    self.tickets.removeAll();
                    self.tickets.push(
                        new TicketModel(null, "Add Paid Ticket")
                    );
                    type = "Add Paid Ticket";
                    $(".form-button").removeAttr('disabled');
                    $("#add-paid-ticket").attr("disabled","true");
                    $("#add-free-ticket").removeAttr('disabled');
                    $("#add-donation-ticket").removeAttr('disabled');
                }
            };
            self.addFreeTicket = function () {
                if (deleteTicketConfirm() || self.tickets().length === 0) {
                    self.tickets.removeAll();
                    self.tickets.push(
                        new TicketModel(null, "Add Free Ticket")
                    );
                    type = "Add Free Ticket";
                    $(".form-button").removeAttr('disabled');
                    $("#add-free-ticket").attr("disabled","true");
                    $("#add-paid-ticket").removeAttr('disabled');
                    $("#add-donation-ticket").removeAttr('disabled');
                }
            };
            self.addDonationTicket = function () {
                if (deleteTicketConfirm() || self.tickets().length === 0) {
                    self.tickets.removeAll();
                    self.tickets.push(
                        new TicketModel(null, "Add Donation Ticket")
                    );
                    type = "Add Donation Ticket";
                    $(".form-button").removeAttr('disabled');
                    $("#add-donation-ticket").attr("disabled","true");
                    $("#add-free-ticket").removeAttr('disabled');
                    $("#add-paid-ticket").removeAttr('disabled');
                }

            };

        };
    });