define([
    'jquery',
    'ko',
    'uiComponent',
    'https://cdn.rawgit.com/konvajs/konva/1.7.6/konva.js',
    'Magenest_VendorTicket/js/model/Chair',
    'Magenest_VendorTicket/js/model/Table',
    'Magenest_VendorTicket/js/model/Ticket',
    "jquery/colorpicker/js/colorpicker"
], function ($, ko, Component, konva, chairt, tablet, ticket) {
    'use strict';

    return Component.extend({
        defaults: {
            dataScope: 'reservationSeat',
            mode: ['map', 'ticket', 'hold'],
            activeMode: 'ticket',
            layoutType: 'roundTable',
            seats: [],
            selectedSeats: [],
            tables: [],
            tableGroupArr: [],
            tickets: [],
            activeTicket: {},
            holds: [],
            layer: '',
            tableNumber: '',
            chairNumber: '',
            stageWidth: '',
            stageHeight: '',
            stage: '',
            defaultColor: 'silver',
            currentTicket: '',
            /**
             *  new Konva.Group
             */
            activeObject: '',
            allObj: [],
            venueMapData: ko.observable(),
            venueTicketData: ko.observable(),
            sessionCanvasId: '',
            selectedChair: '',
            lastColor: '',
            labels: []
        },

        /** @inheritdoc */
        initObservable: function () {
            this._super().observe('activeMode');
            this._super().observe('activeTicket');
            this._super().observe('holds');
            this._super().observe('tableNumber');
            this._super().observe('chairNumber');
            this._super().observe('seats');
            this._super().observe('tables');
            this._super().observe('labels');
            this._super().observe('tickets');
            this._super().observe('activeObject');
            this._super().observe('stage');
            this._super().observe('venueMapData');
            this._super().observe('venueTicketData');
            return this;
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();
            var self = this;
            self.stageWidth = 1200;
            self.stageHeight = 400;
            var stage;
            this.roundTable;
            if (self.stage()) {
                stage = Konva.Node.create(self.stage(), 'canvas' + self.sessionCanvasId);
                self.layer = stage.getChildren()[0];
                $.each(self.layer.getChildren(), function (index, item) {
                    self.mapModeSelectObj(item);
                    $.each(item.getChildren(), function (index, _item) {
                        if (_item.getAttr('objectType') === 'chair')
                            self.seats.push(_item);
                        if (_item.getAttr('objectType') === 'table')
                            self.tables.push(_item);
                        if (_item.className === 'Text')
                            self.labels.push(_item);
                    });
                });
            } else {
                stage = new Konva.Stage({
                    container: 'canvas' + self.sessionCanvasId,
                    width: self.stageWidth,
                    height: self.stageHeight
                });
                self.layer = new Konva.Layer();
                self.initTableAndChair(self.tableNumber(), self.chairNumber());
                stage.add(self.layer);
            }
            if (self.tickets()) {
                $.each(self.tickets(), function (index, ticket) {
                    var label = ticket.label;
                    ticket.label = ko.observable(label);
                    var cnt = ticket.cnt;
                    ticket.cnt = ko.observable(cnt);
                    var colorTag = ticket.colorTag;
                    ticket.colorTag = ko.observable(colorTag);
                    if (ticket.chairs.length === 0) {
                        ticket.chairs = [];
                    } else {
                        var chairsData = [];
                        $.each(ticket.chairs, function (index, chairs) {
                            var data = JSON.parse(chairs);
                            chairsData.push(data);
                        });
                        ticket.chairs = chairsData;
                    }
                });
            } else {
                self.tickets([]);
            }
            self.stage(stage);
            $.each(self.layer.getChildren(), function (index, item) {
                item.on('mouseenter', function () {
                    stage.container().style.cursor = 'move';
                });

                item.on('mouseleave', function () {
                    stage.container().style.cursor = 'default';
                });
            });
            if (self.tickets()) {
                self.bindTicketWithChairGlobal(self.tickets());
            }
            $.each(self.layer.getChildren(), function (index, item) {
                item.setAttrs({draggable: false});
            });
            self.layer.draw();
            self.bindClickActionTicketMode();
        },
        mapModeSelectObj: function (item) {
            var self = this;
            item.on('click.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
            });
            item.on('dragend.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
                self.venueMapData(JSON.stringify(self.stage()));
                $('#venue_map_data').change();
            });
            item.on('dragstart.combo', function () {
                item.setAttrs({fill: '#89b717'});
                self.activeObject(item);
                self.layer.draw();
            });
        },
        selectOtherChair: function (selectedChair, self) {
            if (selectedChair === undefined || selectedChair === "") {
                return;
            }
            $.each(self.seats(), function (index, chairs) {
                if (chairs === selectedChair) {
                    chairs.setAttrs({fill: self.lastColor});
                    chairs.setAttrs({stroke: 'black'});
                }
            });
        },
        /**
         * binding each chair with a ticket type
         */
        bindClickActionTicketMode: function () {
            var self = this;
            if (self.seats().length > 0) {
                $.each(self.seats(), function (index, chair) {
                    chair.on('click.ticket', function () {
                        var chair = this;
                        $('#reserved_seating_session_select_notif').hide();
                        var ticketList = self.tickets();
                        if (chair !== self.selectedChair) {
                            // Select new chair
                            self.selectOtherChair(self.selectedChair, self);
                            self.selectedChair = chair;
                            self.lastColor = chair.getAttr('fill');
                            self.returnTicketIdBySeat(chair, ticketList);
                        } else {
                            // Re-select a chair
                            if (self.lastColor === chair.getAttr('fill')) {
                                self.returnTicketIdBySeat(chair, ticketList);
                            } else {
                                $.each(self.seats(), function (index, chairs) {
                                    if (chairs === chair) {
                                        chairs.setAttrs({stroke: 'black'});
                                        chairs.setAttrs({fill: self.lastColor});
                                    }
                                });
                                $('#ticket_session_id_main').val('');
                                $('#ticket_id_main').val('');
                                $('#ticket_seat_label_main_popup').html('');
                                $('#ticket_session_label_main_popup').html('');
                                $('#ticket_session_price_main_popup').html('');
                            }
                        }
                    });
                });
                $.each(self.labels(), function (index, label) {
                    label.on('click', function () {
                        var label = this;
                        var labelText = label.getAttr('text');
                        $.each(self.seats(), function (index, chair) {
                            if (chair.getAttr('label') === labelText) {
                                chair.fire('click');
                            }
                        });
                    });
                });
            }
        },

        returnTicketIdBySeat: function (chair, ticketList) {
            var self = this;
            var chairX = chair.attrs.x;
            var chairY = chair.attrs.y;
            var hasTicket = false;
            var notif = $('#reserved_seating_session_select_notif');
            $.each(ticketList, function (index, ticket) {
                $.each(ticket.chairs, function (index, seat) {
                    if ((chairX - seat.attrs.x) === 0 && (chairY - seat.attrs.y) === 0) {
                        hasTicket = true;
                        seat.setAttrs({fill: '#FF0000'});
                        seat.setAttrs({stroke: '#FF0000'});

                        var checkSoldOut = window.checkSoldOutUrl;
                        $.ajax({
                            url: checkSoldOut + 'session_ticket_id/' + ticket.id + '/ticket_label/' + seat.getAttr('label'),
                            type: "POST",
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (response) {
                                var data = JSON.parse(response);
                                if (data.success === false) {
                                    notif.show();
                                    notif.html(data.message);
                                } else {
                                    $('#ticket_session_id_main').val(data.session_id);
                                    $('#ticket_seat_label_main_popup').html(data.seat_label);
                                    $('#ticket_session_label_main_popup').html(data.ticket_label);
                                    $('#ticket_session_price_main_popup').html(data.sale_price);
                                    $('#ticket_id_main').val(data.ticket_id);
                                }
                            },
                            showLoader: true,
                            error: function (response) {
                            }
                        });
                    }
                });
            });
            if (!hasTicket) {
                notif.show();
                notif.html('This seat has not ticket for sell yet. Please come back later.');

                $('#ticket_session_id_main').val('');
                $('#ticket_id_main').val('');
                $('#ticket_seat_label_main_popup').html('');
                $('#ticket_session_label_main_popup').html('');
                $('#ticket_session_price_main_popup').html('');
            }
        },

        renderedTicketHandle: function (elements, ticket) {
            var self = this;
            var $el = $(".ticket-color[name='label_" + ticket.id + "']");
            $el.css("backgroundColor", ticket.colorTag()).val(ticket.colorTag());
            $el.val(ticket.colorTag());
            $el.ColorPicker({
                color: ticket.colorTag(),
                onChange: function (hsb, hex, rgb) {
                    $el.css("backgroundColor", "#" + hex).val("#" + hex);
                    $el.val("#" + hex);
                    $el.change();
                    $.each(ticket.chairs, function (index, chair) {
                        chair.setAttrs({fill: "#" + hex});
                    });
                    self.layer.draw();
                }
            });
            $('input.switch[name="switch_' + ticket.id + '"]').on('change', function (event) {
                $('input.switch[name!="switch_' + ticket.id + '"]').prop('checked', null);
                if ($('input.switch[name="switch_' + ticket.id + '"]:checked').length) {
                    self.currentTicket = ticket;
                }
                else {
                    self.currentTicket = false;
                }
            });
        },

        bindTicketWithChairGlobal: function (tickets) {
            var self = this;
            $.each(tickets, function (index, ticket) {
                if (ticket.chairs.length > 0) {
                    var chairsDataBind = [];
                    $.each(ticket.chairs, function (index, chairs) {
                        if (chairs.attrs !== undefined) {
                            var chairX = chairs.attrs.x;
                            var chairY = chairs.attrs.y;
                            $.each(self.seats(), function (index, seat) {
                                var seatX = seat.attrs.x;
                                var seatY = seat.attrs.y;
                                var deltaX = seatX - chairX;
                                var deltaY = seatY - chairY;
                                if (deltaX === 0 && deltaY === 0) {
                                    chairsDataBind.push(seat);
                                    seat.setAttrs({fill: ticket.colorTag()});
                                }
                            });
                        }
                    });
                    ticket.chairs = chairsDataBind;
                }
            });
        }
    });
});
