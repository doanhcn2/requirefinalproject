<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Product;

use Magento\Framework\View\Element\Template;

class VirtualReview extends Template
{
    /**
     * @var $_customerSession \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $session,
        array $data = []
    ) {
        $this->_customerSession = $session;
        $this->setTemplate('Magenest_Groupon::customer/virtual_review.phtml');
        parent::__construct($context, $data);
    }

    public function getCouponProductIds()
    {
        return [$this->getTicketProductId()];
    }

    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    private function getTicketProductId()
    {
        return $this->getProductId();
    }
}
