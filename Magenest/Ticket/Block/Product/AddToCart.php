<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Product;

use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class AddToCart
 * @package Magenest\Ticket\Block\Product
 */
class AddToCart extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_dealFactory \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFactory;

    /**
     * @var $_ticketLocation \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory
     */
    protected $_ticketLocationCollection;

    /**
     * @var $_grouponLocationCollection \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory
     */
    protected $_grouponLocationCollection;

    /**
     * AddToCart constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $ticketLocationCollection
     * @param \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory $grouponLocationFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Ticket\Model\ResourceModel\EventLocation\CollectionFactory $ticketLocationCollection,
        \Magenest\Groupon\Model\ResourceModel\Location\CollectionFactory $grouponLocationFactory,
        array $data = []
    ) {
        $this->_dealFactory = $dealFactory;
        $this->_productFactory = $productFactory;
        $this->_grouponLocationCollection = $grouponLocationFactory;
        $this->_ticketLocationCollection = $ticketLocationCollection;
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->initializeProductCollection();
        }

        return $this->_productCollection;
    }

    private function initializeProductCollection()
    {
        $layer = $this->getLayer();
        /* @var $layer \Magento\Catalog\Model\Layer */
        if ($this->getShowRootCategory()) {
            $this->setCategoryId($this->_storeManager->getStore()->getRootCategoryId());
        }

        // if this is a product view page
        if ($this->_coreRegistry->registry('product')) {
            // get collection of categories this product is associated with
            $categories = $this->_coreRegistry->registry('product')
                ->getCategoryCollection()->setPage(1, 1)
                ->load();
            // if the product is associated with any category
            if ($categories->count()) {
                // show products from this category
                $this->setCategoryId(current($categories->getIterator())->getId());
            }
        }

        $origCategory = null;
        if ($this->getCategoryId()) {
            try {
                $category = $this->categoryRepository->get($this->getCategoryId());
            } catch (NoSuchEntityException $e) {
                $category = null;
            }

            if ($category) {
                $origCategory = $layer->getCurrentCategory();
                $layer->setCurrentCategory($category);
            }
        }
        $collection = $layer->getProductCollection();

        if ($location = $this->getRequest()->getParam('locations')) {
            $productIds = $this->getProductLocations($location);
            $collection->getSelect()->where('e.entity_id IN (?)', $productIds);
        }

        $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

        if ($origCategory) {
            $layer->setCurrentCategory($origCategory);
        }

        $toolbar = $this->getToolbarBlock();
        $this->configureToolbar($toolbar, $collection);

        $this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $collection]
        );

        return $collection;
    }

    private function configureToolbar(Toolbar $toolbar, Collection $collection)
    {
        // use sortable parameters
        $orders = $this->getAvailableOrders();
        if ($orders) {
            $toolbar->setAvailableOrders($orders);
        }
        $sort = $this->getSortBy();
        if ($sort) {
            $toolbar->setDefaultOrder($sort);
        }
        $dir = $this->getDefaultDirection();
        if ($dir) {
            $toolbar->setDefaultDirection($dir);
        }
        $modes = $this->getModes();
        if ($modes) {
            $toolbar->setModes($modes);
        }
        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);
        $this->setChild('toolbar', $toolbar);
    }

    /**
     * Get post parameters
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        $type = $product->getTypeId();
        if ($type == 'ticket') {
            $url = $this->getUrlTicket($product);
        }

        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }

    /**
     * @param $product
     * @param array $additional
     * @return mixed
     */
    protected function getUrlTicket($product, $additional = [])
    {
        return $product->getUrlModel()->getUrl($product, $additional);
    }

    public function getEventInfo($productId){
        $allSession = \Magenest\Ticket\Helper\Event::getAllTicketAndSession($productId);
        $firstSession = $allSession->getFirstItem();
        $data = [];
        $time = $firstSession->getData('session_from');
        $weekday = date('D',strtotime($time));
        $day = date('j',strtotime($time));
        $month = date('M', strtotime($time));
        $data['week_day'] = $weekday;
        $data['day'] = $day;
        $data['month'] = $month;
        $countSessions = [];
        foreach ($allSession as $session){
            if($session->getData('tickets_id')!=''){
                if(isset($countSessions[$session->getData('tickets_id')])){
                    $countSessions[$session->getData('tickets_id')]++;
                }else{
                    $countSessions[$session->getData('tickets_id')]=0;
                }
            }
        }
        $data['number_session'] = max($countSessions);
        return $data;
    }

    public function getGrouponCountdownTimer($productId)
    {
        $product = $this->_productFactory->create()->load($productId);
        if ($product->getCountdownEnabled() === "1" || $product->getCountdownEnabled() === 1) {
            return true;
        }
        return false;
    }

    public function getProductLocations($locations)
    {
        $idLocation = [];
        $location = explode('>', $locations);
        foreach($location as $item) {
            $locationByLocation = $this->getProductHasLocation($item);
            $idLocation = array_merge($idLocation, $locationByLocation);
        }

        return array_unique($idLocation);
    }

    /**
     * @param $locations
     * @return array
     */
    private function getProductHasLocation($locations)
    {
        $location = explode('||', $locations);
        if (!isset($location[0]) || !isset($location[1])) {
            return [];
        }
        $key = $location[0];
        $ticketIds = $this->_ticketLocationCollection->create()
            ->addFieldToFilter(['city', 'state'], [['like' => $key], ['like' => $key]])
            ->addFieldToFilter('country', $location[1])->getColumnValues('product_id');

        $grouponLocationIds = $this->_grouponLocationCollection->create()
            ->addFieldToFilter(['city', 'region'], [['like' => $key], ['like' => $key]])
            ->addFieldToFilter('country', $location[1])->getColumnValues('location_id');

        $fields = [];
        $searches = [];
        foreach($grouponLocationIds as $locationId) {
            array_push($fields, "location_ids");
            $string = $locationId;
            array_push($searches, ['like' => $string]);
            array_push($fields, "location_ids");
            $string = $locationId . ",%";
            array_push($searches, ['like' => $string]);
            array_push($fields, "location_ids");
            $string = "%," . $locationId . ",%";
            array_push($searches, ['like' => $string]);
            array_push($fields, "location_ids");
            $string = "%," . $locationId;
            array_push($searches, ['like' => $string]);
        }
        $grouponIds = $this->_dealFactory->create()->getCollection()
            ->addFieldToFilter($fields, $searches)->getColumnValues('product_id');

        $productIds = array_merge($ticketIds, $grouponIds);

        return array_unique($productIds);
    }
}
