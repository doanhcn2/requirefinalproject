<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Product\View;

use Magento\Catalog\Block\Product\View\Attributes as MagentoAttributes;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Attributes extends MagentoAttributes
{
    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Framework\Registry $registry,
                                PriceCurrencyInterface $priceCurrency,
                                \Magenest\Ticket\Model\EventFactory $eventFactory,
                                array $data = [])
    {
        $this->_eventFactory = $eventFactory;
        parent::__construct($context, $registry, $priceCurrency, $data);
    }

    public function getEventTicket()
    {
        $productId = $this->getProduct()->getId();

        return $this->_eventFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getFirstItem();
    }
}