<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Product;

use Magento\Framework\View\Element\Template;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magento\Checkout\Model\Cart as CustomerCart;


class Ticket extends Template
{
    /**
     * Google Map API key
     */
    const XML_PATH_GOOGLE_MAP_API_KEY = 'event_ticket/general_config/google_api_key';

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $eventFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * @var $_venueMapDataFactory VenueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * Ticket constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     * @param \Magenest\Ticket\Model\SessionFactory $eventSessionFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magento\Directory\Model\Currency $currency
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        \Magenest\Ticket\Model\SessionFactory $eventSessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magento\Directory\Model\Currency $currency,
        VenueMapDataFactory $venueMapDataFactory,
        CustomerCart $cart,
        array $data
    ) {
        $this->_venueMapDataFactory = $venueMapDataFactory;
        $this->_currency            = $currency;
        $this->eventFactory         = $eventFactory;
        $this->tickets              = $ticketsFactory;
        $this->location             = $eventLocationFactory;
        $this->session              = $eventSessionFactory;
        $this->_coreRegistry        = $context->getRegistry();
        $this->cart                 = $cart;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getGiftDetails()
    {
        $result        = ['name' => '', 'email' => '', 'message' => ''];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $objectManager->get('Magento\Framework\App\Request\Http');
        $layout  = $request->getFullActionName();
        if ($layout == 'checkout_cart_configure') {
            $quoteItemId = $this->getRequest()->getParam('id');
            if (!$quoteItemId)
                return $result;
            $quoteItem = $this->cart->getQuote()->getItemById($quoteItemId);

            $additionalOptions = $quoteItem->getBuyRequest()->getAdditionalOptions();
            $result            = [
                'name'    => @$additionalOptions['To'] == null ? '' : @$additionalOptions['To'],
                'email'   => @$additionalOptions['Email'] == null ? '' : @$additionalOptions['Email'],
                'message' => @$additionalOptions['Message'] == null ? '' : @$additionalOptions['Message']
            ];

        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getCurrentProductId()
    {
        $id = $this->_coreRegistry->registry('current_product')->getId();

        return $id;
    }

    public function getAllSessionMap()
    {
        return $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getCurrentProductId());
    }

    public function getAllSessionMapDataIds()
    {
        $allSessionsMapId = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getCurrentProductId());
        $ids              = [];
        foreach ($allSessionsMapId as $session) {
            array_push($ids, $session->getSessionMapId());
        }
        return $ids;
    }

    /**
     * @return mixed
     */
    public function getProductType()
    {
        $product = $this->_coreRegistry->registry('current_product');

        return $product->getTypeId();
    }

    /**
     * @return bool
     */
    public function isEventTicketProduct()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $event   = $this->eventFactory->create()->getCollection()->addFieldToFilter('product_id', $product->getId())->getFirstItem();
        if ($event->getEventId()) {
            return true;
        } else {
            return false;
        }
    }

    public function isReservedSeatingEvent()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $event   = $this->eventFactory->create()->getCollection()->addFieldToFilter('product_id', $product->getId())->getFirstItem();
        if ($event->getEventId() && @$event->getIsReservedSeating() === '1') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function getCoordinates()
    {
        $location = $this->getLocation();
        if ($location != null && count($location->getData()) > 0) {
            return [
                'latitude'  => $location['latitude'],
                'longitude' => $location['longitude']
            ];
        }

        return null;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol = $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    /**
     * @return $this
     */
    public function getEvent()
    {
        $model = $this->eventFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $this->getCurrentProductId())->getFirstItem();

        return $model;
    }

    public function getTicketType()
    {
        return $this->getEvent()->getTicketType();
    }

    /**
     * @return $this
     */
    public function getLocation()
    {
        $model = null;
        if ($this->getEvent() && $this->getEvent()->getIsOnline() == 1) {
            $model = $this->location->create()->getCollection()
                ->addFieldToFilter('product_id', $this->getCurrentProductId())->getFirstItem();
        }

        return $model;
    }

    /**
     * @return $this|null
     */
    public function getTicket()
    {
        if ($this->getEvent()->getTicketType()) {
            $model  = $this->tickets->create()->getCollection()
                ->addFieldToFilter('product_id', $this->getCurrentProductId())
                ->addFieldToFilter('type', $this->getEvent()->getTicketType());
            $result = $this->ignoreSaleOutTicketBaseOnSetting($model);

            return $result;
        }

        return false;
    }

    public function getSessionReservedSeatingEvent()
    {
        if ($this->getEvent()->getTicketType()) {
            /**
             * @var $collection \Magenest\Ticket\Model\ResourceModel\Session\Collection
             */
            $collection  = $this->session->create()->getCollection();
            $sessionData = $collection->addFieldToSelect('product_id')->addFieldToSelect('session_map_id')->addFieldToSelect('session_from')->addFieldToSelect('session_to')->addFieldToFilter('product_id', $this->getCurrentProductId())->distinct(true);

            $optionSession  = [];
            $existSessionId = [];
            foreach ($sessionData as $session) {
                if (!in_array($session->getSessionMapId(), $existSessionId)) {
                    $title = "From " . $session->getSessionFrom() . " To " . $session->getSessionTo();
                    array_push($optionSession, ['value' => $session->getSessionMapId(), 'label' => $title]);
                }
                array_push($existSessionId, $session->getSessionMapId());
            }
            return $optionSession;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getLayoutSingle()
    {
        $layout = $this->_layout->createBlock('Magenest\Ticket\Block\Product\Single');

        return $layout->toHtml();
    }

    /**
     * @return string
     */
    public function getReturnSession()
    {
        return $this->getUrl('ticket/order/session');
    }

    /**
     * @return string
     */
    public function getReturnOptionTime()
    {
        return $this->getUrl('ticket/order/optiontime');
    }

    /**
     * @param $sessionId
     * @return string
     */
    public function getSaleTimeBySession($sessionId)
    {
        $session = $this->session->create()->load($sessionId);
        return $session->getSellFrom() . " - " . $session->getSellTo();
    }

    /**
     * @param $model
     * @return array
     */
    private function ignoreSaleOutTicketBaseOnSetting($model)
    {
        $result = [];
        foreach ($model as $ticket) {
            $productId  = $ticket->getProductId();
            $ticketId   = $ticket->getTicketsId();
            $allSession = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketId)->addFieldToFilter('product_id', $productId)->getData();
            foreach ($allSession as $session) {
                if ($session['hide_sold_out'] === '0') {
                    array_push($result, $ticket);
                    break;
                } elseif ($session['qty_available'] !== '0') {
                    array_push($result, $ticket);
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getSessionOptions()
    {
        $product    = $this->_coreRegistry->registry('current_product');
        $productId  = $product->getId();
        $ticketList = $this->tickets->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getAllIds();
        $sessions   = $this->session->create()->getCollection()
            ->addFieldToFilter('tickets_id', array('in' => $ticketList));

        return $sessions->getSize();
    }

    /**
     * @return mixed
     */
    public function getSessionOptionsArray()
    {
        $product       = $this->_coreRegistry->registry('current_product');
        $productId     = $product->getId();
        $ticketList    = $this->tickets->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getAllIds();
        $sessions      = $this->session->create()->getCollection()
            ->addFieldToFilter('tickets_id', array('in' => $ticketList));
        $dateArrayTemp = [];
        /**
         * @var \Magenest\Ticket\Model\Session $item
         */
        foreach ($sessions->getItems() as $item) {
            $dateArrayTemp[] = strtotime(date('Y-m-d', strtotime($item->getSessionFrom())));
        }
        $dateArrayTemp = array_unique($dateArrayTemp);
        asort($dateArrayTemp);
//        $dateArray = [];
//        foreach ($dateArrayTemp as $value) {
//            $dateArray[] = date('Y-m-d', $value);
//        }


        return $dateArrayTemp;
    }

    /**
     * @return array|null
     */
    public function allTicketPrice()
    {
        $product       = $this->_coreRegistry->registry('current_product');
        $productId     = $product->getId();
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        if ($sessionTicket->count() == 0)
            return null;
        $result = [];

        /**
         * @var \Magenest\Ticket\Model\Session $session
         */
        foreach ($sessionTicket as $session) {
            $current = date('Y-m-d H:i:s');
            if (strtotime($current) < strtotime($session->getSellTo()) AND $session->getQtyAvailable() > 0) {
                if ($session->getSalePrice()) {
                    array_push($result, $session->getSalePrice());
                } else {
                    array_push($result, $session->getOriginalValue());
                }
            }
        }
        if (count($result) == 0) return [0, 0];
        elseif (count($result) == 1) return [@$result[0], @$result[0]];
        else return [@min($result), @max($result)];
    }

    /**
     * @return null
     */
    public function getLastTimeCoundown()
    {
        $product       = $this->_coreRegistry->registry('current_product');
        $productId     = $product->getId();
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
        if ($sessionTicket->count() == 0)
            return null;
        $result = [];

        foreach ($sessionTicket as $session) {
            $current = date('Y-m-d H:i:s');
            if (strtotime($current) < strtotime($session->getSellTo())) {
                array_push($result, strtotime($session->getSellTo()));

            }
        }
        return max($result);
    }
}
