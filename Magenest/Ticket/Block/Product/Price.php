<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Product;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;
use Magenest\Ticket\Model\SessionFactory;

/**
 * Class Ticket
 * @package Magenest\Ticket\Block\Product
 */
class Price extends Template
{
    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $eventFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * Price constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param SessionFactory $sessionFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magento\Directory\Model\Currency $currency
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magento\Directory\Model\Currency $currency,
        array $data
    )
    {
        $this->_currency = $currency;
        $this->eventFactory = $eventFactory;
        $this->tickets = $ticketsFactory;
        $this->session = $sessionFactory;
        $this->_coreRegistry = $context->getRegistry();
        parent::__construct($context, $data);
    }

    public function getProductType()
    {
        return $this->_coreRegistry->registry('current_product')->getTypeId();
    }

    public function getSimpleProductPrice()
    {
        $result = [];
        $product = $this->_coreRegistry->registry('current_product');
        /**
         * @var $product \Magento\Catalog\Model\Product
         */
        $bestVendor = $product->getUdmultiBestVendor();
        $allUdmultiVendorData = $product->getAllMultiVendorData();
        $vendorMicroSiteId = $this->checkMicroSitePage($allUdmultiVendorData);
        if (!$vendorMicroSiteId) {
            $vendorMicroSiteId = $bestVendor;
        }
        foreach ($allUdmultiVendorData as $key => $vendorData) {
            if (intval($vendorMicroSiteId) !== $key) {
                continue;
            }
            if ($vendorData['special_price']) {
                $specialFrom = $vendorData['special_from_date'];
                $specialTo = $vendorData['special_to_date'];
                if ($specialFrom === null && $specialTo !== null) {
                    $result = $this->compareVendorPrice($vendorData, (time() < strtotime($specialTo)));
                } elseif ($specialTo === null && $specialFrom !== null) {
                    $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom)));
                } elseif ($specialFrom === null && $specialTo === null) {
                    $result['old_price'] = $vendorData['vendor_price'];
                    $result['current_price'] = $vendorData['special_price'];
                } else {
                    $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom) && time() < strtotime($specialTo)));
                }
            } else {
                $result['current_price'] = $vendorData['vendor_price'];
            }
        }
        if (!isset($result['current_price'])) {
            $result['current_price'] = $product->getFinalPrice();
        }

        return $result;
    }

    private function compareVendorPrice($vendorData, $compare)
    {
        $result = [];
        if ($compare) {
            $result['old_price'] = $vendorData['vendor_price'];
            $result['current_price'] = $vendorData['special_price'];
        } else {
            $result['current_price'] = $vendorData['vendor_price'];
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getCurrentProductId()
    {
        $id = $this->_coreRegistry->registry('current_product')->getId();

        return $id;
    }

    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol = $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    public function getCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    /**
     * @return $this
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }

    public function getEvent()
    {
        $model = $this->eventFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $this->getCurrentProductId())->getFirstItem();

        return $model;
    }

    private function getAllProductTicketPrice()
    {
        $productId = $this->getCurrentProductId();
        $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('sell_to', ['gt' => date('Y-m-d H:i:s', time())])->addFieldToFilter('qty_available', ['gt' => 0]);
        $result = [];
        foreach ($sessionTicket as $session) {
            if ($session->getSalePrice()) {
                array_push($result, $session->getSalePrice());
            } else {
                array_push($result, $session->getOriginalValue());
            }
        }
        return $result;
    }

    public function getProductPrice()
    {
        $allPrice = $this->getAllProductTicketPrice();
        if (count($allPrice) === 0) {
            return number_format(0, 2);
        }
        $minPrice = min($allPrice);
        $maxPrice = max($allPrice);
        if ($minPrice == $maxPrice) {
            return number_format($minPrice, 2);
        } else {
            return [number_format($minPrice, 2), number_format($maxPrice, 2)];
        }
    }

    public function isHasRange()
    {
        $productPrice = $this->getProductPrice();
        if (is_array($productPrice)) {
            return true;
        } else {
            return false;
        }
    }

    public function isHasSalePrice()
    {
        $productId = $this->getCurrentProductId();
        $ticketType = $this->eventFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getFirstItem()->getTicketType();
        if ($ticketType === 'paid') {
            $sessionTicket = $this->session->create()->getCollection()->addFieldToFilter('product_id', $productId);
            $result = [];
            $hasSalePrice = false;
            foreach ($sessionTicket as $session) {
                if ($session->getSalePrice()) {
                    $hasSalePrice = true;
                }
                array_push($result, $session->getOriginalValue());
            }
            if ($hasSalePrice) {
                return $this->_preparePrice($result);
            } else {
                return false;
            }
        }
        return false;
    }

    private function _preparePrice($originalPrice)
    {
        if (is_array($originalPrice)) {
            return [min($originalPrice), max($originalPrice)];
        } else {
            return 0;
        }
    }

    public function isFreeTicketType()
    {
        if ($this->getEvent()->getTicketType() === 'free') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if product is view from vendor microsite page
     *
     * @param $allUdmultiData
     * @return bool|mixed
     */
    private function checkMicroSitePage($allUdmultiData)
    {
        $vendors = $this->getVendorIdsFromUdmultiData($allUdmultiData);
        /**
         * @var $micrositeHelper \Unirgy\DropshipMicrosite\Helper\Data
         */
        $micrositeHelper = ObjectManager::getInstance()->create('Unirgy\DropshipMicrosite\Helper\Data');
        if (!$micrositeHelper->getCurrentVendor()) {
            return false;
        }
        $currentVendorId = (int)$micrositeHelper->getCurrentVendor()->getVendorId();
        foreach($vendors as $vendor) {
            if ($vendor === $currentVendorId) {
                return $vendor;
            }
        }

        return false;
    }

    private function getVendorIdsFromUdmultiData($allUdmultiData)
    {
        $result = [];
        if (is_array($allUdmultiData)) {
            foreach($allUdmultiData as $key => $udmultiDatum) {
                array_push($result, $key);
            }
        }

        return $result;
    }
}
