<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:28
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use \Magenest\Ticket\Block\Vendor\Campaign;

class EventActivity extends Campaign
{

    public function getRedeemedTicketsCollection()
    {
        $productId = $this->getProductId();
        return $this->_ticketPurchasedFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 2);
    }

    public function getQtyRedeemed()
    {
        return $this->getRedeemedTicketsCollection()->count();
    }

    public function getActiveTicketsCollection(){
        $productId = $this->getProductId();
        return $this->_ticketPurchasedFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 1);
    }

    public function getQtyAvailable()
    {
        return $this->getActiveTicketsCollection()->count();

    }

    public function getRefundedTicketsCollection(){
        $productId = $this->getProductId();
        return $this->_ticketPurchasedFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)
            ->addFieldToFilter("status", 3);
    }

    public function getQtyRefunded()
    {
        return $this->getRefundedTicketsCollection()->count();
    }

    public function getQtySold(){
        $sessions = $this->getSessionsCollection();
        $qtyPurchased = 0;
        foreach ($sessions as $session) {
            $qtyPurchased += $session->getQtyPurchased();
        }
        return $qtyPurchased;
    }
}