<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use \Magenest\Ticket\Block\Vendor\Campaign;

class SalesChart extends Campaign
{

    public function getDays($dayNum)
    {
        $now = new \DateTime('now');
        $list = array();
        if ($dayNum === "all_time") {
            $saleStart = null;
            $saleEnd = null;
            $sessions = $this->getSessionsCollection();
            foreach ($sessions as $session) {
                $sellFrom = date('Y-m-d', strtotime($session->getSellFrom()));
                $sellTo = date('Y-m-d', strtotime($session->getSellTo()));
                if ($saleStart == null || $sellFrom < $saleStart)
                    $saleStart = $sellFrom;
                if ($saleEnd == null || $sellTo > $saleStart)
                    $saleEnd = $sellTo;
            }
            $start = strtotime($saleStart);
            $end = strtotime($saleEnd);
            $diffInSeconds = $end - $start;
            $datediff = $diffInSeconds / 86400;
            for ($d = 0; $d <= $datediff; $d++) {
                $list[] = $saleStart;
                $saleStart = date('Y-m-d', strtotime('+1 day', strtotime($saleStart)));
            }
        } elseif ($dayNum === "this_month") {
            $month = $now->format('m');
            $year = $now->format('Y');

            for ($d = 1; $d <= 31; $d++) {
                $time = mktime(12, 0, 0, $month, $d, $year);
                if (date('m', $time) == $month)
                    $list[] = date('Y-m-d', $time);
            }
        } else {
            for ($i = $dayNum - 1; $i >= 0; $i--) {
                $date = new \DateTime();
                $date->sub(new \DateInterval('P' . $i . 'D'));
                $list[] = $date->format('Y-m-d');
            }
        }

        return $list;
    }


    public function getOption($dayOption)
    {
        $daysList = $this->getDays($dayOption);
        $return = [];
        $ticketsSoldFrom = $this->getTicketsSoldFrom($daysList);
        $ticketsRefundedFrom = $this->getTicketsRefundedFrom($daysList);
        foreach ($daysList as $day) {

            $return [$day] = [
                'sales' => $ticketsSoldFrom[$day]['sales'],
                'qtySold' => $ticketsSoldFrom[$day]['qtySold'],
                'refunds' => $ticketsRefundedFrom[$day]['refunds'],
                'qtyRefund' => $ticketsRefundedFrom[$day]['qtyRefund']
            ];
        }
        return $return;
    }

    public function getTicketsSoldFrom($daysList)
    {
//        $daysList = $this->getDays($dayNum);
        $return = [];
        $productId = $this->getProductId();
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_ticketPurchasedFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', ['nin' => [3]])
                ->addFieldToFilter('created_at', array('like' => "%$day%"));
            $return [$day] = [
                'sales' => 0,
                'qtySold' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['sales'] += $sold;
                $return [$day]['qtySold']++;
            }
        }
        return $return;
    }

    public function getTicketsRefundedFrom($daysList)
    {
//        $daysList = $this->getDays($dayNum);
        $return = [];
        $productId = $this->getProductId();
        foreach ($daysList as $day) {
            $ticketsCollection = $this->_ticketPurchasedFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('status', ['in' => [3]])
                ->addFieldToFilter('refunded_at', array('like' => "%$day%"));
            $return [$day] = [
                'refunds' => 0,
                'qtyRefund' => 0
            ];
            foreach ($ticketsCollection as $ticket) {
                $sessionId = unserialize($ticket->getInformation())['session_id'];
                $session = $this->_sessionFactory->create()->load($sessionId);
                if ($session->getSalePrice() > 0)
                    $sold = $session->getSalePrice();
                else
                    $sold = $session->getOriginalPrice();
                $return [$day]['refunds'] += $sold;
                $return [$day]['qtyRefund']++;
            }
        }
        return $return;
    }

    public function getValueTicketSoldFrom($dayNum)
    {

        $ticketsSold = $this->getTicketsSoldFrom($this->getDays($dayNum));
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['sales'];
            $qty += $day['qtySold'];
        }

        return [
            'sales' => $sale,
            'qty' => $qty
        ];
    }

    public function getValueTicketRefundedFrom($dayNum)
    {
        $ticketsSold = $this->getTicketsRefundedFrom($this->getDays($dayNum));
        $sale = 0;
        $qty = 0;
        foreach ($ticketsSold as $day) {
            $sale += $day['refunds'];
            $qty += $day['qtyRefund'];
        }

        return [
            'refunds' => $sale,
            'qtyRefund' => $qty
        ];
    }

    public function getQtyRedeemedFrom($dayNum)
    {
        $daysList = $this->getDays($dayNum);
        $qtyRedeemed = 0;
        foreach ($daysList as $day) {
            $ticketsPurchased = $this->_ticketPurchasedFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $this->getProductId())
                ->addFieldToFilter('status', 2)
                ->addFieldToFilter('redeemed_at', array('like' => "%$day%"));
            $qtyRedeemed += $ticketsPurchased->count();
        }
        return $qtyRedeemed;
    }
}