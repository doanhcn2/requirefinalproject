<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use \Magenest\Ticket\Block\Vendor\Campaign;
use Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class Detail extends Campaign
{
    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    public function __construct(
        EventFactory $eventFactory,
        TicketFactory $ticketPurchasedFactory,
        TicketsFactory $ticketsFactory,
        SessionFactory $sessionFactory,
        ProductFactory $productFactory,
        OrderFactory $orderFactory,
        SessionHelper $sessionHelper,
        Template\Context $context,
        array $data = [])
    {
        $this->_productFactory = $productFactory;
        parent::__construct($eventFactory, $ticketPurchasedFactory, $ticketsFactory, $sessionFactory, $orderFactory, $sessionHelper, $context, $data);
    }

    public function getProductName()
    {
        return $this->_eventFactory->create()->load($this->getProductId(), 'product_id')->getEventName();
    }

    public function getProductCampaignStatus()
    {
        $status = $this->_productFactory->create()->load($this->getProductId())->getCampaignStatus();
        return CampaignStatus::getStatusLabelByCode($status);
    }

    public function getCategoriesCount()
    {
        $product = $this->_productFactory->create()->load($this->getProductId());
        return count($product->getCategoryIds());
    }

    public function getSessionCount()
    {
        $tickets = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getAllIds();
        $sessions = $this->_sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $tickets])->getSize();
        return $sessions;
    }

    public function getSessionTitle()
    {
        if ($this->getSessionId()) {
            $session = $this->_sessionFactory->create()->load($this->getSessionId());
            $sessionStart = strtotime($session->getSessionFrom());

            return __(date("D, M d, Y %1 h:m A", $sessionStart), "at");
        }
        return;
    }

    public function getCustomerViewUrl()
    {
        return $this->_productFactory->create()->load($this->getProductId())->getProductUrl();
    }
}
