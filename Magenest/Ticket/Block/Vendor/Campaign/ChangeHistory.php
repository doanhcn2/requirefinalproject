<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Groupon\Model\StatusFactory;
use \Magenest\Ticket\Block\Vendor\Campaign;
use Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\Config\Source\SessionStatus;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class ChangeHistory extends Campaign
{
    /**
     * @var $_statusHistory StatusFactory
     */
    protected $_statusHistory;

    public function __construct(
        EventFactory $eventFactory,
        TicketFactory $ticketPurchasedFactory,
        TicketsFactory $ticketsFactory,
        SessionFactory $sessionFactory,
        OrderFactory $orderFactory,
        SessionHelper $sessionHelper,
        StatusFactory $statusFactory,
        Template\Context $context,
        array $data = []
    ) {
        $this->_statusHistory = $statusFactory;
        parent::__construct($eventFactory, $ticketPurchasedFactory, $ticketsFactory, $sessionFactory, $orderFactory, $sessionHelper, $context, $data);
    }

    public function getChangeHistory()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 20;

        $collection = $this->_statusHistory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->setOrder('created_at', 'DESC');
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }

        return $collection;
    }

    public function getStatusLabel($id)
    {
        return CampaignStatus::getStatusLabelByCode($id);
    }
}