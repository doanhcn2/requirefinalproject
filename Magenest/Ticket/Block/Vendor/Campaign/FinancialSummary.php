<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use \Magenest\Ticket\Block\Vendor\Campaign;
use Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\Config\Source\SessionStatus;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class FinancialSummary extends Campaign
{
    public function __construct(EventFactory $eventFactory, TicketFactory $ticketPurchasedFactory, TicketsFactory $ticketsFactory, SessionFactory $sessionFactory, OrderFactory $orderFactory, SessionHelper $sessionHelper, Template\Context $context, array $data = [])
    {
        parent::__construct($eventFactory, $ticketPurchasedFactory, $ticketsFactory, $sessionFactory, $orderFactory, $sessionHelper, $context, $data);
    }
}
