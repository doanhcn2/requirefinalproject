<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Ticket\Block\Vendor\Campaign\Session;

use \Magenest\Ticket\Block\Vendor\Campaign;
use Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class TicketTypeGrid extends Campaign
{

    public function __construct(EventFactory $eventFactory, TicketFactory $ticketPurchasedFactory, TicketsFactory $ticketsFactory, SessionFactory $sessionFactory, OrderFactory $orderFactory, SessionHelper $sessionHelper, Template\Context $context, array $data = [])
    {
        parent::__construct($eventFactory, $ticketPurchasedFactory, $ticketsFactory, $sessionFactory, $orderFactory, $sessionHelper, $context, $data);
    }

    public function getTicketTypeBySessionCollection($sessionId)
    {
        $params = $this->getRequest()->getParams();
        /** @var \Magenest\Ticket\Model\ResourceModel\Session\Collection $collection */
        $collection = $this->_sessionFactory->create()->getCollection();
        $sessions = $this->getRelatedSession($sessionId);
        if ($sessionId) {
            $collection->addFieldToFilter('main_table.session_id', ['in' => $sessions]);
        }
        if($this->getEventType()=='free')
            $revenue = "`main_table`.`qty_purchased`*0";
        else
            $revenue ="`main_table`.`sale_price`*`main_table`.`qty_purchased`";
        $collection->join(
            $collection->getTable('magenest_ticket_sessions'),
            "`main_table`.`session_id`=`magenest_ticket_sessions`.`session_id`
            AND `main_table`.`product_id` = ".$params['id'],
            "(".$revenue.") AS revenueOld"
        )->join(
            $collection->getTable('magenest_ticket_tickets'),
                "`main_table`.`tickets_id` = `magenest_ticket_tickets`.`tickets_id`",
            ["magenest_ticket_tickets.title AS ticket_category", "magenest_ticket_tickets.tickets_id as ticket_ref"]);
        $page = @$params['p'] ? $params['p'] : 1;
        $pageSize = $this->getPageSizeSession();
//        $pageSize = 2;

        if (isset($params['sortt']) && isset($params['order'])) {
            $collection->setOrder($params['sortt'], strtoupper($params['order']));
        }
        $collection->setPageSize($pageSize)->setCurPage($page);
        return $collection;
    }

    public function getSessionStatus($sessionId)
    {
        return $this->_sessionHelper->getSessionStatus($sessionId);
    }

    public function getSessionTitle($sessionId)
    {
        $session = $this->_sessionFactory->create()->load($sessionId);
        $sessionStart = strtotime($session->getSessionFrom());

        return __(date("D, M d, Y %1 h:m A", $sessionStart), "at");
    }

    public function getSessionRevenue($sessionId)
    {
        return $this->_sessionHelper->getSessionRevenue($sessionId,$this->getProductId());
    }

    public function getQtyRedeemed($sessionId)
    {
        return $this->_sessionHelper->getQtyRedeemed($sessionId, $this->getProductId());

    }
}
