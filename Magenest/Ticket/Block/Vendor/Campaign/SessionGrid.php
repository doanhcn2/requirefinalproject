<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 10:29
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use \Magenest\Ticket\Block\Vendor\Campaign;

class SessionGrid extends Campaign
{

    public function getSessionStatus($sessionId)
    {
        return $this->_sessionHelper->getSessionStatus($sessionId);
    }

    public function getSessionTitle($sessionId)
    {
        $session = $this->_sessionFactory->create()->load($sessionId);
        $sessionStart = strtotime($session->getSessionFrom());

        return __(date("D, M d, Y %1 h:m A", $sessionStart), "at");
    }

    public function getSessionRevenue($sessionId)
    {
        return $this->_sessionHelper->getSessionRevenue($sessionId,$this->getProductId());
    }


    public function getQtyRedeemed($sessionId)
    {
        return $this->_sessionHelper->getQtyRedeemed($sessionId, $this->getProductId());

    }

}