<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Vendor\Campaign;

use \Magenest\Ticket\Block\Vendor\Campaign;
use Magenest\Ticket\Helper\Session as SessionHelper;
use Magenest\Ticket\Model\Config\Source\SessionStatus;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\Ticket;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\OrderFactory;

class PurchasedTickets extends Campaign
{

    public function getPurchasedTicketList()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 20;

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_ticketPurchasedFactory->create()->getCollection();
        $collection->addFieldToFilter('event_id', $this->getEventId())->addFieldToFilter('vendor_id', $this->getVendorId());
        $params = $this->getRequest()->getParams();
        if ($this->getSessionId()) {
            $collection->addFieldToFilter('main_table.session_id', ['in' => $this->getRelatedSession($this->getSessionId())]);
        }
        if (isset($params['search'])) {
            $pattern = '%' . $params['search'] . '%';
            $collection->addFieldToFilter(['code','customer_name'], [['like' => $pattern], ['like' => $pattern]]);
        }

        if (isset($params['sortpt']) && isset($params['order'])) {
            $collection->setOrder($params['sortpt'], strtoupper($params['order']));
        }

        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }

        $eventTable = $collection->getTable('magenest_ticket_sessions');
        $ticketsTable = $collection->getTable('magenest_ticket_tickets');

        $joinStatement = $collection->getSelect()->joinLeft(
            $eventTable,
            "main_table.session_id=$eventTable.session_id",
            ["$eventTable.session_from"]
        )->joinLeft(
            $ticketsTable,
            "main_table.tickets_id=$ticketsTable.tickets_id",
            ["$ticketsTable.title as ticket_type"]
        );

        $collection->getConnection()->fetchAll($joinStatement);
        return $collection;
    }

    private function getEventId()
    {
        return $this->_eventFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())
            ->getFirstItem()->getId();
    }

    public function getTicketStatus($status)
    {
        $result = null;
        switch ($status) {
            case Ticket::STATUS_AVAILABLE:
                {
                    $result = 'Available';
                    break;
                }
            case Ticket::STATUS_USED:
                {
                    $result = 'Used';
                    break;
                }
            case Ticket::STATUS_REFUNDED:
                {
                    $result = 'Refunded';
                    break;
                }
            case Ticket::STATUS_CANCELED:
                {
                    $result = 'Canceled';
                    break;
                }
            default:
                {
                    $result = null;
                    break;
                }
        }
        return $result;
    }
}
