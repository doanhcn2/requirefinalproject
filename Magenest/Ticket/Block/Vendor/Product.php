<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Vendor;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Unirgy\Dropship\Helper\Data as HelperData;

/**
 * Class Product
 * @package Magenest\Ticket\Block\Vendor
 */
class Product extends Template
{
    protected $_template = 'vendor/product.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $country;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $event;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var $venueMapFactory VenueMapFactory
     */
    protected $venueMapFactory;

    protected $_productUnirgyCollection;
    /**
     * @var HelperData
     */
    protected $_hlp;

    /**
     * Product constructor.
     * @param Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Directory\Model\Config\Source\Country $country
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param HelperData $helperData
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     * @param VenueMapFactory $venueMapFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\Collection $productUnirgyCollection,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        HelperData $helperData,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        VenueMapFactory $venueMapFactory,
        array $data = []
    )
    {
        $this->_productUnirgyCollection = $productUnirgyCollection;
        $this->venueMapFactory = $venueMapFactory;
        $this->_coreRegistry = $registry;
        $this->_hlp = $helperData;
        $this->country = $country;
        $this->event = $eventFactory;
        $this->location = $eventLocationFactory;
        parent::__construct($context, $data);
    }

    /**
     * initial construct
     */
    protected function construct()
    {
        parent::_construct();
    }

    /**
     * @return array
     */
    public function getListCountry()
    {
        return $this->country->toOptionArray();
    }

    /**
     * @return bool
     */
    public function getProductId()
    {
        $id = $this->getInformation();

        if ($id) {
            return $id;
        }

        return "new";
    }

    /**
     * @return $this|bool
     */
    public function getEvent()
    {
        if ($this->getProductId()) {
            $model = $this->event->create()->load($this->getProductId(), 'product_id');

            return $model;
        }

        return false;
    }

    public function getVendorSku()
    {
        $productId = $this->getProductId();
        if ($productId){
            $productUnirgy = $this->_productUnirgyCollection->addProductFilter([$productId]);

            if($productUnirgy->count()>0)
                return $productUnirgy->getFirstItem()->getVendorSku();
        }

        return null;
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    /**
     * @return $this|bool
     */
    public function getLocation()
    {
        if ($this->getProductId() && $this->getEvent() && $this->getEvent()->getIsOnline() == 1) {
            $model = $this->location->create()->load($this->getProductId(), 'product_id');
            return $model;
        }

        return false;
    }

    public function getVenueMapOptions()
    {
        $vendorId = $this->_hlp->session()->getVendorId();
        $allVenueMap = $this->venueMapFactory->create()->getCollection()->addFieldToFilter('vendor_id', $vendorId);
        $optionsResult = [];
        foreach ($allVenueMap as $option) {
            $item = ['key' => $option->getVenueMapId(), 'label' => $option->getVenueMapName()];
            array_push($optionsResult, $item);
        }
        return $optionsResult;
    }
}
