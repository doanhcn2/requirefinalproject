<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Vendor;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magenest\Ticket\Model\EventFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\Product as ProductModel;

class ManageEvent extends Template
{
    protected $_template = 'vendor/manage_event.phtml';

    /**
     * @var $_attributeInterface \Magento\Eav\Api\AttributeRepositoryInterface
     */
    protected $_attributeInterface;

    /**
     * @var $_eventsFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventsFactory;

    /**
     * @var $productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var $_productRepositoryInterface \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepositoryInterface;

    /**
     * @var $_unirgyHelper \Unirgy\Dropship\Helper\Data
     */
    protected $_unirgyHelper;

    /**
     * @var $_unirgySession \Unirgy\Dropship\Model\Session
     */
    protected $_unirgySession;

    /**
     * @var $_searchCri \Magento\Framework\Api\SearchCriteriaInterface
     */
    protected $_searchCriteriaInterface;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroup
     */
    protected $_filterGroup;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var $_productCollection \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollection;

    /**
     * @var $_ticketsFactory \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketsFactory;

    /**
     * @var $_sessionFactory \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * @var $_orderedTicketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_orderedTicketFactory;

    public function __construct(
        Template\Context $context,
        EventFactory $eventFactory,
        \Unirgy\Dropship\Helper\Data $helperData,
        \Unirgy\Dropship\Model\Session $session,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
        array $data = [])
    {
        $this->_attributeInterface = $attributeRepository;
        $this->_orderedTicketFactory = $ticketFactory;
        $this->_sessionFactory = $sessionFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_productCollection = $collectionFactory;
        $this->_objectManager = $objectManager;
        $this->_filterGroup = $filterGroup;
        $this->_searchCriteriaInterface = $searchCriteria;
        $this->_productRepositoryInterface = $productRepository;
        $this->productFactory = $product;
        $this->_unirgySession = $session;
        $this->_unirgyHelper = $helperData;
        $this->_eventsFactory = $eventFactory;
        parent::__construct($context, $data);
    }

    public function getEventProductCollection()
    {
        $page = $this->getRequest()->getParam('p', 1);
        //get values of current limit
        $pageSize = $this->getPageSize();
        $params = $this->getRequest()->getParams();

        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $reportTable = $resource->getTableName('report_event');
        $eventTable = $resource->getTableName('magenest_ticket_event');
        $ticketsTable = $resource->getTableName('magenest_ticket_tickets');
        $ticketTable = $resource->getTableName('magenest_ticket_ticket');
        $sessionTable = $resource->getTableName('magenest_ticket_sessions');
        $productTable = $resource->getTableName('catalog_product_entity');
        $productIntTable = $resource->getTableName('catalog_product_entity_int');
        $productVarcharTable = $resource->getTableName('catalog_product_entity_varchar');

        $sql = "SELECT
  view.*,
  count(" . $reportTable . ".event_id) AS view
FROM
  (SELECT
     old.*,
     count(ticket.event_id)   AS sold,
     sum(ticket.ticket_price) AS revenue
   FROM
     (SELECT
        `e`.entity_id,
        `at_udropship_vendor`.`value`              AS `udropship_vendor`,
        `at_campaign_status`.`value`  AS `campaign_status`,
        `" . $eventTable . "`.`event_id`,
        `" . $eventTable . "`.`event_name`,
        MIN(" . $sessionTable . ".session_from) AS `start`,
        MAX(" . $sessionTable . ".session_to)   AS `end`
      FROM `" . $productTable . "` AS `e`
        INNER JOIN `" . $productIntTable . "` AS `at_udropship_vendor`
          ON (`at_udropship_vendor`.`row_id` = `e`.`row_id`) AND (`at_udropship_vendor`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('udropship_vendor') . "') AND
             (`at_udropship_vendor`.`store_id` = 0)
        INNER JOIN `" . $productVarcharTable . "` AS `at_campaign_status`
          ON (`at_campaign_status`.`row_id` = `e`.`row_id`) AND (`at_campaign_status`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('campaign_status') . "') AND
             (`at_campaign_status`.`store_id` = 0)
        INNER JOIN `" . $eventTable . "` ON entity_id = " . $eventTable . ".product_id
        INNER JOIN `" . $ticketsTable . "` ON " . $eventTable . ".product_id = " . $ticketsTable . ".product_id
        LEFT JOIN `" . $sessionTable . "`
          ON " . $sessionTable . ".tickets_id = " . $ticketsTable . ".tickets_id
      WHERE
        (at_udropship_vendor.value = '" . $this->getVendorId() . "') AND (`e`.`type_id` = 'ticket') AND (e.created_in <= 1) AND
        (e.updated_in > 1)";

        if ($this->getFilter() !== 'all') {
            $sql .= " AND (at_campaign_status.value = '" . $this->getFilter() . "') ";
        }

        $sql .= "GROUP BY `entity_id`) AS old

     LEFT JOIN `" . $ticketTable . "` AS ticket
       ON ticket.product_id = old.entity_id
   GROUP BY entity_id) AS view
  LEFT JOIN `" . $reportTable . "` ON " . $reportTable . ".object_id = entity_id AND " . $reportTable . ".event_type_id = 1
GROUP BY entity_id
";

        if (isset($params['sort']) && isset($params['order'])) {
            if ($params['sort'] === 'view') {
                $field = '`view`';
            } else {
                $field = '`view`.`' . $params['sort'] . '`';
            }
            $sql .= " ORDER BY " . $field . " " . strtoupper($params['order']);
        }

        if ($page && $pageSize) {
            if ($page === 1) {
                $offset = 0;
            } else {
                $offset = (intval($page) - 1) * intval($pageSize);
            }
            $sql .= " LIMIT $pageSize OFFSET $offset";
        }

        $result = $connection->fetchAll($sql);

        return $result;
    }

    protected function getAttributeIdByEntityCode($code)
    {
        try {
            $result = $this->_attributeInterface->get(ProductModel::ENTITY, $code);
            return $result->getAttributeId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    public function getCampaignStatusTitle($campaignId)
    {
        return CampaignStatus::getStatusLabelByCode($campaignId);
    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        return $this->_unirgySession->getVendorId();
    }

    public function getFilter()
    {
        $status = $this->getRequest()->getParam('filter');
        if ($status === '0') {
            return $status;
        } else {
            return $status ? $status : 'all';
        }
    }

    public function getSessionDataByEventId($productId)
    {
        $result = [];
        $ticketList = $this->_ticketsFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getAllIds();
        $result['ticket_options'] = count($ticketList);
        $sessions = $this->_sessionFactory->create()->getCollection()
            ->addFieldToFilter('tickets_id', array('in' => $ticketList));
        $result['buy_options'] = $sessions->getSize();

        $result['redeemed'] = $this->getRedeemedAmount($productId);
        $result['sold'] = $result['redeemed']['total'];
        $result['revenue'] = $result['redeemed']['revenue'] ? $result['redeemed']['revenue'] : 0;
        $result['product_id'] = $productId;

        return $result;
    }

    protected function getRedeemedAmount($productId)
    {
        $ticketSold = $this->_orderedTicketFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        $result['total'] = intval($ticketSold->getSize());
        $count = 0;
        $revenue = 0;
        foreach ($ticketSold as $item) {
            $revenue += floatval($item->getTicketPrice());
            if ($item->getStatus() === '2') {
                $count++;
            }
        }
        $result['redeem'] = $count;
        $result['revenue'] = $revenue;
        if ($result['total'] !== 0) {
            $result['percent'] = number_format($count / $result['total'] * 100, 2);
        } else {
            $result['percent'] = 0;
        }
        return $result;
    }

    public function getProductImage($productId)
    {
        $image = $this->productFactory->create()->load($productId)->getImage();
        if ($image && $image !== 'no_selection') {
            return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $image;
        } else {
            return $this->getDefaultPlaceholderImageUrl();
        }
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    public function getPagerInformation()
    {
        $result = [];

        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $pageSize = $this->getPageSize();

        $reportTable = $resource->getTableName('report_event');
        $eventTable = $resource->getTableName('magenest_ticket_event');
        $ticketsTable = $resource->getTableName('magenest_ticket_tickets');
        $ticketTable = $resource->getTableName('magenest_ticket_ticket');
        $sessionTable = $resource->getTableName('magenest_ticket_sessions');
        $productTable = $resource->getTableName('catalog_product_entity');
        $productIntTable = $resource->getTableName('catalog_product_entity_int');
        $productVarcharTable = $resource->getTableName('catalog_product_entity_varchar');

        $sql = "SELECT
  view.*,
  count(" . $reportTable . ".event_id) AS view
FROM
  (SELECT
     old.*,
     count(ticket.event_id)   AS sold,
     sum(ticket.ticket_price) AS revenue
   FROM
     (SELECT
        `e`.entity_id,
        `at_udropship_vendor`.`value`              AS `udropship_vendor`,
        `at_campaign_status`.`value`  AS `campaign_status`,
        `" . $eventTable . "`.`event_id`,
        `" . $eventTable . "`.`event_name`,
        MIN(" . $sessionTable . ".session_from) AS `start`,
        MAX(" . $sessionTable . ".session_to)   AS `end`
      FROM `" . $productTable . "` AS `e`
        INNER JOIN `" . $productIntTable . "` AS `at_udropship_vendor`
          ON (`at_udropship_vendor`.`row_id` = `e`.`row_id`) AND (`at_udropship_vendor`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('udropship_vendor') . "') AND
             (`at_udropship_vendor`.`store_id` = 0)
        INNER JOIN `" . $productVarcharTable . "` AS `at_campaign_status`
          ON (`at_campaign_status`.`row_id` = `e`.`row_id`) AND (`at_campaign_status`.`attribute_id` = '" . $this->getAttributeIdByEntityCode('campaign_status') . "') AND
             (`at_campaign_status`.`store_id` = 0)
        INNER JOIN `" . $eventTable . "` ON entity_id = " . $eventTable . ".product_id
        INNER JOIN `" . $ticketsTable . "` ON " . $eventTable . ".product_id = " . $ticketsTable . ".product_id
        LEFT JOIN `" . $sessionTable . "`
          ON " . $sessionTable . ".tickets_id = " . $ticketsTable . ".tickets_id
      WHERE
        (at_udropship_vendor.value = '" . $this->getVendorId() . "') AND (`e`.`type_id` = 'ticket') AND (e.created_in <= 1) AND
        (e.updated_in > 1)";

        if ($this->getFilter() !== 'all') {
            $sql .= " AND (at_campaign_status.value = '" . $this->getFilter() . "') ";
        }

        $sql .= "GROUP BY `entity_id`) AS old

     LEFT JOIN `" . $ticketTable . "` AS ticket
       ON ticket.product_id = old.entity_id
   GROUP BY entity_id) AS view
  LEFT JOIN `" . $reportTable . "` ON " . $reportTable . ".object_id = entity_id AND " . $reportTable . ".event_type_id = 1
GROUP BY entity_id
";

        $datas = $connection->fetchAll($sql);
        $result['total_record'] = count($datas);
        $result['max_num_page'] = ceil(count($datas) / intval($pageSize));

        return $result;
    }

    public function getPageSize()
    {
        return $this->getRequest()->getParam('limit', 20);
    }

    public function getCampaignStatus($productId)
    {
        $campaignStatus = $this->productFactory->create()->load($productId)->getCampaignStatus();

        return $this->getCampaignStatusTitle($campaignStatus);
    }

    public function getCounterCampaignStatus()
    {
        $result = [];
        $result[CampaignStatus::STATUS_REJECTED] = 0;
        $result[CampaignStatus::STATUS_DRAFT] = 0;
        $result[CampaignStatus::STATUS_PENDING_REVIEW] = 0;
        $result[CampaignStatus::STATUS_SCHEDULED] = 0;
        $result[CampaignStatus::STATUS_LIVE] = 0;
        $result[CampaignStatus::STATUS_ACTIVE] = 0;
        $result[CampaignStatus::STATUS_EXPIRED] = 0;
        $result[CampaignStatus::STATUS_SOLD_OUT] = 0;
        $result[CampaignStatus::STATUS_PAUSED] = 0;
        $result[CampaignStatus::STATUS_APPROVED] = 0;
        $result['all'] = 0;

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productFactory->create()->getCollection();
        $collection->addAttributeToSelect(['campaign_status'])
            ->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->setFlag('has_stock_status_filter', 1);
        foreach($collection as $item) {
            $campaignStatus = (int)$item->getCampaignStatus();
            $result[$campaignStatus]++;
            $result['all']++;
        }

        return $result;
    }
}
