<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Vendor;

use Magento\Framework\View\Element\Template;

/**
 * Class Report
 * @package Magenest\Ticket\Block\Vendor
 */
class Report extends Template
{
    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketInfoFactory;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_ticketSessionFactory;

    /**
     * @var \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * Ticket constructor.
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory
     * @param \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory,
        \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        Template\Context $context,
        array $data = []
    ) {
        $this->_eventFactory = $eventFactory;
        $this->_ticketFactory = $ticketFactory;
        $this->_ticketSessionFactory = $ticketSessionFactory;
        $this->_ticketInfoFactory = $ticketInfoFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getAttendeesData()
    {
        $sessions = $this->getSessions();
        $data = [];
        foreach($sessions as $session) {
            $ticketInfo = $this->_ticketInfoFactory->create()->load($session['tickets_id']);
            if ($session['salePrice'] === null) {
                $sessionPrice = $session['orig_value'];
            } else {
                $sessionPrice = $session['salePrice'];
            }
            array_push($data, [
                'id' => $session['id'],
                'title' => $ticketInfo->getTitle(),
                'qty' => $session['qty'] + 0,
                'price' => $sessionPrice,
                'available' => $session['available'],
                'purchased' => $session['purchased'],
                'revenue' => $session['purchased'] * $session['salePrice']
            ]);
        }

        return $data;
    }

    /**
     * @return array
     */
    function getSessions()
    {
        $productId = $this->getRequest()->getParam("id");
        $sessions = $this->_ticketSessionFactory->create()->getCollection()->addFieldToFilter("product_id", $productId);
        $array = [];
        foreach($sessions as $session) {
            array_push($array, [
                'id' => $session->getSessionId(),
                'tickets_id' => $session->getTicketsId(),
                'session_start' => $this->convertTime($session->getSessionFrom()),
                'session_end' => $this->convertTime($session->getSessionTo()),
                'sale_start' => $this->convertTime($session->getSellFrom()),
                'sale_end' => $this->convertTime($session->getSellTo()),
                'qty' => $session->getQty(),
                'salePrice' => $session->getSalePrice(),
                'orig_value' => $session->getOriginalValue(),
                'available' => $session->getQtyAvailable(),
                'purchased' => $session->getQtyPurchased(),
                'max_qty' => $session->getMaxQty(),
                'is_hide' => $session->getHideSoldOut(),
            ]);
        }

        return $array;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);

        return date('d-m-Y H:i:s', $strTime);
    }

    /**
     * @return array
     */
    public function getSummaryData()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productId = $this->getRequest()->getParam("id");
        $event = $this->_eventFactory->create()->loadByProductId($productId);
        $tickets = $this->_ticketFactory->create()->getCollection()->addFieldToFilter("event_id", $event->getEventId());
        $data = [];
        foreach($tickets as $ticket) {
            $ticketInfo = unserialize($ticket->getInformation());
            $session = $this->_ticketSessionFactory->create()->load($ticketInfo['session_id']);
            $order = $objectManager->create('\Magento\Sales\Model\Order')
                ->load($ticket->getOrderId());
            $orderItem = $order->getItemById($ticket->getOrderItemId());
            array_push($data, [
                'id' => $ticket->getTicketId(),
                'code' => $ticket->getCode(),
                'qty' => 1,
                'name' => $ticket->getCustomerName(),
                'email' => $ticket->getCustomerEmail(),
                'order' => $ticket->getOrderIncrementId(),
                'information' => "From: " . $session->getSessionFrom() . "\nTo: " . $session->getSessionTo(),
                'created_at' => $ticket->getCreatedAt(),
                'status' => $ticket->getStatus() == 1 ? 'Not Redeem' : 'Redeem'
            ]);
        }

        return $data;
    }

    /**
     * @return array
     */
    function getTicketData()
    {
        $tickets = $this->getTicketInfo();
        $data = [];
        if ($tickets)
            foreach($tickets as $ticket) {
                array_push($data, [
                    'id' => $ticket->getTicketsId(),
                    'type' => $this->convertTypeName($ticket->getType()),
                    'title' => $ticket->getTitle(),
                    'description' => $ticket->getDescription(),
                    'sessions' => $this->getSessions($ticket->getTicketsId())
                ]);
            }

        return $data;
    }

    /**
     * @return $this|null
     */
    function getTicketInfo()
    {
        $productId = $this->getRequest()->getParam("id");
        if ($productId) {
            return $this->_ticketInfoFactory->create()->getCollection()->addFieldToFilter("product_id", $productId);
        }

        return null;
    }

    /**
     * @param $type
     * @return string
     */
    function convertTypeName($type)
    {
        if ($type == "paid")
            return "Add Paid Ticket";
        elseif ($type == "free")
            return "Add Free Ticket";
        return "Add Donation Ticket";
    }
}
