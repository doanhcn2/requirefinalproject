<?php
/**
 * Created by PhpStorm.
 * User: thanhnam95
 * Date: 26/04/2018
 * Time: 11:11
 */

namespace Magenest\Ticket\Block\Vendor;

use \Magento\Framework\View\Element\Template;
use \Magenest\Ticket\Model\EventFactory;
use \Magenest\Ticket\Model\TicketFactory;
use \Magenest\Ticket\Model\TicketsFactory;
use \Magenest\Ticket\Model\SessionFactory;
use \Magenest\Ticket\Helper\Session as SessionHelper;
use \Magento\Sales\Model\OrderFactory;

abstract class Campaign extends Template
{

    protected $_eventFactory;
    protected $_ticketPurchasedFactory;
    protected $_ticketsFactory;
    protected $_sessionFactory;
    protected $_orderFactory;
    protected $_sessionStatus;
    protected $_sessionHelper;

    public function __construct(
        EventFactory $eventFactory,
        TicketFactory $ticketPurchasedFactory,
        TicketsFactory $ticketsFactory,
        SessionFactory $sessionFactory,
        OrderFactory $orderFactory,
        SessionHelper $sessionHelper,
        Template\Context $context,
        array $data = []
    )
    {
        $this->_sessionHelper = $sessionHelper;
        $this->_orderFactory = $orderFactory;
        $this->_eventFactory = $eventFactory;
        $this->_ticketsFactory = $ticketsFactory;
        $this->_ticketPurchasedFactory = $ticketPurchasedFactory;
        $this->_sessionFactory = $sessionFactory;
        parent::__construct($context, $data);
    }

    public function getProductId()
    {
        return $this->getRequest()->getParam("id");
    }

    public function getEvent()
    {
        $productId = $this->getProductId();
        return $this->_eventFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)->getFirstItem();
    }

    public function getEventType()
    {
        return $this->getEvent()->getTicketType();
    }

    public function getOptionsColletion()
    {
        return $this->_ticketsFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $this->getProductId());
    }

    public function getPageSizeSession()
    {
        return 20;
    }

    public function getSessionsCollection($sessionId = null)
    {
        $params = $this->getRequest()->getParams();
        /** @var \Magenest\Ticket\Model\ResourceModel\Session\Collection $collection */
        $collection = $this->_sessionFactory->create()->getCollection();
        if ($sessionId) {
            $collection->addFieldToFilter('main_table.session_id', ['in' => $this->getRelatedSession($sessionId)]);
        }
        if($this->getEventType()=='free')
            $revenue = "`main_table`.`qty_purchased`*0";
        else
            $revenue ="`main_table`.`sale_price`*`main_table`.`qty_purchased`";
        $collection->join(
            $collection->getTable('magenest_ticket_sessions'),
            "`main_table`.`session_id`=`magenest_ticket_sessions`.`session_id`
            AND `main_table`.`product_id` = ".$params['id'],
            "(".$revenue.") AS revenueOld"
        );
        $page = @$params['p'] ? $params['p'] : 1;
        $pageSize = $this->getPageSizeSession();

        if (isset($params['sort'], $params['order'])) {
            $collection->setOrder($params['sort'], strtoupper($params['order']));
        }
        $collection->setPageSize($pageSize)->setCurPage($page);
        return $collection;
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getCurrencySymbol()
    {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
        $currencyCode = $storeManager->getStore()->getCurrentCurrencyCode();
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Directory\Model\CurrencyFactory')->create()
            ->load($currencyCode)->getCurrencySymbol();
    }

    public function getSessionId()
    {
        return $this->getRequest()->getParam('session_id');
    }

    public function getRelatedSession($sessionId)
    {
        $productId = $this->getProductId();
        $tickets = $this->_ticketsFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->getAllIds();
        $result = [];

        $currentSession = $this->_sessionFactory->create()->load($sessionId);
        $neededSessionStart = $currentSession->getSessionFrom();
        $neededSessionEnd = $currentSession->getSessionTo();

        $allSession = $this->_sessionFactory->create()->getCollection()->addFieldToFilter('tickets_id', ['in' => $tickets]);
        foreach ($allSession as $item) {
            if (abs(strtotime($neededSessionStart) - strtotime($item->getSessionFrom())) <= 60 && abs(strtotime($neededSessionEnd) - strtotime($item->getSessionTo())) <= 60) {
                $result[] = $item->getSessionId();
            }
        }

        return $result;
    }

}