<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Vendor;

use Magento\Framework\View\Element\Template;

/**
 * Class Ticket
 * @package Magenest\Ticket\Block\Vendor
 */
class Ticket extends Template
{
    /**
     * @var \Magenest\Ticket\Model\TicketInfoFactory
     */
    protected $_ticketInfoFactory;

    /**
     * @var \Magenest\Ticket\Model\TicketSessionFactory
     */
    protected $_ticketSessionFactory;

    /**
     * Ticket constructor.
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory
     * @param \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory,
        \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory,
        Template\Context $context,
        array $data = []
    ) {
        $this->_ticketSessionFactory = $ticketSessionFactory;
        $this->_ticketInfoFactory = $ticketInfoFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return $this|null
     */
    function getTicketInfo(){
        $productId = $this->getRequest()->getParam("id");
        if($productId){
            return $this->_ticketInfoFactory->create()->getCollection()->addFieldToFilter("product_id",$productId);
        }

        return null;
    }

    /**
     * @param $ticketsId
     * @return array
     */
    function getSessions($ticketsId)
    {
        $sessions = $this->_ticketSessionFactory->create()->getCollection()->addFieldToFilter("tickets_id",$ticketsId);
        $array=[];
        foreach ($sessions as $session) {
            array_push($array,[
                'id'=>$session->getSessionId(),
                'session_start'=>$this->convertTime($session->getSessionFrom()),
                'session_end'=>$this->convertTime($session->getSessionTo()),
                'sale_start'=>$this->convertTime($session->getSellFrom()),
                'sale_end'=>$this->convertTime($session->getSellTo()),
                'qty'=>$session->getQty(),
                'salePrice'=>$session->getSalePrice()?$session->getSalePrice()+0:$session->getSalePrice(),
                'orig_value'=>$session->getOriginalValue()?$session->getOriginalValue()+0:$session->getOriginalValue(),
                'max_qty'=>$session->getMaxQty(),
                'is_hide'=>$session->getHideSoldOut(),
                'session_map_id' => $session->getSessionMapId()
            ]);
        }

        return $array;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);

        return date('d-m-Y H:i:s', $strTime);
    }

    /**
     * @return array
     */
    function getTicketData()
    {
        $tickets = $this->getTicketInfo();
        $data = [];
        if($tickets)
        foreach ($tickets as $ticket) {
            array_push($data,[
                'id'=> $ticket->getTicketsId(),
                'type'=>$this->convertTypeName($ticket->getType()),
                'title'=>$ticket->getTitle(),
                'description'=>addslashes($ticket->getDescription()),
                'sessions'=>$this->getSessions($ticket->getTicketsId())
            ]);
        }

        return $data;
    }

    /**
     * @param $type
     * @return string
     */
    function convertTypeName($type)
    {
        if($type == "paid")
            return "Add Paid Ticket";
        elseif ($type == "free")
            return "Add Free Ticket";
        return "Add Donation Ticket";
    }
}
