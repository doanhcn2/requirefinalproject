<?php
/**
 * Created by PhpStorm.
 * User: thang
 * Date: 19/06/2018
 * Time: 15:45
 */

namespace Magenest\Ticket\Block\Order;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PdfTicket
 * @package Magenest\Ticket\Block\Order
 */
class PdfTicket extends Template
{
    public function getTicketPath()
    {
        $ticketId = (int)$this->getRequest()->getParam('ticket_id');
        $directoryPath = DirectoryList::VAR_DIR . '/pdf/';
        $ticketName = 'Ticket_' . $ticketId . '.pdf';
        return $directoryPath . $ticketName;
    }
}
