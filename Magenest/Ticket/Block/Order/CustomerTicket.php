<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Block\Order;

use Magenest\Ticket\Model\Ticket;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Magenest\Ticket\Model\TicketFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Framework\App\ResourceConnection;


/**
 * Class CustomerTicket
 * @package Magenest\Ticket\Block\Order
 */
class CustomerTicket extends Template
{

    /**
     * @var string
     */
    protected $_template = 'order/customer/ticket.phtml';

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var AbstractProduct
     */
    protected $_absProduct;

    /**
     * @var
     */
    protected $_locationFactory;

    /**
     * @var array
     */
    protected $_productLoaded = [];

    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_vendorHelper;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_catalogProductTypeConfigurable;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderReposity;

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * CustomerTicket constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param TicketFactory $ticketFactory
     * @param ProductFactory $productFactory
     * @param AbstractProduct $abstractProduct
     * @param \Unirgy\Dropship\Helper\Data $vendorHelper
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param ResourceConnection $resourceConnection
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        TicketFactory $ticketFactory,
        ProductFactory $productFactory,
        AbstractProduct $abstractProduct,
        \Unirgy\Dropship\Helper\Data $vendorHelper,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        ResourceConnection $resourceConnection,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        array $data = []
    ) {
        $this->_sessionFactory = $sessionFactory;
        $this->_orderReposity = $orderRepository;
        $this->_vendorHelper = $vendorHelper;
        $this->_catalogProductTypeConfigurable = $configurable;
        $this->_absProduct = $abstractProduct;
        $this->_productFactory = $productFactory;
        $this->_ticketFactory = $ticketFactory;
        $this->_customerSession = $customerSession;
        $this->_resource = $resourceConnection;
        parent::__construct($context, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Tickets'));
    }

    /**
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    /**
     * @return int|mixed
     */
    public function getStatus()
    {

        $status = $this->getRequest()->getParam('status');

        if ($status == 'available') {
            $status = 1;
        } elseif ($status == 'used') {
            $status = 2;
        } elseif ($status == 'expired') {
            $status = 5;
        } elseif ($status == 'all') {
            $status = 0;
        } elseif ($status == 'expired_soon') {
            $status = 4;
        } elseif ($status == 'gifted') {
            $status = 10;
        }

        return $status;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->getRequest()->getParam('filter');
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     * @throws \Exception
     */
    public function getProductIdsCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $productIds = $this->getTicket()->getColumnValues('product_id');
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productFactory->create()->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
        if ($pageSize && $page) {
            $collection->setPageSize($pageSize)->setCurPage($page);
        }
        return $collection;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     * @throws \Exception
     */
    public function getTicket()
    {
        $status = $this->getStatus();
        $customerId = $this->getCustomerId();
        if (!$status || $status == 0) {
            /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('main_table.status', ['neq' => Ticket::STATUS_CANCELED]);
        } elseif ($status == 4) {
            $daystart = new \DateTime();
            $today = $daystart->format('Y-m-d h:m:s');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d h:m:s');
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('main_table.status', 1);
            $session = $this->_resource->getTableName('magenest_ticket_sessions');
            $collection->getSelect()
                ->joinLeft(['mts' => $session],
                    "main_table.session_id = mts.session_id",
                    ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
                )->where('mts.session_to >= "' . $today . '" AND mts.session_to <= "' . $dateText . '"');

        } elseif ($status == 2) {
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter(['main_table.customer_status', 'main_table.status'], [Ticket::CUSTOMER_STATUS_USED, Ticket::STATUS_USED])
                ->addFieldToFilter('customer_id', $customerId);
        } elseif ($status == 10) {
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter('main_table.status', ['neq' => Ticket::STATUS_CANCELED])
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('recipient', ['notnull' => true]);
        } elseif ($status != 4) {
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => Ticket::CUSTOMER_STATUS_USED], ['null' => true]])
                ->addFieldToFilter('main_table.status', ['neq' => Ticket::STATUS_CANCELED])
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('main_table.status', $status);
        }

        $keyword = $this->getKeyWord();
        if ($keyword != '') {
            $collection->addFieldToFilter(['title', 'code'], [['like' => '%' . $keyword . '%'], ['like' => '%' . $keyword . '%']]);
        }

        return $collection;
    }

    /**
     * @param $status
     * @return int
     * @throws \Exception
     */
    public function getNumberOfStatusTicket($status)
    {
        if ($status == 2) {
            return $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter(['main_table.customer_status', 'main_table.status'], [Ticket::CUSTOMER_STATUS_USED, Ticket::STATUS_USED])
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->getSize();
        } elseif ($status === 10) {
            return $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter('main_table.status', ['neq' => Ticket::STATUS_CANCELED])
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('recipient', ['notnull' => true])->getSize();
        } elseif ($status != 4) {
            return $this->_ticketFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter(['main_table.customer_status', 'main_table.customer_status'], [['neq' => Ticket::CUSTOMER_STATUS_USED], ['null' => true]])
                ->addFieldToFilter('status', $status)->getSize();
        }
        if ($status == 4) {
            $daystart = new \DateTime();
            $today = $daystart->format('Y-m-d h:m:s');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d h:m:s');
            $collection = $this->_ticketFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('main_table.status', 1);
            $session = $this->_resource->getTableName('magenest_ticket_sessions');
            $collection->getSelect()
                ->joinLeft(['mts' => $session],
                    "main_table.session_id = mts.session_id",
                    ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
                )->where('mts.session_to >= "' . $today . '" AND mts.session_to <= "' . $dateText . '"');
            return $collection->getSize();
        }
        return 0;
    }

    /**
     * @param $orderId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrder($orderId)
    {
        return $this->_orderReposity->get($orderId);
    }

    /**
     * @param $ticketCollection
     * @return array
     */
    public function getDataByProduct($ticketCollection)
    {
        $resultData = [];
        foreach($ticketCollection as $ticket) {
            $productId = $ticket->getData('product_id');
            if (!isset($resultData[$productId])) {
                $resultData[$productId] = [];
            }
            $resultData[$productId][] = $ticket;
        }
        return $resultData;
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getVendorName($product)
    {
        $vendor = $this->_vendorHelper->getVendor($product);
        return $vendor->getData('vendor_name');
    }

    /**
     * @param $ticketId
     * @return mixed
     */
    public function getOrderId($ticketId)
    {
        $model = $this->_ticketFactory->create()->load($ticketId, 'ticket_id');

        return $model->getOrderId();
    }

    /**
     * @param $product
     * @param $param
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getProductImage($product, $param)
    {
        $img = $this->_absProduct->getImage($product, $param);

        return $img;
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getItems($productId)
    {
        $product = $this->_productFactory->create()->load($productId);

        return $product;
    }

    /**
     * @param $ticketId
     * @return string
     */
    public function printUrl($ticketId)
    {
        $url = $this->getUrl('ticket/order/pdfticket', ['ticket_id' => $ticketId]);

        return $url;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getProductIdsCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'fme.news.pager'
            )->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
                $this->getProductIdsCollection()
            );
            $this->setChild('pager', $pager);
            $this->getProductIdsCollection()->load();
        }

        return $this;
    }


    /**
     * @param $locationId
     * @return string
     */
    public function getLocationInfo($locationId)
    {
        $location = $this->_locationFactory->create()->load($locationId);
        $result = $location->getData('street') . ' ' . $location->getData('city') . ' ' . $location->getData('country') . ' ' . $location->getData('phone');
        return $result;
    }

    /**
     * @param $status
     * @return string
     */
    public function getStatusTicketUrl($status)
    {
        if ($status == 1) {
            $status = 'available';
        } elseif ($status == 2) {
            $status = 'used';
        } elseif ($status == 5) {
            $status = 'expired';
        } elseif ($status == 0) {
            $status = 'all';
        } elseif ($status == 4) {
            $status = 'expired_soon';
        } elseif ($status == 10) {
            $status = 'gifted';
        }

        return $this->getUrl('ticket/order/history') . '?' . http_build_query(['status' => $status]);
    }

    /**
     * @param $sortOrder
     * @return string
     */
    public function getFilterUrl($sortOrder)
    {
        $status = $this->getRequest()->getParam('status');
        $params = [];
        if ($status != '') {
            $param['status'] = $status;
        }
        $params['filter'] = $sortOrder;
        return $this->getUrl('ticket/order/history') . '?' . http_build_query($params);
    }

    /**
     * @param $filter
     * @return string
     */
    public function filterSelected($filter)
    {
        if ($filter == $this->getFilter()) return 'selected';
        return '';
    }

    /**
     * @return mixed
     */
    private function getKeyWord()
    {
        return $this->getRequest()->getParam('key_word');
    }

    /**
     * @return mixed
     */
    public function getCurrentTime()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        return $objDate->date()->format('Y-m-d H:i:s');
    }

    /**
     * @param $sessionId
     * @return mixed
     */
    public function getTicketSessionBySessionId($sessionId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $ticketSession = $objectManager->get('Magenest\Ticket\Model\Session')->load($sessionId);
        return $ticketSession;
    }

    public function getBaseMediaUrl()
    {
        return $this->_storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getDefaultPlaceholderImageUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $placeholderImageUrl = $this->_storeManager->getStore()->getConfig('catalog/placeholder/thumbnail_placeholder');
        if ($placeholderImageUrl !== null) {
            $imageUrl = $mediaUrl . 'catalog/product/placeholder/' . $placeholderImageUrl;
        } else {
            $imageUrl = '';
        }

        return $imageUrl;
    }

    public function isRedeemable($status, $sessionId, $customerStatus)
    {
        if ($status == Ticket::STATUS_AVAILABLE && $customerStatus !== Ticket::CUSTOMER_STATUS_USED) {
            return true;
        }

        return false;
    }

    public function getDisplayTextNonAvailable($status, $customerStatus)
    {
        if ($customerStatus === Ticket::CUSTOMER_STATUS_USED) {
            return 'Used';
        }
        if ($status === Ticket::STATUS_USED) {
            return 'Used';
        } elseif ($status === Ticket::STATUS_EXPIRED) {
            return 'Expired';
        } elseif ($status === Ticket::STATUS_REFUNDED) {
            return 'Refunded';
        }
        return 'Used';
    }

    public function getSessionTime($sessionId) {
        if ($sessionId) {
            $session = $this->_sessionFactory->create()->load($sessionId);
            $sessionStart = $session->getSessionFrom();
            $sessionTo = $session->getSessionTo();
            return $this->formatDisplaySession($sessionStart) . ' - ' . $this->formatDisplaySession($sessionTo);
        }
        return '';
    }

    public function formatDisplaySession($time) {
        return __(date("D, M d, Y %1 h:m A", strtotime($time)), "at");
    }
}
