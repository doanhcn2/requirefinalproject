<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Order;

use Magenest\Ticket\Model\Ticket;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Ticket\Model\TicketFactory as TicketCollectionFactory;
use Magenest\Ticket\Helper\Information;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\ResourceConnection;

/**
 * Class History
 * @package Magenest\Ticket\Block\Order
 */
class History extends Template
{
    /**
     * Google Map API key
     */
    const XML_PATH_GOOGLE_MAP_API_KEY = 'event_ticket/general_config/google_api_key';

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/history.phtml';

    /**
     * @var TicketCollectionFactory
     */
    protected $_ticketCollectionFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var string
     */
    protected $tickets;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $_locationFactory;

    protected $_resource;

    /**
     * History constructor.
     * @param Context $context
     * @param TicketCollectionFactory $ticketCollectionFactory
     * @param CustomerSession $customerSession
     * @param \Magenest\Ticket\Model\EventLocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     * @param Information $information
     * @param array $data
     */
    public function __construct(
        Context $context,
        TicketCollectionFactory $ticketCollectionFactory,
        CustomerSession $customerSession,
        \Magenest\Ticket\Model\EventLocationFactory $locationFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        Information $information,
        ResourceConnection $resourceConnection,
        array $data = []
    ) {
        $this->_sessionFactory = $sessionFactory;
        $this->_locationFactory = $locationFactory;
        $this->information = $information;
        $this->_ticketCollectionFactory = $ticketCollectionFactory;
        $this->_customerSession = $customerSession;
        $this->_resource = $resourceConnection;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Tickets'));
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);
        return date('d/m/Y H:i:s', $strTime);
    }

    /**
     * @param $id
     * @return null
     */
    public function getTicketInfo($id){
        $ticket = $this->_ticketCollectionFactory->create()->load($id);
        $ticketInfo = unserialize($ticket->getInformation());
        $session = $this->_sessionFactory->create()->load($ticketInfo['session_id']);
        $data['date'] = $this->convertTime($session->getSessionFrom())." - ".$this->convertTime($session->getSessionTo());
        if($ticket){
            $location = $this->_locationFactory->create()->getCollection()->addFieldToFilter('event_id',$ticket->getEventId());
            if($location->count()>0){
                $location = $location->getFirstItem();
                $data['location']= $location->getVenueTitle().", ".$location->getAddress();
                $data['longitude']=$location->getLongitude();
                $data['latitude']=$location->getLatitude();
            }else{
                $data['location']= "online";
            }

            return $data;
        }

        return null;
    }
    /**
     * Get Ticket Collection
     *
     * @return bool|\Magenest\Ticket\Model\ResourceModel\Ticket\Collection
     */
    public function getTickets()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->tickets) {
            $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
            //get values of current limit
            $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
            $status = $this->getStatus();

            if (!$status || $status == 0) {
                $collection = $this->_ticketCollectionFactory->create()->getCollection()->addFieldToSelect(
                    '*'
                )->addFieldToFilter('main_table.status', ['neq' => Ticket::STATUS_CANCELED]);
            } elseif ($status == 4) {
                $daystart = new \DateTime();
                $today = $daystart->format('Y-m-d h:m:s');
                $daystart->add(new \DateInterval('P7D'));
                $dateText = $daystart->format('Y-m-d h:m:s');
                $collection = $this->_ticketCollectionFactory->create()
                    ->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('customer_id', $this->getCustomerId())
                    ->addFieldToFilter('main_table.status', 1);
                $session = $this->_resource->getTableName('magenest_ticket_sessions');
                $collection->getSelect()
                    ->joinLeft( ['mts' => $session],
                        "main_table.session_id = mts.session_id",
                        ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
                    )->where('mts.session_to >= "' .$today.'" AND mts.session_to <= "'.$dateText.'"');
            } elseif ($status==6){
                $daystart = new \DateTime();
                $today = $daystart->format('Y-m-d h:m:s');
                $collection = $this->_ticketCollectionFactory->create()
                    ->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('customer_id', $this->getCustomerId())
                    ->addFieldToFilter('main_table.status', 1);
                $session = $this->_resource->getTableName('magenest_ticket_sessions');
                $collection->getSelect()
                    ->joinLeft( ['mts' => $session],
                        "main_table.session_id = mts.session_id",
                        ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
                    )->where('mts.session_to < "' .$today.'"');
            } else {
                $collection = $this->_ticketCollectionFactory->create()->getCollection()->addFieldToSelect(
                    '*'
                )->addFieldToFilter('main_table.status', ['eq' => $status]);
            }

            $collection->setOrder(
                    'main_table.event_id',
                    'desc'
                )->setOrder(
                    'main_table.created_at',
                    'desc'
                );
            $collection->getSelect()->joinLeft(
                ['event' => 'magenest_ticket_event'],
                'main_table.event_id = event.event_id ',
                ['*']
            );
            return $collection;
        }
        return false;

    }

    public function getStatusTicketUrl($status)
    {
        if ($status == 1) {
            $status = 'available';
        } elseif ($status == 2) {
            $status = 'used';
        } elseif ($status == 6) {
            $status = 'expired';
        } elseif ($status == 0) {
            $status = 'all';
        } elseif ($status == 4) {
            $status = 'expired_soon';
        } elseif ($status == 5) {
            $status = 'gifted';
        }
        return $this->getUrl('ticket/order/history').'?'.http_build_query(['status' => $status]);
    }

    /**
     * @return int|mixed
     */
    public function getStatus()
    {

        $status = $this->getRequest()->getParam('status');

        if ($status == 'available') {
            $status = 1;
        } elseif ($status == 'used') {
            $status = 2;
        } elseif ($status == 'expired') {
            $status = 3;
        } elseif ($status == 'all') {
            $status = 0;
        } elseif ($status == 'expired_soon') {
            $status = 4;
        } elseif ($status == 'gifted') {
            $status = 5;
        }

        return $status;
    }


    public function getNumberOfStatusTicket($status)
    {
        if($status!=4&&$status!=5&&$status!=6){
            return $this->_ticketCollectionFactory->create()->getCollection()
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('status', $status)->getSize();
        }
        if($status==4){
            $daystart = new \DateTime();
            $today = $daystart->format('Y-m-d h:m:s');
            $daystart->add(new \DateInterval('P7D'));
            $dateText = $daystart->format('Y-m-d h:m:s');
            $collection = $this->_ticketCollectionFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('main_table.status', 1);
            $session = $this->_resource->getTableName('magenest_ticket_sessions');
            $collection->getSelect()
            ->joinLeft( ['mts' => $session],
                "main_table.session_id = mts.session_id",
                ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
            )->where('mts.session_to >= "' .$today.'" AND mts.session_to <= "'.$dateText.'"');
            return $collection->getSize();
        }
        if($status==6){
            $daystart = new \DateTime();
            $today = $daystart->format('Y-m-d h:m:s');
            $collection = $this->_ticketCollectionFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', $this->getCustomerId())
                ->addFieldToFilter('main_table.status', 1);
            $session = $this->_resource->getTableName('magenest_ticket_sessions');
            $collection->getSelect()
                ->joinLeft( ['mts' => $session],
                    "main_table.session_id = mts.session_id",
                    ['mts_session_from' => 'mts.session_from', 'mts_session_to' => 'mts.session_to']
                )->where('mts.session_to < "' .$today.'"');
            return $collection->getSize();
        }
        if ($status == 5) {
            //Waiting for gifted function to implemented
            return 0;
        }
        return 0;
    }

    /**
     * Prepare layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getTickets()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'ticket.order.history.pager'
            )->setCollection(
                $this->getTickets()
            );
            $this->setChild('pager', $pager);
            $this->getTickets()->load();
        }

        return $this;
    }

    public function filterSelected($filter){
        if($filter == $this->getFilter()) return 'selected';
        return '';
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->getRequest()->getParam('filter');
    }

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    /**
     * Get Google Map Api key
     *
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_API_KEY);
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param object $ticket
     * @return string
     */
    public function getViewUrl($ticket)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $ticket->getOrderId()]);
    }

    /**
     * @param object $ticket
     * @return string
     */
    public function getPrintTicketUrl($ticket)
    {
        return $this->getUrl('ticket/order/pdfticket', ['ticket_id' => $ticket->getId()]);
    }

    /**
     * @param $option
     * @return array
     */
    public function getDataTicket($option)
    {
        $array = unserialize($option);
        $data = $this->information->getDataTicket($array);

        return $data;
    }
}
