<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Adminhtml\Event\Edit\Tab;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Widget\Grid\Extended;

/**
 * Class Attendees
 *
 * @package Magenest\Ticket\Block\Adminhtml\Event\Edit\Tab
 */
class Summary extends Extended
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;

    /**
     * Summary constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_sessionFactory = $sessionFactory;
        parent::__construct($context, $backendHelper, $data);
    }


    /**
     * Prepare collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $id = $product->getId();
        $collection = $this->_sessionFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            'magenest_ticket_tickets',
            'main_table.tickets_id = magenest_ticket_tickets.tickets_id',
            'title'
        )->where(
            'main_table.product_id = ?',
            $id
        );
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Get Totals
     *
     * @return \Magento\Framework\DataObject
     */
    public function getTotals()
    {
        $this->_varTotals = new DataObject;
        $fields = [
            'revenue' => 0,
            'available_qty' => 0,
            'purcharsed_qty' => 0,
            'qty' => 0,
        ];
        foreach ($this->getCollection() as $item) {
            foreach ($fields as $field => $value) {
                $fields[$field] += $item->getData($field);
            }
        }
        $fields['title']='Totals';
        $this->_varTotals->setData($fields);
        return $this->_varTotals;
    }

    /**
     * Prepare Columns
     *
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'session_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'session_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('title', ['header' => __('Title'), 'index' => 'title']);
        $this->addColumn('qty', ['header' => __('Qty'), 'index' => 'qty']);
        $this->addColumn('qty_available', ['header' => __('Available'), 'index' => 'qty_available']);
        $this->addColumn('qty_purchased', ['header' => __('Purchased'), 'index' => 'qty_purchased']);

        $this->addColumn(
            'revenue',
            [
                'header' => __('Revenue'),
                'index' => 'revenue',
                'renderer' => 'Magenest\Ticket\Block\Adminhtml\Event\Renderer\Revenue'
            ]);
        $this->addExportType('magenest_ticket/Event/exportAttendeesCsv', __('CSV'));
        return parent::_prepareColumns();
    }
}
