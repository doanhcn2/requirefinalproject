<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Adminhtml\Event\Renderer;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

/**
 * Class Date
 * @package Magenest\Ticket\Block\Adminhtml\Event\Renderer
 */
class Date extends AbstractRenderer
{
    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketFactory
     */
    protected $ticket;

    /**
     * Date constructor.
     * @param \Magento\Backend\Block\Context $context
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Magenest\Ticket\Model\SessionFactory $eventSessionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\SessionFactory $eventSessionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->ticket = $ticketFactory;
        $this->session = $eventSessionFactory;
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $result = '';
        if ($value) {
            $data = unserialize($value);
            if (isset($data['title']) && $data['title'] != null) {
                $result .= '<div>';
                $result .= '<span>Option : '.$data['title'].'</span><br>';
                $result .= '</div>';
            }
            if (isset($data['session']) && $data['session'] != null) {
                $result .= '<div>';
                $result .= '<span> Session : '.$data['session'].'</span>';
                $result .= '</div>';
            }
        }

        return $result;
    }
}
