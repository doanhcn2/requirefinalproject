<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Ticket\Block\Adminhtml\Event\Renderer;

use Magento\Framework\DataObject;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;

/**
 * Class Revenue
 * @package Magenest\Ticket\Block\Adminhtml\Event\Renderer
 */
class Revenue extends AbstractRenderer
{
    /**
     * @var \Magenest\Ticket\Model\EventSessionFactory
     */
    protected $session;

    /**
     * @var \Magenest\Ticket\Model\TicketFactory
     */
    protected $ticket;

    /**
     * Revenue constructor.
     * @param \Magento\Backend\Block\Context $context
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Magenest\Ticket\Model\SessionFactory $eventSessionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\SessionFactory $eventSessionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->ticket = $ticketFactory;
        $this->session = $eventSessionFactory;
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $purchased = $row->getQtyPurchased();
        $price = $row->getSalePrice();
        return $price*$purchased;
    }
}
