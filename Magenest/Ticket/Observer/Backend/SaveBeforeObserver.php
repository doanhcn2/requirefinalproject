<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magento\Framework\Exception\StateException;
use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;

/**
 * Class EventTicketObserver
 *
 * @method Observer getProduct()
 */
class SaveBeforeObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;


    /**
     * @var LoggerInterface
     */
    protected $_logger;


    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var TicketsFactory
     */
    protected $_ticketsFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * SaveBeforeObserver constructor.
     * @param RequestInterface $request
     * @param StoreManagerInterface $storeManagerInterface
     * @param SessionFactory $eventSessionFactory
     * @param TicketsFactory $ticketsFactory
     * @param ManagerInterface $messageManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManagerInterface,
        SessionFactory $eventSessionFactory,
        TicketsFactory $ticketsFactory,
        ManagerInterface $messageManager,
        LoggerInterface $logger
    )
    {
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_ticketsFactory = $ticketsFactory;
        $this->messageManager = $messageManager;
        $this->session = $eventSessionFactory;
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     * @throws StateException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $params = $this->_request->getParams();

        if ($product->getTypeId() != "ticket")
            return;
        $ticketType = array_values(
            array_intersect_key($params['event'],
                array_flip(
                    array_filter(array_keys($params['event']),
                        function ($a) {
                            return substr($a, 0, 10) == "event_add_";
                        }
                    )
                )
            )
        );
        if (count($ticketType) > 1) {
            throw new LocalizedException(__("Only save one type ticket."));
        }
        if ($params['product']['campaign_status'] == -1) {
            $latestSessionEnd = null;
            $earliestSessionStart = null;
            $earliestSaleStart = null;
            if (@$params['approval'] && $params['approval'] === 'approved') {
                $tickets = @$params['event']['ticket_info'];
                foreach($tickets as &$ticket) {
                    foreach ($ticket['sessions'] as $session) {
                        if ($earliestSaleStart == null || $this->compareTime($earliestSaleStart, $session['sale_start']))
                            $earliestSaleStart = strtotime($session['sale_start']);
                        if ($earliestSessionStart == null || $this->compareTime($earliestSessionStart, $session['session_start']))
                            $earliestSessionStart = strtotime($session['session_start']);
                        if ($latestSessionEnd == null || !$this->compareTime($latestSessionEnd, $session['session_end']))
                            $latestSessionEnd = strtotime($session['session_end']);
                    }
                }
            } else {
                $tickets = $ticketType[0];
                foreach ($tickets as $ticket) {
                    foreach ($ticket['row_session'] as $session) {
                        if ($earliestSaleStart == null || $this->compareTime($earliestSaleStart, $session['sell_from']))
                            $earliestSaleStart = strtotime($session['sell_from']);
                        if ($earliestSessionStart == null || $this->compareTime($earliestSessionStart, $session['session_from']))
                            $earliestSessionStart = strtotime($session['session_from']);
                        if ($latestSessionEnd == null || !$this->compareTime($latestSessionEnd, $session['session_to']))
                            $latestSessionEnd = strtotime($session['session_to']);
                    }
                }
            }
            // must fix 1 after add approve campaign in admin
            $now = strtotime(date('Y-m-d H:m:s'));
            if (count($tickets) > 0) {
                if ($now >= $latestSessionEnd) {
                    $product->setCampaignStatus(CampaignStatus::STATUS_EXPIRED);
                } else {
                    if ($now >= $earliestSessionStart) {
                        $product->setCampaignStatus(CampaignStatus::STATUS_ACTIVE);
                    } else {
                        if ($now >= $earliestSaleStart) {
                            $product->setCampaignStatus(CampaignStatus::STATUS_LIVE);
                        } else
                            $product->setCampaignStatus(CampaignStatus::STATUS_SCHEDULED);
                    }
                }
            }
        }
        return;
    }

    public function compareTime($time1, $time2)
    {
        return $time1 > strtotime($time2);
    }
}
