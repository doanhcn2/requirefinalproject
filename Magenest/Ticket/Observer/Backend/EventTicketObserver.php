<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\EventLocationFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Magento\Framework\App\Action\Context;
use Magenest\Ticket\Helper\Information;

/**
 * Class EventTicketObserver
 *
 * @method Observer getProduct()
 */
class EventTicketObserver implements ObserverInterface
{
    /**
     * @var $_hlp Information
     */
    protected $_hlp;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var $_ticketOrderFactory \Magenest\Ticket\Model\TicketFactory;
     */
    protected $_ticketOrderFactory;

    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var EventFactory
     */
    protected $event;

    /**
     * @var EventLocationFactory
     */
    protected $location;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var TicketsFactory
     */
    protected $tickets;

    /**
     * @var VenueMapDataFactory $_venueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var $_venueMapFactory VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    protected $mimPrice;

    /**
     * EventTicketObserver constructor.
     * @param RequestInterface $request
     * @param Filesystem $filesystem
     * @param StoreManagerInterface $storeManagerInterface
     * @param EventFactory $eventFactory
     * @param EventLocationFactory $eventLocationFactory
     * @param SessionFactory $eventSessionFactory
     * @param TicketsFactory $ticketsFactory
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param VenueMapFactory $venueMapFactory
     * @param Context $context
     * @param Information $information
     * @param \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        RequestInterface $request,
        Filesystem $filesystem,
        StoreManagerInterface $storeManagerInterface,
        EventFactory $eventFactory,
        EventLocationFactory $eventLocationFactory,
        SessionFactory $eventSessionFactory,
        TicketsFactory $ticketsFactory,
        VenueMapDataFactory $venueMapDataFactory,
        VenueMapFactory $venueMapFactory,
        Context $context,
        Information $information,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        $this->productFactory = $productFactory;
        $this->_unirgyProductFactory = $unirgyProductFactory;
        $this->_orderFactory = $orderFactory;
        $this->_ticketOrderFactory = $ticketFactory;
        $this->_hlp = $information;
        $this->_venueMapFactory = $venueMapFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_filesystem = $filesystem;
        $this->event = $eventFactory;
        $this->location = $eventLocationFactory;
        $this->session = $eventSessionFactory;
        $this->tickets = $ticketsFactory;
        $this->messageManager = $context->getMessageManager();
        $this->mimPrice = null;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $status = $product->getStatus();
        $productTypeId = $product->getTypeId();
        $params = $this->_request->getParams();
        if (!empty($params['event']) && $productTypeId == 'ticket' && !isset($params['approval'])) {
            $array = $params['event'];
            $isReservedSeating = false;
            if (@$array['is_reserved_seating']) {
                $isReservedSeating = $array['is_reserved_seating'];
            }
            $venueMap = null;
            if ($isReservedSeating !== false && $isReservedSeating !== "0") {
                $venueMap = $array['venue_map']['venue_map_choose'];
            } else {
                if (array_key_exists('event_add_paid_ticket', $array)) {
                    if (count($array['event_add_paid_ticket']) !== 0) {
                        foreach ($array['event_add_paid_ticket'] as $tickets) {
                            if (isset($tickets['row_session']) && !empty($tickets['row_session'])) {
                                foreach ($tickets['row_session'] as $session) {
                                    if (array_key_exists('session_map_id', $session)) {
                                        unset($session['session_map_id']);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $model = $this->event->create()->load($productId, 'product_id');

            $result = [
                'product_id' => $productId,
                'event_name' => $product->getName(),
                'is_online' => isset($array['is_online']) && $array['is_online'] == 1 ? 1 : 0,
                'event_type' => $array['event_type'],
                'age' => @$array['age'],
                'show_up' => isset($array['show_up']) && $array['show_up'] == 1 ? 1 : 0,
                'show_up_time' => $array['show_up_time'] && isset($array['show_up_time']) ? $array['show_up_time'] : null,
                'can_purchase' => isset($array['can_purchase']) && $array['can_purchase'] == 1 ? 1 : 0,
                'must_print' => isset($array['must_print']) && $array['must_print'] == 1 ? 1 : 0,
                'refund_policy' => isset($array['refund_policy']) && $array['refund_policy'] == 1 ? 'allow_refund' : 'non_refund',
                'refund_policy_day' => $array['refund_policy'] && isset($array['refund_policy']) ? $array['refund_policy_day'] : null,
                'public_organizer' => isset($array['public_organizer']) && $array['public_organizer'] == 1 ? 1 : 0,
                'organizer_name' => $array['organizer_name'],
                'organizer_description' => $array['organizer_description'],
                'use_map' => $array['ticket_address']['use_map'] && isset($array['ticket_address']['use_map']) ? $array['ticket_address']['use_map'] : 0,
                'email_config' => $array['emailtemplate_config'],
                'is_reserved_seating' => $isReservedSeating,
                'venue_map_id' => $venueMap
            ];
            $result['ticket_type'] = null;
            if ($isReservedSeating === "1") {
                if (isset($array['event_add_free_ticket'])) {
                    unset($array['event_add_free_ticket']);
                }
                if (isset($array['event_add_donation_ticket'])) {
                    unset($array['event_add_donation_ticket']);
                }
            }
            if (isset($array['event_add_paid_ticket'])) {
                $result['ticket_type'] = 'paid';
                $ticketInfo = $array['event_add_paid_ticket'];
            } elseif (isset($array['event_add_free_ticket'])) {
                $result['ticket_type'] = 'free';
                $ticketInfo = $array['event_add_free_ticket'];
            } elseif (isset($array['event_add_donation_ticket'])) {
                $result['ticket_type'] = 'donation';
                $ticketInfo = $array['event_add_donation_ticket'];
            }
            $model->addData($result);
            $model->save();
            if ($array['is_online'] == 1) {
                $location = $this->location->create()->load($productId, 'product_id');
                $locationData = $array['ticket_address'];
                $locationData['product_id'] = $productId;
                $location->addData($locationData)->save();
            }

//            $data['event_id'] = $model->getId();

            if (isset($array['event_add_paid_ticket']) && !empty($array['event_add_paid_ticket'])) {
                $this->saveTickets($array['event_add_paid_ticket'], 'paid', $productId);
            } else {
                $tickets = $this->tickets->create()->getCollection()
                    ->addFieldToFilter('type', 'paid')
                    ->addFieldToFilter('product_id', $productId);
                foreach ($tickets as $ticketDetele) {
                    $session = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketDetele->getId());
                    foreach ($session as $sessionDetele) {
                        $sessionDetele->delete();
                    }
                    $ticketDetele->delete();
                }
            }
            if (isset($array['event_add_free_ticket']) && !empty($array['event_add_free_ticket'])) {
                $this->saveTickets($array['event_add_free_ticket'], 'free', $productId);
            } else {
                $tickets = $this->tickets->create()->getCollection()
                    ->addFieldToFilter('type', 'free')
                    ->addFieldToFilter('product_id', $productId);
                foreach ($tickets as $ticketDetele) {
                    $session = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketDetele->getId());
                    foreach ($session as $sessionDetele) {
                        $sessionDetele->delete();
                    }
                    $ticketDetele->delete();
                }
            }
            if (isset($array['event_add_donation_ticket']) && !empty($array['event_add_donation_ticket'])) {
                $this->saveTickets($array['event_add_donation_ticket'], 'donation', $productId);
            } else {
                $tickets = $this->tickets->create()->getCollection()
                    ->addFieldToFilter('type', 'donation')
                    ->addFieldToFilter('product_id', $productId);
                foreach ($tickets as $ticketDetele) {
                    $session = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $ticketDetele->getId());
                    foreach ($session as $sessionDetele) {
                        $sessionDetele->delete();
                    }
                    $ticketDetele->delete();
                }
            }
            if ($isReservedSeating) {
                $saveSession = $this->addAllSessionMapData($productId, $venueMap, @$array['event_add_paid_ticket']);
                if ($saveSession) {
                    $this->messageManager->addNoticeMessage('You need to bind ticket with chair for this reserved seating event.Do it at: Catalog -> Venue Map For Session');
                }
            } else {
                $this->updateProductQuantity(@$ticketInfo, $productId);
            }
        }

        return;
    }

    /**
     * Generate session map data record for each session map, session map can't be created by vendor or admin
     *
     * @param $productId
     * @param $venueMapId
     * @param $ticket
     * @return bool
     */
    public function addAllSessionMapData($productId, $venueMapId, $ticket)
    {
        if (!isset($ticket) || empty($ticket)) {
            return false;
        }
        $sessionCurrentTicket = $this->getAllSessionFromCurrentTicket($ticket);
        $sessionTicketBeforeCollection = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToSelect('session_map_id')->getData();
        $sessionTicketBefore = [];
        foreach ($sessionTicketBeforeCollection as $item) {
            array_push($sessionTicketBefore, $item['session_map_id']);
        }
        /**
         * @var $venueMapModel \Magenest\VendorTicket\Model\VenueMapData
         */
        $venueMapModel = $this->_venueMapDataFactory->create();
        foreach ($sessionTicketBefore as $sessionBefore) {
            if (!in_array($sessionBefore, $sessionCurrentTicket)) {
                try {
                    $venueMapModel->load($sessionBefore, 'session_map_id')->delete();
                } catch (\Exception $e) {
                }
            }
        }
        foreach ($sessionCurrentTicket as $currentSession) {
            if (in_array($currentSession, $sessionTicketBefore)) {
                $oldMap = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('session_map_id', $currentSession)->addFieldToFilter('product_id', $productId)->getFirstItem()->getVenueMapId();
                if ($oldMap !== $venueMapId) {
                    $oldId = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('session_map_id', $currentSession)->addFieldToFilter('product_id', $productId)->getFirstItem()->getMapDataId();
                    try {
                        $venueMapModel->load($oldId)->delete();
                    } catch (\Exception $e) {
                    }
                    $dataSession = [
                        'product_id' => $productId,
                        'venue_map_id' => $venueMapId,
                        'session_map_id' => $currentSession,
                        'session_map_name' => null,
                        'session_map_data' => $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('venue_map_id', $venueMapId)->getFirstItem()->getVenueMapData()
                    ];
                    try {
                        $this->_venueMapDataFactory->create()->setData($dataSession)->save();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } else {
                $dataSession = [
                    'product_id' => $productId,
                    'venue_map_id' => $venueMapId,
                    'session_map_id' => $currentSession,
                    'session_map_name' => null,
                    'session_map_data' => $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('venue_map_id', $venueMapId)->getFirstItem()->getVenueMapData()
                ];
                try {
                    $venueMapModel->setData($dataSession)->save();
                } catch (\Exception $e) {
                }
            }
        }
        return true;
    }

    public function getAllSessionFromCurrentTicket($ticket)
    {
        $resultArray = [];
        foreach ($ticket as $item) {
            if (isset($item['row_session']) && !empty($item['row_session'])) {
                foreach ($item['row_session'] as $session) {
                    $sessionMapId = $session['session_map_id'];
                    if (!in_array($sessionMapId, $resultArray)) {
                        array_push($resultArray, $sessionMapId);
                    }
                }
            }
        }
        return $resultArray;
    }

    /**
     * @param $data
     * @param $type
     * @param $productId
     */
    public function saveTickets($data, $type, $productId)
    {
        $model = $this->tickets->create()->getCollection()
            ->addFieldToFilter('type', $type)
            ->addFieldToFilter('product_id', $productId);
        $ticketsArr = [];
        foreach ($model as $arr) {
            $id = $arr->getId();
            array_push($ticketsArr, $id);
        }

        $tickArr = [];
        foreach ($data as $tickets) {
            $modelTickets = $this->tickets->create();
            if (isset($tickets['tickets_id'])) {
                array_push($tickArr, $tickets['tickets_id']);
            }
            if (isset($tickets['tickets_id'])) {
                $modelTickets->load($tickets['tickets_id']);
            }
            $result['product_id'] = $productId;
            $result['title'] = $tickets['title'];
            $result['description'] = $tickets['description'];
            $result['type'] = $type;
            $modelTickets->addData($result)->save();
            $idTicket = $modelTickets->getId();
            if (isset($tickets['row_session']) && !empty($tickets['row_session']) && $idTicket) {
                $this->saveSession($tickets['row_session'], $idTicket, $productId, $type);
            } else {
                /** \Magenest\Ticket\Model\Session $model */
                $model = $this->session->create()->getCollection()
                    ->addFieldToFilter('tickets_id', $idTicket);
                foreach ($model as $modelDelete) {
                    $modelDelete->delete();
                }
            }
        }
        $ticketsDelete = array_diff($ticketsArr, $tickArr);
        if (!empty($ticketsDelete)) {
            foreach ($ticketsDelete as $value) {
                $this->tickets->create()->load($value)->delete();
            }
        }
    }

    /**
     * @param $data
     * @param $idTicket
     * @param $productId
     * @param $type
     */
    public function saveSession($data, $idTicket, $productId, $type)
    {
        $model = $this->session->create()->getCollection()
            ->addFieldToFilter('tickets_id', $idTicket);
        $sessionArr = [];
        foreach ($model as $arr) {
            $id = $arr->getId();
            array_push($sessionArr, $id);
        }

        $sesArr = [];
        $validation = true;
        foreach ($data as $session) {
            if ($this->validate($session)) {
                $modelSession = $this->session->create();
                if (isset($session['session_id'])) {
                    array_push($sesArr, $session['session_id']);
                }
                if (isset($session['session_id'])) {
                    $modelSession->load($session['session_id']);
                }
                $session['tickets_id'] = $idTicket;
                $session['product_id'] = $productId;
                if ($type === "paid") {
                    if (empty($session['sale_price'])) {
                        $session['sale_price'] = null;
                    }
                    if ($session['sale_price'] > $session['original_value']) {
                        $session['sale_price'] = null;
                        $this->messageManager->addErrorMessage('Sale Price must less than Original Price.');
                    }
                } else {
                    if (empty($session['sale_price'])) {
                        $session['sale_price'] = 0;
                    }
                }
                if ($session['max_qty'] === "")
                    $session['max_qty'] = null;
                if (@$modelSession->getQtyAvailable() === null)
                    $session['qty_available'] = $session['qty'];
                if (@$modelSession->getQtyPurchased() === null)
                    $session['qty_purchased'] = 0;

                unset($session['session_id']);
                $modelSession->addData($session)->save();

                if ($session['sale_price'])
                    $min = $session['sale_price'];
                elseif ($session['original_value'])
                    $min = $session['original_value'];
                else
                    $min = null;
                if (!$this->mimPrice || $this->mimPrice > $min)
                    $this->mimPrice = $min;
            } else {
                $validation = false;
            }
        }
        if (!$validation) {
            $this->messageManager->addErrorMessage(__('Date fields is invalidate. Some ticket\'sessions with wrong date field was not saved.'));
        }

        $sessionDelete = array_diff($sessionArr, $sesArr);
        if (!empty($sessionDelete)) {
            foreach ($sessionDelete as $value) {
                $this->session->create()->load($value)->delete();
            }
        }
    }

    private function updateProductQuantity($ticket_info, $productId)
    {
        $qty = $this->getTicketQuantity($ticket_info, $productId);
        if (@$qty !== 0) {
            /**
             * @var $product \Magento\Catalog\Model\Product
             */
            $product = $this->productFactory->create();
            try {
                if (!$this->mimPrice)
                    $this->mimPrice = 0;
                $product->load($productId)->setQty(floatval($qty))->setPrice($this->mimPrice)->setData('vendor_price', $this->mimPrice)->save();
            } catch (\Exception $e) {
            }

            $unirgyProduct = $this->_unirgyProductFactory->create()->load('product_id', $productId);
            $unirgyProduct->setStockQty(floatval($qty))->save();
        }
    }

    private function getTicketQuantity($ticket_info, $productId)
    {
        $count = 0;
        foreach ($ticket_info as $item) {
            if (isset($item['sessions']) && !empty($item['sessions'])) {
                foreach ($item['sessions'] as $session) {
                    if ($session['qty']) {
                        $count += intval($session['qty']);
                    }
                }
            }
        }
        $ordereds = $this->_ticketOrderFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        foreach ($ordereds as $order) {
            $orderedCount = 0;
            $orderId = $order->getOrderId();
            $orderItems = $this->_orderFactory->create()->load($orderId)->getAllItems();
            foreach ($orderItems as $itemOrder) {
                if ($itemOrder->getId() === $productId) {
                    $orderedCount += $itemOrder->getQtyOrdered();
                }
            }
        }
        if (@$orderedCount) {
            return $count - intval($orderedCount);
        }

        return $count;
    }

    private function validate($session)
    {
        if (strtotime($session['session_from']) && strtotime($session['session_to']) && strtotime($session['sell_from']) && strtotime($session['sell_to'])) {
            return true;
        } else {
            return false;
        }
    }
}
