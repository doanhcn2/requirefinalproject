<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Backend;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magenest\Ticket\Model\Mail\Template\TransportBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\Mail\MessageInterface;

/**
 * Class SendEmailToCustomer
 * @package Magenest\Ticket\Observer\Backend
 */
class SendEmailToCustomer implements ObserverInterface
{
    const XML_PATH_EMAIL_SENDER = 'trans_email/ident_general/email';

    const XML_PATH_NAME_SENDER = 'trans_email/ident_general/name';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var MessageInterface
     */
    protected $_message;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $deal;
    /**
     * @var \Magenest\Groupon\Model\GrouponFactory
     */
    protected $groupon;
    /**
     * @var \Magenest\Groupon\Model\TicketFactory
     */
    protected $ticket;
    /**
     * @var \Magenest\Groupon\Helper\RenderCode
     */
    protected $renderCode;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * SendEmailToCustomer constructor.
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param \Magenest\Groupon\Helper\RenderCode $renderCode
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\RequestInterface $request
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param MessageInterface $message
     */
    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magenest\Groupon\Helper\RenderCode $renderCode,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\RequestInterface $request,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        MessageInterface $message
    ) {
        $this->deal = $dealFactory;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $loggerInterface;
        $this->renderCode = $renderCode;
        $this->messageManager = $messageManager;
        $this->_request = $request;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_message = $message;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\MailException
     */
    public function execute(Observer $observer)
    {
        $orderId = $observer->getEvent()->getInvoice()->getData('order_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $item){
            if( $item->getData('product_type') == 'ticket'){
                $productOptions = $item->getData('product_options');
                $additionalOptions = $productOptions['additional_options'];
                if($additionalOptions != null && $this->isGiftOptions($additionalOptions)){
                    $data = [
                        'dealOption' => @$additionalOptions[0]['value'],
                        'recipientName' => @$additionalOptions[2]['value'],
                        'recipientEmail' => @$additionalOptions[3]['value'],
                        'message' => @$additionalOptions[4]['value'],
                        'vendor' => @$additionalOptions[5]['value']
                    ];
                    $this->sendEmail($data);
                }
            }
        }
        return;
    }

    /**
     * @param $data
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendEmail($data){
        $this->_inlineTranslation->suspend();
        $emailTemplate = "groupon_gift_mail_template";

        $this->_message->clearFrom()->clearSubject();

        $transport = $this->_transportBuilder->setTemplateIdentifier($emailTemplate)->setTemplateOptions(
            [
                'area' => Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            [
                'deal_option' => $data['dealOption'],
                'recipient_name' => $data['recipientName'],
                'recipient_email' => $data['recipientEmail'],
                'message' => $data['message'],
                'vendor' => $data['vendor']
            ]
        )->setFrom(
            [
                'email' => $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER),
                'name' => $this->_scopeConfig->getValue(self::XML_PATH_NAME_SENDER)
            ]
        )->addTo(
            $data['recipientEmail'],
            $data['recipientName']
        )->getTransport();
        $transport->sendMessage();
        $this->_inlineTranslation->resume();
    }

    /**
     * @param $additionalOptions
     * @return bool
     */
    private function isGiftOptions($additionalOptions)
    {
        $result = 0;
        foreach($additionalOptions as $item) {
            if ($item['label'] === "Gift's Recipient Email") {
                $result++;
            }
            if ($item['label'] === "Gift's Recipient Name") {
                $result++;
            }
        }

        return $result == 2 ? true : false;
    }
}
