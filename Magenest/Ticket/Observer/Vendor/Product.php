<?php

namespace Magenest\Ticket\Observer\Vendor;

use Magenest\Ticket\Model\TicketSession;
use Magento\Framework\Event\ObserverInterface;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Unirgy\Dropship\Helper\Data as HelperData;

/**
 * Class Product
 * @package Magenest\Groupon\Observer\Vendor
 */
class Product implements ObserverInterface
{
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $event;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketInfoFactory;

    /**
     * @var $_ticketOrderFactory \Magenest\Ticket\Model\TicketFactory;
     */
    protected $_ticketOrderFactory;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_ticketSessionFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var $_venueMapDataFactory VenueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var HelperData
     */
    protected $_hlp;

    /**
     * Product constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory
     * @param \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param VenueMapFactory $venueMapFactory
     * @param HelperData $data
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Message\ManagerInterface $manager
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory,
        \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        VenueMapDataFactory $venueMapDataFactory,
        VenueMapFactory $venueMapFactory,
        HelperData $data,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Message\ManagerInterface $manager,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory
    )
    {
        $this->_unirgyProductFactory = $unirgyProductFactory;
        $this->_orderFactory = $orderFactory;
        $this->_ticketOrderFactory = $ticketFactory;
        $this->_hlp = $data;
        $this->_venueMapFactory = $venueMapFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        $this->messageManager = $manager;
        $this->event = $eventFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->_ticketInfoFactory = $ticketInfoFactory;
        $this->_ticketSessionFactory = $ticketSessionFactory;
        $this->location = $eventLocationFactory;
        $this->productFactory = $productFactory;
        $this->stockStateInterface = $stockStateInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $product->setTypeId('ticket');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $modelProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId());
        $modelProduct->setTypeId('ticket')->save();
        $productId = $product->getId();
        $params = $observer->getEvent()->getInformation();
        $store = $product->getStore()->getStoreId();
        if (!empty($params['event'])) {
            $array = $params['event'];
            $isReservedSeating = isset($array['is_reserved_seating_event']) ? 1 : 0;
            $venueMap = null;
            if ($isReservedSeating !== 0) {
                $venueMap = $array['exist_venue_map'];
                if ($venueMap === "-1") {
                    $this->messageManager->addErrorMessage("Please select a venue map.");
                    return false;
                }
            }
            $result = [
                'product_id' => $productId,
                'event_name' => $product->getName(),
                'is_online' => isset($array['is_online']) ? 1 : 0,
                'event_type' => $array['event_type'],
                'age' => $array['age_suitability'],
                'show_up' => isset($array['show_up']) ? 1 : 0,
                'show_up_time' => $array['time_show_up'] && isset($array['time_show_up']) ? $array['time_show_up'] : null,
                'can_purchase' => isset($array['can_purchase']) ? 1 : 0,
                'must_print' => isset($array['must_print']) ? 1 : 0,
                'refund_policy' => isset($array['refund_policy']) ? $array['refund_policy'] : 'non_refund',
                'refund_policy_day' => $array['refund_day'] && isset($array['refund_day']) ? $array['refund_day'] : null,
                'public_organizer' => isset($array['organizer_detail']) ? 1 : 0,
                'organizer_name' => $array['organizer_name'],
                'organizer_description' => $array['organizer_description'],
                'use_map' => array_key_exists("use_map", $array) ? 1 : 0,
                'email_config' => 'ticket_email_template',
                'is_reserved_seating' => $isReservedSeating,
                'venue_map_id' => $venueMap
            ];

            $model = $this->event->create()->load($productId, 'product_id');
            $model->addData($result)->save();
            $minPrice = null;

            if (isset($array['ticket_info']) && !empty($array['ticket_info'])) {
                $this->removeDeletedTicket($array['ticket_info'], $productId);
                if ($isReservedSeating === 1) {
                    $this->addAllSessionMapData($productId, $venueMap, $array['ticket_info']);
                }

                foreach ($array['ticket_info'] as $ticket) {
                    $model->setTicketType($ticket['type'])->save();
                    $modelTicketInfo = $this->_ticketInfoFactory->create();
                    if (array_key_exists("id", $ticket) && $ticket['id'] != "") {
                        $modelTicketInfo->load($ticket['id']);
                    }
                    $modelTicketInfo->addData([
                        'product_id' => $model->getProductId(),
                        'title' => $ticket['title'],
                        'description' => $ticket['description'],
                        'type' => $ticket['type']
                    ])->save();
                    $donation = false;
                    if ($ticket['type'] === "donation") {
                        $donation = true;
                    }
                    foreach ($ticket['sessions'] as $session) {
                        $modelTicketSession = $this->_ticketSessionFactory->create();
                        if ($ticket['type'] === 'paid') {
                            if (array_key_exists("salePrice", $session) && !empty(@$session['salePrice'])) {
                                $salePrices = $session['salePrice'];
                            } else {
                                $salePrices = null;
                            }
                        } else {
                            $salePrices = array_key_exists("salePrice", $session) ? $session['salePrice'] : 0;
                        }
                        $origPrice = array_key_exists('orig_value', $session) ? $session['orig_value'] : null;

                        if (array_key_exists("id", $session)) {
                            $modelTicketSession->load($session['id']);
                        }
                        $modelTicketSession->addData([
                            "tickets_id" => $modelTicketInfo->getTicketsId(),
                            "session_from" => $session['session_start'] ? $this->convertTime($session['session_start']) : null,
                            "session_to" => $session['session_end'] ? $this->convertTime($session['session_end']) : null,
                            "sell_from" => $session['sale_start'] ? $this->convertTime($session['sale_start']) : null,
                            "sell_to" => $session['sale_end'] ? $this->convertTime($session['sale_end']) : null,
                            "sale_price" => $salePrices,
                            "original_value" => $origPrice,
                            "qty" => array_key_exists('qty', $session) ? $session['qty'] : 0,
                            "qty_available" => array_key_exists('qty', $session) ? $session['qty'] : 0,
                            "max_qty" => $session['max_qty'] ? $session['max_qty'] : null,
                            'product_id' => $productId,
                            "hide_sold_out" => array_key_exists('is_hide', $session) ? 1 : 0,
                            "session_map_id" => array_key_exists("session_map_id", $session) ? $session['session_map_id'] : null,
                            "min_donation" => $donation ? $salePrices : null
                        ])->save();
                        if ($salePrices)
                            $min = $salePrices;
                        elseif ($origPrice)
                            $min = $origPrice;
                        else
                            $min = null;
                        if (!$minPrice || $minPrice > $min)
                            $minPrice = $min;
                    }

                }
                if (!$minPrice)
                    $minPrice = 0;
                $modelProduct->setPrice($minPrice)->setData('vendor_price', $minPrice);
                $modelProduct->save();

                if ($isReservedSeating === 1) {
                    $this->updateBindingTicketInformation($productId, $venueMap, $array['ticket_info']);
                }
            }

            if (isset($array['is_online'])) {
                $this->saveLocation($array['location'], $productId, $model->getId());
            }
        }

        return;
    }

    public function addAllSessionMapData($productId, $venueMapId, $ticket)
    {
        if (!isset($ticket) || empty($ticket)) {
            return;
        }
        $sessionCurrentTicket = $this->getAllSessionFromCurrentTicket($ticket);
        $sessionTicketBeforeCollection = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToSelect('session_map_id')->getData();
        $sessionTicketBefore = [];
        foreach ($sessionTicketBeforeCollection as $item) {
            array_push($sessionTicketBefore, $item['session_map_id']);
        }
        /**
         * @var $venueMapModel \Magenest\VendorTicket\Model\VenueMapData
         */
        $venueMapModel = $this->_venueMapDataFactory->create();
        foreach ($sessionTicketBefore as $sessionBefore) {
            if (!in_array($sessionBefore, $sessionCurrentTicket)) {
                try {
                    $venueMapModel->load($sessionBefore, 'session_map_id')->delete();
                } catch (\Exception $e) {
                }
            }
        }
        foreach ($sessionCurrentTicket as $currentSession) {
            if (in_array($currentSession, $sessionTicketBefore)) {
                $oldMap = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('session_map_id', $currentSession)->addFieldToFilter('product_id', $productId)->getFirstItem()->getVenueMapId();
                if ($oldMap !== $venueMapId) {
                    $oldId = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('session_map_id', $currentSession)->addFieldToFilter('product_id', $productId)->getFirstItem()->getMapDataId();
                    try {
                        $venueMapModel->load($oldId)->delete();
                    } catch (\Exception $e) {
                    }
                    $dataSession = [
                        'product_id' => $productId,
                        'venue_map_id' => $venueMapId,
                        'session_map_id' => $currentSession,
                        'session_map_name' => null,
                        'session_map_data' => $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('venue_map_id', $venueMapId)->getFirstItem()->getVenueMapData(),
                        'vendor_id' => $this->_hlp->session()->getVendorId()
                    ];
                    try {
                        $this->_venueMapDataFactory->create()->setData($dataSession)->save();
                    } catch (\Exception $e) {
                    }
                }
                continue;
            } else {
                $dataSession = [
                    'product_id' => $productId,
                    'venue_map_id' => $venueMapId,
                    'session_map_id' => $currentSession,
                    'session_map_name' => null,
                    'session_map_data' => $this->_venueMapFactory->create()->getCollection()->addFieldToFilter('venue_map_id', $venueMapId)->getFirstItem()->getVenueMapData(),
                    'vendor_id' => $this->_hlp->session()->getVendorId()
                ];
                try {
                    $venueMapModel->setData($dataSession)->save();
                } catch (\Exception $e) {
                }
            }
        }
    }

    public function getAllSessionFromCurrentTicket($ticket)
    {
        $resultArray = [];
        foreach ($ticket as $item) {
            if (isset($item['sessions']) && !empty($item['sessions'])) {
                foreach ($item['sessions'] as $session) {
                    $sessionMapId = $session['session_map_id'];
                    if (!in_array($sessionMapId, $resultArray)) {
                        array_push($resultArray, $sessionMapId);
                    }
                }
            }
        }
        return $resultArray;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);

        return date('Y-m-d H:i:s', $strTime);
    }

    /**
     * @param $data
     * @param $productId
     * @param $eventId
     * @throws \Exception
     */
    public function saveLocation($data, $productId, $eventId)
    {
        $data['event_id'] = $eventId;
        $data['product_id'] = $productId;
        if (isset($data['use_map'])) {
            unset($data['use_map']);
        }
        $location = $this->location->create()->load($productId, 'product_id');
        $location->addData($data)->save();
    }

    public function removeDeletedTicket($tickets, $productId)
    {
        /**
         * @var $ticketFactory \Magenest\Ticket\Model\Tickets
         */
        $ticketFactory = $this->_ticketInfoFactory->create();
        /**
         * @var $ticketSessionFactory \Magenest\Ticket\Model\Session
         */
        $ticketSessionFactory = $this->_ticketSessionFactory->create();

        $newTicketId = [];
        foreach ($tickets as $ticket) {
            if (!array_key_exists('id', $ticket)) {
                continue;
            }

            if (!array_key_exists('sessions', $ticket)) {
                try {
                    $ticketFactory->load($ticket['id'])->delete();
                } catch (\Exception $e) {
                }
                continue;
            }

            $newSessionId = [];
            foreach ($ticket['sessions'] as $session) {
                if (isset($session['id']) && !empty($session['id'])) {
                    array_push($newSessionId, $session['id']);
                }
            }
            $ticketsSessionCollection = $ticketSessionFactory->getCollection()->addFieldToFilter('tickets_id', $ticket['id']);
            foreach ($ticketsSessionCollection as $item) {
                if (!in_array($item->getSessionId(), $newSessionId)) {
                    try {
                        $ticketSessionFactory->load($item->getSessionId())->delete();
                    } catch (\Exception $e) {
                    }
                }
            }

            if (isset($ticket['id']) && !empty($ticket['id'])) {
                array_push($newTicketId, $ticket['id']);
            }
        }

        $ticketsCollection = $ticketFactory->getCollection()->addFieldToFilter('product_id', $productId);
        foreach ($ticketsCollection as $oldTicket) {
            if (!in_array($oldTicket->getTicketsId(), $newTicketId)) {
                try {
                    $ticketFactory->load($oldTicket->getTicketsId())->delete();
                } catch (\Exception $e) {
                }
            }
        }
    }

    /**
     * Add new ticket has been created by vendor to json string of ticket data
     * Update new ticket to session map data
     *
     * @param $productId
     * @param $venueMap
     * @param $ticket_info
     */
    private function updateBindingTicketInformation($productId, $venueMap, $ticket_info)
    {
        $oldSession = $this->getOldSessionByProductId($productId, $venueMap);
        foreach ($oldSession as $oddSession) {
            $dbData = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('venue_map_id', $venueMap)->addFieldToFilter('session_map_id', intval($oddSession))->getFirstItem()->getSessionTicketData();
            $dbId = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('venue_map_id', $venueMap)->addFieldToFilter('session_map_id', intval($oddSession))->getFirstItem()->getMapDataId();
            $sessionTicketData = json_decode($dbData, true);
            if ($sessionTicketData === null) $sessionTicketData = [];
            $newTicketData = $this->getTicketDataBySession($ticket_info, $oddSession);
            $change = $this->hasChangeStatus($newTicketData, $productId, $oddSession);
            if ($change['has_change']) {
                $dataChange = $change['change'];
                foreach ($dataChange as $itemChange) {
                    for ($i = 0; $i < count($sessionTicketData); $i++) {
                        if ($sessionTicketData[$i]["id"] === $itemChange["id"]) {
                            $sessionTicketData[$i]["label"] = $itemChange["label"];
                            break;
                        }
                    }
                }
            }
            $newTicket = $this->getAllNewTicketHandle($sessionTicketData, $oddSession, $productId);
            if (count($newTicket) > 0) {
                foreach ($newTicket as $ticket) {
                    array_push($sessionTicketData, $ticket);
                }
            }
            if (count($newTicket) > 0 || $change['has_change']) {
                $oldObject = $this->_venueMapDataFactory->create()->load($dbId);
                $data = $oldObject->getData();
                $data['session_ticket_data'] = json_encode($sessionTicketData);
                $oldObject->addData($data)->save();
            }
        }
    }

    private function getOldTicketFromEachMapSessionData($productId, $sessionMapId)
    {
        $sessionTicketData = json_decode($this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('session_map_id', $sessionMapId)->getFirstItem()->getSessionTicketData(), true);
        $oldTicket = [];
        if ($sessionTicketData === null) {
            return $oldTicket;
        }
        foreach ($sessionTicketData as $sessionTicketDatum) {
            array_push($oldTicket, [$sessionTicketDatum['id'], $sessionTicketDatum['label']]);
        }
        return $oldTicket;
    }

    private function getOldSessionByProductId($productId, $venueMap)
    {
        $oldSession = $this->_venueMapDataFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('venue_map_id', $venueMap)->getData();
        $sessionArr = [];
        foreach ($oldSession as $item) {
            if (!in_array($item['session_map_id'], $sessionArr)) {
                array_push($sessionArr, $item['session_map_id']);
            }
        }
        return $sessionArr;
    }

    private function hasChangeStatus($newTicketData, $productId, $sessionMapId)
    {
        $oldTicket = $this->getOldTicketFromEachMapSessionData($productId, $sessionMapId);
        $oldTicketId = [];
        foreach ($oldTicket as $ticket) {
            if (!in_array($ticket[0], $oldTicketId)) {
                array_push($oldTicketId, $ticket[0]);
            }
        }
        $result = [];
        $result['has_change'] = false;
        $result['change'] = [];
        foreach ($newTicketData as $ticket) {
            if (in_array($ticket['id'], $oldTicketId)) {
                foreach ($oldTicket as $old) {
                    if ($ticket['id'] === $old[0]) {
                        if ($ticket['label'] !== $old[1]) {
                            $result['has_change'] = true;
                            array_push($result['change'], ['id' => $ticket['id'], 'label' => $ticket['label']]);
                        }
                    }
                }
            }
        }

        return $result;
    }

    private function getTicketDataBySession($ticket_info, $oddSession)
    {
        $ticketArray = [];
        foreach ($ticket_info as $item) {
            if (isset($item['sessions']) && !empty($item['sessions'])) {
                foreach ($item['sessions'] as $session) {
                    if ($session['session_map_id'] === $oddSession) {
                        if (!empty($session['id'])) {
                            $ticketItem = $this->renderTicketPattern($session['id'], $item['title']);
                            array_push($ticketArray, $ticketItem);
                        }
                    }
                }
            }
        }
        return $ticketArray;
    }

    private function renderTicketPattern($id, $label)
    {
        return [
            'id' => $id,
            'label' => $label,
            'cnt' => 0,
            'colorTag' => '#ffffff',
            'chairs' => []
        ];
    }

    private function getAllNewTicketHandle($sessionTicketData, $oddSession, $productId)
    {
        $oldTicketId = $this->getOldSessionTicketId($sessionTicketData);
        $allNewSession = $this->_ticketSessionFactory->create()->getCollection()->addFieldToFilter('session_map_id', $oddSession)->addFieldToFilter('product_id', $productId);
        $result = [];
        foreach ($allNewSession as $session) {
            $newTicketId = $session->getTicketsId();
            if (!in_array($newTicketId, $oldTicketId)) {
                $sessionItem = $this->renderTicketPattern($session->getSessionId(), $this->getTicketLabel($session->getTicketsId()));
                array_push($result, $sessionItem);
            }
        }
        return $result;
    }

    private function getOldSessionTicketId($sessionTicketData)
    {
        $result = [];
        if ($sessionTicketData === null) {
            return $result;
        }
        foreach ($sessionTicketData as $item) {
            $ticketId = $this->_ticketSessionFactory->create()->load($item['id'])->getTicketsId();
            if (!in_array($ticketId, $result)) {
                array_push($result, $ticketId);
            }
        }
        return $result;
    }

    private function getTicketLabel($getTicketsId)
    {
        return $this->_ticketInfoFactory->create()->load($getTicketsId)->getTitle();
    }
}
