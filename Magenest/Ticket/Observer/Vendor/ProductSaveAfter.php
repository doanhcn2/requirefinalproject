<?php

namespace Magenest\Ticket\Observer\Vendor;

use Magenest\Ticket\Model\TicketSession;
use Magento\Framework\Event\ObserverInterface;
use Magenest\VendorTicket\Model\VenueMapDataFactory;
use Magenest\VendorTicket\Model\VenueMapFactory;
use Unirgy\Dropship\Helper\Data as HelperData;

/**
 * Class Product
 * @package Magenest\Groupon\Observer\Vendor
 */
class ProductSaveAfter implements ObserverInterface
{
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $event;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_ticketInfoFactory;

    /**
     * @var $_ticketOrderFactory \Magenest\Ticket\Model\TicketFactory;
     */
    protected $_ticketOrderFactory;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_ticketSessionFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var $_venueMapDataFactory VenueMapDataFactory
     */
    protected $_venueMapDataFactory;

    /**
     * @var VenueMapFactory
     */
    protected $_venueMapFactory;

    /**
     * @var HelperData
     */
    protected $_hlp;

    /**
     * Product constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Ticket\Model\EventFactory $eventFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory
     * @param \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param VenueMapDataFactory $venueMapDataFactory
     * @param VenueMapFactory $venueMapFactory
     * @param HelperData $data
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Message\ManagerInterface $manager
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $unirgyProductFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\EventLocationFactory $eventLocationFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketInfoFactory,
        \Magenest\Ticket\Model\SessionFactory $ticketSessionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        VenueMapDataFactory $venueMapDataFactory,
        VenueMapFactory $venueMapFactory,
        HelperData $data,
        \Magento\CatalogInventory\Api\StockStateInterface $stockStateInterface,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Message\ManagerInterface $manager,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory
    )
    {
        $this->_unirgyProductFactory = $unirgyProductFactory;
        $this->_orderFactory = $orderFactory;
        $this->_ticketOrderFactory = $ticketFactory;
        $this->_hlp = $data;
        $this->_venueMapFactory = $venueMapFactory;
        $this->_venueMapDataFactory = $venueMapDataFactory;
        $this->messageManager = $manager;
        $this->event = $eventFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->_ticketInfoFactory = $ticketInfoFactory;
        $this->_ticketSessionFactory = $ticketSessionFactory;
        $this->location = $eventLocationFactory;
        $this->productFactory = $productFactory;
        $this->stockStateInterface = $stockStateInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $productId = $observer->getProductId();
        $event = $observer->getEvent();
        $eventData = $event->getData();
        $arrayDataTicketInfo = $eventData['information']['event']['ticket_info'];
        $this->updateProductQuantity($arrayDataTicketInfo, $productId);
    }

    private function updateProductQuantity($ticket_info, $productId)
    {
        $qty = $this->getTicketQuantity($ticket_info, $productId);
        if (@$qty !== 0) {
            /**
             * @var $product \Magento\Catalog\Model\Product
             */
            $product = $this->productFactory->create();
            try {
                $product->load($productId)->setQty(floatval($qty))->save();
            } catch (\Exception $e) {
            }

            $unirgyProduct = $this->_unirgyProductFactory->create()->load($productId, 'product_id');
            $unirgyProduct->setStockQty(floatval($qty))->save();
        }
    }

    private function getTicketQuantity($ticket_info, $productId)
    {
        $count = 0;
        foreach ($ticket_info as $item) {
            if (isset($item['sessions']) && !empty($item['sessions'])) {
                foreach ($item['sessions'] as $session) {
                    if ($session['qty']) {
                        $count += intval($session['qty']);
                    }
                }
            }
        }
        $ordereds = $this->_ticketOrderFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
        foreach ($ordereds as $order) {
            $orderedCount = 0;
            $orderId = $order->getOrderId();
            $orderItems = $this->_orderFactory->create()->load($orderId)->getAllItems();
            foreach ($orderItems as $itemOrder) {
                if ($itemOrder->getProductId() === $productId) {
                    $orderedCount += $itemOrder->getQtyOrdered();
                }
            }
        }
        if (@$orderedCount) {
            return $count - intval($orderedCount);
        }

        return $count;
    }
}
