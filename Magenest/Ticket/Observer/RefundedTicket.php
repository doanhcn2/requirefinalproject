<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\EventFactory;
use Magento\Sales\Model\Order\Item as OrderItem;

/**
 * Class RefundedTicket
 * @package Magenest\Ticket\Observer
 */
class RefundedTicket implements ObserverInterface
{

    protected $_sessionFactory;

    protected $_ticketFactory;

    public function __construct(
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory


    )
    {
        $this->_ticketFactory = $ticketFactory;
        $this->_sessionFactory = $sessionFactory;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var OrderItem $orderItem */
        $orderId = $observer->getCreditmemo()->getOrderId();
        $tickets = $this->_ticketFactory->create()->getCollection()->addFieldToFilter("order_id", $orderId);
        if($tickets->count()==0)
            return ;
        $restockSession = [];
        foreach ($tickets as $ticket) {
            if($ticket)
            $ticket->setRefundedAt(date("Y-m-d"));
            $ticket->setStatus(3)->save();
            $info =unserialize($ticket->getInformation());
            $sessionId =$info['session_id'];
            if(!array_key_exists($sessionId,$restockSession))
                $restockSession[$sessionId]=['qty'=>1];
            else
                $restockSession[$sessionId]['qty']++;
        }

        foreach ($restockSession as $item=>$qty) {
            $session = $this->_sessionFactory->create()->load($item);
            $session->setQtyAvailable($session->getQtyAvailable()+$qty['qty']);
            $session->setQtyRefunded($session->getQtyRefunded()+$qty['qty']);
            $session->setQtyPurchased($session->getQtyPurchased()-$qty['qty'])->save();
        }

        return;
    }
}
