<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Ticket\Observer;

use Magenest\Ticket\Helper\Event as HelperEvent;
use Magenest\Ticket\Helper\Information;
use Magenest\Ticket\Model\Event;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item as OrderItem;

/**
 * Class GenerateTicket
 * @package Magenest\Ticket\Observer
 */
class GenerateTicket implements ObserverInterface
{
    /**
     * email config
     */
    const XML_PATH_EMAIL = 'event_ticket/email_config/email';
    /**
     * qty config
     */
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';
    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var HelperEvent
     */
    protected $_helperEvent;

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_sessionFactory;
    /**
     * GenerateTicket constructor.
     * @param TicketFactory $ticketFactory
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param EventFactory $eventFactory
     * @param HelperEvent $helperEvent
     * @param Information $information
     */
    public function __construct(
        TicketFactory $ticketFactory,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        EventFactory $eventFactory,
        HelperEvent $helperEvent,
        Information $information
    ) {
        $this->_sessionFactory = $sessionFactory;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->logger = $loggerInterface;
        $this->_ticketFactory = $ticketFactory;
        $this->_eventFactory = $eventFactory;
        $this->_helperEvent = $helperEvent;
        $this->information = $information;
        $this->messageManager = $messageManager;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var OrderItem $orderItem */
        $orderItem = $observer->getEvent()->getItem();
        /** @var \Magento\Catalog\Model\Product $product */
        $productType = $orderItem->getProductType();
        $buyInfo = $orderItem->getBuyRequest();
        $event = $this->_eventFactory->create()->loadByProductId($orderItem->getProductId());
        if ($event->getId() && $productType == Event::PRODUCT_TYPE && $orderItem->getStatusId() == OrderItem::STATUS_INVOICED && !isset($request['udpo'])) {
            if (!$this->_helperEvent->getTicket($orderItem->getId())) {
                $options = $buyInfo->getAdditionalOptions();
                $prices = intval($buyInfo->getQty()) * floatval(@$options['ticket_price']);
                /** @var \Magento\Sales\Model\Order $order */
                $order = $orderItem->getOrder();
                $qty = $orderItem->getQtyOrdered();
                $email = $order->getCustomerEmail();
                $firstname = $order->getCustomerFirstname();
                $lastname = $order->getCustomerLastname();
                $customerId = $order->getCustomerId();
                $customerName = $firstname . " " . $lastname;

                if (!$customerId) {
                    $customerName = 'Guest';
                }

//                $configEmail = $this->_scopeConfig->getValue(self::XML_PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
//                if ($configEmail == 'send_multi_email') {
                    $putQty = 1;
                    $number = $qty;
//                } else {
//                    $putQty = $qty;
//                    $number = 1;
//                }
                $arrayInformation = $this->information->getAll($options);

                if (isset($options['To'])) {
                    $recipient = json_encode([
                        "name" => @$options['To'],
                        "mail" => @$options['Email'],
                        "message" => @$options['Message'],
                        "type" => @$options['Type']
                    ]);
                } else {
                    $recipient = null;
                }

                $ticketData = [
                    'title' => $orderItem->getName(),
                    'tickets_id' => @$arrayInformation['tickets_id'],
                    'session_id' => @$arrayInformation['session_id'],
                    'event_id' => $event->getId(),
                    'product_id' => $orderItem->getProductId(),
                    'customer_name' => $customerName,
                    'customer_email' => $email,
                    'customer_id' => $customerId,
                    'order_item_id' => $orderItem->getId(),
                    'order_id' => $order->getId(),
                    'order_increment_id' => $order->getIncrementId(),
                    'information' => serialize($arrayInformation),
                    'qty' => $putQty,
                    'status' => 1,
                    'vendor_id' => $orderItem->getUdropshipVendor(),
                    'ticket_price' => floatval(@$options['ticket_price']),
                    'recipient' => $recipient,
                ];
                $sessionRow = $this->_sessionFactory->create()->load(@$arrayInformation['session_id']);
                $oldRevenue = floatval($sessionRow->getRevenue());
                $oldRevenue += $prices;
                $sessionRow->setRevenue($oldRevenue)->save();
                for ($i = 0; $i < $number; $i++) {
                    /** @var array $ticketData */
                    $ticketData['code'] = $this->_helperEvent->generateCode();
                    $ticketData['redemption_code'] = $this->_helperEvent->generateRedemptionCode();
                    $model = $this->_ticketFactory->create();
                    $model->setData($ticketData)->save();
                }

                try {
                    $modelTicket = $this->_ticketFactory->create()->getCollection()
                        ->addFieldToFilter('event_id', $event->getId())
                        ->addFieldToFilter('product_id', $orderItem->getProductId())
                        ->addFieldToFilter('order_increment_id', $order->getIncrementId());
                    foreach ($modelTicket as $ticketMail) {
                        $this->_ticketFactory->create()->sendMail($ticketMail->getTicketId());
                    }
                } catch (\Exception $exception) {
                    $this->messageManager->addErrorMessage($exception->getMessage());
                }
                $configQty = $this->_scopeConfig->getValue(self::XML_PATH_QTY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if ($configQty == 2) {
                    $session = $this->_sessionFactory->create()->load(@$arrayInformation['session_id']);
                    if($session->getId()){
                        $session->setQtyAvailable($session->getQtyAvailable()-$qty);
                        $session->setQtyPurchased($session->getQtyPurchased()+$qty);
                        $session->save();
                    }
                }
            }
        }

        return;
    }
}
