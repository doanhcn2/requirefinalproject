<?php
/**
 * Created by PhpStorm.
 * User: gialam
 * Date: 7/26/2016
 * Time: 1:39 PM
 */

namespace Magenest\Ticket\Observer\Option;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart as CurrentCart;

/**
 * Class Cart
 * @package Magenest\Ticket\Observer\Option
 */
class Cart implements ObserverInterface
{
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $session;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $location;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $tickets;

    /**
     * @var $cart CurrentCart
     */
    protected $cart;

    /**
     * Cart constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magenest\Ticket\Model\EventLocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\SessionFactory $sessionFactory
     * @param \Magenest\Ticket\Model\TicketsFactory $ticketsFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param CurrentCart $cart
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Ticket\Model\EventLocationFactory $locationFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        CurrentCart $cart
    )
    {
        $this->cart = $cart;
        $this->_productRepository = $productRepository;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->location = $locationFactory;
        $this->session = $sessionFactory;
        $this->tickets = $ticketsFactory;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $product = $item->getProduct();
        $productId = $product->getId();
        $data = $this->_request->getParams();
        $checkTypeProduct = $this->_productRepository->getById($productId)->getTypeId();
        if ($checkTypeProduct == 'ticket') {
            if (!@$data['ticket'] && !@$data['ticket_session'])
            {
                $optionsByCode = $product->getCustomOption('info_buyRequest')->getData();
                $data = json_decode($optionsByCode['value'], true);
            }
            if (!empty($data['additional_options'])) {
                $options = $data['additional_options'];
                $additionalOptions = [];
                if (isset($options['ticket']) && (!empty($options['ticket']))) {
                    $ticketModel = $this->tickets->create()->load($options['ticket']);

                    $additionalOptions[] = array(
                        'label' => 'Option',
                        'value' => $ticketModel->getTitle()
                    );
                }

                if (isset($options['ticket_session']) && (!empty($options['ticket_session']))) {
                    $ticketModel = $this->session->create()->load($options['ticket_session']);
                    $additionalOptions[] = array(
                        'label' => 'Session',
                        'value' => $this->convertTime($ticketModel->getSessionFrom()) . ' - ' . $this->convertTime($ticketModel->getSessionTo())
                    );
                }

                if (isset($data['additional_options']['To'])) {
                    $additionalOptions[] = [
                        'label' => 'Gift\'s Recipient Name',
                        'value' => $data['additional_options']['To'],
                    ];
                }
                if (isset($data['additional_options']['Email'])) {
                    $additionalOptions[] = [
                        'label' => 'Gift\'s Recipient Email',
                        'value' => $data['additional_options']['Email'],
                    ];
                }
                if (isset($data['additional_options']['Message'])) {
                    $additionalOptions[] = [
                        'label' => 'Gift\'s Message',
                        'value' => $data['additional_options']['Message'],
                    ];
                }

                if (isset($options['ticket_seat_label']) && !empty($options['ticket_seat_label'])) {
                    $additionalOptions[] = array(
                        'label' => 'Seat',
                        'value' => $options['ticket_seat_label']
                    );
                }

                $item->addOption(array(
                    'code' => 'additional_options',
                    'value' => json_encode($additionalOptions)
                ));

                if (!empty($data['additional_options']['ticket_price']) || $data['additional_options']['ticket_price'] == 0) {
                    $item->setOriginalCustomPrice($options['ticket_price']);
                }
            }

            if (isset($data['additional_options']['ticket_seat_label']) && !empty($data['additional_options']['ticket_seat_label']) && $this->cart->getItems()) {
                $allItemsInCart = $this->cart->getItems();
                $addedProductId = $item->getProductId();
                if ($allItemsInCart) {
                    $listCartItems = $allItemsInCart->getItems();
                } else {
                    $listCartItems = [];
                }
                $addedSeatLabel = $data['additional_options']['ticket_seat_label'];
                $addedSeatSession = $data['additional_options']['ticket_session'];

                foreach ($listCartItems as $items) {
                    $productItemsId = $items->getProductId();
                    if ($productItemsId === $addedProductId) {
                        $itemOptions = $items->getOptionByCode('additional_options')->getData();
                        if ($itemOptions['item_id'] === null) {
                            continue;
                        }
                        $itemOptionsValue = json_decode($itemOptions['value'], true);
                        $itemCodeLabel = '';
                        $itemCodeSession = '';
                        foreach ($itemOptionsValue as $options) {
                            if ($options['label'] === 'ticket_seat_label') {
                                $itemCodeLabel = $options['value'];
                            }
                            if ($options['label'] === 'ticket_session') {
                                $itemCodeSession = $options['value'];
                            }
                        }
                        if ($itemCodeLabel === $addedSeatLabel && $itemCodeSession === $addedSeatSession) {
                            throw new \Magento\Framework\Exception\AlreadyExistsException(__('You added this seat before, please choose another.'));
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $time
     * @return false|string
     */
    public
    function convertTime($time)
    {
        $strTime = strtotime($time);

        return date('d-m-Y H:i:s', $strTime);
    }
}
