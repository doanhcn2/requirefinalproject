<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Observer\Option;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Groupon\Helper\Data;
use Magenest\Ticket\Model\Event;
use Psr\Log\LoggerInterface;

class UpdateGift implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var $_locationFactory \Magenest\Groupon\Model\LocationFactory
     */
    protected $_locationFactory;

    /**
     * @var \Magenest\Ticket\Model\SessionFactory
     */
    protected $_session;

    /**
     * @var \Magenest\Ticket\Model\TicketsFactory
     */
    protected $_tickets;

    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magenest\Groupon\Model\LocationFactory $locationFactory,
        \Magenest\Ticket\Model\TicketsFactory $ticketsFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory
    ) {
        $this->_tickets = $ticketsFactory;
        $this->_session = $sessionFactory;
        $this->_locationFactory = $locationFactory;
        $this->_productRepository = $productRepository;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getEvent()->getQuoteItem();
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $item->getProduct();
            $productId = $product->getId();
            $optionsByCode = $product->getCustomOption('info_buyRequest')->getData();
            $data = json_decode($optionsByCode['value'], true);
            $productType = $product->getTypeId();
            if (Data::isDeal($productId)) {
                if (isset($data['additional_options'])) {
                    $additionalOptions = [];
                    if (isset($data['additional_options']['coupon_selected_children'])) {
                        $additionalOptions[] = [
                            'label' => 'Deal Option',
                            'value' => $this->_productRepository->getById((int)$data['additional_options']['coupon_selected_children'])->getName(),
                        ];
                    }
                    if (isset($data['additional_options']['To'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Name',
                            'value' => $data['additional_options']['To'],
                        ];
                    }
                    if (isset($data['additional_options']['Email'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Email',
                            'value' => $data['additional_options']['Email'],
                        ];
                    }
                    if (isset($data['additional_options']['Message'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Message',
                            'value' => $data['additional_options']['Message'],
                        ];
                    }
                    if (isset($data['additional_options']['redemption_location_id'])) {
                        $additionalOptions[] = [
                            'label' => 'Location',
                            'value' => $this->getGrouponLocation($data['additional_options']['redemption_location_id']),
                        ];
                    }
                    $array = [
                        'code' => 'additional_options',
                        'value' => json_encode($additionalOptions)
                    ];
                    $item->addOption($array);
                }
            }

            if ($productType == Event::PRODUCT_TYPE) {
                if (!empty($data['additional_options'])) {
                    $options = $data['additional_options'];
                    $additionalOptions = [];
                    if (isset($options['ticket']) && (!empty($options['ticket']))) {
                        $ticketModel = $this->_tickets->create()->load($options['ticket']);

                        $additionalOptions[] = array(
                            'label' => 'Option',
                            'value' => $ticketModel->getTitle()
                        );
                    }

                    if (isset($options['ticket_session']) && (!empty($options['ticket_session']))) {
                        $ticketModel = $this->_session->create()->load($options['ticket_session']);
                        $additionalOptions[] = array(
                            'label' => 'Session',
                            'value' => $this->convertTime($ticketModel->getSessionFrom()) . ' - ' . $this->convertTime($ticketModel->getSessionTo())
                        );
                    }

                    if (isset($data['additional_options']['To'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Name',
                            'value' => $data['additional_options']['To'],
                        ];
                    }
                    if (isset($data['additional_options']['Email'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Recipient Email',
                            'value' => $data['additional_options']['Email'],
                        ];
                    }
                    if (isset($data['additional_options']['Message'])) {
                        $additionalOptions[] = [
                            'label' => 'Gift\'s Message',
                            'value' => $data['additional_options']['Message'],
                        ];
                    }

                    $array = array(
                        'code' => 'additional_options',
                        'value' => json_encode($additionalOptions)
                    );

                    $item->addOption($array);

                    if (!empty($data['additional_options']['ticket_price']) || $data['additional_options']['ticket_price'] == 0) {
                        $item->setOriginalCustomPrice($options['ticket_price']);
                    }
                }
            }

            if ($productType == 'coupon') {
                if (isset($data['additional_options']['is_gifted'])) {
                    $additionalOptions[] = [
                        'label' => 'Gift to',
                        'value' => $data['additional_options']['To'],
                    ];
                    $additionalOptions[] = [
                        'label' => 'Recipient Email',
                        'value' => $data['additional_options']['Email'],
                    ];
                    $additionalOptions[] = [
                        'label' => 'Message',
                        'value' => $data['additional_options']['Mesage'],
                    ];
                    $array = [
                        'code' => 'additional_options',
                        'value' => json_encode($additionalOptions)
                    ];
                    $item->addOption($array);
                }

            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->debug('Add to cart exception: ' . $e->getMessage());
        }
    }

    private function getGrouponLocation($locationId)
    {
        $location = $this->_locationFactory->create()->load($locationId);

        $result = "";
        if ($location->getLacationName()) {
            $result .= $location->getLacationName() . " - ";
        }
        $locations = [];
        if ($location->getStreet()) {
            array_push($locations, $location->getStreet());
        } else {
            array_push($locations, $location->getStreetTwo());
        }
        if ($location->getCity()) {
            array_push($locations, $location->getCity());
        }
        if ($location->getRegion()) {
            array_push($locations, $location->getRegion());
        }
        if ($location->getCountry()) {
            $country = ObjectManager::getInstance()->create('Magenest\Ticket\Helper\Event')->getCountryNameByCode($location->getCountry());
            array_push($locations, $country);
        }

        return $result . implode(', ', $locations);
    }

    /**
     * @param $time
     * @return false|string
     */
    public function convertTime($time)
    {
        $strTime = strtotime($time);
        return date('d-m-Y H:i:s', $strTime);
    }
}
