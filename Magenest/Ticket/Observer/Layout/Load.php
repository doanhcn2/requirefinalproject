<?php

namespace Magenest\Ticket\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Block\Product\Context;
use Magenest\Ticket\Model\EventFactory;

/**
 * Class LayoutLoadBeforeFrontend
 * @package Magenest\Reservation\Observer\Layout
 */
class Load implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $eventFactory;

    public function __construct(Context $context,
                                EventFactory $eventFactory)
    {
        $this->eventFactory = $eventFactory;
        $this->_coreRegistry = $context->getRegistry();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->isTicketProduct()) {
            $fullActionName = $observer->getEvent()->getFullActionName();
            /** @var  $layout \Magento\Framework\View\Layout */
            $layout = $observer->getEvent()->getLayout();
            $handler = '';
            if ($fullActionName == 'catalog_product_view' || $fullActionName == 'checkout_cart_configure') {
                $handler = 'catalog_product_view_ticket';
            }
            if ($handler) {
                $layout->getUpdate()->addHandle($handler);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getCurrentProductId()
    {
        $registry = $this->_coreRegistry->registry('current_product');
        if ($registry) {
            $id = $this->_coreRegistry->registry('current_product')->getId();
        } else {
            $id = null;
        }

        return $id;
    }

    private function isTicketProduct()
    {
        $productId = $this->getCurrentProductId();
        if ($productId === null) {
            return false;
        }
        return count($this->eventFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId)->getFirstItem()->getData()) !== 0 ? true : false;
    }
}
