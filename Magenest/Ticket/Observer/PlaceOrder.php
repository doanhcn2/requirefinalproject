<?php
/**
 * Created by PhpStorm.
 * User: gialam
 * Date: 7/26/2016
 * Time: 1:39 PM
 */

namespace Magenest\Ticket\Observer;

use Magenest\Ticket\Model\SessionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Ticket\Model\TicketFactory;

/**
 * Class PlaceOrder
 * @package Magenest\Ticket\Observer
 */
class PlaceOrder implements ObserverInterface
{
    const XML_PATH_QTY = 'event_ticket/general_config/delete_qty';

    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;


    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var SessionFactory
     */
    protected $_sessionFactory;

    /**
     * @var TicketFactory
     */
    protected $ticketFactory;

    /**
     * PlaceOrder constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param TicketFactory $ticketFactory
     * @param SessionFactory $sessionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Cart $cart,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_cart = $cart;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->ticketFactory = $ticketFactory;
        $this->_sessionFactory = $sessionFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderItem = $observer->getEvent()->getOrder();
        foreach ($orderItem->getAllItems() as $item) {
            $qty = $item->getQtyOrdered();
            $productType = $item->getProductType();
            $productOption = $item->getProductOptions();
            if ($productType == 'ticket') {
                $configQty = $this->_scopeConfig->getValue('event_ticket/general_config/delete_qty');
                if ($configQty == 1 && array_key_exists('additional_options', $productOption['info_buyRequest'])) {
                    $session = $this->_sessionFactory->create()->load($productOption['info_buyRequest']['additional_options']['ticket_session']);
                    if ($session) {
                        $session->setQtyAvailable($session->getQtyAvailable() - $qty);
                        $session->setQtyPurchased($session->getQtyPurchased() + $qty);
                        $session->save();
                    }
                }
            }
        }
    }
}
