<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Helper;

use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\ScopeInterface;

/**
 * Checkout default helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Event extends AbstractHelper
{

    const XML_PATH_TICKET_PATTERN_CODE = 'event_ticket/general_config/pattern_code';

    const XML_PATH_TICKET_REDEMPTION_PATTERN_CODE = 'event_ticket/general_config/redemption_code';

    /**
     *
     * @var \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var $_locationFactory \Magenest\Ticket\Model\EventLocationFactory
     */
    protected $_locationFactory;

    /**
     * @var $_countryFactory \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var $_urlInterface \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;

    /**
     * Event constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param TicketFactory $ticketFactory
     * @param \Magenest\Ticket\Model\EventLocationFactory $locationFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\UrlInterface $urlInterface
     */
    public function __construct(
        Context $context,
        EventFactory $eventFactory,
        TicketFactory $ticketFactory,
        \Magenest\Ticket\Model\EventLocationFactory $locationFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        parent::__construct($context);
        $this->_urlInterface = $urlInterface;
        $this->_countryFactory = $countryFactory;
        $this->_locationFactory = $locationFactory;
        $this->_scopeConfig = $this->scopeConfig;
        $this->_eventFactory = $eventFactory;
        $this->_ticketFactory = $ticketFactory;
    }

    /**
     * Generate code
     *
     * @return mixed
     */
    public function generateCode()
    {
        $gen_arr = [];

        $pattern = $this->_scopeConfig->getValue(self::XML_PATH_TICKET_PATTERN_CODE, ScopeInterface::SCOPE_STORE);
        if (!$pattern) {
            $pattern = '[A2][N1][A2]Magenest[N1][A1]';
        }

        preg_match_all("/\[[AN][.*\d]*\]/", $pattern, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $delegate = substr($match [0], 1, 1);
            $length = substr($match [0], 2, strlen($match [0]) - 3);
            $gen = '';
            if ($delegate == 'A') {
                $gen = $this->generateString($length);
            } elseif ($delegate == 'N') {
                $gen = $this->generateNum($length);
            }

            $gen_arr [] = $gen;
        }
        foreach ($gen_arr as $g) {
            $pattern = preg_replace('/\[[AN][.*\d]*\]/', $g, $pattern, 1);
        }
        return $pattern;
    }

    /**
     * Generate redemption code
     *
     * @return mixed
     */
    public function generateRedemptionCode()
    {
        $gen_arr = [];

        $pattern = $this->_scopeConfig->getValue(self::XML_PATH_TICKET_REDEMPTION_PATTERN_CODE, ScopeInterface::SCOPE_STORE);
        if (!$pattern) {
            $pattern = '[A2][N1][A2]Magenest[N1][A1]';
        }

        preg_match_all("/\[[AN][.*\d]*\]/", $pattern, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $delegate = substr($match [0], 1, 1);
            $length = substr($match [0], 2, strlen($match [0]) - 3);
            $gen = '';
            if ($delegate == 'A') {
                $gen = $this->generateString($length);
            } elseif ($delegate == 'N') {
                $gen = $this->generateNum($length);
            }

            $gen_arr [] = $gen;
        }
        foreach ($gen_arr as $g) {
            $pattern = preg_replace('/\[[AN][.*\d]*\]/', $g, $pattern, 1);
        }
        return $pattern;
    }

    /**
     * Generate String
     *
     * @param $length
     * @return string
     */
    public function generateString($length)
    {
        if ($length == 0 || $length == null || $length == '') {
            $length = 5;
        }
        $c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $rand = '';
        for ($i = 0; $i < $length; $i++) {
            $rand .= $c [rand(0, 51)];
        }

        return $rand;
    }

    /**
     * Generate Number
     *
     * @param $length
     * @return string
     */
    public function generateNum($length)
    {
        if ($length == 0 || $length == null || $length == '') {
            $length = 5;
        }
        $c = "0123456789";
        $rand = '';
        for ($i = 0; $i < $length; $i++) {
            $rand .= $c [rand(0, 9)];
        }
        return $rand;
    }

    /**
     * Check Event
     *
     * @param $id
     * @return bool
     */
    public function isEvent($id)
    {
        $model = $this->_eventFactory->create();
        $collection = $model->getCollection()->addFilter('product_id', $id);
//            ->addFilter('enable', 1, 'and');
        if ($collection->getSize() > 0) {
            return $collection->getFirstItem()->getId();
        }
        return false;
    }

    /**
     * Get Ticket
     *
     * @param $id
     * @return bool
     */
    public function getTicket($id)
    {
        $model = $this->_ticketFactory->create();
        $collection = $model->getCollection()->addFilter('order_item_id', $id);
        if ($collection->getSize() > 0) {
            return $collection->getFirstItem();
        }
        return false;
    }

    /**
     * @param $productId
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public static function getAllTicketAndSession($productId)
    {
        /** @var $ticketsModel \Magenest\Ticket\Model\Tickets */
        $ticketsModel = ObjectManager::getInstance()->create('Magenest\Ticket\Model\Session');
        $ticketsCollection = $ticketsModel->getCollection();
        $ticketsCollection->addFieldToFilter('main_table.product_id', $productId);
        $ticketsCollection->getSelect()->joinLeft(['ticket_table' => $ticketsCollection->getTable('magenest_ticket_tickets')], 'main_table.tickets_id=ticket_table.tickets_id', ['type', 'title']);
        $ticketsCollection->setOrder('main_table.session_from', 'asc');
        return $ticketsCollection;
    }

    public static function getTicketInfo($productId)
    {
        $allSession = \Magenest\Ticket\Helper\Event::getAllTicketAndSession($productId);
        $firstSession = $allSession->getFirstItem();
        $data = [];
        $time = $firstSession->getData('session_from');
        $data['first_session'] = $time;
        $weekday = date('D', strtotime($time));
        $day = date('j', strtotime($time));
        $month = date('M', strtotime($time));
        $data['week_day'] = $weekday;
        $data['day'] = $day;
        $data['month'] = $month;
        $countSessions = [];
        foreach ($allSession as $session) {
            $data['sessions'][] = $session->getSessionFrom();
            if ($session->getData('tickets_id') != '') {
                if (isset($countSessions[$session->getData('tickets_id')])) {
                    $countSessions[$session->getData('tickets_id')]++;
                } else {
                    $countSessions[$session->getData('tickets_id')] = 0;
                }
            }
        }
        $data['more_sessions'] = [];
        foreach ($data['sessions'] as $session) {
            $q = strtotime($session);
            $w = date('Y/m/d', $q);
            $date = strtotime($w);
            $numberDate = count($data['more_sessions']);
            if ($numberDate == 0 || $date != $data['more_sessions'][$numberDate - 1])
                $data['more_sessions'][] = $date;
        }
        if (count($data['more_sessions']) >= 1)
            array_splice($data['more_sessions'], 0, 1);
        $data['number_session'] = count($data['more_sessions']);
        return $data;
    }

    public function getTicketLocation($productId)
    {
        $event = $this->_eventFactory->create()->load($productId, 'product_id');
        if ($event->getIsOnline() === '1') {
            $location = $this->_locationFactory->create()->load($productId, 'product_id');
            $result = "";
            if ($location->getState()) {
                $result .= $location->getState();
            } elseif ($location->getCity()) {
                $result .= $location->getCity();
            }
            $result .= ", ";
            $country = $this->_countryFactory->create()->loadByCode($location->getCountry());
            $result .= $country->getName();
            if ($country->getName()) {
                return $result;
            } else {
                return "";
            }
        } else {
            return "Online Event";
        }
    }

    public function isFreeTicketType($productId)
    {
        if ($this->_eventFactory->create()->load($productId, 'product_id')->getTicketType() === 'free') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get location string by: [location_name] - [city], [state], [country]
     * @param $productId
     * @return string
     */
    public function getLocationString($productId)
    {
        $_eventLocation = $this->_locationFactory->create()->load($productId, 'product_id');
        $ticketLocation = "";
        if (@$_eventLocation->getVenueTitle() || trim($_eventLocation->getVenueTitle()) !== '') {
            $ticketLocation .= $_eventLocation->getVenueTitle() . " - ";
        }

        $locations = [];
        if (@$_eventLocation->getCity() || trim($_eventLocation->getCity()) !== '') {
            array_push($locations, $_eventLocation->getCity());
        }
        if (@$_eventLocation->getState() || trim($_eventLocation->getState()) !== '') {
            array_push($locations, $_eventLocation->getState());
        }
        if (@$_eventLocation->getCountry() || trim($_eventLocation->getCountry()) !== '') {
            $country = $this->_countryFactory->create()->loadByCode($_eventLocation->getCountry());
            array_push($locations, $country->getName());
        }

        return $ticketLocation . implode(', ', $locations);
    }

    public function getLocationStringKey($productId)
    {
        $_eventLocation = $this->_locationFactory->create()->load($productId, 'product_id');
        $ticketLocation = "";
        if (@$_eventLocation->getState()) {
            $ticketLocation .= $_eventLocation->getState() . "||";
        } elseif (@$_eventLocation->getCity()) {
            $ticketLocation .= $_eventLocation->getCity() . "||";
        }
        if (@$_eventLocation->getCountry()) {
            $ticketLocation .= $_eventLocation->getCountry();
        }

        return $ticketLocation;
    }

    public function getCountryNameByCode($code = null)
    {
        if ($code) {
            $country = $this->_countryFactory->create()->loadByCode($code);
            return $country->getName();
        }
        return "";
    }

    public function getLocationFilterLabel($locations)
    {
        $location = explode('||', $locations);
        if (!isset($location[0]) || !isset($location[1])) {
            return "";
        }
        $country = $this->getCountryNameByCode($location[1]);

        return $location[0] . ", " . $country;
    }

    public function getClearLinkUrl($filter)
    {
        $url = $this->_urlInterface->getCurrentUrl();
        $currentBaseUrl = explode('?', $url);
        if (!$this->_getRequest()->getParam('locations') || !isset($currentBaseUrl[0])) {
            return '#';
        }

        $params = $this->_getRequest()->getParams();
        unset($params['id']);
        $locations = @$params['locations'];
        $newLocations = [];
        $location = explode('>', $locations);
        foreach($location as $item) {
            if ($item !== $filter) {
                array_push($newLocations, $item);
            }
        }
        if (count($newLocations) == 0) {
            unset($params['locations']);
        } else {
            $params['locations'] = implode('>', $newLocations);
        }

        if (count($params) === 0) {
            return $currentBaseUrl[0];
        } else {
            return $currentBaseUrl[0] . "?" . http_build_query($params);
        }
    }

    public function getClearAllUrl()
    {
        $url = $this->_urlInterface->getCurrentUrl();
        $currentBaseUrl = explode('?', $url);

        return isset($currentBaseUrl[0]) ? $currentBaseUrl[0] : false;
    }
}
