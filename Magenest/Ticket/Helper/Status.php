<?php

namespace Magenest\Ticket\Helper;

class Status extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var
     */
    protected $_templateId;

    /**
     * Status constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->_scopeConfig = $context;
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
    }

    /**
     * @return mixed
     */
    public function getCurrentTime(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        return $objDate->date();
    }

    /**
     * @return mixed
     */
    public function getTicketSessionsCollection(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $ticketSessionsCollection = $objectManager->create('Magenest\Ticket\Model\ResourceModel\Session\Collection');
        $ticketSessionsCollection->addFieldToSelect('*');
        $ticketSessionsCollection->addFieldToFilter('session_to', array('like' => $this->getCurrentTime()->format('Y-m-d') . '%'));
        return $ticketSessionsCollection;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTicketBySessionId($sessionId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $ticketCollection = $objectManager->create('Magenest\Ticket\Model\ResourceModel\Ticket\Collection');
        $ticketCollection->addFieldToSelect('*');
        $ticketCollection->addFieldToFilter('session_id', array('eq' => $sessionId));
        return $ticketCollection;
    }

    /**
     *
     */
    public function changeStatus(){
        $ticketSessionsCollection = $this->getTicketSessionsCollection();
        foreach ($ticketSessionsCollection as $ticketSession){
            if(strtotime($ticketSession->getData('session_to')) <= strtotime($this->getCurrentTime()->format('Y-m-d H:i:s'))){
                $ticketCollection = $this->getTicketBySessionId($ticketSession->getData('session_id'));
                if($ticketCollection != null){
                    foreach ($ticketCollection as $ticket){
                        $ticket->setData('status', 5);
                        $ticket->save();
                    }
                }
            }
        }
    }

}