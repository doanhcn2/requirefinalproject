<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Ticket\Helper;

use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\EventLocationFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magenest\Ticket\Model\TicketFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magenest\Ticket\Model\Ticket;
use Magento\Framework\App\ObjectManager;

/**
 * Class Information
 *
 * @package Magenest\Ticket\Helper
 */
class Information extends AbstractHelper
{
    protected $event;
    protected $location;
    protected $ticket;
    protected $session;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\Collection */
    protected $creditmemoItemCollectionFactory;

    /**
     * @var $_productCollection \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollection;
    protected $_ticketBuildedFactory;

    /**
     * Information constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param EventLocationFactory $locationFactory
     * @param TicketsFactory $ticketFactory
     * @param SessionFactory $sessionFactory
     */
    public function __construct(
        Context $context,
        EventFactory $eventFactory,
        EventLocationFactory $locationFactory,
        TicketsFactory $ticketFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\CollectionFactory $creditmemoItemCollectionFactory,
        TicketFactory $ticketBuildedFactoy,
        SessionFactory $sessionFactory
    ) {
        $this->event = $eventFactory;
        $this->_ticketBuildedFactory = $ticketBuildedFactoy;
        $this->location = $locationFactory;
        $this->creditmemoItemCollectionFactory = $creditmemoItemCollectionFactory;
        $this->_productCollection = $collectionFactory;
        $this->ticket = $ticketFactory;
        $this->session = $sessionFactory;
        parent::__construct($context);
    }

    /**
     * @param $options
     * @return array
     */
    public function getAll($options)
    {
//        $modelTicket = $this->ticket->create()->load($options['ticket']);
        $array = [
            'tickets_id' => $options['ticket'],
            'session_id' => $options['ticket_session']
        ];
        if (array_key_exists('ticket_seat_label', $options)) {
            $array['ticket_seat_label'] = $options['ticket_seat_label'];
        }
        return $array;
    }

    /**
     * @param $id
     * @return array
     */
    public static function getSession($productId)
    {
        $sessionCollection = ObjectManager::getInstance()
            ->create(\Magenest\Ticket\Model\ResourceModel\Session\Collection::class);
        $sessionModel = $sessionCollection->addFieldToFilter('product_id', $productId)->getFirstItem();

        $start = date('d-m-Y H:i:s', strtotime($sessionModel->getSessionFrom()));
        $end = date('d-m-Y H:i:s', strtotime($sessionModel->getSessionTo()));

        return [
            "from" => $start,
            "to" => $end
        ];
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getTicketCollectionSaleToday()
    {
        $date = new \DateTime();
        $today = $date->format('Y-m-d');
        return $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('created_at', array('like' => "%$today%"));
    }

    public function getSoldFromDay($key)
    {
        $day = $this->getDay($key);
        $tickets = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter("vendor_id", $this->getVendorId())
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $tickets;
    }

    public function getSoldInDay($collection)
    {
        $arr = [];
        foreach ($collection->getColumnValues('created_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getSoldInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $ticket) {
            $key = explode(' ', $ticket->getCreatedAt())[0];
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->session->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $sold = $session->getSalePrice();
            else
                $sold = $session->getOriginalPrice();
            if (!isset($arr[$key]))
                $arr[$key] = $sold;
            else
                $arr[$key] += $sold;
        }
        return $arr;
    }

    public function getRefundedInDay($collection)
    {
        $arr = [];
        foreach ($collection->getColumnValues('refunded_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getRefundedInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $ticket) {
            $key = explode(' ', $ticket->getRefundedAt())[0];
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->session->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $refund = $session->getSalePrice();
            else
                $refund = $session->getOriginalPrice();
            if (!isset($arr[$key])) {
                $arr[$key] = $refund;
            } else {
                $arr[$key] += $refund;
            }
        }
        return $arr;
    }

    public function getDay($daynum)
    {
        $date = new \DateTime();
        if ($daynum === "all_time") {
            $startTime = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor()->getCreatedAt();
            if ($startTime === null) {
                $startTime = '2018-01-01 00:00:00';
            }
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);
            if (time() - $startTime->getTimestamp() < 86400) {
                $newTime = $startTime->sub(new \DateInterval('P7D'));
                return $newTime->format('Y-m-d');
            }

            return $startTime->format('Y-m-d');
        } elseif ($daynum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));

            return $startTime->format('Y-m-d');
        } else {
            $date->sub(new \DateInterval('P' . $daynum . 'D'));
        }

        return $date->format('Y-m-d');
    }

    public function getOption($daynum)
    {
        $arr = [];
        $daystart = new \DateTime();
        if (is_integer($daynum)) {
            $dayCount = (int)$daynum;
        } else {
            $dayCount = floor((time() - strtotime($this->getDay($daynum))) / 86400);
            $dayCount = (int)$dayCount;
        }
        $date = $daystart->sub(new \DateInterval('P' . $dayCount . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $soldInDays = $this->getSoldInDay($this->getSoldFromDay($dayCount));
        $soldInDaysValue = $this->getSoldInDayValue($this->getSoldFromDay($dayCount));;
        $refundInDays = $this->getRefundedInDay($this->getRefundedFromDay($dayCount));
        $refundInDaysValue = $this->getRefundedInDayValue($this->getRefundedFromDay($dayCount));
        for ($i = 0; $i < $dayCount; $i++) {
            $date->add(new \DateInterval('P1D'));
            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [$dateText, $sales, $qtySale, $refunds, $qtyRefund, 'color: #017cb9; stroke-color: #017cb9'];
        }
        return [$startDateText, $dateText, $arr];
    }


    public function getSaleValue($key)
    {
        $tickets = $this->getSoldFromDay($key);
        $total = 0;
        foreach ($tickets as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->session->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }
        return $total;
    }

    public function getRedeemedFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('redeemed_at', array('gt' => $day));
        return $collection;
    }

    public function getRefundedFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('refunded_at', array('gt' => $day));

        return $collection;
    }

    public function getRefundedValueFromDay($daynum)
    {
        $ticketsRefunded = $this->getRefundedFromDay($daynum);
        $total = 0;
        foreach ($ticketsRefunded as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->session->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }

        return $total;
    }


    public function getTodaySale()
    {
        $tickets = $this->getTicketCollectionSaleToday();
        $total = 0;
        foreach ($tickets as $ticket) {
            $sessionId = unserialize($ticket->getInformation())['session_id'];
            $session = $this->session->create()->load($sessionId);
            if ($session->getSalePrice() > 0)
                $total += $session->getSalePrice();
            else
                $total += $session->getOriginalPrice();
        }
        return $total;
    }

    public function getTodayRedeemed()
    {
        $tickets = $this->getTicketCollectionSaleToday()->addFieldToFilter('status', 2);

        return count($tickets);
    }

    public function getRedeemed($ids)
    {
        $tickets = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter("status", Ticket::STATUS_USED)->addFieldToFilter('product_id', ['in' => $ids])->getAllIds();

        return count($tickets);
    }

    public function getRemaining($ids)
    {
        $tickets = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter("status", Ticket::STATUS_AVAILABLE)->addFieldToFilter('product_id', ['in' => $ids])->getAllIds();

        return count($tickets);
    }

    public function getActiveCampaignCollection()
    {
        return $this->getEventProductCollection()->addFieldToFilter("campaign_status", 5);
    }

    public function getActiveCampaignCollectionIds()
    {
        return $this->getActiveCampaignCollection()->getAllIds();
    }

    public function getSold($ids)
    {
        if (count($ids) == 0) return 0;
        $tickets = $this->_ticketBuildedFactory->create()->getCollection()->addFieldToFilter('product_id', ['in' => $ids])->getAllIds();
        return count($tickets) - $this->getRefunded($ids);
    }

    public function getRefunded($ids)
    {
        $tickets = $this->_ticketBuildedFactory->create()->getCollection()
            ->addFieldToFilter("status", Ticket::STATUS_REFUNDED)->addFieldToFilter('product_id', ['in' => $ids])->getAllIds();

        return count($tickets);
    }

    public function getEventProductCollection()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productCollection->create();
        $collection->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->addFieldToFilter('type_id', 'ticket')->addAttributeToSelect(['campaign_status', 'image']);
        $eventTable = $collection->getTable('magenest_ticket_event');
        $joinStatement = $collection->getSelect()->join(
            $eventTable,
            "entity_id=$eventTable.product_id",
            ["$eventTable.event_id", "$eventTable.event_name", "$eventTable.is_reserved_seating", "$eventTable.venue_map_id"]
        );

        $collection->getConnection()->fetchAll($joinStatement);
        return $collection;
    }

    public function getSoldOutOption()
    {
        $productIds = $this->getEventProductCollection()->getAllIds();
        if (count($productIds) == 0) return 0;
        $tickets = $this->ticket->create()->getCollection()->addFieldToFilter('product_id', $productIds);
        $sessions = $this->session->create()->getCollection()->addFieldToFilter('tickets_id', $tickets->getAllIds())
            ->addFieldToFilter("qty_available", 0);
        return count($sessions);
    }
}
