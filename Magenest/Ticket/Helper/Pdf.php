<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Ticket\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Ticket\Model\EventFactory;
use Magenest\Ticket\Model\TicketsFactory;
use Magenest\Ticket\Model\EventLocationFactory;
use Magenest\Ticket\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\View\Element\Template;

/**
 * Class Pdf
 *
 * @package Magenest\Ticket\Helper
 */
class Pdf extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * @var File
     */
    protected $fileFramework;

    /**
     * @var Template
     */
    protected $template;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * @var EventLocationFactory
     */
    protected $location;

    /**
     * @var Information
     */
    protected $information;

    /**
     * @var TicketsFactory
     */
    protected $ticketInfoFactory;
    /**
     * Pdf constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param EventFactory $eventFactory
     * @param File $fileFramework
     * @param Template $template
     * @param SessionFactory $eventSessionFactory
     * @param EventLocationFactory $locationFactory
     * @param Information $information
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        EventFactory $eventFactory,
        TicketsFactory $ticketsFactory,
        File $fileFramework,
        Template $template,
        SessionFactory $eventSessionFactory,
        EventLocationFactory $locationFactory,
        Information $information
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->_eventFactory = $eventFactory;
        $this->fileFramework = $fileFramework;
        $this->template = $template;
        $this->session = $eventSessionFactory;
        $this->location = $locationFactory;
        $this->information = $information;
        $this->ticketInfoFactory = $ticketsFactory;
    }

    /**
     * @param \Magenest\Ticket\Model\Ticket $ticket
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     */
    public function getPdf($ticket)
    {
        $pdf = new \Zend_Pdf();
        $ticketData = unserialize($ticket->getInformation());
        $event = $ticket->getEvent();
        $ticketInfo = $this->ticketInfoFactory->create()->load($ticketData['tickets_id']);
        $session = $this->session->create()->load($ticketData['session_id']);
        $location = $this->location->create()->getCollection()->addFieldToFilter('event_id',$event->getEventId());
        if($location->count()>0)
            $location = $location->getFirstItem();
        else
            $location = null;
        $font_regular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $font_bold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $width = 890;
        $height = 850;

        $size = $width. ':'. $height;
        $page = $pdf->newPage($size);
        /**
         * Background
         */
            $background = $this->filesystem->getDirectoryWrite(DirectoryList::ROOT)
                ->getAbsolutePath('app/code/Magenest/Ticket/view/frontend/web/images/ticket_template.png');
            if (is_file($background)) {
                $image = \Zend_Pdf_Image::imageWithPath($background);
                $page->drawImage($image, 0, 0, $width, $height);
            }

        $code = $ticket->getCode();
        $page->setFont($font_regular, 15);

        /**
         * Insert Barcode to PDF File
         */
            $barcodeOptions = ['text' => $code, 'drawText' => false];
            $rendererOptions = [];
            $imageResource = \Zend_Barcode::draw('code128', 'image', $barcodeOptions, $rendererOptions);
            $barcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('barcode.jpg');
            imagejpeg($imageResource, $barcode, 100);
            imagedestroy($imageResource);
            $image = \Zend_Pdf_Image::imageWithPath($barcode);
            $page->drawImage(
                $image,
                30,
                440,
                30 + 100*2,
                440 + 100
            );
            unlink($barcode);
        /**
         * Code
         */
        $textWidth = $this->getTextWidth($code, $font_regular, 9);
        $page->drawText($code,
            (200 / 2) - ($textWidth / 2),
            420);
        $page->setFont($font_regular, 13);

        $textWidth = $this->getTextWidth("#".$code, $font_regular, 13);
        $page->drawText("#".$code,
            870 - $textWidth - 13,
            800);
        /**
         * Event Name
         */
        $page->setFont($font_bold, 23);
        $page->drawText($event->getEventName(),
            40,
            730,
            'UTF-8'
        );

        /**
         * Ticket Type Title
         */
        $page->setFont($font_regular, 17);
        $page->drawText($ticketInfo->getTitle(),
            40,
            700,
            'UTF-8'
        );

        /**
         * Description
         */
        $noteHeight = 670;
        $page->setFont($font_regular, 15);
        $description = wordwrap($ticketInfo->getDescription(), 65, "\n");
        foreach (explode("\n", $description) as $text) {
            $page->drawText(strip_tags(ltrim($text)), 40, $noteHeight, 'UTF-8');
            $noteHeight -= 18;
        }

        /**
         * Customer Name
         */
        $page->setFont($font_regular, 15);
        $page->drawText($ticket->getCustomerName(),
            310,
            480,
            'UTF-8'
        );

        /**
         * Valid from
         */
        $page->setFont($font_regular, 15);
        $page->drawText($session->getSessionFrom(),
            325,
            460,
            'UTF-8'
        );

        /**
         * Exprires date
         */
        $page->setFont($font_regular, 15);
        $page->drawText($session->getSessionTo(),
            320,
            440,
            'UTF-8'
        );

        /**
         * Location Detail
         */
        if($location) {
            $page->setFont($font_regular, 15);
            //Venue Name
            $page->drawText($location->getVenueTitle(),
                550,
                650,
                'UTF-8'
            );

            //Address1
            $page->drawText('Address: ' . $location->getAddress(),
                550,
                630,
                'UTF-8'
            );
            //Address2
            $line = 610;
            if ($location->getData('address2')) {
                $page->drawText('Address 2: ' . $location->getData('address2'),
                    550,
                    $line,
                    'UTF-8'
                );
                $line -= 20;
            }

            //City , State , Country
            $page->drawText($location->getCity() . ", " . $location->getState() . ", " . $location->getCountry(),
                550,
                $line,
                'UTF-8'
            );
        }
        /**
         * Term
         */
        $line = 250;
        $page->setFont($font_regular, 13);

        if ($event->getShowUp() == 1){

            $page->drawText('* Must Show up before session start ' . $event->getShowUpTime() . ' minutes before start of session',
                70,
                $line,
                'UTF-8'
            );
            $line -= 15;
        }

        if ($event->getCanPurchase() == 1){
            $page->drawText('* Additional Tickets can be purchased at venue (full price and upon availability)',
                70,
                $line,
                'UTF-8'
            );
            $line -= 15;
        }
        if ($event->getMustPrint() == 1){
            $page->drawText('* Tickets must be printed',
                70,
                $line,
                'UTF-8'
            );
            $line -= 15;
        }
        if ($event->getAge()){
            $page->drawText('* Age Suitability: ' . $event->getAge(),
                70,
                $line,
                'UTF-8'
            );
        }
        $pdf->pages[] = $page;

        return $pdf;
    }

    /**
     * @param $data
     * @return string
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getPreviewPdf($data)
    {
        $path = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath("template.pdf");

        $pdf = $this->getPrintPdfPreview($data);
        $pdf->render();
        $pdf->save($path);
        $file = $this->template->getBaseUrl()."pub/media/template.pdf";
        return $file;
    }

    function getTextWidth($text, $font, $font_size) {
        $drawing_text = iconv('', 'UTF-8', $text);
        $characters    = array();
        for ($i = 0; $i < strlen($drawing_text); $i++) {
            $characters[] = ord ($drawing_text[$i]);
        }
        $glyphs        = $font->glyphNumbersForCharacters($characters);
        $widths        = $font->widthsForGlyphs($glyphs);
        $text_width   = (array_sum($widths) / $font->getUnitsPerEm()) * $font_size;
        return $text_width;
    }


    /**
     * @param \Magenest\Ticket\Model\Ticket $ticket
     * @param $info
     * @return string
     */
    public function replaceByTicket($ticket, $info)
    {
        $event = $this->_eventFactory->create()->load($ticket->getEventId());

        $array = unserialize($ticket->getInformation());
        $arrayInfo = $this->information->getDataTicket($array);
        $text = '';

        switch ($info) {
            case 'event_name':
                $text = $event->getEventName();
                break;
            case 'location_title':
                $text = $arrayInfo['location_title'];
                break;
            case 'location_detail':
                $text = $arrayInfo['location_detail'];
                break;
            case 'date':
                $text = $arrayInfo['date'];
                break;
            case 'qty':
                $text = $ticket->getQty();
                break;
            case 'start_time':
                $text = $arrayInfo['start_time'];
                break;
            case 'end_time':
                $text = $arrayInfo['end_time'];
                break;
            case 'type':
                $text = $ticket->getNote();
                break;
            case 'code':
                $text = $ticket->getCode();
                break;
            case 'customer_name':
                $text = $ticket->getCustomerName();
                break;
            case 'customer_email':
                $text = $ticket->getCustomerEmail();
                break;
            case 'order_increment_id':
                $text = $ticket->getOrderIncrementId();
                break;
            default:
                break;
        }

        return $text;
    }

    /**
     * Print PDF Template Preview
     *
     * @param array $data
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getPrintPdfPreview($data)
    {
        $coordinates = [];
        if ($data['pdf_coordinates']) {
            $coordinates = unserialize($data['pdf_coordinates']);
        }
        $backgroundLink = unserialize($data['pdf_background']);

        $pdf = new \Zend_Pdf();
        $fontRegular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);

        $width = $data['pdf_page_width'];
        $height = $data['pdf_page_height'];

        $size = $width. ':'. $height;
        $page = $pdf->newPage($size);
        if (isset($backgroundLink) && !empty($backgroundLink)) {
            $background = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('ticket/template/'.$backgroundLink['0']['file']);
            if (is_file($background)) {
                $image = \Zend_Pdf_Image::imageWithPath($background);
                $page->drawImage($image, 0, 0, $width, $height);
            }
        }

        $code = 'MagenestA4vM';
        $page->setFont($fontRegular, 15);
        $tableRowsArr = $coordinates;

        $path = $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        );

        foreach ($tableRowsArr as $param) {
            /**
             * Insert QR Code to PDF File
             */
            if (!empty($param['info']) && $param['info'] == 'qr_code' && !empty($param['x'])  && !empty($param['y'])  && !empty($param['size'])) {
                $fileName = $this->getQrCode($code);
                $pathQrcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($fileName);
                $image = \Zend_Pdf_Image::imageWithPath($pathQrcode);
                $page->drawImage(
                    $image,
                    $param['x'],
                    $param['y'],
                    $param['x'] + $param['size'],
                    $param['y'] + $param['size']
                );

                if ($path->isFile($fileName)) {
                    $this->filesystem->getDirectoryWrite(
                        DirectoryList::MEDIA
                    )->delete($fileName);
                }

                continue;
            }
            /**
             * Insert Barcode to PDF File
             */
            if (!empty($param['info']) && $param['info'] == 'bar_code') {
                $barcodeOptions = ['text' => $code, 'drawText' => false];
                $rendererOptions = [];
                $imageResource = \Zend_Barcode::draw('code128', 'image', $barcodeOptions, $rendererOptions);
                $barcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('barcode.jpg');
                imagejpeg($imageResource, $barcode, 100);
                imagedestroy($imageResource);
                $image = \Zend_Pdf_Image::imageWithPath($barcode);
                $page->drawImage(
                    $image,
                    $param['x'],
                    $param['y'],
                    $param['x'] + $param['size']*2,
                    $param['y'] + $param['size']
                );
                if ($path->isFile('barcode.jpg')) {
                    $this->filesystem->getDirectoryWrite(
                        DirectoryList::MEDIA
                    )->delete('barcode.jpg');
                }

                continue;
            }

            /**
             * Insert diffenceinformation
             */
            if (!empty($param['info']) && !empty($param['x'])  && !empty($param['y'])  && !empty($param['size']) && !empty($param['color'])) {
                $page->setFont($fontRegular, $param['size']);
                $color = new \Zend_Pdf_Color_Html($param['color']);
                $page->setFillColor($color);
                $text = $this->replaceByText($param['info'], $data['product_id']);
                if (isset($param['title']) && !empty($param['title'])) {
                    $textEnd = $param['title'].': '.$text;
                } else {
                    $textEnd = $text;
                }
                $page->drawText(
                    $textEnd,
                    $param['x'],
                    $param['y'],
                    'UTF-8'
                );
            }
        }
        $pdf->pages[] = $page;
        return $pdf;
    }

    /**
     * @param $info
     * @param $id
     * @return string
     */
    public function replaceByText($info)
    {
        $text = '';
        switch ($info) {
            case 'event_name':
                $text = 'Event Ticket';
                break;
            case 'location_title':
                $text = 'Thearter';
                break;
            case 'location_detail':
                $text = 'California, USA';
                break;
            case 'date':
                $text = '16/11/2016';
                break;
            case 'start_time':
                $text = '8:00';
                break;
            case 'end_time':
                $text = '11:00';
                break;
            case 'type':
                $text = 'Adult';
                break;
            case 'code':
                $text = 'MagenestA4vM';
                break;
            case 'customer_name':
                $text = 'Magenest JSC';
                break;
            case 'customer_email':
                $text = 'example@gmail.com';
                break;
            case 'order_increment_id':
                $text = '00000026';
                break;
            case 'qty':
                $text = '6';
                break;
            default:
                break;
        }

        return $text;
    }
}
