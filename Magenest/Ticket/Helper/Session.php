<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Ticket extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Ticket
 * @author ThaoPV <thaopw@gmail.com>
 */

namespace Magenest\Ticket\Helper;

use Magenest\Ticket\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Session
 * @package Magenest\Ticket\Helper
 */
class Session extends AbstractHelper
{
    protected $_sessionFactory;

    protected $_ticketPurchasedFactory;

    protected $_eventFactory;

    public function __construct(
        Context $context,
        \Magenest\Ticket\Model\TicketFactory $ticketPurchasedFactory,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magenest\Ticket\Model\SessionFactory $sessionFactory

    )
    {
        $this->_eventFactory = $eventFactory;
        $this->_ticketPurchasedFactory = $ticketPurchasedFactory;
        $this->_sessionFactory = $sessionFactory;
        parent::__construct($context);
    }

    public function getEvent($productId)
    {
        return $this->_eventFactory->create()->getCollection()
            ->addFieldToFilter("product_id", $productId)->getFirstItem();
    }

    public function getEventType($productId)
    {
        return $this->getEvent($productId)->getTicketType();
    }

    public function getSessionStatus($sessionId)
    {
        $session = $this->_sessionFactory->create()->load($sessionId);
        if ($session->getQtyAvailable() === 0)
            $return = [
                'color' => 'F74040',
                'status' => 'Sold Out'
            ];
        else {
            $sessionEnd = strtotime($session->getSessionTo());
            $now = strtotime("now");
            if ($sessionEnd <= $now)
                $return = [
                    'color' => '465a6f',
                    'status' => 'Expired'
                ];
            else {
                $sessionStart = strtotime($session->getSessionFrom());
                if ($sessionStart <= $now)
                    $return = [
                        'color' => '465a6f',
                        'status' => 'Live'
                    ];
                else
                    $return = [
                        'color' => '5AA325',
                        'status' => 'Schedule'
                    ];
            }
        }
        return $return;
    }

    public function getSessionRevenue($sessionId, $productId)
    {
        $session = $this->_sessionFactory->create()->load($sessionId);
        if ($this->getEventType($productId) == "free")
            return 0;
        else {
            if ($session->getSalePrice() > 0)
                $sold = $session->getSalePrice();
            else
                $sold = $session->getOriginalPrice();
            return $session->getQtyPurchased() * $sold;
        }
    }


    public function getTicketsOfSession($sessionId, $productId)
    {
        $tickets = $this->_ticketPurchasedFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId);
        $return = [];
        foreach ($tickets as $ticket) {
            $session = unserialize($ticket->getInformation())['session_id'];
            if ($session == $sessionId)
                $return[] = $ticket;
        }
        return $return;
    }

    public function getQtyRedeemed($sessionId, $productId)
    {
        $tickets = $this->getTicketsOfSession($sessionId, $productId);
        $qtyRedeemed = 0;
        foreach ($tickets as $ticket) {
            if ($ticket->getStatus() == 2)
                $qtyRedeemed++;
        }
        return $qtyRedeemed;
    }

}
