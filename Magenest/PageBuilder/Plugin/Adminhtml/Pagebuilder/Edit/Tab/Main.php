<?php

namespace Magenest\PageBuilder\Plugin\Adminhtml\Pagebuilder\Edit\Tab;

use Magento\Framework\App\ObjectManager;

class Main
{
    public function __construct(
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
    }

    public function aroundGetFormHtml(
        \Ves\PageBuilder\Block\Adminhtml\Pagebuilder\Edit\Tab\Main $subject,
        \Closure $proceed
    )
    {
        $form = $subject->getForm();
        /** @var $model \Ves\PageBuilder\Model\Block */
        $model = $this->_coreRegistry->registry('ves_pagebuilder');
        if (is_object($form)) {
            $fieldset = $form->addFieldset('category_id_select', ['legend' => __('Category')]);
            $fieldset->addField(
                'category_id',
                'select',
                [
                    'name' => 'category_id',
                    'label' => __('Category Id'),
                    'id' => 'category_id',
                    'options' => ObjectManager::getInstance()->get(\Magenest\PageBuilder\Model\Config\Source\CategoryList::class)->toArray(),// \Magenest\PageBuilder\Model\Config\Source\CategoryList::class,
                    'title' => __('Category Id'),
                    'required' => false
                ]
            );
            $form->setValues($model->getData());
            $subject->setForm($form);
        }

        return $proceed();
    }
}