<?php
namespace Magenest\CustomCommission\Observer\Category;

use Magento\Catalog\Model\Category;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class LoadCommissionSetting implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        try {
            $category = $observer->getEntity();
            if ($category instanceof Category) {
                if ($customCommission = $category->getData(SaveCommissionSetting::COMMISSION_KEY)) {
                    $customCommission = \Zend_Json::decode($customCommission);
                    if (!is_array($customCommission)) {
                        return;
                    }
                    foreach ($customCommission as $k => $v) {
                        $category->setData($k, $v);
                    }
                }
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }
}