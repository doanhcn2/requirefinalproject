<?php
namespace Magenest\CustomCommission\Observer\Category;

use Magento\Catalog\Model\Category;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveCommissionSetting implements ObserverInterface
{
    const DEFAULT_COMMISSION_PERCENT_KEY = 'default_commission';
    const DEFAULT_FIXED_RATE_KEY = 'default_fixed_rate';
    const COMMISSION_BY_COUNTRY_KEY = 'commission_by_country';
    const COMMISSION_KEY = 'custom_commission';

    public function execute(Observer $observer)
    {
        try {
            /** @var Category $category */
            $category = $observer->getCategory();
            /** @var RequestInterface $request */
            $request = $observer->getRequest();
            $defaultCommissionPercent = $request->getParam(self::DEFAULT_COMMISSION_PERCENT_KEY);
            $defaultFixedRate = $request->getParam(self::DEFAULT_FIXED_RATE_KEY);
            $commissionByCountry = $request->getParam(self::COMMISSION_BY_COUNTRY_KEY, []);
            if (!$defaultCommissionPercent && !$defaultFixedRate && !$commissionByCountry) {
                $category->setData(self::COMMISSION_KEY, \Zend_Json::encode([]));
                return;
            }
            if ($defaultCommissionPercent || $defaultFixedRate || $commissionByCountry) {
                $data = [
                    self::DEFAULT_COMMISSION_PERCENT_KEY => $defaultCommissionPercent,
                    self::DEFAULT_FIXED_RATE_KEY => $defaultFixedRate,
                    self::COMMISSION_BY_COUNTRY_KEY => $commissionByCountry
                ];
                $category->setData(self::COMMISSION_KEY, \Zend_Json::encode($data));
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }
}