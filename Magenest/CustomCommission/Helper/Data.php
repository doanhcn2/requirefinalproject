<?php
namespace Magenest\CustomCommission\Helper;

use Magenest\CustomCommission\Observer\Category\SaveCommissionSetting;
use Magento\Catalog\Model\Category;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Unirgy\DropshipTierCommission\Helper
 */
class Data extends \Unirgy\DropshipTierCommission\Helper\Data
{
    /**
     * @param $po
     */
    protected function _processPoCommission($po)
    {
        $v = $this->_hlp->getVendor($po->getUdropshipVendor());
        $cFallbackMethod = $this->_hlp->getVendorFallbackField(
            $v, 'tiercom_fallback_lookup', 'udropship/tiercom/fallback_lookup'
        );

//        $tierRates = $po->getUdropshipVendor() ? $this->getTiercomRates($po->getUdropshipVendor()) : [];
//        $globalTierRates = $this->getGlobalTierComConfig();

        $topCats = $this->getTopCategories();
        $catIdsToLoad = $catIds = [];
        $pIds = [];
        foreach ($po->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) continue;
            $pIds[] = $item->getProductId();
        }
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $products */
        $products = $this->_hlp->createObj('\Magento\Catalog\Model\ResourceModel\Product\Collection');
        $products->addIdFilter($pIds);
        if (($tcProdAttr = $this->getCommProductAttribute())) {
            $products->addAttributeToSelect($tcProdAttr->getAttributeCode());
        }
        foreach ($po->getAllItems() as $item) {
            $itemId = spl_object_hash($item);
            if ($item->getOrderItem()->getParentItem() || !($product = $products->getItemById($item->getProductId()))) continue;
            $_catIds = $product->getCategoryIds();
            if (empty($_catIds)) continue;
            reset($_catIds);
            $catIdsToLoad = array_merge($catIdsToLoad, $_catIds);
            $catIds[$itemId] = $_catIds;
        }
        $format = $this->_formatLocale;
        $catIdsToLoad = array_unique($catIdsToLoad);
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $iCats */
        $iCats = $this->_hlp->createObj('\Magento\Catalog\Model\ResourceModel\Category\Collection');
        $iCats->addIdFilter($catIdsToLoad);
        $subcatMatchFlag = true;

        $ratesToUse = [];
        foreach ($po->getAllItems() as $item) {
            $itemId = spl_object_hash($item);
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            if (($product = $products->getItemById($item->getProductId()))
                && $tcProdAttr
                && '' !== $product->getData($tcProdAttr->getAttributeCode())
                && null !== $product->getData($tcProdAttr->getAttributeCode())
            ) {
                $ratesToUse[$itemId]['value'] = $format->getNumber(
                    $product->getData($tcProdAttr->getAttributeCode())
                );
            } elseif (!empty($catIds[$itemId])) {
                $exactMatched = $subcatMatched = false;
                $isGlobalTier = true;
                foreach ($catIds[$itemId] as $iCatId) {
                    if (!($iCat = $iCats->getItemById($iCatId))) continue;
                    $_exactMatched = $_subcatMatched = false;
                    $_isGlobalTier = true;
                    $_exactMatched = $topCats->getItemById($iCatId);
                    $catId = null;
                    $defaultCommissionPercent = 0;
                    $defaultFixedRate = 0;
                    $commissionByCountry = [];
                    $_catPath = explode(',', $this->_catalogHelper->getPathInStore($iCat));
                    foreach ($_catPath as $_catPathId) {
                        $category = ObjectManager::getInstance()->create(Category::class);
                        $category->load($_catPathId);
                        $defaultCommissionPercent = $category->getData(SaveCommissionSetting::DEFAULT_COMMISSION_PERCENT_KEY);
                        $defaultFixedRate = $category->getData(SaveCommissionSetting::DEFAULT_FIXED_RATE_KEY);
                        $commissionByCountry = $category->getData(SaveCommissionSetting::COMMISSION_BY_COUNTRY_KEY);
                        if ($defaultCommissionPercent || $defaultFixedRate || $commissionByCountry) {
                            $catId = $_catPathId;
                            $_subcatMatched = true;
                            break;
                        }
                    }
                    if ($catId) {
                        $_rateToUse = [];
                        $value = $defaultCommissionPercent ?: 0;
                        if (is_array($commissionByCountry)) {
                            foreach ($commissionByCountry as $comByCountry) {
                                if ($v->getCountryId() == @$comByCountry['country']) {
                                    $value = @$comByCountry['commission'] ?: 0;
                                    break;
                                }
                            }
                        }
                        $_rateToUse['value'] = $value;
                        $_rateToUse['is_global_tier'] = true;

                        if ($_rateToUse['value'] !== null && $_rateToUse['value'] !== ''
                            && (
                                !$_isGlobalTier && $isGlobalTier
                                || !$_isGlobalTier && ($_exactMatched || !$exactMatched)
                                || $_isGlobalTier && $isGlobalTier && ($_exactMatched || !$exactMatched)
                            )
                        ) {
                            $_rateToUse['value'] = $format->getNumber($_rateToUse['value']);
                            $ratesToUse[$itemId] = $_rateToUse;
                        }
                    }
                    $exactMatched = $exactMatched || $_exactMatched;
                    $subcatMatched = $subcatMatched || $_subcatMatched;
                    $isGlobalTier = $isGlobalTier && $_isGlobalTier;
                }
            }

            if (!isset($ratesToUse[$itemId])
                || !empty($ratesToUse[$itemId]['is_global_tier'])
                && $cFallbackMethod == 'vendor'
                && '' !== $v->getCommissionPercent()
                && null !== $v->getCommissionPercent()
            ) {
                if ('' !== $v->getCommissionPercent()
                    && null !== $v->getCommissionPercent()
                ) {
                    $ratesToUse[$itemId]['value'] = $format->getNumber($v->getCommissionPercent());
                } else {
                    $ratesToUse[$itemId]['value'] = $format->getNumber($this->scopeConfig->getValue('udropship/tiercom/commission_percent',
                        ScopeInterface::SCOPE_STORE));
                }
            }
            $item->setCommissionPercent(@$ratesToUse[$itemId]['value']);
        }
        if ('' !== $v->getCommissionPercent()
            && null !== $v->getCommissionPercent()
        ) {
            $poComPercent = $format->getNumber($v->getCommissionPercent());
        } else {
            $poComPercent = $format->getNumber($this->scopeConfig->getValue('udropship/tiercom/commission_percent',
                ScopeInterface::SCOPE_STORE));
        }
        $po->setCommissionPercent($poComPercent);
    }

    /**
     * @param $po
     */
    protected function _processItemTierTransactionFee($po)
    {
        $v = $this->_hlp->getVendor($po->getUdropshipVendor());
//        $tierRates = $po->getUdropshipVendor() ? $this->getTiercomRates($po->getUdropshipVendor()) : [];
//        $globalTierRates = $this->getGlobalTierComConfig();
        $topCats = $this->getTopCategories();
        $catIdsToLoad = $catIds = [];
        $pIds = [];
        foreach ($po->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) continue;
            $pIds[] = $item->getProductId();
        }
        $products = $this->_hlp->createObj('\Magento\Catalog\Model\ResourceModel\Product\Collection')->addIdFilter($pIds);
        if (($tcProdAttr = $this->getFixedRateProductAttribute())) {
            $products->addAttributeToSelect($tcProdAttr->getAttributeCode());
        }
        foreach ($po->getAllItems() as $item) {
            $itemId = spl_object_hash($item);
            if ($item->getOrderItem()->getParentItem() || !($product = $products->getItemById($item->getProductId()))) continue;
            $_catIds = $product->getCategoryIds();
            if (empty($_catIds)) continue;
            reset($_catIds);
            $catIdsToLoad = array_merge($catIdsToLoad, $_catIds);
            $catIds[$itemId] = $_catIds;
        }
        $format = $this->_formatLocale;
        $catIdsToLoad = array_unique($catIdsToLoad);
        $iCats = $this->_hlp->createObj('\Magento\Catalog\Model\ResourceModel\Category\Collection')->addIdFilter($catIdsToLoad);
        $subcatMatchFlag = true;
        $ratesToUse = [];
        foreach ($po->getAllItems() as $item) {
            $itemId = spl_object_hash($item);
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            if (($product = $products->getItemById($item->getProductId()))
                && $tcProdAttr
                && '' !== $product->getData($tcProdAttr->getAttributeCode())
                && null !== $product->getData($tcProdAttr->getAttributeCode())
            ) {
                $ratesToUse[$itemId]['fixed'] = $format->getNumber(
                    $product->getData($tcProdAttr->getAttributeCode())
                );
            } elseif (!empty($catIds[$itemId])) {
                $exactMatched = $subcatMatched = false;
                foreach ($catIds[$itemId] as $iCatId) {
                    if (!($iCat = $iCats->getItemById($iCatId))) continue;
                    $_exactMatched = $_subcatMatched = false;
                    $catId = null;
                    $_catPath = explode(',', $this->_catalogHelper->getPathInStore($iCat));
                    $defaultCommissionPercent = 0;
                    $defaultFixedRate = 0;
                    $commissionByCountry = [];
                    foreach ($_catPath as $_catPathId) {
                        $category = ObjectManager::getInstance()->create(Category::class);
                        $category->load($_catPathId);
                        $defaultCommissionPercent = $category->getData(SaveCommissionSetting::DEFAULT_COMMISSION_PERCENT_KEY);
                        $defaultFixedRate = $category->getData(SaveCommissionSetting::DEFAULT_FIXED_RATE_KEY);
                        $commissionByCountry = $category->getData(SaveCommissionSetting::COMMISSION_BY_COUNTRY_KEY);
                        if ($defaultCommissionPercent || $defaultFixedRate || $commissionByCountry) {
                            $catId = $_catPathId;
                            $_subcatMatched = true;
                            break;
                        }
                    }
                    if ($catId) {
                        $_rateToUse = [];

                        $globalTierRate = $defaultFixedRate;
                        if (is_array($commissionByCountry)) {
                            foreach ($commissionByCountry as $comByCountry) {
                                if ($v->getCountryId() == @$comByCountry['country']) {
                                    $globalTierRate = @$comByCountry['fixed_rate'];
                                    break;
                                }
                            }
                        }
                        $_rateToUse['fixed'] = isset($tierRates[$catId]) && isset($tierRates[$catId]['fixed']) && $tierRates[$catId]['fixed'] !== ''
                            ? $tierRates[$catId]['fixed']
                            : $globalTierRate;
                        $_rateToUse['fixed'] = $format->getNumber($_rateToUse['fixed']);
                        $ratesToUse[$itemId] = $_rateToUse;
                    }
                    $exactMatched = $exactMatched || $_exactMatched;
                    $subcatMatched = $subcatMatched || $_subcatMatched;
                }
            }

            if (!empty($ratesToUse[$itemId]['fixed'])) {
                $item->setTransactionFee($item->getTransactionFee() + $item->getQty() * $ratesToUse[$itemId]['fixed']);
            }
        }
    }
}
