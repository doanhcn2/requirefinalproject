define([
    'uiComponent'
], function(Component){

    return Component.extend({
        defaults: {
            template: 'Magenest_CustomCommission/custom-commission',
            commissionSettings: [],
            elementPrefix: '',
            countryOptions: window.countryList
        },

        initObservable: function() {
            this._super()
                .observe(['commissionSettings']);
            return this;
        },

        addNewRow: function() {
            this.commissionSettings.push({
                country: '',
                percent: '',
                fixed: ''
            });
        },

        deleteRow: function(row) {
            this.commissionSettings.remove(row);
        },

        getElementName: function(index, elementName) {
            return this.elementPrefix + '[custom_commissions]' + '['+index+']'+'['+elementName+']'
        }
    })
});