<?php
namespace Magenest\VendorBalanceImport\Model\Import;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\Dropship\Model\VendorFactory;
use Unirgy\DropshipPo\Model\Statement;
use Unirgy\DropshipPo\Model\StatementFactory;

class Balance extends \Magento\ImportExport\Model\Import\AbstractEntity
{
    const ENTITY_TYPE_CODE = 'vendor_balances';
    const ERROR_INVALID_BALANCE_AMOUNT = 'invalidBalanceAmount';

    protected $masterAttributeCode = 'company_email';
    protected $needColumnCheck = true;

    protected $validColumnNames = [
        'company_name',
        'company_email',
        'balance'
    ];

    protected $messageTemplate = [
        self::ERROR_INVALID_BALANCE_AMOUNT => 'Invalid Balance Amount'
    ];

    protected $vendorFactory;
    protected $statementFactory;

    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\ImportExport\Model\ImportFactory $importFactory,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        ResourceConnection $resource,
        ProcessingErrorAggregatorInterface $errorAggregator,
        VendorFactory $vendorFactory,
        StatementFactory $statementFactory,
        array $data = []
    ) {
        parent::__construct($string, $scopeConfig, $importFactory, $resourceHelper, $resource, $errorAggregator, $data);
        foreach ($this->messageTemplate as $code => $message) {
            $this->addMessageTemplate($code, $message);
        }
        $this->vendorFactory = $vendorFactory;
        $this->statementFactory = $statementFactory;
    }

    protected function _importData()
    {
        /** @var Vendor $vendorModel */
        $vendorModel = $this->vendorFactory->create();
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNumber => $rowData) {
                $vendorModel->unsetData();
                /** @var Statement $statementModel */
                $statementModel = $this->statementFactory->create();
                $balance = $this->stripSemicolonFromAmount($rowData['balance']);
                $vendorEmail = $rowData['company_email'];
                $vendorModel->load($vendorEmail, 'email');
                if ($vendorModel->getId()) {
                    $statementModel->setData([
                        'vendor_id' => $vendorModel->getId(),
                        'statement_period' => date('md'),
                        'statement_id' => $vendorModel->getId() . '-' . date('md'),
                        'statement_file_name' => 'statement'.'-'.$vendorModel->getId() . '-' . date('md'),
                        'order_from_date' => time(),
                        'order_to_date' => time(),
                        'created_at' => time(),
                        'po_type' => 'po',
                        'order_data' => '{"orders":[],"refunds":[],"totals":{"subtotal":"$0.00","tax":"$0.00","shipping":"$0.00","handling":"$0.00","discount":"$0.00","hidden_tax":"$0.00","com_amount":"$0.00","trans_fee":"$0.00","adj_amount":"$0.00","total_payout":"$0.00","total_refund":"$0.00","refund_invoice":"$0.00","refund_payment":"$0.00","total_payment":"$0.00","total_invoice":"$0.00","payment_paid":"$0.00","payment_due":"$0.00","invoice_paid":"$0.00","invoice_due":"$0.00","total_paid":"$0.00","total_due":"$0.00"},"totals_amount":{"subtotal":0,"tax":0,"shipping":0,"handling":0,"discount":0,"hidden_tax":0,"com_amount":0,"trans_fee":0,"adj_amount":0,"total_payout":0,"total_refund":0,"refund_invoice":0,"refund_payment":0,"total_payment":0,"total_invoice":0,"payment_paid":0,"payment_due":0,"invoice_paid":0,"invoice_due":0,"total_paid":0,"total_due":0},"extra_adjustments":[],"payouts":[]}',
                        'notes' => '',
                        'use_locale_timezone' => 0,
                        'email_sent' => 0
                    ]);
                    $adjustBalance = $statementModel->createAdjustment($balance)
                        ->setComment('Imported from CSV')
                        ->setPoType('po')
                        ->setUsername(ObjectManager::getInstance()->get(\Magento\Backend\Model\Auth::class)->getUser()->getUsername())
                        ->setPoId(null);
                    $statementModel->addAdjustment($adjustBalance);
                    $statementModel->finishStatement()->save();
                }
            }

        }
        return true;
    }

    private function stripSemicolonFromAmount($amount)
    {
        if (is_numeric($amount)) {
            return $amount;
        }
        if (strpos($amount, ',') !== false) {
            $amount = (string)$amount;
            $amount = (float)str_replace(',','',$amount);
        }
        return (float)$amount;
    }

    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE_CODE;
    }


    public function validateRow(array $rowData, $rowNumber)
    {
        if (isset($this->_validatedRows[$rowNumber])) {
            // check that row is already validated
            return !$this->getErrorAggregator()->isRowInvalid($rowNumber);
        }
        $this->_validatedRows[$rowNumber] = true;
        if (!is_numeric($this->stripSemicolonFromAmount($rowData['balance']))) {
            $this->addRowError(self::ERROR_INVALID_BALANCE_AMOUNT, $rowNumber);
        }
        return true;
    }

}
