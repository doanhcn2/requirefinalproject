define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($, $t, ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/redemption',
                logs: ko.observableArray(),
                redeemedLogs: ko.observableArray(),
                failedLogs: ko.observableArray(),
                requestedTab: 'all',
                allPage: ko.observable('1'),
                redeemedPage: ko.observable('1'),
                failedPage: ko.observable('1'),
                totalAll: '20',
                totalRedeemed: '20',
                totalFailed: '20',
                requestedPage: '1',
                limit: '20',
                show: 0,
                firstIndexNumber: ko.observable(1),
                lastIndexNumber: ko.observable(20),
                totalRecordNumber: ko.observable(20),
            },
            initObservable: function () {
                this._super().observe('logs');
                this._super().observe('redeemedLogs');
                this._super().observe('failedLogs');
                this._super().observe('totalRecord');
                this.selectedTab = this.requestedTab;
                this._super().observe('selectedTab');
                this._super().observe('firstIndex');
                this._super().observe('lastIndex');
                this._super().observe('firstIndexNumber');
                this._super().observe('lastIndexNumber');
                this._super().observe('totalRecordNumber');
                this.firstIndexNumber(this.firstIndex());
                this.lastIndexNumber(this.lastIndex());
                this.totalRecordNumber(this.totalRecord());
                return this;
            },
            firstIndex: function () {
                if (this.totalRecord() === 0) {
                    return 0;
                }
                var page = 1;
                if (this.selectedTab() === this.requestedTab) {
                    page = this.requestedPage;
                }
                return (page - 1) * this.limit + 1;
            },
            totalRecord: function () {
                if (this.selectedTab() === 'all') {
                    return this.totalAll;
                }
                if (this.selectedTab() === 'redeemed') {
                    return this.totalRedeemed;
                }
                if (this.selectedTab() === 'failed') {
                    return this.totalFailed;
                }
            },
            lastIndex: function () {
                var page = 1;
                if (this.selectedTab() === this.requestedTab) {
                    page = this.requestedPage;
                }
                if (page * this.limit > this.totalRecord()) {
                    return this.totalRecord();
                }
                return page * this.limit;
            },
            addLog: function (log) {
                this.logs.unshift(log);
                this.totalAll++;
                if (log.status === 'redeemed') {
                    this.totalRedeemed++;
                    this.redeemedLogs.unshift(log);
                } else {
                    this.totalFailed++;
                    this.failedLogs.unshift(log);
                }
                this.updatePager();
            },
            setShowAll: function () {
                this.selectedTab('all');
                this.show = 0;
                this.updatePager();
            },
            setShowRedeemed: function () {
                this.selectedTab('redeemed');
                this.show = 1;
                this.updatePager();
            },
            setShowFailed: function () {
                this.selectedTab('failed');
                this.show = 2;
                this.updatePager();
            },
            downloadLog: function () {
                window.location.href = window.ORIG_BASE_URL + 'redeem/redemption/download?list=' + this.show;
            },
            changePage: function (param) {
                var params = {};
                params['tab'] = this.selectedTab();
                if (param === 'firstPage') {
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'prePage') {
                    if (this.selectedTab() !== this.requestedTab) {
                        return;
                    }
                    var queryPage = -1 + parseInt(this.requestedPage);
                    if (queryPage <= 0) return;
                    params['page'] = queryPage;
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'nextPage') {
                    var page = 2;
                    if (this.selectedTab() === this.requestedTab) {
                        page = 1 + parseInt(this.requestedPage);
                    }
                    params['page'] = page;
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'lastPage') {
                    if (this.selectedTab() === 'all') {
                        params['page'] = Math.ceil(this.totalAll / 20);
                    } else if (this.selectedTab() === 'redeemed') {
                        params['page'] = Math.ceil(this.totalRedeemed / 20);
                    } else if (this.selectedTab() === 'failed') {
                        params['page'] = Math.ceil(this.totalFailed / 20);
                    }
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                this.updatePager();
            },
            updatePager: function () {
                let self = this;
                self.firstIndexNumber(self.firstIndex());
                self.lastIndexNumber(self.lastIndex());
                self.totalRecordNumber(self.totalRecord());
            }
        });
    }
);