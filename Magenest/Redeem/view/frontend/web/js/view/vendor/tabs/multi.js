/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry'
    ], function ($, $t, ko, Component, uiRegistry) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/tabs/multi',
                multiCode: ko.observable()
            },
            redeemMulti: function () {
                var self = this;
                var parent = this.containers[0];
                var redemption_log = uiRegistry.get('redemptionLog');
                if
                (self.multiCode())
                    $.ajax({
                        url: window.ORIG_BASE_URL + 'redeem/redemption/redeem',
                        type: 'POST',
                        showLoader :true,
                        dataType: 'json',
                        data: {
                            multi: true,
                            code: self.multiCode()
                        },
                        success: function (response) {
                            if (response.success) {
                                $.each(response.logs, function (k, log) {
                                    redemption_log.addLog($.parseJSON(log));
                                });
                            }
                            if (response.error) {
                                parent.addError($t(response.errorMessage));
                            }
                        },
                        error: function (response) {
                            parent.addError($t(response.statusText));
                        }
                    });
                else
                    parent.addError($t('Please Enter a Redemption code!'));
            }
        });
    }
);