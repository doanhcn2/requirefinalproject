/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'Magento_Ui/js/modal/confirm',
        'uiComponent',
        'uiRegistry'
    ], function ($, $t, ko, confirm, Component, uiRegistry) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/tabs/single',
                singleCode: ko.observable()
            },
            initialize: function () {
                let self = this;
                self._super();
                this.singleCode.subscribe(function () {
                    self.containers[0].addError('');
                    $('#redeem-code').removeClass('validation-failed');
                })
            },
            sendRedeemRequest: function(confirmedLocation) {
                var self = this;
                var parent = this.containers[0];
                $.ajax({
                    url: window.ORIG_BASE_URL + 'redeem/redemption/redeem',
                    type: 'POST',
                    dataType: 'json',
                    showLoader: true,
                    data: {
                        single: true,
                        code: self.singleCode(),
                        confirmedLocation: confirmedLocation != undefined ? confirmedLocation : null
                    },
                    success: function (response) {
                        if (response.confirmLocation) {
                            confirm({
                                content: response.message,
                                actions: {
                                    /**
                                     * Confirm action.
                                     */
                                    confirm: function () {
                                        self.sendRedeemRequest(true);
                                    }
                                }
                            });
                        } else {
                            if (response.success) {
                                var redemption_log = uiRegistry.get('redemptionLog');
                                redemption_log.addLog($.parseJSON(response.log));
                            }
                            if (response.error) {
                                parent.addError($t(response.errorMessage));
                            }
                        }
                    },
                    error: function (response) {
                        parent.addError($t(response.statusText));
                    }
                });
            },
            redeemSingle: function () {
                var self = this;
                var parent = this.containers[0];
                if (self.singleCode()) {
                    self.sendRedeemRequest();
                } else {
                    parent.addError($t('Please Enter a Redemption code!'));
                }
            }
        });
    }
);