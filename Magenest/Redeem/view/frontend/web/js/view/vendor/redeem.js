define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($, $t, ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/redeem',
                currentTab: ko.observable(0),
                error: ko.observable('')
            },
            initObservable: function () {
                this._super().observe('currentTab');
                return this;
            },
            addError: function (m) {
                let self = this;
                self.error(m);
                if (self.currentTab() === 0 && m !== '') {
                    $('#redeem-code').addClass('validation-failed');
                } else if (self.currentTab() === 1 && m !== '') {
                    $('#verify-code').addClass('validation-failed');
                }
            },
            moveToStep: function (i) {
                switch (i){
                    case 0 :
                        this.currentTab(0);
                        this.error('');
                        break;
                    case 1 :
                        this.currentTab(1);
                        this.error('');
                        break;
                    default:
                        this.currentTab(0);
                        this.error('');
                        break;
                }
            },
            getTabLabel: function (i) {
                switch (i){
                    case 0 :
                        return $t('Single Redeem');
                    case 1 :
                        return $t('Verify Code');
                    default:
                        return $t('');
                }
            }
        });
    }
);