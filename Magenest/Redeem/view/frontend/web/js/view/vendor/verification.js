define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent'
    ], function ($, $t, ko, Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/verification',
                logs: ko.observableArray(),
                foundLogs: ko.observableArray(),
                notFoundlogs: ko.observableArray(),
                selectedTab: ko.observable("all_ver"),
                totalAll: '20',
                totalFound: '20',
                totalNotFound: '20',
                show: 0,
                firstIndexNumber: ko.observable(1),
                lastIndexNumber: ko.observable(20),
                totalRecordNumber: ko.observable(20),
            },
            initObservable: function () {
                this._super().observe('logs');
                this._super().observe('foundLogs');
                this._super().observe('notFoundlogs');
                this._super().observe('selectedTab');
                this._super().observe('firstIndex');
                this._super().observe('lastIndex');
                this._super().observe('totalRecord');
                this._super().observe('firstIndexNumber');
                this._super().observe('lastIndexNumber');
                this._super().observe('totalRecordNumber');
                this.firstIndexNumber(this.firstIndex());
                this.lastIndexNumber(this.lastIndex());
                this.totalRecordNumber(this.totalRecord());
                return this;
            },
            addLog: function (log) {
                this.logs.unshift(log);
                this.totalAll++;
                if (log.status === 'found') {
                    this.totalFound++;
                    this.foundLogs.unshift(log);
                } else {
                    this.totalNotFound++;
                    this.notFoundlogs.unshift(log);
                }
                this.updatePager();
            },
            setShowAll: function () {
                this.selectedTab('all_ver');
                this.updatePager();
            },
            setShowFound: function () {
                this.selectedTab('found');
                this.updatePager();
            },
            setShowNotFound: function () {
                this.selectedTab('not_found');
                this.updatePager();
            },
            downloadLog: function () {
                window.location.href = window.ORIG_BASE_URL + 'redeem/verification/download?list=' + this.show;
            },

            firstIndex: function () {
                if (this.totalRecord() === 0) {
                    return 0;
                }
                var page = 1;
                if (this.selectedTab() === this.requestedTab) {
                    page = this.requestedPage;
                }
                return (page - 1) * this.limit + 1;
            },
            totalRecord: function () {
                if (this.selectedTab() === 'all_ver') {
                    return this.totalAll;
                }
                if (this.selectedTab() === 'found') {
                    return this.totalFound;
                }
                if (this.selectedTab() === 'not_found') {
                    return this.totalNotFound;
                }
            },
            lastIndex: function () {
                var page = 1;
                if (this.selectedTab() === this.requestedTab) {
                    page = this.requestedPage;
                }
                if (page * this.limit > this.totalRecord()) {
                    return this.totalRecord();
                }
                return page * this.limit;
            },
            changePage: function (param) {
                var params = {};
                params['verify_tab'] = this.selectedTab();
                params['tab_session'] = 'verify';
                if (param === 'firstPage') {
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'prePage') {
                    if (this.selectedTab() !== this.requestedTab) {
                        return;
                    }
                    var queryPage = -1 + parseInt(this.requestedPage);
                    if (queryPage <= 0) return;
                    params['verify_page'] = queryPage;
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'nextPage') {
                    var page = 2;
                    if (this.selectedTab() === this.requestedTab) {
                        page = 1 + parseInt(this.requestedPage);
                    }
                    params['verify_page'] = page;
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                if (param === 'lastPage') {
                    if (this.selectedTab() === 'all_ver') {
                        params['verify_page'] = Math.ceil(this.totalAll / 20);
                    } else if (this.selectedTab() === 'found') {
                        params['verify_page'] = Math.ceil(this.totalFound / 20);
                    } else if (this.selectedTab() === 'not_found') {
                        params['verify_page'] = Math.ceil(this.totalNotFound / 20);
                    }
                    var query = $.param(params);
                    window.location.href = '?' + query;
                }
                this.updatePager();
            },
            updatePager: function () {
                let self = this;
                self.firstIndexNumber(self.firstIndex());
                self.lastIndexNumber(self.lastIndex());
                self.totalRecordNumber(self.totalRecord());
            }
        });
    }
);