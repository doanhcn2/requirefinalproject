/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'uiRegistry'
    ], function ($, $t, ko, Component, uiRegistry) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_Redeem/vendor/tabs/verify',
                verifyCode: ko.observable()
            },
            initialize: function () {
                let self = this;
                self._super();
                this.verifyCode.subscribe(function () {
                    self.containers[0].addError('');
                    $('#verify-code').removeClass('validation-failed');
                })
            },
            verify: function () {
                var self = this;
                var parent = this.containers[0];
                var verification_log = uiRegistry.get('verificationLog');
                if
                (self.verifyCode())
                    $.ajax({
                        url: window.ORIG_BASE_URL + 'redeem/verification/verify',
                        type: 'POST',
                        showLoader :true,
                        dataType: 'json',
                        data: {
                            code: self.verifyCode()
                        },
                        success: function (response) {
                            if (response.success) {
                                verification_log.addLog($.parseJSON(response.log));
                            }
                            if (response.error) {
                                parent.addError($t(response.errorMessage));
                            }
                        },
                        error: function (response) {
                            parent.addError($t(response.statusText));
                        }
                    });
                else
                    parent.addError($t('Please Enter a Redemption code!'));
            }
        });
    }
);