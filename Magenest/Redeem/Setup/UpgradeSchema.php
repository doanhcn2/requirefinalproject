<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Redeem\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package Magenest\Ticket\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $redemptionLogTable = $setup->getTable('magenest_groupon_redemption_log');
            $setup->getConnection()->addColumn(
                $redemptionLogTable,
                'redeemed_at',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'comment' => 'Redeemed At',
                    'nullable' => true
                ]
            );
            $redemptionLogTable = $setup->getTable('magenest_groupon_verification_log');
            $setup->getConnection()->addColumn(
                $redemptionLogTable,
                'verified_at',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    'comment' => 'Verified At',
                    'nullable' => true
                ]
            );
        }

        $setup->endSetup();
    }
}
