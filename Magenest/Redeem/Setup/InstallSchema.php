<?php

namespace Magenest\Redeem\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    const TABLE_PREFIX = 'magenest_groupon_';

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $installer = $setup;
        $tableName = self::TABLE_PREFIX . 'redemption_log';
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn('id',       Table::TYPE_INTEGER,null,   ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('code',     Table::TYPE_TEXT,   50,     ['nullable' => false,], 'Redemption code')
            ->addColumn('vendor_id',Table::TYPE_INTEGER,null,   ['nullable' => false,], 'Vendor Id')
            ->addColumn('status',   Table::TYPE_TEXT,   null,   ['nullable' => false,], 'Redemption status')
            ->addColumn('message',  Table::TYPE_TEXT,   null,   ['nullable' => false,], 'message')
            ->setComment('Redemption Log');
        $installer->getConnection()->createTable($table);
        $setup->endSetup();

        $setup->startSetup();
        $installer = $setup;
        $tableName = self::TABLE_PREFIX . 'verification_log';
        $table = $installer->getConnection()->newTable($installer->getTable($tableName))
            ->addColumn('id',       Table::TYPE_INTEGER,null,   ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('code',     Table::TYPE_TEXT,   50,     ['nullable' => false,], 'Verification code')
            ->addColumn('vendor_id',Table::TYPE_INTEGER,null,   ['nullable' => false,], 'Vendor Id')
            ->addColumn('status',   Table::TYPE_TEXT,   null,   ['nullable' => false,], 'Verification status')
            ->setComment('Verification Log');
        $installer->getConnection()->createTable($table);
        $setup->endSetup();
    }
}