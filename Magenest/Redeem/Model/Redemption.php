<?php
namespace Magenest\Redeem\Model;

use Magento\Framework\Model\AbstractModel;

class Redemption extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Magenest\Redeem\Model\ResourceModel\Redemption');
    }
}
