<?php
namespace Magenest\Redeem\Model\ResourceModel\Verification;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Redeem\Model\Verification',
            'Magenest\Redeem\Model\ResourceModel\Verification'
        );
    }
}
