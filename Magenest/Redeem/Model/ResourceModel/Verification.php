<?php
namespace Magenest\Redeem\Model\ResourceModel;

class Verification extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('magenest_groupon_verification_log', 'id');
    }
}
