<?php
namespace Magenest\Redeem\Model\ResourceModel\Redemption;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Redeem\Model\Redemption',
            'Magenest\Redeem\Model\ResourceModel\Redemption'
        );
    }
}
