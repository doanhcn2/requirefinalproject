<?php

namespace Magenest\Redeem\Block\Redeem;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
    private $redemptionLog;
    private $verificationLog;
    private $page;

    public function __construct(
        \Magenest\Redeem\Model\VerificationFactory $verificationFactory,
        \Magenest\Redeem\Model\RedemptionFactory $redemptionFactory,
        Template\Context $context, array $data = []
    ) {
        $this->redemptionLog = $redemptionFactory;
        $this->verificationLog = $verificationFactory;
        $this->page = 1;
        parent::__construct($context, $data);
    }

	public function getRequestedPage()
	{
		$pages = $this->getRequest()->getParam('page');
		if (isset($pages)) {
			return (int)$pages;
		}
		return $this->page;
	}

    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getRedemptionLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->redemptionLog->create()
            ->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize(20)->setOrder('redeemed_at', 'desc');
        if ($this->getSelectedTab() == 'all') {
            if ($this->getCurrentPage() > $collection->getLastPageNumber()) {
                $this->page = $collection->getLastPageNumber();
                $collection->setCurPage($collection->getLastPageNumber());
            } else {
                $this->page = $this->getCurrentPage();
                $collection->setCurPage($this->getCurrentPage());
            }
        } else {
            $collection->setCurPage(1);
            $this->page = 1;
        }
        return $collection;
    }

    public function getRedeemedLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->redemptionLog->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('status', 'redeemed')->setOrder('redeemed_at', 'desc');
        $collection->setPageSize(20);
        if ($this->getSelectedTab() == 'redeemed') {
            if ($this->getCurrentPage() > $collection->getLastPageNumber()) {
                $this->page = $collection->getLastPageNumber();
                $collection->setCurPage($collection->getLastPageNumber());
            } else {
                $this->page = $this->getCurrentPage();
                $collection->setCurPage($this->getCurrentPage());
            }
        } else {
            $collection->setCurPage(1);
            $this->page = 1;
        }
        return $collection;
    }

    public function getFailedLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->redemptionLog->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('status', ['neq' => 'redeemed'])->setOrder('redeemed_at', 'desc');
        $collection->setPageSize(20);
        if ($this->getSelectedTab() == 'failed') {
            if ($this->getCurrentPage() > $collection->getLastPageNumber()) {
                $this->page = $collection->getLastPageNumber();
                $collection->setCurPage($collection->getLastPageNumber());
            } else {
                $this->page = $this->getCurrentPage();
                $collection->setCurPage($this->getCurrentPage());
            }
        } else {
            $collection->setCurPage(1);
            $this->page = 1;
        }
        return $collection;
    }


    public function getVerificationLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->verificationLog->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize('20')->setOrder('verified_at', 'desc')
            ->setCurPage($this->getVerifyPage());
        return $collection;
    }

    public function getFoundVerificationLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->verificationLog->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('status', 'found')->setOrder('verified_at', 'desc')
            ->setPageSize('20')
            ->setCurPage($this->getVerifyPage());
        return $collection;
    }

    public function getNotFoundVerificationLog()
    {
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->verificationLog->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('status', ['neq' => 'found'])->setOrder('verified_at', 'desc')
            ->setPageSize('20')
            ->setCurPage($this->getVerifyPage());
        return $collection;
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getCurrentPage()
    {
        $page = $this->getRequest()->getParam('page');
        if (is_numeric($page) && $page != '') {
            $this->page = $page;
            return $page;
        }
        return 1;
    }

    public function getSelectedTab()
    {
        $tab = $this->getRequest()->getParam('tab');
        if ($tab != '') {
            if ($tab == 'all' || $tab == 'redeemed' || $tab == 'failed') {
                return $tab;
            }
        }
        return 'all';
    }

    public function getVerificationTab()
    {
        $tab = $this->getRequest()->getParam('verify_tab');
        if ($tab == 'all_ver' || $tab == 'found' || $tab == 'not_found') {
            return $tab;
        }
        return 'all_ver';
    }


    public function getVerifyPage()
    {
        $pages = $this->getRequest()->getParam('verify_page');
        if (isset($pages)) {
            $this->page = (int)$pages;
            return (int)$pages;
        }
        return 1;
    }

    public function getRedeemTab()
    {
        if ($this->getRequest()->getParam('tab_session') == 'verify') {
            return 1;
        }
        return 0;
    }
}
