<?php

namespace Magenest\Redeem\Controller\Redemption;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\Filesystem\Directory\WriteInterface;

class Download extends \Magento\Framework\App\Action\Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    /**
     * @var FileFactory $fileFactory
     */
    protected $fileFactory;

    protected $redemptionCollection;

    /**
     * Download constructor.
     * @param \Magenest\Redeem\Model\ResourceModel\Redemption\CollectionFactory $collectionFactory
     * @param Filesystem $filesystem
     * @param FileFactory $fileFactory
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        \Magenest\Redeem\Model\ResourceModel\Redemption\CollectionFactory $collectionFactory,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->redemptionCollection = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $show = $this->getRequest()->getParam('list');
            $collection = $this->redemptionCollection->create()->addFieldToFilter('vendor_id', $this->getVendorId());
            switch ($show) {
                case '0' :
                    $show = '';
                    break;
                case '1' :
                    $show = 'Redeemed_';
                    $collection->addFieldToFilter('status', 'redeemed');
                    break;
                case '2' :
                    $show = 'Failed_';
                    $collection->addFieldToFilter('status', array('neq' => 'redeemed'));
                    break;
                default:
                    $show = '';
                    break;
            }
            $heading = [
                ('id'),
                ('code'),
                ('redeemed at'),
                ('status'),
                ('message')
            ];

            $items = [];
            foreach($collection as $log) {
                $row = [
                    $log->getData('id'),
                    $log->getData('code'),
                    $log->getData('redeemed_at'),
                    $log->getData('status'),
                    $log->getData('message')
                ];
                array_push($items, $row);
            }
            $file = 'var/tmp/' . "Redemption_Log_" . $show . date('Ymd_His') . ".csv";
            $this->directory->create('export');
            $stream = $this->directory->openFile($file, 'w+');
            $stream->lock();
            $stream->writeCsv($heading);
            foreach($items as $item) {
                $stream->writeCsv($item);

            }

            return $this->fileFactory->create("Ticket_Log_" . date('Ymd_His') . ".csv", [
                'type' => 'filename',
                'value' => $file,
                'rm' => true  // can delete file after use
            ], 'var');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }

        return $resultRedirect->setPath('*/*/*');
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
