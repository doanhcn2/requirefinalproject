<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Redeem\Controller\Redemption;

use Magenest\Groupon\Model\Groupon;
use Magenest\Groupon\Model\LocationFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;

class Redeem extends \Magento\Framework\App\Action\Action
{
    const PROMOTE_PRODUCT_TICKET = '3';

    const PROMOTE_PRODUCT_GROUPON = '1';

    const PROMOTE_PRODUCT_ACCOMMODATION = '2';

    const PROMOTE_PRODUCT_SIMPLE = '4';

    protected $jsonFactory;
    protected $redemptionLog;
    protected $grouponFactory;
    protected $locationFactory;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    /**
     * @var $_unirgyHelper \Unirgy\Dropship\Helper\Data
     */
    protected $_unirgyHelper;

    /**
     * Redeem constructor.
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magenest\Redeem\Model\RedemptionFactory $redemptionFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param LocationFactory $locationFactory
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Unirgy\Dropship\Helper\Data $data
     * @param Context $context
     */
    public function __construct(
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Redeem\Model\RedemptionFactory $redemptionFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LocationFactory $locationFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Unirgy\Dropship\Helper\Data $data,
        Context $context
    ) {
        $this->_unirgyHelper = $data;
        $this->_ticketFactory = $ticketFactory;
        $this->grouponFactory = $grouponFactory;
        $this->redemptionLog = $redemptionFactory;
        $this->jsonFactory = $resultJsonFactory;
        $this->locationFactory = $locationFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                $code = $this->getRequest()->getParam('code');
                $code = strip_tags(trim($code));
                $confirmedLocation = $this->getRequest()->getParam('confirmedLocation');
                $result = [
                    'success' => true,
                    'error' => false
                ];
                if ($this->getVendorPromoteProduct() === self::PROMOTE_PRODUCT_GROUPON) {
                    $modelHandle = $this->grouponFactory->create()->loadByCode($code, $this->getVendorId());
                } elseif ($this->getVendorPromoteProduct() === self::PROMOTE_PRODUCT_TICKET) {
                    $modelHandle = $this->_ticketFactory->create()->loadByCode($code, $this->getVendorId());
                } else {
                    throw new \Exception('Vendor has not register yet.');
                }
                if (!$confirmedLocation && $confirmData = $modelHandle->needLocationConfirmation()) {
                    $result = $confirmData;
                } else {
                    $result['log'] = $modelHandle->redeem($code, $this->getVendorId());
                }

            } catch (\Exception $e) {
                $result = [
                    'success' => false,
                    'error' => true,
                    'errorMessage' => $e->getMessage()
                ];
            }
            return $this->jsonFactory->create()->setData($result);
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    private function getVendorPromoteProduct()
    {
        return $this->_unirgyHelper->getVendor($this->getVendorId())->getPromoteProduct();
    }
}
