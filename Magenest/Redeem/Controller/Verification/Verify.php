<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Redeem\Controller\Verification;

use Magenest\Groupon\Model\Groupon;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;

class Verify extends \Magento\Framework\App\Action\Action
{
    const STATUS_NOTFOUND_TEXT = 'not found';

    const STATUS_FOUND_TEXT = 'found';

    const PROMOTE_PRODUCT_TICKET = '3';

    const PROMOTE_PRODUCT_GROUPON = '1';

    const PROMOTE_PRODUCT_ACCOMMODATION = '2';

    const PROMOTE_PRODUCT_SIMPLE = '4';

    /**
     * @var $_unirgyHelper \Unirgy\Dropship\Helper\Data
     */
    protected $_unirgyHelper;

    /**
     * @var $_ticketFactory \Magenest\Ticket\Model\TicketFactory
     */
    protected $_ticketFactory;

    protected $jsonFactory;
    protected $verificationLog;
    protected $grouponFactory;

    /**
     * Verify constructor.
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magenest\Redeem\Model\VerificationFactory $verificationFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magenest\Ticket\Model\TicketFactory $ticketFactory
     * @param \Unirgy\Dropship\Helper\Data $data
     * @param Context $context
     */
    public function __construct(
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Redeem\Model\VerificationFactory $verificationFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magenest\Ticket\Model\TicketFactory $ticketFactory,
        \Unirgy\Dropship\Helper\Data $data,
        Context $context)
    {
        $this->_ticketFactory = $ticketFactory;
        $this->_unirgyHelper = $data;
        $this->grouponFactory = $grouponFactory;
        $this->verificationLog = $verificationFactory;
        $this->jsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                $code = $this->getRequest()->getParam('code');
                $code = strip_tags(trim($code));
                $result = [
                    'success' => true,
                    'error' => false
                ];
                if (@$code) {
                    $result['log'] = $this->verification($code);
                } else {
                    $result = [
                        'success' => false,
                        'error' => true,
                        'errorMessage' => __("Something went wrong. Please try again later.")
                    ];
                }
            } catch (\Exception $e) {
                $result = [
                    'success' => false,
                    'error' => true,
                    'errorMessage' => $e->getMessage()
                ];
            }
            return $this->jsonFactory->create()->setData($result);
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param $code
     * @return string
     * @throws \Exception
     */
    public function verification($code)
    {
        if ($this->getVendorPromoteProduct() === self::PROMOTE_PRODUCT_GROUPON) {
            $collection = $this->grouponFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
        } elseif ($this->getVendorPromoteProduct() === self::PROMOTE_PRODUCT_TICKET) {
            $collection = $this->_ticketFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
        } else {
            throw new \Exception('Vendor has not register yet.');
        }
        $model = $collection->addFieldToFilter('redemption_code', @$code)->getFirstItem();
        if ($model->getId() && $model->getData('status') == 1)
            return $this->saveLog($code, self::STATUS_FOUND_TEXT);
        return $this->saveLog($code, self::STATUS_NOTFOUND_TEXT);

    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    /**
     * @param $code
     * @param $status
     * @return string
     * @throws \Exception
     */
    protected function saveLog($code = '', $status)
    {
        $log = $this->verificationLog->create();
        $log->setData('code', $code);
        $log->setData('vendor_id', @$this->getVendorId());
        $log->setData('status', $status);
        $log->setData('verified_at', date('Y-m-d H:i:s', time()));
        $log->save();
        return json_encode($log->getData());
    }

    private function getVendorPromoteProduct()
    {
        return $this->_unirgyHelper->getVendor($this->getVendorId())->getPromoteProduct();
    }
}
