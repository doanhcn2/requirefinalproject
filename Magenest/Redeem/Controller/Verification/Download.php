<?php

namespace Magenest\Redeem\Controller\Verification;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\Filesystem\Directory\WriteInterface;

class Download extends \Magento\Framework\App\Action\Action
{
    /**
     * @var WriteInterface
     */
    protected $directory;

    /**
     * @var FileFactory $fileFactory
     */
    protected $fileFactory;

    protected $verificationCollection;

    /**
     * Download constructor.
     * @param \Magenest\Redeem\Model\ResourceModel\Verification\CollectionFactory $collectionFactory
     * @param Filesystem $filesystem
     * @param FileFactory $fileFactory
     * @param Context $context
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        \Magenest\Redeem\Model\ResourceModel\Verification\CollectionFactory $collectionFactory,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        Context $context
    ) {
        $this->fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->verificationCollection = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $show = $this->getRequest()->getParam('list');
            $collection = $this->verificationCollection->create()->addFieldToFilter('vendor_id', $this->getVendorId());
            switch ($show) {
                case '0' :
                    $show = '';
                    break;
                case '1' :
                    $show = 'Found_';
                    $collection->addFieldToFilter('status', 'found');
                    break;
                case '2' :
                    $show = 'Not_Found_';
                    $collection->addFieldToFilter('status', array('neq' => 'found'));
                    break;
                default:
                    $show = '';
                    break;
            }
            $heading = [
                ('id'),
                ('code'),
                ('verified at'),
                ('status')
            ];

            $items = [];
            foreach($collection as $log) {
                $row = [
                    $log->getData('id'),
                    $log->getData('code'),
                    $log->getData('verified_at'),
                    $log->getData('status')
                ];
                array_push($items, $row);
            }
            $file = 'var/tmp/' . "Verification_Log_" . $show . date('Ymd_His') . ".csv";
            $this->directory->create('export');
            $stream = $this->directory->openFile($file, 'w+');
            $stream->lock();
            $stream->writeCsv($heading);
            foreach($items as $item) {
                $stream->writeCsv($item);

            }

            return $this->fileFactory->create("Ticket_Log_" . date('Ymd_His') . ".csv", [
                'type' => 'filename',
                'value' => $file,
                'rm' => true  // can delete file after use
            ], 'var');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e, __('Something went wrong while export.'));
        }

        return $resultRedirect->setPath('*/*/*');
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
