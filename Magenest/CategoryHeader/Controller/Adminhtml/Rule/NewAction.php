<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;

/**
 * Class NewAction
 * @package Magenest\CategoryHeader\Controller\Adminhtml\Rule
 */
class NewAction extends Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $forward;

    /**
     * NewAction constructor.
     * @param Action\Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $forwardFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $forwardFactory
    ) {
        parent::__construct($context);
        $this->forward = $forwardFactory;
    }

    /**
     * forward to edit
     *
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->forward->create();
        
        return $resultForward->forward('edit');
    }
}
