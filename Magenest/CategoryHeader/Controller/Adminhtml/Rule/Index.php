<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Controller\Adminhtml\Rule;

use Magenest\CategoryHeader\Controller\Adminhtml\Rule as RuleController;

/**
 * Class Index
 * @package Magenest\CategoryHeader\Controller\Adminhtml\Event
 */
class Index extends RuleController
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_setPageData();
        return $this->getResultPage();
    }
}
