<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;

/**
 * Class Save
 * @package Magenest\CategoryHeader\Controller\Adminhtml\Rule
 */
class Save extends Action
{
    /**
     * Save Rule
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $model = $this->_objectManager->create('Magenest\CategoryHeader\Model\Rule');
            $arrayItem = [];
            $oldItem = null;
            if (!empty($data['rule_id'])) {
                $model->load($data['rule_id']);
                $oldItem = $model->getListItem();
                $arrayItem = explode(',', $oldItem);
            }
            $info = [
                'status' => $data['status'],
                'title' => $data['title'],
                'number' => $data['number'],
                'use_slider' => $data['use_slider'],
                'category' => $data['category'],
                'priority' => $data['priority'],
            ];
            if (isset($data['select']['product']) && !empty($data['select']['product'])) {
                foreach ($data['select']['product'] as $key=>$value) {
                    if (strpos($oldItem, $value) === false) {
                        array_push($arrayItem , $value);
                    }
                }
            }
            if (isset($data['delete']) && $data['delete']) {
                foreach ($data['delete'] as $keyDelete=>$valueDelete) {
                    foreach ($arrayItem as $keyInfo=>$valueInfo)
                    {
                        if ($keyDelete == $valueInfo) {
                            unset($arrayItem[$keyInfo]);
                        }
                    }
                }
            }
            $info['list_item'] = implode(',', $arrayItem);

            $model->addData($info);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());
            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('Rule has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the Rule.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
