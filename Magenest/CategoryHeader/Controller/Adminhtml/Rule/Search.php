<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Controller\Adminhtml\Rule;

use \Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Save
 * @package Magenest\CategoryHeader\Controller\Adminhtml\Rule
 */
class Search extends Action
{

    /**
     * @return bool|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $input = $this->_request->getParam('input');
        $category = $this->_request->getParam('category');
        if ($input && $category) {
            $arrayCategory = [$category];
            $searchStr = '%'.trim($input).'%';
            /** @var  $model \Magento\Catalog\Model\ResourceModel\Product\Collection */
            $model = $this->_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
            $model->addAttributeToSelect('*')
                ->addCategoriesFilter(['in' => $arrayCategory])
                ->addAttributeToFilter('name', array('like' => $searchStr));
            $count = count($model);
            $content= null;
            $content = $this->_view->getLayout()->createBlock('Magenest\CategoryHeader\Block\Adminhtml\Rule\Search')
                    ->setInformation($model)->toHtml();
//            $resultArray = json_encode($content);
            $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
            $result->setContents($content);
//            $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
//            $resultJson->setData($resultArray);=

            return $result;
        }

        return false;
    }
}
