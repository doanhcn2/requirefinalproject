<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;

/**
 * Class Refresh
 * @package Magenest\CategoryHeader\Controller\Adminhtml\User
 */
class Refresh extends Action
{
    /**
     * execute
     */
    public function execute()
    {
        $this->messageManager->addSuccessMessage('Refesh success !');
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl($this->getUrl('*')));
        $this->_redirect('adminhtml/*/');

        return;
    }
}
