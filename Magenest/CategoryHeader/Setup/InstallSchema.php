<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Magenest\CategoryHeader\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('magenest_category_header')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_category_header')
            )->addColumn(
                'rule_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Rule ID'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Title'
            )->addColumn(
                'list_item',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'List Item'
            )->addColumn(
                'category',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Category'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Created At'
            )->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Updated At'
            )->addColumn(
                'status',
                Table::TYPE_DECIMAL,
                2,
                ['nullable' => true],
                'Status'
            )->addColumn(
                'number',
                Table::TYPE_INTEGER,
                3,
                ['nullable' => true],
                'Number'
            )->addColumn(
                'use_slider',
                Table::TYPE_DECIMAL,
                2,
                ['nullable' => true],
                'Use Slider'
            )->addColumn(
                'priority',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Priority'
            )->setComment('CategoryHeader Table');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
