<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime as LibDateTime;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class Event
 * @package Magenest\CategoryHeader\Model\ResourceModel
 */
class Rule extends AbstractDb
{
    /**
     * Date time handler
     *
     * @var LibDateTime
     */
    protected $_dateTime;

    /**
     * Date model
     *
     * @var DateTime
     */
    protected $_date;

    /**
     * constructor
     *
     * @param LibDateTime $dateTime
     * @param DateTime $date
     * @param Context $context
     */
    public function __construct(
        LibDateTime $dateTime,
        DateTime $date,
        Context $context
    ) {
    
        $this->_dateTime = $dateTime;
        $this->_date     = $date;
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_category_header', 'rule_id');
    }
}
