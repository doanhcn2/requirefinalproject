<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Event
 *
 * @package Magenest\CategoryHeader\Model
 *
 * @method int getRuleId()
 * @method string getTitle()
 * @method int getStatus()
 * @method string getListItem()
 */
class Rule extends AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'magenest_category_header';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'magenest_category_header';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magenest_category_header';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\CategoryHeader\Model\ResourceModel\Rule');
    }
}
