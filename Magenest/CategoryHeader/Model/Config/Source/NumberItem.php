<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Model\Config\Source;

/**
 * Class NumberItem
 * @package Magenest\CategoryHeader\Model\Config\Source
 */
class NumberItem implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 3, 'label' => __('3 items/row')], ['value' => 5, 'label' => __('5 items/row')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [3 => __('3 items/row'), 5 => __('5 items/row')];
    }
}