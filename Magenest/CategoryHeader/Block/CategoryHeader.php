<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Block;

use Magenest\Groupon\Model\Product\Attribute\CampaignStatus;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;

/**
 * Class Ticket
 * @package Magenest\Ticket\Block\Product
 */
class CategoryHeader extends Template
{
    /**
     * @var \Magenest\CategoryHeader\Model\RuleFactory
     */
    protected $rule;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $product;

    protected $_reviewFactory;

    /**
     * @var $_udprodProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_udprodProductFactory;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * CategoryHeader constructor.
     * @param Template\Context $context
     * @param \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Unirgy\Dropship\Model\Vendor\ProductFactory $udProductFactory
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $udProductFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_vendorFactory = $vendorFactory;
        $this->_udprodProductFactory = $udProductFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->rule = $ruleFactory;
        $this->product = $productFactory;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');

        return $category->getId();
    }

    /**
     * @return array
     */
    public function collectionHeader()
    {
        $modelRule = $this->rule->create()->getCollection()
            ->addFieldToFilter('category', $this->getCategoryId())
            ->addFieldToFilter('status', 1)
            ->setOrder('priority','asc');

        return $modelRule;
    }

    /**
     * @return array
     */
    public function collectionItem($ruleId)
    {
        $modelRule = $this->rule->create()->load($ruleId);
        $listItem = explode(',',$modelRule->getListItem());

        return $listItem;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDataItem($id)
    {
        $data = $this->product->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $id)->getFirstItem();

        return $data;
    }

    /**
     * @return string
     */
    public function getPathImage()
    {
        $path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);;

        return $path;
    }

    public function getRatingSummary($productId)
    {
        $this->_reviewFactory->create()->getEntitySummary($this->getDataItem($productId), $this->_storeManager->getStore()->getId());
        $ratingSummary = $this->getDataItem($productId)->getRatingSummary()->getRatingSummary();
        return $ratingSummary;
    }

    /**
     * @param $productId
     * @return bool
     */
    public function isDisplayFrontend($productId) {
        $product = $this->product->create()->load($productId);
        if (intval($product->getStatus()) === Status::STATUS_ENABLED && in_array($product->getVisibility(), [Visibility::VISIBILITY_BOTH, Visibility::VISIBILITY_IN_CATALOG]) && in_array(intval($product->getCampaignStatus()), [CampaignStatus::STATUS_APPROVED, CampaignStatus::STATUS_ACTIVE, CampaignStatus::STATUS_LIVE])) {
            return false;
        }
        return true;
    }

    public function trimText($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    public function getVendorName($productId)
    {
        $vendorId = $this->_udprodProductFactory->create()->load($productId, 'product_id')->getVendorId();
        return $this->_vendorFactory->create()->load($vendorId)->getVendorName();
    }

    public function getAttributeOptionText($product, $attributeOptionId)
    {
        $attributeId = $product->getResource()->getAttribute('brand');
        if ($attributeId->usesSource()) {
            $data = $attributeId->getSource()->getAttribute()->getData();
            if ($attributeOptionId) {
                $optionText = $attributeId->getSource()->getOptionText($attributeOptionId);
            } else {
                $optionText = $attributeId->getSource()->getOptionText($data['default_value']);
            }
        }

        return @$optionText;
    }
}
