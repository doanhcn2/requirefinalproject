<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Block\Adminhtml\Rule\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Main
 *
 * @package Magenest\CategoryHeader\Block\Adminhtml\User\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * @var \Magenest\CategoryHeader\Model\RuleFactory
     */
    protected $rule;

    /**
     * @var \Magenest\Odoo\Model\Status
     */
    protected $_status;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $category;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory
     * @param \Magenest\CategoryHeader\Model\Status $status
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory,
        \Magenest\CategoryHeader\Model\Status $status,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->rule = $ruleFactory;
        $this->_status = $status;
        $this->category = $categoryFactory;
    }

    /**
     * prepare form
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $id = $this->_request->getParam('id');
        $model = false;
        /* @var $model \Magenest\CategoryHeader\Model\AttributeRule */
        if ($id) {
            $model = $this->rule->create()->load($id);
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Rule Information')]);

        if ($model) {
            $fieldset->addField(
                'rule_id',
                'hidden',
                [
                    'name' =>'rule_id'
                ]
            );
        }

        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->_status->getOptionArray(),
            ]
        );
        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'category',
            'select',
            [
                'label' => __('Category'),
                'title' => __('Category'),
                'name' => 'category',
                'required' => true,
                'options' => $this->getListCategory(),
            ]
        );
        $fieldset->addField(
            'number',
            'select',
            [
                'label' => __('Number Items/Slide'),
                'title' => __('Number Items/Slide'),
                'name' => 'number',
                'required' => true,
                'options' => $this->getListNumber(),
            ]
        );

        $fieldset->addField(
            'use_slider',
            'select',
            [
                'label' => __('Use Slider'),
                'title' => __('Use Slider'),
                'name' => 'use_slider',
                'options' => $this->_status->getOptionArray(),
            ]
        );
        $fieldset->addField(
            'priority',
            'text',
            [
                'label' => __('Priority'),
                'title' => __('Priority'),
                'name' => 'priority',
                'required' => true,
                'class' => 'validate-zero-or-greater',
            ]
        );

        if ($model) {
            $form->setValues($model->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return array
     */
    protected function getListCategory()
    {
        /** @var \Magenest\Odoo\Model\Odoo $modelOdoo */
        $model = $this->category->create()->getCollection()->addAttributeToSelect('*');
        $array = [
            ''=> '--- Select ---'
        ];
        foreach ($model as $category) {
            if ($category->getName()) {
                $array[$category->getEntityId()] = $category->getName();
            }
        }

        return $array;
    }

    /**
     * @return array
     */
    protected function getListNumber()
    {
        return [
            '' => '--- Select ---',
            '1' => '1 item/slide',
            '5' => '5 items/slide',
        ];
    }


    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Rule Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Rule Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
