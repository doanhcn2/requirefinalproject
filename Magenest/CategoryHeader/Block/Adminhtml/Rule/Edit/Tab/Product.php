<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Block\Adminhtml\Rule\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data  as BackendHelper;
use Magento\Framework\Registry;

/**
 * Class Product
 * @package Magenest\CategoryHeader\Block\Adminhtml\Product\Edit\Tab
 */
class Product extends Extended
{
    protected $_template = 'main.phtml';
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\CategoryHeader\Model\RuleFactory
     */
    protected $rule;

    /**
     * Product constructor.
     * @param Context $context
     * @param BackendHelper $backendHelper
     * @param Registry $coreRegistry
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        BackendHelper $backendHelper,
        Registry $coreRegistry,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\CategoryHeader\Model\RuleFactory $ruleFactory,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->rule = $ruleFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return string
     */
    public function searchProduct()
    {
        return $this->getUrl('category_header/rule/search',['form_key' => $this->getFormKey()]);
    }

    /**
     * @return bool
     */
    public function getListItem()
    {
        if ($this->_request->getParam('id')) {
            $data = $this->rule->create()->load($this->_request->getParam('id'));
            $listItem = explode(',', $data->getListItem());
            $model = $this->_productFactory->create()->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => $listItem));

            return $model;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getPathImage()
    {
        $path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);;

        return $path;
    }
}
