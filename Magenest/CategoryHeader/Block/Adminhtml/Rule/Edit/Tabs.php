<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Magenest\CategoryHeader\Block\Adminhtml\Rule\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_base_fieldset');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Rule Category'));
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'main',
            [
                'label'   => __('Main'),
                'content' => $this->getLayout()->createBlock(
                    'Magenest\CategoryHeader\Block\Adminhtml\Rule\Edit\Tab\Main',
                    'category.header.tab.main'
                )->toHtml(),
                'active' => true
            ]
        );
        $this->addTab(
            'product',
            [
                'label'   => __('Products Apply'),
                'content' => $this->getLayout()->createBlock(
                    'Magenest\CategoryHeader\Block\Adminhtml\Rule\Edit\Tab\Product',
                    'category.header.tab.product'
                )->toHtml(),
                'active' => false
            ]
        );
    }
}
