<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\CategoryHeader\Block\Adminhtml\Rule;

use Magento\Backend\Block\Template;

/**
 * Class Search
 * @package Magenest\CategoryHeader\Block\Adminhtml\Rule
 */
class Search extends Template
{
    /**
     * @var string
     */
    protected $_template = 'category/search/result.phtml';

    protected $path;

    /**
     * Update constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Framework\App\Filesystem\DirectoryList $directory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->path = $directory;
    }

    /**
     * @return string
     */
    public function getPathImage()
    {
        $path = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);;

        return $path;
    }
}
