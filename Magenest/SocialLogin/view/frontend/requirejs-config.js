/*
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    shim:{
        'Magenest_SocialLogin/js/hideshare':{
            'deps':['jquery']
        }
    }
};
