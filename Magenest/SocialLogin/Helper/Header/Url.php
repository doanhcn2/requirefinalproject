<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 09/04/2018
 * Time: 10:34
 */
namespace Magenest\SocialLogin\Helper\Header;
use Magento\Framework\App\Helper\AbstractHelper;

class Url extends AbstractHelper{

    protected $_facebookClient;

    protected $_googleClient;
    public function __construct(
      \Magento\Framework\App\Helper\Context $context,
      \Magenest\SocialLogin\Model\Facebook\Client $facebookClient,
      \Magenest\SocialLogin\Model\Google\Client $googleClient
    )
    {
        $this->_facebookClient = $facebookClient;
        $this->_googleClient = $googleClient;
        parent::__construct($context);
    }
    
    public function getFacebookLoginUrl(){
        return $this->_facebookClient->createAuthUrl();
    }
    
    public function getGoogleLoginUrl(){
        return $this->_googleClient->createAuthUrl();
    }
}