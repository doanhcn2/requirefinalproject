<?php
namespace Magenest\SocialLogin\Helper;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

class SocialLogin extends AbstractHelper
{
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * SocialLogin constructor.
     * @param Session $customerSession
     * @param CustomerFactory $customerFactory
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     */

    public function __construct(
        Session $customerSession,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager,
        Context $context
    ) {
    
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Login and save with customer email
     *
     * @param \Magento\Customer\Model\Customer $customer
     * @param array $data
     */
    public function login($customer, $data)
    {
        $data = array_replace_recursive($customer->getData(), $data);
        $customer->setData($data);
        $customer->save();
        $this->setLogin($customer);
    }

    public function setLogin($customer){
        $cust_repo = \Magento\Framework\App\ObjectManager::getInstance()->create('Magento\Customer\Api\CustomerRepositoryInterface');
        $customerAss = $cust_repo->getById($customer->getId());
        $this->_customerSession->setCustomerDataAsLoggedIn($customerAss);
        $this->_customerSession->regenerateId();
        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }
    }
    /**
     * Create new Customer
     *
     * @param array $data
     */
    public function creatingAccount($data)
    {
        $customer = $this->_customerFactory->create();
        $customer->setData($data);
        $customer->save();
        $this->setLogin($customer);
    }

    /**
     * Get Customer by an attribute
     *
     * @param $id
     * @param $type
     * @return \Magento\Customer\Model\Customer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomers($id, $type)
    {
        $customer = $this->_customerFactory->create()
            ->getResourceCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('magenest_sociallogin_id', $id)
            ->addAttributeToFilter('magenest_sociallogin_type', $type)
            ->getFirstItem();
        return $customer;
    }

    /**
     * Get Customer By Email
     *
     * @param $email
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomerByEmail($email)
    {
        $websiteId = $this->_storeManager->getWebsite()->getId();
        $customer = $this->_customerFactory->create()->setWebsiteId($websiteId)->loadByEmail($email);
        return $customer;
    }


    /**
     * Retrieve cookie manager
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }
}
