<?php

namespace Magenest\SocialLogin\Block;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Share extends Template
{
    /**
     * @var \Magenest\SocialLogin\Model\Share\Share
     */
    protected $_clientShare;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param \Magenest\SocialLogin\Model\Share\Share $clientShare
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magenest\SocialLogin\Model\Share\Share $clientShare,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_clientShare = $clientShare;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isShareEnabled()
    {
        return $this->_clientShare->isEnabled();
    }

    public function getEmailShareUrl()
    {
        $_product = $this->_coreRegistry->registry('current_product');
        if ($_product) {
            return ObjectManager::getInstance()->create('Magento\Catalog\Helper\Product')->getEmailToFriendUrl($_product);
        }
        return '#';
    }
}
