<?php
namespace Magenest\SalesAgent\Plugin\Product;

use Magenest\SalesAgent\Model\AbstractFilter;

class AddFilter extends AbstractFilter
{
    public function beforeLoad(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
        $printQuery = false,
        $logQuery = false
    ) {
        if ($this->isSalesAgent()) {
            $collection->addIdFilter($this->getAssignedProducts());
        }
        return [$printQuery, $logQuery];
    }
}