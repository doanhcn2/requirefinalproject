<?php
namespace Magenest\SalesAgent\Plugin\Vendor;

use Magenest\SalesAgent\Model\AbstractFilter;

class DenyAccess extends AbstractFilter
{
    public function aroundExecute(\Unirgy\Dropship\Controller\Adminhtml\Vendor\Edit $controller, $proceed) {
        $id = $controller->getRequest()->getParam('id');
        if ($id && $this->isSalesAgent()) {
            if (!in_array($id, $this->getAssignedVendors())) {
                /** @var \Magento\Framework\App\Response\Http $response */
                $response = $controller->getResponse();
                $response->setStatusCode(404);
                return $response;
            }
        }
        return $proceed();
    }
}