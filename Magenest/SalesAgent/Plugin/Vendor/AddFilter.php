<?php
namespace Magenest\SalesAgent\Plugin\Vendor;

use Magenest\SalesAgent\Model\AbstractFilter;

class AddFilter extends AbstractFilter
{
    public function beforeLoad(
        \Unirgy\Dropship\Model\ResourceModel\Vendor\Collection $collection,
        $printQuery = false,
        $logQuery = false
    ) {
        if ($this->isSalesAgent()) {
            $collection->addVendorFilter($this->getAssignedVendors());
        }
        return [$printQuery, $logQuery];
    }
}