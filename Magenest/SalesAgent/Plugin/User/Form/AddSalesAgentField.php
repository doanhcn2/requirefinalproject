<?php
namespace Magenest\SalesAgent\Plugin\User\Form;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;

class AddSalesAgentField
{
    protected $_registry;

    public function __construct(
        Registry $registry
    ) {
        $this->_registry = $registry;
    }

    public function beforeGetFormHtml(\Magento\User\Block\User\Edit\Tab\Main $subject)
    {
        try {
            /** @var $model \Magento\User\Model\User */
            $model = $this->_registry->registry('permissions_user');
            $defaultCommission = ObjectManager::getInstance()->get(ScopeConfigInterface::class)->getValue('sales_agent/commission/commission_percent', ScopeInterface::SCOPE_STORE);
            $commission = is_null($model->getCommission()) ? $defaultCommission : $model->getCommission();
            $formFieldset = $subject->getForm()->getElement('base_fieldset');
            if ($formFieldset) {
                $formFieldset->addField(
                    'is_sales_agent',
                    'checkbox',
                    [
                        'name' => 'is_sales_agent',
                        'label' => __('Is Sales Agent'),
                        'id' => 'is_sales_agent',
                        'title' => __('Is Sales Agent'),
                        'checked' => $model->getIsSalesAgent() == 1,
                        'value' => '1'
                    ],
                    "^"
                );
                $formFieldset->addField(
                    'commission',
                    'text',
                    [
                        'name' => 'commission',
                        'label' => __('Commission Percent'),
                        'id' => 'commission',
                        'title' => __('Commission Percent'),
                        'value' => $commission
                    ],
                    "is_sales_agent"
                );
            }
        } catch (\Exception $e) {
        }
    }
}