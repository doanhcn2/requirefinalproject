<?php
/**
 * Author: Eric Quach
 * Date: 3/19/18
 */
namespace Magenest\SalesAgent\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->addIsSalesAgentColumn($setup);
        $this->addAssignedVendorsColumn($setup);
        $this->addAssignedProductsColumn($setup);
        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addIsSalesAgentColumn($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('admin_user'),
            'is_sales_agent',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Is Sales Agent'
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addAssignedVendorsColumn($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('admin_user'),
            'assigned_vendors',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Assigned Vendors'
            ]
        );
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addAssignedProductsColumn($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('admin_user'),
            'assigned_products',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Assigned Products'
            ]
        );
    }

}