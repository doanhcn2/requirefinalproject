<?php
/**
 * Author: Eric Quach
 * Date: 4/14/18
 */
namespace Magenest\SalesAgent\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addCommissionColumn($setup);
        }
        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addCommissionColumn($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('admin_user'),
            'commission',
            [
                'type' => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => 'Commission'
            ]
        );
    }

}