<?php
/**
 * Author: Eric Quach
 * Date: 3/20/18
 */
namespace Magenest\SalesAgent\Model;

use Magento\Backend\Model\Auth\Session;

abstract class AbstractFilter
{
    protected $backendSession;
    protected $backendUserSession;
    protected $assignedVendors;
    protected $assignedProducts;

    public function __construct(
        Session $session
    ) {
        $this->backendSession = $session;
    }

    public function getAssignedVendors()
    {
        if (is_null($this->assignedVendors)) {
            $assignedVendors = $this->getBackendUser()->getAssignedVendors();
            try {
                $this->assignedVendors = array_values(\Zend_Json::decode($assignedVendors));
                return $this->assignedVendors;
            } catch (\Exception $e) {
            }
            $this->assignedVendors = [];
        }
        return $this->assignedVendors;
    }

    public function getAssignedProducts()
    {
        if (is_null($this->assignedProducts)) {
            $assignedProducts = $this->getBackendUser()->getAssignedProducts();
            try {
                $this->assignedProducts = array_values(\Zend_Json::decode($assignedProducts));
                return $this->assignedProducts;
            } catch (\Exception $e) {
            }
            $this->assignedProducts = [];
        }
        return $this->assignedProducts;
    }

    public function isSalesAgent()
    {
        return !!$this->getBackendUser()->getIsSalesAgent();
    }

    public function getBackendUser()
    {
        if (!$this->backendUserSession) {
            $this->backendUserSession = $this->backendSession->getUser();
        }
        return $this->backendUserSession;
    }
}