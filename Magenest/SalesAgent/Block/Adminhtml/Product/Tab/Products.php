<?php
namespace Magenest\SalesAgent\Block\Adminhtml\Product\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as HelperData;
use Magento\Backend\Model\Auth\Session;
use Unirgy\Dropship\Model\ResourceModel\Vendor\Product\Collection;
use Unirgy\Dropship\Model\ResourceModel\Vendor\Product\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\User\Model\UserFactory;

class Products extends \Magento\Backend\Block\Widget\Grid\Extended implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_registry;
    protected $collectionFactory;
    protected $productCollectionFactory;
    protected $userFactory;
    protected $savedSelectedProduct = null;

    public function __construct(
        Registry $registry,
        Context $context,
        HelperData $backendHelper,
        CollectionFactory $collectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        UserFactory $userFactory,
        array $data = []
    )
    {
        $this->_registry = $registry;
        $this->collectionFactory = $collectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->userFactory = $userFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->setId('assigned_products');
        $this->setDefaultSort('product_id');
        $this->setSaveParametersInSession(true);
        $this->setMassactionIdField('entity_id');
        $this->setUseAjax(true);
        $this->savedSelectedProduct = $this->_getSavedSelectedProducts();
    }

    protected function _prepareCollection()
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        /** @var ProductCollection $productCollection */
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('name');

        $productCollection
            ->setRowIdFieldName('vendor_product_id')
            ->getSelect()
            ->joinLeft(
                ['vdp' => $collection->getSelect()],
                'vdp.product_id = e.entity_id'
            )->joinLeft(
                ['vd' => $collection->getTable('udropship_vendor')],
                'vdp.vendor_id = vd.vendor_id',
                ['vendor_name', 'email']
            );

        $this->setCollection($productCollection);

        return parent::_prepareCollection();
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'assigned_product_ids') {
            $assignedProductIds =  array_map('intval', $this->_getSelectedProducts());
            if (empty($assignedProductIds)) {
                $assignedProductIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->getSelect()
                    ->where('e.entity_id IN (?)', $assignedProductIds);
            } else {
                if ($assignedProductIds) {
                    $this->getCollection()->getSelect()
                        ->where('e.entity_id NOT IN (?)', $assignedProductIds);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'assigned_product_ids',
            [
                'header_css_class' => 'data-grid-actions-cell',
                'type' => 'checkbox',
                'header' => __('Assigned'),
                'field_name' => 'assigned[]',
                'values' => $this->_getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id',
                'use_index' => true
            ]
        );
        $this->addColumn(
            'vendor_product_id',
            [
                'header' => __('Vendor Product ID'),
                'sortable' => true,
                'width' => '20',
                'index' => 'vendor_product_id'
            ]
        );
        $this->addColumn(
            'entity_id',
            [
                'header' => __('Product ID'),
                'sortable' => true,
                'width' => '20',
                'index' => 'entity_id'
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name'
            ]
        );
        $this->addColumn(
            'vendor_name',
            [
                'header' => __('Vendor Name'),
                'width' => '80',
                'index' => 'vendor_name'
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'width' => '80',
                'index' => 'sku'
            ]
        );

        $this->addColumn(
            'vendor_email',
            [
                'header' => __('Vendor Email'),
                'index' => 'email'
            ]
        );
        return parent::_prepareColumns();
    }

    protected function _getSelectedProducts()
    {
        if ($assignedParam = $this->getRequest()->getParam('assigned_product_ids')) {
            return $assignedParam;
        } else if ($savedSelectedProducts = $this->_getSavedSelectedProducts()) {
            return $savedSelectedProducts;
        }

        return [];
    }

    protected function _getSavedSelectedProducts()
    {
        if (is_null($this->savedSelectedProduct)) {
            $userId = $this->getRequest()->getParam('user_id');
            $currentUser = $this->userFactory->create()->load($userId);
            if ($currentUser->getId()) {
                $assignedProductIds = $currentUser->getAssignedProducts();
                try {
                    $this->savedSelectedProduct = \Zend_Json::decode($assignedProductIds);
                } catch (\Exception $e) {
                }
            }
            if (!$this->savedSelectedProduct) {
                $this->savedSelectedProduct = [];
            }
        }
        return $this->savedSelectedProduct;
    }

    public function getGridUrl()
    {
        return $this->getUrl('salesagent/product/productgrid', ['_current' => true]);
    }

    public function getTabLabel()
    {
        return __('Assigned Deals');
    }

    public function getTabTitle()
    {
        return __('Assigned Deals');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getAdditionalJavaScript()
    {
        $js = "
            var grid = {$this->getJsObjectName()};
                if (!grid.reloadParams) {
                    grid.reloadParams = {};
                }
                ";
            foreach ($this->_getSavedSelectedProducts() as $id) {
                $js .= "if (!grid.reloadParams['assigned_product_ids[{$id}]']) {
                grid.reloadParams['assigned_product_ids[{$id}]'] = '{$id}';
                }";
            }
        $js .= "
            jQuery('#edit_form').on('submit', function() {
                jQuery('#edit_form').append(
                    jQuery('<input />').attr('type', 'hidden')
                                        .attr('name', 'assigned_product_ids')
                                         .attr('value', JSON.stringify(grid.reloadParams))
                );
            });
        ";
        return $js;
    }

    public function getCheckboxCheckCallback()
    {
        $js = '
            function (grid, element, checked) {
                if (!grid.reloadParams) {
                    grid.reloadParams = {};
                }
                if (checked) {
                    grid.reloadParams[\'assigned_product_ids[\'+element.value+\']\'] = element.value;
                } else {
                    jQuery.each(grid.reloadParams, function(key, value){
                        if (value == element.value) {
                            delete grid.reloadParams[key];
                        }
                    });
                }
            }
        ';
        return $js;
    }

    public function getRowClickCallback()
    {
        $js = "
        function (grid, event) {
        var trElement = Event.findElement(event, 'tr');
        var isInput = Event.element(event).tagName.match(/(input|select|option)/i);
        if (trElement) {
            var checkbox = Element.getElementsBySelector(trElement, 'input');
                if (checkbox[0]) {
                    var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    {$this->getJsObjectName()}.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }";
        return $js;
    }

    public function getRequireJsDependencies()
    {
        return ['jquery'];
    }
}
