<?php
namespace Magenest\SalesAgent\Block\Adminhtml\Vendor\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as HelperData;
use Magento\Backend\Model\Auth\Session;
use Unirgy\Dropship\Model\ResourceModel\Vendor\Collection;
use Unirgy\Dropship\Model\ResourceModel\Vendor\CollectionFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\User\Model\UserFactory;

class Vendors extends \Magento\Backend\Block\Widget\Grid\Extended implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_registry;
    protected $collectionFactory;
    protected $userFactory;
    protected $savedSelectedVendor = null;

    public function __construct(
        Registry $registry,
        Context $context,
        HelperData $backendHelper,
        CollectionFactory $collectionFactory,
        UserFactory $userFactory,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->collectionFactory = $collectionFactory;
        $this->userFactory = $userFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->setId('assigned_vendors');
        $this->setDefaultSort('vendor_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->savedSelectedVendor = $this->_getSavedSelectedVendors();
    }

    protected function _prepareCollection()
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'assigned_vendor_ids') {
            $assignedProductIds =  array_map('intval', $this->_getSelectedVendors());
            if (empty($assignedProductIds)) {
                $assignedProductIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('vendor_id', ['in' => $assignedProductIds]);
            } else {
                if ($assignedProductIds) {
                    $this->getCollection()->addFieldToFilter('vendor_id', ['nin' => $assignedProductIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'assigned_vendor_ids',
            [
                'header_css_class' => 'data-grid-actions-cell',
                'type' => 'checkbox',
                'header' => __('Assigned'),
                'field_name' => 'assigned[]',
                'values' => $this->_getSelectedVendors(),
                'align' => 'center',
                'index' => 'vendor_id',
                'use_index' => true
            ]
        );
        $this->addColumn(
            'vendor_id',
            [
                'header' => __('Vendor ID'),
                'sortable' => true,
                'width' => '20',
                'index' => 'vendor_id'
            ]
        );
        $this->addColumn(
            'vendor_name',
            [
                'header' => __('Vendor Name'),
                'index' => 'vendor_name'
            ]
        );
        $this->addColumn(
            'vendor_email',
            [
                'header' => __('Email'),
                'width' => '80',
                'index' => 'email'
            ]
        );
        $this->addColumn(
            'telephone',
            [
                'header' => __('Telephone'),
                'index' => 'telephone'
            ]
        );
        return parent::_prepareColumns();
    }

    protected function _getSelectedVendors()
    {
        if ($assignedParam = $this->getRequest()->getParam('assigned_vendor_ids')) {
            return $assignedParam;
        } else if ($savedSelectedProducts = $this->_getSavedSelectedVendors()) {
            return $savedSelectedProducts;
        }

        return [];
    }

    protected function _getSavedSelectedVendors()
    {
        if (is_null($this->savedSelectedVendor)) {
            $userId = $this->getRequest()->getParam('user_id');
            $currentUser = $this->userFactory->create()->load($userId);
            if ($currentUser->getId()) {
                $assignedVendorIds = $currentUser->getAssignedVendors();
                try {
                    $this->savedSelectedVendor = \Zend_Json::decode($assignedVendorIds);
                } catch (\Exception $e) {
                }
            }
            if (!$this->savedSelectedVendor) {
                $this->savedSelectedVendor = [];
            }
        }
        return $this->savedSelectedVendor;
    }

    public function getGridUrl()
    {
        return $this->getUrl('salesagent/vendor/vendorgrid', ['_current' => true]);
    }

    public function getTabLabel()
    {
        return __('Assigned Vendors');
    }

    public function getTabTitle()
    {
        return __('Assigned Vendors');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getAdditionalJavaScript()
    {
        $js = "
            var grid = {$this->getJsObjectName()};
                if (!grid.reloadParams) {
                    grid.reloadParams = {};
                }
                ";
            foreach ($this->_getSavedSelectedVendors() as $id) {
                $js .= "if (!grid.reloadParams['assigned_vendor_ids[{$id}]']) {
                grid.reloadParams['assigned_vendor_ids[{$id}]'] = '{$id}';
                }";
            }
        $js .= "
            jQuery('#edit_form').on('submit', function() {
                jQuery('#edit_form').append(
                    jQuery('<input />').attr('type', 'hidden')
                                        .attr('name', 'assigned_vendor_ids')
                                         .attr('value', JSON.stringify(grid.reloadParams))
                );
            });
        ";
        return $js;
    }

    public function getCheckboxCheckCallback()
    {
        $js = '
            function (grid, element, checked) {
                if (!grid.reloadParams) {
                    grid.reloadParams = {};
                }
                if (checked) {
                    grid.reloadParams[\'assigned_vendor_ids[\'+element.value+\']\'] = element.value;
                } else {
                    jQuery.each(grid.reloadParams, function(key, value){
                        if (value == element.value) {
                            delete grid.reloadParams[key];
                        }
                    });
                }
                console.log(grid.reloadParams);
            }
        ';
        return $js;
    }

    public function getRowClickCallback()
    {
        $js = "
        function (grid, event) {
        var trElement = Event.findElement(event, 'tr');
        var isInput = Event.element(event).tagName.match(/(input|select|option)/i);
        if (trElement) {
            var checkbox = Element.getElementsBySelector(trElement, 'input');
                if (checkbox[0]) {
                    var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    {$this->getJsObjectName()}.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }";
        return $js;
    }

    public function getRequireJsDependencies()
    {
        return ['jquery'];
    }
}
