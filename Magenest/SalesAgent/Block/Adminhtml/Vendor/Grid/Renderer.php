<?php
namespace Magenest\SalesAgent\Block\Adminhtml\Vendor\Grid;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Renderer extends AbstractRenderer
{
    public function render(DataObject $row)
    {
        $href = $this->getUrl('salesagent/vendor/loginas', ['vendor_id' => $row->getVendorId()]);
        $html = "<a target='_blank' href='{$href}'>Login as Vendor</a>";
        return $html;
    }

}