<?php
namespace Magenest\SalesAgent\Block\Adminhtml\Payout\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as HelperData;
use Magento\Framework\Registry;
use Unirgy\Dropship\Helper\Data as DropshipHelperData;
use Unirgy\DropshipPayout\Block\Adminhtml\Payout\Edit\Tab\Rows;
use Unirgy\DropshipPayout\Model\Payout\RowFactory;
use Unirgy\DropshipPayout\Model\PayoutFactory;
use Magenest\SalesAgent\Model\Filter;

class Payouts extends Rows
{
    protected $filter;

    public function __construct(
        Context $context,
        HelperData $backendHelper,
        Registry $frameworkRegistry,
        PayoutFactory $modelPayoutFactory,
        RowFactory $payoutRowFactory,
        DropshipHelperData $helperData,
        Filter $filter,
        array $data = []
    )
    {
        parent::__construct($context, $backendHelper, $frameworkRegistry, $modelPayoutFactory, $payoutRowFactory, $helperData, $data);
        $this->filter = $filter;
        $this->setUseAjax(false);
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumnAfter(
            'order_created_at',
            [
                'header' => __('Order Date'),
                'index' => 'order_created_at',
                'type' => 'datetime'
            ],
            'po_increment_id'
        );
        $this->addColumnAfter(
            'po_created_at',
            [
                'header' => __('PO Date'),
                'index' => 'po_created_at',
                'type' => 'datetime'
            ],
            'po_increment_id'
        );
        $this->addColumnAfter(
            'po_statement_date',
            [
                'header' => __('PO Ready Date'),
                'index' => 'po_statement_date',
                'type' => 'datetime'
            ],
            'po_increment_id'
        );

        return $this;
    }


    protected function _prepareCollection()
    {
        $collection = $this->_payoutRowFactory->create()->getCollection();
        $collection->getSelect()
            ->joinInner(
                ['udpoi' => $collection->getTable('udropship_po_item')],
                'main_table.po_item_id = udpoi.entity_id',
                'udpoi.product_id'
            );
        if ($this->filter->isSalesAgent()) {
            $assignedProducts = array_map('intval', $this->filter->getAssignedProducts());
            $collection->getSelect()
                ->where('udpoi.product_id IN (?)', $assignedProducts);
        }
        $this->setCollection($collection);

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }

    public function getGridUrl()
    {
        return $this->getAbsoluteGridUrl();
    }
}