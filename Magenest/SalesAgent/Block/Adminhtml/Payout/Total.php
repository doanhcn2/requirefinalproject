<?php
/**
 * Author: Eric Quach
 * Date: 3/20/18
 */
namespace Magenest\SalesAgent\Block\Adminhtml\Payout;

use Magenest\SalesAgent\Model\Filter;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\DB\Select;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipPayout\Model\Payout\RowFactory;

class Total extends Template
{
    protected $filter;
    protected $_payoutRowFactory;
    protected $payoutCollection;
    protected $siteTotal;
    protected $localeCurrency;

    public function __construct(
        Context $context,
        RowFactory $payoutRowFactory,
        Filter $filter,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->filter = $filter;
        $this->_payoutRowFactory = $payoutRowFactory;
        $this->localeCurrency = $localeCurrency;
    }

    public function getLocaleCurrency()
    {
        return $this->localeCurrency;
    }

    public function getBaseCurrency()
    {
        return $this->_scopeConfig->getValue('currency/options/base');
    }

    public function getPayoutCollection()
    {
        if (is_null($this->payoutCollection)) {
            /** @var \Magenest\SalesAgent\Block\Adminhtml\Payout\Tab\Payouts $payoutRows */
            $payoutRows = $this->getLayout()->getBlock('payout_report');
            if ($payoutRows instanceof \Magento\Framework\View\Element\AbstractBlock) {
                $payoutRows = clone $payoutRows;
                $payoutRows->toHtml();
                $collection = $payoutRows->getPreparedCollection();
            } else {
                throw new \Exception('Missing Payout Report Block');
            }

            $this->payoutCollection = $collection;
        }

        return $this->payoutCollection;
    }

    public function getSiteTotalEarned()
    {
        if (is_null($this->siteTotal)) {
            $select = $this->getPayoutCollection()->getSelect()
                ->reset(Select::COLUMNS)
                ->columns(['total_commission' => 'SUM(com_amount)']);
            $total = $this->getPayoutCollection()->getConnection()->fetchRow($select);
            $this->siteTotal = reset($total);
            if (!$this->siteTotal) {
                $this->siteTotal = 0;
            }
        }
        return $this->siteTotal;
    }

    public function getCommissionPercent()
    {
        $user = $this->filter->getBackendUser();
        $defaultCommissionPercent = $this->_scopeConfig->getValue('sales_agent/commission/commission_percent', ScopeInterface::SCOPE_STORE);
        $commissionPercent = $user ? !is_null($user->getCommission()) ? $user->getCommission() : $defaultCommissionPercent : $defaultCommissionPercent;
        return $commissionPercent;
    }

    public function getSelfTotalEarned()
    {
        return round($this->getSiteTotalEarned() * $this->getCommissionPercent() / 100, 2);
    }
}