<?php
namespace Magenest\SalesAgent\Observer\AdminUser;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveIsSalesAgent implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            $user = $observer->getDataObject();
            $isSalesAgent = $user->getIsSalesAgent();
            if ($isSalesAgent) {
                $user->setIsSalesAgent(1);
            } else {
                $user->setIsSalesAgent(0);
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }

}