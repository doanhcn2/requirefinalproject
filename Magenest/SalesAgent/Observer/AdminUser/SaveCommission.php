<?php
namespace Magenest\SalesAgent\Observer\AdminUser;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveCommission implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            $user = $observer->getDataObject();
            $commission = $user->getCommission();
            if ($commission && $commission >= 0 && $commission <= 100) {
                $user->setCommission($commission);
            } else {
                $user->setCommission(0);
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }

}