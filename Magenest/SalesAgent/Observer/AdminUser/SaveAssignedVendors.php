<?php
namespace Magenest\SalesAgent\Observer\AdminUser;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveAssignedVendors implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            $user = $observer->getDataObject();
            $assignedVendorIds = $user->getAssignedVendorIds();
            if (!is_null($assignedVendorIds)) {
                $user->setAssignedVendors($assignedVendorIds);
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }

}