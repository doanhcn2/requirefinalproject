<?php
namespace Magenest\SalesAgent\Observer\AdminUser;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class SaveAssignedProducts implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        try {
            $user = $observer->getDataObject();
            $assignedProductIds = $user->getAssignedProductIds();
            if (!is_null($assignedProductIds)) {
                $user->setAssignedProducts($assignedProductIds);
            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical($e);
        }
    }

}