<?php
namespace Magenest\SalesAgent\Observer\Vendor;

use Magenest\SalesAgent\Model\AbstractFilter;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Unirgy\Dropship\Model\Vendor;

class Filter extends AbstractFilter implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        /** @var Vendor $vendor */
        $vendor = $observer->getVendor();
        if ($vendor->getId() &&
            $this->isSalesAgent() &&
            $this->getAssignedVendors()
        ) {
            if (!in_array($vendor->getId(), $this->getAssignedVendors())) {
                $vendor->unsetData();
            }
        }
    }

}