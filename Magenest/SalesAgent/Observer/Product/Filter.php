<?php
namespace Magenest\SalesAgent\Observer\Product;

use Magenest\SalesAgent\Model\AbstractFilter;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Filter extends AbstractFilter implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        /** @var Product $vendor */
        $product = $observer->getProduct();
        if ($product->getId() &&
            $this->isSalesAgent() &&
            $this->getAssignedProducts()
        ) {
            if (!in_array($product->getId(), $this->getAssignedProducts())) {
                $product->unsetData();
            }
        }
    }

}