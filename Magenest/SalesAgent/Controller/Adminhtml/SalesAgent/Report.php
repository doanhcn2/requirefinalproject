<?php
/**
 * Author: Eric Quach
 * Date: 3/20/18
 */
namespace Magenest\SalesAgent\Controller\Adminhtml\SalesAgent;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Report extends Action
{
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->addBreadcrumb(__('Sales Report'), __('Sales Report'));
        $resultPage->getConfig()->getTitle()->prepend(__('Sales Report'));
        return $resultPage;
    }
}