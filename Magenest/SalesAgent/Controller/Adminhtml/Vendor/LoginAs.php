<?php
/**
 * Author: Eric Quach
 * Date: 3/21/18
 */
namespace Magenest\SalesAgent\Controller\Adminhtml\Vendor;

use Magenest\SalesAgent\Model\Filter;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Url;
use Unirgy\Dropship\Model\Session;
use Magento\Backend\Model\Url as BackendUrlBuilder;
use Unirgy\Dropship\Model\VendorFactory;

class LoginAs extends Action
{
    protected $vendorSession;
    protected $frontendUrlBuilder;
    protected $vendorFactory;
    protected $filter;

    public function __construct(
        Action\Context $context,
        Session $vendorSession,
        Url $url,
        VendorFactory $vendorFactory,
        Filter $filter
    ) {
        parent::__construct($context);
        $this->vendorSession = $vendorSession;
        $this->frontendUrlBuilder = $url;
        $this->vendorFactory = $vendorFactory;
        $this->filter = $filter;
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $vendorId = $this->getRequest()->getParam('vendor_id');
        if (!$vendorId) {
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        $this->vendorSession->setVendorAsLoggedIn($this->vendorFactory->create()->load($vendorId));
        $resultRedirect->setUrl($this->frontendUrlBuilder->getUrl('overview/index/index'));
        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_isUserAllowed() && $this->_authorization->isAllowed('Unirgy_Dropship::vendor') && parent::_isAllowed();
    }

    protected function _isUserAllowed()
    {
        if ($this->filter->isSalesAgent()) {
            if (!in_array($this->getRequest()->getParam('vendor_id'), $this->filter->getAssignedVendors())) {
                return false;
            }
        }
        return true;
    }
}