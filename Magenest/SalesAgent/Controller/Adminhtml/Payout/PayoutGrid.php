<?php
namespace Magenest\SalesAgent\Controller\Adminhtml\Payout;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class PayoutGrid extends Action
{
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
