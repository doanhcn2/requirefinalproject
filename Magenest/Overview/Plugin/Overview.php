<?php

namespace Magenest\Overview\Plugin;

class Overview
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function aroundExecute(
        \Unirgy\Dropship\Controller\Vendor\index $subject,
        \Closure $proceed
    )
    {
        return $this->resultRedirectFactory->create()->setPath('overview');
    }
}