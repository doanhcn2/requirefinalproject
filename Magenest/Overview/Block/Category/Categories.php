<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\Overview\Block\Category;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class Categories extends Template
{
    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context, $data);
    }

    public function getCategoriesTree()
    {
        $objectManager = ObjectManager::getInstance();
        $tree = $objectManager->create('Magento\Catalog\Block\Adminhtml\Category\Tree')->getTree();

        return $tree[0]['children'];
    }

    public function renderChildrenBlock($category)
    {
        $result = '';
        $result .= '<div class="grouped-list">';
        $result .= '<ul class="side-nav">';
        foreach($category as $item) {
            if (isset($item['children']) && count($item['children']) > 0) {
                $plus = '<i class="fa fa-plus"></i>';
                $class = 'class="parent"';
                $classA = 'class="sub-shop-title"';
            } else {
                $plus = '';
                $class = '';
                $classA = '';
            }
            $result .= '<li ' . $class . '>';
            $result .= '<a ' . $classA . ' href="' . $this->getCategoryLink($item['id']) . '">' . $plus . $this->prepareCategoryName($item['text']) . '</a>';
            if (isset($item['children']) && count($item['children']) > 0) {
                $result .= $this->getGrandChildBlock($item['children']);
            }
            $result .= '</li>';
        }
        $result .= '</ul>';
        $result .= '</div>';

        return $result;
    }

    public function getGrandChildBlock($category)
    {
        $result = '';
        $result .= '<ul class="sub-shop-list">';
        foreach($category as $item) {
            if (isset($item['children']) && count($item['children']) > 0) {
                $plus = '<i class="fi-plus"></i>';
                $class = 'class="parent"';
                $classA = 'class="sub-shop-title"';
            } else {
                $plus = '';
                $class = '';
                $classA = '';
            }
            $result .= '<li ' . $class . '>';
            $result .= '<a ' . $classA . ' href="' . $this->getCategoryLink($item['id']) . '">' . $plus . $this->prepareCategoryName($item['text']) . '</a>';
            if (isset($item['children']) && count($item['children']) > 0) {
                $result .= $this->getGrandChildBlock($item['children']);
            }
            $result .= '</li>';
        }
        $result .= '</ul>';

        return $result;
    }

    public function getCategoryLink($id)
    {
        return $this->_categoryFactory->create()->load($id)->getUrl();
    }

    public function prepareCategoryName($name)
    {
        $strings = explode('(', $name);

        if (isset($strings[0])) {
            return $strings[0];
        } else {
            return $name;
        }
    }
}
