<?php

namespace Magenest\Overview\Block\Index;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Unirgy\Dropship\Model\ResourceModel\Vendor\Product\Collection;
use Unirgy\Dropship\Model\Vendor;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class PaymentChart extends Template
{
    /**
     * @var \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\CollectionFactory
     */
    protected $vendorProductCollectionFactory;

    /**
     * @var \Magenest\Groupon\Model\GrouponFactory
     */
    protected $grouponFactory;

    /**
     * @var \Magenest\Ticket\Helper\Information
     */
    protected $_ticketHelper;

    /**
     * @var \Magenest\SellYours\Helper\Dashboard
     */
    protected $_simpleHelper;

    /**
     * @var \Magenest\Hotel\Helper\Dashboard
     */
    protected $_hotelHelper;

    /**
     * @var \Magento\Reports\Model\EventFactory
     */
    protected $_magentoEventFactory;

    /**
     * @var HelperData
     */
    protected $_rateHlp;

    public function __construct(
        Context $context,
        \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\CollectionFactory $collectionFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\SellYours\Helper\Dashboard $simpleHelper,
        \Magenest\Ticket\Helper\Information $ticketHelper,
        \Magento\Reports\Model\EventFactory $eventFactory,
        \Magenest\Hotel\Helper\Dashboard $hotelHelper,
        HelperData $helperData,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_rateHlp = $helperData;
        $this->_hotelHelper = $hotelHelper;
        $this->_ticketHelper = $ticketHelper;
        $this->_simpleHelper = $simpleHelper;
        $this->grouponFactory = $grouponFactory;
        $this->_magentoEventFactory = $eventFactory;
        $this->vendorProductCollectionFactory = $collectionFactory;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getDay($daynum)
    {
        $date = new \DateTime();
        if ($daynum === "all_time") {
            $startTime = $this->getVendor()->getCreatedAt();
            if ($startTime === null) {
                $startTime = '2018-01-01 00:00:00';
            }
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);

            return $startTime->format('Y-m-d');
        } elseif ($daynum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));

            return $startTime->format('Y-m-d');
        } else {
            $date->sub(new \DateInterval('P' . $daynum . 'D'));
        }

        return $date->format('Y-m-d');
    }

    public function getSoldFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $collection;
    }

    /**
     * @return $this|\Magenest\Hotel\Helper\Dashboard|\Magenest\SellYours\Helper\Dashboard|\Magenest\Ticket\Helper\Information
     */
    public function getBlockFixed()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return $this->_ticketHelper;
        elseif ($productType == 2)
            return $this->_hotelHelper;
        elseif ($productType == 4)
            return $this->_simpleHelper;
        return $this;
    }

    public function getSoldInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection->getColumnValues('created_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getSoldInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('created_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('price') * $item->getData('qty');
            else
                $arr[$key] += $item->getData('price') * $item->getData('qty');
        }
        return $arr;
    }

    public function getRedeemedFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('redeemed_at', array('gt' => $day));
        return $collection;
    }

    public function getRefundedFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('refunded_at', array('gt' => $day));
        return $collection;
    }

    public function getRefundedInDay($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection->getColumnValues('refunded_at') as $value) {
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = 1;
            else
                $arr[$key]++;
        }
        return $arr;
    }

    public function getRefundedInDayValue($collection)
    {
        $arr = [];
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        foreach ($collection as $item) {
            $key = explode(' ', $item->getData('refunded_at'))[0];
            if (!isset($arr[$key]))
                $arr[$key] = $item->getData('price') * $item->getData('qty');
            else
                $arr[$key] += $item->getData('price') * $item->getData('qty');
        }
        return $arr;
    }

    public function getOption($daynum)
    {
        $arr = [];
        $daystart = new \DateTime();
        if (is_integer($daynum)) {
            $dayCount = (int)$daynum;
        } else {
            $dayCount = floor((time() - strtotime($this->getDay($daynum))) / 86400);
            $dayCount = (int)$dayCount;
        }
        $date = $daystart->sub(new \DateInterval('P' . $dayCount . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $soldInDays = $this->getSoldInDay($this->getSoldFromDay($dayCount));
        $soldInDaysValue = $this->getSoldInDayValue($this->getSoldFromDay($dayCount));;
        $refundInDays = $this->getRefundedInDay($this->getRefundedFromDay($dayCount));
        $refundInDaysValue = $this->getRefundedInDayValue($this->getRefundedFromDay($dayCount));
        for ($i = 0; $i < $dayCount; $i++) {
            $date->add(new \DateInterval('P1D'));
            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [$dateText, $sales, $qtySale, $refunds, $qtyRefund, 'color: #017cb9; stroke-color: #017cb9'];
        }
        return [$startDateText, $dateText, $arr];
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getProductTypeTitle()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return "Events";
        elseif ($productType == 2)
            return "Hotel";
        elseif ($productType == 4)
            return "Inventory";
        return "Campaigns";
    }

    public function getProductCount($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->vendorProductCollectionFactory->create()
            ->addFieldToFilter('vendor_id', $this->getVendorId());
        $ids = $collection->getColumnValues('product_id');
        /** @var  $eventCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $eventCollection = $this->_magentoEventFactory->create()->getCollection();
        $eventCollection->addFieldToFilter('event_type_id', \Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW)
            ->addFieldToFilter('object_id', ['in' => $ids])
            ->addFieldToFilter('logged_at', array('gt' => $day));
        return count($eventCollection);
    }

    public function getFeedbackCount($daynum)
    {
        $day = $this->getDay($daynum);
        $reviewCollection = $this->_rateHlp->getVendorReviewsCollection($this->getVendorId());
        $reviewCollection->addFieldToFilter('created_at', array('gt' => $day));
        return count($reviewCollection);
    }
}
