<?php

namespace Magenest\Overview\Block\Index;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Payment extends Template
{
    protected $payout;

    /**
     * @var $_udropshipVendor \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_udropshipVendor;

    public function __construct(
        Context $context,
        \Unirgy\DropshipPayout\Model\PayoutFactory $payoutFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        array $data = [])
    {
        $this->_udropshipVendor = $vendorFactory;
        $this->payout = $payoutFactory;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getPayoutCollection()
    {
        return $this->payout->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->setPageSize(3)
            ->setCurPage(1)
            ->setOrder('created_at', 'DES');
    }

    public function getVendorPromoteProduct()
    {
        return $this->_udropshipVendor->create()->load($this->getVendorId())->getPromoteProduct();
    }
}
