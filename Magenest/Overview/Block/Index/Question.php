<?php

namespace Magenest\Overview\Block\Index;

use Magento\Framework\App\ObjectManager;
use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;

class Question extends Template
{
    const NUMBER_QUESTION_SHOW = 3;
    /**
     * @var $_question QuestionFactory
     */
    protected $_question;

    /**
     * @var $_udropshipVendor \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_udropshipVendor;

    /**
     * Question constructor.
     * @param Context $context
     * @param QuestionFactory $questionFactory
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        QuestionFactory $questionFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        array $data = []
    ) {
        $this->_udropshipVendor = $vendorFactory;
        $this->_question = $questionFactory;
        parent::__construct($context, $data);
    }

    public function getRecentQuestion()
    {
        /**
         * @var \Unirgy\DropshipVendorAskQuestion\Model\ResourceModel\Question\Collection $questions
         */
        $questions = $this->_question->create()->getCollection();
        $allData = $questions->addFieldToFilter('question_status', '1')
            ->addFieldToFilter('answer_text', ['null' => true])
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addOrder('question_date', 'DESC')->getData();
        $count = 0;
        $result = [];
        foreach($allData as $item) {
            if ($count < self::NUMBER_QUESTION_SHOW) {
                array_push($result, $item);
            } else break;
            $count++;
        }
        return $result;
    }

    public function getQuestionUrl()
    {
        return $this->getUrl('questions/index/index');
    }

    public function getVendorId()
    {
        return ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getVendorPromoteProduct()
    {
        return $this->_udropshipVendor->create()->load($this->getVendorId())->getPromoteProduct();
    }
}
