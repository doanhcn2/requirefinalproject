<?php

namespace Magenest\Overview\Block\Index;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Campaign extends Template
{
    protected $grouponFactory;
    /** @var \Magenest\Groupon\Model\Deal $dealFactory */
    protected $dealFactory;

    protected $productFactory;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\Collection */
    protected $creditmemoItemCollectionFactory;

    protected $activeCampaignCollectionIds;

    protected $_ticketHelper;
    protected $_hotelHelper;
    protected $_simpleHelper;

    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Hotel\Helper\Dashboard $hotelHelper,
        \Magenest\SellYours\Helper\Dashboard $dashboardSimple,
        \Magenest\Ticket\Helper\Information $ticketHelper,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\CollectionFactory $creditmemoItemCollectionFactory,
        array $data = [])
    {
        $this->grouponFactory = $grouponFactory;
        $this->creditmemoItemCollectionFactory = $creditmemoItemCollectionFactory;
        $this->dealFactory = $dealFactory;
        $this->_simpleHelper = $dashboardSimple;
        $this->_ticketHelper = $ticketHelper;
        $this->_hotelHelper = $hotelHelper;
        $this->productFactory = $productFactory;
        parent::__construct($context, $data);
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getBlockFixed()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return $this->_ticketHelper;
        elseif ($productType == 2)
            return $this->_hotelHelper;
        elseif ($productType == 4)
            return $this->_simpleHelper;
        return $this;
    }

    public function getActiveCampaignCollection()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productFactory->create()->getCollection();
        return $collection->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->addFieldToFilter('type_id', 'configurable')->addAttributeToSelect(['campaign_status'])->addFieldToFilter('campaign_status', 5);
    }

    public function getActiveCampaignCollectionIds()
    {
        if (!$this->activeCampaignCollectionIds) {
            /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $collection */
            $collection = $this->dealFactory->create()->getCollection();
            $this->activeCampaignCollectionIds = $collection
                ->addFieldToFilter('product_type', 'coupon')
                ->addFieldToFilter('vendor_id', $this->getVendorId())
                ->addFieldToFilter('groupon_expire', array('gteq' => date('Y-m-d')))
                ->addFieldToFilter('enable', '1')->getColumnValues('product_id');
        }
        return $this->activeCampaignCollectionIds;
    }

    public function getSold($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('refunded_at', array('null' => true));
        /** @var \Magenest\Groupon\Model\Groupon $item */
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count;
    }

    public function getProductTypeTitle()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return "Events";
        if ($productType == 2)
            return "Hotel";
        if ($productType == 4)
            return "Inventory";
        return "Campaigns";
    }

    public function getRedeemed($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('status', '2');
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count;
    }

    public function getRemaining($ids)
    {
        $count = 0;
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('status', '1');
        foreach ($collection as $item)
            $count += (int)$item->getQty();
        return $count - $this->getRefunded($ids);
    }

    public function getRefunded($ids)
    {
        $count = 0;
        /** @var \Magento\Sales\Model\ResourceModel\Order\Creditmemo\Item\Collection $collection */
        $collection = $this->creditmemoItemCollectionFactory->create();
        $collection->addFieldToFilter('product_id', ['in' => $ids]);
        /** @var \Magento\Sales\Model\Order\Creditmemo\Item $item */
        foreach ($collection as $item) {
            $count += (int)$item->getQty();
        }
        return $count;
    }
}