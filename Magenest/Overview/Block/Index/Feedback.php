<?php

namespace Magenest\Overview\Block\Index;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class Feedback extends Template
{
    const NUMBER_FEEDBACK_SHOW = 2;
    /**
     * @var HelperData
     */
    protected $_rateHlp;
    /**
     * @var \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection
     */
    protected $_reviewCollection;

    /**
     * @var $_udropshipVendor \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_udropshipVendor;

    /**
     * Feedback constructor.
     * @param Context $context
     * @param HelperData $helperData
     * @param \Unirgy\Dropship\Model\VendorFactory $vendorFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
        array $data = []
    )
    {
        $this->_udropshipVendor = $vendorFactory;
        $this->_rateHlp = $helperData;
        parent::__construct($context, $data);
    }

    public function getReviewsCollection()
    {
        if (null === $this->_reviewCollection) {
            $this->_reviewCollection = $this->_rateHlp->getVendorReviewsCollection($this->getVendorId());
            $this->_reviewCollection->setPageSize(self::NUMBER_FEEDBACK_SHOW)->setCurPage(1);
        }
        return $this->_reviewCollection;
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getFeedbackUrl(){
        return $this->getUrl('questions/rating/vendor');
    }

    public function getVendorPromoteProduct()
    {
        return $this->_udropshipVendor->create()->load($this->getVendorId())->getPromoteProduct();
    }
}