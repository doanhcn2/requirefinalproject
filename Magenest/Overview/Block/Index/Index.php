<?php

namespace Magenest\Overview\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Unirgy\DropshipVendorRatings\Helper\Data as HelperData;

class Index extends PaymentChart
{
    /**
     * @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection|null
     */
    protected $grouponTodayCollection = null;

    /**
     * @var \Magenest\Groupon\Model\Deal $dealFactory
     */
    protected $dealFactory;

    /**
     * @var
     */
    protected $dealActive;

    /**
     * @var
     */
    protected $_rateHlp;

    /**
     * @var \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection
     */
    protected $_reviewCollection;

    /**
     * @var QuestionFactory
     */
    protected $_question;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magenest\Payments\Block\Vendor\Dashboard
     */
    protected $_paymentDashboard;

    public function __construct(
        Context $context,
        \Unirgy\Dropship\Model\ResourceModel\Vendor\Product\CollectionFactory $collectionFactory,
        \Magenest\Payments\Block\Vendor\Dashboard $paymentDashboard,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\SellYours\Helper\Dashboard $simpleHelper,
        \Magenest\Ticket\Helper\Information $ticketHelper,
        \Magento\Reports\Model\EventFactory $eventFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Hotel\Helper\Dashboard $hotelHelper,
        QuestionFactory $questionFactory,
        HelperData $helperData,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $collectionFactory,
            $grouponFactory,
            $simpleHelper,
            $ticketHelper,
            $eventFactory,
            $hotelHelper,
            $helperData,
            $data
        );
        $this->_paymentDashboard = $paymentDashboard;
        $this->_productFactory = $productFactory;
        $this->_question = $questionFactory;
        $this->dealFactory = $dealFactory;
    }

    public function getTodaySale()
    {
        $return = 0;
        $collection = $this->getGrouponTodayCollection()->getColumnValues('price');
        foreach ($collection as $value)
            $return += (float)$value;
        return $return;
    }

    public function getVendor()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendor();
    }

    public function getProductTypeTitle()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return "tickets";
        elseif ($productType == 2)
            return "reservations";
        elseif ($productType == 4)
            return "units";
        return "coupons";
    }

    public function getTodayRedeemed()
    {
        $collection = $this->getGrouponTodayCollection()->addFieldToFilter('status', \Magenest\Groupon\Model\Groupon::STATUS_USED);
        return count($collection->getData());
    }

    public function getSoldOutOption()
    {
        $productIds = $this->getActiveCampaignCollection()->getData();
        $ids = [];
        foreach($productIds as $productId) {
            if (@$productId['product_id']) {
                array_push($ids, $productId['product_id']);
            }
        }
        $collection = $this->dealFactory->create()->getCollection()
            ->addFieldToFilter('parent_id', ['in' => $ids])
            ->addFieldToFilter('available_qty', ['lt' => 1]);
        return count($collection->getData());
    }

    public function getActiveCampaignCollection()
    {
        if ($this->dealActive == null) {
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $this->_productFactory->create()->getCollection();
            $collection->addAttributeToSelect(['campaign_status', 'udropship_vendor'])->addFieldToFilter('udropship_vendor', $this->getVendorId())
                ->addFieldToFilter('type_id', 'configurable')->addFieldToFilter('campaign_status', 5);
            $dealTable = $collection->getTable('magenest_groupon_deal');
            $collection->getSelect()->joinLeft(
                $dealTable,
                "entity_id = $dealTable.product_id",
                "*"
            );
            $this->dealActive = $collection;
        }
        return $this->dealActive;
    }

    public function getGrouponTodayCollection()
    {
        if (!$this->grouponTodayCollection) {
            $date = new \DateTime();
            $today = $date->format('Y-m-d');
            /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
            $this->grouponTodayCollection = $this->grouponFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $this->getVendorId())
                ->addFieldToFilter('created_at', array('like' => "%$today%"));
        }
        return $this->grouponTodayCollection;
    }

    public function getReviewsCollection()
    {
        if (null === $this->_reviewCollection) {
            $this->_reviewCollection = $this->_rateHlp->getVendorReviewsCollection($this->getVendorId());
        }
        $total = 0;
        $i = 0;
        foreach ($this->_reviewCollection as $review) {
            $_votes = [];
            foreach ($review->getRatingVotes() as $_vote) {
                if ($_vote->getIsAggregate()) {
                    $_votes[] = $_vote;
                }
            }
            foreach ($_votes as $vote) {
                $total += $vote->getPercent();
                $i++;
            }
        }

        return $i == 0 ? 0 : number_format(($total * 5) / (100 * $i), 1);
    }

    public function getPendingBalance()
    {
        return $this->_paymentDashboard->getOwed();
    }

    public function countProductsSaleToday()
    {
        $productType = $this->getVendor()->getPromoteProduct();
        if ($productType == 3)
            return count($this->_ticketHelper->getTicketCollectionSaleToday());
        if ($productType == 2)
            return count($this->_hotelHelper->getReservationsCollectionSaleToday());
        if ($productType == 4)
            return $this->_simpleHelper->getUnitsSaleToday();
        return count($this->getGrouponTodayCollection());
    }

    public function getCurrentCurrency()
    {
        $storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Store\Model\StoreManagerInterface');
        return $storeManager->getStore()->getCurrentCurrencyCode();
    }
}
