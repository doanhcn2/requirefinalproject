<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\SellYours\Helper;

use Magento\Framework\App\ObjectManager;

class PriceHelper
{
    /**
     * @var $_microSiteHelper \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $_microSiteHelper;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    public function __construct(
        \Unirgy\DropshipMicrosite\Helper\Data $microSite,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\Dropship\Model\VendorFactory $vendorFactory
    ) {
        $this->_vendorFactory = $vendorFactory;
        $this->_productFactory = $productFactory;
        $this->_microSiteHelper = $microSite;
    }

    public function getSimpleProductPrice($productId)
    {
        $result = [];
        $product = $this->_productFactory->create()->load($productId);
        $bestVendor = $product->getUdmultiBestVendor();
        $allUdmultiVendorData = $product->getAllMultiVendorData();
        $vendorMicroSiteId = $this->checkMicroSitePage($allUdmultiVendorData);
        if (!$vendorMicroSiteId) {
            $vendorMicroSiteId = $bestVendor;
        }
        foreach ($allUdmultiVendorData as $key => $vendorData) {
            if (intval($vendorMicroSiteId) !== $key) {
                continue;
            }
            if ($vendorData['special_price']) {
                $specialFrom = $vendorData['special_from_date'];
                $specialTo = $vendorData['special_to_date'];
                if ($specialFrom === null && $specialTo !== null) {
                    $result = $this->compareVendorPrice($vendorData, (time() < strtotime($specialTo)));
                } elseif ($specialTo === null && $specialFrom !== null) {
                    $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom)));
                } elseif ($specialFrom === null && $specialTo === null) {
                    $result['old_price'] = $vendorData['vendor_price'];
                    $result['current_price'] = $vendorData['special_price'];
                } else {
                    $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom) && time() < strtotime($specialTo)));
                }
            } else {
                $result['current_price'] = $vendorData['vendor_price'];
            }
        }
        if (!isset($result['current_price'])) {
            $result['current_price'] = $product->getFinalPrice();
        }

        return $result;
    }

    private function compareVendorPrice($vendorData, $compare)
    {
        $result = [];
        if ($compare) {
            $result['old_price'] = $vendorData['vendor_price'];
            $result['current_price'] = $vendorData['special_price'];
        } else {
            $result['current_price'] = $vendorData['vendor_price'];
        }

        return $result;
    }

    private function checkMicroSitePage($allUdmultiData)
    {
        $vendors = $this->getVendorIdsFromUdmultiData($allUdmultiData);
        /**
         * @var $micrositeHelper \Unirgy\DropshipMicrosite\Helper\Data
         */
        $micrositeHelper = ObjectManager::getInstance()->create('Unirgy\DropshipMicrosite\Helper\Data');
        if (!$micrositeHelper->getCurrentVendor()) {
            return false;
        }
        $currentVendorId = (int)$micrositeHelper->getCurrentVendor()->getVendorId();
        foreach($vendors as $vendor) {
            if ($vendor === $currentVendorId) {
                return $vendor;
            }
        }

        return false;
    }

    private function getVendorIdsFromUdmultiData($allUdmultiData)
    {
        $result = [];
        if (is_array($allUdmultiData)) {
            foreach($allUdmultiData as $key => $udmultiDatum) {
                array_push($result, $key);
            }
        }

        return $result;
    }

    public function getSimpleProductUrlByVendor($productId)
    {
        $product = $this->_productFactory->create()->load($productId);
        $bestVendor = $product->getUdmultiBestVendor();
        $allUdmultiVendorData = $product->getAllMultiVendorData();
        $vendorMicroSiteId = $this->checkMicroSitePage($allUdmultiVendorData);
        if (!$vendorMicroSiteId) {
            $vendorMicroSiteId = $bestVendor;
        }

        $vendor = $this->_vendorFactory->create()->load($vendorMicroSiteId);

        return $this->_microSiteHelper->getVendorUrl($vendor, $product);
    }
}
