<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/10/18
 * Time: 8:42 AM
 */

namespace Magenest\SellYours\Helper;

use Unirgy\DropshipPo\Model\Source as ShipmentStatus;
use Magenest\Groupon\Model\Source\CampaignStatus;
use Magento\Framework\Exception\LocalizedException;

class Dashboard
{
    /**
     * @var \Unirgy\DropshipPo\Model\ResourceModel\Po\Item\CollectionFactory
     */
    protected $_poItemCollectionFactory;

    /**
     * @var \Unirgy\DropshipPo\Model\ResourceModel\Po\CollectionFactory
     */
    protected $_poCollectionFactory;

    /**
     * @var \Unirgy\Dropship\Model\Session
     */
    protected $_vendorSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var string
     */
    protected $todaySale;

    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_orderItemFactory \Magento\Sales\Model\Order\ItemFactory
     */
    protected $_orderItemFactory;

    /**
     * @var $_creditMemoCollection \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory
     */
    protected $_creditMemoCollection;

    /**
     * @var $_urmaFactory \Unirgy\Rma\Model\RmaFactory
     */
    protected $_urmaFactory;

    /**
     * @var $_saleShipmentCollectionFactory \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory
     */
    protected $_saleShipmentCollectionFactory;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var
     */
    protected $unitSoldToday;

    public function __construct(
        \Unirgy\DropshipPo\Model\ResourceModel\Po\Item\CollectionFactory $poItemCollectionFactory,
        \Unirgy\DropshipPo\Model\ResourceModel\Po\CollectionFactory $poCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Unirgy\Dropship\Model\Session $vendorSession,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $productFactory,
        \Magento\Catalog\Model\ProductFactory $factory,
        \Magento\Sales\Model\Order\ItemFactory $itemFactory,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $collection,
        \Unirgy\Rma\Model\RmaFactory $rmaFactory,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $collectionFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_saleShipmentCollectionFactory = $collectionFactory;
        $this->_urmaFactory = $rmaFactory;
        $this->_creditMemoCollection = $collection;
        $this->_orderItemFactory = $itemFactory;
        $this->_productFactory = $factory;
        $this->_unirgyProductFactory = $productFactory;
        $this->_storeManager = $storeManager;
        $this->_vendorSession = $vendorSession;
        $this->_poCollectionFactory = $poCollectionFactory;
        $this->_poItemCollectionFactory = $poItemCollectionFactory;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getVendorId()
    {
        if (!$this->_vendorSession->isLoggedIn()) {
            throw new LocalizedException(__("Vendor not logged in"));
        }
        return $this->_vendorSession->getVendorId();
    }

    public function getUnitsSaleToday()
    {
        if ($this->unitSoldToday === null) {
            $today = date('Y-m-d');
            $vendorId = $this->getVendorId();
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Unirgy\DropshipPo\Model\ResourceModel\Po\Item\Collection $itemPoCollection */
            $itemPoCollection = $this->_poItemCollectionFactory->create();
            $poTable = $itemPoCollection->getTable('udropship_po');
            $condition = "`$poTable`.`entity_id`=`main_table`.`parent_id`" .
                " AND `$poTable`.`created_at` LIKE '%$today%'" .
                " AND `$poTable`.`udropship_vendor`='$vendorId'" .
                " AND `$poTable`.`store_id`='$storeId'";
            $itemPoCollection->addExpressionFieldToSelect('sold', 'SUM({{qty}})', 'qty')
                ->join(
                    $poTable,
                    $condition,
                    []
                );
            $this->unitSoldToday = (int)$itemPoCollection->getFirstItem()->getSold();
        }
        return $this->unitSoldToday;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTodaySale()
    {
        if ($this->todaySale === null) {
            $today = date('Y-m-d');
            /** @var \Unirgy\DropshipPo\Model\ResourceModel\Po\Collection $poCollection */
            $poCollection = $this->_poCollectionFactory->create();
            $poCollection->addExpressionFieldToSelect('today_sale', 'SUM({{total_value}})', 'total_value')
                ->addFieldToFilter('store_id', $this->_storeManager->getStore()->getId())
                ->addFieldToFilter('udropship_vendor', $this->getVendorId())
                ->addFieldToFilter('created_at', ['like' => "%{$today}%"]);
            $todaySale = $poCollection->getFirstItem()->getTodaySale() ?: 0;
            $todaySale = (float)$todaySale;
            $this->todaySale = $todaySale;
        }
        return $this->todaySale;
    }

    /**
     * @throws LocalizedException
     */
    public function getSoldOutOption()
    {
        $result = 0;
        try {
            $collectionProduct = $this->_unirgyProductFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
            foreach ($collectionProduct as $item) {
                if (intval($item->getStockQty()) === 0) {
                    $result++;
                }
            }
            return $result;
        } catch (LocalizedException $e) {
            throw new LocalizedException(__("Vendor not logged in"));
        }
    }

    /**
     * Get active product of simple product vendor
     *
     * @throws LocalizedException
     */
    public function getActiveCampaignCollection()
    {
        $inventoryItems = $this->_unirgyProductFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId())->getColumnValues('product_id');

        $products = $this->_productFactory->create()->getCollection()->addAttributeToSelect('campaign_status')
            ->addFieldToFilter('entity_id', ['in' => $inventoryItems])
            ->addFieldToFilter('campaign_status', ['in' => \Magenest\Groupon\Model\Product\Attribute\CampaignStatus::$approvedState]);
        $result = [];

        foreach ($products->getItems() as $product) {
            $vendorStatus = $this->_unirgyProductFactory->create()->getCollection()
                ->addFieldToFilter('vendor_id', $this->getVendorId())->addFieldToFilter('product_id', $product->getId())->getFirstItem()->getStatus();
            if ($vendorStatus === '1') {
                array_push($result, $product);
            }
        }

        return $result;
    }

    /**
     * Get number of units fulfilled for each vendor
     * @param $ids mixed
     * @return int
     * @throws LocalizedException
     */
    public function getRedeemed($ids)
    {
        $result = 0;
        /** @var \Magento\Sales\Model\ResourceModel\Order\Shipment\Collection $shipments */
        $shipments = $this->_saleShipmentCollectionFactory->create()->addFieldToFilter('udropship_vendor', $this->getVendorId());
        $shipmentItemsTable = $shipments->getTable('sales_shipment_item');
        $shipments->getSelect()->joinLeft(
            $shipmentItemsTable,
            "main_table.entity_id = $shipmentItemsTable.parent_id",
            ["SUM($shipmentItemsTable.qty_shipped) as total_qty_shipped"]
        )->group('entity_id');
        foreach ($shipments as $shipment) {
            if (@$shipment->getTotalQtyShipped() && intval($shipment->getUdropshipStatus()) === ShipmentStatus::UDPO_STATUS_DELIVERED) {
                $result += $shipment->getTotalQtyShipped();
            }
        }

        return $result - $this->getRefunded($ids);
    }

    /**
     * Get number of units pending shipment for vendor simple product type
     * @param $ids
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRemaining($ids)
    {
        return $this->getSold($ids) - $this->getRedeemed($ids);
    }

    /**
     * Get number of units returns for vendor simple
     * @param $ids
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRefunded($ids)
    {
        $items = $this->getRefundedFromDay();
        $result = 0;
        foreach ($items as $item) {
            $result += intval($item->getQty());
        }

        return $result;
    }

    /**
     * Get number of units sold for simple vendor
     * @param $ids
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getSold($ids)
    {
        $items = $this->_orderItemFactory->create()->getCollection()->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->addFieldToFilter('product_id', ['in' => $ids]);
        $result = 0;
        foreach ($items as $item) {
            $result += intval($item->getQtyInvoiced());
        }

        return $result - $this->getRefunded($ids);
    }

    /**
     * Return list id of active product
     * @throws LocalizedException
     */
    public function getActiveCampaignCollectionIds()
    {
        $activeProducts = $this->getActiveCampaignCollection();
        $result = [];
        foreach ($activeProducts as $product) {
            array_push($result, $product->getId());
        }

        return $result;
    }

    /**
     * @param $daynum
     * @return array
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getOption($daynum)
    {
        $arr = [];
        $daystart = new \DateTime();
        if (is_integer($daynum)) {
            $dayCount = (int)$daynum;
        } else {
            $dayCount = floor((time() - strtotime($this->getDay($daynum))) / 86400);
            $dayCount = (int)$dayCount;
        }
        $date = $daystart->sub(new \DateInterval('P' . $dayCount . 'D'));
        $startDateText = $date->format('Y-m-d');
        $dateText = $date->format('Y-m-d');
        $soldInDays = $this->getSoldInDay($this->getSoldFromDay($dayCount));
        $soldInDaysValue = $this->getSoldInDayValue($this->getSoldFromDay($dayCount));
        $refundInDays = $this->getRefundedInDay($this->getRefundedFromDay($dayCount));
        $refundInDaysValue = $this->getRefundedInDayValue($this->getRefundedFromDay($dayCount));
        for ($i = 0; $i < $dayCount; $i++) {
            $date->add(new \DateInterval('P1D'));
            $dateText = $date->format('Y-m-d');
            $sales = isset($soldInDaysValue[$dateText]) ? $soldInDaysValue[$dateText] : 0;
            $qtySale = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $refunds = isset($refundInDaysValue[$dateText]) ? $refundInDaysValue[$dateText] : 0;
            $qtyRefund = isset($refundInDays[$dateText]) ? $refundInDays[$dateText] : 0;
            $arr[] = [$dateText, $sales, $qtySale, $refunds, $qtyRefund, 'color: #017cb9; stroke-color: #017cb9'];
        }
        return [$startDateText, $dateText, $arr];
    }

    /**
     * @param $daynum
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRefundedValueFromDay($daynum)
    {
        $ticketsRefunded = $this->getRefundedFromDay($daynum);
        $total = 0;
        foreach ($ticketsRefunded as $ticket) {
            $total += $ticket->getPrice() * $ticket->getQty();
        }

        return $total;
    }

    /**
     * @param $key
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getFulfilledFromDay($key)
    {
        $day = $this->getDay($key);
        $result = 0;
        $shipments = $this->_saleShipmentCollectionFactory->create()->addFieldToFilter('udropship_vendor', $this->getVendorId());
        $shipmentItemsTable = $shipments->getTable('sales_shipment_item');
        $shipments->addFieldToFilter('created_at', array('gt' => $day));
        $shipments->getSelect()->joinLeft(
            $shipmentItemsTable,
            "main_table.entity_id = $shipmentItemsTable.parent_id",
            ["SUM($shipmentItemsTable.qty_shipped) AS total_qty_shipped"]
        )->group(['main_table.entity_id']);
        foreach ($shipments as $shipment) {
            if (@$shipment->getTotalQtyShipped() && intval($shipment->getUdropshipStatus()) === ShipmentStatus::UDPO_STATUS_DELIVERED) {
                $result += $shipment->getTotalQtyShipped();
            }
        }

        return $result;
    }

    /**
     * @param int $daynum
     * @return mixed
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRedeemedFromDay($daynum)
    {
        $day = $this->getDay($daynum);
        $collection = $this->_orderItemFactory->create()->getCollection()
            ->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->addFieldToFilter('created_at', array('gt' => $day));
        $fulfilled = 0;
        foreach ($collection as $item) {
            if ($item->getQtyRefunded()) {
                $qtyCount = $item->getQtyOrdered() - $item->getQtyRefunded();
                if ($qtyCount < $item->getQtyShipped()) {
                    $fulfilled += $qtyCount;
                } else {
                    $fulfilled += $item->getQtyShipped();
                }
            } else {
                $fulfilled += $item->getQtyShipped();
            }
        }
        return $fulfilled;
    }

    /**
     * @param $key
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getSaleValue($key)
    {
        $tickets = $this->getSoldFromDay($key);
        $total = 0;
        foreach ($tickets as $ticket) {
            $total += $ticket->getBaseRowTotal();
        }
        return $total;
    }

    public function getRefundedInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $ticket) {
            $key = explode(' ', $ticket->getCreatedAt())[0];
            $refund = $ticket->getPrice() * $ticket->getQty();
            if (!isset($arr[$key])) {
                $arr[$key] = $refund;
            } else {
                $arr[$key] += $refund;
            }
        }
        return $arr;
    }

    public function getRefundedInDay($collection)
    {
        $arr = [];
        foreach ($collection as $item) {
            $value = $item->getCreatedAt();
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = floatval($item->getQty());
            else
                $arr[$key] += floatval($item->getQty());
        }
        return $arr;
    }

    /**
     * @param $daynum
     * @return mixed
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRefundedFromDay($daynum = null)
    {
        $rmaItemTable = $this->_creditMemoCollection->create()->getTable('urma_rma_item');
        $collection = $this->_urmaFactory->create()->getCollection()->addFieldToFilter('udropship_vendor', $this->getVendorId())
        ->addFieldToFilter('rma_status', 'approved');
        $collection->getSelect()->joinLeft(
            $rmaItemTable,
            "main_table.entity_id = $rmaItemTable.parent_id",
            ['qty' => 'SUM(qty)', 'price']
        )->group(['main_table.entity_id']);
        if ($daynum) {
            $day = $this->getDay($daynum);
            $collection->addFieldToFilter('created_at', array('gt' => $day));
        }

        return $collection;
    }

    /**
     * @param $daynum
     * @return int
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getRefundedFromDayQty($daynum)
    {
        $collection = $this->getRefundedFromDay($daynum);
        $refunded = 0;

        foreach ($collection->getItems() as $item) {
            $refunded += $item->getQty();
        }
        return $refunded;
    }

    public function getSoldInDay($collection)
    {
        $arr = [];
        foreach ($collection as $item) {
            $value = $item->getCreatedAt();
            $key = explode(' ', $value)[0];
            if (!isset($arr[$key]))
                $arr[$key] = floatval($item->getQtyOrdered());
            else
                $arr[$key] += floatval($item->getQtyOrdered());
        }
        return $arr;
    }


    public function getSoldInDayValue($collection)
    {
        $arr = [];
        foreach ($collection as $ticket) {
            $key = explode(' ', $ticket->getCreatedAt())[0];
            $sold = $ticket->getBaseRowTotal();
            if (!isset($arr[$key]))
                $arr[$key] = $sold;
            else
                $arr[$key] += $sold;
        }
        return $arr;
    }

    /**
     * @param $key
     * @return mixed
     * @throws LocalizedException
     * @throws \Exception
     */
    public function getSoldFromDay($key)
    {
        $day = $this->getDay($key);
        $tickets = $this->_orderItemFactory->create()->getCollection()
            ->addFieldToFilter("udropship_vendor", $this->getVendorId())
            ->addFieldToFilter('created_at', array('gt' => $day));
        return $tickets;
    }

    public function getUnitsSoldFromDay($key)
    {
        $orderItems = $this->getSoldFromDay($key);
        $result = 0;

        foreach ($orderItems as $orderItem) {
            $result += $orderItem->getQtyOrdered();
        }

        return $result;
    }

    /**
     * @param $daynum
     * @return string
     * @throws \Exception
     */
    public function getDay($daynum)
    {
        $date = new \DateTime();
        if ($daynum === "all_time") {
            $startTime = $this->_vendorSession->getVendor()->getCreatedAt();
            if ($startTime === null) {
                $startTime = '2018-01-01 00:00:00';
            }
            $startTime = \DateTime::createFromFormat("Y-m-d H:i:s", $startTime);

            return $startTime->format('Y-m-d');
        } elseif ($daynum === "this_month") {
            $startTime = \DateTime::createFromFormat("Y-m-d", date('Y-m-01'));

            return $startTime->format('Y-m-d');
        } else {
            $date->sub(new \DateInterval('P' . $daynum . 'D'));
        }

        return $date->format('Y-m-d');
    }

    public function __call($name, $arguments)
    {
        return 'test111';
    }
}
