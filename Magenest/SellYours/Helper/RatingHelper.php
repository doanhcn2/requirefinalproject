<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\SellYours\Helper;

use Magento\Framework\App\ObjectManager;
use Unirgy\DropshipVendorRatings\Helper\Data;

class RatingHelper
{
    protected $_dealFactory;

    /**
     * @var $_eventFactory \Magenest\Ticket\Model\EventFactory
     */
    protected $_eventFactory;

    protected $_reviewFactory;

    protected $_storeManager;

    protected $_voteCollectionFactory;

    protected $_ratingInfoFactory;

    protected $_grouponFactory;

    protected $_orderItemRepository;

    /**
     * @var $_orderFactory \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Unirgy\DropshipVendorRatings\Helper\Data
     */
    protected $_uvRatingHelper;

    /**
     * @var \Unirgy\Dropship\Helper\Data
     */
    protected $_udHlp;

    /**
     * @var array
     */
    protected $aggregateRatings;

    /**
     * @var array
     */
    protected $vendorAvgRates = [];

    /**
     * @var array
     */
    protected $reviewsIds = [];

    /**
     * @var \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection[]
     */
    protected $vendorReviewsCollections = [];

    public function __construct(
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Review\Model\ResourceModel\Rating\Option\Vote\CollectionFactory $voteFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\FixUnirgy\Model\RatingInfoFactory $ratingInfoFactory,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Magenest\Ticket\Model\EventFactory $eventFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Unirgy\DropshipVendorRatings\Helper\Data $uvRatingHelper,
        \Unirgy\Dropship\Helper\Data $udHlp,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->_udHlp                 = $udHlp;
        $this->_uvRatingHelper        = $uvRatingHelper;
        $this->_productFactory        = $productFactory;
        $this->_eventFactory          = $eventFactory;
        $this->_dealFactory           = $dealFactory;
        $this->_reviewFactory         = $reviewFactory;
        $this->_storeManager          = $storeManager;
        $this->_voteCollectionFactory = $voteFactory;
        $this->_ratingInfoFactory     = $ratingInfoFactory;
        $this->_grouponFactory        = $grouponFactory;
        $this->_orderItemRepository   = $orderItemRepository;
        $this->_orderFactory          = $orderFactory;
    }

    /**
     * @param $id
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct($id)
    {
        return $this->_productFactory->create()->load($id);
    }

    /**
     * @param $productId
     * @return array
     */
    public function getCurrentProductOrder($productId)
    {
        $currentProductId = $this->getProduct($productId)->getEntityId();
        $result           = [];

        $reviewCollection = $this->_reviewFactory->create()->getCollection();
        $this->shipments($reviewCollection, null);
        $this->orders($reviewCollection);
        $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', Data::ENTITY_TYPE_ID)
            ->setDateOrder();
        $listOrderIds = $reviewCollection->getColumnValues('shipment_order_id');

        foreach ($listOrderIds as $orderId) {
            if (@$orderId && $orderId == '') continue;
            $order = $this->_orderFactory->create()->load($orderId);
            if ($order->getEntityId()) {
                $orderItems = $order->getItems();
                foreach ($orderItems as $item) {
                    if ($currentProductId === $item->getProductId()) {
                        array_push($result, $orderId);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $productRateId
     * @param bool $isApi
     * @param null $vendorId
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getReviewCollection($productRateId, $isApi = false, $vendorId = null)
    {
        /** @var  $reviewCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $reviewCollection = $this->_reviewFactory->create()->getCollection();
        $product          = $this->getProduct($productRateId);
        $childIds         = $product->getTypeInstance()->getChildrenIds($product->getId());
        $productIds       = [];
        if (is_array($childIds) && !empty($childIds)) {
            foreach ($childIds as $productId) {
                if (@$productId && $productId !== '') {
                    $productIds[] = $productId;
                }
            }
        }
        $productIds[] = $product->getId();

        if (empty($vendorId)) {
            $objectManager   = \Magento\Framework\App\ObjectManager::getInstance();
            $micrositeHelper = $objectManager->create('Unirgy\DropshipMicrosite\Helper\Data');
            if ($micrositeHelper->getCurrentVendor())
                try {
                    $vendorId = @$micrositeHelper->getCurrentVendor()->getVendorId();
                } catch (\Exception$exception) {
                    $vendorId = $product->getData('udmulti_best_vendor');
                }
            else
                $vendorId = $product->getData('udmulti_best_vendor');
        }

        $this->shipments($reviewCollection, $vendorId);
        if ($product->getTypeId() !== 'simple') {
            $this->orderItems($reviewCollection);
        }
        $this->orders($reviewCollection);

        if (!$isApi) {
            $reviewCollection->addStoreFilter($this->_storeManager->getStore()->getId());
        }

        if ($product->getTypeId() === 'simple') {
            $reviewCollection
                ->addStatusFilter(__('Approved'))
                ->addFieldToFilter('main_table.entity_id', Data::ENTITY_TYPE_ID)
                ->addFieldToFilter('sm.order_id', ['in' => $this->getCurrentProductOrder($productRateId)])
                ->setDateOrder();
        } else {
            $reviewCollection
                ->addStatusFilter(__('Approved'))
                ->addFieldToFilter('main_table.entity_id', ['in' => [Data::ENTITY_TYPE_ID, \Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID]])
                ->addFieldToFilter('soi.product_id', ['in' => $productIds])
                ->setDateOrder();
        }
        $reviewCollection->addRateVotes();

        return $reviewCollection;
    }

    /**
     * @param $productId
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getProductReviewCollection($productId)
    {
        /** @var  $reviewCollection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $reviewCollection = $this->_reviewFactory->create()->getCollection();

        $reviewCollection
            ->addStatusFilter(__('Approved'))
            ->addFieldToFilter('main_table.entity_id', 1)
            ->addFieldToFilter('main_table.entity_pk_value', $productId)
            ->setDateOrder();
        $reviewCollection->addRateVotes();

        return $reviewCollection;
    }

    /**
     * @param $reviewIds
     * @return int
     */
    public function getAvgRate($reviewIds)
    {
        $collection = $this->getAvgRateCollection($reviewIds);

        return $collection->getFirstItem()->getAvgValue() ? $collection->getFirstItem()->getAvgValue() : 0;
    }

    /**
     * @param $vendor
     * @param bool $groupByRating
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     */
    public function getAvgRateCollectionByVendor($vendor, $groupByRating = false)
    {
        $reviewIds = $this->getReviewIdsArr($vendor);

        return $this->getAvgRateCollection($reviewIds, $groupByRating);
    }

    /**
     * @param $vendor
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     */
    public function getPositiveRateCollectionByVendor($vendor)
    {
        $reviewIds = $this->getReviewIdsArr($vendor);

        return $this->getRateCollection($reviewIds)->addFieldToFilter('value', ['gt' => 2]);
    }

    /**
     * @param $vendor
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     */
    public function getRateCollectionByVendor($vendor)
    {
        $reviewIds = $this->getReviewIdsArr($vendor);

        return $this->getRateCollection($reviewIds);
    }

    /**
     * @param $vendor
     * @return array
     */
    private function getReviewIdsArr($vendor)
    {
        if (!is_array($this->reviewsIds)) {
            $this->reviewsIds = [];
        }
        $vendor = $this->_udHlp->getVendor($vendor);
        if (!isset($this->reviewsIds[$vendor->getId()])) {
            $this->reviewsIds[$vendor->getId()] = $this->getVendorReviewsCollection($vendor)->getColumnValues('review_id');
        }

        return $this->reviewsIds[$vendor->getId()];
    }

    /**
     * @param $reviewIds
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     */
    private function getRateCollection($reviewIds)
    {
        /** @var  $collection \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection */
        $collection = $this->_voteCollectionFactory->create();
        $collection->getSelect()
            ->where('`main_table`.`review_id` IN (?)', $reviewIds)
            ->where('`main_table`.`rating_id` IN (?)', $this->getAggregateRating());

        return $collection;
    }

    /**
     * @param $reviewIds
     * @param bool $groupByRating
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection
     */
    private function getAvgRateCollection($reviewIds, $groupByRating = false)
    {
        $collection = $this->getRateCollection($reviewIds);
        $collection->getSelect()->reset('columns');
        $collection->addExpressionFieldToSelect('avg_value', 'AVG({{value}})', ['value' => '`main_table`.`value`']);

        if ($groupByRating) {
            $this->groupByRating($collection);
        }

        return $collection;
    }

    /**
     * @param \Unirgy\DropshipVendorRatings\Model\ResourceModel\RatingOptionVoteCollection $collection
     */
    private function groupByRating($collection)
    {
        $ratingTable = $collection->getTable('rating');
        $collection->join(
            $ratingTable,
            "`main_table`.`rating_id` = `$ratingTable`.`rating_id`",
            ['rating_code']
        )->getSelect()->group('main_table.rating_id');
    }

    /**
     * @param $vendor
     * @param int $maxPoint
     * @return float|int
     */
    public function getVendorAvgRate($vendor, $maxPoint = 5)
    {
        if (!is_array($this->vendorAvgRates)) {
            $this->vendorAvgRates = [];
        }
        $vendor = $this->_udHlp->getVendor($vendor);
        if (!isset($this->vendorAvgRates[$vendor->getId()])) {
            $this->vendorAvgRates[$vendor->getId()] = $this->getAvgRate($this->getReviewIdsArr($vendor));
        }

        return $this->vendorAvgRates[$vendor->getId()] * ($maxPoint / 5);
    }

    /**
     * @param $vendor
     * @return \Unirgy\DropshipVendorRatings\Model\ResourceModel\Review\Collection
     */
    public function getVendorReviewsCollection($vendor)
    {
        if (!is_array($this->vendorReviewsCollections)) {
            $this->vendorReviewsCollections = [];
        }
        $vendor = $this->_udHlp->getVendor($vendor);
        if (!isset($this->vendorReviewsCollections[$vendor->getId()])) {
            $this->vendorReviewsCollections[$vendor->getId()] = $this->_uvRatingHelper->getVendorReviewsCollection($vendor)->addRateVotes();
        }

        return $this->vendorReviewsCollections[$vendor->getId()];

    }

    /**
     * @param $collection
     * @param $vendorId
     * @return mixed
     */
    public function shipments($collection, $vendorId)
    {
        if (isset($vendorId) && !empty($vendorId)) {
            $collection->getSelect()
                ->joinLeft(
                    ['sm' => 'sales_shipment'],
                    "main_table.rel_entity_pk_value = sm.entity_id and main_table.entity_id = " . Data::ENTITY_TYPE_ID . " and sm.udropship_vendor = " . $vendorId,
                    ['review_entity_id' => 'main_table.entity_id', 'shipment_order_id' => 'sm.order_id']
                );
        } else {
            $collection->getSelect()
                ->joinLeft(
                    ['sm' => 'sales_shipment'],
                    "main_table.rel_entity_pk_value = sm.entity_id and main_table.entity_id = " . Data::ENTITY_TYPE_ID,
                    ['review_entity_id' => 'main_table.entity_id', 'shipment_order_id' => 'sm.order_id']
                );
        }

        return $collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    public function orderItems($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['soi' => 'sales_order_item'],
                "main_table.rel_entity_pk_value = soi.item_id and main_table.entity_id = " . \Magenest\FixUnirgy\Helper\Review\Data::ENTITY_TYPE_ID,
                ['product_id']
            );

        return $collection;
    }

    /**
     * @param $collection
     * @return mixed
     */
    public function orders($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                ['so' => 'sales_order'],
                "sm.order_id = so.entity_id",
                ['review_increment_id' => 'so.increment_id']
            );

        return $collection;
    }

    /**
     * @return array
     */
    private function getAggregateRating()
    {
        if (!$this->aggregateRatings) {
            $this->aggregateRatings = ObjectManager::getInstance()->create('Magento\Review\Model\Rating')
                ->getCollection()->addFieldToFilter('is_aggregate', 1)->getAllIds();
        }

        return $this->aggregateRatings;
    }
}
