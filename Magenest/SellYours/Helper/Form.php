<?php

namespace Magenest\SellYours\Helper;

use Magento\Framework\App\ObjectManager;
use Unirgy\DropshipVendorProduct\Helper\Form as UnirgyForm;

class Form extends UnirgyForm
{
    public function getUdmultiField($field, $mvData)
    {
        $fieldDef = [];
        switch ($field) {
            case 'status':
                $fieldDef = [
                    'id' => 'udmulti_status',
                    'type'     => 'select',
                    'name'     => 'udmulti[status]',
                    'label'    => __('Status'),
                    'options'   => $this->_hlp->getObj('\Unirgy\DropshipMulti\Model\Source')->setPath('vendor_product_status')->toOptionHash(),
                    'value'     => @$mvData['status']
                ];
                break;
            case 'state':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $fieldDef = [
                    'id' => 'udmulti_state',
                    'type'     => 'select',
                    'name'     => 'udmulti[state]',
                    'label'    => __('State (Condition)'),
                    'options'  => $this->_hlp->getObj('Unirgy\DropshipMultiPrice\Model\Source')->setPath('vendor_product_state')->toOptionHash(),
                    'value'    => @$mvData['state']
                ];
                }
                break;
            case 'stock_qty':
                $v = @$mvData['stock_qty'];
                $fieldDef = [
                    'id' => 'udmulti_stock_qty',
                    'type'     => 'text',
                    'name'     => 'udmulti[stock_qty]',
                    'label'    => __('Stock Qty'),
                    'value'    => null !== $v ? $v*1 : ''
                ];
                break;
            case 'state_descr':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $fieldDef = [
                    'id' => 'udmulti_state_descr',
                    'type'     => 'text',
                    'name'     => 'udmulti[state_descr]',
                    'label'    => __('State description'),
                    'value'    => @$mvData['state_descr']
                ];
                }
                break;
            case 'vendor_title':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $fieldDef = [
                    'id' => 'udmulti_vendor_title',
                    'type'     => 'text',
                    'name'     => 'udmulti[vendor_title]',
                    'label'    => __('Vendor Title'),
                    'value'    => @$mvData['vendor_title']
                ];
                }
                break;
            case 'vendor_cost':
                $v = @$mvData['vendor_cost'];
                $fieldDef = [
                    'id' => 'udmulti_vendor_cost',
                    'type'     => 'text',
                    'name'     => 'udmulti[vendor_cost]',
                    'label'    => __('Vendor Cost'),
                    'value'    => null !== $v ? $v*1 : ''
                ];
                break;
            case 'vendor_price':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $v = @$mvData['vendor_price'];
                $fieldDef = [
                    'id' => 'udmulti_vendor_price',
                    'type'     => 'text',
                    'name'     => 'udmulti[vendor_price]',
                    'label'    => __('Vendor Price'),
                    'value'    => null !== $v ? $v*1 : ''
                ];
                }
                break;
            case 'group_price':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                    $v = @$mvData['group_price'];
                    $fieldDef = [
                        'id' => 'udmulti_group_price',
                        'type'     => 'udmulti_group_price',
                        'name'     => 'udmulti[group_price]',
                        'input_renderer' => '\Unirgy\DropshipMulti\Block\Vendor\ProductAttribute\Form\GroupPrice',
                        'label'    => __('Group Price'),
                        'value'    => !empty($v) && is_array($v) ? $v : []
                    ];
                }
                break;
            case 'tier_price':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                    $v = @$mvData['tier_price'];
                    $fieldDef = [
                        'id' => 'udmulti_tier_price',
                        'type'     => 'udmulti_tier_price',
                        'name'     => 'udmulti[tier_price]',
                        'input_renderer' => '\Unirgy\DropshipMulti\Block\Vendor\ProductAttribute\Form\TierPrice',
                        'label'    => __('Tier Price'),
                        'value'    => !empty($v) && is_array($v) ? $v : []
                    ];
                }
                break;
            case 'special_price':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $v = @$mvData['special_price'];
                $fieldDef = [
                    'id' => 'udmulti_special_price',
                    'type'     => 'text',
                    'name'     => 'udmulti[special_price]',
                    'label'    => __('Vendor Special Price'),
                    'value'    => null !== $v ? $v*1 : ''
                ];
                }
                break;
            case 'special_from_date':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $fieldDef = [
                    'id' => 'udmulti_special_from_date',
                    'type'     => 'date',
                    'date_format'   => $this->_hlp->getDefaultDateFormat(),
                    'name'     => 'udmulti[special_from_date]',
                    'label'    => __('Vendor Special From Date'),
                    'value'    => @$mvData['special_from_date'],
                    'class'    => 'validate-date validate-date-range date-range-udmulti_special_date-from'
                ];
                }
                break;
            case 'special_to_date':
                if ($this->_hlp->isUdmultiPriceAvailable()) {
                $fieldDef = [
                    'id' => 'udmulti_special_to_date',
                    'type'     => 'date',
                    'date_format'   => $this->_hlp->getDefaultDateFormat(),
                    'name'     => 'udmulti[special_to_date]',
                    'label'    => __('Vendor Special To Date'),
                    'value'    => @$mvData['special_to_date'],
                    'class'    => 'validate-date validate-date-range date-range-udmulti_special_date-to'
                ];
                }
                break;
            case 'vendor_sku':
                $fieldDef = [
                    'id' => 'udmulti_vendor_sku',
                    'type'     => 'text',
                    'name'     => 'udmulti[vendor_sku]',
                    'label'    => __('Vendor Sku'),
                    'value'    => @$mvData['vendor_sku']
                ];
                break;
            case 'freeshipping':
                $checked = @$mvData['freeshipping']*1 == 1 ? true : false;
                $fieldDef = [
                    'id' => 'udmulti_freeshipping',
                    'type'     => 'checkbox',
                    'name'     => 'udmulti[freeshipping]',
                    'label'    => __('Free Shipping'),
                    'options'  => $this->_hlp->src()->setPath('yesno')->toOptionHash(),
                    'value'    => @$mvData['freeshipping']*1
                ];
                if ($checked) {
                    $fieldDef['checked'] = 'checked';
                }
                break;
            case 'shipping_price':
                $fieldDef = [
                    'id' => 'udmulti_shipping_price',
                    'type'     => 'text',
                    'name'     => 'udmulti[shipping_price]',
                    'label'    => __('Shipping Price'),
                    'value'    => @$mvData['shipping_price']
                ];
                break;
            case 'backorders':
                $fieldDef = [
                    'id' => 'udmulti_backorders',
                    'type'     => 'select',
                    'name'     => 'udmulti[backorders]',
                    'label'    => __('Vendor Backorders'),
                    'options'  => $this->_hlp->getObj('\Unirgy\DropshipMulti\Model\Source')->setPath('backorders')->toOptionHash(),
                    'value'    => @$mvData['backorders']*1
                ];
                break;
            case 'handling_time':
                $fieldDef = [
                    'id' => 'handling_time',
                    'type'     => 'select',
                    'name'     => 'udmulti[handling_time]',
                    'label'    => __('Handling Time'),
                    'options'   => $this->_hlp->getObj('\Magenest\SellYours\Model\Source')->toOptionHash(),
                    'value'     => @$mvData['handling_time']
                ];
                break;
            case 'offer_note':
                $fieldDef = [
                    'id' => 'offer_note',
                    'type'     => 'text',
                    'name'     => 'udmulti[offer_note]',
                    'label'    => __('Offer Note (optional)'),
                    'value'    => @$mvData['offer_note']
                ];
                break;
        }
        return $fieldDef;
    }

    public function getAttributeField($attribute)
    {
        $fieldDef = [];
        if ($attribute && (!$attribute->hasIsVisible() || $attribute->getIsVisible())
            && ($inputType = $attribute->getFrontend()->getInputType())
        ) {
            $fieldType      = $inputType;
            if ($attribute->getAttributeCode()=='tier_price') $fieldType='tier_price';
            if ($attribute->getAttributeCode()=='group_price') $fieldType='group_price';
            $rendererClass  = $attribute->getFrontend()->getInputRendererClass();
            if (!empty($rendererClass)) {
                $fieldType  = $inputType . '_' . $attribute->getAttributeCode();
            }
            $fieldDef = [
                'id'       => $attribute->getAttributeCode(),
                'type'     => $fieldType,
                'name'     => $attribute->getAttributeCode(),
                'label'    => $attribute->getFrontend()->getLabel(),
                'class'    => $attribute->getFrontend()->getClass(),
                'note'     => $attribute->getNote(),
                'input_renderer' => $rendererClass,
                'entity_attribute' => $attribute
            ];
            if ($inputType == 'select') {
                $fieldDef['values'] = $attribute->getSource()->getAllOptions(true, false);
            } else if ($inputType == 'multiselect') {
                $fieldDef['values'] = $attribute->getSource()->getAllOptions(false, false);
                $fieldDef['can_be_empty'] = true;
            } else if ($inputType == 'date') {
                $fieldDef['date_format'] = $this->_hlp->getDefaultDateFormat();
                if ($attribute->getAttributeCode() == 'special_from_date') {
                    $fieldDef['class']  = 'validate-date validate-date-range date-range-special_date-from';
                } elseif ($attribute->getAttributeCode() == 'special_to_date') {
                    $fieldDef['class']  = 'validate-date validate-date-range date-range-special_date-to';
                }
            } else if ($inputType == 'multiline') {
                $fieldDef['line_count'] = $attribute->getMultilineCount();
            }
        }
        $isEditOptionCondition = false;
        $isCreateOptions = ObjectManager::getInstance()->get('Magento\Framework\Registry')->registry('is_create_groupon_options');
        $isEditOptions = ObjectManager::getInstance()->get('Magento\Framework\Registry')->registry('is_edit_groupon_options');
        if (isset($isEditOptions)) {
            if ($isEditOptions  && $isCreateOptions === 'udprod_vendor_productEdit') {
                $isEditOptionCondition = true;
            }
        }
        if (($isCreateOptions === 'udprod_vendor_cfgQuickCreateAttribute' || $isEditOptionCondition) && $fieldDef['name'] === 'name') {
            $fieldDef['label'] = __("Enter a Title to describe the Service being offered");
        }
        if (($isCreateOptions === 'udprod_vendor_cfgQuickCreateAttribute' || $isEditOptionCondition) && $fieldDef['name'] === 'price') {
            $fieldDef['label'] = __("What is the original (non-discounted) value of this service?");
        }
        if (($isCreateOptions === 'udprod_vendor_cfgQuickCreateAttribute' || $isEditOptionCondition) && $fieldDef['name'] === 'special_price') {
            $fieldDef['label'] = __("Customer Pays");
        }
        return $fieldDef;
    }
}
