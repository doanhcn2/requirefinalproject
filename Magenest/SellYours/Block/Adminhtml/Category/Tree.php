<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

/**
 * Categories tree block
 */

namespace Magenest\SellYours\Block\Adminhtml\Category;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Tree\Node;
use Magento\Store\Model\Store;

/**
 * Class Tree
 * @api
 * @package Magento\Catalog\Block\Adminhtml\Category
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Tree
{
    const DEFAULT_CATEGORY = 2;

    /**
     * Retrieve list of categories with name containing $namePart and their parents
     *
     * @param string $namePart
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSuggestedCategoriesJson($namePart, $type = null)
    {
        $storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());

        /* @var $collection Collection */
        $collection = $this->_categoryFactory->create()->getCollection();

        $matchingNamesCollection = clone $collection;
        $rootCategoryId          = $this->getRootCategory($type);
        $escapedNamePart         = $this->_resourceHelper->addLikeEscape(
            $namePart,
            ['position' => 'any']
        );
        $matchingNamesCollection->addAttributeToFilter(
            'name',
            ['like' => $escapedNamePart]
        )->addAttributeToFilter(
            'entity_id',
            ['neq' => Category::TREE_ROOT_ID]
        )->addAttributeToFilter(
            'path',
            ['like' => $this->getRootCategoryPath($rootCategoryId)]
        )->addAttributeToSelect(
            'path'
        )->setStoreId(
            $storeId
        );

        $shownCategoriesIds = [];
        foreach ($matchingNamesCollection as $category) {
            foreach (explode('/', $category->getPath()) as $parentId) {
                $shownCategoriesIds[$parentId] = 1;
            }
        }

        $collection->addAttributeToFilter(
            'entity_id',
            ['in' => array_keys($shownCategoriesIds)]
        )->addAttributeToSelect(
            ['name', 'is_active', 'parent_id']
        )->setStoreId(
            $storeId
        );

        $categoryById = [
            Category::TREE_ROOT_ID => [
                'id'       => Category::TREE_ROOT_ID,
                'children' => [],
            ],
        ];

        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($collection as $category) {
            foreach ([$category->getId(), $category->getParentId()] as $categoryId) {
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = ['id' => $categoryId, 'children' => []];
                }
            }
            $categoryById[$category->getId()]['is_active'] = $category->getIsActive();
            $categoryById[$category->getId()]['label']     = $category->getName();
            $categoryById[$category->getParentId()]['children'][] = &$categoryById[$category->getId()];
        }

        if (isset($categoryById[2]) && $type !== 'admin') {
            return $this->_jsonEncoder->encode($categoryById[2]['children']);
        } else {
            return $this->_jsonEncoder->encode($categoryById[Category::TREE_ROOT_ID]['children']);
        }
    }

    private function getRootCategory($vendorPromote = null)
    {
        if ($vendorPromote === 'admin') {
            return self::DEFAULT_CATEGORY;
        }
        if (empty($vendorPromote)) {
            $vendorId      = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
            $vendorPromote = ObjectManager::getInstance()->create('Unirgy\Dropship\Helper\Data')->getVendor($vendorId)->getPromoteProduct();
        }
        $businessCategory = ObjectManager::getInstance()->create('Magenest\Profile\Model\BusinessCategoryFactory');

        $root = $businessCategory->create()->getCollection()->addFieldToFilter('product_promote', $vendorPromote)->getFirstItem()->getRootCategory();
        if (isset($root)) {
            return $root;
        }

        return self::DEFAULT_CATEGORY;
    }

    private function getRootCategoryPath($rootCategoryId)
    {
        if ($rootCategoryId) {
            if ((int)$rootCategoryId === self::DEFAULT_CATEGORY) {
                return '1/2%';
            } elseif ($rootCategoryId === '5' || $rootCategoryId === 5) {
                return '1/2/' . intval($rootCategoryId) . '/%';
            } else {
                return '1/2/' . intval($rootCategoryId) . '%';
            }
        }

        return '1/2%';
    }
}
