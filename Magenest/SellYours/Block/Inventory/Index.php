<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 05/05/2018
 * Time: 15:46
 */

namespace Magenest\SellYours\Block\Inventory;

use Magenest\SellYours\Model\Status\Source as Status;
use Magento\Framework\View\Element\Template;

class Index extends Template
{

    protected $_vendorSession;

    protected $_currentcy;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var $_vendorProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_vendorProductFactory;

    /**
     * @var $_hlp \Unirgy\Dropship\Helper\Data
     */
    protected $_hlp;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Unirgy\Dropship\Model\Session $dropshipSession,
        \Unirgy\Dropship\Helper\Data $hlp,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Unirgy\Dropship\Model\Vendor\ProductFactory $vendorProductFactory,
        array $data = []
    ) {
        $this->_hlp = $hlp;
        $this->_productFactory = $productFactory;
        $this->_vendorProductFactory = $vendorProductFactory;
        $this->_currentcy = $currency;
        $this->_vendorSession = $dropshipSession;
        parent::__construct($context, $data);
    }

    public function getCurrencySymbol()
    {
        return $this->_currentcy->getCurrencySymbol();
    }

    public function getProductCounterStatus()
    {
        $result = [];
        $result[Status::STATUS_REJECTED] = 0;
        $result[Status::STATUS_DRAFT] = 0;
        $result[Status::STATUS_PENDING_REVIEW] = 0;
        $result[Status::STATUS_SCHEDULED] = 0;
        $result[Status::STATUS_LIVE] = 0;
        $result[Status::STATUS_ACTIVE] = 0;
        $result[Status::STATUS_EXPIRED] = 0;
        $result[Status::STATUS_SOLD_OUT] = 0;
        $result[Status::STATUS_PAUSED] = 0;
        $result[Status::STATUS_APPROVED] = 0;
        $result['all'] = 0;

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productFactory->create()->getCollection()
            ->addFieldToFilter('udropship_vendor', $this->getVendorId())
            ->addFieldToFilter('type_id', 'simple')
            ->setFlag('has_stock_status_filter', 1);
        $collection->getSelect()->join(
            [
                'vendor_prod_table' => $collection->getTable('udropship_vendor_product')
            ],
            "`e`.`entity_id` = `vendor_prod_table`.`product_id` AND `vendor_prod_table`.`vendor_id` = '{$this->_vendorSession->getVendorId()}'",
            ['status']
        );
        $productIds = $collection->getColumnValues('entity_id');
        foreach($collection as $item) {
            $status = (int)$item->getStatus();
            $result[$status]++;
            $result['all']++;
        }

        return $this->countSellYourItems($result, $productIds);
    }

    public function getVendorId()
    {
        return $this->_vendorSession->getVendorId();
    }

    /**
     * Count SellYours product
     *
     * @param $result
     * @param $ids
     *
     * @return mixed
     */
    public function countSellYourItems($result, $ids)
    {
        $vendorProductIds = $this->_vendorProductFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())->addFieldToFilter('product_id', ['nin' => $ids])->getColumnValues('product_id');
        $collection = $this->_productFactory->create()->getCollection()
            ->addFieldToFilter('entity_id', ['in' => $vendorProductIds])
            ->addFieldToFilter('type_id', 'simple');
        $collection->getSelect()->join(
            [
                'vendor_prod_table' => $collection->getTable('udropship_vendor_product')
            ],
            "`e`.`entity_id` = `vendor_prod_table`.`product_id` AND `vendor_prod_table`.`vendor_id` = '{$this->_vendorSession->getVendorId()}'",
            ['status']
        );
        foreach($collection as $item) {
            $status = (int)$item->getStatus();
            $result[$status]++;
            $result['all']++;
        }

        return $result;
    }

    public function isAllowedVendorDeleteProduct()
    {
        $isAllow = $this->_hlp->getScopeConfig('udprod/general/allow_remove');

        return $isAllow === "1" || $isAllow === 1 ? 'true' : 'false';
    }
}
