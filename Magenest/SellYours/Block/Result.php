<?php

namespace Magenest\SellYours\Block;

use Magento\Framework\App\ObjectManager;

class Result extends \Unirgy\DropshipSellYours\Block\Result
{

    protected function _getProductCollection()
    {
        $col = $this->getSearchModel()->getProductCollection();

        if ($this->_registry->registry('current_category')) {
            $col->addCategoryFilter($this->_registry->registry('current_category'));
        }
        $col->addAttributeToFilter('type_id', ['in'=>['simple','downloadable','virtual']]);
        $sess = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        if ($sess->getData('udsell_search_type')) {
            $col->addAttributeToFilter('entity_id', ['in'=>$sess->getVendor()->getVendorTableProductIds()]);
        }
        return $col;
    }
}
