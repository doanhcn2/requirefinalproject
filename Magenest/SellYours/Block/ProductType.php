<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\SellYours\Block;

use \Magento\Framework\View\Element\Template;

class ProductType extends Template
{
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getAttributesSet(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $attributesSet = $objectManager->create('\Magento\Catalog\Model\Product\AttributeSet\Options')->toOptionArray();

        return $attributesSet;
    }
}