<?php

namespace Magenest\SellYours\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\AbstractProduct;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Json\EncoderInterface as JsonEncoderInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\LayoutFactory;
use Unirgy\Dropship\Helper\Catalog;

class ProductView extends \Unirgy\DropshipSellYours\Block\ProductView
{
    /**
     * @var $_vendorFactory \Unirgy\Dropship\Model\VendorFactory
     */
    protected $_vendorFactory;

    /**
     * @var $_productFactory \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    public function __construct(Context $context,
                                EncoderInterface $urlEncoder,
                                JsonEncoderInterface $jsonEncoder,
                                StringUtils $string,
                                Product $productHelper,
                                ConfigInterface $productTypeConfig,
                                FormatInterface $localeFormat,
                                Session $customerSession,
                                ProductRepositoryInterface $productRepository,
                                PriceCurrencyInterface $priceCurrency,
                                Catalog $helperCatalog,
                                \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                array $data = [])
    {
        $this->_productFactory = $productFactory;
        $this->_vendorFactory = $vendorFactory;
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $helperCatalog, $data);
    }

    public function getProductViews($productId)
    {
        $products = $this->_productFactory->create()->load($productId);
        $product = $products->getData();
        /**
         * @todo Return list attribute here
         */
        $attributesSpecification=[];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $attributeGroupCollection = $objectManager
            ->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory');
        $groupCollection = $attributeGroupCollection->create()
            ->setAttributeSetFilter($product['attribute_set_id'])
            ->load(); // product attribute group collection
        foreach ($groupCollection as $group) {
            if ($group->getAttributeGroupName() == "Specifications") {
                $productAttributeCollection = $objectManager
                    ->create('\Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory');
                $groupAttributesCollection = $productAttributeCollection->create()
                    ->setAttributeGroupFilter($group->getId())
                    ->addVisibleFilter()
                    ->load();
                foreach ($groupAttributesCollection->getItems() as $attribute) {
                    $attributesSpecification[]=$attribute->getAttributeCode();
                }
            }
        }


        $result = [];
        foreach ($attributesSpecification as $key) {
            if(!@$product[$key])
                continue;
            $attribute = $products->getResource()->getAttribute($key);
            if ($attribute === false) continue;
            if ($attribute->getFrontendLabel()) {
                $label = $attribute->getFrontendLabel();
            } else {
                $label = $attribute->getDefaultFrontendLabel();
            }
            $values = $attribute->getFrontend()->getValue($products);
            if ($values instanceof \Magento\Framework\Phrase) {
                $finalValue = $values->getText();
            } else {
                $finalValue = $values;
            }

            array_push($result, [$label, $finalValue]);
        }

        $begin = array_slice($result, 0, 8);
        $end = array_slice($result, 8, count($result));
        return ['less' => $begin, 'more' => $end];
    }

    public function getVendorName($vendorId)
    {
        return $this->_vendorFactory->create()->load($vendorId)->getVendorName();
    }
}
