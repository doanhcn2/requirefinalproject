<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 5/23/18
 * Time: 1:23 PM
 */

namespace Magenest\SellYours\Block\Catalog;

use Magento\Framework\View\Element\Template;
use Unirgy\Dropship\Model\Source;
use Unirgy\Dropship\Helper\Data as HelperData;

/**
 * Class ProductView
 * @package Magenest\SellYours\Block\Catalog
 */
class ProductView extends \Magento\Framework\View\Element\Template
{
    /**
     * @var HelperData
     */
    protected $_helperData;
    /**
     * @var Source
     */
    protected $_modelSource;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    /**
     * @var \Unirgy\Dropship\Model\Vendor
     */
    protected $_udprodVendor;

    /**
     * @var \Unirgy\Dropship\Model\Vendor
     */
    protected $vendor;

    /**
     * @var string|float
     */
    protected $lowestPrice;

    /**
     * @var $_micrositeHelper \Unirgy\DropshipMicrosite\Helper\Data
     */
    protected $_micrositeHelper;

    /**
     * ProductView constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Unirgy\Dropship\Model\Vendor $vendor
     * @param Template\Context $context
     * @param \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper
     * @param Source $modelSource
     * @param HelperData $helperData
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Unirgy\Dropship\Model\Vendor $vendor,
        Template\Context $context,
        \Unirgy\DropshipMicrosite\Helper\Data $micrositeHelper,
        Source $modelSource,
        HelperData $helperData,
        array $data = []
    )
    {
        $this->_modelSource = $modelSource;
        $this->_micrositeHelper = $micrositeHelper;
        $this->_udprodVendor = $vendor;
        $this->_coreRegistry = $registry;
        $this->_helperData = $helperData;

        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getCurrentProduct()
    {
        return $this->_coreRegistry->registry('current_product') ?: $this->_coreRegistry->registry('product');
    }

    /**
     * @return string
     */
    protected function getCurrentVendorId()
    {
        return intval($this->getBestVendor());
    }

    /**
     * @return $this|\Unirgy\Dropship\Model\Vendor
     */
    protected function getCurrentVendor()
    {
        if (!$this->vendor) {
            $this->vendor = $this->_udprodVendor->load($this->getCurrentVendorId());
        }
        return $this->vendor;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
        return $this->getCurrentVendor()->getVendorName();
    }

    /**
     * @return string
     */
    public function getSellerNote()
    {
        return @$this->getMultiVendorData()[intval($this->getBestVendor())]["offer_note"];
    }

    /**
     * @return string
     */
    public function getAskVendorUrl()
    {
        return $this->getUrl('udqa/customer/new/');
    }

    /**
     * @return string
     */
    public function getSellOnGosawaUrl()
    {
        return $this->getUrl('udropship/vendor/index/');
    }

    /**
     * @return array
     */
    protected function getMultiVendorData()
    {
        $return = $this->getCurrentProduct()->getMultiVendorData();
        if (!is_array($return)) {
            return [];
        }
        return $return;
    }

    /**
     * @return float|null|string
     */
    public function getLowestPrice()
    {
        if ($this->lowestPrice === null) {
            $bestVendor = $this->getBestVendor();
            $listPriceOtherVendor = [];
            foreach ($this->getMultiVendorData() as $key => $vendorData) {
                if (intval($bestVendor) === $key) {
                    continue;
                }
                if ($vendorData['special_price']) {
                    $specialFrom = $vendorData['special_from_date'];
                    $specialTo = $vendorData['special_to_date'];
                    if ($specialFrom === null && $specialTo !== null) {
                        $result = $this->compareVendorPrice($vendorData, (time() < strtotime($specialTo)));
                    } elseif ($specialTo === null && $specialFrom !== null) {
                        $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom)));
                    } elseif ($specialFrom === null && $specialTo === null) {
                        $result['current_price'] = $vendorData['special_price'];
                    } else {
                        $result = $this->compareVendorPrice($vendorData, (time() > strtotime($specialFrom) && time() < strtotime($specialTo)));
                    }
                } else {
                    $result['current_price'] = $vendorData['vendor_price'];
                }

                array_push($listPriceOtherVendor, $result['current_price']);
            }

            if (is_array($listPriceOtherVendor)) {
                $this->lowestPrice = min($listPriceOtherVendor);
            }
        }
        return $this->lowestPrice;
    }

    /**
     * @param $vendorData
     * @param $compare
     * @return array
     */
    private function compareVendorPrice($vendorData, $compare)
    {
        $result = [];
        if ($compare) {
            $result['old_price'] = $vendorData['vendor_price'];
            $result['current_price'] = $vendorData['special_price'];
        } else {
            $result['current_price'] = $vendorData['vendor_price'];
        }

        return $result;
    }

    /**
     * @return int|void
     */
    public function getOfferNumber()
    {
        return count($this->getMultiVendorData());
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    /**
     * @return bool
     */
    public function isSimpleProduct()
    {
        $product = $this->getCurrentProduct();
        if ($product->getTypeId() === 'simple' || $product->getTypeId() === 'Simple') {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isConfigurationNotGroupon()
    {
        $product = $this->getCurrentProduct();
        $isDeal = \Magenest\Groupon\Helper\Data::isDeal($product->getEntityId());
        if (($product->getTypeId() === 'configuration' && !$isDeal) || ($product->getTypeId() === 'configuration' && !$isDeal)) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    private function getBestVendor()
    {
        return (int)$this->_micrositeHelper->getCurrentVendor()->getVendorId();
    }

    /**
     * @return array
     */
    public function getVendors()
    {
        $product = $this->getCurrentProduct();
        $simpleProducts = [];
        if ($product->getTypeId() == 'configurable') {
            $simpleProducts = $product->getTypeInstance(true)->getUsedProducts($product);
        }
        array_unshift($simpleProducts, $product);
        $vendors = $this->_modelSource->getVendors(true);
        $vIds = [];
        $isUdm = $this->_helperData->isUdmultiActive();
        foreach ($simpleProducts as $p) {
            if ($isUdm) {
                $_vIds = $p->getMultiVendorData();
                $_vIds = is_array($_vIds) ? array_keys($_vIds) : [];
                $vIds = array_merge($vIds, $_vIds);
            } else {
                $vIds[] = $p->getUdropshipVendor();
            }
        }
        $vIds = array_filter($vIds);
        return array_intersect_key($vendors, array_flip($vIds));
    }
}
