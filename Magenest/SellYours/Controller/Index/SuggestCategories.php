<?php

namespace Magenest\SellYours\Controller\Index;

class SuggestCategories extends \Unirgy\Dropship\Controller\Index\SuggestCategories
{
    /**
     * Category list suggestion based on already entered symbols
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setJsonData(
            $this->layoutFactory->create()->createBlock('Magenest\SellYours\Block\Adminhtml\Category\Tree')
                ->getSuggestedCategoriesJson($this->getRequest()->getParam('label_part'), $this->getRequest()->getParam('type', null))
        );
    }
}
