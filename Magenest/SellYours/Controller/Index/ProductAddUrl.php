<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\SellYours\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class ProductAddUrl extends Action
{
    /**
     * @var $_categoryFactory \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    public function __construct(Context $context,
                                \Magento\Catalog\Model\CategoryFactory $categoryFactory)
    {
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        if ($this->getRequest()->getParams()) {
            $params = $this->getRequest()->getParams();
            $categories = json_decode($params['category']);
            $attributeSet= $params['attribute_set'];
            $category = $this->_categoryFactory->create()->getCollection()->addFieldToFilter('entity_id', $params)->getFirstItem();
            $url = $this->_url->getUrl('udprod/vendor/productNew');

            $url .= '?set_id='.$attributeSet.'-Simple&type_of_product=Simple';
            if (@$category) {
                $data = serialize($categories);
                $url .= "&categories=$data";
            }
            $resultArray = ['url' => $url];
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $result->setData($resultArray);

            return $result;
        }

        return;
    }
}
