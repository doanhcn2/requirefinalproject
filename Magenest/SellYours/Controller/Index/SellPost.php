<?php

namespace Magenest\SellYours\Controller\Index;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogSearch\Helper\Data as CatalogSearchHelperData;
use Magento\CatalogSearch\Model\Advanced;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipSellYours\Controller\Index\SellPost as UnirgySellPost;
use Unirgy\DropshipSellYours\Helper\Data as DropshipSellYoursHelperData;

class SellPost extends UnirgySellPost
{
    /**
     * @var $_unirgyProductFactory \Unirgy\Dropship\Model\Vendor\ProductFactory
     */
    protected $_unirgyProductFactory;

    public function __construct(Context $context,
                                ScopeConfigInterface $scopeConfig,
                                DesignInterface $viewDesignInterface,
                                StoreManagerInterface $storeManager,
                                LayoutFactory $viewLayoutFactory,
                                Registry $registry,
                                ForwardFactory $resultForwardFactory,
                                HelperData $helper, PageFactory $resultPageFactory,
                                RawFactory $resultRawFactory, Header $httpHeader,
                                CategoryFactory $modelCategoryFactory, Advanced $modelAdvanced,
                                CatalogSearchHelperData $helperData, DropshipSellYoursHelperData $dropshipSellYoursHelperData,
                                LoggerInterface $logLoggerInterface,
                                \Unirgy\Dropship\Model\Vendor\ProductFactory $productFactory,
                                ProductFactory $modelProductFactory)
    {
        $this->_unirgyProductFactory = $productFactory;
        parent::__construct($context, $scopeConfig, $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory, $httpHeader, $modelCategoryFactory, $modelAdvanced, $helperData, $dropshipSellYoursHelperData, $logLoggerInterface, $modelProductFactory);
    }

    public function execute()
    {
        $hlp = $this->_helperData;
        if ($product = $this->_initProduct()) {
            if (!$this->_getVendorSession()->authenticate($this)) {
                if ($this->getRequest()->isPost()) {
                    $this->_saveSellYoursFormData();
                }
            } else {
                $formData = [];
                if ($this->getRequest()->isPost()) {
                    $formData = $this->getRequest()->getPost();
                } else {
                    $formData = $this->_fetchSellYoursFormData();
                }
                if (!empty($formData)) {
                    $this->_syHlp->processSellRequest($this->_getVendorSession()->getVendor(), $product, $formData);
                    if (isset($formData->get('udmulti')['handling_time']) || isset($formData->get('udmulti')['offer_note'])) {
                        $this->saveOtherInformation($formData->get('udmulti'), $product->getId());
                    }
                    $this->_getC2CSession()->setHideAlreadySellingMsg(true);
                    $this->messageManager->addSuccess(__('Your sell request was succesfully submitted'));
                } else {
                    $this->messageManager->addError(__('Empty sell request form data'));
                }
                $this->_redirect('*/*/sell', ['id'=>$product->getId()]);
            }
        } elseif (!$this->getResponse()->isRedirect()) {
            $this->_forward('noRoute');
        }
    }

    protected function saveOtherInformation($data, $productId)
    {
        $vendorId = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
        $productVendor = $this->_unirgyProductFactory->create()->getCollection()->addFieldToFilter('product_id', $productId)->addFieldToFilter('vendor_id', $vendorId)->getFirstItem();
        $productVendor->setHandlingTime(@$data['handling_time']);
        $productVendor->setOfferNote(@$data['offer_note']);
        if (@$data['freeshipping'] === "0" || @$data['freeshipping'] === '1') {
            $productVendor->setFreeshipping(1);
        } else {
            $productVendor->setFreeshipping(0);
        }
        $productVendor->save();
    }
}

