<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 10/05/2018
 * Time: 13:44
 */
namespace Magenest\SellYours\Controller\Inventory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Model\App;
use Magento\Store\Model\ScopeInterface;
use Unirgy\DropshipVendorProduct\Model\ProductFactory;
class MassDelete extends \Unirgy\DropshipVendorProduct\Controller\Vendor\AbstractVendor{

    protected $_dropshipProductFactory;
    public function __construct(
      \Unirgy\DropshipVendorProduct\Helper\Data $helperData,
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\View\DesignInterface $viewDesignInterface,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Framework\View\LayoutFactory $viewLayoutFactory,
      \Magento\Framework\Registry $registry,
      \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
      \Unirgy\Dropship\Model\Vendor\ProductFactory $dropshipProductFactory,
      \Unirgy\Dropship\Helper\Data $helper,
      \Magento\Framework\View\Result\PageFactory $resultPageFactory,
      \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
      \Magento\Framework\HTTP\Header $httpHeader,
      \Unirgy\Dropship\Helper\Catalog $helperCatalog,
      \Unirgy\DropshipVendorProduct\Model\ProductFactory $modelProductFactory,
      \Magento\MediaStorage\Helper\File\Storage\Database $storageDatabase
    ) {
        parent::__construct($helperData, $context, $scopeConfig,
          $viewDesignInterface, $storeManager, $viewLayoutFactory, $registry,
          $resultForwardFactory, $helper, $resultPageFactory, $resultRawFactory,
          $httpHeader, $helperCatalog, $modelProductFactory, $storageDatabase);
        $this->_dropshipProductFactory = $dropshipProductFactory;
    }

    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $error = false;
        $message = 'Deleted Success !!';
        $jsonIds = $this->getRequest()->getParam('vendor_product_ids');
        $vendorsProductIds = json_decode($jsonIds,true);
        $vendorId = $this->_getSession()->getVendorId();
        if($vendorId==null || $vendorId == '') $error=true;
        $oldStoreId = $this->_storeManager->getStore()->getId();
        if(is_array($vendorsProductIds) && count($vendorsProductIds)>0 && !$error){
            /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection*/
            $collection = $this->_dropshipProductFactory->create()->getCollection()
              ->addFieldToFilter('vendor_id', $vendorId)
              ->addFieldToFilter('vendor_product_id', ['in' => $vendorsProductIds]);
            $coresProductIds = $collection->getColumnValues('product_id');
            foreach ($coresProductIds as $pId){
                if (!$this->scopeConfig->isSetFlag('udprod/general/allow_remove', ScopeInterface::SCOPE_STORE)) {
                    $error =true;
                    $message = 'Forbidden';
                } else {
                    $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
                    $this->_storeManager->setCurrentStore(0);
                    try {
                        $this->_prodHlp->checkProduct($pId);
                        $simplePids = $this->_helperCatalog->getCfgSimplePids($pId);
                        $delPids = [];
                        if (!empty($simplePids) && is_array($simplePids)) {
                            $delPids = $simplePids;
                        }
                        $delPids[] = $pId;
                        $delProducts = $this->_modelProductFactory->create()->getCollection()->addIdFilter($delPids)
                          ->setFlag('udskip_price_index',1)
                          ->setFlag('has_group_entity', 1)
                          ->setFlag('has_stock_status_filter', 1);
                        foreach ($delProducts as $_delProd) {
                            $_delProd->delete();
                        }
                        $this->_storeManager->setCurrentStore($oldStoreId);
                    } catch (\Exception $e) {
                        $this->_storeManager->setCurrentStore($oldStoreId);
                        $this->messageManager->addError($e->getMessage());
                        $error = true;
                        $message = $e->getMessage();
                    }
                }
            }
            $resultJson->setData(
              ['error' => $error, 'message' => $message]
            );
            return $resultJson;
        }
    }
}