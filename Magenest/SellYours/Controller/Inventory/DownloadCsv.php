<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 10/05/2018
 * Time: 09:14
 */
namespace Magenest\SellYours\Controller\Inventory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor;
use Magento\Catalog\Model\Product;
class DownloadCsv extends AbstractVendor{
    protected $_productFactory;

    protected $_vendorFactory;

    protected $_vendorSession;

    protected $_productRepository;

    protected $_coreProductFactory;

    protected $_attributeRepository;

    protected $_imageBuilder;

    protected $_fileSystem;

    protected $directory;

    protected $fileFactory;
    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\View\DesignInterface $viewDesignInterface,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Framework\View\LayoutFactory $viewLayoutFactory,
      \Magento\Framework\Registry $registry,
      \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
      \Unirgy\Dropship\Helper\Data $helper,
      \Magento\Framework\View\Result\PageFactory $resultPageFactory,
      \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
      \Magento\Framework\HTTP\Header $httpHeader,
      \Unirgy\Dropship\Model\Session $vendorSession,
      \Unirgy\Dropship\Model\Vendor\ProductFactory $productFactory,
      \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
      \Magento\Catalog\Model\ProductFactory $coreProductFactory,
      \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
      \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
      \Unirgy\Dropship\Model\VendorFactory $vendorFactory,
      \Magento\Framework\Filesystem $filesystem,
      \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        parent::__construct($context, $scopeConfig, $viewDesignInterface,
          $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory,
          $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
        parent::__construct($context, $scopeConfig, $viewDesignInterface,
          $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory,
          $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
        $this->_productFactory = $productFactory;
        $this->_vendorFactory= $vendorFactory;
        $this->_vendorSession = $vendorSession;
        $this->_productRepository = $productRepository;
        $this->_coreProductFactory = $coreProductFactory;
        $this->_attributeRepository = $attributeRepository;
        $this->_imageBuilder = $imageBuilder;
        $this->_fileSystem = $filesystem;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->fileFactory = $fileFactory;
    }

    public function execute()
    {
        try {
            $jsonData = $this->getRequest()->getParam('json_data');
            if ($jsonData != '') {
                $data       = json_decode($jsonData, true);
                $collection = $this->getSampleCollection($data['status'],
                  $data['key_word'], $data['sort_field'], $data['sort_order']);
                if (isset($data['checked_all']) && $data['checked_all'] == 1) {

                } elseif (isset($data['checked_all'])
                  && $data['checked_all'] == 0
                ) {
                    $this->applyCheckedIdsFiter($collection,
                      $data['checked_id']);
                }
                $heading    = [
                  __('Product Name'),
                  __('Status'),
                  __('Stock Status'),
                  __('Sold'),
                  __('Price'),
                  __('Revenue'),
                  __('Views'),
                  __('Quantity'),
                  __('Last Updated')
                ];
                $file = 'var/tmp/' . "Inventory_" . date('Ymd_His') . ".csv";
                $this->directory->create('export');
                $stream = $this->directory->openFile($file, 'w+');
                $stream->lock();
                $stream->writeCsv($heading);
                if ($collection->getSize() < 500) {
                    $productIds = $collection->getColumnValues('product_id');
                    $products = $this->_coreProductFactory->create()->getCollection()
                      ->addAttributeToSelect(['name'])
                      ->addFieldToFilter('entity_id', ['in' => $productIds])->getItems();
                    foreach ($collection as $item) {
                        if(isset($products[$item->getProductId()])) {
                            $product = $products[$item->getProductId()];
                            $row     = [
                              $product->getName(),
                              Load::getCampaignStatus($item->getCampaignStatus()
                              != null ? $item->getCampaignStatus() : 0),
                              $product->isInStock() ? 'In Stock' : 'Out Stock',
                              $item->getSold() != null ? $item->getSold() : 0,
                              $item->getVendorPrice(),
                              $item->getRevenue() != null ? $item->getRevenue()
                                : 0,
                              $item->getCountView() != null
                                ? $item->getCountView()
                                : 0,
                              $item->getStockQty(),
                              Load::getDateString($item->getProductUpdatedAt())
                            ];
                            $stream->writeCsv($row);
                        }
                    }
                }else{
                    $collection->setPageSize(500);
                    for($i=1; $i<=$collection->getLastPageNumber(); $i++){
                        $collection->clear();
                        $collection->resetData();
                        $collection->setPageSize(500);
                        $collection->setCurPage($i);
                        $productIds = $collection->getColumnValues('product_id');
                        /** @var  $products \Magento\Catalog\Model\ResourceModel\Product\Collection*/
                        $products = $this->_coreProductFactory->create()->getCollection()
                          ->addAttributeToSelect(['name'])
                          ->addFieldToFilter('entity_id', ['in' => $productIds])->getItems();
                        foreach ($collection as $item) {
                            if(isset($products[$item->getProductId()])) {
                                $product = $products[$item->getProductId()];
                                $row     = [
                                  $product->getName(),
                                  Load::getCampaignStatus($item->getCampaignStatus()
                                  != null ? $item->getCampaignStatus() : 0),
                                  $product->isInStock() ? 'In Stock'
                                    : 'Out Stock',
                                  $item->getSold() != null ? $item->getSold()
                                    : 0,
                                  $item->getVendorPrice(),
                                  $item->getRevenue() != null
                                    ? $item->getRevenue() : 0,
                                  $item->getCountView() != null
                                    ? $item->getCountView()
                                    : 0,
                                  $item->getStockQty(),
                                  Load::getDateString($item->getProductUpdatedAt())
                                ];
                                $stream->writeCsv($row);
                            }
                        }
                    }
                }
                return $this->fileFactory->create("Inventory_" . date('Ymd_His') . ".csv", [
                  'type' => 'filename',
                  'value' => $file,
                  'rm' => true  // can delete file after use
                ], 'var');
            }
        }catch (\Exception $exception){
            ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->critical($exception->getMessage());
            throw new \Exception(__("Sorry some thing wrong"));
        }
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            //set appropriate headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
        }
    }

    public function getSampleCollection($status, $keyWord, $sortField, $sortOrder){
        $vendorId = $this->_vendorSession->getVendorId();
        /** @var  $product \Magento\Catalog\Model\Product*/
        $campaignStatusAtt = $this->_attributeRepository->get(Product::ENTITY,'campaign_status');
        if($campaignStatusAtt!=null){
            $campaignStatusAttId = $campaignStatusAtt->getAttributeId();
        }else{
            $campaignStatusAttId=0;
        }
        $productNameAttr = $this->_attributeRepository->get(Product::ENTITY,'name');
        if($productNameAttr!=null){
            $productNameAttId = $productNameAttr->getAttributeId();
        }else{
            $productNameAttId=0;
        }
        $subQuery = new \Zend_Db_Expr('(SELECT  sum(qty_ordered) as sold, product_id from sales_order_item GROUP BY product_id)');
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->_productFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            [
                'view_table' => $collection->getTable('report_event')
            ],'main_table.product_id = view_table.object_id AND view_table.event_type_id = '.\Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW
            ,[]
        )->joinLeft(
            [
                'product_table' => $collection->getTable('catalog_product_entity')
            ],'main_table.product_id = product_table.entity_id'
            ,[]
        )->joinLeft(
            [
                'campain_status_table' => $collection->getTable('catalog_product_entity_varchar')
            ],"product_table.row_id= campain_status_table.row_id AND campain_status_table.attribute_id='".$campaignStatusAttId."'"
            ,[]
        )->joinLeft(
            [
                'product_name_table' => $collection->getTable('catalog_product_entity_varchar')
            ],"product_table.row_id= product_name_table.row_id AND product_name_table.attribute_id='".$productNameAttId."'"
            ,[]
        )->joinLeft(
            [
                'sold_table' => $subQuery
            ],'sold_table.product_id = main_table.product_id'
            ,[]
        );
        $collection->addExpressionFieldToSelect('revenue','({{vendor_price}} * {{sold}})',array('vendor_price'=>'main_table.vendor_price','sold' => 'sold_table.sold'));
        $collection->getSelect()->columns(['count_view' => 'count(view_table.event_id)']);
        $collection->getSelect()->columns(['campaign_status' => 'campain_status_table.value']);
        $collection->getSelect()->columns( ['product_updated_at' => 'product_table.updated_at']);
        //        $collection->getSelect()->columns(['revenue' => 'main_table.vendor_price*sum(order_item_table.qty_ordered)']);
        $collection->getSelect()->columns(['sold' => 'sold_table.sold']);
        $collection->getSelect()->columns(['product_name' => 'product_name_table.value']);
        $collection->addFieldToFilter('main_table.vendor_id',$vendorId);
        $collection->getSelect()
            ->group([
                'main_table.vendor_product_id',
                'campain_status_table.value',
                'product_name_table.value',
                'product_table.updated_at',
                'sold_table.sold',
            ]);
        $this->applyStatusFilter($collection,$status);
        $this->applySortOrder($collection, Load::sortFieldMap($sortField), $sortOrder);
        $this->applyKeyWord($collection, $keyWord);
        return $collection;
    }

    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     *
     * @return mixed
     */
    public function applyStatusFilter($collection,$status){
        try {
            if ((int)$status>=-1&& $status<=8) $filterStatus = $status;
            else $filterStatus = -2;
        }catch (\Exception $e){
            $filterStatus = -2;
        }
        if($filterStatus != -2){
            if($filterStatus!=2) {
                $collection->addFieldToFilter('campain_status_table.value', $filterStatus);
            }else{
                $collection->addFieldToFilter(['campain_status_table.value','campain_status_table.value'], [$filterStatus,['null' => true]]);
            }
        }
        return;
    }
    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function applySortOrder($collection,$sortField, $sortOrder){
        $collection->setOrder( $sortField, $sortOrder);
        $collection->addOrder('main_table.vendor_product_id', 'DESC');
        return;
    }

    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function applyKeyWord($collection,$keyWord){
        if($keyWord!=''){
            $keyWord = str_replace('%',"\%",$keyWord);
            $keyWord = str_replace('*', "\*", $keyWord);
            $paramKeyWord = '%'.$keyWord.'%';
            $collection->addFieldToFilter(['main_table.vendor_sku','product_table.sku', 'product_name_table.value'],
              [['like' => $paramKeyWord],['like' => $paramKeyWord], ['like' => $paramKeyWord]]);
        }
        return;
    }

    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     * @param $checkedIds
     */
    public function applyCheckedIdsFiter($collection, $checkedIds)
    {
        $collection->addFieldToFilter('main_table.vendor_product_id', ['in' => $checkedIds]);
        return;
    }
}