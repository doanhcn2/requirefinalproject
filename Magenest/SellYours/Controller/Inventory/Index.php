<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 05/05/2018
 * Time: 14:35
 */
namespace Magenest\SellYours\Controller\Inventory;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor;
use Magento\Framework\App\ObjectManager;
class Index extends AbstractVendor{

    public function execute()
    {
        if (!$this->isVendorAllow()) {
            parent::_forward('index', 'vendor', 'udropship');
        }
        $session = ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session');
        $session->setUdprodLastGridUrl($this->_url->getUrl('*/*/*', ['__vp'=>true,'_current'=>true]));
        $this->_renderPage(null, 'udsell_inventory');
    }

    protected function isVendorAllow()
    {
        $vendorId = $this->_hlp->session()->getVendorId();
        $vendor = $this->_hlp->getVendor($vendorId);
        $promoteProduct = $vendor->getPromoteProduct();
        if ($promoteProduct === '4') {
            return true;
        } else {
            return false;
        }
    }
}