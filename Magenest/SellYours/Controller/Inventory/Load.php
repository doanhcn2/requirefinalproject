<?php
/**
 * Created by PhpStorm.
 * User: duchai
 * Date: 07/05/2018
 * Time: 13:57
 */
namespace Magenest\SellYours\Controller\Inventory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;
use Unirgy\Dropship\Controller\Vendor\AbstractVendor;
use Magenest\SellYours\Model\Status\Source as Status;
class Load extends AbstractVendor{

    protected $_productFactory;

    protected $_vendorFactory;

    protected $_vendorSession;

    protected $_productRepository;

    protected $_coreProductFactory;

    protected $_attributeRepository;

    protected $_imageBuilder;

    public function __construct(
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Framework\View\DesignInterface $viewDesignInterface,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Framework\View\LayoutFactory $viewLayoutFactory,
      \Magento\Framework\Registry $registry,
      \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
      \Unirgy\Dropship\Helper\Data $helper,
      \Magento\Framework\View\Result\PageFactory $resultPageFactory,
      \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
      \Magento\Framework\HTTP\Header $httpHeader,
      \Unirgy\Dropship\Model\Session $vendorSession,
      \Unirgy\Dropship\Model\Vendor\ProductFactory $productFactory,
      \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
      \Magento\Catalog\Model\ProductFactory $coreProductFactory,
      \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
      \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
      \Unirgy\Dropship\Model\VendorFactory $vendorFactory
    ) {
        parent::__construct($context, $scopeConfig, $viewDesignInterface,
          $storeManager, $viewLayoutFactory, $registry, $resultForwardFactory,
          $helper, $resultPageFactory, $resultRawFactory, $httpHeader);
        $this->_productFactory = $productFactory;
        $this->_vendorFactory= $vendorFactory;
        $this->_vendorSession = $vendorSession;
        $this->_productRepository = $productRepository;
        $this->_coreProductFactory = $coreProductFactory;
        $this->_attributeRepository = $attributeRepository;
        $this->_imageBuilder = $imageBuilder;
    }

    public function execute()
    {
        /** @var  $resultJson \Magento\Framework\Controller\Result\Json */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $productData = $this->getSingleData();
        $resultJson->setData($productData);
        return $resultJson;
    }


    public function getPageSize(){
        return 20;
    }

    public function getPage(){
        return $this->getRequest()->getParam('page')!=''?$this->getRequest()->getParam('page'):1;
    }

    public function getSingleData(){
        $vendorId = $this->_vendorSession->getVendorId();
        $result = array();
        /** @var  $product \Magento\Catalog\Model\Product*/
        $campaignStatusAtt = $this->_attributeRepository->get(Product::ENTITY,'campaign_status');
        if($campaignStatusAtt!=null){
            $campaignStatusAttId = $campaignStatusAtt->getAttributeId();
        }else{
            $campaignStatusAttId=0;
        }
        $productNameAttr = $this->_attributeRepository->get(Product::ENTITY,'name');
        if($productNameAttr!=null){
            $productNameAttId = $productNameAttr->getAttributeId();
        }else{
            $productNameAttId=0;
        }
        $subQuery = new \Zend_Db_Expr('(SELECT  sum(qty_ordered) as sold, product_id from sales_order_item WHERE udropship_vendor = ' . $this->_vendorSession->getVendorId() . ' GROUP BY product_id)');
        /** @var  $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection */
        $collection = $this->_productFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
          [
            'view_table' => $collection->getTable('report_event')
          ],'main_table.product_id = view_table.object_id AND view_table.event_type_id = '.\Magento\Reports\Model\Event::EVENT_PRODUCT_VIEW
          ,[]
        )->join(
          [
            'product_table' => $collection->getTable('catalog_product_entity')
          ],"`main_table`.`product_id` = `product_table`.`entity_id` AND `product_table`.`type_id` = 'simple'"
          ,[]
        )->join(
          [
            'vendor_prod_table' => $collection->getTable('udropship_vendor_product')
          ],
          "`main_table`.`product_id` = `vendor_prod_table`.`product_id` AND `vendor_prod_table`.`vendor_id` = '{$this->_vendorSession->getVendorId()}'",
          ['status']
        )->joinLeft(
          [
            'campain_status_table' => $collection->getTable('catalog_product_entity_varchar')
          ],"product_table.row_id= campain_status_table.row_id AND campain_status_table.attribute_id='".$campaignStatusAttId."'"
          ,[]
        )->joinLeft(
          [
            'product_name_table' => $collection->getTable('catalog_product_entity_varchar')
          ],"product_table.row_id= product_name_table.row_id AND product_name_table.attribute_id='".$productNameAttId."'"
          ,[]
        )->joinLeft(
            [
                'sold_table' => $subQuery
            ],'sold_table.product_id = main_table.product_id'
            ,[]
        );
        $collection->addExpressionFieldToSelect('revenue','({{vendor_price}} * {{sold}})',array('vendor_price'=>'main_table.vendor_price','sold' => 'sold_table.sold'));
        $collection->setFlag('has_stock_status_filter', 1);
        $collection->getSelect()->columns(['count_view' => 'count(view_table.event_id)']);
        $collection->getSelect()->columns(['campaign_status' => 'campain_status_table.value']);
        $collection->getSelect()->columns(['product_updated_at' => 'product_table.updated_at']);
        $collection->getSelect()->columns(['sold' => 'sold_table.sold']);
        $collection->getSelect()->columns(['product_name' => 'product_name_table.value']);
        $collection->addFieldToFilter('main_table.vendor_id',$vendorId);
        $collection->getSelect()
          ->group([
              'main_table.vendor_product_id',
              'campain_status_table.value',
              'product_name_table.value',
              'product_table.updated_at',
              'sold_table.sold',
          ]);
        $this->applyStatusFilter($collection);
        $this->applySortOrder($collection);
        $this->applyKeyWord($collection);
        $collection->setPageSize($this->getPageSize());
        $collection->setCurPage((int)$this->getPage());
        $productIds = $collection->getColumnValues('product_id');
        $products = $this->_coreProductFactory->create()->getCollection()
            ->addAttributeToSelect(['name', 'small_image'])
            ->addFieldToFilter('entity_id', ['in' => $productIds])
            ->setFlag('has_stock_status_filter', 1)
            ->getItems();
        foreach ($collection->getItems() as $item){
            if(isset($products[$item->getProductId()])) {
                $product = $products[$item->getProductId()];
                /** @var  $product  \Magento\Catalog\Model\Product */
                $data     = [
                  'id'           => $item->getVendorProductId(),
                  'name'         => $product->getName(),
                  'editUrl'      => ObjectManager::getInstance()->create(UrlInterface::class)->getUrl('udprod/vendor/productEdit', ['id' => $product->getId()]),
                  'image'        => $this->getImageUrl($product),
                  'status'       => $this->getCampaignStatus($item->getStatus() != null ? $item->getStatus() : 0),
                  'stock_status' => $product->isInStock() ? 'In Stock' : 'Out Stock',
                  'sold'         => number_format($item->getSold() != null ? $item->getSold() : 0, 2),
                  'price'        => number_format($item->getVendorPrice(), 2),
                  'revenue'      => number_format($item->getRevenue() != null ? $item->getRevenue() : 0, 2),
                  'views'        => intval($item->getCountView() != null ? $item->getCountView() : 0),
                  'quantity'     => number_format($item->getStockQty(), 2),
                  'last_updated' => self::getDateString($item->getProductUpdatedAt()),
                ];
                $result[] = $data;
            }
        }
        if ($result) {
            $firstIndex = ($collection->getCurPage() - 1) * $this->getPageSize() + 1;
            $lastIndex = $collection->getCurPage() * $this->getPageSize();
            if ($lastIndex > $collection->count()) {
                $lastIndex = $collection->count();
            }
            $returnData = [
                'items' => $result,
                'total' => $collection->count(),
                'lastPage' => $collection->getLastPageNumber(),
                'firstIndex' => $firstIndex,
                'lastIndex' => $lastIndex
            ];
        } else {
            $returnData = [
                'items' => $result,
                'total' => 0,
                'lastPage' => $collection->getLastPageNumber(),
                'firstIndex' => 0,
                'lastIndex' => 0
            ];
        }

        return $returnData;
    }

    public static function getCampaignStatus($value)
    {
        /** @var \Magenest\SellYours\Model\Status\Source $statusSource */
        $statusSource = ObjectManager::getInstance()->get(\Magenest\SellYours\Model\Status\Source::class);
        $statusArray  = $statusSource->getSatusArray();

        if (isset($statusArray[$value])) {
            return $statusArray[$value];
        }

        return __('Pending Review');
    }

    /**
     * @param $collection \Magento\Sales\Model\ResourceModel\Report\Collection\AbstractCollection
     *
     * @return mixed
     */
    public function applyStatusFilter($collection){
        $status = $this->getRequest()->getParam('status');
        try {
            if ((int)$status >= -1 && $status <= 9) {
                $filterStatus = $status;
            } else {
                $filterStatus = -2;
            }
        } catch (\Exception $e) {
            $filterStatus = -2;
        }
        if ($filterStatus != -2) {
            $collection->addFieldToFilter('vendor_prod_table.status', $filterStatus);
        }

        return;
    }

    public static function getDateString($time){
        return date('d/m/Y \a\t h:i A',strtotime($time));
    }

    public function getImageUrl ($product){
        $image =  $this->_imageBuilder->setProduct($product)
          ->setImageId('product_small_image')
          ->create();
        return $image->getImageUrl();
    }

    public function getSortField(){
        $requestField = $this->getRequest()->getParam('sort_field');
        if($requestField=='last_updated' ||$requestField=='quantity'
          ||$requestField=='view' ||$requestField=='revenue'
          ||$requestField=='price' ||$requestField=='sold'){
            return $requestField;
        }
        return 'last_updated';
    }

    public function getSortOrder(){
        $requestOrder = $this->getRequest()->getParam('sort_order');
        if($requestOrder=='desc'||$requestOrder=='asc'){
            return strtoupper($requestOrder);
        }
        return 'DESC';
    }

    public static function sortFieldMap($key){
        $data = [
          'last_updated' => 'product_updated_at',
          'quantity'     => 'main_table.stock_qty',
          'view'         => 'count_view',
          'revenue'      => 'revenue',
          'price'        => 'vendor_price',
          'sold'         => 'sold',
        ];
        return $data[$key];
    }

    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function applyKeyWord($collection){
        $keyWord = $this->getRequest()->getParam('key_word');
        if($keyWord!=''){
            $keyWord = str_replace('%',"\%",$keyWord);
            $keyWord = str_replace('*', "\*", $keyWord);
            $paramKeyWord = '%'.$keyWord.'%';
            $collection->addFieldToFilter(['main_table.vendor_sku','product_table.sku', 'product_name_table.value'],
              [['like' => $paramKeyWord],['like' => $paramKeyWord], ['like' => $paramKeyWord]]);
        }
        return;
    }
    /**
     * @param $collection \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function applySortOrder($collection){
        $sortOrder = $this->getSortOrder();
        $sortField = self::sortFieldMap($this->getSortField());
        $collection->setOrder( $sortField, $sortOrder);
        $collection->addOrder('main_table.vendor_product_id', 'DESC');
        return;
    }
}
