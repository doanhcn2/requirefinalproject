<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\SellYours\Plugin;

class AddHandlingTimeField
{
    public function afterGetVendorEditFieldsConfig(\Unirgy\DropshipVendorProduct\Helper\Data $data, $result)
    {
        $result['udmulti.handling_time'] = __('Handling Time').' [udmulti.handling_time]';
        $result['udmulti.offer_note'] = __('Offer Note (optional').' [udmulti.offer_note]';
        return $result;
    }
}
