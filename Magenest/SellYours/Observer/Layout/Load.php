<?php

namespace Magenest\SellYours\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Block\Product\Context;
use Magenest\Ticket\Model\EventFactory;

/**
 * Class LayoutLoadBeforeFrontend
 * @package Magenest\Reservation\Observer\Layout
 */
class Load implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var EventFactory
     */
    protected $eventFactory;

    public function __construct(Context $context,
                                EventFactory $eventFactory)
    {
        $this->eventFactory = $eventFactory;
        $this->_coreRegistry = $context->getRegistry();
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->isSimpleProduct()) {
            $fullActionName = $observer->getEvent()->getFullActionName();
            /** @var  $layout \Magento\Framework\View\Layout */
            $layout = $observer->getEvent()->getLayout();
            $handler = '';
            if ($fullActionName == 'catalog_product_view' || $fullActionName == 'checkout_cart_configure') {
                $handler = 'catalog_product_view_sellyours';
            }
            if ($handler) {
                $layout->getUpdate()->addHandle($handler);
            }
        }
    }

    /**
     * @return mixed
     */
    public function isSimpleProduct()
    {
        $product = $this->_coreRegistry->registry('current_product');
        if ($product) {
            if ($product->getTypeId() === 'simple') {
                return true;
            }
        }
        return false;
    }

}
