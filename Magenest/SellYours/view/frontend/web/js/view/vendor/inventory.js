define([
    'jquery',
    'mage/translate',
    'ko',
    'mage/url',
    'uiComponent',
    'Magento_Ui/js/modal/confirm',
    'Magento_Ui/js/modal/alert'
    ], function ($, $t, ko,url, Component, confirm, alert) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Magenest_SellYours/vendor/inventory',
                inventoryItem: ko.observableArray(),
                currencySymbol: "$",
                currentPage: 1,
                currentStatus: -2,
                lastPage: ko.observable(20),
                total: ko.observable(20),
                firstIndex: ko.observable(1),
                lastIndex: ko.observable(20),
                checkedRecord: ko.observableArray(),
                // checkedLength: ko.observable(0),
                storedData: [],
                currentSortField: ko.observable('last_updated'),
                currentSortOrder: ko.observable('DESC'),
                totalSold: 0,
                totalRevenue: 0,
                totalViews: 0,
                preKeyWord: ko.observable(''),
                keyWord: ko.observable(''),
                downloadData: ko.observable(''),
                deleteData: ko.observable(''),
                selectAllFlag: false,
                isAllowedVendorDelete: ko.observable(false),
            },
            initObservable: function () {
                this._super();
                this._super().observe('inventoryItem');
                this._super().observe('currentPage');
                this._super().observe('currentStatus');
                this._super().observe('lastPage');
                this._super().observe('firstIndex');
                this._super().observe('lastIndex');
                this._super().observe('total');
                this._super().observe('checkBox');
                this._super().observe('checkedRecord');
                this._super().observe('currentSortField');
                this._super().observe('currentSortOrder');
                this._super().observe('sort');
                this._super().observe('checkSortOrder');
                this._super().observe('checkedLength');
                this._super().observe('totalSold');
                this._super().observe('totalRevenue');
                this._super().observe('totalViews');
                this._super().observe('keyWord');
                this._super().observe('preKeyWord');
                this._super().observe('defaultCheckbox');
                this._super().observe('tableFoot');
                this._super().observe('isCheckedAll');
                this._super().observe('downloadData');
                this._super().observe('deleteData');
                this._super().observe('isAllowedVendorDelete');
                if (this.isAllowedVendorDelete() === true) {
                    this.isAllowedVendorDelete(true);
                } else {
                    this.isAllowedVendorDelete(false);
                }
                this.tableFoot = ko.computed(function () {
                    var totalSold = 0;
                    var totalRevenue = 0;
                    var totalViews = 0;
                    var totalLength = this.checkedRecord().length;
                    var component = this;
                    var data = {};
                    data['totalSold'] = 0;
                    data['totalRevenue'] = 0;
                    data['totalViews'] = 0;
                    data['totalLength'] = totalLength;
                    $.each(this.storedData[this.currentStatus()], function (idx, item) {
                        if (idx > 0) {
                            $.each(item['items'], function (cidx, citem) {
                                if (component.checkedRecord().indexOf(citem.id) != -1) {
                                    totalSold += parseFloat(citem.sold);
                                    totalRevenue += parseFloat(citem.revenue);
                                    totalViews += parseFloat(citem.views);
                                }
                            });
                        }
                        data['totalSold'] = totalSold;
                        data['totalRevenue'] = totalRevenue;
                        data['totalViews'] = totalViews;
                        data['totalLength'] = totalLength;
                    });
                    return data;
                },this);
                url.setBaseUrl(this.baseUrl);
                this.downloadUrl = url.build('udsell/inventory/downloadCsv');
                this.deleteUrl= url.build('udsell/inventory/massDelete');
                this.changeStatusTab(this.currentStatus(),this.currentPage());
                return this;
            },
            displayedRecord : function () {
                var data = this.inventoryItem();
                return data;
            },
            changeStatusTab : function (status, page, switchTab) {
                var component= this;
                if(component.currentStatus()!=status||this.keyWord()!=this.preKeyWord()){
                    component.checkedRecord([]);
                    component.totalSold(0);
                    component.totalRevenue(0);
                    component.totalViews(0);
                }
                if(switchTab!=false) {
                    component.currentStatus(status);
                    component.currentPage(page);
                }
                var firstLoadFlag = false;
                var loadUrl = url.build("udsell/inventory/load");
                if(this.storedData[status]==undefined){
                    this.storedData[status] = [];
                    firstLoadFlag = true;
                }
                if(this.storedData[status][page]==undefined){
                    this.storedData[status][page]=[];
                    firstLoadFlag = true;
                }
                if(this.keyWord()!=this.preKeyWord()){
                    firstLoadFlag = true;
                }
                if(firstLoadFlag) {
                    var deferred = $.Deferred();
                    $.ajax({
                        method: "POST",
                        url: loadUrl,
                        cache: false,
                        data: {
                            status: status,
                            page: page,
                            sort_field: component.currentSortField,
                            sort_order: component.currentSortOrder,
                            key_word: component.keyWord

                        },
                        success: function (response) {
                                component.storedData[status][page] = response;
                            if(switchTab != false) {
                                component.inventoryItem(component.storedData[status][page]['items']);
                                component.total(response.total);
                                component.lastPage(response.lastPage);
                                component.firstIndex(response.firstIndex);
                                component.lastIndex(response.lastIndex);
                            }else {
                                $.each(component.storedData[status][page]['items'], function (index, value){
                                    if(component.checkedRecord().indexOf(value.id)==-1) {
                                        component.checkedRecord().push(value.id);
                                    }
                                });
                            }
                            deferred.resolve();
                        },
                        error: function (error) {

                        },
                        always: function () {
                            deferred.resolve();
                        },
                        showLoader: true

                    });
                    return deferred;
                }else {
                    if(switchTab != false) {
                        component.inventoryItem(component.storedData[status][page]['items']);
                        component.total(component.storedData[status][page]['total']);
                        component.lastPage(component.storedData[status][page]['lastPage']);
                        component.firstIndex(component.storedData[status][page]['firstIndex']);
                        component.lastIndex(component.storedData[status][page]['lastIndex']);
                    }else {
                        $.each(component.storedData[status][page]['items'], function (index, value){
                            if(component.checkedRecord().indexOf(value.id)==-1) {
                                component.checkedRecord().push(value.id);
                            }
                        });
                    }
                    return 1;
                }
                this.checkedRecord(this.checkedRecord());
            },
            defaultCheckbox: function(data){
                var index = this.checkedRecord().indexOf(data.id);
                if(index==-1){
                    return false;
                }else {
                    return true;
                }
            },
            isCheckedAll: function () {
                if(this.checkedRecord().length!=this.total()){
                    return false;
                }
                if(this.checkedRecord().length==0){
                    return false;
                }
                if(this.checkedRecord().length==this.total()){
                    return true;
                }
            },
            changeCheckBox: function (data, event) {
                var checkbox = $(event.target);
                var index = this.checkedRecord().indexOf(data.id);
                if(checkbox.is(":checked")){
                    if(index==-1) {
                        this.checkedRecord().push(data.id);
                    }
                }else {
                    if(index!=-1){
                        this.checkedRecord().splice(index,1);
                    }
                }
                this.checkedRecord(this.checkedRecord());
            },
            sort: function (sortField, sortOrder) {
                var component= this;
                if(sortField==this.currentSortField()&&sortOrder==this.currentSortOrder()){
                    return;
                }
                if(this.storedData[this.currentStatus()]!= undefined){
                    this.storedData[this.currentStatus()] = [];
                }
                this.currentSortField(sortField);
                this.currentSortOrder(sortOrder);
                this.checkedRecord([]);
                this.changeStatusTab(this.currentStatus(),1);
                this.update();
            },
            downloadCsv: function () {
                var data = {};
                if(this.isCheckedAll()){
                    data['checked_all'] = '1';
                }else {
                    data['checked_all'] = '0';
                    data['checked_id'] = $.extend({},this.checkedRecord());
                }
                data['key_word'] = this.preKeyWord();
                data['status'] = this.currentStatus();
                data['sort_field'] = this.currentSortField();
                data['sort_order'] = this.currentSortOrder();
                var jsonData = JSON.stringify(data);
                this.downloadData(jsonData);
                $("#download-form").submit();
            }
            ,
            checkSortOrder: function (field, sortOrder) {
                return (this.currentSortField()==field&&this.currentSortOrder()==sortOrder)
            },
            searchByKeyWord: function(){
                if(this.keyWord()!=this.preKeyWord()) {
                    this.storedData = [];
                    this.changeStatusTab(this.currentStatus(), 1);
                    this.preKeyWord(this.keyWord());
                }
            },
            loadAll: function (component, event) {
                if($(event.target).is(":checked")) {
                    confirm({
                        content: "It may take a long time to load all the inventory. Are you sure to continue this action ?",
                        actions: {
                            confirm: function () {
                                var diff = [];
                                var checkbox = $(event.target);
                                if (checkbox.is(":checked")) {
                                    for (var i = 1; i <= component.lastPage(); i++) {
                                        diff.push(component.changeStatusTab(component.currentStatus(), i, false));
                                    }
                                }
                                $.when.apply($, diff).always(function () {
                                    component.update();
                                });

                            },
                            cancel: function () {
                                $("#checkAll").removeAttr("checked");
                                return false;
                            }
                        }
                    });
                }else {
                    component.checkedRecord([]);
                }
            }
            ,
            update: function () {
                this.checkedRecord(this.checkedRecord());
            },
            deleteProducts: function () {
                var component = this;
                confirm({
                    content: "Are you sure to delete these products ?",
                    actions: {
                        confirm: function () {
                            var jsonData = JSON.stringify($.extend({},component.checkedRecord()));
                            $.ajax(
                                {
                                    method: "POST",
                                    cache: false,
                                    url: component.deleteUrl,
                                    showLoader:  true,
                                    data: {
                                        vendor_product_ids:jsonData
                                    },
                                    success: function (response) {
                                        if(response.error){
                                            var title = 'Error';
                                        }else {
                                            var title = 'Success'
                                        }
                                        alert({
                                            title: title,
                                            content: response.message,
                                            actions: {
                                                always: function(){
                                                    window.location.reload();
                                                }
                                            }
                                        });
                                    },
                                    error: function (error) {
                                        alert({
                                            title: "Error",
                                            content: "Sorry there are something wrong",
                                            actions: {
                                                always: function(){
                                                    window.location.reload();
                                                }
                                            }
                                        });
                                    },
                                    always: function () {
                                    }
                                }
                            );
                        },
                        cancel: function () {

                        }
                    }
                });
            }
        });
    }
);