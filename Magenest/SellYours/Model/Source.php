<?php
/**
 * Unirgy LLC
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.unirgy.com/LICENSE-M1.txt
 *
 * @category   Unirgy
 * @package    Unirgy_DropshipSplit
 * @copyright  Copyright (c) 2008-2009 Unirgy LLC (http://www.unirgy.com)
 * @license    http:///www.unirgy.com/LICENSE-M1.txt
 */

/**
* Currently not in use
*/
namespace Magenest\SellYours\Model;

use Magento\CatalogInventory\Model\Source\Backorders;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Unirgy\DropshipMulti\Helper\Data as DropshipMultiHelperData;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\Dropship\Model\Source\AbstractSource;

class Source extends AbstractSource
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toOptionHash($selector=false)
    {
        $options = [
            1 => __('1 day'),
            2 => __('2 days'),
            3 => __('3 days'),
            5 => __('5 days'),
            7 => __('7 days'),
            10 => __('10 days'),
        ];
        $options = [''=>__('Please select from the list')] + $options;
        return $options;
    }

}