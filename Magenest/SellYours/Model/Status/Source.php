<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 23/08/2018
 * Time: 10:49
 */

namespace Magenest\SellYours\Model\Status;


class Source
{
    const STATUS_REJECTED       = 0;
    const STATUS_DRAFT          = 9;
    const STATUS_PENDING_REVIEW = -1;
    const STATUS_SCHEDULED      = 3;
    const STATUS_LIVE           = 4;
    const STATUS_ACTIVE         = 5;
    const STATUS_EXPIRED        = 6;
    const STATUS_SOLD_OUT       = 7;
    const STATUS_PAUSED         = 8;
    const STATUS_APPROVED       = 1;

    public function getSatusArray()
    {
        $statusArray = [
            self::STATUS_REJECTED       => __('Rejected'),
            self::STATUS_PENDING_REVIEW => __('Pending Review'),
            self::STATUS_APPROVED       => __('Approved'),
            self::STATUS_SOLD_OUT       => __('Sold Out'),
            self::STATUS_PAUSED         => __('Paused'),
            self::STATUS_DRAFT          => __('Draft'),
            self::STATUS_SCHEDULED      => __('Scheduled'),
            self::STATUS_LIVE           => __('Live'),
            self::STATUS_ACTIVE         => __('Active'),
            self::STATUS_EXPIRED        => __('Expired')
        ];

        return $statusArray;
    }
}
