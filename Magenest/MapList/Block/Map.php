<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 9/14/16
 * Time: 13:31
 */

namespace Magenest\MapList\Block;

class Map extends Block
{
    public function getMap()
    {
        $mapData = new \stdClass();
        $mapData->currentMenu = 'map';
        $mapData->config = $this->getConfig();
        $mapData->map = null;

        //get current store view
        $currentStore = $this->_storeManagerInterface->getStore();
        $currentStoreId = $currentStore->getId();

        $index =0;

        try {
            $LocationModel = $this->_locationFactory->create();
            $LocationData = $LocationModel->getCollection()
                ->addFieldToFilter('is_active', '1')
                ->getData();

            $locationId = array();
            foreach ($LocationData as $key => $locationValue) {
                $posStore = strpos($locationValue['store'], $currentStoreId);
                $allStoreView = strpos($locationValue['store'], "0");
                if ($posStore !== false || $allStoreView !== false) {
                    $locationId[] = $locationValue['location_id'];
                    $LocationData[$key]['small_image_url'] = $this->getImageUrl($locationValue['small_image']);
                    $LocationData[$key]['gallery_image_url'] = $locationValue['gallery'];
                } else {
                    unset($LocationData[$key]);
                }
            }

            foreach ($LocationData as $key => $locationValue) {
                if ($index != $key) {
                    $LocationData[$index] = $locationValue;
                    unset($LocationData[$key]);
                }

                $index++;
            }

            $mapData->location = $LocationData;
        } catch (\Exception $e) {
            $mapData->location = array();
        }

        return $mapData;
    }


    public function getCountry()
    {
        $countrys = $this->_country->toOptionArray();
        $countrys[0] = array(
            'value' => '',
            'label' => 'Select Country'
        );
        return $countrys;
    }
}
