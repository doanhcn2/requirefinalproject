<?php
/**
 * Created by PhpStorm.
 * User: hiennq
 * Date: 9/21/16
 * Time: 08:35
 */

namespace Magenest\MapList\Block;

class Block extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry;

    protected $_scopeConfig;

    protected $_country;

    protected $_locationFactory;

    protected $_storeManagerInterface;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magenest\MapList\Model\LocationFactory $locationFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magento\Store\Model\StoreManager $storeManager
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $registry;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_country = $country;
        $this->_locationFactory = $locationFactory;
        $this->_storeManagerInterface = $storeManager;
    }

    public function getConfig()
    {
        $dataConfig = array();
        $dataConfig['countryCode'] = $this->_scopeConfig->getValue('maplist/map/country');
        $dataConfig['mapApi'] = $this->_scopeConfig->getValue('maplist/map/api');
        $dataConfig['zoom'] = $this->_scopeConfig->getValue('maplist/map/zoom');
        $dataConfig['unit'] = $this->_scopeConfig->getValue('maplist/map/unit');
        $dataConfig['travel_mode'] = $this->_scopeConfig->getValue('maplist/map/travel_mode');
        $dataConfig['max_distance'] = $this->_scopeConfig->getValue('maplist/map/max_distance');
        $dataConfig['default_distance'] = $this->_scopeConfig->getValue('maplist/map/default_distance');

        return $dataConfig;
    }

    protected function getImageUrl($imageData)
    {
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $image = $imageData;
        if (!$image) {
            return $this->getViewFileUrl('Magenest_MapList::images/store-82-200149.png');
        }

        return $mediaDirectory . 'catalog/category/' . $image;
    }

    protected function getGalleryImageUrl($imageData)
    {
        $mediaDirectory = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $image = $imageData;
        if (!$image) {
            return $this->getViewFileUrl('Magenest_MapList::images/storelocator-icon.png');
        }

        return $mediaDirectory . 'catalog/category/' . $image;
    }
}
