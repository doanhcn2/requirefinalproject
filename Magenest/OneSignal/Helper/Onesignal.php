<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/2/18
 * Time: 3:39 PM
 */

namespace Magenest\OneSignal\Helper;


class Onesignal
{
    protected $notifManagement;
    protected $customerSession;

    public function __construct(
        \Magenest\OneSignal\Api\NotificationManagementInterface $notificationManagement,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->notifManagement = $notificationManagement;
        $this->customerSession = $customerSession;
    }

    /**
     * @return int
     */
    public function getUnreadMessagesCount()
    {
        if (!$this->customerSession->isLoggedIn()) {
            return null;
        }
        return $this->notifManagement->countUnread($this->customerSession->getId());
    }
}
