<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 10:26 AM
 */

namespace Magenest\OneSignal\Controller\Webhook;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Receiver extends Action
{
    protected $_logger;
    protected $_httpRequest;
    protected $_notificationRepository;
    protected $_deviceRepository;

    public function __construct(
        Context $context,
        \Magenest\OneSignal\Logger\Logger $logger,
        \Magento\Framework\App\Request\Http $request,
        \Magenest\OneSignal\Api\DeviceRepositoryInterface $deviceRepository,
        \Magenest\OneSignal\Api\NotificationRepositoryInterface $notificationRepository
    ) {
        parent::__construct($context);
        $this->_logger = $logger;
        $this->_httpRequest = $request;
        $this->_deviceRepository = $deviceRepository;
        $this->_notificationRepository = $notificationRepository;
    }

    public function execute()
    {
        try {
            $this->_logger->debug('start');
            $this->_logger->debug($this->_httpRequest->getContent());
            $request = json_decode($this->_httpRequest->getContent(), true);
            if (!is_array($request)
                || !array_key_exists('id', $request)                // no id
                || !array_key_exists('event', $request)          // no event
                || !in_array($request['event'], ['notification.displayed', 'notification.clicked'])
                || !array_key_exists('userId', $request)         // no user
                || empty($request['userId'])                         // no uuid
            ) {
//                $this->_logger->debug(print_r($request, true));
                return;
            }
            if (is_array($request['userId'])) {
                $uuid = $request['userId']['uuid'];
            } else {
                $uuid = $request['userId'];
            }
            $deviceModel = $this->_deviceRepository->get($uuid);
            if (!$deviceModel->getId()) {
                $this->_logger->debug(print_r($deviceModel->getData(), true));
                return;
            }

            $notifModel = $this->_notificationRepository->get($request['id'], $deviceModel->getCustomerId());
            $this->_logger->debug(print_r($notifModel->getData(), true));
//            if (!$notifModel->getId()) {
            unset($request['id']);
            $notifModel->addData($request);
            if ($request['event'] === 'notification.clicked') {
                $notifModel->setStatus(\Magenest\OneSignal\Api\Data\NotificationInterface::STATUS_READ);
            } else {
                $notifModel->setStatus(\Magenest\OneSignal\Api\Data\NotificationInterface::STATUS_UNREAD);
            }
            $notifModel->setData('data', json_encode(@$request['data']));
            $this->_notificationRepository->save($notifModel);
            $this->_logger->debug('saved');
//            }
        } catch (\Exception $e) {
            $this->_logger->debug($e->getMessage());
            $this->_logger->debug($e->getTraceAsString());
        }
    }
}
