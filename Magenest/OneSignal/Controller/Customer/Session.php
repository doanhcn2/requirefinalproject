<?php

namespace Magenest\OneSignal\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Session extends Action
{
    protected $customerSession;

    protected $deviceRepo;

    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magenest\OneSignal\Api\DeviceRepositoryInterface $deviceRepository
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->deviceRepo = $deviceRepository;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Customer\Model\Session $customerSession */
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if (!isset($params['playerId'])) {
            return;
        }
        $customerSession->setPlayerId($params['playerId']);
        if ($customerSession->isLoggedIn()) {
            $device = $this->deviceRepo->get($params['playerId']);
            if (!$device->getId()) {
                $device->setCustomerId($customerSession->getCustomerId());
                $this->deviceRepo->save($device);
            }
        }
        return;
    }

}