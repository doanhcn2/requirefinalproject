<?php

namespace Magenest\OneSignal\Controller\Adminhtml\Rule;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magenest\OneSignal\Model\RuleFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magenest\OneSignal\Controller\Adminhtml\Rule;
use Magento\Framework\File\Uploader;
use Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\DataObject;

class Save extends Rule
{
    protected $_mediaDirectory;
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_uploaderFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter
     * @param \Magenest\OneSignal\Model\RuleFactory $ruleFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Date $dateFilter,
        RuleFactory $ruleFactory,
        LoggerInterface $logger,
        DirectoryList $directoryList,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory
    ) {
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_uploaderFactory = $uploaderFactory;
        parent::__construct($context, $coreRegistry, $fileFactory, $dateFilter, $ruleFactory, $logger);
    }

    /**
     * Rule save action
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->getRequest()->getPostValue()) {
            $this->_redirect('onesignal/*/');
        }

        try {
            /** @var $model \Magenest\OneSignal\Model\Rule */
            $model = $this->ruleFactory->create();
            $this->_eventManager->dispatch(
                'adminhtml_controller_onesignal_prepare_save',
                ['request' => $this->getRequest()]
            );
            $data = $this->getRequest()->getPostValue();
            $id = $this->getRequest()->getParam('rule_id');
            if ($id) {
                $model->load($id);
            }

            $validateResult = $model->validateData(new DataObject($data));
            if ($validateResult !== true) {
                foreach ($validateResult as $errorMessage) {
                    $this->messageManager->addErrorMessage($errorMessage);
                }
                $this->_session->setPageData($data);
                $this->_redirect('onesignal/*/edit', ['id' => $model->getId()]);
                return;
            }

            $this->_session->setPageData($model->getData());
            /**  */
            if((!isset($data['auto_icon'])||$data['auto_icon']==0)&&$data['is_icon_upload']==1)
                $this->uploadImage('icon_upload', $model);
            if((!isset($data['auto_image'])||$data['auto_image']==0)&&$data['is_image_upload']==1)
                $this->uploadImage('image_upload', $model);

            $data = $this->prepareData($data);
            unset($data['icon_upload']);
            unset($data['image_upload']);
            $data['at_time'] = isset($data['at_time']) ? implode(":", $data['at_time']) : null;
            $model->loadPost($data);
            $model->save();
            $this->messageManager->addSuccessMessage(__('You saved the rule.'));
            $this->_session->setPageData(false);
            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('onesignal/*/edit', ['id' => $model->getId()]);
                return;
            }
            $this->_redirect('onesignal/*/');
            $this->_redirect('onesignal/*/edit', ['id' => $model->getId()]);
            return;
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $id = (int)$this->getRequest()->getParam('rule_id');
            if (!empty($id)) {
                $this->_redirect('onesignal/*/edit', ['id' => $id]);
            } else {
                $this->_redirect('onesignal/*/new');
            }
            return;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while saving the rule data. Please review the error log.')
            );
            $this->logger->info('onesignal_save_info', ['save' => $e]);
            $data = !empty($data) ? $data : [];
            $this->_session->setPageData($data);
            $this->_redirect('onesignal/*/edit', ['id' => $this->getRequest()->getParam('rule_id')]);
            return;
        }
    }

    protected function uploadImage($name, $model){
        if (isset($this->getRequest()->getParam($name)['delete'])) {
            $this->setImage('', $name, $model);
        } else {
            try {
                $this->attachUploadedImage($name,$model);
            } catch (\Exception $e) {
                throw new LocalizedException(
                    __('You have not uploaded the image.')
                );
            }
        }
    }

    /**
     * Prepares specific data
     *
     * @param array $data
     * @return array
     */
    protected function prepareData($data)
    {
        if (isset($data['rule']['conditions'])) {
            $data['conditions'] = $data['rule']['conditions'];
        }
        unset($data['rule']);
        return $data;
    }

    /**
     * Attach uploaded image to wrapping
     *
     * @param string $value
     * @param string $imageFieldName
     * @param RuleFactory $model
     * @return $this
     */
    public function setImage($value, $imageFieldName, $model )
    {
        if($value !=''){
            if ($value instanceof Uploader) {
                $value->save($this->_mediaDirectory->getAbsolutePath('onesignal/icon'));
                $value = $value->getUploadedFileName();
            }
            $model->setData($imageFieldName, 'onesignal/icon/'.$value);
        }
        else $model->setData($imageFieldName, '');
        return $this;
    }

    /**
     * Attach uploaded image to wrapping
     *
     * @param string $imageFieldName
     * @param RuleFactory $model
     * @return $this
     */
    public function attachUploadedImage($imageFieldName, $model)
    {
        $isUploaded = true;
        try {
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_uploaderFactory->create(['fileId' => $imageFieldName]);
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowCreateFolders(true);
            $uploader->setFilesDispersion(false);
        } catch (\Exception $e) {
            $isUploaded = false;
        }
        if ($isUploaded) {
            $this->setImage($uploader, $imageFieldName, $model);
        }
        return $this;
    }
}