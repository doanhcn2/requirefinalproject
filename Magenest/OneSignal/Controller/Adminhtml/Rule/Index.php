<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Controller\Adminhtml\Rule;

use Magenest\OneSignal\Controller\Adminhtml\Rule;

class Index extends Rule
{
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $this->_initAction()->_addBreadcrumb(__('OneSignal Rules'), __('OneSignal Rules'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('OneSignal Rules'));
        $this->_view->renderLayout('root');
    }
}