<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Controller\Adminhtml\Rule;

use Magenest\OneSignal\Controller\Adminhtml\Rule;

class Edit extends Rule
{
    /**
     * Rule edit action
     *
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magenest\OneSignal\Model\Rule $model */
        $model = $this->ruleFactory->create();
        $type = $this->getRequest()->getParam('type');
        if ($id) {
            $model->load($id);
            $type = $model->getData('rule_type');
            $model->addData(['rule_type_select'=> $type]);
            if (!$model->getRuleId()) {
                $this->messageManager->addErrorMessage(__('This rule no longer exists.'));
                $this->_redirect('onesignal/*');
                return;
            }
        }
        // set entered data if was error when we do save
        $data = $this->_session->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $model->setTypeRule($type);
        $model->addData(['rule_type_select'=> $type]);
        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');

        $this->coreRegistry->register('current_rule', $model);

        $this->_initAction();
        $this->_view->getLayout()
            ->getBlock('rule_edit')
            ->setData('action', $this->getUrl('onesignal/*/save'))
        ;

        $this->_addBreadcrumb($id ? __('Edit Rule') : __('New Rule'), $id ? __('Edit Rule') : __('New Rule'));

        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            $model->getRuleId() ? $model->getName() : __('New Rule')
        );
        $this->_request->setParams(['type'=>$type]);
        $this->_view->renderLayout();
    }
}