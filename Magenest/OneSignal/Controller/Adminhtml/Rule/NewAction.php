<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Controller\Adminhtml\Rule;

use Magenest\OneSignal\Controller\Adminhtml\Rule;

class NewAction extends Rule
{
    /**
     * New action
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}