<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Logger;

class Logger extends \Monolog\Logger
{
    public function __construct(
        \Magenest\OneSignal\Logger\Handler $handler,
        $name = "OnesignalLogger",
        $handlers = array(),
        $processors = array()
    )
    {
        parent::__construct($name, $handlers, $processors);
//        $this->handlers = ['system' => $handler];
        $this->pushHandler($handler);
    }
}