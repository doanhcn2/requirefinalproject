<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Logger;

use Monolog\Logger as MonoLogger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = MonoLogger::DEBUG;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/magenest/onesignal.log';

    public function __construct(
        \Magento\Framework\Filesystem\Driver\File $filesystem,
        $filePath = null
    )
    {
        parent::__construct($filesystem, $filePath);
    }
}