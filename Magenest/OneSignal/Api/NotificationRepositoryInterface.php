<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Api;

/**
 * Interface NotificationRepositoryInterface
 * @package Magenest\OneSignal\Api
 */
interface NotificationRepositoryInterface
{
    /**
     * @param \Magenest\OneSignal\Api\Data\NotificationInterface $notification
     * @return \Magenest\OneSignal\Api\Data\NotificationInterface
     */
    public function save(\Magenest\OneSignal\Api\Data\NotificationInterface $notification);

    /**
     * @param string $notifId
     * @param string $customerId
     * @return \Magenest\OneSignal\Api\Data\NotificationInterface
     */
    public function get($notifId, $customerId);

    /**
     * @param $customerId
     * @return \Magenest\OneSignal\Api\Data\NotificationInterface[]
     */
    public function getListByCustomer($customerId);

    /**
     * @param int $id
     * @return \Magenest\OneSignal\Api\Data\NotificationInterface
     */
    public function getById($id);
}
