<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/2/18
 * Time: 4:17 PM
 */

namespace Magenest\OneSignal\Api;

/**
 * Interface NotificationManagementInterface
 * @package Magenest\OneSignal\Api
 */
interface NotificationManagementInterface
{
    /**
     * @param $customerId
     * @return int
     */
    public function countUnread($customerId);
}
