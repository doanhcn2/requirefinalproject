<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 1:19 PM
 */

namespace Magenest\OneSignal\Api\Data;

/**
 * Interface NotificationInterface
 * @package Magenest\OneSignal\Api\Data
 */
interface NotificationInterface
{
    const STATUS_UNREAD = 0;
    const STATUS_READ = 1;
}
