<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Api;

/**
 * Interface DeviceRepositoryInterface
 * @package Magenest\OneSignal\Api
 */
interface DeviceRepositoryInterface
{
    /**
     * @param \Magenest\OneSignal\Api\Data\DeviceInterface $device
     * @return \Magenest\OneSignal\Api\Data\DeviceInterface
     */
    public function save(\Magenest\OneSignal\Api\Data\DeviceInterface $device);

    /**
     * @param string $uuid
     * @return \Magenest\OneSignal\Api\Data\DeviceInterface
     */
    public function get($uuid);

    /**
     * @param int $id
     * @return \Magenest\OneSignal\Api\Data\DeviceInterface
     */
    public function getById($id);
}