<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Model;

/**
 * Class NotificationRepository
 * @package Magenest\OneSignal\Model
 */
class NotificationRepository implements \Magenest\OneSignal\Api\NotificationRepositoryInterface
{
    /**
     * @var \Magenest\OneSignal\Model\ResourceModel\Notification
     */
    protected $resourceModel;

    /**
     * @var \Magenest\OneSignal\Model\NotificationFactory
     */
    protected $modelFactory;

    /**
     * @var ResourceModel\ReadStatus\CollectionFactory
     */
    protected $statusCollectionFactory;

    /**
     * @var \Magenest\OneSignal\Logger\Logger
     */
    protected $_logger;

    public function __construct(
        \Magenest\OneSignal\Model\ResourceModel\ReadStatus\CollectionFactory $statusCollectionFactory,
        \Magenest\OneSignal\Model\ResourceModel\Notification $resourceModel,
        \Magenest\OneSignal\Model\NotificationFactory $notificationFactory,
        \Magenest\OneSignal\Logger\Logger $logger
    ) {
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->modelFactory = $notificationFactory;
        $this->resourceModel = $resourceModel;
        $this->_logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magenest\OneSignal\Api\Data\NotificationInterface $notification)
    {
        $this->resourceModel->save($notification);
        $localId = $notification->getId();
        $remoteId = $notification->getNotifId();
        $customerId = $notification->getCustomerId();
        $status = $notification->getStatus();
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $statusModel = $statusCollection->loadByCustomerIdAndNotifId($customerId, $remoteId);
        $statusModel->setLocalNotifId($localId);
        $statusModel->setStatus($status)->save();
        return $notification;
    }

    /**
     * {@inheritdoc}
     */
    public function get($notifId, $customerId)
    {
        $model = $this->modelFactory->create();
        $connection = $this->resourceModel->getConnection();
        $select = $connection->select()
            ->from(['notif' => $connection->getTableName('magenest_onesignal_notifications')])
            ->where('`notif_id` = ?', $notifId)
            ->joinLeft(
                ['status' => $connection->getTableName('magenest_onesignal_notification_read_status')],
                "`notif`.`id` = `status`.`local_notif_id` AND `status`.`customer_id` = $customerId",
                ['customer_id', 'status']
            );
        $this->_logger->debug($select->__toString());
        $data = $connection->fetchAll($select);
        if (isset($data[0])) {
            $model->addData($data[0]);
        }
        $model->setData('notif_id', $notifId);
        $model->setData('customer_id', $customerId);
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getListByCustomer($customerId)
    {
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        return $statusCollection->loadByCustomerId($customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getUnreadListByCustomer($customerId)
    {
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        return $statusCollection->loadUnreadByCustomerId($customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $id);
        return $model;
    }
}
