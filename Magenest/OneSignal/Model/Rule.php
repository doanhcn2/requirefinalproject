<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Model;

use Magento\Quote\Model\Quote\Address;
use Magento\Rule\Model\AbstractModel;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;

/**
 * Class Rule
 * @package Vendor\Rules\Model
 *
 * @method int|null getRuleId()
 * @method Rule setRuleId(int $id)
 */
class Rule extends AbstractModel
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'onesignal';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getRule() in this case
     *
     * @var string
     */
    protected $_eventObject = 'rule';

    /** @var \Magento\SalesRule\Model\Rule\Condition\CombineFactory */
    protected $productCombineFactory;

    /** @var \Magento\SalesRule\Model\Rule\Condition\CombineFactory */
    protected $orderCombineFactory;

    /** @var \Magento\SalesRule\Model\Rule\Condition\Product\CombineFactory */
    protected $condProdCombineF;

    protected $_mediaDirectory;
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_uploaderFactory;

    /**
     * Store already validated addresses and validation results
     *
     * @var array
     */
    protected $validatedAddresses = [];
    protected $salesRuleCombineFactory;
    protected $saleRuleProductCombineFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\SalesRule\Model\Rule\Condition\CombineFactory $condCombineFactory
     * @param \Magento\SalesRule\Model\Rule\Condition\Product\CombineFactory $condProdCombineF
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\CatalogRule\Model\Rule\Condition\CombineFactory $productCombineFactory,
        \Magento\SalesRule\Model\Rule\Condition\CombineFactory $salesRuleCombineFactory,
        \Magento\SalesRule\Model\Rule\Condition\Product\CombineFactory $saleRuleProductCombineFactory,
        \Magenest\OneSignal\Model\Rule\Condition\CombineFactory $orderCombineFactory,
        \Magento\CatalogRule\Model\Rule\Action\CollectionFactory $condProdCombineF,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Magento\Framework\Filesystem $filesystem = null,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory = null,
        array $data = []
    ) {
        $this->productCombineFactory = $productCombineFactory;
        $this->orderCombineFactory = $orderCombineFactory;
        $this->condProdCombineF = $condProdCombineF;
        $this->salesRuleCombineFactory = $salesRuleCombineFactory;
        $this->saleRuleProductCombineFactory = $saleRuleProductCombineFactory;
//        if($filesystem) $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_uploaderFactory = $uploaderFactory;
        parent::__construct($context, $registry, $formFactory, $localeDate, $resource, $resourceCollection, $data);
    }

    /**
     * Set resource model and Id field name
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Magenest\OneSignal\Model\ResourceModel\Rule');
        $this->setIdFieldName('rule_id');
    }

    /**
     * Get rule condition combine model instance
     *
     * @return \Magento\Rule\Model\Condition\Combine
     */
    public function getConditionsInstance()
    {
        $type = $this->getTypeRule();
        if($type == 'product')
            return $this->productCombineFactory->create();
        elseif($type == 'order')
            return $this->orderCombineFactory->create();
        else
            return $this->salesRuleCombineFactory->create();
    }

    /**
     * Get rule condition product combine model instance
     *
     * @return \Magento\SalesRule\Model\Rule\Condition\Product\Combine
     */
    public function getActionsInstance()
    {
        return $this->condProdCombineF->create();
    }

    /**
     * Check cached validation result for specific address
     *
     * @param Address $address
     * @return bool
     */
    public function hasIsValidForAddress($address)
    {
        $addressId = $this->_getAddressId($address);
        return isset($this->validatedAddresses[$addressId]) ? true : false;
    }

    /**
     * Set validation result for specific address to results cache
     *
     * @param Address $address
     * @param bool $validationResult
     * @return $this
     */
    public function setIsValidForAddress($address, $validationResult)
    {
        $addressId = $this->_getAddressId($address);
        $this->validatedAddresses[$addressId] = $validationResult;
        return $this;
    }

    /**
     * Get cached validation result for specific address
     *
     * @param Address $address
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsValidForAddress($address)
    {
        $addressId = $this->_getAddressId($address);
        return isset($this->validatedAddresses[$addressId]) ? $this->validatedAddresses[$addressId] : false;
    }

    /**
     * Return id for address
     *
     * @param Address $address
     * @return string
     */
    private function _getAddressId($address)
    {
        if ($address instanceof Address) {
            return $address->getId();
        }
        return $address;
    }

    public function setTypeRule($type){
        $this->setData('rule_type',$type);
        return $this;
    }

    private function getTypeRule(){
        return $this->getData('rule_type');
    }
    /**
     * Set wrapping image
     *
     * @param string|null|\Magento\MediaStorage\Model\File\Uploader $value
     * @return $this
     */
    public function setImage($value, $imageFieldName)
    {
        //in the current version should be used instance of \Magento\MediaStorage\Model\File\Uploader
        if ($value instanceof \Magento\Framework\File\Uploader) {
            $value->save($this->_mediaDirectory->getAbsolutePath('onesignal/'));
            $value = $value->getUploadedFileName();
        }
        $this->setData($imageFieldName, $value);
        return $this;
    }

    /**
     * Attach uploaded image to wrapping
     *
     * @param string $imageFieldName
     * @param bool $isTemporary
     * @return $this
     */
    public function attachUploadedImage($imageFieldName, $isTemporary = false)
    {
        $isUploaded = true;
        try {
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_uploaderFactory->create(['fileId' => $imageFieldName]);
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowCreateFolders(true);
            $uploader->setFilesDispersion(false);
        } catch (\Exception $e) {
            $isUploaded = false;
        }
        if ($isUploaded) {
            if ($isTemporary) {
//                $this->setTmpImage($uploader);
            } else {
                $this->setImage($uploader, $imageFieldName);
            }
        }
        return $this;
    }

//    /**
//     * Set temporary wrapping image
//     *
//     * @param string|null|\Magento\MediaStorage\Model\File\Uploader $value
//     * @return $this
//     */
//    public function setTmpImage($value)
//    {
//        //in the current version should be used instance of \Magento\MediaStorage\Model\File\Uploader
//        if ($value instanceof \Magento\Framework\File\Uploader) {
//            // Delete previous temporary image if exists
//            $this->unsTmpImage();
//            $value->save($this->_mediaDirectory->getAbsolutePath('onesignal/'));
//            $value = $value->getUploadedFileName();
//        }
//        $this->setData('tmp_image', $value);
//        // Override gift wrapping image name
//        $this->setData('image', $value);
//        return $this;
//    }
//
//    /**
//     * Delete temporary wrapping image
//     *
//     * @return $this
//     */
//    public function unsTmpImage()
//    {
//        if ($this->hasTmpImage()) {
//            $tmpImagePath = 'onesignal/' . $this->getTmpImage();
//            if ($this->_mediaDirectory->isExist($tmpImagePath)) {
//                $this->_mediaDirectory->delete($tmpImagePath);
//            }
//            $this->unsetData('tmp_image');
//        }
//        return $this;
//    }
}