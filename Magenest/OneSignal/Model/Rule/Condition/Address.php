<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Model\Rule\Condition;

use Magento\Rule\Model\Condition\AbstractCondition;
use Magento\Rule\Model\Condition\Context;
use Magento\Directory\Model\Config\Source\Country;
use Magento\Directory\Model\Config\Source\Allregion;
use Magento\Shipping\Model\Config\Source\Allmethods as ShippingAllMethods;
use Magento\Payment\Model\Config\Source\Allmethods as PaymentAllMethods;
use Magento\Sales\Model\Config\Source\Order\Status;
use Magento\Customer\Model\Config\Source\Group;
use Magento\Framework\Model\AbstractModel;

class Address extends AbstractCondition
{
    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_directoryCountry;

    /**
     * @var \Magento\Directory\Model\Config\Source\Allregion
     */
    protected $_directoryAllregion;

    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $_shippingAllmethods;

    /**
     * @var \Magento\Payment\Model\Config\Source\Allmethods
     */
    protected $_paymentAllmethods;

    /**
     * @var \Magento\Sales\Model\Config\Source\Order\Status
     */
    protected $_orderStatus;
    protected $_customerGroup;

    /**
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\Directory\Model\Config\Source\Country $directoryCountry
     * @param \Magento\Directory\Model\Config\Source\Allregion $directoryAllregion
     * @param \Magento\Shipping\Model\Config\Source\Allmethods $shippingAllmethods
     * @param \Magento\Payment\Model\Config\Source\Allmethods $paymentAllmethods
     * @param array $data
     */
    public function __construct(
        Context $context,
        Country $directoryCountry,
        Allregion $directoryAllregion,
        ShippingAllmethods $shippingAllmethods,
        PaymentAllmethods $paymentAllmethods,
        Status $orderStatus,
        Group $customerGroup,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_directoryCountry = $directoryCountry;
        $this->_directoryAllregion = $directoryAllregion;
        $this->_shippingAllmethods = $shippingAllmethods;
        $this->_paymentAllmethods = $paymentAllmethods;
        $this->_orderStatus = $orderStatus;
        $this->_customerGroup = $customerGroup;
    }

    /**
     * Load attribute options
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $attributes = [
            'base_subtotal' => __('Subtotal'),
            'total_qty_count' => __('Total Items Quantity'),
            'weight' => __('Total Weight'),
            'shipping_method' => __('Shipping Method'),
            'shipping_postcode' => __('Shipping Postcode'),
            'shipping_region' => __('Shipping Region'),
            'shipping_region_id' => __('Shipping State/Province'),
            'shipping_country_id' => __('Shipping Country'),
            'billing_postcode' => __('Billing Postcode'),
            'billing_region' => __('Billing Region'),
            'billing_region_id' => __('Billing State/Province'),
            'billing_country_id' => __('Billing Country'),
            'status' => __('Order Status'),
            'tax_amount' => __('Tax Amount'),
            'is_virtual' => __('Is Virtual'),
            'customer_is_guest' => __('Customer is Guest'),
            'customer_group_id' => __('Customer Group'),
            'still_pending' => __('Still Pending'),
            'is_unshipped' => __('UnShipped')
        ];

        $this->setAttributeOption($attributes);

        return $this;
    }

    /**
     * Get attribute element
     *
     * @return $this
     */
    public function getAttributeElement()
    {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    /**
     * Get input type
     *
     * @return string
     */
    public function getInputType()
    {
        switch ($this->getAttribute()) {
            case 'base_subtotal':
            case 'weight':
            case 'total_qty_count':
            case 'still_pending':
            case 'tax_amount':
                return 'numeric';

            case 'shipping_method':
            case 'payment_method':
            case 'shipping_country_id':
            case 'shipping_region_id':
            case 'billing_country_id':
            case 'billing_region_id':
            case 'status':
            case 'is_virtual':
            case 'customer_is_guest':
            case 'customer_group_id':
            case 'is_unshipped':
                return 'select';
        }
        return 'string';
    }

    /**
     * Get value element type
     *
     * @return string
     */
    public function getValueElementType()
    {
        switch ($this->getAttribute()) {
            case 'shipping_method':
            case 'payment_method':
            case 'shipping_country_id':
            case 'shipping_region_id':
            case 'billing_country_id':
            case 'billing_region_id':
            case 'status':
            case 'is_virtual':
            case 'is_unshipped':
            case 'customer_is_guest':
            case 'customer_group_id':
                return 'select';
        }
        return 'text';
    }

    /**
     * Get value select options
     *
     * @return array|mixed
     */
    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            switch ($this->getAttribute()) {
                case 'shipping_country_id':
                    $options = $this->_directoryCountry->toOptionArray();
                    break;

                case 'shipping_region_id':
                    $options = $this->_directoryAllregion->toOptionArray();
                    break;
                case 'billing_country_id':
                    $options = $this->_directoryCountry->toOptionArray();
                    break;

                case 'billing_region_id':
                    $options = $this->_directoryAllregion->toOptionArray();
                    break;

                case 'shipping_method':
                    $options = $this->_shippingAllmethods->toOptionArray();
                    break;

                case 'payment_method':
                    $options = $this->_paymentAllmethods->toOptionArray();
                    break;

                case 'status':
                    $options = $this->_orderStatus->toOptionArray();
                    break;
                case 'is_virtual':
                    $options = [['value' => '1', 'label' => __('True')], ['value' => '0', 'label' => __('False')]];
                    break;
                case 'is_unshipped':
                    $options = [['value' => '1', 'label' => __('True')], ['value' => '0', 'label' => __('False')]];
                    break;
                case 'customer_is_guest':
                    $options = [['value' => '1', 'label' => __('True')], ['value' => '0', 'label' => __('False')]];
                    break;
                case 'customer_group_id':
                    $options = $this->_customerGroup->toOptionArray();
                    break;
                default:
                    $options = [];
            }
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

    /**
     * Validate Address Rule Condition
     *
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(AbstractModel $model)
    {
        $address = $model;
        if (!$address instanceof \Magento\Quote\Model\Quote\Address) {
            if ($model->getQuote()->isVirtual()) {
                $address = $model->getQuote()->getBillingAddress();
            } else {
                $address = $model->getQuote()->getShippingAddress();
            }
        }

        if ('payment_method' == $this->getAttribute() && !$address->hasPaymentMethod()) {
            $address->setPaymentMethod($model->getQuote()->getPayment()->getMethod());
        }

        return parent::validate($address);
    }
}
