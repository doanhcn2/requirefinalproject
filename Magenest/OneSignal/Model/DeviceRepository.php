<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Model;

/**
 * Class DeviceRepository
 * @package Magenest\OneSignal\Model
 */
class DeviceRepository implements \Magenest\OneSignal\Api\DeviceRepositoryInterface
{
    /**
     * @var \Magenest\OneSignal\Model\ResourceModel\Device
     */
    protected $resourceModel;

    /**
     * @var \Magenest\OneSignal\Model\DeviceFactory
     */
    protected $modelFactory;

    public function __construct(
        \Magenest\OneSignal\Model\ResourceModel\Device $resourceModel,
        \Magenest\OneSignal\Model\DeviceFactory $DeviceFactory
    ) {
        $this->modelFactory = $DeviceFactory;
        $this->resourceModel = $resourceModel;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magenest\OneSignal\Api\Data\DeviceInterface $device)
    {
        $this->resourceModel->save($device);
        return $device;
    }

    /**
     * {@inheritdoc}
     */
    public function get($uuid)
    {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $uuid, 'device_id');
        if (!$model->getId()) {
            $model->setDeviceId($uuid);
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $id);
        return $model;
    }
}