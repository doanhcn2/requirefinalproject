<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 9:27 AM
 */

namespace Magenest\OneSignal\Model;

use \Magento\Framework\Model\AbstractModel;

/**
 * Class Notification
 * @package Magenest\OneSignal\Model
 */
class Notification extends AbstractModel implements \Magenest\OneSignal\Api\Data\NotificationInterface
{
    protected $_eventPrefix = 'onesignal_notification_';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\OneSignal\Model\ResourceModel\Notification $resource,
        \Magenest\OneSignal\Model\ResourceModel\Notification\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
