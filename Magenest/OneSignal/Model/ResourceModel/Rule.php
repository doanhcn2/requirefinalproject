<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Model\ResourceModel;

use Magento\Rule\Model\ResourceModel\AbstractResource;

class Rule extends AbstractResource
{

    /**
     * Initialize main table and table id field
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_onesignal_rules', 'rule_id');
    }
}