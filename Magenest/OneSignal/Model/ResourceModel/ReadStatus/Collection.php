<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 9:32 AM
 */

namespace Magenest\OneSignal\Model\ResourceModel\ReadStatus;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            \Magenest\OneSignal\Model\ReadStatus::class,
            \Magenest\OneSignal\Model\ResourceModel\ReadStatus::class
        );
    }

    public function loadByCustomerIdAndNotifId($customerId, $notifId, $isLocalId = false)
    {
        $item = $this->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter($isLocalId ? 'local_notif_id' : 'remote_notif_id', $notifId)
            ->getFirstItem();
        if (!$item->getId()) {
            $item->setData('customer_id', $customerId);
            $item->setData($isLocalId ? 'local_notif_id' : 'remote_notif_id', $notifId);
        }
        return $item;
    }

    public function loadByCustomerId($customerId)
    {
        $notifTable = $this->getTable('magenest_onesignal_notifications');
        return $this->addFieldToFilter('customer_id', $customerId)
            ->join(
                $notifTable,
                "`main_table`.`local_notif_id`=`$notifTable`.`id`",
                ['heading','content','url','icon','badge','image']
            );
    }

    public function loadUnreadByCustomerId($customerId)
    {
        $notifTable = $this->getTable('magenest_onesignal_notifications');
        return $this->addFieldToFilter('customer_id', $customerId)->addFieldToFilter('status', 0)
            ->join(
                $notifTable,
                "`main_table`.`local_notif_id`=`$notifTable`.`id`",
                ['heading','content','url','icon','badge','image']
            );
    }
    public function getTop50NotificationByCustomerId($customerId,$size){
        $size = isset($size)?$size:50;
        $notifTable = $this->getTable('magenest_onesignal_notifications');
        return $this->addFieldToFilter('customer_id', $customerId)
            ->join(
                $notifTable,
                "`main_table`.`local_notif_id`=`$notifTable`.`id`",
                ['id','notif_id','heading','content','url','icon','badge','image','priority','data','created_at']
            )->setPageSize($size)
            ->setOrder("$notifTable.`id`",'DESC')
            ->getItems();
    }
    public function getStatusNotificationForCustomer($customerId, $notifId, $isLocalId){
        $item = $this->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter($isLocalId ? 'local_notif_id' : 'remote_notif_id', $notifId)
            ->getFirstItem();
        return $item;
    }
}
