<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 9:32 AM
 */

namespace Magenest\OneSignal\Model\ResourceModel\Notification;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            \Magenest\OneSignal\Model\Notification::class,
            \Magenest\OneSignal\Model\ResourceModel\Notification::class
        );
    }

}