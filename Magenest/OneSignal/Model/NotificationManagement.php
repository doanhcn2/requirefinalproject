<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/2/18
 * Time: 4:19 PM
 */

namespace Magenest\OneSignal\Model;

use Magenest\OneSignal\Api\Data\NotificationInterface;

class NotificationManagement implements \Magenest\OneSignal\Api\NotificationManagementInterface
{
    protected $statusCollectionFactory;

    public function __construct(
        \Magenest\OneSignal\Model\ResourceModel\ReadStatus\CollectionFactory $statusCollectionFactory
    ) {
        $this->statusCollectionFactory = $statusCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function countUnread($customerId)
    {
        /** @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        return $statusCollection->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('status', NotificationInterface::STATUS_UNREAD)
            ->count();
    }
}
