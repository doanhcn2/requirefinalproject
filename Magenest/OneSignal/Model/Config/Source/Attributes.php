<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Model\Config\Source;

use Magento\Config\Model\Config\CommentInterface;
use Magento\Store\Model\Store;
use Magento\Eav\Model\Entity\Type;
use Magento\Backend\Block\Dashboard ;

class Attributes implements CommentInterface
{
    protected $_entityType;
    protected $_store;
    protected $block;

    public function __construct(
        Store $store,
        Type $entityType,
        Dashboard $resultRedirectFactory
    ) {
        $this->_store = $store;
        $this->_entityType = $entityType;
        $this->block = $resultRedirectFactory;
    }

    public function getCommentText($elementValue)
    {
        return 'Admin has been allowed to create more than one rule <a href="'.$this->block->getUrl('onesignal/rule/index').'">here</a>';
    }

}