<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 3/19/18
 * Time: 9:27 AM
 */

namespace Magenest\OneSignal\Model;

use \Magento\Framework\Model\AbstractModel;

class ReadStatus extends AbstractModel
{
    const UNREAD_STATUS = 0;
    const READ_STATUS = 1;

    protected $_eventPrefix = 'onesignal_device_';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\OneSignal\Model\ResourceModel\ReadStatus $resource,
        \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}