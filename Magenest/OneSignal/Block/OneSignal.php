<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Block;

use Magento\Catalog\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class OneSignal
 * @package Magenest\OneSignal\Block
 */
class OneSignal extends Template
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * OneSignal constructor.
     * @param Context $context
     * @param Data $catalogData
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $catalogData,
        ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getAppApi()
    {
        $value = $this->scopeConfig->getValue("onesignal/account_setting/app_id", ScopeInterface::SCOPE_STORES);

        return json_encode($value);
    }

    public function getWebhookReceiver()
    {
        return $this->getUrl('onesignal/webhook/receiver') . '?XDEBUG_SESSION_START=PHPSTORM';
        return 'https://webhook.site/27e16cd7-92c9-45b0-9fc5-9b9873cf001d';
    }
}
