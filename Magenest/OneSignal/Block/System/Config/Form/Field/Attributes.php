<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class Attributes extends Field
{

    public function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('Magento_CatalogRule::promo/fieldset.phtml');
        }

        return $this;
    }
    protected function _getElementHtml(AbstractElement $element){
        return $this->_toHtml();
    }
}