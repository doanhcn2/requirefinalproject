<?php
/**
 * Copyright (c) 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created by PhpStorm.
 * User: chung
 * Date: 4/2/18
 * Time: 5:11 PM
 */

namespace Magenest\OneSignal\Block\Notification;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class History extends Template
{
    protected $_customerSession;

    protected $notifRepo;

    /**
     * @var \Magenest\OneSignal\Model\ResourceModel\ReadStatus\Collection
     */
    protected $collection;

    public function __construct(
        Template\Context $context,
        CustomerSession $customerSession,
        \Magenest\OneSignal\Api\NotificationRepositoryInterface $notificationRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_customerSession = $customerSession;
        $this->notifRepo = $notificationRepository;
    }

    public function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Notifications'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'onesignal.notif.history.pager'
            )->setCollection(
                $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }

        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCollection()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            throw new LocalizedException(__("Customer not logged in"));
        }
        if (!$this->collection) {
            $this->collection = $this->notifRepo->getListByCustomer($customerId);
        }
        return $this->collection;
    }
}
