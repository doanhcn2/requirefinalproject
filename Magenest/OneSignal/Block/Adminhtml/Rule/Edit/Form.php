<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Block\Adminhtml\Rule\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Rule\Block\Conditions;
use Magento\Backend\Block\Widget\Form\Renderer\Fieldset;
use Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic
{
    /**
     * Core registry
     *
     * @var \Magento\Backend\Block\Widget\Form\Renderer\Fieldset
     */
    protected $rendererFieldset;

    /**
     * @var \Magento\Rule\Block\Conditions
     */
    protected $conditions;
    protected $categoryRule;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Rule\Block\Conditions $conditions
     * @param \Magento\Backend\Block\Widget\Form\Renderer\Fieldset $rendererFieldset
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Conditions $conditions,
        Fieldset $rendererFieldset,
        array $data = []
    )
    {
        $this->rendererFieldset = $rendererFieldset;
        $this->conditions = $conditions;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('rule_form');
        $this->setTitle(__('Rule Information'));

    }

    /**
     * Prepare form before rendering HTML
     *
     * @return \Magento\Backend\Block\Widget\Form\Generic
     */
    protected function _prepareForm()
    {
        $type = $this->getRequest()->getParam('type');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'enctype' => 'multipart/form-data',
                    'action' => $this->getUrl('onesignal/rule/save'),
                    'method' => 'post',
                ],
            ]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('rule_');
        $this->addMainFields($form);
        $this->addActionFields($form,$type);

        $this->setForm($form);
        if (isset($type)) {
            $form->addValues(['rule_type' => $type]);
            $form->addValues(['rule_type_select' => $type]);
        }
        if($type != 'news')
            $this->addConditionFields($form, $type);
        return parent::_prepareForm();
    }

    function addMainFields($form)
    {
        $model = $this->_coreRegistry->registry('current_rule');
        $fieldset = $form->addFieldset('base_fieldset', [
            'legend' => __('General Information'),
            'class' => 'fieldset-wide',
            'collapsible' => true,
        ]);

        if ($model->getId()) {
            $fieldset->addField('rule_id', 'hidden', ['name' => 'rule_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Rule Name'), 'title' => __('Rule Name'), 'required' => true]
        );
        $fieldset->addField(
            'rule_type_select',
            'select',
            ['name' => 'rule_type_select', 'label' => __('Rule Type'), 'title' => __('Rule Name'), 'required' => true,
                'disabled' => 'disabled',
                'options' => [
                    'product' => __('Product'), 'order' => __('Order'), 'news' => __('News'), 'abandoned' => 'Abandoned Cart'
                ]
            ]
        );
        $fieldset->addField(
            'rule_type',
            'hidden',
            ['name' => 'rule_type', 'label' => __('Rule Type'), 'title' => __('Rule Name'), 'required' => true]
        );
        $fieldset->addField(
            'description',
            'text',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description')
            ]
        );
        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Active'), '0' => __('Inactive')]
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $fieldset->addField('sort_order', 'text', ['name' => 'sort_order', 'label' => __('Priority')]);
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $fieldset->addField(
            'from_date',
            'date',
            [
                'name' => 'from_date',
                'label' => __('From'),
                'title' => __('From'),
                'input_format' => \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
                'date_format' => $dateFormat
            ]
        );
        $fieldset->addField(
            'to_date',
            'date',
            [
                'name' => 'to_date',
                'label' => __('To'),
                'title' => __('To'),
                'input_format' => \Magento\Framework\Stdlib\DateTime::DATE_INTERNAL_FORMAT,
                'date_format' => $dateFormat
            ]
        );
        $fieldset->addField(
            'at_time',
            'time',
            [
                'name' => 'at_time',
                'label' => __('At'),
                'title' => __('At'),
                'time_format' => 'hh:mm:ss'
            ]
        );
        $fieldset->addField(
            'time_to_abandoned',
            'text',
            [
                'name' => 'time_to_abandoned',
                'label' => __('Time To Abandoned (seconds)'),
                'title' => __('Time To Abandoned (seconds)')
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

    function addActionFields($form, $type)
    {
        $model = $this->_coreRegistry->registry('current_rule');
        $htmlIdPrefix = $form->getHtmlIdPrefix();
        $fieldset = $form->addFieldset('action_fieldset', [
            'legend' => __('Action'),
            'class' => 'fieldset-wide',
            'expanded' => false, // closed
        ]);

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('Title'),
                'note' => __('You can use this short codes '.$this->getShortCode($type))
            ]
        );
        $fieldset->addField(
            'message',
            'textarea',
            [
                'name' => 'message',
                'label' => __('Message'),
                'title' => __('Message'),
                'note' => __('You can use this short codes '.$this->getShortCode($type))
            ]
        );
        $fieldset->addField(
            'auto_icon',
            'select',
            [
                'name' => 'auto_icon',
                'label' => __('Auto Icon'),
                'title' => __('Auto Icon'),
                'options' => ['1' => __('Yes'), '0' => __('No')],
                'note' => __('Auto use product\'s thumbnail as notification\'s icon')
            ]
        );
        $fieldset->addField(
            'is_icon_upload',
            'select',
            [
                'name' => 'is_icon_upload',
                'label' => __('Use icon upload'),
                'title' => __('Use icon upload'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );
        $fieldset->addField(
            'icon_upload',
            'image',
            [
                'name' => 'icon_upload',
                'label' => __('Icon Upload'),
                'title' => __('Icon Upload'),
                'note' => 'Allow image type: jpg, jpeg, png',
                'renderer' => 'Magenest\OneSignal\Block\Adminhtml\Renderer\LogoImage',
            ]
        );
        $fieldset->addField(
            'icon_url',
            'text',
            [
                'name' => 'icon_url',
                'label' => __('Icon Url'),
                'title' => __('Icon Url'),
                'note' => 'Allow https only'
            ]
        );
        $fieldset->addField(
            'auto_image',
            'select',
            [
                'name' => 'auto_image',
                'label' => __('Auto Image'),
                'title' => __('Auto Image'),
                'options' => ['1' => __('Yes'), '0' => __('No')],
                'note' => __('Auto use product\'s image as notification\'s image')
            ]
        );
        $fieldset->addField(
            'is_image_upload',
            'select',
            [
                'name' => 'is_image_upload',
                'label' => __('Use image upload'),
                'title' => __('Use image upload'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );
        $fieldset->addField(
            'image_upload',
            'image',
            [
                'name' => 'image_upload',
                'label' => __('Image Upload'),
                'title' => __('Image Upload'),
                'note' => 'Allow image type: jpg, jpeg, png'
            ]
        );
        $fieldset->addField(
            'image_url',
            'text',
            [
                'name' => 'image_url',
                'label' => __('Image Url'),
                'title' => __('Image Url'),
                'note' => 'Allow https only'
            ]
        );
        $fieldset->addField(
            'launch_url',
            'text',
            [
                'name' => 'launch_url',
                'label' => __('Launch Url'),
                'title' => __('Launch Url'),
                'note' => 'Opens a web browser to the URL when the user taps on the notification.'
            ]
        );
        $form->setValues($model->getData());

        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                'Magento\Backend\Block\Widget\Form\Element\Dependence'
            )->addFieldMap(
                "{$htmlIdPrefix}rule_type",
                'rule_type'
            )->addFieldMap(
                "{$htmlIdPrefix}at_time",
                'at_time'
            )->addFieldMap(
                "{$htmlIdPrefix}launch_url",
                'launch_url'
            )->addFieldMap(
                "{$htmlIdPrefix}time_to_abandoned",
                'time_to_abandoned'
            )->addFieldMap(
                "{$htmlIdPrefix}auto_icon",
                'auto_icon'
            )->addFieldMap(
                "{$htmlIdPrefix}icon_upload",
                'icon_upload'
            )->addFieldMap(
                "{$htmlIdPrefix}icon_url",
                'icon_url'
            )->addFieldMap(
                "{$htmlIdPrefix}is_icon_upload",
                'is_icon_upload'
            )->addFieldMap(
                "{$htmlIdPrefix}auto_image",
                'auto_image'
            )->addFieldMap(
                "{$htmlIdPrefix}is_image_upload",
                'is_image_upload'
            )->addFieldMap(
                "{$htmlIdPrefix}image_upload",
                'image_upload'
            )->addFieldMap(
                "{$htmlIdPrefix}image_url",
                'image_url'
            )->addFieldDependence(
                "auto_icon",
                'rule_type',
                'product'
            )->addFieldDependence(
                "launch_url",
                'rule_type',
                'news'
            )->addFieldDependence(
                "at_time",
                'rule_type',
                'news'
            )->addFieldDependence(
                "auto_image",
                'rule_type',
                'product'
            )->addFieldDependence(
                "time_to_abandoned",
                'rule_type',
                'abandoned'
            )->addFieldDependence(
                'is_icon_upload',
                'auto_icon',
                '0'
            )->addFieldDependence(
                'icon_upload',
                'is_icon_upload',
                '1'
            )->addFieldDependence(
                'icon_url',
                'is_icon_upload',
                '0'
            )->addFieldDependence(
                'icon_upload',
                'auto_icon',
                '0'
            )->addFieldDependence(
                'icon_url',
                'auto_icon',
                '0'
            )->addFieldDependence(
                'is_image_upload',
                'auto_image',
                '0'
            )->addFieldDependence(
                'image_upload',
                'auto_image',
                '0'
            )->addFieldDependence(
                'image_url',
                'auto_image',
                '0'
            )->addFieldDependence(
                'image_upload',
                'is_image_upload',
                '1'
            )->addFieldDependence(
                'image_url',
                'is_image_upload',
                '0'
            )
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }

    function addConditionFields($form, $type)
    {
        $model = $this->_coreRegistry->registry('current_rule');
        $renderer = $this->rendererFieldset->setTemplate(
            'Magento_CatalogRule::promo/fieldset.phtml'
        )->setNewChildUrl(
            $this->getUrl('onesignal/rule/newConditionHtml/form/rule_conditions_fieldset')
        );

        $fieldset = $form->addFieldset(
            'conditions_fieldset',
            [
                'legend' => __(
                    'Apply the rule only if the following conditions are met (leave blank for all new ' . $type . ').'
                )

            ]
        )->setRenderer(
            $renderer
        );

        $fieldset->addField(
            'conditions',
            'text',
            ['name' => 'conditions', 'label' => __('Conditions'), 'title' => __('Conditions')]
        )
            ->setRule($model)
            ->setRenderer(
                $this->conditions
            );

        $form->setValues($model->getData());

        $this->setForm($form);

        return parent::_prepareForm();
    }
    private function getShortCode($type){
        if($type == 'product')
            return '{{product_name}}, {{product_sku}}, {{product_price}}, {{product_short_description}}, {{product_description}}';
        elseif($type == 'order')
            return '{{order_id}}, {{order_url}}, {{order_total}}';
        elseif($type == 'abandoned')
            return '{{customer_email}}, {{customer_firstname}}, {{customer_lastname}}, {{customer_middlename}}, {{grand_total}}, {{created_at}}';
        elseif($type == 'news')
            return '';
        else
            return '';
    }

}