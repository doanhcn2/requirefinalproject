<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;
use Magento\Backend\Block\Widget\Button\SplitButton;


class Rule extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {

    }
    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'add_new_rule',
            'label' => __('Add New Rule'),
            'class' => 'add',
            'button_class' => '',
            'class_name' => SplitButton::class,
            'options' => $this->_getAddProductButtonOptions(),
        ];
        $this->buttonList->add('add_new', $addButtonProps);

        return parent::_prepareLayout();
    }
    protected function _getAddProductButtonOptions()
    {
        $splitButtonOptions = [];
        $types = [
            [
                'type' => 'product',
                'label' => 'Product'
            ],
            [
                'type' => 'order',
                'label' => 'Order'
            ],
            [
                'type' => 'news',
                'label' => 'News'
            ],
            [
                'type' => 'abandoned',
                'label' => 'Abandoned Cart'
            ]
        ];

        foreach ($types as $typeId => $type) {
            $splitButtonOptions[$typeId] = [
                'label' => __($type['label']),
                'onclick' => "setLocation('" . $this->_getProductCreateUrl($type['type']) . "')",
                'default' => 'product',
            ];
        }

        return $splitButtonOptions;
    }
    protected function _getProductCreateUrl($type)
    {
        return $this->getUrl(
            'onesignal/*/new',
            ['type' => $type]
        );
    }
}