<?php

namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Onepage\Success;

class PlaceOrderAfter implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $success;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Success $success
    )
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->success = $success;
    }
    public function execute(Observer $observer)
    {
        /** @var  \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        $this->sendMessage($order);
        return $this;
    }
    function sendMessage($order){
        $response = '';
        $enable_product = $this->scopeConfig->getValue("onesignal/advance_setting/order_notification",  'stores');
        $new_order_content = $this->scopeConfig->getValue("onesignal/advance_setting/order_notification_content",  'stores');
        $new_order_content = str_replace(["{{order_id}}","{{order_total}}"],[$order->getIncrementId(), $order->getGrandTotal()],$new_order_content);
        if($enable_product){
            $content = array(
                "en" => $new_order_content
            );

            $fields = array(
                'app_id' => $this->scopeConfig->getValue("onesignal/account_setting/app_id",  'stores'),
                'included_segments' => array('Active Users'),
                'data' => array("foo" => "bar"),
                'contents' => $content,
                'url' => $this->success->getUrl('sales/order/view/', ['order_id' => $order->getIncrementId(), '_secure' => true])
            );

            $fields = json_encode($fields);
            $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api",  'stores');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                'Authorization: Basic '.$onesignal_rest_api));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);
        }

        return $response;
    }


}