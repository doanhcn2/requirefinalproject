<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Onepage\Success;
use Magenest\OneSignal\Model\RuleFactory;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface as Logger;

class SaveOrderAfter implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $success;
    protected $ruleFactory;
    protected $order;
    protected $_logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Success $success,
        RuleFactory $ruleFactory,
        Order $order,
        Logger $logger
    )
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->success = $success;
        $this->ruleFactory = $ruleFactory;
        $this->order = $order;
        $this->_logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $order_notification = $this->scopeConfig->getValue("onesignal/advance_setting/order_notification", 'stores');
        if(!$order_notification){
            return $this;
        }
        else{

        $ruleCollection = $this->ruleFactory->create();
        $now = new \DateTime();
        $ruleItems = $ruleCollection->getCollection()
            ->addFieldToFilter('rule_type', 'order')
            ->addFieldToFilter('is_active', '1')
            ->addFieldToFilter('from_date', [
                ['lteq' => $now->format('Y-m-d')],
                ['from_date' ,  'null'=>'']
            ])->addFieldToFilter('to_date', [
                ['gteq' => $now->format('Y-m-d')],
                ['from_date' ,  'null'=>'']
            ])->setOrder('sort_order','DESC' )->load();
            /** @var  \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();
        $send_one_message = $this->scopeConfig->getValue("onesignal/advance_setting/order_notification_once", 'stores');
        foreach ($ruleItems as $ruleItem) {
            $result = $this->checkRule($order, $ruleItem);
            if ($result['result']) {
                if(isset($result['still_pending'])&&$order->getOnesignalNotificationId() == null){
                    if($result['still_pending'] == '') $result['still_pending'] = 0;
                    $date = $now->modify('+'.$result['still_pending'].' day')->format('Y-m-d h:i:s \G\M\T O');
                    $response = json_decode($this->sendMessage($order, $ruleItem, $date),true);
                    $order->setData('onesignal_notification_id',$response['id']);
                    $order->save();
                }
                elseif(isset($result['still_pending'])&&$order->getOnesignalNotificationId() != null && $order->getStatus()!='pending'){
                    $this->removeMessage($order->getOnesignalNotificationId());
                }
                else $this->sendMessage($order, $ruleItem);
                if ($send_one_message) {
                    break;
                }
            }
        }
        return $this;
        }

    }

    protected function checkRule($order, $rule)
    {
        /**
         * @var $order \Magento\Sales\Model\Order
         */
        $return = [];
        $result = true;
        /** @var \Magenest\OneSignal\Model\Rule $rule */
        $conditions = json_decode($rule->getConditionsSerialized(), true);
        if(isset($conditions)){
            $aggregator = $conditions['aggregator'];
            $result = $conditions['value'] == 0 ? false : true;
            if(isset($conditions['conditions'])) {
                foreach ($conditions['conditions'] as $condition) {
                    $operator = $condition['operator']; // ==
                    $value = $condition['value']; //VN
                    $attribute = $condition['attribute'];
                    if (!isset($attribute)) {
                        $result = false;
                        break;
                    } elseif ($attribute == 'still_pending'){
                        $return['still_pending'] = $value;
                        continue;
                    }elseif (strpos($attribute, 'shipping') !== false) {
                        if ($order->getShippingAddress()) {
                            $a = $order->getShippingAddress()->getData(str_replace('shipping_', '', $attribute));
                            $result = ($result and $this->operator($a, $value, $operator));
                        } else {
                            $result = false;
                            break;
                        }
                    } elseif (strpos($attribute, 'billing') !== false) {
                        if ($order->getShippingAddress()) {
                            $a = $order->getBillingAddress()->getData(str_replace('billing_', '', $attribute));
                        } else {
                            $result = false;
                            break;
                        }
                    }
                    elseif($attribute == 'is_unshipped'){
                        $a = $this->checkUnshipped($order);
//                        continue;
                    }
                    else {
                        $a = $order->getData($attribute);
                    }
                    if ($aggregator == 'all')
                        $result = ($result and $this->operator($a, $value, $operator));
                    else $result = ($result or $this->operator($a, $value, $operator));
                }
            }
            $result = $conditions['value'] == 0 ? !$result : $result;
        }
        $return['result'] = $result;
        return $return;
    }

    protected function operator($order, $value, $operator)
    {
        switch ($operator) {
            case '==':
                if ($order == $value)
                    return true;
                else return false;
                break;
            case '!=':
                if ($order != $value)
                    return true;
                else return false;
                break;
            case '>=':
                if ($order >= $value)
                    return true;
                else return false;
                break;
            case '<=':
                if ($order <= $value)
                    return true;
                else return false;
                break;
            case '>':
                if ($order > $value)
                    return true;
                else return false;
                break;
            case '<':
                if ($order < $value)
                    return true;
                else return false;
                break;
            case '{}':
                if (strpos($order, $value) !== false)
                    return true;
                else return false;
                break;
            case '!{}':
                if (strpos($order, $value) == false)
                    return true;
                else return false;
                break;
            case '()':
                if (strpos($value, $order) !== false)
                    return true;
                else return false;
                break;
            case '!()':
                if (strpos($value, $order) == false)
                    return true;
                else return false;
                break;
            default:
                return true;
        }
    }

    protected function sendMessage($order, $ruleItem, $still_pending = null)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $message = str_replace([
            "{{order_id}}", "{{order_total}}"
        ], [
            $order->getIncrementId(), $order->getGrandTotal()
        ], $ruleItem->getMessage());
        $title = str_replace([
            "{{order_id}}", "{{order_total}}"
        ], [
            $order->getIncrementId(), $order->getGrandTotal()
        ], $ruleItem->getTitle());
        $content = array(
            "en" => $message
        );
        $heading = array(
            "en" => $title
        );
        $icon = $this->getImageUrl($ruleItem,'icon');
        $image = $this->getImageUrl($ruleItem,'image');
        $include_player_ids = $order->getOnesignalPlayerId() ? [$order->getOnesignalPlayerId()] : null;
        $fields = array(
            'app_id' => $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores'),
            'included_segments' => array('Active Users'),
            'include_player_ids' => $include_player_ids,
            'contents' => $content,
            'headings' => $heading,
            'send_after' => $still_pending,
            'chrome_web_icon' => $icon,
            'chrome_web_image' => $image,
            'url' => $this->storeManager->getStore()->getBaseUrl().'sales/order/view/order_id/'.$order->getEntityId()
        );
        if(!isset($still_pending)){
            unset($fields['send_after']);
        }
        if(!isset($include_player_ids)){
            unset($fields['include_player_ids']);
        }
        $fields = json_encode($fields);
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_create' => $response]);
        return $response;
    }
    protected function removeMessage($id){
        $app_id = $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores');
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications/".$id."?app_id=".$app_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_delete' => $response]);
        return $response;
    }
    protected function getImageUrl($ruleItem,$key){
        if ($ruleItem->getData("is_{$key}_upload")){
            $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$ruleItem->getData("{$key}_upload");
        }else $url = $ruleItem->getData("{$key}_url");
        return $url;
    }
    private function checkUnShipped($order){
        $items = $order->getItems();
        $result = false;
        /** @var \Magento\Sales\Model\Order\Item $item */
        /** @var \Magento\Sales\Model\Order $order */
        $shippment = $order->getShipmentsCollection();
        if($shippment) {
            foreach($items as $item){
                $qty_shippped = $item->getQtyShipped();
                $qty_ordered = $item->getQtyInvoiced();
                if($qty_shippped < $qty_ordered){
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }
}