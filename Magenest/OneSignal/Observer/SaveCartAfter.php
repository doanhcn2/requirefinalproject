<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Onepage\Success;
use Magenest\OneSignal\Model\RuleFactory;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface as Logger;

class SaveCartAfter implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $success;
    protected $ruleFactory;
    protected $order;
    protected $_logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Success $success,
        RuleFactory $ruleFactory,
        Order $order,
        Logger $logger
    ) {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->success = $success;
        $this->ruleFactory = $ruleFactory;
        $this->order = $order;
        $this->_logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $cart_notification = $this->scopeConfig->getValue("onesignal/advance_setting/cart_notification", 'stores');
        if (!$cart_notification) {
            return $this;
        } else {
            /** @var  \Magento\Checkout\Model\Cart $cart */
            $cart = $observer->getEvent()->getCart();
            if ($cart->getQuote()->getItemsQty() > 0) {
                $ruleCollection = $this->ruleFactory->create();
                $now = new \DateTime();
                $ruleItems = $ruleCollection->getCollection()
                    ->addFieldToFilter('rule_type', 'abandoned')
                    ->addFieldToFilter('is_active', '1')
                    ->addFieldToFilter('from_date', [
                        ['lteq' => $now->format('Y-m-d')],
                        ['from_date', 'null' => '']
                    ])->addFieldToFilter('to_date', [
                        ['gteq' => $now->format('Y-m-d')],
                        ['from_date', 'null' => '']
                    ])->setOrder('sort_order', 'DESC')->load();
                $send_one_message = $this->scopeConfig->getValue("onesignal/advance_setting/cart_notification_once", 'stores');

                foreach ($ruleItems as $ruleItem) {
                    $result = $this->checkRule($cart->getQuote(), $ruleItem);
                    if ($result) {
                        $time_to_abandoned = $ruleItem->getTimeToAbandoned();
                        $date = $now->modify('+' . $time_to_abandoned . ' seconds')->format('Y-m-d h:i:s \G\M\T O');
                        if ($cart->getQuote()->getOnesignalId())
                            $this->removeMessage($cart->getQuote()->getOnesignalId());
                        $response = json_decode($this->sendMessage($ruleItem, $date, $cart->getQuote()), true);
                        $cart->getQuote()->setData('onesignal_id', $response['id']);
                        try {
                            $cart->getQuote()->save();
                        } catch (\Exception $e) {
                            $r = $e;
                        }
                        if ($send_one_message) {
                            break;
                        }
                    }
                }
                $cart->setData('onesinal_new', '0');
                return $this;
            } else {
                if ($cart->getQuote()->getOnesignalId())
                    $this->removeMessage($cart->getQuote()->getOnesignalId());
                return $this;
            }
        }

    }

    protected function sendMessage($ruleItem, $start_date, $cart)
    {
        /** @var \Magento\Quote\Model\Quote $cart */
        $message = str_replace([
            "{{customer_email}}", "{{customer_firstname}}", "{{customer_lastname}}", "{{customer_middlename}}", "{{grand_total}}", "{{created_at}}"
        ], [
            $cart->getCustomerEmail(), $cart->getCustomerFirstname(), $cart->getCustomerLastname(), $cart->getCustomerMiddlename(), $cart->getGrandTotal(), $cart->getCreatedAt()
        ], $ruleItem->getMessage());
        $title = str_replace([
            "{{customer_email}}", "{{customer_firstname}}", "{{customer_lastname}}", "{{customer_middlename}}", "{{grand_total}}", "{{created_at}}"
        ], [
            $cart->getCustomerEmail(), $cart->getCustomerFirstname(), $cart->getCustomerLastname(), $cart->getCustomerMiddlename(), $cart->getGrandTotal(), $cart->getCreatedAt()
        ], $ruleItem->getTitle());
        $content = array(
            "en" => $message
        );
        $heading = array(
            "en" => $title
        );
        $icon = $this->getImageUrl($ruleItem, 'icon');
        $image = $this->getImageUrl($ruleItem, 'image');
        $include_player_ids = $cart->getOnesignalPlayerId() ? [$cart->getOnesignalPlayerId()] : null;
        $datediff = strtotime($ruleItem->getToDate()) - strtotime($ruleItem->getFromDate());
        $ttl = round($datediff / (60 * 60 * 24));
        $fields = array(
            'app_id' => $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores'),
            'included_segments' => array('Active Users'),
            'include_player_ids' => $include_player_ids,
            'contents' => $content,
            'headings' => $heading,
            'send_after' => $start_date,
            'delayed_option' => 'last-active',
            'chrome_web_icon' => $icon,
            'chrome_web_image' => $image,
            'ttl' => $ttl,
            'url' => $ruleItem->getLaunchUrl()
        );
        if ($ttl <= 0) {
            unset($fields['ttl']);
        }
        if ($include_player_ids != null) {
            unset($fields['included_segments']);
        }else
            unset($fields['include_player_ids']);

        $fields = json_encode($fields);
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_create' => $response]);
        return $response;
    }

    protected function removeMessage($id)
    {
        $app_id = $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores');
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications/" . $id . "?app_id=" . $app_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_delete' => $response]);
        return $response;
    }

    protected function getImageUrl($ruleItem, $key)
    {
        if ($ruleItem->getData("is_{$key}_upload")) {
            $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $ruleItem->getData("{$key}_upload");
        } else $url = $ruleItem->getData("{$key}_url");
        return $url;
    }

    protected function checkRule($item, $rule)
    {
        $result = true;
        /** @var \Magenest\OneSignal\Model\Rule $rule */
        $conditions = is_array($rule) ? $rule : json_decode($rule->getConditionsSerialized(), true);
        if (isset($conditions)) {
            $aggregator = $conditions['aggregator'];
            $result = $conditions['value'] == 0 ? false : true;
            if (isset($conditions['conditions'])) {
                foreach ($conditions['conditions'] as $condition) {
                    $operator = $condition['operator']; // ==
                    $value = $condition['value']; //VN
                    $attribute = $condition['attribute'];
                    if (isset($condition['conditions'])) {
                        $result = $this->checkRule($item, $condition);
                    } elseif (!isset($attribute)) {
                        $result = false;
                        break;
                    } elseif ($attribute == 'category_ids') {
                        foreach ($item->getItems() as $i) {
                            $categories = $i->getProduct()->getData($attribute);
                            sort($categories);
                            $cat = implode(", ", $categories);
                            if ($aggregator == 'all')
                                $result = ($result and $this->operator($cat, $value, $operator));
                            else $result = ($result or $this->operator($cat, $value, $operator));

                        }
                        continue;
//                        $categories = $item->getData($attribute);

                    } else {
                        $a = $item->getData($attribute);
                    }
                    if (!isset($condition['conditions'])) {
                        if ($aggregator == 'all')
                            $result = ($result and $this->operator($a, $value, $operator));
                        else $result = ($result or $this->operator($a, $value, $operator));
                    }

                }
            }
            $result = $conditions['value'] == 0 ? !$result : $result;
        }
        return $result;
    }

    protected function operator($item, $value, $operator)
    {
        switch ($operator) {
            case '==':
                if ($item == $value)
                    return true;
                else return false;
                break;
            case '!=':
                if ($item != $value)
                    return true;
                else return false;
                break;
            case '>=':
                if ($item >= $value)
                    return true;
                else return false;
                break;
            case '<=':
                if ($item <= $value)
                    return true;
                else return false;
                break;
            case '>':
                if ($item > $value)
                    return true;
                else return false;
                break;
            case '<':
                if ($item < $value)
                    return true;
                else return false;
                break;
            case '{}':
                if (strpos($item, $value) !== false)
                    return true;
                else return false;
                break;
            case '!{}':
                if (strpos($item, $value) == false)
                    return true;
                else return false;
                break;
            case '()':
                if (strpos($value, $item) !== false)
                    return true;
                else return false;
                break;
            case '!()':
                if (strpos($value, $item) == false)
                    return true;
                else return false;
                break;
            default:
                return true;
        }
    }
}