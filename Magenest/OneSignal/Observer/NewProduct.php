<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\OneSignal\Model\RuleFactory;
use Psr\Log\LoggerInterface as Logger;

class NewProduct implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $ruleFactory;
    protected $_logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        RuleFactory $ruleFactory,
        Logger $logger
    )
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->ruleFactory = $ruleFactory;
        $this->storeManager = $storeManager;
        $this->_logger = $logger;
    }
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $product_notification = $this->scopeConfig->getValue("onesignal/advance_setting/product_notification", 'stores');
        if(!$product_notification||$product->getData('onesinal_new') != '1'||$product->getStatus()!='1'||$product->getQuantityAndStockStatus()!='1'){
            return $this;
        }
        else{
            /** @var \Magento\Catalog\Model\Product $product */
            $ruleCollection = $this->ruleFactory->create();
            $now = new \DateTime();
            $ruleItems = $ruleCollection->getCollection()
                ->addFieldToFilter('rule_type', 'product')
                ->addFieldToFilter('is_active', '1')
                ->addFieldToFilter('from_date', [
                    ['lteq' => $now->format('Y-m-d')],
                    ['from_date' ,  'null'=>'']
                ])->addFieldToFilter('to_date', [
                    ['gteq' => $now->format('Y-m-d')],
                    ['from_date' ,  'null'=>'']
                ])->setOrder('sort_order','DESC' )->load();
            $send_one_message = $this->scopeConfig->getValue("onesignal/advance_setting/product_notification_once", 'stores');
            foreach ($ruleItems as $ruleItem) {
                $result = $this->checkRule($product, $ruleItem);
                if ($result) {
                    $this->sendMessage($product, $ruleItem);
                    if ($send_one_message) {
                        break;
                    }
                }
            }
            $product->setData('onesinal_new','0');
            $product->save();
            return $this;
        }
    }
    protected function checkRule($item, $rule)
    {
        $result = true;
        /** @var \Magenest\OneSignal\Model\Rule $rule */
        $conditions = json_decode($rule->getConditionsSerialized(), true);
        if(isset($conditions)){
            $aggregator = $conditions['aggregator'];
            $result = $conditions['value'] == 0 ? false : true;
            if(isset($conditions['conditions'])) {
                foreach ($conditions['conditions'] as $condition) {
                    $operator = $condition['operator']; // ==
                    $value = $condition['value']; //VN
                    $attribute = $condition['attribute'];
                    if (!isset($attribute)) {
                        $result = false;
                        break;
                    } elseif ($attribute == 'category_ids') {
                        $categories = $item->getData($attribute);
                        sort($categories);
                        $a = implode(", ", $categories);
                    } else {
                        $a = $item->getData($attribute);
                    }
                    if ($aggregator == 'all')
                        $result = ($result and $this->operator($a, $value, $operator));
                    else $result = ($result or $this->operator($a, $value, $operator));
                }
            }
            $result = $conditions['value'] == 0 ? !$result : $result;
        }
        return $result;
    }

    protected function operator($item, $value, $operator)
    {
        switch ($operator) {
            case '==':
                if ($item == $value)
                    return true;
                else return false;
                break;
            case '!=':
                if ($item != $value)
                    return true;
                else return false;
                break;
            case '>=':
                if ($item >= $value)
                    return true;
                else return false;
                break;
            case '<=':
                if ($item <= $value)
                    return true;
                else return false;
                break;
            case '>':
                if ($item > $value)
                    return true;
                else return false;
                break;
            case '<':
                if ($item < $value)
                    return true;
                else return false;
                break;
            case '{}':
                if (strpos($item, $value) !== false)
                    return true;
                else return false;
                break;
            case '!{}':
                if (strpos($item, $value) == false)
                    return true;
                else return false;
                break;
            case '()':
                if (strpos($value, $item) !== false)
                    return true;
                else return false;
                break;
            case '!()':
                if (strpos($value, $item) == false)
                    return true;
                else return false;
                break;
            default:
                return true;
        }
    }

    protected function sendMessage($product, $ruleItem)
    {
        /**
         * @var $product \Magento\Catalog\Model\Product
         */
        $message = str_replace([
            "{{product_name}}","{{product_sku}}","{{product_price}}","{{product_short_description}}", "{{product_description}}"
        ],[
            $product->getName(), $product->getSku(), $product->getPrice(), $product->getData('short_description'), $product->getData('description')
        ],$ruleItem->getMessage());
        $title = str_replace([
            "{{product_name}}","{{product_sku}}","{{product_price}}","{{product_short_description}}", "{{product_description}}"
        ],[
            $product->getName(), $product->getSku(), $product->getPrice(), $product->getData('short_description'), $product->getData('description')
        ],$ruleItem->getTitle());
        $content = array(
            "en" => $message
        );
        $heading = array(
            "en" => $title
        );
        $icon = $this->getImageUrl($product,$ruleItem,'icon');
        $image = $this->getImageUrl($product,$ruleItem,'image');
        $fields = array(
            'app_id' => $this->scopeConfig->getValue("onesignal/account_setting/app_id",  'stores'),
            'included_segments' => array('Active Users'),
            'headings' => $heading,
            'contents' => $content,
            'chrome_web_icon' => $icon,
            'chrome_web_image' => $image,
            'url' => $this->storeManager->getStore()->getBaseUrl().$product->getUrlKey().'.html'
        );
        $fields = json_encode($fields);
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api",  'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.$onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['product_create' => $response]);
        return $response;
    }
    protected function getImageUrl($product,$ruleItem,$key){
        if($ruleItem->getData("auto_{$key}")){
            $url = $key == 'icon' ? $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$product->getThumbnail() :
                $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$product->getImage();
        }elseif ($ruleItem->getData("is_{$key}_upload")){
            $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$ruleItem->getData("{$key}_upload");
        }else $url = $ruleItem->getData("{$key}_url");
        return $url;
    }
}