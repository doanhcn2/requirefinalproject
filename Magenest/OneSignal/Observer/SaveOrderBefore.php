<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Onepage\Success;
use Magenest\OneSignal\Model\RuleFactory;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface as Logger;

class SaveOrderBefore implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $success;
    protected $ruleFactory;
    protected $order;
    protected $_logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Success $success,
        RuleFactory $ruleFactory,
        Order $order,
        Logger $logger
    )
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->success = $success;
        $this->ruleFactory = $ruleFactory;
        $this->order = $order;
        $this->_logger = $logger;
    }

    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $new_player_id = $customerSession->getPlayerId();
        $old_player_id = $order->getPlayerId();
        if(isset($new_player_id)){
            $order->setData('onesignal_player_id', $new_player_id);
        }
        return $this;
    }


}