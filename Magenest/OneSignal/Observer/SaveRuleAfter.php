<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Indexer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Block\Onepage\Success;
use Magenest\OneSignal\Model\RuleFactory;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface as Logger;

class SaveRuleAfter implements ObserverInterface
{
    protected $indexerRegistry;
    protected $indexerConfig;
    protected $scopeConfig;
    protected $storeManager;
    protected $success;
    protected $ruleFactory;
    protected $order;
    protected $_logger;

    public function __construct(
        IndexerRegistry $indexerRegistry,
        Config $indexerConfig,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Success $success,
        RuleFactory $ruleFactory,
        Order $order,
        Logger $logger
    )
    {
        $this->indexerRegistry = $indexerRegistry;
        $this->indexerConfig = $indexerConfig;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->success = $success;
        $this->ruleFactory = $ruleFactory;
        $this->order = $order;
        $this->_logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $news_notification = $this->scopeConfig->getValue("onesignal/advance_setting/news_notification", 'stores');
        if(!$news_notification){
            return $this;
        }
        else{
            /** @var  \Magenest\OneSignal\Model\Rule $rule */
            $rule = $observer->getEvent()->getRule();
            if($rule->getRuleType() != 'news'){
                return $this;
            }
            else {
                $now = new \DateTime();
                $from_date = $rule->getFromDate();
                if($from_date <= $now->format('Y-m-d')){
                    $start_date = $now->format('Y-m-d h:i:s \G\M\T O');
                }else{
                    $start_date = $rule->getFromDate()->format('Y-m-d h:i:s \G\M\T O');
                }
                $this->sendMessage( $rule, $start_date);

                return $this;
            }
        }

    }

    protected function sendMessage($ruleItem, $start_date)
    {
        $content = array(
            "en" => $ruleItem->getMessage()
        );
        $heading = array(
            "en" => $ruleItem->getTitle()
        );
        $icon = $this->getImageUrl($ruleItem,'icon');
        $image = $this->getImageUrl($ruleItem,'image');

        $timeDiff = abs($ruleItem->getToDate() - $ruleItem->getFromDate());
        $ttl = intval($timeDiff);
        $fields = array(
            'app_id' => $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores'),
            'included_segments' => array('Active Users'),
            'contents' => $content,
            'headings' => $heading,
            'send_after' => $start_date,
            'chrome_web_icon' => $icon,
            'chrome_web_image' => $image,
            'ttl' => $ttl,
            'url' => $ruleItem->getLaunchUrl()
        );
        if($ttl <= 0){
            unset($fields['ttl']);
        }
        $fields = json_encode($fields);
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_create' => $response]);
        return $response;
    }
    protected function removeMessage($id){
        $app_id = $this->scopeConfig->getValue("onesignal/account_setting/app_id", 'stores');
        $onesignal_rest_api = $this->scopeConfig->getValue("onesignal/account_setting/onesignal_rest_api", 'stores');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications/".$id."?app_id=".$app_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_rest_api));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        $this->_logger->info('onesignal_response_info', ['order_delete' => $response]);
        return $response;
    }
    protected function getImageUrl($ruleItem,$key){
        if ($ruleItem->getData("is_{$key}_upload")){
            $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$ruleItem->getData("{$key}_upload");
        }else $url = $ruleItem->getData("{$key}_url");
        return $url;
    }
}