require([
    'jquery',
    'mage/url'
], function ($, url) {
    var OneSignal = window.OneSignal || [];
    url.setBaseUrl(BASE_URL);
    var ajaxUrl = url.build('onesignal/customer/session');
    OneSignal.push(function () {
        // console.log(window.onesignalWebhookReceiver);
        OneSignal.init({
            appId: window.getAccountSettings,   //account setting
            webhooks: {
                cors: false, // Defaults to false if omitted
                'notification.displayed': window.onesignalWebhookReceiver // e.g. https://site.com/hook
                // ... follow the same format for any event in the list above
            }
        });
        OneSignal.getUserId(function (userId) {
            // console.log("OneSignal User ID:", userId);
            // // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316
            // console.log(ajaxUrl);
            // console.log(window.location.origin);
            if (userId) {
                // var url = ajaxUrl;
                $.ajax({
                    url: ajaxUrl,
                    type: 'post',
                    async: true,
                    data: {playerId: userId, isAjax: 'Yes'},
                    // contentType: "application/json; charset=utf-8",
                    // dataType: "json",
                    success: function (data) {
                        //console.log("OneSignal User ID:", playerId);
                    }
                });
            }
        });
    });
    // OneSignal.push(["init", {
    //     // Your other settings
    //     webhooks: {
    //         cors: false, // Defaults to false if omitted
    //         'notification.displayed': 'https://webhook.site/27e16cd7-92c9-45b0-9fc5-9b9873cf001d' // e.g. https://site.com/hook
    //         // ... follow the same format for any event in the list above
    //     }
    // }]);
    // console.log('blahablah');
});