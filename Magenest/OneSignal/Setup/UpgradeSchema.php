<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\OneSignal\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => 255,
                    'nullable' => true,
                    'comment' => 'Title'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'auto_icon',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'size' => null,
                    'nullable' => false, 'default' => '0',
                    'comment' => 'Auto icon'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'is_icon_upload',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'size' => null,
                    'nullable' => false, 'default' => '0',
                    'comment' => 'Is Icon Upload'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'auto_image',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'size' => null,
                    'nullable' => false, 'default' => '0',
                    'comment' => 'Is Icon Upload'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'icon_upload',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => 255,
                    'nullable' => true,
                    'comment' => 'Icon'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'icon_url',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '64k',
                    'nullable' => true,
                    'comment' => 'Icon Url'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'is_image_upload',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'size' => null,
                    'nullable' => false, 'default' => '1',
                    'comment' => 'Is Image Upload'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'image_upload',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => 255,
                    'nullable' => true,
                    'comment' => 'Image Upload'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'image_url',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '64k',
                    'nullable' => true,
                    'comment' => 'Image Url'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'at_time',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => 255,
                    'nullable' => true,
                    'comment' => 'At Time'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'launch_url',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '64k',
                    'nullable' => false, 'default' => '',
                    'comment' => 'Launch Url'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.8.0', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('magenest_onesignal_rules'),
                'time_to_abandoned',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
//                    'size' => '64k',
                    'nullable' => false, 'default' => '5',
                    'comment' => 'Time To Abandoned'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('quote'),
                'onesignal_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '64k',
                    'nullable' => false, 'default' => '',
                    'comment' => 'OneSignal Id'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('quote'),
                'onesignal_player_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '64k',
                    'nullable' => true,
                    'comment' => 'OneSignal Player Id'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.9.1', '<')) {
            $this->createDevicesTable($installer);
            $this->createNotificationsTable($installer);
            $this->createReadStatusTable($installer);
        }
        if (version_compare($context->getVersion(), '1.9.2', '<')) {
            $this->addCreatedAtColumn($installer);
        }
        $installer->endSetup();
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     */
    private function addCreatedAtColumn($installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable("magenest_onesignal_notifications"),
            "created_at",
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                'size' => null,
                'nullable' => false,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
                'comment' => 'Created At'
            ]
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createDevicesTable($installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_onesignal_device_id')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'record id'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'customer id'
        )->addColumn(
            'device_id',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'device id'
        )->setComment('Devices (or players or users) on Onesignal server');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createNotificationsTable($installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_onesignal_notifications')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'record id'
        )->addColumn(
            'notif_id',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'onesignal notification ids'
        )->addColumn(
            'heading',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'heading'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'content'
        )->addColumn(
            'url',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'URL to launch'
        )->addColumn(
            'icon',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'icon url'
        )->addColumn(
            'badge',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'badge img url'
        )->addColumn(
            'image',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'img url'
        )->addColumn(
            'priority',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'priority high or normal'
        )->addColumn(
            'data',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'additional data'
        )->setComment('Onesignal Notifications');

        $installer->getConnection()->createTable($table);
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     * @throws \Zend_Db_Exception
     */
    private function createReadStatusTable($installer)
    {
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_onesignal_notification_read_status')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'record id'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'customer id'
        )->addColumn(
            'local_notif_id',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Notif id on local server'
        )->addColumn(
            'remote_notif_id',
            Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Notif id on Onesignal server'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Status'
        )->setComment('notification read status');

        $installer->getConnection()->createTable($table);
    }
}
