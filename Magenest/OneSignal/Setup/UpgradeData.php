<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Setup;

use Magento\Catalog\Model\Product;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Upgrade Data script
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class UpgradeData implements UpgradeDataInterface
{

    protected $eavSetupFactory;

    function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $this->addOneSignalAttribute($eavSetup);
            $setup->endSetup();
        }
        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $this->addOnesignalCategorynotification($eavSetup);
            $setup->endSetup();
        }
        if (version_compare($context->getVersion(), '1.6.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $this->addOneSignalid($eavSetup);
            $setup->endSetup();
        }
        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $setup->startSetup();
            $this->addPlayerId($eavSetup);
            $setup->endSetup();
        }
    }
    private function addOneSignalAttribute($eavSetup)
    {
        $eavSetup->addAttribute(
            Product::ENTITY,
            'onesinal_new',
            [
                'group' => 'General',
                'type' => 'int',
                'visible' => 1,
                'required' => 0,
                'input' => 'boolean',
                'user_defined' => 1,
                'visible_on_front' => 0,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'OneSignal Notification',
                'system' => 0,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0
            ]
        );
    }
    private function addOneSignalid($eavSetup)
    {
        $eavSetup->addAttribute(
            \Magento\Sales\Model\Order::ENTITY,
            'onesignal_notification_id',
        [
            'type'     => "varchar",
            'label'    => "Order Notification Id",
            'input'    => "text",
            'user_defined' => true,
            'visible' => 0,
        ]
        );
    }
    private function addPlayerId($eavSetup)
    {
        $eavSetup->addAttribute(
            \Magento\Sales\Model\Order::ENTITY,
            'onesignal_player_id',
            [
                'type'     => "varchar",
                'label'    => "Order Notification Id",
                'input'    => "text",
                'user_defined' => true,
                'visible' => 0,
            ]
        );
    }
    private function addOnesignalCategorynotification($eavSetup){
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'send_category_notification',
            [
                'type' => 'int',
                'label' => 'Send Category Notification',
                'input' => 'boolean',
                'required' => false,
                'visible'  => true,
                'sort_order' => 12,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
    }
    private function addIsSentNoti($eavSetup){
        $eavSetup->addAttribute(
            \Magento\Quote\Model\Quote::ENTITY,
            'onesignal_in_quote',
            [
                'type' => 'int',
                'label' => 'OneSignal in quote',
                'input' => 'boolean',
                'required' => false,
                'visible'  => true,
                'sort_order' => 12,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]
        );
    }
}
