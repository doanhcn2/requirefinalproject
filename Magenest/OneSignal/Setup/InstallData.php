<?php
/**
 * Copyright © 2018 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\OneSignal\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        $this->addOneSignalAttribute($eavSetup);
        $setup->endSetup();
    }

    private function addOneSignalAttribute($eavSetup)
    {
        $eavSetup->addAttribute(
            Product::ENTITY,
            'onesinal_new',
            [
                'group' => 'General',
                'type' => 'int',
                'visible' => 0,
                'required' => 0,
                'input' => 'boolean',
                'user_defined' => 1,
                'visible_on_front' => 0,
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'label' => 'OneSignal Notification',
                'system' => 0,
                'is_used_in_grid' => 0,
                'is_visible_in_grid' => 0,
                'is_filterable_in_grid' => 0
            ]
        );
    }
}