<?php
namespace Magenest\Insight\Controller\Index;

use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;

class Index extends AbstractVendor
{
    public function execute()
    {
        $this->_renderPage(null, 'magenest_insight');
    }
}