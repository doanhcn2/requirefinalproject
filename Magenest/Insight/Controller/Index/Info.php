<?php
namespace Magenest\Insight\Controller\Index;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Unirgy\Dropship\Helper\Data as HelperData;
use Unirgy\DropshipVendorAskQuestion\Controller\Vendor\AbstractVendor;
use Unirgy\DropshipVendorAskQuestion\Model\QuestionFactory;
use Magento\Framework\Controller\ResultFactory;

class Info extends AbstractVendor
{
    public function execute()
    {
        $view = $this->_hlp->createObj('\Magento\Framework\App\ViewInterface');
        $this->_setTheme();
        $view->addActionLayoutHandles();
        /** @var \Magenest\Questions\Block\Questions\Index $infoBlock */
        $infoBlock = $view->getLayout()->getBlock('info.insight');
        $view->getLayout()->initMessages();
        $htmlData = $infoBlock->toHtml();
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData(['result' => $htmlData]);
    }
}