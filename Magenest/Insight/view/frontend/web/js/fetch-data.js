define([
    'Magento_Ui/js/modal/alert',
    'jquery',
    'mage/loader',
    'loader'
], function (alert, $) {
    var current_product_id;

    function fetchData(url, product_id) {
        $.ajax({
            url: url,
            type: 'post',
            data: {
                product_id: product_id
            },
            dataType: 'json',
            showLoader: true,
            success: function (response) {
                if (response.result) {
                    $('div#insight-container').html(response.result);
                } else {
                    $('div#insight-container').html('no information');
                }
            },
            error: function () {
                alert({
                    content: $.mage.__('Sorry, something went wrong. Please try again later.')
                });
            }
        });
    }

    $('select#campaign-selector').on('change', function (e) {
        var product_id = $('select#campaign-selector option:selected').val();
        if (product_id && product_id !== current_product_id)
            fetchData(window.BASE_URL + 'index/info', product_id);
    });

    return function (config) {
        if (config.product_id != undefined && config.product_id != '')
            fetchData(config.url, config.product_id);
    }
})