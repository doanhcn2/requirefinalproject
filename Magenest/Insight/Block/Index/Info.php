<?php

namespace Magenest\Insight\Block\Index;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Items\AbstractItems;

class Info extends AbstractItems
{
    protected $campaignFactory;
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        Registry $frameworkRegistry,
        array $data = [])
    {
        $this->campaignFactory = $dealFactory;
        $this->_coreRegistry = $frameworkRegistry;
        parent::__construct($context, $data);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }
}
