<?php

namespace Magenest\Insight\Block\Index\Info;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Items\AbstractItems;

class Campaign extends AbstractItems
{
    protected $_coreRegistry;
    protected $grouponFactory;
    protected $dealFactory;
    protected $productFactory;
    protected $id;
    protected $ids;
    protected $customerFactory;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        Registry $frameworkRegistry,
        array $data = [])
    {
        $this->customerFactory = $customerFactory;
        $this->dealFactory = $dealFactory;
        $this->grouponFactory = $grouponFactory;
        $this->productFactory = $productFactory;
        $this->_coreRegistry = $frameworkRegistry;
        parent::__construct($context, $data);
        $id = (int)$this->getRequest()->getParam('product_id');
        $this->setCouponIds($id);
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getCouponIds()
    {
        return $this->ids;
    }

    public function setCouponIds($id)
    {
        if ($id != -1)
            $this->ids = $this->dealFactory->create()->getCollection()->addFieldToFilter('parent_id', $id)->getColumnValues('product_id');
        else
            $this->ids = $this->dealFactory->create()->getCollection()->addFieldToFilter('product_type', 'coupon')->addFieldToFilter('vendor_id', $this->getVendorId())->getColumnValues('product_id');
    }

    public function getSold()
    {
        $ids = $this->getCouponIds();
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)));
        return (int)@count($collection);
    }

    public function getRedeemed()
    {
        $ids = $this->getCouponIds();
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('product_id', array('in' => implode(',', $ids)))
            ->addFieldToFilter('status', '2');
        return (int)@count($collection);
    }

    public function getRemaining()
    {
        $ids = $this->getCouponIds();
        $count = 0;
        foreach ($ids as $id) {
            /** @var \Magento\Catalog\Model\Product $model */
            $model = $this->productFactory->create()->load($id);
            $count += $model->getData('quantity_and_stock_status')['qty'];
        }
        return $count;
    }

    public function getGroupon()
    {
        $id = (int)$this->getRequest()->getParam('product_id');
        if ($id != -1)
            return $this->grouponFactory->create()->getCollection()->addFieldToFilter('product_id', ['in' => $this->getCouponIds()]);
        else
            return $this->grouponFactory->create()->getCollection()->addFieldToFilter('vendor_id', $this->getVendorId());
    }
}
