<?php

namespace Magenest\Insight\Block\Index\Info;

class Sales extends \Magenest\Overview\Block\Index\PaymentChart
{
    public function getOption($daynum)
    {
        $arr = [];
        $daystart = new \DateTime();
        $weekDay = (int)$daystart->format('N');
        $daystart->add(new \DateInterval('P' . (7 - $weekDay) . 'D'));
//        $daynumnull = $daynum - $weekDay;
        $date = $daystart->sub(new \DateInterval('P' . $daynum . 'D'));
        $startDateText = $date->format('F d');
        $soldInDays = $this->getSoldInDay($this->getSoldFromDay($daynum));
        for ($i = 0; $i < 6; $i++) {
            $dateText = $date->format('Y-m-d');
            $count = isset($soldInDays[$dateText]) ? $soldInDays[$dateText] : 0;
            $dateText = $date->format('M d');
            $arr[] = [$dateText, $count];
            $dateText = $date->format('F d');
            $date->add(new \DateInterval('P1D'));
        }
        return [$startDateText, $dateText, $arr];
    }

    public function getSoldFromDay($daynum)
    {
        $id = (int)$this->getRequest()->getParam('product_id');
        $day = $this->getDay($daynum);
        $collection = $this->grouponFactory->create()->getCollection()
            ->addFieldToFilter('created_at', array('gt' => $day));
        if ($id === -1)
            $collection->addFieldToFilter('vendor_id', $this->getVendorId());
        else
            $collection->addFieldToFilter('product_id', $id);
        return $collection;
    }
}
