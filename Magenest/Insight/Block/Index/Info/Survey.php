<?php

namespace Magenest\Insight\Block\Index\Info;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;

class Survey extends Campaign
{

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory, Context $context, \Magento\Catalog\Model\ProductFactory $productFactory, \Magenest\Groupon\Model\GrouponFactory $grouponFactory, \Magenest\Groupon\Model\DealFactory $dealFactory, Registry $frameworkRegistry, array $data = [])
    {
        parent::__construct($customerFactory, $context, $productFactory, $grouponFactory, $dealFactory, $frameworkRegistry, $data);
    }

    public function getPercent()
    {
//        $id = (int)$this->getRequest()->getParam('product_id');
//        $total = count($this->getCollection($id));
//        if ($total > 0) {
//            $option1 = count($this->getCollection($id)->addFieldToFilter('service_type', '1'));
//            $option2 = count($this->getCollection($id)->addFieldToFilter('service_type', '2'));
//            return array(
//                'total' => $total,
//                'option1' => (int)number_format(100*$option1/$total),
//                'option2' => (int)number_format(100*$option2/$total),
//                'option3' => 100 - (int)number_format(100*$option1/$total) - number_format(100*$option2/$total),
//            );
//        }
        return array(
            'total' => 0,
            'option1' => 0,
            'option2' => 0,
            'option3' => 0,
        );
    }

    public function getCollection($id)
    {
        return 1;
//        $surveyCollection = $this->collectionFactory->create();
//        if ($id === -1)
//            return $surveyCollection->addFieldToFilter('vendor_id', $this->getVendorId());
//        else
//            return $surveyCollection->addFieldToFilter('product_id', $id);
    }
}
