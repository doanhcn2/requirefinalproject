<?php

namespace Magenest\Insight\Block\Index\Info;

class Gender extends Campaign
{
    public function getCustomersGender()
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        $collection = $this->getGroupon();
        $result = [
            'male' => 0,
            'female' => 0
        ];
        $customers = $collection->getColumnValues('customer_id');
        $customers = array_unique($customers);
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $maleCustomerCollection */
        $maleCustomerCollection = $this->customerFactory->create()->getCollection();
        $maleCustomerCollection
            ->addFieldToFilter('entity_id', array('in' => implode(',', $customers)))
            ->addFieldToFilter('gender', '1');
        $male = count($maleCustomerCollection);
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $femaleCustomerCollection */
        $femaleCustomerCollection = $this->customerFactory->create()->getCollection();
        $femaleCustomerCollection->addFieldToSelect('gender')
            ->addFieldToFilter('entity_id', array('in' => implode(',', $customers)))
            ->addFieldToFilter('gender', '2');
        $female = count($femaleCustomerCollection);
        $total = $male + $female;
        if ($total > 0) {
            $result['male'] = (int)number_format(100 * $male / $total);
            $result['female'] = (int)number_format(100 * $female / $total);
            return $result;
        }
        return $result;
    }
}
