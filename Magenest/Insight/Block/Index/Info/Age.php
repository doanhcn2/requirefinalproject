<?php

namespace Magenest\Insight\Block\Index\Info;

class Age extends Campaign
{
    public function getCustomersAge()
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Groupon\Collection $collection */
        $collection = $this->getGroupon();
        $result = [0, 0, 0, 0, 0];
        $customers = $collection->getColumnValues('customer_id');
        sort($customers);
        $customers = array_unique($customers);
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection */
        $customerCollection = $this->customerFactory->create()->getCollection();
        $dobs = $customerCollection
            ->addFieldToFilter('entity_id', array('in' => implode(',', $customers)))
            ->addFieldToFilter('dob', ['neq' => 'NULL'])
            ->getColumnValues('dob');
        $total = count($customerCollection);
        foreach ($dobs as $dob) {
            $age = (int)\DateTime::createFromFormat('Y-m-d', $dob)
                ->diff(new \DateTime('now'))
                ->y;
            if ($age <= 17)
                $result[0]++;
            elseif ($age >= 18 && $age <= 25)
                $result[1]++;
            elseif ($age >= 26 && $age <= 35)
                $result[2]++;
            elseif ($age >= 36 && $age <= 50)
                $result[3]++;
            else
                $result[4]++;
        }
        if ($total > 0)
            foreach ($result  as $key => $value) {
                $result[$key] = (int)number_format(100 * $value / $total);
            }
        return $result;
    }
}
