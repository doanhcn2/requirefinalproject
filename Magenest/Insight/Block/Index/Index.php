<?php

namespace Magenest\Insight\Block\Index;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Items\AbstractItems;

class Index extends AbstractItems
{
    protected $campaignFactory;
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        Registry $frameworkRegistry,
        array $data = [])
    {
        $this->campaignFactory = $dealFactory;
        $this->_coreRegistry = $frameworkRegistry;
        parent::__construct($context, $data);
        $this->getCampaign();
    }

    public function getVendorId()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get('Unirgy\Dropship\Model\Session')->getVendorId();
    }

    public function getCampaign()
    {
        /** @var \Magenest\Groupon\Model\ResourceModel\Deal\Collection $collection */
        $collection = $this->campaignFactory->create()->getCollection()
            ->addFieldToFilter('vendor_id', $this->getVendorId())
            ->addFieldToFilter('product_type', 'configurable')
            ->addFieldToSelect('product_id')
            ->addFieldToSelect('deal_id');
        $conn = $collection->join($collection->getTable('catalog_product_entity_varchar'), 'main_table.product_id=catalog_product_entity_varchar.row_id ', 'catalog_product_entity_varchar.value AS product_name')
            ->getSelect()->distinct(true)->where("catalog_product_entity_varchar.attribute_id=73")->__toString();
        return $collection->getConnection()->fetchAll($conn);
    }
}
